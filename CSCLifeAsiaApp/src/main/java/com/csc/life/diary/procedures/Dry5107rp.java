/*
 * File: Dry5107rp.java
 * Date: DECEMBER 12, 2015 06:11 PM IST
 * Author: Vaibhav Chawda
 * 
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptTableDAM;
import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.diary.recordstructures.R5107rec;
import com.csc.life.unitlinkedprocessing.reports.R5107Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.diary.recordstructures.Drycntrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5107rp extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5107Report printFile = new R5107Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5107RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private FixedLengthStringData wsaaRptCount = new FixedLengthStringData(5);
		
	/* TABLES */
	private static final String t1693 = "T1693";
	
	/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5107H01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5107h01O = new FixedLengthStringData(73).isAPartOf(r5107H01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5107h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5107h01O, 1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5107h01O, 31);
	
	private DescTableDAM descIO = new DescTableDAM();
	private DrptTableDAM drptIO = new DrptTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R5107D01RecordInner r5107D01RecordInner = new R5107D01RecordInner();

	private FixedLengthStringData r5107E01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5107e01O = new FixedLengthStringData(73).isAPartOf(r5107E01Record, 0);
	public Dry5107rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
	wsaaRptCount.set("0");
		printFile.openOutput();
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
	//	indicarea.set(ZERO);
		
		wsaaOverflow.set("Y");
	
		//Get Company Description
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
				drylogrec.statuz.set(descIO.getStatuz());
				drylogrec.params.set(descIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				fatalError();
		}
		companynm.set(descIO.getLongdesc());
		
		//Set up the DRPTSRT ready to read...
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
				
		//Loop through each DRPTSRT record printing the R5106
		//report ..                                          
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
				
		
			}
		endReport500();
		printFile.close();
		}
protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
	SmartFileCode.execute(appVars, drptsrtIO);
	if (isNE(drptsrtIO.getStatuz(), varcom.oK)
	&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
		drylogrec.params.set(drptsrtIO.getParams());
		drylogrec.statuz.set(drptsrtIO.getStatuz());
		drylogrec.dryDatabaseError.setTrue();
		fatalError();//Modify for ILPI-65 
	}
	if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
	|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
	|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
	|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
		drptsrtIO.setStatuz(varcom.endp);
		return ;
	}
	if (isNE(dryoutrec.runNumber, ZERO)
	&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
		drptsrtIO.setStatuz(varcom.endp);
		return ;
	}
//	Check if the sort key has changed... 
	
	
		writeLine300();
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{/*Check first if totals have to be written before a new page
	 is written..                                              */
	checkTotals600();

	/*Check first if totals have to be written before a new page
	is written..                                                   */
	
	if (pageOverflow.isTrue()) {
		wsaaOverflow.set("N");
		newPage400();
		}
		/* Fill detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		
		if (isEQ(dryrDryrptRecInner.r5107Rectype,"UTRN")){
			r5107D01RecordInner.rectype.set("U");
		}
		if (isEQ(dryrDryrptRecInner.r5107Rectype,"ITRN")){
			r5107D01RecordInner.rectype.set("I");
		}
		if (isEQ(dryrDryrptRecInner.r5107Rectype,"COVR")){
			r5107D01RecordInner.rectype.set("C");
		}
		r5107D01RecordInner.lapind.set(dryrDryrptRecInner.r5107Lapind);
		r5107D01RecordInner.chdrnum.set(dryrDryrptRecInner.r5107Chdrnum);
		r5107D01RecordInner.life.set(dryrDryrptRecInner.r5107Life);
		r5107D01RecordInner.coverage.set(dryrDryrptRecInner.r5107Coverage);
		r5107D01RecordInner.rider.set(dryrDryrptRecInner.r5107Rider);
		r5107D01RecordInner.plnsfx.set(dryrDryrptRecInner.r5107Plnsfx);
		//r5107D01RecordInner.crtable.set(dryrDryrptRecInner.r5107Crtable);
		r5107D01RecordInner.vrtfnd.set(dryrDryrptRecInner.r5107Vrtfnd);
		r5107D01RecordInner.unityp.set(dryrDryrptRecInner.r5107Unityp);
		r5107D01RecordInner.tranno.set(dryrDryrptRecInner.r5107Tranno);
		r5107D01RecordInner.batctrcde.set(dryrDryrptRecInner.r5107Batctrcde);
		r5107D01RecordInner.cntamnt.set(dryrDryrptRecInner.r5107Cntamnt);
		r5107D01RecordInner.cntcurr.set(dryrDryrptRecInner.r5107Cntcurr);
		r5107D01RecordInner.fundamnt.set(dryrDryrptRecInner.r5107Fundamnt);
		r5107D01RecordInner.fndcurr.set(dryrDryrptRecInner.r5107Fndcurr);
		r5107D01RecordInner.fundrate.set(dryrDryrptRecInner.r5107Fundrate);
		r5107D01RecordInner.age.set(dryrDryrptRecInner.r5107Age);
		r5107D01RecordInner.triger.set(dryrDryrptRecInner.r5107Triger);
		printRecord.set(SPACES);
		printFile.printR5107d01(r5107D01RecordInner.r5107D01Record, indicArea);
//		wsaaOverflow.set("Y");
		
		//Increment the report record count..
		compute(wsaaRptCount).set(add(wsaaRptCount,1)); 
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		company.set(dryoutrec.company);
		printRecord.set(SPACES);
		printFile.printR5107h01(r5107H01Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/
	if (pageOverflow.isTrue()) {
		wsaaOverflow.set("N");
		newPage400();
	}
	printFile.printR5107e01(r5107E01Record, indicArea);
	}

protected void checkTotals600()
{
	/*Report totals will need to be printed where there is a 
	change of key..making sure a new page is written if    
	required..  */           
	
	//If there are no records printed, then exit this section..
	
	if (isEQ(wsaaRptCount,0)) {
		return;
	}
	
	/*Write New Page if required, setting the virtual fund
	in the header to match the one for the totals.  */    
	
	if (pageOverflow.isTrue()) {
		wsaaOverflow.set("N");
		newPage400();
	}
}	
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		dryoutrec.statuz.set(drylogrec.statuz);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		//exitProgram();
		//ILPI-61 
		a001FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5107DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5107Lapind = new FixedLengthStringData(1).isAPartOf(r5107DataArea, 0);
	private FixedLengthStringData r5107Rectype = new FixedLengthStringData(4).isAPartOf(r5107DataArea, 1);
	private FixedLengthStringData r5107Chdrnum = new FixedLengthStringData(8).isAPartOf(r5107DataArea, 5);
	private FixedLengthStringData r5107Life = new FixedLengthStringData(2).isAPartOf(r5107DataArea, 13);
	private FixedLengthStringData r5107Coverage = new FixedLengthStringData(2).isAPartOf(r5107DataArea, 15);
	private FixedLengthStringData r5107Rider = new FixedLengthStringData(2).isAPartOf(r5107DataArea, 17);
	private FixedLengthStringData r5107Plnsfx = new FixedLengthStringData(4).isAPartOf(r5107DataArea, 19);
	private FixedLengthStringData r5107Vrtfnd = new FixedLengthStringData(4).isAPartOf(r5107DataArea, 23);
	private FixedLengthStringData r5107Unityp = new FixedLengthStringData(1).isAPartOf(r5107DataArea, 27);
	private FixedLengthStringData r5107Tranno = new FixedLengthStringData(5).isAPartOf(r5107DataArea, 28);
	private FixedLengthStringData r5107Batctrcde = new FixedLengthStringData(4).isAPartOf(r5107DataArea, 33);
	private ZonedDecimalData r5107Cntamnt = new ZonedDecimalData(11, 2).isAPartOf(r5107DataArea, 37);
	private FixedLengthStringData r5107Cntcurr = new FixedLengthStringData(3).isAPartOf(r5107DataArea, 48);
	private ZonedDecimalData r5107Fundamnt = new ZonedDecimalData(11, 2).isAPartOf(r5107DataArea, 51);
	private FixedLengthStringData r5107Fndcurr = new FixedLengthStringData(3).isAPartOf(r5107DataArea, 62);
	private ZonedDecimalData r5107Fundrate = new ZonedDecimalData(10, 9).isAPartOf(r5107DataArea, 65);
	private ZonedDecimalData r5107Age = new ZonedDecimalData(11, 5).isAPartOf(r5107DataArea, 75);
	private FixedLengthStringData r5107Triger = new FixedLengthStringData(10).isAPartOf(r5107DataArea, 86);
}
/*
 * Class transformed  from Data Structure R5135-D01-RECORD--INNER
 */
private static final class R5107D01RecordInner { 

	private FixedLengthStringData r5107D01Record = new FixedLengthStringData(105);
	private FixedLengthStringData r5107d01O = new FixedLengthStringData(105).isAPartOf(r5107D01Record, 0);

	private FixedLengthStringData rectype = new FixedLengthStringData(1).isAPartOf(r5107d01O, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5107d01O, 1);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5107d01O, 9);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5107d01O, 11);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5107d01O, 13);
	private FixedLengthStringData plnsfx = new FixedLengthStringData(4).isAPartOf(r5107d01O, 15);
	//private ZonedDecimalData crtable = new ZonedDecimalData(4, 0).isAPartOf(r5107d01O, 14);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r5107d01O, 19);
	private FixedLengthStringData unityp = new FixedLengthStringData(1).isAPartOf(r5107d01O, 23);
	private FixedLengthStringData tranno = new FixedLengthStringData(5).isAPartOf(r5107d01O, 24);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5107d01O, 29);
	private ZonedDecimalData cntamnt = new ZonedDecimalData(17, 2).isAPartOf(r5107d01O, 33);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(r5107d01O, 50);
	private ZonedDecimalData fundamnt = new ZonedDecimalData(17, 2).isAPartOf(r5107d01O, 53);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r5107d01O, 70);
	private ZonedDecimalData fundrate = new ZonedDecimalData(18, 9).isAPartOf(r5107d01O, 73);
	private FixedLengthStringData lapind = new FixedLengthStringData(1).isAPartOf(r5107d01O, 91);
	private ZonedDecimalData age = new ZonedDecimalData(3, 0).isAPartOf(r5107d01O, 92);
	private FixedLengthStringData triger = new FixedLengthStringData(10).isAPartOf(r5107d01O, 95);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
