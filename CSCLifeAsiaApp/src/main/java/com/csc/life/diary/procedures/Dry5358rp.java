/*
 * File: Dry5358rp.java
 * Date: March 26, 2014 3:02:06 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5358RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.flexiblepremium.reports.R5358Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*    FLEXIBLE PREMIUMS BELOW MINIMUM VALUE ALLOWED.
*    ----------------------------------------------
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5358rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5358Report printFile = new R5358Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5358RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaTotMinFail = new PackedDecimalData(17, 2).init(ZERO);
	private ZonedDecimalData wsaaRptCount = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();
		/* WSAA-SORT-KEY */
	private FixedLengthStringData wsaaSortChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSortPayrseqno = new ZonedDecimalData(1, 0).setUnsigned();
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5358h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5358h01O = new FixedLengthStringData(31).isAPartOf(r5358h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5358h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5358h01O, 1);

	private FixedLengthStringData r5358d01Record = new FixedLengthStringData(65);
	private FixedLengthStringData r5358d01O = new FixedLengthStringData(63).isAPartOf(r5358d01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5358d01O, 0);
	private FixedLengthStringData btdate = new FixedLengthStringData(10).isAPartOf(r5358d01O, 8);
	private ZonedDecimalData linstamt = new ZonedDecimalData(11, 2).isAPartOf(r5358d01O, 18);
	private ZonedDecimalData minprem = new ZonedDecimalData(17, 2).isAPartOf(r5358d01O, 29);
	private ZonedDecimalData susamt = new ZonedDecimalData(17, 2).isAPartOf(r5358d01O, 46);

	private FixedLengthStringData r5358t01Record = new FixedLengthStringData(20);
	private FixedLengthStringData r5358t01O = new FixedLengthStringData(18).isAPartOf(r5358t01Record, 0);
	private ZonedDecimalData toporg = new ZonedDecimalData(18, 2).isAPartOf(r5358t01O, 0);
	private DescTableDAM descIO = new DescTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	//private ItdmTableDAM itdmIO = new ItdmTableDAM();
	//private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();

	public Dry5358rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		wsaaOverflow.set("Y");
		/* Initialise any working storage fields.*/
		/* Set up today's date...*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		/* Set up the DRPTSRT ready to read...*/
		drptsrtIO.setRecKeyData(SPACES);
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setSortkey(SPACES);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		/* Loop through each DRPTSRT record printing the R5358*/
		/* report ..*/
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		
		endReport500();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		/* Check that we are still working with the R5358 data.*/
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		/* Move the sort key details to the report copybook.*/
		dryrDryrptRecInner.r5358SortKey.set(drptsrtIO.getSortkey());
		/* Start writing the report details.*/
		writeLine300();
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Fill the detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		chdrnum.set(dryrDryrptRecInner.r5358Chdrnum);
		btdate.set(dryrDryrptRecInner.r5358Btdate);
		linstamt.set(dryrDryrptRecInner.r5358Linstamt);
		minprem.set(dryrDryrptRecInner.r5358Minprem);
		susamt.set(dryrDryrptRecInner.r5358Susamt);
		wsaaTotMinFail.add(dryrDryrptRecInner.r5358Susamt);
		/* Increment the report record count..*/
		/* Write the detail line to the report ....*/
		printRecord.set(SPACES);
		printFile.printR5358d01(r5358d01Record, indicArea);
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		company.set(dryoutrec.company);
		/* Get the company description..*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		companynm.set(descIO.getLongdesc());
		/*  Write the header details.....*/
		printRecord.set(SPACES);
		printFile.printR5358h01(r5358h01Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Write the accumulated totals of all records failing*/
		/* the minimum variance test.*/
		toporg.set(wsaaTotMinFail);
		wsaaTotMinFail.set(ZERO);
		printRecord.set(SPACES);
		printFile.printR5358t01(r5358t01Record, indicArea);
		/*EXIT*/
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		dryoutrec.statuz.set(drylogrec.statuz);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		//exitProgram();
		a000FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5358DataArea = new FixedLengthStringData(497).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5358Btdate = new FixedLengthStringData(10).isAPartOf(r5358DataArea, 0);
	private ZonedDecimalData r5358Linstamt = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 10);
	private ZonedDecimalData r5358Minprem = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 21);
	private ZonedDecimalData r5358Susamt = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 32);

	private FixedLengthStringData r5358SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r5358Chdrnum = new FixedLengthStringData(8).isAPartOf(r5358SortKey, 0);
	private ZonedDecimalData r5358Payrseqno = new ZonedDecimalData(1, 0).isAPartOf(r5358SortKey, 8).setUnsigned();
}
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
