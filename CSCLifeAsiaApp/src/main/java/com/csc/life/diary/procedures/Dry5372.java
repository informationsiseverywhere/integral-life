/*
 * File: Dry5372.java
 * Date: March 26, 2014 3:03:39 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5372.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.diary.dataaccess.CovriucTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Diary Initial Unit Cancellation Subroutine.
* ===========================================
*
* This program processes a contract's coverages due for Initial
* Unit Cancellation, based on the Initial Unit Cancellation Date
*
* Coverages are read using the COVRIUC logical, contract and
* coverage status codes are validated against T5679.
*
* Using Coverage type, the General Unit Linked Details table,
* T5540, is read for the Initial Unit Discount Factor. This is
* used to read the Initial Unit Discount Basis table, T5519. If
* either of the table reads are unsuccessful or the processing
* subroutine on T5519 is empty, do not process the coverage.
*
* The processing subroutine on T5519 is called for the coverage.
*
* The Initial Unit Cancellation Date on the coverage is increased
* by one frequency from T5519 before updating the coverage.
*
* The Contract Header record is updated and a PTRN transaction
* record written when the first coverage for the contract is
* processed.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5372 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5372");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTimeRound = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaTimeRound, "Y");
	private Validator otherTime = new Validator(wsaaTimeRound, "N");

	private FixedLengthStringData wsaaChdrValid = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaChdrValid, "Y");
	private Validator invalidContract = new Validator(wsaaChdrValid, "N");

	private FixedLengthStringData wsaaCovrValid = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaCovrValid, "Y");
	private Validator invalidCoverage = new Validator(wsaaCovrValid, "N");

	private FixedLengthStringData wsaaCovrsUpdated = new FixedLengthStringData(1).init("N");
	private Validator coveragesUpdated = new Validator(wsaaCovrsUpdated, "Y");
	private Validator coveragesNotUpdated = new Validator(wsaaCovrsUpdated, "N");
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0).init(ZERO);
	private ZonedDecimalData wsaaFirstIucdate = new ZonedDecimalData(8, 0).init(ZERO);
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String covrrec = "COVRREC";
	private static final String covriucrec = "COVRIUCREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t5519 = "T5519";
	private static final String t5540 = "T5540";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovriucTableDAM covriucIO = new CovriucTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private T5519rec t5519rec = new T5519rec();
	private T5540rec t5540rec = new T5540rec();
	private T5679rec t5679rec = new T5679rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit890
	}

	public Dry5372() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		finish180();
	}

protected void start110()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		initialise200();
		if (invalidContract.isTrue()) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
			/*        GO TO 190-EXIT                                           */
			return ;
		}
		/* Retrieve the COVRIUC records for the contract,*/
		/* if appropriate process them.*/
		fetchCoverage500();
		/* If no valid coverages are found for the contract,*/
		/* log the details and exit.*/
		if (coveragesNotUpdated.isTrue()) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
		}
	}

protected void finish180()
	{
		/* On completion pass parameters back to the linkage*/
		/* copybook to allow trigger details to be updated.*/
		finish4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaNewTranno.set(ZERO);
		wsaaFirstIucdate.set(varcom.vrcmMaxDate);
		/* Fetch the CHDR record for the entity parameters passed.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Check if contract valid*/
		validateContract300();
		if (invalidContract.isTrue()) {
			drycntrec.contotNumber.set(ct01);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
	}

protected void validateContract300()
	{
		start310();
	}

protected void start310()
	{
		/* Ensure that the CHDR record meets requirements ...*/
		invalidContract.setTrue();
		if (isEQ(chdrlifIO.getCntbranch(), drypDryprcRecInner.drypBranch)
		&& isEQ(chdrlifIO.getValidflag(), "1")
		&& isNE(chdrlifIO.getBillchnl(), "N")) {
			/*CONTINUE_STMT*/
		}
		else {
			invalidContract.setTrue();
			return ;
		}
		/* Read T5679 for valid statii for contract header and coverage.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Validate the contract status.*/
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIx.toInt()], chdrlifIO.getStatcode())) {
				wsaaIx.set(13);
				validContract.setTrue();
			}
		}
		if (invalidContract.isTrue()) {
			return ;
		}
		invalidContract.setTrue();
		/* Validate the premium status.*/
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnPremStat[wsaaIx.toInt()], chdrlifIO.getPstatcode())) {
				wsaaIx.set(13);
				validContract.setTrue();
			}
		}
	}

protected void fetchCoverage500()
	{
		/*BEGN*/
		/* Set up key to read the COVRIUC with a function of BEGN.*/
		covriucIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covriucIO.setChdrnum(chdrlifIO.getChdrnum());
		covriucIO.setInitUnitCancDate(ZERO);
		covriucIO.setFormat(covriucrec);
		covriucIO.setFunction(varcom.begn);
		covriucIO.setStatuz(varcom.oK);
		coveragesNotUpdated.setTrue();
		while ( !(isEQ(covriucIO.getStatuz(), varcom.endp))) {
			processCoverages600();
		}

		/*EXIT*/
	}

protected void processCoverages600()
	{
		coverages610();
	}

protected void coverages610()
	{
		SmartFileCode.execute(appVars, covriucIO);
		if (isNE(covriucIO.getStatuz(), varcom.oK)
		&& isNE(covriucIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(covriucIO.getStatuz());
			drylogrec.params.set(covriucIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(covriucIO.getStatuz(), varcom.endp)
		|| isNE(covriucIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covriucIO.getChdrnum(), chdrlifIO.getChdrnum())) {
			covriucIO.setStatuz(varcom.endp);
			return ;
		}
		invalidCoverage.setTrue();
		/* Save the earliest Initial Unit Cancellation date. Only Coverage*/
		/* records with this Initial Unit Cancellation date will be*/
		/* processed.*/
		if (isEQ(covriucIO.getInitUnitCancDate(), 0)) {
			covriucIO.setFunction(varcom.nextr);
			return ;
		}
		if (isEQ(wsaaFirstIucdate, varcom.vrcmMaxDate)) {
			wsaaFirstIucdate.set(covriucIO.getInitUnitCancDate());
		}
		if (isEQ(covriucIO.getInitUnitCancDate(), wsaaFirstIucdate)) {
			validateCovrStatus700();
		}
		else {
			covriucIO.setStatuz(varcom.endp);
			return ;
		}
		if (invalidCoverage.isTrue()) {
			covriucIO.setFunction(varcom.nextr);
			return ;
		}
		if (validCoverage.isTrue()) {
			if (firstTime.isTrue()) {
				updateChdrWritePtrn1000();
				otherTime.setTrue();
			}
			callT5519Subroutine1100();
			updateCovriucWriteCovr1200();
		}
		covriucIO.setFunction(varcom.nextr);
	}

protected void validateCovrStatus700()
	{
		validateStatus710();
	}

protected void validateStatus710()
	{
		/* Validate the coverage risk status.*/
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaIx.toInt()], covriucIO.getStatcode())) {
				wsaaIx.set(13);
				validCoverage.setTrue();
			}
		}
		if (invalidCoverage.isTrue()) {
			return ;
		}
		invalidCoverage.setTrue();
		/* Validate the coverage premium status.*/
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaIx.toInt()], covriucIO.getPstatcode())) {
				wsaaIx.set(13);
				validCoverage.setTrue();
			}
		}
		if (invalidCoverage.isTrue()) {
			return ;
		}
		readTables800();
	}

protected void readTables800()
	{
		try {
			t5540810();
			t5519850();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void t5540810()
	{
		/* Use the CRTABLE (covr table) to read T5540 for*/
		/* the Initial Units Discount Basis.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covriucIO.getCrtable());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5540)
		|| isNE(itdmIO.getItemitem(), covriucIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		/* If no discount basis exists, do not process the coverage.*/
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			invalidCoverage.setTrue();
			goTo(GotoLabel.exit890);
		}
	}

protected void t5519850()
	{
		/* Use the discount basis to read T5519 for the generic*/
		/* processing subroutine and relevant details.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5519);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5519)
		|| isNE(itdmIO.getItemitem(), t5540rec.iuDiscBasis)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
		/* If no discount subroutine exits, don't process the coverage.*/
		if (isEQ(t5519rec.subprog, SPACES)) {
			invalidCoverage.setTrue();
			return ;
		}
	}

protected void updateChdrWritePtrn1000()
	{
		updateChdr1010();
		writePtrn1050();
	}

protected void updateChdr1010()
	{
		/* Because we may process more than one coverage, reread the*/
		/* CHDR record with a function of READH before REWRTing it.*/
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Increment the Transaction number on the CHDR record.*/
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writePtrn1050()
	{
		/* Write a PTRN record with the parameters we have ...*/
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTransactionDate(wsaaFirstIucdate);
		ptrnIO.setPtrneff(wsaaFirstIucdate);
		/*                                PTRN-PTRNEFF                  */
		/*                                PTRN-DATESUB.                 */
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		varcom.vrcmTime.set(getCobolTime());
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(wsaaProg);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void callT5519Subroutine1100()
	{
		t55191110();
	}

protected void t55191110()
	{
		initialize(annprocrec.annpllRec);
		annprocrec.company.set(covriucIO.getChdrcoy());
		annprocrec.chdrnum.set(covriucIO.getChdrnum());
		annprocrec.life.set(covriucIO.getLife());
		annprocrec.coverage.set(covriucIO.getCoverage());
		annprocrec.rider.set(covriucIO.getRider());
		annprocrec.planSuffix.set(covriucIO.getPlanSuffix());
		annprocrec.effdate.set(covriucIO.getInitUnitCancDate());
		annprocrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		annprocrec.crdate.set(covriucIO.getCrrcd());
		annprocrec.crtable.set(covriucIO.getCrtable());
		annprocrec.user.set(wsaaProg);
		annprocrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		annprocrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		annprocrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		annprocrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		annprocrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		annprocrec.batcpfx.set(drypDryprcRecInner.drypBatcpfx);
		annprocrec.annchg.set(t5519rec.annchg);
		annprocrec.initUnitChargeFreq.set(t5519rec.initUnitChargeFreq);
		callProgram(t5519rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(annprocrec.statuz);
			drylogrec.params.set(annprocrec.annpllRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

protected void updateCovriucWriteCovr1200()
	{
		updateCovriuc1210();
		writeCovr1250();
	}

protected void updateCovriuc1210()
	{
		coveragesUpdated.setTrue();
		/* Rewrite the COVRIUC record with a validflag of 2, and*/
		/* set the CURRTO to the Cancellation Date just processed.*/
		covriucIO.setValidflag("2");
		covriucIO.setCurrto(covriucIO.getInitUnitCancDate());
		covriucIO.setFunction(varcom.writd);
		covriucIO.setFormat(covriucrec);
		SmartFileCode.execute(appVars, covriucIO);
		if (isNE(covriucIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covriucIO.getStatuz());
			drylogrec.params.set(covriucIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writeCovr1250()
	{
		setNextCancelDate1300();
		covrIO.setParams(SPACES);
		/* Read the COVR record using key fields passed from COVRIUC.*/
		covrIO.setChdrcoy(covriucIO.getChdrcoy());
		covrIO.setChdrnum(covriucIO.getChdrnum());
		covrIO.setLife(covriucIO.getLife());
		covrIO.setCoverage(covriucIO.getCoverage());
		covrIO.setRider(covriucIO.getRider());
		covrIO.setPlanSuffix(covriucIO.getPlanSuffix());
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Update relevant fields to create a new COVR record.*/
		covrIO.setValidflag("1");
		covrIO.setInitUnitCancDate(datcon2rec.intDate2);
		covrIO.setCurrfrom(covriucIO.getInitUnitCancDate());
		covrIO.setCurrto(varcom.vrcmMaxDate);
		covrIO.setTranno(chdrlifIO.getTranno());
		covrIO.setFunction(varcom.writr);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void setNextCancelDate1300()
	{
		para1310();
	}

protected void para1310()
	{
		/* Add 1 T5519 frequency to the Unit Cancellation date.*/
		initialize(datcon2rec.datcon2Rec);
		if (isEQ(t5519rec.initUnitChargeFreq, NUMERIC)) {
			datcon2rec.frequency.set(t5519rec.initUnitChargeFreq);
		}
		else {
			datcon2rec.frequency.set("00");
		}
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(covriucIO.getInitUnitCancDate());
		datcon2rec.intDate2.set(ZERO);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

protected void finish4000()
	{
		/*FINISH*/
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
}
}
