/*
 * File: Dryh520.java
 * Date: January 15, 2015 4:14:51 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYH520.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HpuapfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hpuapf;
import com.csc.life.cashdividends.recordstructures.Hdivdrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.dataaccess.CovrbonTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6639rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UstmTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(c) Copyright Continuum Corporation Ltd.  1986....1995.
 *    All rights reserved.  Continuum Confidential.
 *
 *REMARKS.
 *
 *   This program is responsible for processing the coverage record 
 *   based on the Unit Statement Date(CBUNST) and Bonus indicator.
 *   Dividend is calculated and allocated to each eligible
 *   coverage by the subroutine defined in T6639.  Then these
 *   dividend transaction details are written to 2 files,
 *        HDIS - dividend transaction summary
 *        HDIV - dividend transaction details
 *
 *   PROCESSING
 *   ==========
 *
 *   Control totals:
 *     01 - No. COVR recs read
 *     02 - No. CHDR with OS premium
 *     03 - No. COVR recs processed
 *     04 - Total value of Dividend
 *
 *   Error Processing:
 *     If a system error move the error code into the DRYL-STATUZ
 *     If a database error move the XXXX-PARAMS to DRYL-PARAMS.
 *     Perform the A000-FATAL-ERROR section.
 *
 ****************************************************************** ****
 *                                                                     *
 * </pre>
 */
public class Dryh520 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH520");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);

	private FixedLengthStringData wsaaT6639Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBonus = new FixedLengthStringData(4).isAPartOf(wsaaT6639Key, 0);
	private FixedLengthStringData wsaaPremStat = new FixedLengthStringData(2).isAPartOf(wsaaT6639Key, 4);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLastBonusDate = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaNewBonusDate = new PackedDecimalData(8, 0).init(0);
	private ZonedDecimalData wsaaNextIntDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNextIntdate = new FixedLengthStringData(8).isAPartOf(wsaaNextIntDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextintYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextIntdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextintMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextintDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextIntdate, 6).setUnsigned();
	private ZonedDecimalData wsaaNextCapDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaNextCapdate = new FixedLengthStringData(8).isAPartOf(wsaaNextCapDate, 0, REDEFINE);
	private ZonedDecimalData wsaaNextcapYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaNextCapdate, 0).setUnsigned();
	private ZonedDecimalData wsaaNextcapMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextCapdate, 4).setUnsigned();
	private ZonedDecimalData wsaaNextcapDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaNextCapdate, 6).setUnsigned();
	private FixedLengthStringData wsaaLastdayOfMonths = new FixedLengthStringData(24).init("312831303130313130313031");

	private FixedLengthStringData wsaaLastddOfMonths = new FixedLengthStringData(24).isAPartOf(wsaaLastdayOfMonths, 0, REDEFINE);
	private ZonedDecimalData[] wsaaLastdd = ZDArrayPartOfStructure(12, 2, 0, wsaaLastddOfMonths, 0, UNSIGNED_TRUE);
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaDivdAmount = new PackedDecimalData(11, 2).init(0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator eof = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);
	/* TABLES */
	private static final String th501 = "TH501";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t6639 = "T6639";
	/* ERRORS */
	private static final String h134 = "H134";
	private static final String hl17 = "HL17";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrbonTableDAM covrbonIO = new CovrbonTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UstmTableDAM ustmIO = new UstmTableDAM();
	private Varcom varcom = new Varcom();
	private Th501rec th501rec = new Th501rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6639rec t6639rec = new T6639rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Hdivdrec hdivdrec = new Hdivdrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private List<Hpuapf> hpuapfList;
	private Hpuapf hpuapfIO;
	private int hpuapfCount = 0;
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit190
	}

	public Dryh520() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}



	protected void startProcessing100()
	{
		try {
			start110();
			start120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void start110()
	{
		drylogrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		initialise200();
		/* Replacement of SQL by COBOL*/
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readh);
		procChdrlif5100();
		if (isNE(chdrlifIO.getValidflag(), "1")
				|| !(isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat01)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat02)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat03)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat04)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat05)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat06)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat07)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat08)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat09)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat10)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat11)
						|| isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat12))
				|| !(isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat01)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat02)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat03)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat04)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat05)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat06)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat07)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat08)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat09)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat10)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat11)
						|| isEQ(chdrlifIO.getPstatcode(), t5679rec.covPremStat12))) {
			goTo(GotoLabel.exit190);
		}
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		/* Get the coverage .........*/
		covrbonIO.setParams(SPACES);
		covrbonIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrbonIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covrbonIO.setPlanSuffix(ZERO);
		covrbonIO.setFormat(formatsInner.covrbonrec);
		covrbonIO.setFunction("BEGN");
	}

	protected void start120()
	{
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(), varcom.oK)
				&& isNE(covrbonIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrbonIO.getParams());
			drylogrec.statuz.set(covrbonIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(covrbonIO.getStatuz(), varcom.endp)
				|| isNE(covrbonIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(covrbonIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			return ;
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		//drycntrec.setWsaaProg(wsaaProg);
		d000ControlTotals();
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaLastBonusDate.set(datcon2rec.intDate2);
		if (isEQ(covrbonIO.getStatuz(), varcom.oK)
				&& isEQ(covrbonIO.getBonusInd(), "D")
				&& isLTE(covrbonIO.getUnitStatementDate(), wsaaLastBonusDate)
				&& (isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat01)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat02)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat03)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat04)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat05)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat06)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat07)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat08)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat09)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat10)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat11)
						|| isEQ(covrbonIO.getStatcode(), t5679rec.cnRiskStat12))
				&& (isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat01)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat02)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat03)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat04)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat05)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat06)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat07)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat08)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat09)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat10)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat11)
						|| isEQ(covrbonIO.getPstatcode(), t5679rec.covPremStat12))) {
			process3000();
			covrbonIO.setFunction(varcom.nextr);
			start120();
			return ;
		}
		else {
			return ;
		}
		/* UNREACHABLE CODE
		finish1000();
		 */
	}

	protected void initialise200()
	{
		initialise210();
	}

	protected void initialise210()
	{
		getT5679T56451300();
		/*    Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.drypEffectiveDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(drypDryprcRecInner.drypLanguage);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}

	protected void getT5679T56451300()
	{
		getT56791310();
	}

	protected void getT56791310()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5645 to obtain the accounting rules.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(h134);
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

	protected void process3000()
	{
		update3010();
	}

	protected void update3010()
	{
		/*    Calculate new Bonus Date by adding 1 year to the CBUNST*/
		/*    of HDVXPF*/
		datcon2rec.intDate1.set(covrbonIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNewBonusDate.set(datcon2rec.intDate2);
		hcsdIO.setChdrcoy(covrbonIO.getChdrcoy());
		hcsdIO.setChdrnum(covrbonIO.getChdrnum());
		hcsdIO.setLife(covrbonIO.getLife());
		hcsdIO.setCoverage(covrbonIO.getCoverage());
		hcsdIO.setRider(covrbonIO.getRider());
		hcsdIO.setPlanSuffix(covrbonIO.getPlanSuffix());
		hcsdIO.setFunction(varcom.readr);
		readHcsd5120();
		/*    Match for the TH501 item with the Cash Dividend method held*/
		/*    on HCSD and WSAA-NEW-BONUS-DATE from array.*/
		/*	itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), th501)
		|| isNE(itdmIO.getItemitem(), hcsdIO.getZcshdivmth())) {
			itdmIO.setStatuz(varcom.endp);
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemtabl(th501);
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemitem(hcsdIO.getZcshdivmth().toString());
		itempf.setItmfrm(wsaaNewBonusDate.getbigdata());
		itempf.setItmto(wsaaNewBonusDate.getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				th501rec.th501Rec.set(StringUtil.rawToString(it.getGenarea()));	
			}
		}     
		/*    Determined by the dividend allocation indicators in TH501,*/
		/*      TH501-IND-01  =  at due date*/
		/*      TH501-IND-02  =  on receipt of full premium*/
		/*      TH501-IND-03  =  on receipt of full premium + 1 instal.*/
		/*    Check if full premium has to be paid to allocate dividend or*/
		/*    skip process this HDVX record.*/
		if ((isEQ(th501rec.ind02, "X")
				&& isGT(wsaaNewBonusDate, chdrlifIO.getPtdate()))
				|| (isEQ(th501rec.ind03, "X")
						&& isLTE(chdrlifIO.getPtdate(), wsaaNewBonusDate))) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		covrbonIO.setFunction(varcom.readh);
		procCovrbon5110();
		hdisIO.setChdrcoy(covrbonIO.getChdrcoy());
		hdisIO.setChdrnum(covrbonIO.getChdrnum());
		hdisIO.setLife(covrbonIO.getLife());
		hdisIO.setCoverage(covrbonIO.getCoverage());
		hdisIO.setRider(covrbonIO.getRider());
		hdisIO.setPlanSuffix(covrbonIO.getPlanSuffix());
		hdisIO.setFunction(varcom.readr);
		procHdis5130();
		/*    Calculate the Next Interest Allocation Date*/
		if (isEQ(hdisIO.getStatuz(), varcom.oK)) {
			datcon4rec.intDate1.set(hdisIO.getNextIntDate());
			wsaaHdisDate.set(hdisIO.getNextIntDate());
			datcon4rec.frequency.set(th501rec.freqcy02);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdisIO.getIntDueMm());
			datcon4rec.billday.set(wsaaHdisDd);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon4rec.datcon4Rec);
				a000FatalError();
			}
			wsaaNextIntDate.set(datcon4rec.intDate2);
		}
		else {
			datcon2rec.intDate1.set(wsaaNewBonusDate);
			datcon2rec.frequency.set(th501rec.freqcy02);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon2rec.datcon2Rec);
				a000FatalError();
			}
			wsaaNextIntDate.set(datcon2rec.intDate2);
			if (isNE(th501rec.hfixdd02, SPACES)) {
				wsaaNextintDay.set(th501rec.hfixdd02);
			}
			if (isNE(th501rec.hfixmm02, SPACES)) {
				if (isGT(th501rec.hfixmm02, wsaaNextintMth)) {
					wsaaNextintYear.subtract(1);
				}
				wsaaNextintMth.set(th501rec.hfixmm02);
			}
			/*    Ensure this Next Interest Allocation date is valid*/
			datcon1rec.intDate.set(wsaaNextIntDate);
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaNextintDay.set(wsaaLastdd[wsaaNextintMth.toInt()]);
				if (isEQ(wsaaNextintMth, 2)
						&& (setPrecision(wsaaNextintYear, 0)
								&& isEQ((mult((div(wsaaNextintYear, 4)), 4)), wsaaNextintYear))
						&& isNE(wsaaNextintYear, 1900)) {
					wsaaNextintDay.add(1);
				}
			}
		}
		/*    Calculate the Next Interest Capitalisation Date*/
		if (isEQ(hdisIO.getStatuz(), varcom.oK)) {
			datcon4rec.intDate1.set(hdisIO.getNextCapDate());
			wsaaHdisDate.set(hdisIO.getNextCapDate());
			datcon4rec.frequency.set(th501rec.freqcy01);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdisIO.getCapDueMm());
			datcon4rec.billday.set(wsaaHdisDd);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon4rec.datcon4Rec);
				a000FatalError();
			}
			wsaaNextCapDate.set(datcon4rec.intDate2);
		}
		else {
			datcon2rec.intDate1.set(wsaaNewBonusDate);
			datcon2rec.frequency.set(th501rec.freqcy01);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon2rec.datcon2Rec);
				a000FatalError();
			}
			wsaaNextCapDate.set(datcon2rec.intDate2);
			if (isNE(th501rec.hfixdd01, SPACES)) {
				wsaaNextcapDay.set(th501rec.hfixdd01);
			}
			if (isNE(th501rec.hfixmm01, SPACES)) {
				if (isGT(th501rec.hfixmm01, wsaaNextcapMth)) {
					wsaaNextcapYear.subtract(1);
				}
				wsaaNextcapMth.set(th501rec.hfixmm01);
			}
			/*    Ensure this Next Interest Capitalisation date is valid*/
			datcon1rec.intDate.set(wsaaNextCapDate);
			datcon1rec.function.set("CONV");
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz, varcom.oK)) {
				wsaaNextcapDay.set(wsaaLastdd[wsaaNextcapMth.toInt()]);
				if (isEQ(wsaaNextcapMth, 2)
						&& (setPrecision(wsaaNextcapYear, 0)
								&& isEQ((mult((div(wsaaNextcapYear, 4)), 4)), wsaaNextcapYear))
						&& isNE(wsaaNextcapYear, 1900)) {
					wsaaNextcapDay.add(1);
				}
			}
		}
		/*    Find dividend allocation subroutine from T6639.*/
		wsaaBonus.set(hcsdIO.getZcshdivmth());
		wsaaPremStat.set(covrbonIO.getPstatcode());
		/*	itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(covrbonIO.getChdrcoy());
		itdmIO.setItemtabl(t6639);
		itdmIO.setItemitem(wsaaT6639Key);
		itdmIO.setItmfrm(wsaaNewBonusDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrbonIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6639)
		|| isNE(itdmIO.getItemitem(), wsaaT6639Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT6639Key);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(g408);
			a000FatalError();
		}
		else {
			t6639rec.t6639Rec.set(itdmIO.getGenarea());
		}*/
		itempf = new Itempf();
		itempf.setItemcoy(covrbonIO.getChdrcoy().toString());
		itempf.setItemtabl(t6639);
		itempf.setItemitem(wsaaT6639Key.toString());
		itempf.setItempfx("IT");
		itempf.setItmfrm(wsaaNewBonusDate.getbigdata());
		itempf.setItmto(wsaaNewBonusDate.getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				t6639rec.t6639Rec.set(StringUtil.rawToString(it.getGenarea()));	
			}
		}
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg, SPACES)) {
			itdmIO.setStatuz(hl17);
			drylogrec.params.set(itdmIO.getParams());
			a000FatalError();
		}
		/*    Initialise the accumulate field for dividend amount*/
		wsaaDivdAmount.set(0);
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(covrbonIO.getChdrcoy());
		hdivdrec.chdrChdrnum.set(covrbonIO.getChdrnum());
		hdivdrec.lifeLife.set(covrbonIO.getLife());
		hdivdrec.covrCoverage.set(covrbonIO.getCoverage());
		hdivdrec.covrRider.set(covrbonIO.getRider());
		hdivdrec.plnsfx.set(covrbonIO.getPlanSuffix());
		hdivdrec.cntcurr.set(chdrlifIO.getCntcurr());
		hdivdrec.cnttype.set(chdrlifIO.getCnttype());
		hdivdrec.transcd.set(drypDryprcRecInner.drypBatctrcde);
		hdivdrec.premStatus.set(covrbonIO.getPstatcode());
		hdivdrec.crtable.set(covrbonIO.getCrtable());
		hdivdrec.sumin.set(covrbonIO.getSumins());
		hdivdrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		hdivdrec.ptdate.set(chdrlifIO.getPtdate());
		hdivdrec.allocMethod.set(covrbonIO.getBonusInd());
		hdivdrec.mortcls.set(covrbonIO.getMortcls());
		hdivdrec.sex.set(covrbonIO.getSex());
		hdivdrec.issuedAge.set(covrbonIO.getAnbAtCcd());
		hdivdrec.tranno.set(wsaaNewTranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrbonIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(covrbonIO.getCrrcd());
		hdivdrec.cessDate.set(covrbonIO.getRiskCessDate());
		wsaaBillfreq.set(chdrlifIO.getBillfreq());
		compute(hdivdrec.annualisedPrem, 3).setRounded(mult(covrbonIO.getInstprem(), wsaaBillfreq9));
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz, varcom.oK)) {
			drylogrec.params.set(hdivdrec.dividendRec);
			drylogrec.statuz.set(hdivdrec.statuz);
			a000FatalError();
		}
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isNE(hdivdrec.divdAmount, 0)) {
			wsaaDivdAmount.add(hdivdrec.divdAmount);
			hdivIO.setChdrcoy(drypDryprcRecInner.drypCompany);
			hdivIO.setChdrnum(drypDryprcRecInner.drypEntity);
			hdivIO.setLife(covrbonIO.getLife());
			hdivIO.setJlife(covrbonIO.getJlife());
			hdivIO.setCoverage(covrbonIO.getCoverage());
			hdivIO.setRider(covrbonIO.getRider());
			hdivIO.setPlanSuffix(covrbonIO.getPlanSuffix());
			hdivIO.setTranno(wsaaNewTranno);
			hdivIO.setEffdate(wsaaNewBonusDate);
			hdivIO.setDivdAllocDate(drypDryprcRecInner.drypRunDate);
			hdivIO.setDivdIntCapDate(wsaaNextCapDate);
			hdivIO.setCntcurr(chdrlifIO.getCntcurr());
			hdivIO.setDivdAmount(hdivdrec.divdAmount);
			hdivIO.setDivdRate(hdivdrec.divdRate);
			hdivIO.setDivdRtEffdt(hdivdrec.rateDate);
			hdivIO.setBatccoy(drypDryprcRecInner.drypCompany);
			hdivIO.setBatcbrn(drypDryprcRecInner.drypBranch);
			hdivIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
			hdivIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
			hdivIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
			hdivIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
			hdivIO.setDivdType("D");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFunction(varcom.writr);
			writeHdiv5140();
			hdisIO.setChdrcoy(hdivIO.getChdrcoy());
			hdisIO.setChdrnum(hdivIO.getChdrnum());
			hdisIO.setLife(hdivIO.getLife());
			hdisIO.setCoverage(hdivIO.getCoverage());
			hdisIO.setRider(hdivIO.getRider());
			hdisIO.setPlanSuffix(hdivIO.getPlanSuffix());
			hdisIO.setFunction(varcom.readh);
			procHdis5130();
			if (isEQ(hdisIO.getStatuz(), varcom.mrnf)) {
				hdisIO.setNonKey(SPACES);
				hdisIO.setJlife(hdivIO.getJlife());
				hdisIO.setCntcurr(hdivIO.getCntcurr());
				hdisIO.setValidflag("1");
				hdisIO.setTranno(wsaaNewTranno);
				hdisIO.setFirstDivdDate(wsaaNewBonusDate);
				hdisIO.setLastDivdDate(wsaaNewBonusDate);
				hdisIO.setLastIntDate(wsaaNewBonusDate);
				hdisIO.setLastCapDate(wsaaNewBonusDate);
				hdisIO.setNextIntDate(wsaaNextIntDate);
				hdisIO.setNextCapDate(wsaaNextCapDate);
				hdisIO.setBalAtLastDivd(hdivdrec.divdAmount);
				hdisIO.setBalSinceLastCap(hdivdrec.divdAmount);
				hdisIO.setOsInterest(ZERO);
				hdisIO.setDivdStmtNo(ZERO);
				hdisIO.setBalAtStmtDate(ZERO);
				hdisIO.setDivdStmtDate(varcom.vrcmMaxDate);
				/*    Set the due month and day for interest allocation and*/
				/*    capitalisation.  Since the due month and due day must be*/
				/*    compatible, say 06 and 31, then make them 05 and 31 instead.*/
				/*    Note: due month and day on HDIS once written is permanent.*/
				if (isNE(th501rec.hfixdd02, SPACES)) {
					hdisIO.setIntDueDd(th501rec.hfixdd02);
				}
				else {
					hdisIO.setIntDueDd(wsaaNextintDay);
				}
				if (isLTE(hdisIO.getIntDueDd(), wsaaLastdd[wsaaNextintMth.toInt()])
						|| (isEQ(wsaaNextintMth, 2)
								&& isEQ(hdisIO.getIntDueDd(), 29))) {
					hdisIO.setIntDueMm(wsaaNextintMth);
				}
				else {
					wsaaNextintMth.subtract(1);
					hdisIO.setIntDueMm(wsaaNextintMth);
					if (isEQ(wsaaNextintMth, 0)) {
						hdisIO.setIntDueMm("12");
					}
				}
				if (isNE(th501rec.hfixdd01, SPACES)) {
					hdisIO.setCapDueDd(th501rec.hfixdd01);
				}
				else {
					hdisIO.setCapDueDd(wsaaNextcapDay);
				}
				if (isLTE(hdisIO.getCapDueDd(), wsaaLastdd[wsaaNextcapMth.toInt()])
						|| (isEQ(wsaaNextcapMth, 2)
								&& isEQ(hdisIO.getCapDueDd(), 29))) {
					hdisIO.setCapDueMm(wsaaNextcapMth);
				}
				else {
					wsaaNextcapMth.subtract(1);
					hdisIO.setCapDueMm(wsaaNextcapMth);
					if (isEQ(wsaaNextcapMth, 0)) {
						hdisIO.setCapDueMm("12");
					}
				}
				hdisIO.setFunction(varcom.writr);
				procHdis5130();
			}
			else {
				hdisIO.setValidflag("2");
				hdisIO.setFunction(varcom.rewrt);
				procHdis5130();
				hdisIO.setValidflag("1");
				hdisIO.setTranno(wsaaNewTranno);
				hdisIO.setLastDivdDate(wsaaNewBonusDate);
				setPrecision(hdisIO.getBalAtLastDivd(), 2);
				hdisIO.setBalAtLastDivd(add(hdisIO.getBalAtLastDivd(), hdivdrec.divdAmount));
				setPrecision(hdisIO.getBalSinceLastCap(), 2);
				hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), hdivdrec.divdAmount));
				hdisIO.setFunction(varcom.writr);
				procHdis5130();
			}
		}
		/*    Allocate Dividend to Paid-up Additions*/
		/*	hpuaIO.setChdrcoy(hdivdrec.chdrChdrcoy);
		hpuaIO.setChdrnum(hdivdrec.chdrChdrnum);
		hpuaIO.setLife(hdivdrec.lifeLife);
		hpuaIO.setCoverage(hdivdrec.covrCoverage);
		hpuaIO.setRider(hdivdrec.covrRider);
		hpuaIO.setPlanSuffix(hdivdrec.plnsfx);
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFunction(varcom.begn);
		procHpua5150();
	(isEQ(hpuapfIO.getStatuz(), varcom.endp)*/
		Hpuapf hpuaData = new Hpuapf();		
		HpuapfDAO hpuapfDAO = getApplicationContext().getBean("hpuapfDAO",HpuapfDAO.class);		
		hpuaData.setChdrcoy(hdivdrec.chdrChdrcoy.toString());
		hpuaData.setChdrnum(hdivdrec.chdrChdrnum.toString());
		hpuaData.setLife(hdivdrec.lifeLife.toString());
		hpuaData.setCoverage(hdivdrec.covrCoverage.toString());
		hpuaData.setRider(hdivdrec.covrRider.toString());
		hpuaData.setPlanSuffix(hdivdrec.plnsfx.toInt());
		hpuaData.setPuAddNbr(0);
		hpuapfList = hpuapfDAO.selectHpuaRecord(hpuaData);
		if(isEQ(hpuapfList.size(),ZERO))
		{
			wsaaEof.set("Y");
		}
		while ( !(isEQ(wsaaEof, "Y"))) {
			if(hpuapfList.size()>hpuapfCount)
			{
				hpuapfIO=hpuapfList.get(hpuapfCount);
				if( isNE(hpuapfIO.getChdrcoy(), hdivdrec.chdrChdrcoy)
						|| isNE(hpuapfIO.getChdrnum(), hdivdrec.chdrChdrnum)
						|| isNE(hpuapfIO.getLife(), hdivdrec.lifeLife)
						|| isNE(hpuapfIO.getCoverage(), hdivdrec.covrCoverage)
						|| isNE(hpuapfIO.getRider(), hdivdrec.covrRider)
						|| isNE(hpuapfIO.getPlanSuffix(), hdivdrec.plnsfx)) {
					//hpuaIO.setStatuz(varcom.endp);
					wsaaEof.set("Y");
				}
				dividendPaidup3100();
			}
			//break;
		}

		/*    Now write accounting records for the calculated dividend if*/
		/*    it is not zero.*/
		if (isNE(wsaaDivdAmount, 0)) {
			writeAccounting3200();
		}
		/*    Update existing COVR as history and write a new one.*/
		covrbonIO.setValidflag("2");
		covrbonIO.setCurrto(drypDryprcRecInner.drypRunDate);
		covrbonIO.setFunction(varcom.rewrt);
		covrbonIO.setFormat(formatsInner.covrbonrec);
		procCovrbon5110();
		covrbonIO.setValidflag("1");
		covrbonIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		covrbonIO.setCurrto(varcom.vrcmMaxDate);
		covrbonIO.setUnitStatementDate(wsaaNewBonusDate);
		covrbonIO.setTranno(wsaaNewTranno);
		covrbonIO.setFunction(varcom.writr);
		procCovrbon5110();
		/*    Update existing CHDR as history and write a new one.*/
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setFunction(varcom.rewrt);
		procChdrlif5100();
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.writr);
		procChdrlif5100();
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setValidflag("1");
		ptrnIO.setPtrneff(wsaaNewBonusDate);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionTime(varcom.vrcmTimen);
		ptrnIO.setUser(999999);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			a000FatalError();
		}
		agntIO.setParams(SPACES);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(chdrlifIO.getAgntcoy());
		agntIO.setAgntnum(chdrlifIO.getAgntnum());
		agntIO.setFormat(formatsInner.agntrec);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
				&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(agntIO.getStatuz());
			drylogrec.params.set(agntIO.getParams());
			a000FatalError();
		}
		ustmIO.setClntpfx(fsupfxcpy.clnt);
		ustmIO.setClntcoy(agntIO.getClntcoy());
		ustmIO.setClntnum(agntIO.getClntnum());
	}

	protected void dividendPaidup3100()
	{
		dividend3110();
		nextHpua3180();
	}

	protected void dividend3110()
	{
		/*    Skip this HPUA if it is not Dividend participant*/
		if (isNE(hpuapfIO.getDivdParticipant(), "Y")) {
			return ;
		}
		/*    Find dividend allocation subroutine from T6639.*/
		/*    Check if dividend allocation subroutine is defined*/
		if (isEQ(t6639rec.revBonusProg, SPACES)) {
			itdmIO.setStatuz(hl17);
			drylogrec.params.set(itdmIO.getParams());
			a000FatalError();
		}
		/*    Prepare to call cash dividend allocation subroutine*/
		initialize(hdivdrec.dividendRec);
		hdivdrec.chdrChdrcoy.set(covrbonIO.getChdrcoy());
		hdivdrec.chdrChdrnum.set(covrbonIO.getChdrnum());
		hdivdrec.lifeLife.set(covrbonIO.getLife());
		hdivdrec.covrCoverage.set(covrbonIO.getCoverage());
		hdivdrec.covrRider.set(covrbonIO.getRider());
		hdivdrec.plnsfx.set(covrbonIO.getPlanSuffix());
		hdivdrec.cntcurr.set(chdrlifIO.getCntcurr());
		hdivdrec.cnttype.set(chdrlifIO.getCnttype());
		hdivdrec.transcd.set(drypDryprcRecInner.drypBatctrcde);
		hdivdrec.premStatus.set(hpuapfIO.getPstatcode());
		hdivdrec.crtable.set(hpuapfIO.getCrtable());
		hdivdrec.sumin.set(hpuapfIO.getSumin());
		hdivdrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		hdivdrec.ptdate.set(chdrlifIO.getPtdate());
		hdivdrec.allocMethod.set(covrbonIO.getBonusInd());
		hdivdrec.mortcls.set(covrbonIO.getMortcls());
		hdivdrec.sex.set(covrbonIO.getSex());
		hdivdrec.issuedAge.set(hpuapfIO.getAnbAtCcd());
		hdivdrec.tranno.set(wsaaNewTranno);
		hdivdrec.divdCalcMethod.set(hcsdIO.getZcshdivmth());
		hdivdrec.periodFrom.set(covrbonIO.getUnitStatementDate());
		hdivdrec.periodTo.set(wsaaNewBonusDate);
		hdivdrec.crrcd.set(hpuapfIO.getCrrcd());
		hdivdrec.cessDate.set(hpuapfIO.getRiskCessDate());
		hdivdrec.annualisedPrem.set(hpuapfIO.getSingp());
		hdivdrec.divdDuration.set(ZERO);
		hdivdrec.termInForce.set(ZERO);
		hdivdrec.divdAmount.set(ZERO);
		hdivdrec.divdRate.set(ZERO);
		hdivdrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Dividend Paid-up Allocation Subroutine*/
		callProgram(t6639rec.revBonusProg, hdivdrec.dividendRec);
		if (isNE(hdivdrec.statuz, varcom.oK)) {
			drylogrec.params.set(hdivdrec.dividendRec);
			drylogrec.statuz.set(hdivdrec.statuz);
			a000FatalError();
		}
		/*    When the dividend amount from allocation is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history) if exists or create one.*/
		if (isNE(hdivdrec.divdAmount, 0)) {
			wsaaDivdAmount.add(hdivdrec.divdAmount);
			hdivIO.setChdrcoy(covrbonIO.getChdrcoy());
			hdivIO.setChdrnum(covrbonIO.getChdrnum());
			hdivIO.setLife(covrbonIO.getLife());
			hdivIO.setJlife(covrbonIO.getJlife());
			hdivIO.setCoverage(covrbonIO.getCoverage());
			hdivIO.setRider(covrbonIO.getRider());
			hdivIO.setPlanSuffix(covrbonIO.getPlanSuffix());
			hdivIO.setTranno(wsaaNewTranno);
			hdivIO.setEffdate(wsaaNewBonusDate);
			hdivIO.setDivdAllocDate(drypDryprcRecInner.drypRunDate);
			hdivIO.setDivdIntCapDate(wsaaNextCapDate);
			hdivIO.setCntcurr(chdrlifIO.getCntcurr());
			hdivIO.setDivdAmount(hdivdrec.divdAmount);
			hdivIO.setDivdRate(hdivdrec.divdRate);
			hdivIO.setDivdRtEffdt(hdivdrec.rateDate);
			hdivIO.setBatccoy(drypDryprcRecInner.drypCompany);
			hdivIO.setBatcbrn(drypDryprcRecInner.drypBranch);
			hdivIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
			hdivIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
			hdivIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
			hdivIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
			hdivIO.setDivdType("D");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(hpuapfIO.getPuAddNbr());
			hdivIO.setFunction(varcom.writr);
			writeHdiv5140();
			hdisIO.setChdrcoy(hdivIO.getChdrcoy());
			hdisIO.setChdrnum(hdivIO.getChdrnum());
			hdisIO.setLife(hdivIO.getLife());
			hdisIO.setCoverage(hdivIO.getCoverage());
			hdisIO.setRider(hdivIO.getRider());
			hdisIO.setPlanSuffix(hdivIO.getPlanSuffix());
			hdisIO.setFunction(varcom.readh);
			procHdis5130();
			if (isEQ(hdisIO.getTranno(), wsaaNewTranno)) {
				setPrecision(hdisIO.getBalAtLastDivd(), 2);
				hdisIO.setBalAtLastDivd(add(hdisIO.getBalAtLastDivd(), hdivdrec.divdAmount));
				setPrecision(hdisIO.getBalSinceLastCap(), 2);
				hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), hdivdrec.divdAmount));
				hdisIO.setFunction(varcom.rewrt);
				procHdis5130();
			}
			else {
				hdisIO.setValidflag("2");
				hdisIO.setFunction(varcom.rewrt);
				procHdis5130();
				hdisIO.setValidflag("1");
				hdisIO.setTranno(wsaaNewTranno);
				hdisIO.setLastDivdDate(wsaaNewBonusDate);
				setPrecision(hdisIO.getBalAtLastDivd(), 2);
				hdisIO.setBalAtLastDivd(add(hdisIO.getBalAtLastDivd(), hdivdrec.divdAmount));
				setPrecision(hdisIO.getBalSinceLastCap(), 2);
				hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), hdivdrec.divdAmount));
				hdisIO.setFunction(varcom.writr);
				procHdis5130();
			}
		}
	}

	protected void nextHpua3180()
	{
		/*hpuaIO.setFunction(varcom.nextr);
		procHpua5150();
		if (isEQ(hpuaIO.getStatuz(), varcom.endp)
		|| isNE(hpuaIO.getChdrcoy(), hdivdrec.chdrChdrcoy)
		|| isNE(hpuaIO.getChdrnum(), hdivdrec.chdrChdrnum)
		|| isNE(hpuaIO.getLife(), hdivdrec.lifeLife)
		|| isNE(hpuaIO.getCoverage(), hdivdrec.covrCoverage)
		|| isNE(hpuaIO.getRider(), hdivdrec.covrRider)
		|| isNE(hpuaIO.getPlanSuffix(), hdivdrec.plnsfx)) {
			hpuaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
		hpuapfCount++ ;
		if((hpuapfList.size() <= hpuapfCount)
				|| isNE(hpuapfIO.getChdrcoy(), hdivdrec.chdrChdrcoy)
				|| isNE(hpuapfIO.getChdrnum(), hdivdrec.chdrChdrnum)
				|| isNE(hpuapfIO.getLife(), hdivdrec.lifeLife)
				|| isNE(hpuapfIO.getCoverage(), hdivdrec.covrCoverage)
				|| isNE(hpuapfIO.getRider(), hdivdrec.covrRider)
				|| isNE(hpuapfIO.getPlanSuffix(), hdivdrec.plnsfx)) {
			wsaaEof.set("Y");
		}
	}

	protected void writeAccounting3200()
	{
		accounting3210();
	}

	protected void accounting3210()
	{
		/*    Determine contract or component level accounting in T5688*/
		/*	itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(hcsdIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(e308);
			a000FatalError();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemtabl(t5688);
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemitem(chdrlifIO.getCnttype().toString());
		itempf.setItmfrm(hcsdIO.getEffdate().getbigdata());
		itempf.setItmto(hcsdIO.getEffdate().getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				t5688rec.t5688Rec.set(StringUtil.rawToString(it.getGenarea()));	
				wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
			}
		} 
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaDivdAmount);
		/*    Post against expenses.*/
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(covrbonIO.getCrtable());
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(covrbonIO.getPlanSuffix());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(covrbonIO.getChdrnum());
			stringVariable1.addExpression(covrbonIO.getLife());
			stringVariable1.addExpression(covrbonIO.getCoverage());
			stringVariable1.addExpression(covrbonIO.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			x1000CallLifacmv();
		}
		/*    Post opposite entry in payable.*/
		wsaaJrnseq.add(1);
		t5645Ix.set(2);
		lifacmvrec.substituteCode[6].set(SPACES);
		lifacmvrec.rldgacct.set(covrbonIO.getChdrnum());
		x1000CallLifacmv();
		/*    Count no.of COVR   records processed in CT03 and total*/
		/*    dividend amount in CT04*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct04);
		drycntrec.contotValue.set(wsaaDivdAmount);
		d000ControlTotals();
	}

	protected void procChdrlif5100()
	{
		/*CHDRLIF*/
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void procCovrbon5110()
	{
		/*READH*/
		covrbonIO.setFormat(formatsInner.covrbonrec);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrbonIO.getParams());
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void readHcsd5120()
	{
		/*HCSD*/
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(hcsdIO.getParams());
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void procHdis5130()
	{
		/*HDIS*/
		hdisIO.setFormat(formatsInner.hdisrec);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)
				&& isNE(hdisIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(hdisIO.getParams());
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void writeHdiv5140()
	{
		/*HDIV*/
		hdivIO.setFormat(formatsInner.hdivrec);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(hdivIO.getParams());
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void procHpua5150()
	{
		/*HPUA*/
		/*hpuaIO.setFormat(formatsInner.hpuarec);
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), varcom.oK)
		&& isNE(hpuaIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(hpuaIO.getParams());
			a000FatalError();
		}*/
		/*EXIT*/
	}

	protected void x1000CallLifacmv()
	{
		x1010Call();
	}

	protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTD");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.sacscode.set(t5645rec.sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.tranref.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.tranno.set(wsaaNewTranno);
		lifacmvrec.effdate.set(wsaaNewBonusDate);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		//lifacmvrec.threadNumber.set(drypDryprcRecInner.drypThreadNumber);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			a000FatalError();
		}
	}

	protected void finish1000()
	{
		finishPara1000();
	}

	protected void finishPara1000()
	{
		/* Update the DRYP fields to determine next processing date.*/
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		/* FORMATS */
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
		private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
		private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
		private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
		private FixedLengthStringData covrbonrec = new FixedLengthStringData(10).init("COVRBONREC");
		private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
		private FixedLengthStringData agntrec = new FixedLengthStringData(10).init("AGNTREC");
	}
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner { 

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);


		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);

		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);

		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);

		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);

		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		//cluster support by vhukumagrawa
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}
}
