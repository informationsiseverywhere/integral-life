/*
 * File: Dry5365.java
 * Date: January 15, 2015 4:14:25 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5365.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.diary.dataaccess.RegpdteTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.dataaccess.ChdrpayTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegpenqTableDAM;
import com.csc.life.terminationclaims.recordstructures.Regpsubrec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*              REGULAR PAYMENTS PROCESSING
*              ~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Overview
*  ~~~~~~~~
*  This program will process the Regular Payment per Contract
*  that is due to make its next payment.
*  A consideration is that the job should be run before any
*  collection so that any Waiver of Premium claims are able
*  to post their monies to the appropriate contract suspense
*  account ready for the collection run to pick up.
*
*  It will select the payment records where the Next Payment
*  Date is less than or equal to the Effective Date of the run.
*
*  Each payment will be further screened by checking its
*  Payment Status against the allowable codes on table T6693.
*  Another screening will then be made by checking the risk
*  and premium status codes of the contract and associated
*  component against the allowable codes on table T5679.
*
*  Regular Payments that are past their Review Dates or Final
*  Payment Dates will not be processed.
*
*  The actual processing of the payment will be carried out by
*  generic subroutines.  These will be obtained from T5671 and
*  there may be up to four subroutines for each component.  They
*  will be called in succession and will perform all of the
*  detail processing.
*
*  The main program will then advance the Next Payment Date by
*  one frequency.  The payment will be processed again if the
*  Next Payment Date is still within the Effective Date of the
*  run.
*
*  If the Next Payment Date is advanced beyond the Final
*  Payment Date, which will happen if the Final Payment Date
*  does not fall on a payment anniversary, then the Next
*  Payment Date will be set to the Final Payment Date.
*
*  Any Anniversary processing will be performed by Anniversary
*  Processing subroutines and will be called from within the
*  generic subroutines themselves.
*
*  The following control totals are maintained within the
*  program:
*
*       CT01 - Number of regular payment records extracted
*       CT02 - Number of records processed
*       CT03 - Number of records rejected
*       CT04 - Payments with ineligible status
*       CT05 - Contract with ineligible status
*       CT06 - Components with ineligible status
*       CT07 - Number of payments in review
*       CT08 - Number of payments Terminating
*
*   Tables used are :
*
*    T5671 - Generic Program Switching
*            Key: Transaction Code || CRTABLE
*    T5679 - Transaction Codes For Status
*            Key: Transaction Code
*    T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*    T6762 - Regular payments exception codes
*            Key: Program name
*
*    N.B. CRTABLE may be set to '****'.
*
*  Process REGP
*  ~~~~~~~~~~~~~
*   Read the REGPDTE records for the selected contract
*   sequentially incrementing control total CT01.
*   Read and validate the current transaction code held on T6693
*   against those on the extra data screen.
*   Read and validate the transaction code against those held
*   on T5679.
*   Read and validate the CHDR Risk Status and Premium Status
*   against those held on T5679.
*   Read and validate the COVR Risk Status and Premium Status
*   against those held on T5679.
*   If the Review Date is greater than or equal to the Next
*   Payment Date process next record.
*
*  UPDATE
*  ~~~~~~~
*   Increment control total CT02.
*   Read and hold the regular payments record using logical
*   file REGPDTE.
*   Read table T5671 with the Transaction Code and Component
*   Code to find any generic subroutines for the contract.
*   Set the REGP Validflag to '2' and perform a REWRT on the
*   REGP file.  Set it back to '1' and perform a KEEPS on REGP.
*   Check each of the four subroutine entries on T5671 in turn.
*   For each one that has an entry, call it using the copybook
*   REGPSUBREC.  On return from each subroutine check the return
*   status.  If this is not O-K then pass it to the fatal error
*   routine along with the rest of the linkage area and perform
*   fatal error processing.
*
*   When all of the subroutines have been successfully called
*   read CHDR with READH, rewrite the CHDR with a VALIDFLAG of
*   '2'.  Increment TRANNO by 1, set the Validflag back to '1'
*   and write the new record.
*   Write a PTRN record with the new incremented TRANNO.
*   Perform a RETRV on REGP.  Set the Last Paid Date to the Next
*   Payment Date and advance the Next Payment Date by one
*   Regular Payment Frequency.
*
*   If the new Next Payment Date is greater than the Final
*   Payment Date then set the Next Payment Date to the Final
*   Payment Date.
*   Place the new TRANNO on REGP and perform a WRITR on REGP.
*   If the Next Payment Date is still less than or equal to the
*   Effective Date then process the Regular Payment again.
*
*   Read the next REGP record.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5365 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5365");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaProcessContract = new FixedLengthStringData(1).init("Y");
	private Validator processContract = new Validator(wsaaProcessContract, "Y");

	private FixedLengthStringData wsaaNotProcessPayment = new FixedLengthStringData(1).init("N");
	private Validator notProcessPayment = new Validator(wsaaNotProcessPayment, "Y");

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 6, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5671Key1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Transcd1 = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key1, 0);
	private FixedLengthStringData wsaaT5671Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key1, 4);

	private FixedLengthStringData wsaaItemFound = new FixedLengthStringData(1).init("N");
	private Validator itemFound = new Validator(wsaaItemFound, "Y");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");

	private FixedLengthStringData wsaaValidTrcode = new FixedLengthStringData(1).init("N");
	private Validator validTrcode = new Validator(wsaaValidTrcode, "Y");
		/* WSAA-MISCELLANEOUS */
	private String wsaaStopPayment = "";
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private String wsaaPtrnWritten = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSubprogIx = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String regprec = "REGPREC";
	private static final String regpenqrec = "REGPENQREC";
	private static final String regpdterec = "REGPDTEREC";
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String covrrec = "COVRREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String payrrec = "PAYRREC";
	private static final String chdrpayrec = "CHDRPAYREC";
		/* TABLES */
	private static final String t5671 = "T5671";
	private static final String t5679 = "T5679";
	private static final String t6693 = "T6693";
	private static final String t6762 = "T6762";
	private static final String t6689 = "T6689";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private ChdrpayTableDAM chdrpayIO = new ChdrpayTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RegpTableDAM regpIO = new RegpTableDAM();
	private RegpdteTableDAM regpdteIO = new RegpdteTableDAM();
	private RegpenqTableDAM regpenqIO = new RegpenqTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Regpsubrec regpsubrec = new Regpsubrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6762rec t6762rec = new T6762rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		notFound2150, 
		exit2190
	}

	public Dry5365() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		initialise200();
		validateContract300();
		readRegp400();
		finish4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaValidContract.set("N");
		wsaaNotProcessPayment.set("N");
		wsaaItemFound.set("N");
		wsaaValidStatus.set("N");
		wsaaValidTrcode.set("N");
		wsaaProcessContract.set("Y");
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrrgpIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void validateContract300()
	{
		validate310();
	}

protected void validate310()
	{
		/* Read T5679 for valid status requirements for transactions.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Validate the new contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrrgpIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrrgpIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

protected void readRegp400()
	{
		/*READ*/
		regpdteIO.setParams(SPACES);
		regpdteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		regpdteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		regpdteIO.setNextPaydate(ZERO);
		regpdteIO.setFormat(regpdterec);
		regpdteIO.setFunction(varcom.begn);
		while ( !(isEQ(regpdteIO.getStatuz(), varcom.endp))) {
			processRegp500();
		}
		
		/*EXIT*/
	}

protected void processRegp500()
	{
		process510();
	}

protected void process510()
	{
		SmartFileCode.execute(appVars, regpdteIO);
		if (isNE(regpdteIO.getStatuz(), varcom.oK)
		&& isNE(regpdteIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(regpdteIO.getStatuz());
			drylogrec.params.set(regpdteIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, regpdteIO.getChdrcoy())
		|| isNE(drypDryprcRecInner.drypEntity, regpdteIO.getChdrnum())
		|| isEQ(regpdteIO.getStatuz(), varcom.endp)
		|| isGT(regpdteIO.getNextPaydate(), drypDryprcRecInner.drypRunDate)) {
			regpdteIO.setStatuz(varcom.endp);
			return ;
		}
		/* It is necessary to use REGP Logical as in B5365.*/
		regpIO.setDataArea(SPACES);
		regpIO.setChdrcoy(regpdteIO.getChdrcoy());
		regpIO.setChdrnum(regpdteIO.getChdrnum());
		regpIO.setLife(regpdteIO.getLife());
		regpIO.setCoverage(regpdteIO.getCoverage());
		regpIO.setRider(regpdteIO.getRider());
		regpIO.setRgpynum(regpdteIO.getRgpynum());
		regpIO.setFunction(varcom.readr);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(regpIO.getStatuz());
			drylogrec.params.set(regpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		readTables1000();
		if (!validContract.isTrue()) {
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			dryrDryrptRecInner.r537001Excode.set(t6762rec.regpayExcpIchs);
			dryrDryrptRecInner.r537001Exreport.set("Y");
			reportProcess5000();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			regpdteIO.setStatuz(varcom.endp);
			return ;
		}
		processRegp2000();
		if (!notProcessPayment.isTrue()) {
			updateRegp3000();
		}
		regpdteIO.setFunction(varcom.nextr);
	}

protected void readTables1000()
	{
		start1000();
	}

protected void start1000()
	{
		/* Call routine to get processing date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* set up the TRANID.*/
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(999999);
		varcom.vrcmTermid.set("9999");
		/* Read T6762 to get valid exception codes.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6762);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6762rec.t6762Rec.set(itemIO.getGenarea());
		/* Read T5671 for coverage switching details*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		wsaaT5671Transcd1.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT5671Crtable1.set(regpIO.getCrtable());
		itemIO.setItemitem(wsaaT5671Key1);
		itemIO.setItemtabl(t5671);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.params.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}

protected void processRegp2000()
	{
		/*START*/
		readT66932100();
		if (notProcessPayment.isTrue()) {
			return ;
		}
		else {
			readChdrCovr2200();
		}
		/*EXIT*/
	}

protected void readT66932100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2100();
				case notFound2150: 
					notFound2150();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		/* Check through T6693 to find allowable actions from current*/
		/* status.*/
		/* If actions from current status found read through array of*/
		/* allowable transactions to determine if current transaction*/
		/* valid.*/
		wsaaT6693Paystat1.set(regpIO.getRgpystat());
		wsaaT6693Crtable1.set(regpIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemitem(wsaaT6693Key1);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
		|| isNE(t6693, itdmIO.getItemtabl())
		|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6693Crtable1.set("****");
			itdmIO.setItemitem(wsaaT6693Key1);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
			|| isNE(t6693, itdmIO.getItemtabl())
			|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				goTo(GotoLabel.notFound2150);
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		wsaaValidTrcode.set("N");
		for (index1.set(1); !(isGT(index1, 12)); index1.add(1)){
			if (isEQ(t6693rec.trcode[index1.toInt()], drypDryprcRecInner.drypBatctrcde)) {
				validTrcode.setTrue();
				index1.set(15);
			}
		}
		if (validTrcode.isTrue()) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void notFound2150()
	{
		notProcessPayment.setTrue();
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct04);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drypDryprcRecInner.processUnsuccesful.setTrue();
		dryrDryrptRecInner.r537001Excode.set(t6762rec.regpayExcpIpst);
		dryrDryrptRecInner.r537001Exreport.set("Y");
		reportProcess5000();
	}

protected void readChdrCovr2200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Read the coverage record and validate the status against*/
		/* those on T5679.*/
		/* Read in the relevant COVR record.*/
		covrIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrIO.setChdrnum(regpIO.getChdrnum());
		covrIO.setLife(regpIO.getLife());
		covrIO.setCoverage(regpIO.getCoverage());
		covrIO.setRider(regpIO.getRider());
		covrIO.setPlanSuffix(regpIO.getPlanSuffix());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrIO.getParams());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Validate the coverage statii against T5679.*/
		wsaaValidStatus.set("N");
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateCovrStatus2300();
		}
		if (!validStatus.isTrue()) {
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drycntrec.contotNumber.set(ct06);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			dryrDryrptRecInner.r537001Excode.set(t6762rec.regpayExcpIcos);
			dryrDryrptRecInner.r537001Exreport.set("Y");
			reportProcess5000();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			notProcessPayment.setTrue();
		}
	}

protected void validateCovrStatus2300()
	{
		/*START*/
		/* Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateCovrPremStatus2400();
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus2400()
	{
		/*START*/
		/* Validate coverage premium status*/
		if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
			wsaaSub.set(13);
			validStatus.setTrue();
		}
		/*EXIT*/
	}

protected void updateRegp3000()
	{
		start3010();
	}

protected void start3010()
	{
		/* If the situation arises of the Next Payment Date being equal*/
		/* or being set equal, to the Final Payment Date while still*/
		/* being less than or equal to the Effective Date then a loop*/
		/* will occur. To avoid this the indicator WSAA-STOP-PAYMENT is*/
		/* set to various values depending on whether it is the first*/
		/* time (N), the second or last time (L) or processing complete*/
		/* (Y).  Processing proceeds on this basis.*/
		wsaaStopPayment = "N";
		wsaaPtrnWritten = "N";
		if (isEQ(regpIO.getNextPaydate(), regpIO.getFinalPaydate())) {
			wsaaStopPayment = "L";
		}
		while ( !((isGT(regpIO.getNextPaydate(), regpIO.getRevdte()))
		|| (isGT(regpIO.getNextPaydate(), drypDryprcRecInner.drypRunDate))
		|| (isEQ(wsaaStopPayment, "Y")))) {
			updatePayments3100();
		}
		
		/* Increment no of records processed*/
		/* note: this is at record level not instalment*/
		if (isEQ(regpsubrec.subStatuz, varcom.oK)) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}
		/* Check if the record is going into review and still eligible*/
		/* to be processed.*/
		if (isGTE(regpIO.getNextPaydate(), regpIO.getRevdte())
		&& isLTE(regpIO.getNextPaydate(), drypDryprcRecInner.drypRunDate)) {
			drycntrec.contotNumber.set(ct07);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}
		/* Check if the record is going to be terminated and is still*/
		/* eligible to be processed.*/
		if (isGTE(regpIO.getNextPaydate(), regpIO.getFinalPaydate())) {
			drycntrec.contotNumber.set(ct08);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}
		if (isEQ(wsaaPtrnWritten, "Y")) {
			statistics6000();
		}
	}

protected void updatePayments3100()
	{
		/*START*/
		/* Perform update payments processing.*/
		updateRegpValidflag3300();
		genericProcessing3400();
		if (isEQ(wsaaStopPayment, "Y")) {
			return ;
		}
		rewriteChdr3600();
		rewritePayr3200();
		writePtrn3700();
		updateRegp3800();
		/*EXIT*/
	}

protected void rewritePayr3200()
	{
		start3210();
	}

protected void start3210()
	{
		/* Obtain the PAYR record*/
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(payrIO.getChdrnum(), chdrrgpIO.getChdrnum())
		|| isNE(payrIO.getChdrcoy(), chdrrgpIO.getChdrcoy())
		|| isNE(payrIO.getValidflag(), "1")
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write validflag '2' Payer (PAYR) record*/
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.writd);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		payrIO.setTranno(chdrpayIO.getTranno());
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void updateRegpValidflag3300()
	{
		/*START*/
		regpIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpIO.getParams());
			drylogrec.statuz.set(regpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void genericProcessing3400()
	{
		/*START*/
		/* Perform section to call generic subroutine(s) if present.*/
		for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx, 4)
		|| isEQ(wsaaStopPayment, "Y")); wsaaSubprogIx.add(1)){
			if (isNE(t5671rec.subprog[wsaaSubprogIx.toInt()], SPACES)) {
				callGeneric3500();
			}
		}
		/*EXIT*/
	}

protected void callGeneric3500()
	{
		start3510();
	}

protected void start3510()
	{
		/* Call generic subroutine.*/
		regpsubrec.subStatuz.set(varcom.oK);
		regpsubrec.subEffdate.set(drypDryprcRecInner.drypRunDate);
		regpsubrec.subCompany.set(drypDryprcRecInner.drypCompany);
		regpsubrec.subLanguage.set(drypDryprcRecInner.drypLanguage);
		regpsubrec.subBatcpfx.set(drypDryprcRecInner.drypBatcpfx);
		regpsubrec.subBatctrcde.set(drypDryprcRecInner.drypBatctrcde);
		regpsubrec.subBatcactyr.set(drypDryprcRecInner.drypBatcactyr);
		regpsubrec.subBatcactmn.set(drypDryprcRecInner.drypBatcactmn);
		regpsubrec.subBatcbatch.set(drypDryprcRecInner.drypBatcbatch);
		regpsubrec.subBatcbrn.set(drypDryprcRecInner.drypBatcbrn);
		regpsubrec.subBatccoy.set(drypDryprcRecInner.drypBatccoy);
		regpsubrec.subCnttype.set(chdrrgpIO.getCnttype());
		regpsubrec.subTranid.set(ZERO);
		regpsubrec.subTermid.set(varcom.vrcmTranid);
		regpsubrec.subUser.set(regpIO.getUser());
		callProgram(t5671rec.subprog[wsaaSubprogIx.toInt()], regpsubrec.batcsubRec);
		if (isNE(regpsubrec.subStatuz, varcom.oK)) {
			checkReason3900();
			if (!itemFound.isTrue()) {
				drylogrec.params.set(regpsubrec.batcsubRec);
				drylogrec.statuz.set(regpsubrec.subStatuz);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			else {
				drycntrec.contotNumber.set(ct04);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				drycntrec.contotNumber.set(ct03);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				wsaaStopPayment = "Y";
				dryrDryrptRecInner.r537001Excode.set(regpsubrec.subStatuz);
				dryrDryrptRecInner.r537001Exreport.set("Y");
				reportProcess5000();
				drypDryprcRecInner.processUnsuccesful.setTrue();
			}
		}
	}

protected void rewriteChdr3600()
	{
		start3610();
	}

protected void start3610()
	{
		/* Obtain the CHDR record using CHDRPAY logical.*/
		chdrpayIO.setParams(SPACES);
		chdrpayIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrpayIO.setChdrnum(chdrrgpIO.getChdrnum());
		chdrpayIO.setFunction(varcom.readh);
		chdrpayIO.setFormat(chdrpayrec);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write validflag '2' Contract Header (CHDR) record*/
		chdrpayIO.setValidflag("2");
		chdrpayIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrpayIO.setFunction(varcom.rewrt);
		chdrpayIO.setFormat(chdrpayrec);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		setPrecision(chdrpayIO.getTranno(), 0);
		chdrpayIO.setTranno(add(chdrpayIO.getTranno(), 1));
		chdrpayIO.setValidflag("1");
		chdrpayIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrpayIO.setCurrto(varcom.vrcmMaxDate);
		chdrpayIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writePtrn3700()
	{
		start3710();
	}

protected void start3710()
	{
		/* Create a PTRN record*/
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setTranno(chdrpayIO.getTranno());
		ptrnIO.setPtrneff(regpIO.getNextPaydate());
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrpayIO.getChdrcoy());
		ptrnIO.setChdrnum(regpIO.getChdrnum());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaPtrnWritten = "Y";
	}

protected void updateRegp3800()
	{
		start3810();
	}

protected void start3810()
	{
		/* readh REGP rec, using a different logical*/
		readhRegp3850();
		/* Update the REGP record with valid flag of '2'*/
		regpenqIO.setValidflag("2");
		regpenqIO.setFunction(varcom.rewrt);
		regpenqIO.setFormat(regpenqrec);
		SmartFileCode.execute(appVars, regpenqIO);
		if (isNE(regpenqIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpenqIO.getParams());
			drylogrec.statuz.set(regpenqIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Retrieve the REGP record.*/
		regpIO.setFunction(varcom.retrv);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpIO.getParams());
			drylogrec.statuz.set(regpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Release the REGP record.*/
		regpIO.setFunction(varcom.rlse);
		regpIO.setFormat(regprec);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpIO.getParams());
			drylogrec.statuz.set(regpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Update the REGP date fields.*/
		/* Call DATCON2 to calculate the Next Payment Date.*/
		/* Check if the Final Payment Date has been reached.*/
		regpIO.setLastPaydate(regpIO.getNextPaydate());
		datcon2rec.frequency.set(regpIO.getRegpayfreq());
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(regpIO.getNextPaydate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		regpIO.setNextPaydate(datcon2rec.intDate2);
		if (isGTE(regpIO.getNextPaydate(), regpIO.getFinalPaydate())) {
			regpIO.setNextPaydate(regpIO.getFinalPaydate());
			if (isEQ(wsaaStopPayment, "L")) {
				wsaaStopPayment = "Y";
				drypDryprcRecInner.processUnsuccesful.setTrue();
			}
			if (isEQ(wsaaStopPayment, "N")) {
				wsaaStopPayment = "L";
			}
		}
		/* Update REGP with the TRANNO with the new*/
		/* value from the contract.*/
		regpIO.setTranno(chdrpayIO.getTranno());
		regpIO.setFormat(regprec);
		regpIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, regpIO);
		if (isNE(regpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpIO.getParams());
			drylogrec.statuz.set(regpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* write a non-exception record to the Regular Payment copybook*/
		dryrDryrptRecInner.r537001Excode.set(SPACES);
		dryrDryrptRecInner.r537001Exreport.set(SPACES);
		reportProcess5000();
	}

protected void readhRegp3850()
	{
		regpRead3850();
	}

protected void regpRead3850()
	{
		/*  Read and hold the REGP record, using a different logical*/
		/*  as not to interfere with the keeps & retrv processing.*/
		regpenqIO.setDataArea(SPACES);
		regpenqIO.setChdrcoy(regpIO.getChdrcoy());
		regpenqIO.setChdrnum(regpIO.getChdrnum());
		regpenqIO.setLife(regpIO.getLife());
		regpenqIO.setCoverage(regpIO.getCoverage());
		regpenqIO.setRider(regpIO.getRider());
		regpenqIO.setRgpynum(regpIO.getRgpynum());
		regpenqIO.setFunction(varcom.readh);
		regpenqIO.setFormat(regpenqrec);
		SmartFileCode.execute(appVars, regpenqIO);
		if (isNE(regpenqIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regpenqIO.getParams());
			drylogrec.statuz.set(regpenqIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void checkReason3900()
	{
		readTable3910();
	}

protected void readTable3910()
	{
		/* Read T6689 for reason codes*/
		wsaaItemFound.set("N");
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6689);
		itemIO.setItemitem(regpsubrec.subStatuz);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			itemFound.setTrue();
		}
	}

protected void finish4000()
	{
		/*FINISH*/
		/** No processing required as yet.*/
		/*EXIT*/
	}

protected void reportProcess5000()
	{
		start5010();
	}

protected void start5010()
	{
		/* Write detail record to regular payment copybook*/
		/* This will be printed out in the Regular Payments*/
		/* processing program DRY537001. Note that the records*/
		/* may be exception or non-exception records.*/
		dryrDryrptRecInner.r537001Chdrnum.set(regpIO.getChdrnum());
		dryrDryrptRecInner.r537001Life.set(regpIO.getLife());
		dryrDryrptRecInner.r537001Coverage.set(regpIO.getCoverage());
		dryrDryrptRecInner.r537001Rider.set(regpIO.getRider());
		dryrDryrptRecInner.r537001Rgpynum.set(regpIO.getRgpynum());
		dryrDryrptRecInner.r537001Rgpytype.set(regpIO.getRgpytype());
		dryrDryrptRecInner.r537001Rgpystat.set(regpIO.getRgpystat());
		dryrDryrptRecInner.r537001Crtable.set(regpIO.getCrtable());
		dryrDryrptRecInner.r537001Tranno.set(regpIO.getTranno());
		dryrDryrptRecInner.r537001Pymt.set(regpIO.getPymt());
		dryrDryrptRecInner.r537001Currcd.set(regpIO.getCurrcd());
		dryrDryrptRecInner.r537001Prcnt.set(regpIO.getPrcnt());
		dryrDryrptRecInner.r537001Payreason.set(regpIO.getPayreason());
		dryrDryrptRecInner.r537001Revdte.set(regpIO.getRevdte());
		dryrDryrptRecInner.r537001FirstPaydate.set(regpIO.getFirstPaydate());
		dryrDryrptRecInner.r537001LastPaydate.set(regpIO.getLastPaydate());
		/* Set up the report name.*/
		/* Move up the sort key.*/
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void statistics6000()
	{
		start6010();
	}

protected void start6010()
	{
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.chdrcoy.set(chdrrgpIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrrgpIO.getChdrnum());
		lifsttrrec.tranno.set(chdrpayIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r537001DataArea = new FixedLengthStringData(493).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r537001Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537001DataArea, 0);
	private ZonedDecimalData r537001Pymt = new ZonedDecimalData(17, 2).isAPartOf(r537001DataArea, 5);
	private FixedLengthStringData r537001Currcd = new FixedLengthStringData(3).isAPartOf(r537001DataArea, 22);
	private ZonedDecimalData r537001Prcnt = new ZonedDecimalData(7, 2).isAPartOf(r537001DataArea, 25);
	private FixedLengthStringData r537001Payreason = new FixedLengthStringData(2).isAPartOf(r537001DataArea, 32);
	private FixedLengthStringData r537001Rgpystat = new FixedLengthStringData(2).isAPartOf(r537001DataArea, 34);
	private FixedLengthStringData r537001Excode = new FixedLengthStringData(4).isAPartOf(r537001DataArea, 149);
	private ZonedDecimalData r537001FirstPaydate = new ZonedDecimalData(8, 0).isAPartOf(r537001DataArea, 153);
	private ZonedDecimalData r537001LastPaydate = new ZonedDecimalData(8, 0).isAPartOf(r537001DataArea, 161);
	private ZonedDecimalData r537001Revdte = new ZonedDecimalData(8, 0).isAPartOf(r537001DataArea, 169);
	private ZonedDecimalData r537001Tranno = new ZonedDecimalData(5, 0).isAPartOf(r537001DataArea, 177);

	private FixedLengthStringData r537001SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r537001Exreport = new FixedLengthStringData(1).isAPartOf(r537001SortKey, 0);
	private FixedLengthStringData r537001Chdrnum = new FixedLengthStringData(8).isAPartOf(r537001SortKey, 1);
	private FixedLengthStringData r537001Rgpytype = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 9);
	private FixedLengthStringData r537001Crtable = new FixedLengthStringData(4).isAPartOf(r537001SortKey, 11);
	private FixedLengthStringData r537001Life = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 15);
	private FixedLengthStringData r537001Coverage = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 17);
	private FixedLengthStringData r537001Rider = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 19);
	private FixedLengthStringData r537001ReportName = new FixedLengthStringData(10).init("R537001  ");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
}
