/*
 * File: Dry5137.java
 * Date: March 26, 2014 2:59:48 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5137.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PcdtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.procedures.Acomcalc;
import com.csc.life.contractservicing.recordstructures.Acomcalrec;
import com.csc.life.diary.dataaccess.dao.Dry5137DAO;
import com.csc.life.diary.dataaccess.model.Dry5137Dto;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.procedures.Rasaltr;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.reassurance.recordstructures.Rasaltrrec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*         ACTUAL AUTOMATIC INCREASE
*         =========================
*
* This program comprises part of the Diary Batch System. It
* has been substantially restructured for this system.
*
* Process all 'Increase Pending' records for the specified
* contract on the actual increase date. Update the contract
* records with the calculated new amounts by writing new
* CHDR, COVR, and PAYR details and rewriting the old details
* with a valid flag of '2'.
*
* The records to be processed are read from INCR and, for each
* INCR found, read the relevant COVR (valid flag '1') and
* rewrite this with a valid flag '2' as well as updating
* the CURRTO date.
*
* Read T6658 using INCR-ANNVRY to decide whether a new
* component should be added or a change made to the existing
* component.
*
* COMMISSION
* ==========
* Commission can be affected either way, i.e., if the
* indicator on T6658 for 'Commission' is non-spaces.
*
* Note: The T5687 Basic Calculation Method used for
*       Initial Commission is used for the increase.
*       This is included on the INCR record.
*
* Subroutine ACOMCALC will be called if commission is to
* be calculated. The effective date of the appropriate
* AGCM records is the 'increase date'.
*
* COVR
* ====
* - Move/Add-to the relevant new premium on the COVR.
* - Accumulate a working field that holds the premium amounts
*   for writing new PAYR and CHDR records.
*
* - If Modify Component then COVR-CPI-DATE needs to be
*   recalculated based on the T6658 frequency.
*   However:-
*     IF
*       - the calculated term to Risk Cessation is less
*         than T6658 Minimum Term to Cessation
*     OR
*       - the calculated age at Effective Date exceeds
*         T6658 Maximum Age
*     OR
*       - Fixed Number of Increases has been exceeded
*     THEN
*       - COVR-CPI-DATE is set to MAXDATE.
* - Valid CURRFROM/CURRTO dates are maintained on the
*   creation of:
*       - Validflag '2' of the old record;
*       - Validflag '1' for the new date with the
*                       Indexation Indicator set to spaces
*                       and the installment premium and
*                       sum assured update.
*
* - If Add Component:
* - Valid CURRFROM/CURRTO dates are maintained on the
*   creation of:
*       - Validflag '2' of the old record;
*       - Validflag '1' for the new date with the
*                       Indexation Indicator set to spaces
*                       for the old coverage (no change
*                       to the premium details);
*       - Validflag '1' for the new coverage with CPI-DATE
*                       set to MAXDATE, Indexation Indicator
*                       set to spaces, new instalment premium
*                       and sum assured values captured.
*
* GENERIC PROCESSING
* ==================
* - Call Generic Processing routines via T5671.
*   (e.g. UNLCHG £INCI record effects]).
*
* STATISTICS
* ==========
* If T6658 Statistics not spaces call LIFSTTR to create the
* statistical records.
*
* INCREASE RECORD MAINTENANCE
* ===========================
* - Set INCR record to Validflag '2' to avoid being processed
*   again.
*
* CONTRACT PROCESSING
* ===================
* - Rewrite the relevant old CHDR and PAYR records with
*   Validflag '2' and appropriate CURRTO date.
* - Write new Validflag '1' records with specific CURRFROM and
*   CURRTO dates and the correct premium.
* - Add the accumulated COVR premium values to SINSTAMT on
*   CHDR and PAYR.
* - Write PTRN.
*
****************************************************************** ****
*                                                                     *
 * </pre>
 */
public class Dry5137 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOldCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaFixNoIncr = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaMinTrmToCess = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaZbinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZlinstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(13, 2).init(ZERO);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();

	private String wsaaSubprogFound = "N";

	private FixedLengthStringData wsaaNextCovridno = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaNextCovno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 0);
	private FixedLengthStringData wsaaNextRidno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 2);

	private FixedLengthStringData filler4 = new FixedLengthStringData(4).isAPartOf(wsaaNextCovridno, 0,
			FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextCovnoR = new ZonedDecimalData(2, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaNextRidnoR = new ZonedDecimalData(2, 0).isAPartOf(filler4, 2).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaStoreRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaStorePlnsfx = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	/*
	 * Arrays to store regularly-referenced data, and reduce the r number accesses
	 * to the Itempf. Array sizes are also declared so that an increase of table
	 * size does not impact its processing.
	 */
	private ZonedDecimalData wsaaT5679Ix = new ZonedDecimalData(2, 0).setUnsigned();

	private PackedDecimalData wsaaT6658IxMax = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5671IxMax = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5688IxMax = new PackedDecimalData(5, 0);
	private static final int wsaaT5447Size = 1000;
	private FixedLengthStringData[] wsaaT5447Rec = FLSInittedArray(1000, 3);
	private FixedLengthStringData[] wsaaT5447Key = FLSDArrayPartOfArrayStructure(3, wsaaT5447Rec, 0);
	private FixedLengthStringData[] wsaaT5447ChdrlifCnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5447Key, 0,
			HIVALUES);

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray(1000, 14);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Rec, 0);
	private PackedDecimalData[] wsaaT5688Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Rec, 5);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 10);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(1, wsaaT5688Rec, 13);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 0);
	/* WSAA-T5671-ITEM */

	/*
	 * WSAA-T5671-ARRAY 03 WSAA-T5671-REC OCCURS 50
	 */
	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray(1000, 48);
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Authcode = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprog1 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 0);
	private FixedLengthStringData[] wsaaT5671Subprog2 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 10);
	private FixedLengthStringData[] wsaaT5671Subprog3 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 20);
	private FixedLengthStringData[] wsaaT5671Subprog4 = FLSDArrayPartOfArrayStructure(10, wsaaT5671Data, 30);

	private FixedLengthStringData wsaaSubprogs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSubprog = FLSArrayPartOfStructure(4, 10, wsaaSubprogs, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(wsaaSubprogs, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaSubprog1 = new FixedLengthStringData(10).isAPartOf(filler5, 0);
	private FixedLengthStringData wsaaSubprog2 = new FixedLengthStringData(10).isAPartOf(filler5, 10);
	private FixedLengthStringData wsaaSubprog3 = new FixedLengthStringData(10).isAPartOf(filler5, 20);
	private FixedLengthStringData wsaaSubprog4 = new FixedLengthStringData(10).isAPartOf(filler5, 30);

	private FixedLengthStringData wsaaValidComp = new FixedLengthStringData(1).init("N");
	private Validator validComp = new Validator(wsaaValidComp, "Y");
	private Validator invalidComp = new Validator(wsaaValidComp, "N");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private Validator invalidContract = new Validator(wsaaValidContract, "N");

	private FixedLengthStringData wsaaBypassFlag = new FixedLengthStringData(1).init("N");
	private Validator processReassurance = new Validator(wsaaBypassFlag, "Y");
	private Validator bypassReassurance = new Validator(wsaaBypassFlag, "N");
	/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t6658 = "T6658";
	private static final String t5671 = "T5671";

	private static final String t5447 = "T5447";
	/* ERRORS */

	private static final String h791 = "H791";
	private static final String h036 = "H036";

	private IntegerData wsaaT6658Ix = new IntegerData();
	private IntegerData wsaaT5447Ix = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private IntegerData wsaaT5671Ix = new IntegerData();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PcdtmjaTableDAM pcdtmjaIO = new PcdtmjaTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6658rec t6658rec = new T6658rec();
	private Acomcalrec acomcalrec = new Acomcalrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Actvresrec actvresrec = new Actvresrec();
	private Rasaltrrec rasaltrrec = new Rasaltrrec();
	private T5687rec t5687rec = new T5687rec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT6658ArrayInner wsaaT6658ArrayInner = new WsaaT6658ArrayInner();
	private Dry5137DAO dry5137DAO = getApplicationContext().getBean("dry5137DAO", Dry5137DAO.class);

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);

	private Chdrpf chdrlifRec = new Chdrpf();
	private Map<String, List<Itempf>> t6658Map = new HashMap<>();
	private Map<String, List<Itempf>> t5688Map = new HashMap<>();
	private Map<String, List<Itempf>> t5671Map = new HashMap<>();
	private Map<String, List<Itempf>> t5447Map = new HashMap<>();
	private Map<String, List<Itempf>> t5687Map = new HashMap<>();

	private Map<String, List<Chdrpf>> chdrlifMap = new HashMap<>();
	private Map<String, List<Payrpf>> payrMap = new HashMap<>();
	private Map<String, List<Incrpf>> incrMap = new HashMap<>();
	private Map<String, List<Covrpf>> covrMap = new HashMap<>();
	private Map<String, List<Lifepf>> lifelnbMap = new HashMap<>();
	private Map<String, List<Racdpf>> racdlnbMap = new HashMap<>();

	/* CONTROL-TOTALS */

	private int ct01Value;
	private int ct02Value;
	private int ct03Value;
	private int ct04Value;
	private int ct05Value;
	private int ct06Value;
	private int ct07Value;
	private int ct08Value;
	private int ct09Value;
	private int ct10Value;
	private int ct11Value;

	private List<Chdrpf> updateChdrlifList;
	private List<Chdrpf> insertChdrlifList;
	private List<Payrpf> updatePayrList;
	private List<Payrpf> insertPayrList;
	private List<Ptrnpf> insertPtrnList;
	private List<Incrpf> updateIncrList;
	private List<Covrpf> updateCovrList;
	private List<Covrpf> insertCovrList;
	private List<Covrpf> insertNewCovrList;
	private List<Racdpf> deleteRacdList;
	private boolean riskPremflag;
	private static final String RISKPREM_FEATURE_ID = "NBPRP094";// ILIFE-7845
	private Premiumrec premiumrec = new Premiumrec(); // ILIFE-7845
	private boolean stampDutyflag;
	private boolean reinstflag;

	private static final String ta85 = "TA85";
	private boolean isFoundPro;

	private PackedDecimalData wsaaProCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNewCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData chdrTranno = new PackedDecimalData(5, 0);
	private boolean prmhldtrad;
	private boolean lapseReinstated;
	private Map<String, Itempf> ta524Map;
	private Prmhpf prmhpf;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5137");
	private Datcon1rec datcon1rec = new Datcon1rec();

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dry5137() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {

		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry5137Dto> dry5137dtoList = readChunkRecord();
		int i = 1;
		for (Dry5137Dto dto : dry5137dtoList) {
			ct01Value++;
			edit2500(dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dto);
				if(i == dry5137dtoList.size()) {
					updateHeader2080(dto);
				}
			}
			i++;
		}
		updateDates();
		commit3500();
	}
	
	protected void updateHeader2080(Dry5137Dto dto) {
		Chdrpf c = chdrlifUpdate8000(dto);
		if (isFoundPro) {
			while (isLT(wsaaLastCpiDate, drypDryprcRecInner.drypRunDate)) {
				ptrnWrite8100(c);
				wsaaLastCpiDate.set(getChdrTranno(wsaaLastCpiDate.toInt()));
			}
		} else {
			ptrnWrite8100(c);
		}
	}

	protected void initialise1000() {

		wsaaTransDate.set(drypDryprcRecInner.drypRunDate);
		varcom.vrcmTimex.set(getCobolTime());
		wsaaTransTime.set(varcom.vrcmTimen);
		wsspEdterror.set(Varcom.oK);
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl("T5679");
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		t5679rec.t5679Rec.set(itemIO.getGenarea());

		String coy = drypDryprcRecInner.drypCompany.toString();
		t6658Map = itemDAO.loadSmartTable("IT", coy, "T6658");
		wsaaT6658Ix.set(1);
		loadT66581500();

		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");
		wsaaT5688Ix.set(1);
		loadT56881600();

		t5671Map = itemDAO.loadSmartTable("IT", coy, "T5671");
		wsaaT5671Ix.set(1);
		loadT56711700();

		t5447Map = itemDAO.loadSmartTable("IT", coy, "T5447");
		wsaaT5447Ix.set(1);
		loadT54471800();

		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");

		String stampdutyItem = "NBPROP01";
		stampDutyflag = FeaConfg.isFeatureExist(coy, stampdutyItem, appVars, "IT");
		String prmhldtradItem = "CSOTH010";// ILIFE-8509
		prmhldtrad = FeaConfg.isFeatureExist(coy, prmhldtradItem, appVars, "IT");
		if (prmhldtrad) // ILIFE-8179
			ta524Map = itemDAO.getItemMap("IT", coy, "TA524");
	}

	protected void loadT66581500() {
		if (t6658Map == null || t6658Map.size() == 0) {
			return;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t6658Map.size() > 200) {
			drylogrec.statuz.set(h036);
			drylogrec.params.set(t6658);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		for (List<Itempf> items : t6658Map.values()) {
			for (Itempf item : items) {
				t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT6658ArrayInner.wsaaT6658Annvry[wsaaT6658Ix.toInt()].set(item.getItemitem());
				wsaaT6658ArrayInner.wsaaT6658Itmfrm[wsaaT6658Ix.toInt()].set(item.getItmfrm());
				wsaaT6658ArrayInner.wsaaT6658Itmto[wsaaT6658Ix.toInt()].set(item.getItmto());
				wsaaT6658ArrayInner.wsaaT6658Data[wsaaT6658Ix.toInt()].set(t6658rec.t6658Rec);
				wsaaT6658IxMax.set(wsaaT6658Ix);
				wsaaT6658Ix.add(1);
			}
		}
	}

	protected void loadT56881600() {
		if (t5688Map == null || t5688Map.size() == 0) {
			return;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t5688Map.size() > 1000) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t5688);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		for (List<Itempf> items : t5688Map.values()) {
			for (Itempf item : items) {
				t5688rec.t5688Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(item.getItemitem());
				wsaaT5688Itmfrm[wsaaT5688Ix.toInt()].set(item.getItmfrm());
				wsaaT5688Itmto[wsaaT5688Ix.toInt()].set(item.getItmto());
				wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()].set(t5688rec.comlvlacc);
				wsaaT5688IxMax.set(wsaaT5688Ix);
				wsaaT5688Ix.add(1);
			}
		}
	}

	protected void loadT56711700() {
		if (t5671Map == null || t5671Map.size() == 0) {
			drylogrec.params.set("t5671:" + drypDryprcRecInner.drypBatctrcde);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

			return;
		}
		for (List<Itempf> items : t5671Map.values()) {
			for (Itempf item : items) {
				if (isEQ(subString(item.getItemitem(), 1, 4), drypDryprcRecInner.drypBatctrcde)) {
					/* If the number of items stored exceeds the working storage */
					/* array size, this is a fatal error. */
					if (isGT(wsaaT5671Ix, 1000)) {
						drylogrec.statuz.set(h791);
						drylogrec.params.set(t5671);
						drylogrec.drySystemError.setTrue();
						a000FatalError();
					}
					t5671rec.t5671Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5671Key[wsaaT5671Ix.toInt()].set(item.getItemitem());
					wsaaT5671Subprog1[wsaaT5671Ix.toInt()].set(t5671rec.subprog01);
					wsaaT5671Subprog2[wsaaT5671Ix.toInt()].set(t5671rec.subprog02);
					wsaaT5671Subprog3[wsaaT5671Ix.toInt()].set(t5671rec.subprog03);
					wsaaT5671Subprog4[wsaaT5671Ix.toInt()].set(t5671rec.subprog04);
					wsaaT5671IxMax.set(wsaaT5671Ix);
					wsaaT5671Ix.add(1);
				}
			}
		}
		if (wsaaT5671Ix.toInt() == 1) {
			drylogrec.params.set("t5671:" + drypDryprcRecInner.drypBatctrcde);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

			return;
		}
	}

	protected void loadT54471800() {
		if (t5447Map == null || t5447Map.size() == 0) {
			return;
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t5447Map.size() > wsaaT5447Size) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t5447);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		for (List<Itempf> items : t5447Map.values()) {
			for (Itempf item : items) {
				wsaaT5447Key[wsaaT5447Ix.toInt()].set(item.getItemitem());
				wsaaT5447Ix.add(1);
			}
		}
	}

	private List<Dry5137Dto> readChunkRecord() {
		List<Dry5137Dto> recordList = dry5137DAO.getActualAutoIncrease(drypDryprcRecInner.drypCompany.toString(),
				drypDryprcRecInner.drypRunDate.toString(), drypDryprcRecInner.drypEntity.toString(), "1");

		if (!recordList.isEmpty()) {
			List<String> chdrNumList = Arrays.asList(drypDryprcRecInner.drypEntity.toString());
			chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrNumList);
			payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(drypDryprcRecInner.drypCompany.trim(), chdrNumList);
			incrMap = incrpfDAO.searchIncrRecordByChdrnum(chdrNumList);
			covrMap = covrpfDAO.searchValidCovrByChdrnum(chdrNumList);
			lifelnbMap = lifepfDAO.searchLifelnbRecord(drypDryprcRecInner.drypCompany.trim(), chdrNumList);

		}
		return recordList;

	}

	protected void edit2500(Dry5137Dto dry5137Dto) {
		wsspEdterror.set(Varcom.oK);
		checkComponentStatuz2600(dry5137Dto);
		if (isEQ(dry5137Dto.getChdrnum(), wsaaChdrnum)) {
			return;
		} else {
			lapseReinstated = false;
		}
		chdrlifRec = readChdr2700(dry5137Dto);
		checkContractStatuz2750(chdrlifRec);
		if (invalidContract.isTrue()) {
			wsspEdterror.set(SPACES);
			ct11Value++;
		}
	}

	protected void checkComponentStatuz2600(Dry5137Dto dry5137Dto) {
		invalidComp.setTrue();
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validComp.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(dry5137Dto.getStatcode(), t5679rec.covRiskStat[wsaaT5679Ix.toInt()])) {
				validComp.setTrue();
			}
		}
		if (!validComp.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
		invalidComp.setTrue();
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validComp.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(dry5137Dto.getPstatcode(), t5679rec.covPremStat[wsaaT5679Ix.toInt()])) {
				validComp.setTrue();
			}
		}
		if (!validComp.isTrue()) {
			wsspEdterror.set(SPACES);
		}
	}

	protected Chdrpf readChdr2700(Dry5137Dto dry5137Dto) {
		Chdrpf chdrlifIO = null;
		if (chdrlifMap != null && chdrlifMap.containsKey(dry5137Dto.getChdrnum())) {
			for (Chdrpf c : chdrlifMap.get(dry5137Dto.getChdrnum())) {
				if (c.getChdrcoy().toString().equals(dry5137Dto.getChdrcoy())) {
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			drylogrec.params.set(wsaaChdrnum.toString());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		} else {
			compute(wsaaTranno, 0).set(add(chdrlifIO.getTranno(), 1));
			wsaaChdrnum.set(dry5137Dto.getChdrnum());
			wsaaNextCovnoR.set(ZERO);
			wsaaNextRidnoR.set(ZERO);
			wsaaInstprem.set(ZERO);
			acomcalrec.acomcalRec.set(SPACES);
			acomcalrec.chdrCurrfrom.set(ZERO);
			acomcalrec.chdrOccdate.set(ZERO);
			acomcalrec.covrAnnprem.set(ZERO);
			acomcalrec.covrPlanSuffix.set(ZERO);
			acomcalrec.covtAnnprem.set(ZERO);
			acomcalrec.covtPlanSuffix.set(ZERO);
			acomcalrec.fpcoCurrto.set(ZERO);
			acomcalrec.fpcoTargPrem.set(ZERO);
			acomcalrec.payrBtdate.set(ZERO);
			acomcalrec.payrPtdate.set(ZERO);
			acomcalrec.tranno.set(ZERO);
			acomcalrec.transactionDate.set(ZERO);
			acomcalrec.transactionTime.set(ZERO);
			acomcalrec.user.set(ZERO);
		}
		return chdrlifIO;
	}

	protected void checkContractStatuz2750(Chdrpf chdrlifIO) {
		invalidContract.setTrue();
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validContract.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(chdrlifIO.getStatcode(), t5679rec.cnRiskStat[wsaaT5679Ix.toInt()])) {
				validContract.setTrue();
			}
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
		invalidContract.setTrue();
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validContract.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(chdrlifIO.getPstcde(), t5679rec.cnPremStat[wsaaT5679Ix.toInt()])) {
				validContract.setTrue();
			}
		}
		if (prmhldtrad && !validContract.isTrue()) {
			validatePhContract(chdrlifIO);
		}
	}

	protected void validatePhContract(Chdrpf chdrlifIO) {
		if (ta524Map.get(chdrlifIO.getCnttype()) != null) {
			prmhpf = prmhpfDAO.getPrmhRecord(chdrlifIO.getChdrcoy().toString(), chdrlifIO.getChdrnum());
			if (prmhpf != null && drypDryprcRecInner.drypRunDate.toInt() >= prmhpf.getFrmdate()) {
				validContract.setTrue();
			}
		}
	}

	protected void update3000(Dry5137Dto dry5137Dto) {
		/* START */
		/* Increase the instalment premium by the difference between */
		/* the new and the last instalments. */
		reinstflag = FeaConfg.isFeatureExist(drypDryprcRecInner.drypCompany.toString(), "CSLRI003", appVars, "IT");
		if (reinstflag) {
			markprorated(dry5137Dto);
		}
		compute(wsaaInstprem, 2).set(sub(add(wsaaInstprem, dry5137Dto.getNewinst()), dry5137Dto.getLastInst()));
		compute(wsaaZbinstprem, 2).set(sub(add(wsaaZbinstprem, dry5137Dto.getZbnewinst()), dry5137Dto.getZblastinst()));
		compute(wsaaZlinstprem, 2).set(sub(add(wsaaZlinstprem, dry5137Dto.getZlnewinst()), dry5137Dto.getZllastinst()));
		Incrpf i = updateIncr3100(dry5137Dto);
		searchT66583150(dry5137Dto);
		if (isFoundPro && (isNE(wsaaProCpiDate, dry5137Dto.getCrrcd()))) {
			return;
		} else {
			if (isFoundPro) {

				while (isLT(wsaaNewCpiDate, drypDryprcRecInner.drypRunDate)) {
					wsaaNewCpiDate.set(getChdrTranno(wsaaNewCpiDate.toInt()));
					compute(wsaaTranno, 0).set(add(wsaaTranno, 1));
				}
			}
			Covrpf c = t6658Test3200(i, dry5137Dto);
			a100ReadT5687(dry5137Dto);
			commissionProcess3300(c, dry5137Dto);
			statistics3400(dry5137Dto);
		}
		/* EXIT */
	}

	protected void markprorated(Dry5137Dto dry5137Dto) {
		List<Ptrnpf> ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(dry5137Dto.getChdrcoy(), dry5137Dto.getChdrnum());

		if (ptrnrecords != null) {
			for (Ptrnpf ptrn : ptrnrecords) {
				if (isNE(ptrn.getBatctrcde(), ta85) && isNE(ptrn.getBatctrcde(), "B524")) {
					continue;
				}
				if (isEQ(ptrn.getBatctrcde(), "B524")) {
					break;
				}
				if (isEQ(ptrn.getBatctrcde(), ta85)) {
					isFoundPro = true;

					calcProCpi(dry5137Dto);
				}
			}
		}
	}

	protected void calcProCpi(Dry5137Dto dry5137Dto) {
		Incrpf incrpf = incrpfDAO.getIncrByCrrcdDate(dry5137Dto.getChdrcoy(), dry5137Dto.getChdrnum(),
				dry5137Dto.getLife(), dry5137Dto.getCoverage(), dry5137Dto.getRider(), dry5137Dto.getPlnsfx(),
				drypDryprcRecInner.drypRunDate.toInt(), "1");/* IJTI-1523 */
		if (incrpf != null) {
			wsaaProCpiDate.set(incrpf.getCrrcd());
		}
		if (isEQ(wsaaLastCpiDate, ZERO)) {
			wsaaLastCpiDate.set(chdrlifRec.getBillcd());
			wsaaNewCpiDate.set(chdrlifRec.getBillcd());
			chdrTranno.set(wsaaTranno);
		}
	}

	protected int getChdrTranno(int date) {

		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(date);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		return datcon2rec.intDate2.toInt();
	}

	protected Incrpf updateIncr3100(Dry5137Dto dry5137Dto) {
		Incrpf incrIO = null;
		if (incrMap != null && incrMap.containsKey(dry5137Dto.getChdrnum())) {
			for (Incrpf i : incrMap.get(dry5137Dto.getChdrnum())) {
				if (dry5137Dto.getChdrcoy().equals(i.getChdrcoy()) && dry5137Dto.getLife().equals(i.getLife())
						&& dry5137Dto.getCoverage().equals(i.getCoverage())
						&& dry5137Dto.getRider().equals(i.getRider()) && dry5137Dto.getPlnsfx().equals(i.getPlnsfx())
						&& i.getValidflag().equals("1")) {
					if (isFoundPro) {
						if (isEQ(dry5137Dto.getCrrcd(), i.getCrrcd())) {
							incrIO = i;
							break;
						}
					} else {
						incrIO = i;
						break;
					}
				}
			}
		}
		if (incrIO == null) {
			drylogrec.params.set(dry5137Dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		} else {
			if (updateIncrList == null) {
				updateIncrList = new ArrayList<>();
			}

			incrIO.setValidflag("2");
			updateIncrList.add(incrIO);
			ct02Value++;

		}
		return incrIO;
	}

	protected void searchT66583150(Dry5137Dto dry5137Dto) {
		/* Check the from and to date on T6658 comply with the */
		/* contract risk commencement date. */
		for (wsaaT6658Ix.set(1); !(isGT(wsaaT6658Ix, wsaaT6658IxMax)
				|| (isEQ(wsaaT6658ArrayInner.wsaaT6658Annvry[wsaaT6658Ix.toInt()], dry5137Dto.getAnnvry())
						&& isGTE(dry5137Dto.getCrrcd(), wsaaT6658ArrayInner.wsaaT6658Itmfrm[wsaaT6658Ix.toInt()])
						&& isLTE(dry5137Dto.getCrrcd(),
								wsaaT6658ArrayInner.wsaaT6658Itmto[wsaaT6658Ix.toInt()]))); wsaaT6658Ix.add(1)) {
			/* CONTINUE_STMT */
		}
		if (isGT(wsaaT6658Ix, wsaaT6658IxMax)) {
			drylogrec.statuz.set(h036);
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(Varcom.begn);
			itdmIO.setStatuz(Varcom.endp);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t6658);
			itdmIO.setItemitem(dry5137Dto.getAnnvry());
			itdmIO.setItmfrm(dry5137Dto.getCrrcd());
			drylogrec.params.set(itdmIO.getParams());
			a000FatalError();
		}
	}

	protected Covrpf t6658Test3200(Incrpf incrIO, Dry5137Dto dry5137Dto) {
		/* START */
		/* Part of the updating in this section is performed for both */
		/* updating existing components and adding new components. */
		Covrpf insertCovr = null;
		if (isNE(wsaaT6658ArrayInner.wsaaT6658Addnew[wsaaT6658Ix.toInt()], SPACES)
				|| isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
			insertCovr = commonProcessing3210(dry5137Dto, incrIO);
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
				searchT54473800();
				checkReass3220(incrIO, dry5137Dto, insertCovr);
			} else {
				insertCovr = addCovr3230(insertCovr, dry5137Dto, incrIO);
			}
			genericProcessing3700(insertCovr, dry5137Dto);
		}
		return insertCovr;
		/* EXIT */
	}

	protected Covrpf commonProcessing3210(Dry5137Dto dry5137Dto, Incrpf incrIO) {
		Covrpf covrIO = null;
		if (covrMap != null && covrMap.containsKey(dry5137Dto.getChdrnum())) {
			for (Covrpf i : covrMap.get(dry5137Dto.getChdrnum())) {
				if (dry5137Dto.getChdrcoy().equals(i.getChdrcoy()) && dry5137Dto.getLife().equals(i.getLife())
						&& dry5137Dto.getCoverage().equals(i.getCoverage())
						&& dry5137Dto.getRider().equals(i.getRider()) && dry5137Dto.getPlnsfx() == i.getPlanSuffix()) {
					if (isFoundPro) {
						covrIO = i;
						break;
					} else {
						covrIO = i;
						break;
					}
				}
			}
		}
		if (covrIO == null) {
			drylogrec.params.set(dry5137Dto.getChdrnum());

			a000FatalError();
		} // IJTI-462 START
		else {
			/* Update the COVR record with a valid flag of 2 and set */
			/* 'COVR-CURRTO' date to the effective date of the Increase. */
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
				rasaltrrec.oldSumins.set(covrIO.getSumins());
			}

			if (updateCovrList == null) {
				updateCovrList = new ArrayList<>();
			}

			covrIO.setValidflag("2");
			if (isFoundPro) {
				covrIO.setCurrto(wsaaProCpiDate.toInt());
			} else {
				covrIO.setCurrto(covrIO.getCpiDate());
			}
			updateCovrList.add(covrIO);

			Covrpf insertCovr = new Covrpf(covrIO);
			insertCovr.setValidflag("1");
			insertCovr.setTranno(wsaaTranno.toInt());// ILIFE-8472
			if (isFoundPro) {
				insertCovr.setCurrfrom(wsaaProCpiDate.toInt());
			} else {
				insertCovr.setCurrfrom(covrIO.getCpiDate());
			}
			insertCovr.setCurrto(varcom.vrcmMaxDate.toInt());
			insertCovr.setTransactionDate(wsaaTransDate.toInt());
			insertCovr.setTransactionTime(wsaaTransTime.toInt());
			insertCovr.setUser(drypDryprcRecInner.drypUser.toInt());
			initialize(varcom.vrcmTranid);
			insertCovr.setTermid(varcom.vrcmTermid.toString());
			insertCovr.setIndexationInd(SPACES.toString());
			if (isFoundPro) {
				wsaaOldCpiDate.set(wsaaProCpiDate.toInt());
			} else {
				wsaaOldCpiDate.set(insertCovr.getCpiDate());
			}
			calcNextCpiDate3211(insertCovr);
			getLifergpDetails7000(insertCovr);
			trmToCessDate3214(insertCovr);
			calcIncrProcess3216(insertCovr, dry5137Dto);
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Addexist[wsaaT6658Ix.toInt()], SPACES)) {
				insertCovr.setInstprem(incrIO.getNewinst());
				insertCovr.setZbinstprem(incrIO.getZbnewinst());
				insertCovr.setZlinstprem(incrIO.getZlnewinst());
				insertCovr.setSumins(incrIO.getNewsum());
			}

			// ILIFE-7845
			premiumrec.riskPrem.set(BigDecimal.ZERO);
			insertCovr.setRiskprem(BigDecimal.ZERO);
			riskPremflag = FeaConfg.isFeatureExist(drypDryprcRecInner.drypBatccoy.toString().trim(),
					RISKPREM_FEATURE_ID, appVars, "IT");
			if (riskPremflag) {
				premiumrec.riskPrem.set(ZERO);
				premiumrec.cnttype.set(chdrlifRec.getCnttype());
				premiumrec.crtable.set(covrIO.getCrtable());
				premiumrec.calcTotPrem.set(incrIO.getNewinst());
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
				insertCovr.setRiskprem(premiumrec.riskPrem.getbigdata()); // ILIFE-7845
			}

			// ILIFE-7845 end
			insertCovr.setZclstate(SPACE);
			insertCovr.setZstpduty01(BigDecimal.ZERO);
			if (stampDutyflag) {
				insertCovr.setZstpduty01(incrIO.getZstpduty01());
				insertCovr.setZclstate(covrIO.getZclstate());
			}
			if (prmhldtrad && "Y".equals(covrIO.getReinstated()))
				insertCovr.setReinstated(SPACE);
			if (insertCovrList == null) {
				insertCovrList = new ArrayList<>();
			}
			insertCovrList.add(insertCovr);
			if ("Y".equals(covrIO.getReinstated()))
				lapseReinstated = true;
			return insertCovr;
		}
		return null;
	}

	protected void calcNextCpiDate3211(Covrpf insertCovr) {
		/* CALC */
		initialize(datcon2rec.datcon2Rec);
		if (isFoundPro) {
			datcon2rec.intDate1.set(wsaaProCpiDate);
		} else {
			datcon2rec.intDate1.set(insertCovr.getCpiDate());
		}
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		insertCovr.setCpiDate(datcon2rec.intDate2.toInt());
		/* EXIT */
	}

	protected void trmToCessDate3214(Covrpf insertCovr) {
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(insertCovr.getCpiDate());
		datcon3rec.intDate2.set(insertCovr.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaMinTrmToCess.set(datcon3rec.freqFactor);
	}

	protected void calcIncrProcess3216(Covrpf covrIO, Dry5137Dto dry5137Dto) {
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(covrIO.getCpiDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaFixNoIncr.set(datcon3rec.freqFactor);
		wsaaBillfreq.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		compute(wsaaFixNoIncr, 1).setRounded(mult(wsaaFixNoIncr, wsaaBillfreq));
		/* Set the maximum age and fixed number of increases to maximum */
		/* values if they are not set on the table. */
		if (isLTE(wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()], 0)) {
			/* MOVE 99 TO WSAA-T6658-MAX-AGE */
			wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()].set(999);
		}
		if (isLTE(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()], 0)) {
			/* MOVE 99 TO WSAA-T6658-FIXDTRM */
			wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()].set(999);
		}
		/* If the component is no longer eligible for increases for the */
		/* following reasons, according to the values on T6658: */
		/* - it has less than the minimum term left to run */
		/* - it has already processed the fixed number of increases */
		/* - the life assured has passed the maximum age for increases */
		/* write out a message to indicate this and set the next */
		/* increase date to 9s to prevent it from being processed */
		/* again. */
		if (isGT(wsaaT6658ArrayInner.wsaaT6658Minctrm[wsaaT6658Ix.toInt()], wsaaMinTrmToCess)
				|| isLT(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()], wsaaFixNoIncr)
				|| isLT(wsaaT6658ArrayInner.wsaaT6658MaxAge[wsaaT6658Ix.toInt()], wsaaAnb)
				|| isGT(covrIO.getCpiDate(), covrIO.getRiskCessDate())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(dry5137Dto.getChdrnum());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5137Dto.getLife());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5137Dto.getCoverage());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5137Dto.getRider());
			covrIO.setCpiDate(99999999);
		}
	}

	protected void checkReass3220(Incrpf incrIO, Dry5137Dto dry5137Dto, Covrpf covr) {
		if (processReassurance.isTrue()) {
			rasaltrrec.newSumins.set(incrIO.getNewsum());
			rasaltrrec.addNewCovr.set("N");
			readLife3250(dry5137Dto);
			processReassurance5000(covr, dry5137Dto);
		}
		acomcalrec.covtLife.set(dry5137Dto.getLife());
		acomcalrec.covtCoverage.set(dry5137Dto.getCoverage());
		acomcalrec.covtRider.set(dry5137Dto.getRider());
		acomcalrec.covtPlanSuffix.set(dry5137Dto.getPlnsfx());
		ct06Value++;

	}

	protected Covrpf addCovr3230(Covrpf origCovrIO, Dry5137Dto dry5137Dto, Incrpf incrIO) {
		Covrpf covrIO = new Covrpf(origCovrIO);
		covrIO.setValidflag("1");
		covrIO.setTranno(wsaaTranno.toInt());
		covrIO.setCurrfrom(wsaaOldCpiDate.toInt());
		covrIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrIO.setTransactionDate(wsaaTransDate.toInt());
		covrIO.setTransactionTime(wsaaTransTime.toInt());
		covrIO.setUser(drypDryprcRecInner.drypUser.toInt());
		initialize(varcom.vrcmTranid);
		covrIO.setTermid(varcom.vrcmTermid.toString());
		covrIO.setIndexationInd(SPACES.toString());
		covrIO.setCpiDate(99999999);
		covrIO.setCrrcd(dry5137Dto.getCrrcd());
		covrIO.setInstprem(sub(incrIO.getNewinst(), incrIO.getLastInst()).getbigdata());
		covrIO.setZbinstprem(sub(incrIO.getZbnewinst(), incrIO.getZblastinst()).getbigdata());
		covrIO.setZlinstprem(sub(incrIO.getZlnewinst(), incrIO.getZllastinst()).getbigdata());
		covrIO.setSumins(sub(incrIO.getNewsum(), incrIO.getLastSum()).getbigdata());
		nextComponent6000(dry5137Dto);
		wsaaCount.set(ZERO);
		wsaaStoreRider.set(dry5137Dto.getRider());
		wsaaStorePlnsfx.set(dry5137Dto.getPlnsfx());
		wsaaNextCovnoR.add(1);
		covrIO.setCoverage(wsaaNextCovno.toString());
		if (isNE(dry5137Dto.getRider(), wsaaStoreRider)) {
			if (isEQ(dry5137Dto.getPlnsfx(), wsaaStorePlnsfx)) {
				wsaaCount.add(1);
			}
		}
		wsaaNextRidnoR.set(wsaaCount);
		covrIO.setRider(wsaaNextRidno.toString());
		covrIO.setAnbAtCcd(sub(wsaaAnb, 1).toInt());

		if (insertNewCovrList == null) {
			insertNewCovrList = new ArrayList<>();
		}
		insertNewCovrList.add(covrIO);
		searchT54473800();
		if (processReassurance.isTrue()) {
			rasaltrrec.newSumins.set(incrIO.getNewsum());
			rasaltrrec.oldSumins.set(incrIO.getLastSum());
			rasaltrrec.addNewCovr.set("Y");
			readLife3250(dry5137Dto);
			processReassurance5000(covrIO, dry5137Dto);
		}
		acomcalrec.covtLife.set(dry5137Dto.getLife());
		acomcalrec.covtCoverage.set(wsaaNextCovnoR);
		acomcalrec.covtRider.set(wsaaNextRidnoR);
		acomcalrec.covtPlanSuffix.set(dry5137Dto.getPlnsfx());
		ct07Value++;

		return covrIO;
	}

	protected void readLife3250(Dry5137Dto dry5137Dto) {
		if (lifelnbMap != null && lifelnbMap.containsKey(chdrlifRec.getChdrnum())) {
			boolean foundFlag = false;
			for (Lifepf l : lifelnbMap.get(chdrlifRec.getChdrnum())) {
				if (l.getChdrcoy().equals(String.valueOf(chdrlifRec.getChdrcoy()))
						&& l.getLife().equals(dry5137Dto.getLife())// ILIFE-6658
						&& "00".equals(l.getJlife())) {
					wsaaL1Clntnum.set(l.getLifcnum());
					foundFlag = true;
					break;
				}
			}
			if (!foundFlag) {

				drylogrec.params.set(chdrlifRec.getChdrnum());
				drylogrec.drySystemError.setTrue();
				a000FatalError();

			}
			wsaaL2Clntnum.set(SPACES);
			for (Lifepf l : lifelnbMap.get(chdrlifRec.getChdrnum())) {
				if (l.getChdrcoy().equals(String.valueOf(chdrlifRec.getChdrcoy()))
						&& l.getLife().equals(dry5137Dto.getLife())// ILIFE-6658
						&& "01".equals(l.getJlife())) {
					wsaaL2Clntnum.set(l.getLifcnum());
					break;
				}
			}
		}
	}

	protected void commissionProcess3300(Covrpf covrIO, Dry5137Dto dry5137Dto) {
		/* If no commission is required, leave the section. */
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Comind[wsaaT6658Ix.toInt()], SPACES)) {
			return;
		}

		/* Check if contract or component level accounting is required. */
		for (wsaaT5688Ix.set(1); !(isGT(wsaaT5688Ix, wsaaT5688IxMax)
				|| (isEQ(wsaaT5688Cnttype[wsaaT5688Ix.toInt()], chdrlifRec.getCnttype())
						&& isGTE(chdrlifRec.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix.toInt()])
						&& isLTE(chdrlifRec.getOccdate(), wsaaT5688Itmto[wsaaT5688Ix.toInt()]))); wsaaT5688Ix.add(1)) {
			/* CONTINUE_STMT */
		}
		if (isGT(wsaaT5688Ix, wsaaT5688IxMax)) {
			drylogrec.statuz.set("E308");
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(Varcom.begn);
			itdmIO.setStatuz(Varcom.endp);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5688);
			itdmIO.setItemitem(chdrlifRec.getCnttype());
			itdmIO.setItmfrm(chdrlifRec.getOccdate());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		/* Create a PCDT record for the Servicing Agent Number on the , */
		/* CHDR and then call ACOMCALC subroutine to revalidate all */
		/* Commission records. The PCDT record will later be deleted. */
		pcdtmjaIO.setRecKeyData(SPACES);
		pcdtmjaIO.setRecNonKeyData(SPACES);
		pcdtmjaIO.setChdrcoy(dry5137Dto.getChdrcoy());
		pcdtmjaIO.setChdrnum(dry5137Dto.getChdrnum());
		pcdtmjaIO.setPlanSuffix(dry5137Dto.getPlnsfx());
		pcdtmjaIO.setLife(dry5137Dto.getLife());
		pcdtmjaIO.setCoverage(acomcalrec.covtCoverage);
		pcdtmjaIO.setRider(acomcalrec.covtRider);
		pcdtmjaIO.setTranno(wsaaTranno);
		pcdtmjaIO.setAgntnum(1, chdrlifRec.getAgntnum());
		pcdtmjaIO.setSplitc(1, 100);
		pcdtmjaIO.setFunction(Varcom.writr);
		pcdtmjaIO.setFormat("PCDTMJAREC");
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(pcdtmjaIO.getStatuz());
			drylogrec.params.set(pcdtmjaIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		ct08Value++;

		acomcalrec.statuz.set(Varcom.oK);
		acomcalrec.company.set(dry5137Dto.getChdrcoy());
		acomcalrec.chdrnum.set(dry5137Dto.getChdrnum());
		acomcalrec.covrLife.set(dry5137Dto.getLife());
		acomcalrec.covrCoverage.set(dry5137Dto.getCoverage());
		acomcalrec.covrRider.set(dry5137Dto.getRider());
		acomcalrec.covrPlanSuffix.set(dry5137Dto.getPlnsfx());
		acomcalrec.tranno.set(wsaaTranno);
		acomcalrec.transactionDate.set(wsaaTransDate);
		acomcalrec.transactionTime.set(wsaaTransTime);
		varcom.vrcmTranid.set(SPACES);
		acomcalrec.termid.set(varcom.vrcmTermid);
		/* Read the PAYR details here as some fields are needed in this */
		/* section. */
		Payrpf payrIO = readPayrDetails3350(dry5137Dto);
		acomcalrec.payrBtdate.set(payrIO.getBtdate());
		/* The PAYR-PTDATE is not passed in the linkage, instead the */
		/* CRRCD of the Increase records is passed, because this */
		/* date is used in ACOMCALC for the Effective Date of the AGCM. */
		acomcalrec.payrPtdate.set(dry5137Dto.getCrrcd());
		acomcalrec.payrBillfreq.set(payrIO.getBillfreq());
		wsaaBillfreq.set(payrIO.getBillfreq());
		acomcalrec.payrCntcurr.set(payrIO.getCntcurr());
		acomcalrec.cnttype.set(chdrlifRec.getCnttype());
		acomcalrec.chdrOccdate.set(chdrlifRec.getOccdate());
		acomcalrec.chdrCurrfrom.set(chdrlifRec.getCurrfrom());
		acomcalrec.chdrAgntcoy.set(dry5137Dto.getChdrcoy());
		acomcalrec.covrJlife.set(covrIO.getJlife());
		acomcalrec.covrCrtable.set(covrIO.getCrtable());
		acomcalrec.comlvlacc.set(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()]);
		acomcalrec.basicCommMeth.set(dry5137Dto.getBasicCommMeth());
		acomcalrec.bascpy.set(dry5137Dto.getBascpy());
		acomcalrec.rnwcpy.set(dry5137Dto.getRnwcpy());
		acomcalrec.srvcpy.set(dry5137Dto.getSrvcpy());
		/* COMPUTE ACOM-COVR-ANNPREM ROUNDED = LASTINST */
		/* * WSAA-BILLFREQ. */
		/* COMPUTE ACOM-COVT-ANNPREM ROUNDED = NEWINST */
		/* * WSAA-BILLFREQ. */
		if (isNE(t6658rec.addnew, SPACES)) {
			acomcalrec.covrAnnprem.set(ZERO);
		} else {
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				compute(acomcalrec.covrAnnprem, 2).set(mult(dry5137Dto.getZblastinst(), wsaaBillfreq));
			} else {
				compute(acomcalrec.covrAnnprem, 2).set(mult(dry5137Dto.getLastInst(), wsaaBillfreq));
			}
		}
		if (isNE(t6658rec.addnew, SPACES)) {
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				compute(acomcalrec.covtAnnprem, 2).set(sub((mult(dry5137Dto.getZbnewinst(), wsaaBillfreq)),
						(mult(dry5137Dto.getZblastinst(), wsaaBillfreq))));
			} else {
				compute(acomcalrec.covtAnnprem, 2).set(sub((mult(dry5137Dto.getNewinst(), wsaaBillfreq)),
						(mult(dry5137Dto.getLastInst(), wsaaBillfreq))));
			}
		} else {
			if (isNE(t5687rec.zrrcombas, SPACES)) {
				compute(acomcalrec.covtAnnprem, 2).set(mult(dry5137Dto.getZbnewinst(), wsaaBillfreq));
			} else {
				compute(acomcalrec.covtAnnprem, 2).set(mult(dry5137Dto.getNewinst(), wsaaBillfreq));
			}
		}
		acomcalrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		acomcalrec.atmdLanguage.set(drypDryprcRecInner.drypLanguage);
		acomcalrec.atmdBatchKey.set(drypDryprcRecInner.drypBatchKey);
		acomcalrec.fpcoTargPrem.set(0);
		acomcalrec.fpcoCurrto.set(0);
		callProgram(Acomcalc.class, acomcalrec.acomcalRec);
		if (isNE(acomcalrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(acomcalrec.statuz);
			drylogrec.params.set(acomcalrec.acomcalRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		pcdtmjaIO.setFunction(Varcom.delet);
		SmartFileCode.execute(appVars, pcdtmjaIO);
		if (isNE(pcdtmjaIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(pcdtmjaIO.getParams());
			drylogrec.statuz.set(pcdtmjaIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
	}

	protected Payrpf readPayrDetails3350(Dry5137Dto dry5137Dto) {
		Payrpf payrIO = null;
		if (payrMap != null && payrMap.containsKey(wsaaChdrnum.toString())) {
			for (Payrpf p : payrMap.get(dry5137Dto.getChdrnum())) {
				if (dry5137Dto.getChdrcoy().equals(p.getChdrcoy()) && 1 == p.getPayrseqno()) {
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			drylogrec.params.set(dry5137Dto.getChdrnum());
			a000FatalError();
		}
		return payrIO;
	}

	protected void statistics3400(Dry5137Dto dry5137Dto) {
		/* If no statistics are required, leave the section. */
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Statind[wsaaT6658Ix.toInt()], SPACES)) {
			return;
		}
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.chdrcoy.set(dry5137Dto.getChdrcoy());
		lifsttrrec.chdrnum.set(dry5137Dto.getChdrnum());
		lifsttrrec.tranno.set(wsaaTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, Varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
	}

	protected void commit3500() {
		if (updateIncrList != null && !updateIncrList.isEmpty()) {
			incrpfDAO.updateValidFlag(updateIncrList);
			updateIncrList.clear();
		}
		if (updateCovrList != null && !updateCovrList.isEmpty()) {
			covrpfDAO.updateCovrRecord(updateCovrList, 0);
			updateCovrList.clear();
		}
		List<Covrpf> insertAllCovrpfList = new ArrayList<>();
		if (insertCovrList != null && !insertCovrList.isEmpty()) {
			insertAllCovrpfList.addAll(insertCovrList);
			insertCovrList.clear();
		}
		covrpfDAO.insertCovrBulk(insertAllCovrpfList);

		if (insertPtrnList != null && !insertPtrnList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnList);
			insertPtrnList.clear();
		}
		if (updatePayrList != null && !updatePayrList.isEmpty()) {
			payrpfDAO.updatePayrRecord(updatePayrList);
			updatePayrList.clear();
		}
		if (insertPayrList != null && !insertPayrList.isEmpty()) {
			payrpfDAO.insertPayrpfList(insertPayrList);
			insertPayrList.clear();
		}
		if (deleteRacdList != null && !deleteRacdList.isEmpty()) {
			racdpfDAO.deleteRacdRcds(deleteRacdList);
			deleteRacdList.clear();
		}
		if (updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
			chdrpfDAO.updateChdrRecord(updateChdrlifList, 0);
			updateChdrlifList.clear();
		}
		if (insertChdrlifList != null && !insertChdrlifList.isEmpty()) {
			chdrpfDAO.insertChdrAmt(insertChdrlifList);
			insertChdrlifList.clear();
		}
		commitControlTotals();
	}

	private void commitControlTotals() {
		drycntrec.contotNumber.set(1);
		drycntrec.contotValue.set(ct01Value);
		d000ControlTotals();
		ct01Value = 0;

		drycntrec.contotNumber.set(2);
		drycntrec.contotValue.set(ct02Value);
		d000ControlTotals();
		ct02Value = 0;

		drycntrec.contotNumber.set(3);
		drycntrec.contotValue.set(ct03Value);
		d000ControlTotals();
		ct03Value = 0;

		drycntrec.contotNumber.set(4);
		drycntrec.contotValue.set(ct04Value);
		d000ControlTotals();
		ct04Value = 0;

		drycntrec.contotNumber.set(5);
		drycntrec.contotValue.set(ct05Value);
		d000ControlTotals();
		ct05Value = 0;

		drycntrec.contotNumber.set(6);
		drycntrec.contotValue.set(ct06Value);
		d000ControlTotals();
		ct06Value = 0;

		drycntrec.contotNumber.set(7);
		drycntrec.contotValue.set(ct07Value);
		d000ControlTotals();
		ct07Value = 0;

		drycntrec.contotNumber.set(8);
		drycntrec.contotValue.set(ct08Value);
		d000ControlTotals();
		ct08Value = 0;

		drycntrec.contotNumber.set(9);
		drycntrec.contotValue.set(ct09Value);
		d000ControlTotals();
		ct09Value = 0;

		drycntrec.contotNumber.set(10);
		drycntrec.contotValue.set(ct10Value);
		d000ControlTotals();
		ct10Value = 0;

		drycntrec.contotNumber.set(11);
		drycntrec.contotValue.set(ct11Value);
		d000ControlTotals();
		ct11Value = 0;

	}

	protected void genericProcessing3700(Covrpf covrIO, Dry5137Dto dry5137Dto) {
		wsaaSubprogFound = "N";
		for (wsaaT5671Ix.set(1); !(isGT(wsaaT5671Ix, wsaaT5671IxMax) || isEQ(wsaaSubprogFound, "Y")); wsaaT5671Ix
				.add(1)) {
			if (isEQ(wsaaT5671Crtable[wsaaT5671Ix.toInt()], dry5137Dto.getCrtable())) {
				wsaaSubprog1.set(wsaaT5671Subprog1[wsaaT5671Ix.toInt()]);
				wsaaSubprog2.set(wsaaT5671Subprog2[wsaaT5671Ix.toInt()]);
				wsaaSubprog3.set(wsaaT5671Subprog3[wsaaT5671Ix.toInt()]);
				wsaaSubprog4.set(wsaaT5671Subprog4[wsaaT5671Ix.toInt()]);
				wsaaSubprogFound = "Y";
			}
		}
		if (isNE(wsaaSubprogFound, "Y")) {
			drylogrec.statuz.set("E302");
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(Varcom.readr);
			itdmIO.setStatuz(Varcom.mrnf);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5671);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(drypDryprcRecInner.drypBatctrcde);
			stringVariable1.addExpression(dry5137Dto.getCrtable());
			stringVariable1.setStringInto(itemIO.getItemitem());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Set function to non-spaces to prevent generic processing */
		/* creating UTRNS for U/Linked components. */
		isuallrec.function.set("ACTIN");
		isuallrec.company.set(drypDryprcRecInner.drypCompany);
		isuallrec.chdrnum.set(drypDryprcRecInner.drypEntity);
		isuallrec.life.set(covrIO.getLife());
		isuallrec.coverage.set(covrIO.getCoverage());
		isuallrec.rider.set(covrIO.getRider());
		isuallrec.planSuffix.set(covrIO.getPlanSuffix());
		isuallrec.oldcovr.set(dry5137Dto.getCoverage());
		isuallrec.oldrider.set(dry5137Dto.getRider());
		isuallrec.freqFactor.set(1);
		isuallrec.batchkey.set(drypDryprcRecInner.drypBatchKey);
		isuallrec.transactionDate.set(wsaaTransDate);
		isuallrec.transactionTime.set(wsaaTransTime);
		isuallrec.user.set(drypDryprcRecInner.drypUser.toInt());
		varcom.vrcmTranid.set(SPACES);
		isuallrec.termid.set(varcom.vrcmTermid);
		isuallrec.effdate.set(dry5137Dto.getCrrcd());
		isuallrec.runDate.set(drypDryprcRecInner.drypRunDate);
		isuallrec.newTranno.set(wsaaTranno);
		isuallrec.covrSingp.set(ZERO);
		isuallrec.covrInstprem.set(covrIO.getInstprem());
		isuallrec.convertUnlt.set(SPACES);
		isuallrec.language.set(drypDryprcRecInner.drypLanguage);
		if (isGT(chdrlifRec.getPolinc(), 1)) {
			isuallrec.convertUnlt.set("Y");
		}
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 4)); wsaaSub1.add(1)) {
			subCall3750();
		}

	}

	protected void subCall3750() {
		/* SUB-CALL */
		isuallrec.statuz.set(Varcom.oK);
		if (isNE(wsaaSubprog[wsaaSub1.toInt()], SPACES)) {
			callProgram(wsaaSubprog[wsaaSub1.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(isuallrec.statuz);
			drylogrec.params.set(isuallrec.isuallRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void searchT54473800() {
		/* START */
		wsaaT5447Ix.set(1);
		searchlabel1: {
			for (; isLT(wsaaT5447Ix, wsaaT5447Rec.length); wsaaT5447Ix.add(1)) {
				if (isEQ(wsaaT5447ChdrlifCnttype[wsaaT5447Ix.toInt()], chdrlifRec.getCnttype())) {
					bypassReassurance.setTrue();
					break searchlabel1;
				}
			}
			processReassurance.setTrue();
		}
		/* EXIT */
	}

	protected void processReassurance5000(Covrpf covrIO, Dry5137Dto dry5137Dto) {
		rasaltrrec.statuz.set(Varcom.oK);
		rasaltrrec.function.set("INCR");
		rasaltrrec.chdrcoy.set(chdrlifRec.getChdrcoy());
		rasaltrrec.chdrnum.set(chdrlifRec.getChdrnum());
		rasaltrrec.life.set(covrIO.getLife());
		rasaltrrec.jlife.set(covrIO.getJlife());
		rasaltrrec.coverage.set(covrIO.getCoverage());
		rasaltrrec.rider.set(covrIO.getRider());
		rasaltrrec.planSuffix.set(covrIO.getPlanSuffix());
		rasaltrrec.polsum.set(chdrlifRec.getPolsum());
		rasaltrrec.effdate.set(drypDryprcRecInner.drypRunDate);
		rasaltrrec.batckey.set(drypDryprcRecInner.drypBatchKey);
		rasaltrrec.tranno.set(wsaaTranno);
		rasaltrrec.l1Clntnum.set(wsaaL1Clntnum);
		rasaltrrec.l2Clntnum.set(wsaaL2Clntnum);
		rasaltrrec.fsuco.set(drypDryprcRecInner.drypFsuCompany);
		rasaltrrec.crtable.set(covrIO.getCrtable());
		rasaltrrec.cnttype.set(chdrlifRec.getCnttype());
		rasaltrrec.cntcurr.set(chdrlifRec.getCntcurr());
		rasaltrrec.language.set(drypDryprcRecInner.drypLanguage);
		rasaltrrec.crrcd.set(covrIO.getCrrcd());
		callProgram(Rasaltr.class, rasaltrrec.rasaltrRec);
		if (isNE(rasaltrrec.statuz, Varcom.oK) && isNE(rasaltrrec.statuz, "FACL") && isNE(rasaltrrec.statuz, "TRET")) {
			drylogrec.statuz.set(rasaltrrec.statuz);
			drylogrec.params.set(rasaltrrec.rasaltrRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		List<String> chdrList = new ArrayList<>();
		chdrList.add(dry5137Dto.getChdrnum());
		racdlnbMap = racdpfDAO.searchRacdlnbRecord(chdrList);
		if (racdlnbMap != null && racdlnbMap.containsKey(dry5137Dto.getChdrnum())) {
			for (Racdpf r : racdlnbMap.get(dry5137Dto.getChdrnum())) {
				if (dry5137Dto.getChdrcoy().equals(r.getChdrcoy())) {
					if (isEQ(r.getRetype(), "F")
							// ILIFE-1296 Starts
							&& isNE(r.getRasnum(), SPACES) && isNE(r.getRngmnt(), SPACES)
					// ILIFE-1296 Ends
					) {
						reassuranceDetail5700(dry5137Dto, r);
						if (deleteRacdList == null) {
							deleteRacdList = new ArrayList<>();
						}
						deleteRacdList.add(r);
					} else {
						callActvres5460(covrIO, dry5137Dto);
					}
				}
			}
		}
	}

	protected void callActvres5460(Covrpf covrIO, Dry5137Dto dry5137Dto) {
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(chdrlifRec.getChdrcoy());
		actvresrec.chdrnum.set(chdrlifRec.getChdrnum());
		actvresrec.cnttype.set(chdrlifRec.getCnttype());
		actvresrec.currency.set(chdrlifRec.getCntcurr());
		actvresrec.tranno.set(wsaaTranno);
		actvresrec.life.set(dry5137Dto.getLife());
		actvresrec.coverage.set(dry5137Dto.getCoverage());
		actvresrec.rider.set(dry5137Dto.getRider());
		actvresrec.planSuffix.set(dry5137Dto.getPlnsfx());
		actvresrec.crtable.set(dry5137Dto.getCrtable());
		actvresrec.effdate.set(drypDryprcRecInner.drypRunDate);
		actvresrec.clntcoy.set(drypDryprcRecInner.drypFsuCompany);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.jlife.set(dry5137Dto.getJlife());
		actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(dry5137Dto.getNewsum());
		actvresrec.crrcd.set(covrIO.getCrrcd());
		actvresrec.language.set(drypDryprcRecInner.drypLanguage);
		actvresrec.function.set("ACT8");
		actvresrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(actvresrec.statuz);
			drylogrec.params.set(actvresrec.actvresRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void nextComponent6000(Dry5137Dto dry5137Dto) {
		if (covrMap != null && covrMap.containsKey(dry5137Dto.getChdrnum())) {
			for (Covrpf i : covrMap.get(dry5137Dto.getChdrnum())) {
				if (dry5137Dto.getChdrcoy().equals(i.getChdrcoy()) && dry5137Dto.getLife().equals(i.getLife())
						&& "00".equals(i.getJlife()) && isGTE(i.getCoverage(), dry5137Dto.getCoverage())
						&& "00".equals(i.getRider())) {
					if (isGT(i.getCoverage(), wsaaNextCovno)) {
						wsaaNextCovno.set(i.getCoverage());
					}
				}
			}
		}
	}

	protected void getLifergpDetails7000(Covrpf covrIO) {
		if (lifelnbMap != null && lifelnbMap.containsKey(covrIO.getChdrnum())) {
			boolean foundFlag = true;
			for (Lifepf l : lifelnbMap.get(covrIO.getChdrnum())) {
				if ("1".equals(l.getValidflag()) && l.getChdrcoy().equals(covrIO.getChdrcoy())
						&& l.getLife().equals(covrIO.getLife()) && "00".equals(l.getJlife())) {
					calculateAnb7500(l, covrIO);
					foundFlag = false;
					break;
				}
			}
			if (foundFlag) {
				drylogrec.params.set(covrIO.getChdrnum());
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			for (Lifepf l : lifelnbMap.get(covrIO.getChdrnum())) {
				if ("1".equals(l.getValidflag()) && l.getChdrcoy().equals(covrIO.getChdrcoy())
						&& l.getLife().equals(covrIO.getLife()) && "01".equals(l.getJlife())) {
					calculateAnb7500(l, covrIO);
					break;
				}
			}
		}
	}

	protected void calculateAnb7500(Lifepf lifergpIO, Covrpf covrIO) {
		if (isEQ(lifergpIO.getCltdob(), ZERO)) {
			covrIO.setCpiDate(0);
			return;
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCB");
		agecalcrec.cnttype.set(chdrlifRec.getCnttype());
		agecalcrec.intDate1.set(lifergpIO.getCltdob());
		agecalcrec.intDate2.set(covrIO.getCpiDate());
		agecalcrec.company.set(drypDryprcRecInner.drypFsuCompany);
		agecalcrec.language.set(drypDryprcRecInner.drypLanguage);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(agecalcrec.statuz);
			drylogrec.params.set(agecalcrec.agecalcRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

	protected Chdrpf chdrlifUpdate8000(Dry5137Dto dry5137Dto) {
		Chdrpf chdrlifIO = null;
		if (chdrlifMap != null && chdrlifMap.containsKey(wsaaChdrnum.toString())) {
			for (Chdrpf c : chdrlifMap.get(wsaaChdrnum.toString())) {
				if (c.getChdrcoy().toString().equals(dry5137Dto.getChdrcoy())) {
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			drylogrec.params.set(wsaaChdrnum.toString());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		if (updateChdrlifList == null) {
			updateChdrlifList = new ArrayList<>();
		}

		chdrlifIO.setValidflag('2');
		chdrlifIO.setCurrto(wsaaOldCpiDate.toInt());
		updateChdrlifList.add(chdrlifIO);

		if (insertChdrlifList == null) {
			insertChdrlifList = new ArrayList<>();
		}

		Chdrpf insertChdr = new Chdrpf(chdrlifIO);
		insertChdr.setUniqueNumber(chdrlifIO.getUniqueNumber());
		insertChdr.setTranno(wsaaTranno.toInt());
		insertChdr.setValidflag('1');
		insertChdr.setCurrfrom(wsaaOldCpiDate.toInt());
		insertChdr.setCurrto(varcom.vrcmMaxDate.toInt());
		if (!prmhldtrad || !lapseReinstated)
			insertChdr.setSinstamt01(insertChdr.getSinstamt01().add(wsaaInstprem.getbigdata()));
		insertChdr.setSinstamt06(
				add(add(add(insertChdr.getSinstamt01(), insertChdr.getSinstamt02()), insertChdr.getSinstamt03()),
						insertChdr.getSinstamt04()).getbigdata());

		insertChdrlifList.add(insertChdr);

		Payrpf payrIO = null;
		if (payrMap != null && payrMap.containsKey(wsaaChdrnum.toString())) {
			for (Payrpf p : payrMap.get(wsaaChdrnum.toString())) {
				if (dry5137Dto.getChdrcoy().equals(p.getChdrcoy()) && 1 == p.getPayrseqno()) {
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			drylogrec.params.set(wsaaChdrnum);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		} // IJTI-462 START
		else {
			payrIO.setValidflag("2");
			if (updatePayrList == null) {
				updatePayrList = new ArrayList<>();
			}
			updatePayrList.add(payrIO);
			Payrpf payrInsert = new Payrpf(payrIO);
			payrInsert.setTranno(chdrlifIO.getTranno());
			payrInsert.setValidflag("1");
			if (!prmhldtrad || !lapseReinstated) {// ILIFE-8509
				payrInsert.setSinstamt01(payrInsert.getSinstamt01().add(wsaaInstprem.getbigdata()));
				payrInsert.setTranno(wsaaTranno.toInt());
			}
			payrInsert.setSinstamt06(
					add(add(add(payrInsert.getSinstamt01(), payrInsert.getSinstamt02()), payrInsert.getSinstamt03()),
							payrInsert.getSinstamt04()).getbigdata());
			payrInsert.setEffdate(wsaaOldCpiDate.toInt());
			if (insertPayrList == null) {
				insertPayrList = new ArrayList<>();
			}
			insertPayrList.add(payrInsert);
			ct03Value++;

		} // IJTI-462 END
		return insertChdr;
	}

	protected void ptrnWrite8100(Chdrpf chdrlifIO) {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		if (isFoundPro) {
			ptrnIO.setTranno(chdrTranno.toInt());
			compute(chdrTranno, 0).set(add(chdrTranno, 1));
		} else {
			ptrnIO.setTranno(chdrlifIO.getTranno());
		}
		ptrnIO.setTrdt(drypDryprcRecInner.drypRunDate.toInt());
		ptrnIO.setTrtm(wsaaTransTime.toInt());
		if (isFoundPro) {
			ptrnIO.setPtrneff(wsaaLastCpiDate.toInt());
		} else {
			ptrnIO.setPtrneff(wsaaOldCpiDate.toInt());
		}
		ptrnIO.setUserT(drypDryprcRecInner.drypUser.toInt());
		varcom.vrcmTranid.set(SPACES);
		ptrnIO.setTermid(varcom.vrcmTermid.toString());

		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx.toString());
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate.toInt());
		if (insertPtrnList == null) {
			insertPtrnList = new ArrayList<>();
		}
		insertPtrnList.add(ptrnIO);
		ct04Value++;

	}

	protected void a100ReadT5687(Dry5137Dto dry5137Dto) {
		boolean foundFlag = false;
		if (t5687Map == null) {
			t5687Map = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString(), "T5687");
		}
		if (t5687Map != null && t5687Map.containsKey(dry5137Dto.getCrtable())) {
			for (Itempf i : t5687Map.get(dry5137Dto.getCrtable())) {
				if (i.getItmfrm().compareTo(drypDryprcRecInner.drypRunDate.getbigdata()) <= 0) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(i.getGenarea()));
					foundFlag = true;
					break;
				}
			}
		}

		if (!foundFlag) {
			itdmIO.setItemitem(dry5137Dto.getCrtable());

			drylogrec.statuz.set("F294");
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	/*
	 * Class transformed from Data Structure WSAA-T6658-ARRAY--INNER
	 */
	private static final class WsaaT6658ArrayInner {

		public WsaaT6658ArrayInner() {
		};

		/* WSAA-T6658-ARRAY */
		private FixedLengthStringData[] wsaaT6658Rec = FLSInittedArray(200, 78);
		private PackedDecimalData[] wsaaT6658Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 0);
		private PackedDecimalData[] wsaaT6658Itmto = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 5);
		private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 10);
		private FixedLengthStringData[] wsaaT6658Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0, HIVALUES);
		private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(64, wsaaT6658Rec, 14);
		private FixedLengthStringData[] wsaaT6658Addexist = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 0);
		private FixedLengthStringData[] wsaaT6658Addnew = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 1);
		private ZonedDecimalData[] wsaaT6658MaxAge = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 2);
		private FixedLengthStringData[] wsaaT6658Billfreq = FLSDArrayPartOfArrayStructure(2, wsaaT6658Data, 5);
		private FixedLengthStringData[] wsaaT6658Comind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 7);
		private FixedLengthStringData[] wsaaT6658CompoundInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 8);
		private ZonedDecimalData[] wsaaT6658Fixdtrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 9);
		private FixedLengthStringData[] wsaaT6658Manopt = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 12);
		private ZonedDecimalData[] wsaaT6658MaxPcnt = ZDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 13);
		private ZonedDecimalData[] wsaaT6658MaxRefusals = ZDArrayPartOfArrayStructure(2, 0, wsaaT6658Data, 18);
		private ZonedDecimalData[] wsaaT6658Minctrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 20);
		private ZonedDecimalData[] wsaaT6658Minpcnt = ZDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 23);
		private FixedLengthStringData[] wsaaT6658Nocommind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 28);
		private FixedLengthStringData[] wsaaT6658Nostatin = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 29);
		private FixedLengthStringData[] wsaaT6658Optind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 30);
		private ZonedDecimalData[] wsaaT6658RefusalPeriod = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 31);
		private FixedLengthStringData[] wsaaT6658SimpleInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 34);
		private FixedLengthStringData[] wsaaT6658Statind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 35);
		private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 36);
		private FixedLengthStringData[] wsaaT6658Premsubr = FLSDArrayPartOfArrayStructure(8, wsaaT6658Data, 46);
		private FixedLengthStringData[] wsaaT6658Trevsub = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 54);
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	private void updateDates() {
		if(updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
			Chdrpf chdrpf = updateChdrlifList.get(0);
			if(chdrpf != null) {
				drypDryprcRecInner.drypBillcd.set(chdrpf.getBillcd());
				drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
				drypDryprcRecInner.drypBillfreq.set(chdrpf.getBillfreq());
				drypDryprcRecInner.drypBillchnl.set(chdrpf.getBillchnl());
				drypDryprcRecInner.drypBtdate.set(chdrpf.getBtdate());
				drypDryprcRecInner.drypPtdate.set(chdrpf.getPtdate());
				drypDryprcRecInner.drypTranno.set(chdrpf.getTranno());
				drypDryprcRecInner.drypCnttype.set(chdrpf.getCnttype());
				drypDryprcRecInner.drypOccdate.set(chdrpf.getOccdate());
				drypDryprcRecInner.drypStatcode.set(chdrpf.getStatcode());
				drypDryprcRecInner.drypPstatcode.set(chdrpf.getPstcde());
			}
		}
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
	}
	
	protected void reassuranceDetail5700(Dry5137Dto dry5137Dto,Racdpf racdlnbIO)
	{
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(racdlnbIO.getCmdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		//ILPI-121
		dryrDryrptRecInner.dryrReportName.set("R5138");
		dryrDryrptRecInner.r5138Cmdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5138Crtable.set(rasaltrrec.crtable);
		dryrDryrptRecInner.r5138Chdrnum.set(dry5137Dto.getChdrnum());
		dryrDryrptRecInner.r5138Life.set(dry5137Dto.getLife());
		dryrDryrptRecInner.r5138Coverage.set(dry5137Dto.getCoverage());
		dryrDryrptRecInner.r5138Rider.set(dry5137Dto.getRider());
		dryrDryrptRecInner.r5138Plnsfx.set(dry5137Dto.getPlnsfx());
		dryrDryrptRecInner.r5138Rasnum.set(racdlnbIO.getRasnum());
		dryrDryrptRecInner.r5138Rngmnt.set(racdlnbIO.getRngmnt());
		dryrDryrptRecInner.r5138Raamount.set(racdlnbIO.getRaAmount());
		e000ReportRecords();
		ct09Value++;
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		public FormatsInner() {
			// Default Constructor
		};

		private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {
		// ILPI-97 starts
		public DrypDryprcRecInner() {
		};

		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}

}
