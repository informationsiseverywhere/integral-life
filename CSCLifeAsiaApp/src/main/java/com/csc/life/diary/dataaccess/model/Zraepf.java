package com.csc.life.diary.dataaccess.model;

import com.csc.life.anticipatedendowment.dataaccess.model.Bd5ksDTO;
import java.math.BigDecimal;
import com.csc.life.anticipatedendowment.dataaccess.model.Bd5krDTO;

public class Zraepf {

	private long unique_number;
	
	private String	chdrcoy;
	private String	chdrnum;
	private String	life;
	private String	coverage;
	private String	rider;
	private int	plnsfx;
	private String	validflag;
	private int	tranno;
	private int	currfrom;
	private int	currto;
	private int	zrduedte01;
	private int	zrduedte02;
	private int	zrduedte03;
	private int	zrduedte04;
	private int	zrduedte05;
	private int	zrduedte06;
	private int	zrduedte07;
	private int	zrduedte08;
	private int	prcnt01;
	private int	prcnt02;
	private int	prcnt03;
	private int	prcnt04;
	private int	prcnt05;
	private int	prcnt06;
	private int	prcnt07;
	private int	prcnt08;
	private int	paydte01;
	private int	paydte02;
	private int	paydte03;
	private int	paydte04;
	private int	paydte05;
	private int	paydte06;
	private int	paydte07;
	private int	paydte08;
	private int	paid01;
	private int	paid02;
	private int	paid03;
	private int	paid04;
	private int	paid05;
	private int	paid06;
	private int	paid07;
	private int	paid08;
	private String	paymmeth01;
	private String	paymmeth02;
	private String	paymmeth03;
	private String	paymmeth04;
	private String	paymmeth05;
	private String	paymmeth06;
	private String	paymmeth07;
	private String	paymmeth08;
	private String	zrpayopt01;
	private String	zrpayopt02;
	private String	zrpayopt03;
	private String	zrpayopt04;
	private String	zrpayopt05;
	private String	zrpayopt06;
	private String	zrpayopt07;
	private String	zrpayopt08;
	private int	totamnt;
	private int	npaydate;
	private String	payclt;
	private String	bankkey;
	private String	bankacckey;
	private String	paycurr;
	private String	usrprf;
	private String	jobnm;
	private String 	datime;
	private Bd5ksDTO bd5ksDTO;
	private Bd5krDTO bd5krDTO;
	private String	flag;
	private BigDecimal apcaplamt;
	private BigDecimal apintamt;
	private Integer aplstcapdate;
	private Integer apnxtcapdate;
	private Integer aplstintbdte;
	private Integer apnxtintbdte;
	private String wdrgpymop;
	private String wdbankkey;
	private String wdbankacckey;
	
	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public int getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(int plnsfx) {
		this.plnsfx = plnsfx;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public int getZrduedte01() {
		return zrduedte01;
	}
	public void setZrduedte01(int zrduedte01) {
		this.zrduedte01 = zrduedte01;
	}
	public int getZrduedte02() {
		return zrduedte02;
	}
	public void setZrduedte02(int zrduedte02) {
		this.zrduedte02 = zrduedte02;
	}
	public int getZrduedte03() {
		return zrduedte03;
	}
	public void setZrduedte03(int zrduedte03) {
		this.zrduedte03 = zrduedte03;
	}
	public int getZrduedte04() {
		return zrduedte04;
	}
	public void setZrduedte04(int zrduedte04) {
		this.zrduedte04 = zrduedte04;
	}
	public int getZrduedte05() {
		return zrduedte05;
	}
	public void setZrduedte05(int zrduedte05) {
		this.zrduedte05 = zrduedte05;
	}
	public int getZrduedte06() {
		return zrduedte06;
	}
	public void setZrduedte06(int zrduedte06) {
		this.zrduedte06 = zrduedte06;
	}
	public int getZrduedte07() {
		return zrduedte07;
	}
	public void setZrduedte07(int zrduedte07) {
		this.zrduedte07 = zrduedte07;
	}
	public int getZrduedte08() {
		return zrduedte08;
	}
	public void setZrduedte08(int zrduedte08) {
		this.zrduedte08 = zrduedte08;
	}
	public int getPrcnt01() {
		return prcnt01;
	}
	public void setPrcnt01(int prcnt01) {
		this.prcnt01 = prcnt01;
	}
	public int getPrcnt02() {
		return prcnt02;
	}
	public void setPrcnt02(int prcnt02) {
		this.prcnt02 = prcnt02;
	}
	public int getPrcnt03() {
		return prcnt03;
	}
	public void setPrcnt03(int prcnt03) {
		this.prcnt03 = prcnt03;
	}
	public int getPrcnt04() {
		return prcnt04;
	}
	public void setPrcnt04(int prcnt04) {
		this.prcnt04 = prcnt04;
	}
	public int getPrcnt05() {
		return prcnt05;
	}
	public void setPrcnt05(int prcnt05) {
		this.prcnt05 = prcnt05;
	}
	public int getPrcnt06() {
		return prcnt06;
	}
	public void setPrcnt06(int prcnt06) {
		this.prcnt06 = prcnt06;
	}
	public int getPrcnt07() {
		return prcnt07;
	}
	public void setPrcnt07(int prcnt07) {
		this.prcnt07 = prcnt07;
	}
	public int getPrcnt08() {
		return prcnt08;
	}
	public void setPrcnt08(int prcnt08) {
		this.prcnt08 = prcnt08;
	}
	public int getPaydte01() {
		return paydte01;
	}
	public void setPaydte01(int paydte01) {
		this.paydte01 = paydte01;
	}
	public int getPaydte02() {
		return paydte02;
	}
	public void setPaydte02(int paydte02) {
		this.paydte02 = paydte02;
	}
	public int getPaydte03() {
		return paydte03;
	}
	public void setPaydte03(int paydte03) {
		this.paydte03 = paydte03;
	}
	public int getPaydte04() {
		return paydte04;
	}
	public void setPaydte04(int paydte04) {
		this.paydte04 = paydte04;
	}
	public int getPaydte05() {
		return paydte05;
	}
	public void setPaydte05(int paydte05) {
		this.paydte05 = paydte05;
	}
	public int getPaydte06() {
		return paydte06;
	}
	public void setPaydte06(int paydte06) {
		this.paydte06 = paydte06;
	}
	public int getPaydte07() {
		return paydte07;
	}
	public void setPaydte07(int paydte07) {
		this.paydte07 = paydte07;
	}
	public int getPaydte08() {
		return paydte08;
	}
	public void setPaydte08(int paydte08) {
		this.paydte08 = paydte08;
	}
	public int getPaid01() {
		return paid01;
	}
	public void setPaid01(int paid01) {
		this.paid01 = paid01;
	}
	public int getPaid02() {
		return paid02;
	}
	public void setPaid02(int paid02) {
		this.paid02 = paid02;
	}
	public int getPaid03() {
		return paid03;
	}
	public void setPaid03(int paid03) {
		this.paid03 = paid03;
	}
	public int getPaid04() {
		return paid04;
	}
	public void setPaid04(int paid04) {
		this.paid04 = paid04;
	}
	public int getPaid05() {
		return paid05;
	}
	public void setPaid05(int paid05) {
		this.paid05 = paid05;
	}
	public int getPaid06() {
		return paid06;
	}
	public void setPaid06(int paid06) {
		this.paid06 = paid06;
	}
	public int getPaid07() {
		return paid07;
	}
	public void setPaid07(int paid07) {
		this.paid07 = paid07;
	}
	public int getPaid08() {
		return paid08;
	}
	public void setPaid08(int paid08) {
		this.paid08 = paid08;
	}
	public String getPaymmeth01() {
		return paymmeth01;
	}
	public void setPaymmeth01(String paymmeth01) {
		this.paymmeth01 = paymmeth01;
	}
	public String getPaymmeth02() {
		return paymmeth02;
	}
	public void setPaymmeth02(String paymmeth02) {
		this.paymmeth02 = paymmeth02;
	}
	public String getPaymmeth03() {
		return paymmeth03;
	}
	public void setPaymmeth03(String paymmeth03) {
		this.paymmeth03 = paymmeth03;
	}
	public String getPaymmeth04() {
		return paymmeth04;
	}
	public void setPaymmeth04(String paymmeth04) {
		this.paymmeth04 = paymmeth04;
	}
	public String getPaymmeth05() {
		return paymmeth05;
	}
	public void setPaymmeth05(String paymmeth05) {
		this.paymmeth05 = paymmeth05;
	}
	public String getPaymmeth06() {
		return paymmeth06;
	}
	public void setPaymmeth06(String paymmeth06) {
		this.paymmeth06 = paymmeth06;
	}
	public String getPaymmeth07() {
		return paymmeth07;
	}
	public void setPaymmeth07(String paymmeth07) {
		this.paymmeth07 = paymmeth07;
	}
	public String getPaymmeth08() {
		return paymmeth08;
	}
	public void setPaymmeth08(String paymmeth08) {
		this.paymmeth08 = paymmeth08;
	}
	public String getZrpayopt01() {
		return zrpayopt01;
	}
	public void setZrpayopt01(String zrpayopt01) {
		this.zrpayopt01 = zrpayopt01;
	}
	public String getZrpayopt02() {
		return zrpayopt02;
	}
	public void setZrpayopt02(String zrpayopt02) {
		this.zrpayopt02 = zrpayopt02;
	}
	public String getZrpayopt03() {
		return zrpayopt03;
	}
	public void setZrpayopt03(String zrpayopt03) {
		this.zrpayopt03 = zrpayopt03;
	}
	public String getZrpayopt04() {
		return zrpayopt04;
	}
	public void setZrpayopt04(String zrpayopt04) {
		this.zrpayopt04 = zrpayopt04;
	}
	public String getZrpayopt05() {
		return zrpayopt05;
	}
	public void setZrpayopt05(String zrpayopt05) {
		this.zrpayopt05 = zrpayopt05;
	}
	public String getZrpayopt06() {
		return zrpayopt06;
	}
	public void setZrpayopt06(String zrpayopt06) {
		this.zrpayopt06 = zrpayopt06;
	}
	public String getZrpayopt07() {
		return zrpayopt07;
	}
	public void setZrpayopt07(String zrpayopt07) {
		this.zrpayopt07 = zrpayopt07;
	}
	public String getZrpayopt08() {
		return zrpayopt08;
	}
	public void setZrpayopt08(String zrpayopt08) {
		this.zrpayopt08 = zrpayopt08;
	}
	public int getTotamnt() {
		return totamnt;
	}
	public void setTotamnt(int totamnt) {
		this.totamnt = totamnt;
	}
	public int getNpaydate() {
		return npaydate;
	}
	public void setNpaydate(int npaydate) {
		this.npaydate = npaydate;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public String getPaycurr() {
		return paycurr;
	}
	public void setPaycurr(String paycurr) {
		this.paycurr = paycurr;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	
	public Bd5ksDTO getBd5ksDTO() {
		return bd5ksDTO;
	}

	public void setBd5ksDTO(Bd5ksDTO bd5ksDTO) {
		this.bd5ksDTO = bd5ksDTO;
	}

	public Bd5krDTO getBd5krDTO() {
		return bd5krDTO;
	}

	public void setBd5krDTO(Bd5krDTO bd5krDTO) {
		this.bd5krDTO = bd5krDTO;
	}
	
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	
	public BigDecimal getApcaplamt(){
		return this.apcaplamt;
	}
	public BigDecimal getApintamt() {
		return apintamt;
	}
	public Integer getAplstcapdate(){
		return this.aplstcapdate;
	}
	public Integer getApnxtcapdate(){
		return this.apnxtcapdate;
	}
	public Integer getAplstintbdte(){
		return this.aplstintbdte;
	}
	public Integer getApnxtintbdte(){
		return this.apnxtintbdte;
	}
	
	public void setApcaplamt( BigDecimal apcaplamt ){
		 this.apcaplamt = apcaplamt;
	}
	

	public void setApintamt(BigDecimal apintamt) {
		this.apintamt = apintamt;
	}

	public void setAplstcapdate( Integer aplstcapdate ){
		 this.aplstcapdate = aplstcapdate;
	}
	public void setApnxtcapdate( Integer apnxtcapdate ){
		 this.apnxtcapdate = apnxtcapdate;
	}
	public void setAplstintbdte( Integer aplstintbdte ){
		 this.aplstintbdte = aplstintbdte;
	}
	public void setApnxtintbdte( Integer apnxtintbdte ){
		 this.apnxtintbdte = apnxtintbdte;
	}
	public String getWdrgpymop() {
		return wdrgpymop;
	}
	public void setWdrgpymop(String wdrgpymop) {
		this.wdrgpymop = wdrgpymop;
	}
	public String getWdbankkey() {
		return wdbankkey;
	}
	public void setWdbankkey(String wdbankkey) {
		this.wdbankkey = wdbankkey;
	}
	public String getWdbankacckey() {
		return wdbankacckey;
	}
	public void setWdbankacckey(String wdbankacckey) {
		this.wdbankacckey = wdbankacckey;
	}
	public Zraepf(Zraepf zraepf) {
		this.unique_number = zraepf.unique_number;
		this.chdrcoy = zraepf.chdrcoy;
		this.chdrnum = zraepf.chdrnum;
		this.life = zraepf.life;
		this.coverage = zraepf.coverage;
		this.rider = zraepf.rider;
		this.plnsfx = zraepf.plnsfx;
		this.validflag = zraepf.validflag;
		this.tranno = zraepf.tranno;
		this.currfrom = zraepf.currfrom;
		this.currto = zraepf.currto;
		this.zrduedte01 = zraepf.zrduedte01;
		this.zrduedte02 = zraepf.zrduedte02;
		this.zrduedte03 = zraepf.zrduedte03;
		this.zrduedte04 = zraepf.zrduedte04;
		this.zrduedte05 = zraepf.zrduedte05;
		this.zrduedte06 = zraepf.zrduedte06;
		this.zrduedte07 = zraepf.zrduedte07;
		this.zrduedte08 = zraepf.zrduedte08;
		this.prcnt01 = zraepf.prcnt01;
		this.prcnt02 = zraepf.prcnt02;
		this.prcnt03 = zraepf.prcnt03;
		this.prcnt04 = zraepf.prcnt04;
		this.prcnt05 = zraepf.prcnt05;
		this.prcnt06 = zraepf.prcnt06;
		this.prcnt07 = zraepf.prcnt07;
		this.prcnt08 = zraepf.prcnt08;
		this.paydte01 = zraepf.paydte01;
		this.paydte02 = zraepf.paydte02;
		this.paydte03 = zraepf.paydte03;
		this.paydte04 = zraepf.paydte04;
		this.paydte05 = zraepf.paydte05;
		this.paydte06 = zraepf.paydte06;
		this.paydte07 = zraepf.paydte07;
		this.paydte08 = zraepf.paydte08;
		this.paid01 = zraepf.paid01;
		this.paid02 = zraepf.paid02;
		this.paid03 = zraepf.paid03;
		this.paid04 = zraepf.paid04;
		this.paid05 = zraepf.paid05;
		this.paid06 = zraepf.paid06;
		this.paid07 = zraepf.paid07;
		this.paid08 = zraepf.paid08;
		this.paymmeth01 = zraepf.paymmeth01;
		this.paymmeth02 = zraepf.paymmeth02;
		this.paymmeth03 = zraepf.paymmeth03;
		this.paymmeth04 = zraepf.paymmeth04;
		this.paymmeth05 = zraepf.paymmeth05;
		this.paymmeth06 = zraepf.paymmeth06;
		this.paymmeth07 = zraepf.paymmeth07;
		this.paymmeth08 = zraepf.paymmeth08;
		this.zrpayopt01 = zraepf.zrpayopt01;
		this.zrpayopt02 = zraepf.zrpayopt02;
		this.zrpayopt03 = zraepf.zrpayopt03;
		this.zrpayopt04 = zraepf.zrpayopt04;
		this.zrpayopt05 = zraepf.zrpayopt05;
		this.zrpayopt06 = zraepf.zrpayopt06;
		this.zrpayopt07 = zraepf.zrpayopt07;
		this.zrpayopt08 = zraepf.zrpayopt08;
		this.totamnt = zraepf.totamnt;
		this.npaydate = zraepf.npaydate;
		this.payclt = zraepf.payclt;
		this.bankkey = zraepf.bankkey;
		this.bankacckey = zraepf.bankacckey;
		this.paycurr = zraepf.paycurr;
		this.usrprf = zraepf.usrprf;
		this.jobnm = zraepf.jobnm;
		this.datime = zraepf.datime;
		//this.bd5ksDTO = bd5ksDTO;
		//this.bd5krDTO = bd5krDTO;
		this.flag = zraepf.flag;
		this.apcaplamt = zraepf.apcaplamt;
		this.apintamt = zraepf.apintamt;
		this.aplstcapdate = zraepf.aplstcapdate;
		this.apnxtcapdate = zraepf.apnxtcapdate;
		this.aplstintbdte = zraepf.aplstintbdte;
		this.apnxtintbdte = zraepf.apnxtintbdte;
		this.wdrgpymop = zraepf.wdrgpymop;
		this.wdbankkey = zraepf.wdbankkey;
		this.wdbankacckey = zraepf.wdbankacckey;
	}
	
	public Zraepf(long unique_number, String chdrcoy, String chdrnum, String life, String coverage, String rider,
			int plnsfx, String validflag, int tranno, int currfrom, int currto, int zrduedte01, int zrduedte02,
			int zrduedte03, int zrduedte04, int zrduedte05, int zrduedte06, int zrduedte07, int zrduedte08, int prcnt01,
			int prcnt02, int prcnt03, int prcnt04, int prcnt05, int prcnt06, int prcnt07, int prcnt08, int paydte01,
			int paydte02, int paydte03, int paydte04, int paydte05, int paydte06, int paydte07, int paydte08,
			int paid01, int paid02, int paid03, int paid04, int paid05, int paid06, int paid07, int paid08,
			String paymmeth01, String paymmeth02, String paymmeth03, String paymmeth04, String paymmeth05,
			String paymmeth06, String paymmeth07, String paymmeth08, String zrpayopt01, String zrpayopt02,
			String zrpayopt03, String zrpayopt04, String zrpayopt05, String zrpayopt06, String zrpayopt07,
			String zrpayopt08, int totamnt, int npaydate, String payclt, String bankkey, String bankacckey,
			String paycurr, String usrprf, String jobnm, String datime,
			String flag, BigDecimal apcaplamt, BigDecimal apintamt, Integer aplstcapdate, Integer apnxtcapdate,
			Integer aplstintbdte, Integer apnxtintbdte, String wdrgpymop, String wdbankkey, String wdbankacckey) {
		super();
		this.unique_number = unique_number;
		this.chdrcoy = chdrcoy;
		this.chdrnum = chdrnum;
		this.life = life;
		this.coverage = coverage;
		this.rider = rider;
		this.plnsfx = plnsfx;
		this.validflag = validflag;
		this.tranno = tranno;
		this.currfrom = currfrom;
		this.currto = currto;
		this.zrduedte01 = zrduedte01;
		this.zrduedte02 = zrduedte02;
		this.zrduedte03 = zrduedte03;
		this.zrduedte04 = zrduedte04;
		this.zrduedte05 = zrduedte05;
		this.zrduedte06 = zrduedte06;
		this.zrduedte07 = zrduedte07;
		this.zrduedte08 = zrduedte08;
		this.prcnt01 = prcnt01;
		this.prcnt02 = prcnt02;
		this.prcnt03 = prcnt03;
		this.prcnt04 = prcnt04;
		this.prcnt05 = prcnt05;
		this.prcnt06 = prcnt06;
		this.prcnt07 = prcnt07;
		this.prcnt08 = prcnt08;
		this.paydte01 = paydte01;
		this.paydte02 = paydte02;
		this.paydte03 = paydte03;
		this.paydte04 = paydte04;
		this.paydte05 = paydte05;
		this.paydte06 = paydte06;
		this.paydte07 = paydte07;
		this.paydte08 = paydte08;
		this.paid01 = paid01;
		this.paid02 = paid02;
		this.paid03 = paid03;
		this.paid04 = paid04;
		this.paid05 = paid05;
		this.paid06 = paid06;
		this.paid07 = paid07;
		this.paid08 = paid08;
		this.paymmeth01 = paymmeth01;
		this.paymmeth02 = paymmeth02;
		this.paymmeth03 = paymmeth03;
		this.paymmeth04 = paymmeth04;
		this.paymmeth05 = paymmeth05;
		this.paymmeth06 = paymmeth06;
		this.paymmeth07 = paymmeth07;
		this.paymmeth08 = paymmeth08;
		this.zrpayopt01 = zrpayopt01;
		this.zrpayopt02 = zrpayopt02;
		this.zrpayopt03 = zrpayopt03;
		this.zrpayopt04 = zrpayopt04;
		this.zrpayopt05 = zrpayopt05;
		this.zrpayopt06 = zrpayopt06;
		this.zrpayopt07 = zrpayopt07;
		this.zrpayopt08 = zrpayopt08;
		this.totamnt = totamnt;
		this.npaydate = npaydate;
		this.payclt = payclt;
		this.bankkey = bankkey;
		this.bankacckey = bankacckey;
		this.paycurr = paycurr;
		this.usrprf = usrprf;
		this.jobnm = jobnm;
		this.datime = datime;
		this.flag = flag;
		this.apcaplamt = apcaplamt;
		this.apintamt = apintamt;
		this.aplstcapdate = aplstcapdate;
		this.apnxtcapdate = apnxtcapdate;
		this.aplstintbdte = aplstintbdte;
		this.apnxtintbdte = apnxtintbdte;
		this.wdrgpymop = wdrgpymop;
		this.wdbankkey = wdbankkey;
		this.wdbankacckey = wdbankacckey;
	}
	
	public Zraepf() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public Zraepf clone() throws CloneNotSupportedException {
		Zraepf zraepfClone = new Zraepf(this);
		
		zraepfClone.unique_number = unique_number;
		zraepfClone.chdrcoy = chdrcoy;
		zraepfClone.chdrnum = chdrnum;
		zraepfClone.life = life;
		zraepfClone.coverage = coverage;
		zraepfClone.rider = rider;
		zraepfClone.plnsfx = plnsfx;
		zraepfClone.validflag = validflag;
		zraepfClone.tranno = tranno;
		zraepfClone.currfrom = currfrom;
		zraepfClone.currto = currto;
		zraepfClone.zrduedte01 = zrduedte01;
		zraepfClone.zrduedte02 = zrduedte02;
		zraepfClone.zrduedte03 = zrduedte03;
		zraepfClone.zrduedte04 = zrduedte04;
		zraepfClone.zrduedte05 = zrduedte05;
		zraepfClone.zrduedte06 = zrduedte06;
		zraepfClone.zrduedte07 = zrduedte07;
		zraepfClone.zrduedte08 = zrduedte08;
		zraepfClone.prcnt01 = prcnt01;
		zraepfClone.prcnt02 = prcnt02;
		zraepfClone.prcnt03 = prcnt03;
		zraepfClone.prcnt04 = prcnt04;
		zraepfClone.prcnt05 = prcnt05;
		zraepfClone.prcnt06 = prcnt06;
		zraepfClone.prcnt07 = prcnt07;
		zraepfClone.prcnt08 = prcnt08;
		zraepfClone.paydte01 = paydte01;
		zraepfClone.paydte02 = paydte02;
		zraepfClone.paydte03 = paydte03;
		zraepfClone.paydte04 = paydte04;
		zraepfClone.paydte05 = paydte05;
		zraepfClone.paydte06 = paydte06;
		zraepfClone.paydte07 = paydte07;
		zraepfClone.paydte08 = paydte08;
		zraepfClone.paid01 = paid01;
		zraepfClone.paid02 = paid02;
		zraepfClone.paid03 = paid03;
		zraepfClone.paid04 = paid04;
		zraepfClone.paid05 = paid05;
		zraepfClone.paid06 = paid06;
		zraepfClone.paid07 = paid07;
		zraepfClone.paid08 = paid08;
		zraepfClone.paymmeth01 = paymmeth01;
		zraepfClone.paymmeth02 = paymmeth02;
		zraepfClone.paymmeth03 = paymmeth03;
		zraepfClone.paymmeth04 = paymmeth04;
		zraepfClone.paymmeth05 = paymmeth05;
		zraepfClone.paymmeth06 = paymmeth06;
		zraepfClone.paymmeth07 = paymmeth07;
		zraepfClone.paymmeth08 = paymmeth08;
		zraepfClone.zrpayopt01 = zrpayopt01;
		zraepfClone.zrpayopt02 = zrpayopt02;
		zraepfClone.zrpayopt03 = zrpayopt03;
		zraepfClone.zrpayopt04 = zrpayopt04;
		zraepfClone.zrpayopt05 = zrpayopt05;
		zraepfClone.zrpayopt06 = zrpayopt06;
		zraepfClone.zrpayopt07 = zrpayopt07;
		zraepfClone.zrpayopt08 = zrpayopt08;
		zraepfClone.totamnt = totamnt;
		zraepfClone.npaydate = npaydate;
		zraepfClone.payclt = payclt;
		zraepfClone.bankkey = bankkey;
		zraepfClone.bankacckey = bankacckey;
		zraepfClone.paycurr = paycurr;
		zraepfClone.usrprf = usrprf;
		zraepfClone.jobnm = jobnm;
		zraepfClone.datime = datime;
		zraepfClone.flag = flag;
		zraepfClone.apcaplamt = apcaplamt;
		zraepfClone.apintamt = apintamt;
		zraepfClone.aplstcapdate = aplstcapdate;
		zraepfClone.apnxtcapdate = apnxtcapdate;
		zraepfClone.aplstintbdte = aplstintbdte;
		zraepfClone.apnxtintbdte = apnxtintbdte;
		zraepfClone.wdrgpymop = wdrgpymop;
		zraepfClone.wdbankkey = wdbankkey;
		zraepfClone.wdbankacckey = wdbankacckey;
		return zraepfClone;
	}

}
