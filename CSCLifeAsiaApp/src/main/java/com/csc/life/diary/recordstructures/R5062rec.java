package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:50
 * Description:
 * Copybook name: R5062REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5062rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(499);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(dataArea, 0);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(dataArea, 3);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(dataArea, 33).setUnsigned();
  	public FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(dataArea, 38);
  	public FixedLengthStringData errorLine = new FixedLengthStringData(1).isAPartOf(dataArea, 48);
  	public FixedLengthStringData filler = new FixedLengthStringData(450).isAPartOf(dataArea, 49, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(537);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 499);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(sortKey, 507, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5062     ");


	public void initialize() {
		COBOLFunctions.initialize(batctrcde);
		COBOLFunctions.initialize(trandesc);
		COBOLFunctions.initialize(tranno);
		COBOLFunctions.initialize(effdate);
		COBOLFunctions.initialize(errorLine);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		batctrcde.isAPartOf(baseString, true);
    		trandesc.isAPartOf(baseString, true);
    		tranno.isAPartOf(baseString, true);
    		effdate.isAPartOf(baseString, true);
    		errorLine.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}