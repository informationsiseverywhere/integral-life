/*
 * File: Dry5106rp.java
 * Date: November 23, 2015 12:42 AM 
 * Author: sbatra9
 * 
 * Class transformed from DRY5106RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptTableDAM;
import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.diary.recordstructures.R5106rec;
import com.csc.life.unitlinkedprocessing.reports.R5106Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5106rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5106Report printFile = new R5106Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5106RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTotalFundamnt = new PackedDecimalData(16,0);
	private PackedDecimalData wsaaTotalNoFrunts = new PackedDecimalData(16,5);
	private PackedDecimalData wsaaTotalDmdunts = new PackedDecimalData(16,5);
	private FixedLengthStringData wsaaVrtfnd = new FixedLengthStringData(4);
	private PackedDecimalData wsaaRptCount = new PackedDecimalData(5, 0);
	
	private FixedLengthStringData wsaaSortKey = new FixedLengthStringData(100);
	private FixedLengthStringData wsaaSortVrtfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSortUnityp = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSortChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSortBatctrcde = new FixedLengthStringData(4);
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5515 = "T5515";
		/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private Getdescrec getdescrec = new Getdescrec();
	private T5515rec t5515rec = new T5515rec();
	private R5106rec r5106rec = new R5106rec();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5106H01Record = new FixedLengthStringData(109);
	private FixedLengthStringData r5106h01O = new FixedLengthStringData(109).isAPartOf(r5106H01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5106h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5106h01O, 1);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(r5106h01O, 31);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r5106h01O, 35);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(r5106h01O, 65);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(r5106h01O, 68);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(1).isAPartOf(r5106h01O, 98);
	
	private FixedLengthStringData r5106h02Record = new FixedLengthStringData(2);
	
	private FixedLengthStringData r5106T01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5106t01O = new FixedLengthStringData(73).isAPartOf(r5106T01Record, 0);
	private FixedLengthStringData totfndamt = new FixedLengthStringData(18).isAPartOf(r5106t01O, 0);
	private FixedLengthStringData totrunts = new FixedLengthStringData(16).isAPartOf(r5106t01O, 18);
	private FixedLengthStringData totdunts = new FixedLengthStringData(16).isAPartOf(r5106t01O, 34);
	
	private FixedLengthStringData r5106N01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5106n01O = new FixedLengthStringData(73).isAPartOf(r5106N01Record, 0);
	private FixedLengthStringData schalph = new FixedLengthStringData(10).isAPartOf(r5106n01O, 0);
	
	private FixedLengthStringData r5106E01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5106e01O = new FixedLengthStringData(73).isAPartOf(r5106E01Record, 0);
	

	private DescTableDAM descIO = new DescTableDAM();
	private DrptTableDAM drptIO = new DrptTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R5106D01RecordInner r5106D01RecordInner = new R5106D01RecordInner();
	private R5106H02RecordInner r5106H02RecordInner = new R5106H02RecordInner();

	public Dry5106rp() {
		super();
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		
		//Intitialise any working storage totals.....  
		wsaaTotalFundamnt.set(ZERO);
		wsaaTotalNoFrunts.set(ZERO);
		wsaaTotalDmdunts.set(ZERO);
		wsaaRptCount.set(ZERO);
		wsaaSortKey.set(ZERO);
		wsaaVrtfnd.set(ZERO);
		wsaaOverflow.set("Y");
		
		//Set up today's date...
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		
		//Get Company Description
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		companynm.set(descIO.getLongdesc());
		
		//Set up the DRPTSRT ready to read...
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		
		//Loop through each DRPTSRT record printing the R5106
		//report ..                                          
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		endReport500();
		printFile.close();	
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{	
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}	
		writeLine300();
		//Set up the working storage sort key..
		
		wsaaSortVrtfnd.set(dryrDryrptRecInner.r5106Vrtfnd);
		wsaaSortUnityp.set(dryrDryrptRecInner.r5106Unityp);
		wsaaSortBatctrcde.set(dryrDryrptRecInner.r5106Batctrcde);
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/*Check first if totals have to be written before a new page
		 is written.. 
		                                              */
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		checkTotals600();
	
		/*Write New Page if required making sure the correct VRTFND
		is written to the header record      */
		
		if (pageOverflow.isTrue()) {
			wsaaVrtfnd.set(dryrDryrptRecInner.r5106Vrtfnd);
			wsaaOverflow.set("N");
			newPage400();
	}
		
		/* Fill detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		r5106D01RecordInner.chdrnum.set(dryrDryrptRecInner.r5106Chdrnum);
		r5106D01RecordInner.fundamnt.set(dryrDryrptRecInner.r5106Fundamt);
		r5106D01RecordInner.priceused.set(dryrDryrptRecInner.r5106Priceused);
		r5106D01RecordInner.nofrunts.set(dryrDryrptRecInner.r5106NoFrunts);
		r5106D01RecordInner.pricedt.set(dryrDryrptRecInner.r5106PriceDt);
		r5106D01RecordInner.pricedt.set(datcon1rec.extDate);
		r5106D01RecordInner.batctrcde.set(dryrDryrptRecInner.r5106Batctrcde);
		r5106D01RecordInner.nofdunts.set(dryrDryrptRecInner.r5106Dmdunts);
		
		//Accumulate totals for the fund, currency and unit type.
		wsaaTotalFundamnt.set(add(dryrDryrptRecInner.r5106Fundamt,wsaaTotalFundamnt)); 
		wsaaTotalNoFrunts.set(add(dryrDryrptRecInner.r5106NoFrunts,wsaaTotalNoFrunts)); 
		wsaaTotalDmdunts.set(add(dryrDryrptRecInner.r5106Dmdunts,wsaaTotalDmdunts)); 
		
		//Increment the report record count..
		compute(wsaaRptCount).set(add(wsaaRptCount,1)); 
		
		printRecord.set(SPACES);
		printFile.printR5106d01(r5106D01RecordInner.r5106D01Record, indicArea);
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		company.set(dryoutrec.company);
		
		//Set up the virtual fund details...
		vrtfnd.set(wsaaVrtfnd);
		
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(dryoutrec.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(wsaaVrtfnd);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		longdesc.set(descIO.getLongdesc());
		
		//Set up the currency details...
		itdmIO.setDataKey(SPACES);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(dryoutrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(wsaaVrtfnd);
		itdmIO.setItmfrm(dryoutrec.effectiveDate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), dryoutrec.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),wsaaVrtfnd)
		|| isEQ(itdmIO.getStatuz(),varcom.endp))
		{
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		else
		{
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		fndcurr.set(t5515rec.currcode);
		
		//Look up currency description
		
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dryoutrec.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(t5515rec.currcode);

		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(dryoutrec.language);
		getdescrec.function.set("CHECK");

		callProgram(Getdesc.class, getdescrec.getdescRec);

		if (isNE(getdescrec.statuz, varcom.oK)) {
			curdesc.set('?');
		} else {
			curdesc.set(getdescrec.longdesc);
		}
	//Set up the UNIT TYPE 
		fundtyp.set(dryrDryrptRecInner.r5106Unityp);
	//Write the first set of header details..... 
		printRecord.set(SPACES);
		printFile.printR5106h01(r5106H01Record, indicArea);
	//Write the 2nd set of header details.... 
		printRecord.set(SPACES);
		printFile.printR5106h02(r5106H02RecordInner.r5106H02Record, indicArea);
	}

protected void endReport500() {
	/*NEW*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}

		//If no details have been printed, then write the N01 line 
		if(!isGT(wsaaRptCount,0)){
			printRecord.set(SPACES);
			printFile.printR5106n01(r5106E01Record, indicArea);
		}
		//Write New Page if required, setting the virtual fund
		//in the header to match the one for the totals.   
		if (pageOverflow.isTrue()) {
			wsaaVrtfnd.set(wsaaSortVrtfnd);
			wsaaOverflow.set("N");
			newPage400();
		}
		//Write out the totals for the records just processed
		totfndamt.set(wsaaTotalFundamnt);
		totrunts.set(wsaaTotalNoFrunts);
		totdunts.set(wsaaTotalDmdunts);
		printFile.printR5106t01(r5106T01Record, indicArea);
		wsaaOverflow.set("Y");

		//write the end of report line 
		printFile.printR5106e01(r5106E01Record, indicArea);

		/*EXIT*/
	}

protected void checkTotals600()
{
	/*Report totals will need to be printed where there is a 
	change of key..making sure a new page is written if    
	required..  */           
	
	//If there are no records printed, then exit this section..
	
	if (isEQ(wsaaRptCount,0)) {
		return;
	}
	
	//The key has not changed so get out of here!!
	
	if (isEQ(dryrDryrptRecInner.r5106Vrtfnd,wsaaSortVrtfnd)
	&& (isEQ(dryrDryrptRecInner.r5106Unityp,wsaaSortUnityp))) {
		return;
	}
	
	/*Write New Page if required, setting the virtual fund 
	in the header to match the one for the totals. */
	
	if (pageOverflow.isTrue()) {
		wsaaVrtfnd.set(wsaaSortVrtfnd);
		wsaaOverflow.set("N");
		newPage400();
	}
	
	/*OK, now we have a key change, so do the totals..
    
	Write the end of report line         */
		
	totfndamt.set(wsaaTotalFundamnt);
	totrunts.set(wsaaTotalNoFrunts);
	totdunts.set(wsaaTotalDmdunts);

	printFile.printR5106t01(r5106T01Record, indicArea);
	wsaaOverflow.set("Y");
	
	//Intitialise any working storage totals.....
	wsaaTotalFundamnt.set(ZERO);
	wsaaTotalNoFrunts.set(ZERO);
	wsaaTotalDmdunts.set(ZERO);
	wsaaOverflow.set("Y");
}



protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		dryoutrec.statuz.set(drylogrec.statuz);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		a001FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);

	private FixedLengthStringData r5106DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r5106Fundamt = new ZonedDecimalData(17, 2).isAPartOf(r5106DataArea, 0);
	private ZonedDecimalData r5106Priceused = new ZonedDecimalData(9, 5).isAPartOf(r5106DataArea, 17);
	private ZonedDecimalData r5106NoFrunts = new ZonedDecimalData(16, 5).isAPartOf(r5106DataArea, 26);
	private FixedLengthStringData r5106PriceDt = new  FixedLengthStringData(10).isAPartOf(r5106DataArea, 42);
	private ZonedDecimalData r5106Dmdunts = new ZonedDecimalData(16, 5).isAPartOf(r5106DataArea, 52);
	private FixedLengthStringData r5106Vrtfnd = new FixedLengthStringData(4).isAPartOf(r5106DataArea, 68);
	private FixedLengthStringData r5106Unityp = new FixedLengthStringData(1).isAPartOf(r5106DataArea, 72);
	private FixedLengthStringData r5106Chdrnum = new FixedLengthStringData(8).isAPartOf(r5106DataArea, 73);
	private FixedLengthStringData r5106Batctrcde = new  FixedLengthStringData(4).isAPartOf(r5106DataArea, 81);
	
	public FixedLengthStringData r5106R5106Key = new FixedLengthStringData(38).isAPartOf(dryrSortkey, 0, REDEFINE);
	public FixedLengthStringData r5106vrtfnd = new FixedLengthStringData(4).isAPartOf(r5106R5106Key, 0);
	public FixedLengthStringData r5106unityp = new FixedLengthStringData(1).isAPartOf(r5106R5106Key, 4);
	public FixedLengthStringData r5106chdrnum = new FixedLengthStringData(8).isAPartOf(r5106R5106Key, 5);
	public FixedLengthStringData r5106batctrcde = new FixedLengthStringData(4).isAPartOf(r5106R5106Key, 13);
	public FixedLengthStringData r5106ReportName = new FixedLengthStringData(10);
}
/*
 * Class transformed  from Data Structure R5106-D01-RECORD--INNER
 */
private static final class R5106D01RecordInner { 

	private FixedLengthStringData r5106D01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5106d01O = new FixedLengthStringData(99).isAPartOf(r5106D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5106d01O, 0);
	private FixedLengthStringData fundamnt = new FixedLengthStringData(17).isAPartOf(r5106d01O, 8);
	private FixedLengthStringData priceused = new FixedLengthStringData(9).isAPartOf(r5106d01O, 25);
	private FixedLengthStringData nofrunts = new FixedLengthStringData(16).isAPartOf(r5106d01O, 34);
	private FixedLengthStringData pricedt = new FixedLengthStringData(10).isAPartOf(r5106d01O, 50);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5106d01O, 60);
	private ZonedDecimalData nofdunts = new ZonedDecimalData(16,5).isAPartOf(r5106d01O, 64);
}

private static final class R5106H01RecordInner { 

	private FixedLengthStringData r5106H01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5106h01O = new FixedLengthStringData(99).isAPartOf(r5106H01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(8).isAPartOf(r5106h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(16).isAPartOf(r5106h01O, 8);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(10).isAPartOf(r5106h01O, 24);
	private FixedLengthStringData longdesc = new FixedLengthStringData(10).isAPartOf(r5106h01O, 30);
	private ZonedDecimalData fndcurr = new ZonedDecimalData(4, 0).isAPartOf(r5106h01O, 40);
	private FixedLengthStringData curdesc = new FixedLengthStringData(4).isAPartOf(r5106h01O, 50);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(4).isAPartOf(r5106h01O, 54);
}

private static final class R5106H02RecordInner { 

	private FixedLengthStringData r5106H02Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5106h02O = new FixedLengthStringData(99).isAPartOf(r5106H02Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(8).isAPartOf(r5106h02O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(16).isAPartOf(r5106h02O, 8);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(10).isAPartOf(r5106h02O, 24);
	private FixedLengthStringData longdesc = new FixedLengthStringData(10).isAPartOf(r5106h02O, 30);
	private ZonedDecimalData fndcurr = new ZonedDecimalData(4, 0).isAPartOf(r5106h02O, 40);
	private FixedLengthStringData curdesc = new FixedLengthStringData(4).isAPartOf(r5106h02O, 50);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(4).isAPartOf(r5106h02O, 54);
	
}
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
