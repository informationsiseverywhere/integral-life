package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:08
 * Description:
 * Copybook name: R537002REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R537002rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(492);
  	public ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0).isAPartOf(dataArea, 0);
  	public ZonedDecimalData pymt = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 4);
  	public FixedLengthStringData currcd = new FixedLengthStringData(3).isAPartOf(dataArea, 21);
  	public ZonedDecimalData prcnt = new ZonedDecimalData(7, 2).isAPartOf(dataArea, 24);
  	public FixedLengthStringData payreason = new FixedLengthStringData(2).isAPartOf(dataArea, 31);
  	public FixedLengthStringData rgpystat = new FixedLengthStringData(2).isAPartOf(dataArea, 33);
  	public FixedLengthStringData date_var = new FixedLengthStringData(10).isAPartOf(dataArea, 35);
  	public FixedLengthStringData rptdesc = new FixedLengthStringData(33).isAPartOf(dataArea, 45);
  	public FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(dataArea, 78);
  	public FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(dataArea, 88);
  	public FixedLengthStringData logstr = new FixedLengthStringData(30).isAPartOf(dataArea, 118);
  	public FixedLengthStringData excode = new FixedLengthStringData(4).isAPartOf(dataArea, 148);
  	public ZonedDecimalData firstPaydate = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 152);
  	public ZonedDecimalData lastPaydate = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 160);
  	public ZonedDecimalData revdte = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 168);
  	public ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(dataArea, 176);
  	public FixedLengthStringData filler = new FixedLengthStringData(311).isAPartOf(dataArea, 181, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(530);
  	public FixedLengthStringData exreport = new FixedLengthStringData(1).isAPartOf(sortKey, 492);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 493);
  	public FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(sortKey, 501);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(sortKey, 503);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(sortKey, 507);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(sortKey, 509);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(sortKey, 511);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(17).isAPartOf(sortKey, 513, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R537002  ");


	public void initialize() {
		COBOLFunctions.initialize(rgpynum);
		COBOLFunctions.initialize(pymt);
		COBOLFunctions.initialize(currcd);
		COBOLFunctions.initialize(prcnt);
		COBOLFunctions.initialize(payreason);
		COBOLFunctions.initialize(rgpystat);
		COBOLFunctions.initialize(date_var);
		COBOLFunctions.initialize(rptdesc);
		COBOLFunctions.initialize(repdate);
		COBOLFunctions.initialize(longdesc);
		COBOLFunctions.initialize(logstr);
		COBOLFunctions.initialize(excode);
		COBOLFunctions.initialize(firstPaydate);
		COBOLFunctions.initialize(lastPaydate);
		COBOLFunctions.initialize(revdte);
		COBOLFunctions.initialize(tranno);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(exreport);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(rgpytype);
		COBOLFunctions.initialize(crtable);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rgpynum.isAPartOf(baseString, true);
    		pymt.isAPartOf(baseString, true);
    		currcd.isAPartOf(baseString, true);
    		prcnt.isAPartOf(baseString, true);
    		payreason.isAPartOf(baseString, true);
    		rgpystat.isAPartOf(baseString, true);
    		date_var.isAPartOf(baseString, true);
    		rptdesc.isAPartOf(baseString, true);
    		repdate.isAPartOf(baseString, true);
    		longdesc.isAPartOf(baseString, true);
    		logstr.isAPartOf(baseString, true);
    		excode.isAPartOf(baseString, true);
    		firstPaydate.isAPartOf(baseString, true);
    		lastPaydate.isAPartOf(baseString, true);
    		revdte.isAPartOf(baseString, true);
    		tranno.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		exreport.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		rgpytype.isAPartOf(baseString, true);
    		crtable.isAPartOf(baseString, true);
    		life.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		rider.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}