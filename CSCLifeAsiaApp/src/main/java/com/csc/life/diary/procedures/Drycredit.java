package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;

import com.csc.diary.procedures.Drylog;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.financials.dataaccess.PreqdryTableDAM;
import com.csc.fsu.general.recordstructures.Reqproccpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Drycredit extends Maind {
	
	/*
	 *
      *(c) Copyright Continuum Corporation Ltd.  1986....1995.
      *    All rights reserved.  Continuum Confidential.
      *
      *REMARKS.
      *
      * This is the Direct Credit Diary Update Subroutine.
      *
      ***********************************************************************
      *                                                                     *
      * ......... New Version of the Amendment History.                     *
      *                                                                     *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 24/04/01  01/01   DRYAPL       Jacco Landskroon                     *
      *           Initial Version                                           *
      *                                                                     *
      * 23/04/09  01/01   ZD0029       CSC - Niamh Fogarty                  *
      *           CSC Europe version of DRYCREDIT to be used by Life/       *
      *           Asia.                                                     *
      *                                                                     *
      * 28/04/09  01/01   A08346       CSC - Niamh Fogarty                  *
      *           - Replace hardcoded payment type with parameter.          *
      *           - Replace hardcoded CHEQ status with copybook field.      *
      *           - Regular Payments adds Direct Credit PREQ records        *
      *             with PREQ-RLDGACCT = CHDRNUM + LIFE + COVERAGE +        *
      *             RIDER + PLAN-SUFFIX. Direct Credits adds Direct         *
      *             Credit PREQ records with PREQ-RLDGACCT = CHDRNUM.       *
      *             Therefore DRYCREDIT should process all PREQDRY          *
      *             records with PREQDRY-RLDGACCT(1:8) = DRYP-ENTITY.
      **DD/MM/YY*************************************************************
      *
	 */

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYCREDIT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	/* FORMATS */
	private static final String cheqrec = "CHEQREC";
	private static final String preqdryrec = "PREQDRYREC";
	
	private Varcom varcom = new Varcom();
	private Reqproccpy reqproccpy = new Reqproccpy();
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private PreqdryTableDAM preqdryIO = new PreqdryTableDAM();
	
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, nextr180, exit190
	}

	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainLine000() {
		main010();
		exit090();
	}

	protected void main010() {
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		drypDryprcRecInner.drypNxtprcdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
		
		/* Read the PREQ record to see if a requisition exists. */
		preqdryIO.setParams(SPACES);

		preqdryIO.rldgcoy.set(drypDryprcRecInner.drypCompany);
		preqdryIO.branch.set(drypDryprcRecInner.drypBranch);
		preqdryIO.rldgacct.set(drypDryprcRecInner.drypEntity);
		preqdryIO.setFunction(varcom.begn);
		preqdryIO.setFormat(preqdryIO);
		preqdryIO.setStatuz(varcom.oK);
		
		while (!(isEQ(preqdryIO.getStatuz(), varcom.endp))) {
			readPreq100();
		}
		
		if (isEQ(drypDryprcRecInner.drypNxtprcdate, varcom.vrcmMaxDate)) {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

	protected void exit090() {
		exitProgram();
	}

	protected void readPreq100() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read110();
				case nextr180:
					nextr180();
				case exit190:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void read110() {
		SmartFileCode.execute(appVars, preqdryIO);
		if (!isEQ(preqdryIO.getStatuz(), varcom.oK)
				&& !isEQ(preqdryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(preqdryIO.getStatuz());
			drylogrec.params.set(preqdryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(preqdryIO.getStatuz(), varcom.endp)
				|| !isEQ(preqdryIO.rldgcoy, drypDryprcRecInner.drypCompany)
				|| !isEQ(preqdryIO.branch, drypDryprcRecInner.drypBranch)
				|| !isEQ(preqdryIO.getRldgacct().setSub1String(1, 8),
						drypDryprcRecInner.drypEntity)) {
			preqdryIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		/* If a requisition exists, check if it is authorised. */
		cheqIO.setParams(SPACES);
		cheqIO.reqncoy.set(preqdryIO.rdoccoy);
		cheqIO.reqnno.set(preqdryIO.rdocnum);
		cheqIO.setFunction(varcom.readr);
		cheqIO.setFormat(cheqrec);
		SmartFileCode.execute(appVars, cheqIO);
		if (!isEQ(cheqIO.getStatuz(), varcom.oK)
				&& !isEQ(cheqIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(cheqIO.getStatuz());
			drylogrec.params.set(cheqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}

		/*
		 **** IF  CHEQ-STATUZ              = MRNF                          <A08346>
		 **** OR  CHEQ-PROCIND         NOT = 'AU'                          <A08346>
		 **** OR  CHEQ-REQNTYPE        NOT = '5'                           <A08346>
		 ****     GO TO 180-NEXTR                                          <A08346>
		 **** END-IF.
		 */

		if (isEQ(cheqIO.statuz, varcom.mrnf)
				|| !isEQ(cheqIO.procind, reqproccpy.auth)
				|| !isEQ(cheqIO.reqntype, drypDryprcRecInner.drypSystParm[01])) {
			goTo(GotoLabel.nextr180);
		}
		if (isLT(cheqIO.paydate, drypDryprcRecInner.drypNxtprcdate)) {
			drypDryprcRecInner.drypNxtprcdate.set(cheqIO.paydate);
		}
	}

	protected void nextr180() {
		preqdryIO.setFunction(varcom.nextr);
	}

//	protected void fatalError() {
//		/* A010-FATAL */
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		} else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		a000FatalError();
//	}

	private static final class DrypDryprcRecInner {

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}
