package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/*
 *(C) Copyright CSC Corporation Limited 1986 - 2000.
 *    All rights reserved. CSC Confidential.
 *
 *REMARKS.
 *
 * This is the transaction detail record change subroutine for
 * IBF Deal transactions.
 *
 ***********************************************************************
 *           AMENDMENT  HISTORY                                        *
 ***********************************************************************
 * DATE.... VSN/MOD  WORK UNIT    BY....                               *
 *                                                                     *
 * 21/02/06  01/01   DRYAP2       Tony Tang - CSC HK                   *
 *           Initial Version                                           *
 *                                                                     *
 * 21/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
 *           Re-Compile.                                               *
 *                                                                     *
 **DD/MM/YY*************************************************************
 */
public class Dryibfdel extends Maind {

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYIBFDEL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDeal = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator dealFound = new Validator(wsaaDeal, "Y");
	private Validator noDealFound = new Validator(wsaaDeal, "N");

	/* FORMATS */
	private static final String hitrdryrec = "HITRDRYREC";

//	private Hitrdrykey wsaaHitrdryKey = new Hitrdrykey();
//	private HitrdryTableDAM hitrdryIO = new HitrdryTableDAM();
	private Hitrpf hitrdryIO = new Hitrpf();
	private Varcom varcom = new Varcom();
//	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Freqcpy freqcpy = new Freqcpy();
	private HitrpfDAO hitrpfDAO=getApplicationContext().getBean("hitrpfDAO" , HitrpfDAO.class);

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit490
	}

	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainLine000() {
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		noDealFound.setTrue();

		/*
		 * Determine if an IBF deal transaction is needed. Check the HITRs.
		 */
		process200();
		exitProgram();
	}

	protected void process200() {
		checkHitrs300();
		if (dealFound.isTrue() && !isEQ(hitrdryIO.getEffdate(), varcom.maxdate)) {
			datcon2rec.function.set(SPACE);
			datcon2rec.intDate1.set(hitrdryIO.getEffdate());
			datcon2rec.frequency.set(freqcpy.daily);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		} else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

	protected void checkHitrs300() {
/*		hitrdryIO.setParams(SPACES);
		hitrdryIO.setRecKeyData(SPACES);
		hitrdryIO.setRecNonKeyData(SPACES);

		hitrdryIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
		hitrdryIO.chdrnum.set(drypDryprcRecInner.drypEntity);
		hitrdryIO.effdate.set(ZERO);
		hitrdryIO.setFormat(hitrdryrec);
		hitrdryIO.setFunction(varcom.begn);
		hitrdryIO.setStatuz(varcom.oK);
		while (!(isEQ(hitrdryIO.getStatuz(), varcom.endp)
				|| dealFound.isTrue())) {
			readHitr400();
		}*/
		List<Hitrpf> hitrpfList = hitrpfDAO.searchHitrdryRecord(drypDryprcRecInner.drypCompany.toString(), drypDryprcRecInner.drypEntity.toString());
		if(hitrpfList.isEmpty()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return;
		}
		for(Hitrpf pf: hitrpfList) {
			hitrdryIO = pf;
			check410();
			if(dealFound.isTrue()) {
				break;
			}
		}
		if(!dealFound.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

	protected void readHitr400() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					check410();
				case exit490:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void check410() {
//		wsaaHitrdryKey.set(hitrdryIO.getRecKeyData());
//		SmartFileCode.execute(appVars, hitrdryIO);
//		/*
//		 * Abort if a database error has been found
//		 */
//		if (!isEQ(hitrdryIO.getStatuz(), varcom.oK)
//				&& !isEQ(hitrdryIO.getStatuz(), varcom.endp)) {
//			drylogrec.params.set(wsaaHitrdryKey);
//			drylogrec.statuz.set(hitrdryIO.getStatuz());
//			drylogrec.dryDatabaseError.setTrue();
//			a000FatalError();//ILPI-61
//		}
		/*
		 * Where there are no matches, then exit the section setting the flag to
		 * skip creating/updating the DTRD for the unit deal transaction.
		 */

//		if (!isEQ(hitrdryIO.chdrcoy, drypDryprcRecInner.drypCompany)
//				|| !isEQ(hitrdryIO.chdrnum, drypDryprcRecInner.drypEntity)
//				|| isEQ(hitrdryIO.getStatuz(), varcom.endp)) {
//			hitrdryIO.setStatuz(varcom.endp);
//			drypDryprcRecInner.dtrdNo.setTrue();
//
//			goTo(GotoLabel.exit490);
//		}
		if (isEQ(hitrdryIO.getFeedbackInd(), SPACES)) {
			dealFound.setTrue();
			//goTo(GotoLabel.exit490);
		}
//		hitrdryIO.setFunction(varcom.nextr);
	}

//	protected void fatalError() {
//		/* A010-FATAL */
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		} else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		a000FatalError();
//	}

	private static final class DrypDryprcRecInner {

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);

	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}
