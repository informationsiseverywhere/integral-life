package com.csc.life.diary.dataaccess.model;

import java.math.BigDecimal;

/**
 * Dry5134Dto
 * 
 * @author hmahajan6
 *
 */
public class Dry5134Dto {
	private String cownnum;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private String statcode;
	private String pstatcode;
	private int crrcd;
	private String premCurrency;
	private String crtable;
	private int riskCessDate;
	private BigDecimal sumins;
	private BigDecimal instprem;
	private BigDecimal zbinstprem;
	private BigDecimal zlinstprem;
	private BigDecimal singp;
	private int cpiDate;
	private String indexationInd;
	private String mortcls;
	private String validflag;
	
	public Dry5134Dto() {
		//default no argument constructor
	}

	/**
	 * Getter for Cownum
	 * 
	 * @return cownum
	 */
	public String getCownnum() {
		return cownnum;
	}

	/**
	 * Setter for cownnum
	 * 
	 * @param cownnum - cownnum
	 */
	public void setCownnum(String cownnum) {
		this.cownnum = cownnum;
	}

	/**
	 * Getter for Company
	 * 
	 * @return chdrcoy - Company
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * Setter for Company
	 * 
	 * @param chdrcoy - Company
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * Getter for Contract Number
	 * 
	 * @return chdrnum - Contract Number
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * Setter for Contract Number
	 * 
	 * @param chdrnum - Contract Number
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * Getter for life
	 * 
	 * @return life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * Setter for life
	 * 
	 * @param life - Life
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * Getter for jlife
	 * 
	 * @return jlife
	 */
	public String getJlife() {
		return jlife;
	}

	/**
	 * Setter for jlife
	 * 
	 * @param jlife - Jlife
	 */
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	/**
	 * Getter for coverage
	 * 
	 * @return coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * Setter for Coverage
	 * 
	 * @param coverage - Coverage
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * Getter for rider
	 * 
	 * @return rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * Setter for rider
	 * 
	 * @param rider - Rider
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * Getter for planSuffix
	 * 
	 * @return planSuffix
	 */
	public int getPlanSuffix() {
		return planSuffix;
	}

	/**
	 * Setter for planSuffix
	 * 
	 * @param planSuffix - PlanSuffix
	 */
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	/**
	 * Getter for validflag
	 * 
	 * @return validflag
	 */
	public String getValidflag() {
		return validflag;
	}

	/**
	 * Setter for validflag
	 * 
	 * @param validflag - Validflag
	 */
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	/**
	 * Getter for statcode
	 * 
	 * @return statcode
	 */
	public String getStatcode() {
		return statcode;
	}

	/**
	 * Setter for statcode
	 * 
	 * @param statcode - Statcode
	 */
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}

	/**
	 * Getter for pstatcode
	 * 
	 * @return pstatcode
	 */
	public String getPstatcode() {
		return pstatcode;
	}

	/**
	 * Setter for pstatcode
	 * 
	 * @param pstatcode - Pstatcode
	 */
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}

	/**
	 * Getter for crrcd
	 * 
	 * @return crrcd
	 */
	public int getCrrcd() {
		return crrcd;
	}

	/**
	 * Setter for crrcd
	 * 
	 * @param crrcd - Crrcd
	 */
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	/**
	 * Getter for premCurrency
	 * 
	 * @return premCurrency
	 */
	public String getPremCurrency() {
		return premCurrency;
	}

	/**
	 * Setter for premCurrency
	 * 
	 * @param premCurrency - PremCurrency
	 */
	public void setPremCurrency(String premCurrency) {
		this.premCurrency = premCurrency;
	}

	/**
	 * Getter for crtable
	 * 
	 * @return crtable
	 */
	public String getCrtable() {
		return crtable;
	}

	/**
	 * Setter for crtable
	 * 
	 * @param crtable - Ctable
	 */
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	/**
	 * Getter for singp
	 * 
	 * @return singp
	 */
	public BigDecimal getSingp() {
		return singp;
	}

	/**
	 * Setter for singp
	 * 
	 * @param singp - Singp
	 */
	public void setSingp(BigDecimal singp) {
		this.singp = singp;
	}

	/**
	 * Getter for riskCessDate
	 * 
	 * @return riskCessDate
	 */
	public int getRiskCessDate() {
		return riskCessDate;
	}

	/**
	 * Setter for riskCessDate
	 * 
	 * @param riskCessDate - RiskCessDate
	 */
	public void setRiskCessDate(int riskCessDate) {
		this.riskCessDate = riskCessDate;
	}

	/**
	 * Getter for sumins
	 * 
	 * @return sumins
	 */
	public BigDecimal getSumins() {
		return sumins;
	}

	/**
	 * Setter for sumins
	 * 
	 * @param sumins - Sumins
	 */
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	/**
	 * Getter for mortcls
	 * 
	 * @return mortcls
	 */
	public String getMortcls() {
		return mortcls;
	}

	/**
	 * Setter for mortcls
	 * 
	 * @param mortcls - Mortcls
	 */
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}

	/**
	 * Getter for indexationInd
	 * 
	 * @return indexationInd
	 */
	public String getIndexationInd() {
		return indexationInd;
	}

	/**
	 * Setter for indexationInd
	 * 
	 * @param indexationInd - IndexationInd
	 */
	public void setIndexationInd(String indexationInd) {
		this.indexationInd = indexationInd;
	}

	/**
	 * Getter for instprem
	 * 
	 * @return instprem
	 */
	public BigDecimal getInstprem() {
		return instprem;
	}

	/**
	 * Setter for instprem
	 * 
	 * @param instprem - Instprem
	 */
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}

	/**
	 * Getter for zbinstprem
	 * 
	 * @return zbinstprem
	 */
	public BigDecimal getZbinstprem() {
		return zbinstprem;
	}

	/**
	 * Setter for zbinstprem
	 * 
	 * @param zbinstprem - Zbinstprem
	 */
	public void setZbinstprem(BigDecimal zbinstprem) {
		this.zbinstprem = zbinstprem;
	}

	/**
	 * Getter for zlinstprem
	 * 
	 * @return zlinstprem
	 */
	public BigDecimal getZlinstprem() {
		return zlinstprem;
	}

	/**
	 * Setter for zlinstprem
	 * 
	 * @param zlinstprem - Zlinstprem
	 */
	public void setZlinstprem(BigDecimal zlinstprem) {
		this.zlinstprem = zlinstprem;
	}

	/**
	 * Getter for cpiDate
	 * 
	 * @return cpiDate
	 */
	public int getCpiDate() {
		return cpiDate;
	}

	/**
	 * Setter for cpiDate
	 * 
	 * @param cpiDate - CpiDate
	 */
	public void setCpiDate(int cpiDate) {
		this.cpiDate = cpiDate;
	}
}
