package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:09
 * Description:
 * Copybook name: R537004REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R537004rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(498);
  	public ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0).isAPartOf(dataArea, 0);
  	public ZonedDecimalData pymt = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 4);
  	public FixedLengthStringData currcd = new FixedLengthStringData(3).isAPartOf(dataArea, 21);
  	public ZonedDecimalData prcnt = new ZonedDecimalData(7, 2).isAPartOf(dataArea, 24);
  	public FixedLengthStringData payreason = new FixedLengthStringData(2).isAPartOf(dataArea, 31);
  	public FixedLengthStringData rgpystat = new FixedLengthStringData(2).isAPartOf(dataArea, 33);
  	public FixedLengthStringData date_var = new FixedLengthStringData(10).isAPartOf(dataArea, 35);
  	public FixedLengthStringData rptdesc = new FixedLengthStringData(33).isAPartOf(dataArea, 45);
  	public FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(dataArea, 78);
  	public FixedLengthStringData regpayfreq = new FixedLengthStringData(2).isAPartOf(dataArea, 88);
  	public FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(dataArea, 90);
  	public FixedLengthStringData longstr = new FixedLengthStringData(30).isAPartOf(dataArea, 120);
  	public FixedLengthStringData excode = new FixedLengthStringData(4).isAPartOf(dataArea, 150);
  	public FixedLengthStringData certdate = new FixedLengthStringData(10).isAPartOf(dataArea, 154);
  	public FixedLengthStringData filler = new FixedLengthStringData(334).isAPartOf(dataArea, 164, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(544);
  	public FixedLengthStringData exreport = new FixedLengthStringData(1).isAPartOf(sortKey, 498);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 499);
  	public FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(sortKey, 507);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(sortKey, 509);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(sortKey, 511);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(sortKey, 513);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(sortKey, 515, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5227     ");


	public void initialize() {
		COBOLFunctions.initialize(rgpynum);
		COBOLFunctions.initialize(pymt);
		COBOLFunctions.initialize(currcd);
		COBOLFunctions.initialize(prcnt);
		COBOLFunctions.initialize(payreason);
		COBOLFunctions.initialize(rgpystat);
		COBOLFunctions.initialize(date_var);
		COBOLFunctions.initialize(rptdesc);
		COBOLFunctions.initialize(repdate);
		COBOLFunctions.initialize(regpayfreq);
		COBOLFunctions.initialize(longdesc);
		COBOLFunctions.initialize(longstr);
		COBOLFunctions.initialize(excode);
		COBOLFunctions.initialize(certdate);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(exreport);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(rgpytype);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rgpynum.isAPartOf(baseString, true);
    		pymt.isAPartOf(baseString, true);
    		currcd.isAPartOf(baseString, true);
    		prcnt.isAPartOf(baseString, true);
    		payreason.isAPartOf(baseString, true);
    		rgpystat.isAPartOf(baseString, true);
    		date_var.isAPartOf(baseString, true);
    		rptdesc.isAPartOf(baseString, true);
    		repdate.isAPartOf(baseString, true);
    		regpayfreq.isAPartOf(baseString, true);
    		longdesc.isAPartOf(baseString, true);
    		longstr.isAPartOf(baseString, true);
    		excode.isAPartOf(baseString, true);
    		certdate.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		exreport.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		rgpytype.isAPartOf(baseString, true);
    		life.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		rider.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}