/*
 * File: Dryrlagprd.java
 * Date: December 3, 2013 2:27:46 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYRLAGPRD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.MacfenqTableDAM;
import com.csc.life.agents.dataaccess.MacfprdTableDAM;
import com.csc.life.agents.recordstructures.Rlagprdrec;
import com.csc.life.agents.tablestructures.Tm603rec;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.statistics.dataaccess.AgprTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Dryprcrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This diary subroutine calculates and update the agency
*   production figures in Diary Accumulator File DACM.
*
*   It is called from LIFACMV if T3695 for the premium or
*   commission production flag is on for the sub-account type.
*
*   These files are read for:
*     HPAD - first issue date
*     AGLF - agent type (used if no MACF)
*     MACF - agent type
*
*   These tables are read for:
*     TM603 - production group to determine the production
*             production categories and identify direct or
*             group production updates
*
*****************************************************************
* </pre>
*/
public class Dryrlagprd extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "DRYRLAGPRD";
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex1 = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaTeamFlg = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOvrFlg = new FixedLengthStringData(1);
	private String wsaaExist = "";

	private FixedLengthStringData wsaaTm603Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaTm603Agtype = new FixedLengthStringData(2).isAPartOf(wsaaTm603Key, 0);
	private FixedLengthStringData wsaaTm603Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTm603Key, 2);
	private ZonedDecimalData wsaaInteger = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaCurrfrom = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaMlperpc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlperpp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlgrppc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlgrppp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMldirpc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMldirpp = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(4, 8, wsaaReportags, 0);
	private static final String e031 = "E031";
		/* TABLES */
	private static final String tm603 = "TM603";
	private static final String macfprdrec = "MACFPRDREC";
	private static final String macfenqrec = "MACFENQREC";
	private static final String hpadrec = "HPADREC";
	private static final String dacmrec = "DACMREC";
	private static final String agprrec = "AGPRREC";
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AgprTableDAM agprIO = new AgprTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MacfenqTableDAM macfenqIO = new MacfenqTableDAM();
	private MacfprdTableDAM macfprdIO = new MacfprdTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Tm603rec tm603rec = new Tm603rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Rlagprdrec rlagprdrec = new Rlagprdrec();
	private Dryprcrec dryprcrec = new Dryprcrec();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYRLAGPRD");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	public Dryrlagprd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryprcrec.dryprcRec = convertAndSetParam(dryprcrec.dryprcRec, parmArray, 1);
		rlagprdrec.rlagprdRec = convertAndSetParam(rlagprdrec.rlagprdRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(dryprcrec.dryprcRec);
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		/* Initialise all working storage fields*/
		initialize(wsaaTeamFlg);
		initialize(wsaaOvrFlg);
		initialize(wsaaIndex);
		initialize(wsaaIndex1);
		initialize(wsaaEffdate);
		initialize(wsaaCurrfrom);
		initialize(wsaaAgntnum);
		wsaaMlgrppc.set(ZERO);
		wsaaMlgrppp.set(ZERO);
		wsaaMldirpc.set(ZERO);
		wsaaMldirpp.set(ZERO);
		wsaaMlperpc.set(ZERO);
		wsaaMlperpp.set(ZERO);
		drylogrec.subrname.set(wsaaSubr);
		datcon3rec.intDate1.set(rlagprdrec.occdate);
		datcon3rec.intDate2.set(rlagprdrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			a000FatalError();//ILPI-61
		}
		wsaaInteger.set(datcon3rec.freqFactor);
		wsaaInteger.add(1);
		hpadIO.setChdrcoy(rlagprdrec.chdrcoy);
		hpadIO.setChdrnum(rlagprdrec.chdrnum);
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(hpadIO.getStatuz());
			drylogrec.params.set(hpadIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		wsaaCurrfrom.set(hpadIO.getHissdte());
		aglflnbIO.setAgntnum(rlagprdrec.agntnum);
		aglflnbIO.setAgntcoy(rlagprdrec.agntcoy);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(aglflnbIO.getStatuz());
			drylogrec.params.set(aglflnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		macfprdIO.setAgntcoy(rlagprdrec.agntcoy);
		macfprdIO.setAgntnum(rlagprdrec.agntnum);
		macfprdIO.setEffdate(wsaaCurrfrom);
		macfprdIO.setAgmvty(SPACES);
		macfprdIO.setStatuz(varcom.oK);
		macfprdIO.setFormat(macfprdrec);
		macfprdIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfprdIO);
		if (isNE(macfprdIO.getStatuz(), varcom.oK)
		&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(macfprdIO.getStatuz());
			drylogrec.params.set(macfprdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(rlagprdrec.agntnum, macfprdIO.getAgntnum())
		|| isNE(rlagprdrec.agntcoy, macfprdIO.getAgntcoy())
		|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
			macfprdIO.setParams(SPACES);
			macfprdIO.setAgntcoy(rlagprdrec.agntcoy);
			macfprdIO.setAgntnum(rlagprdrec.agntnum);
			macfprdIO.setEffdate(ZERO);
			macfprdIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, macfprdIO);
			if (isNE(macfprdIO.getStatuz(), varcom.oK)
			&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(macfprdIO.getStatuz());
				drylogrec.params.set(macfprdIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
			if (isNE(rlagprdrec.agntnum, macfprdIO.getAgntnum())
			|| isNE(rlagprdrec.agntcoy, macfprdIO.getAgntcoy())
			|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(macfprdIO.getStatuz());
				drylogrec.params.set(macfprdIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
		}
		readTm603100();
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			rlagprdrec.statuz.set(varcom.endp);
			return ;
		}
		if (isNE(tm603rec.active01, "Y")
		&& isNE(tm603rec.active02, "Y")
		&& isNE(tm603rec.active03, "Y")) {
			return ;
		}
		personalProduction300();
		wsaaIndex1.set(1);
		wsaaExist = "N";
		wsaaReportags.set(SPACES);
		wsaaReportag[1].set(macfprdIO.getZrptga());
		wsaaReportag[2].set(macfprdIO.getZrptgb());
		wsaaReportag[3].set(macfprdIO.getZrptgc());
		wsaaReportag[4].set(macfprdIO.getZrptgd());
		/*-LA1174 ---> There are only 4 report tag, since we haven't*/
		/* encountered a situation of having five levels, if happened*/
		/* to be there, might have  ended in MCH0603 subscript error.*/
		while ( !(isGT(wsaaIndex1, 4)
		|| isEQ(wsaaReportag[wsaaIndex1.toInt()], SPACES))) {
			updateProduction200();
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTm603100()
	{
		start100();
	}

protected void start100()
	{
		tm603rec.tm603Rec.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rlagprdrec.agntcoy);
		itdmIO.setItemtabl(tm603);
		if (isEQ(macfprdIO.getMlagttyp(), SPACES)) {
			wsaaTm603Agtype.set(aglflnbIO.getAgtype());
		}
		else {
			wsaaTm603Agtype.set(macfprdIO.getMlagttyp());
		}
		wsaaTm603Cnttype.set(rlagprdrec.cnttype);
		itdmIO.setItemitem(wsaaTm603Key);
		itdmIO.setItmfrm(rlagprdrec.occdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), rlagprdrec.agntcoy)
		&& isEQ(itdmIO.getItemtabl(), tm603)
		&& isEQ(itdmIO.getItemitem(), wsaaTm603Key)) {
			tm603rec.tm603Rec.set(itdmIO.getGenarea());
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(rlagprdrec.agntcoy);
		itdmIO.setItemtabl(tm603);
		if (isEQ(macfprdIO.getMlagttyp(), SPACES)) {
			wsaaTm603Agtype.set(aglflnbIO.getAgtype());
		}
		else {
			wsaaTm603Agtype.set(macfprdIO.getMlagttyp());
		}
		wsaaTm603Cnttype.set("***");
		itdmIO.setItemitem(wsaaTm603Key);
		itdmIO.setItmfrm(rlagprdrec.occdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), rlagprdrec.agntcoy)
		&& isEQ(itdmIO.getItemtabl(), tm603)
		&& isEQ(itdmIO.getItemitem(), wsaaTm603Key)) {
			tm603rec.tm603Rec.set(itdmIO.getGenarea());
		}
		else {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(e031);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
	}

protected void updateProduction200()
	{
		start210();
	}

protected void start210()
	{
		macfenqIO.setAgntcoy(rlagprdrec.agntcoy);
		macfenqIO.setAgntnum(wsaaReportag[wsaaIndex1.toInt()]);
		macfenqIO.setEffdate(wsaaCurrfrom);
		macfenqIO.setAgmvty(SPACES);
		macfenqIO.setStatuz(varcom.oK);
		macfenqIO.setFormat(macfenqrec);
		macfenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfenqIO);
		if (isNE(macfenqIO.getStatuz(), varcom.oK)
		&& isNE(macfenqIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(macfenqIO.getStatuz());
			drylogrec.params.set(macfenqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(wsaaReportag[wsaaIndex1.toInt()], macfenqIO.getAgntnum())
		|| isNE(rlagprdrec.agntcoy, macfenqIO.getAgntcoy())
		|| isEQ(macfenqIO.getStatuz(), varcom.endp)) {
			macfenqIO.setParams(SPACES);
			macfenqIO.setAgntcoy(rlagprdrec.agntcoy);
			macfenqIO.setAgntnum(wsaaReportag[wsaaIndex1.toInt()]);
			macfenqIO.setEffdate(ZERO);
			macfenqIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, macfenqIO);
			if (isNE(macfenqIO.getStatuz(), varcom.oK)
			&& isNE(macfenqIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(macfenqIO.getStatuz());
				drylogrec.params.set(macfenqIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
			if (isNE(wsaaReportag[wsaaIndex1.toInt()], macfenqIO.getAgntnum())
			|| isNE(rlagprdrec.agntcoy, macfenqIO.getAgntcoy())
			|| isEQ(macfenqIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(macfenqIO.getStatuz());
				drylogrec.params.set(macfenqIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
		}
		wsaaTeamFlg.set("N");
		wsaaOvrFlg.set("N");
		wsaaMlgrppc.set(ZERO);
		wsaaMlgrppp.set(ZERO);
		wsaaMldirpc.set(ZERO);
		wsaaMldirpp.set(ZERO);
		wsaaMlperpc.set(ZERO);
		wsaaMlperpp.set(ZERO);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)
		|| isEQ(wsaaTeamFlg, "Y")); wsaaIndex.add(1)){
			if (isEQ(macfenqIO.getMlagttyp(), tm603rec.mlagttyp[wsaaIndex.toInt()])) {
				wsaaTeamFlg.set("Y");
			}
		}
		if (isEQ(wsaaTeamFlg, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlgrppc.set(rlagprdrec.origamt);
			}
			else {
				wsaaMlgrppp.set(rlagprdrec.origamt);
			}
		}
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)
		|| isEQ(wsaaOvrFlg, "Y")
		|| isEQ(wsaaExist, "Y")); wsaaIndex.add(1)){
			if (isEQ(macfenqIO.getMlagttyp(), tm603rec.agtype[wsaaIndex.toInt()])) {
				wsaaOvrFlg.set("Y");
				wsaaExist = "Y";
			}
		}
		if (isEQ(wsaaOvrFlg, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMldirpc.set(rlagprdrec.origamt);
			}
			else {
				wsaaMldirpp.set(rlagprdrec.origamt);
			}
		}
		wsaaAgntnum.set(wsaaReportag[wsaaIndex1.toInt()]);
		wsaaEffdate.set(macfenqIO.getEffdate());
		writeDacm400();
		wsaaIndex1.add(1);
	}

protected void personalProduction300()
	{
		start310();
	}

protected void start310()
	{
		if (isEQ(tm603rec.active03, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlperpc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMlperpp.add(rlagprdrec.origamt);
			}
		}
		if (isEQ(tm603rec.active01, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMldirpc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMldirpp.add(rlagprdrec.origamt);
			}
		}
		if (isEQ(tm603rec.active02, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlgrppc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMlgrppp.add(rlagprdrec.origamt);
			}
		}
		wsaaAgntnum.set(rlagprdrec.agntnum);
		wsaaEffdate.set(macfprdIO.getEffdate());
		writeDacm400();
	}

protected void writeDacm400()
	{
		start410();
	}

protected void start410()
	{
		agprIO.setDataArea(SPACES);
		initialize(agprIO.getMlperpps());
		initialize(agprIO.getMlperpcs());
		initialize(agprIO.getMlgrppps());
		initialize(agprIO.getMlgrppcs());
		initialize(agprIO.getMldirpps());
		initialize(agprIO.getMldirpcs());
		initialize(agprIO.getCntcount());
		initialize(agprIO.getSumins());
		agprIO.setAgntcoy(rlagprdrec.agntcoy);
		agprIO.setAgntnum(wsaaAgntnum);
		agprIO.setAcctyr(rlagprdrec.actyear);
		agprIO.setMnth(rlagprdrec.actmnth);
		agprIO.setCnttype(rlagprdrec.cnttype);
		agprIO.setEffdate(wsaaEffdate);
		agprIO.setFormat(agprrec);
		if (isGT(wsaaInteger, 10)) {
			setPrecision(agprIO.getMlperpc(10), 2);
			agprIO.setMlperpc(10, add(agprIO.getMlperpc(10), wsaaMlperpc));
			setPrecision(agprIO.getMlperpp(10), 2);
			agprIO.setMlperpp(10, add(agprIO.getMlperpp(10), wsaaMlperpp));
			setPrecision(agprIO.getMlgrppc(10), 2);
			agprIO.setMlgrppc(10, add(agprIO.getMlgrppc(10), wsaaMlgrppc));
			setPrecision(agprIO.getMlgrppp(10), 2);
			agprIO.setMlgrppp(10, add(agprIO.getMlgrppp(10), wsaaMlgrppp));
			setPrecision(agprIO.getMldirpc(10), 2);
			agprIO.setMldirpc(10, add(agprIO.getMldirpc(10), wsaaMldirpc));
			setPrecision(agprIO.getMldirpp(10), 2);
			agprIO.setMldirpp(10, add(agprIO.getMldirpp(10), wsaaMldirpp));
		}
		else {
			setPrecision(agprIO.getMlperpc(wsaaInteger), 2);
			agprIO.setMlperpc(wsaaInteger, add(agprIO.getMlperpc(wsaaInteger), wsaaMlperpc));
			setPrecision(agprIO.getMlperpp(wsaaInteger), 2);
			agprIO.setMlperpp(wsaaInteger, add(agprIO.getMlperpp(wsaaInteger), wsaaMlperpp));
			setPrecision(agprIO.getMlgrppc(wsaaInteger), 2);
			agprIO.setMlgrppc(wsaaInteger, add(agprIO.getMlgrppc(wsaaInteger), wsaaMlgrppc));
			setPrecision(agprIO.getMlgrppp(wsaaInteger), 2);
			agprIO.setMlgrppp(wsaaInteger, add(agprIO.getMlgrppp(wsaaInteger), wsaaMlgrppp));
			setPrecision(agprIO.getMldirpc(wsaaInteger), 2);
			agprIO.setMldirpc(wsaaInteger, add(agprIO.getMldirpc(wsaaInteger), wsaaMldirpc));
			setPrecision(agprIO.getMldirpp(wsaaInteger), 2);
			agprIO.setMldirpp(wsaaInteger, add(agprIO.getMldirpp(wsaaInteger), wsaaMldirpp));
		}
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(dryprcrec.threadNumber);
		dacmIO.setCompany(dryprcrec.company);
		dacmIO.setRecformat(agprrec);
		dacmIO.setDataarea(agprIO.getDataArea());
		dacmIO.setFormat(dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaSubr);
//		drylogrec.effectiveDate.set(dryprcrec.runDate);
//		drylogrec.entityKey.set(dryprcrec.entityKey);
//		drylogrec.runNumber.set(ZERO);
//		rlagprdrec.statuz.set(drylogrec.statuz);
//		dryprcrec.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
//End ILPI-65 
}
