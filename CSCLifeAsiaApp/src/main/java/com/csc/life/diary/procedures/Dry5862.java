/**
  *(C) Copyright CSC Corporation Limited 1986 - 2000.
  *    All rights reserved. CSC Confidential.
  *
  *REMARKS.
  *
  *   This will read through UTRNUFM logical and for each
  *   record found with a fund name  equal  to  the  Fund
  *   entity and with the Monies Date less than or  equal
  *   to the Diary Effective Date, it will write  a  unit
  *   deal diary trigger for that policy.
  *
  *   100-START-PROCESSING SECTION
  *   =============================
  *   This will call the sections to read the UTRN record
  *   and process the UTRN record.
  *
  *   1000-READ-UTRN SECTION.
  *   =======================
  *   This will read the first record in UTRNUFM file.
  *
  *   2000-PROCESS-UTRN SECTION.
  *   ==========================
  *   To check if the fund obtained is the required fund.
  *   If it is, then the transaction code is searched  in
  *   T7508. If an entry corresponding to the transaction
  *   code is found in T7508, 3000-CALL-DIARY SECTION  is
  *   called, otherwise next record is read in UTRNUFM.
  *
  *   3000-CALL-DIARY SECTION.
  *   ========================
  *   If item corresponding to  Transaction Code is found,
  *   then, particular contract is softlocked and Diary is
  *   called for the item obtained from T7508.
  *
  ***********************************************************************
  *           AMENDMENT  HISTORY                                        *
  ***********************************************************************
  * DATE.... VSN/MOD  WORK UNIT    BY....                               *
  *                                                                     *
  * 16/07/09  01/01   ZD0179       Utkarsh Pandey                       *
  *           IFA REQ031007 - Unit Price Initiate Unit Deal.            *
  *           Initial Version.                                          *
  *                                                                     *
  * 18/05/10  01/01   ZM0367       CSC - Niamh Fogarty                  *
  *           Bugzilla 1142                                             *
  *           If UTRN not found for Fund then exit processing.          *
  **DD/MM/YY***********
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diary.procedures.Dryproces;
import com.csc.diary.recordstructures.Drycntrec;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.diary.tablestructures.T7508rec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.recordstructures.Dryprclnk;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnufmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Dry5862 extends Maind {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5862");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8).init(0);
	private FixedLengthStringData wsaaDryenttp = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaProcess = new FixedLengthStringData(6).init(SPACES);
	private FixedLengthStringData wsaaEntity = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(8);
	private FixedLengthStringData wsaaDrypecRec = new FixedLengthStringData(621);
	private static final String dacmRec = "DACMREC";
	private static final String itemRec = "ITEMREC";
	private static final String slckRec = "SLCKREC";
	private static final String utrnufmRec = "UTRNUFMREC";
	private FixedLengthStringData tables = new FixedLengthStringData(6);
	private FixedLengthStringData t7508 = new FixedLengthStringData(6).isAPartOf(tables, 0).init("T7508");
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private Batcuprec batcuprec = new Batcuprec();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Drylogrec drylogrec = new Drylogrec();
	private Drycntrec drycntrec = new Drycntrec();
	private Dryrptrec dryrptrec = new Dryrptrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T7508rec t7508rec = new T7508rec();
	private Varcom varcom = new Varcom();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private UtrnufmTableDAM utrnufmIO = new UtrnufmTableDAM();
	private Dryprclnk dryprclnk = new Dryprclnk();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	/**
	* Contains all possible labels used by goTo action.
	*/
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readNext2080,
		exit2090,
		exit3090,
		exit190,
	}

	public Dry5862() {
		super();
	}
	
	/**
	 * ILPI-97
	 */
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	protected void startProcessing100(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true){
			try{
				switch (nextMethod){
				case DEFAULT:
					start110();
				case exit190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start110(){

		wsaaToday.set(SPACES);
		wsaaDrypecRec.set(SPACES);
		wsaaDryenttp.set(SPACES);
		wsaaProcess.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		callProgram(Datcon1.class,datcon1rec.datcon1Rec);
		if(isNE(datcon1rec.statuz, varcom.oK)){
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Table T7508 is read corresponding to the transaction
		* code. If the record is found,diary is called.*/

		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t7508);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemRec);
		SmartFileCode.execute(appVars, itemIO);
		if(isNE(itemIO.getStatuz(),varcom.oK)
				&& isNE(itemIO.getStatuz(),varcom.mrnf)){
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(isNE(itemIO.getStatuz(),varcom.oK)){
			goTo(GotoLabel.exit190);
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		wsaaDryenttp.set(t7508rec.dryenttp01);
		wsaaProcess.set(t7508rec.proces01);

		wsaaDrypecRec.set(drypDryprcRecInner.drypDryprcRec);
		wsaaEntity.set(drypDryprcRecInner.drypEntity);
		wsaaEffectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		readUtrn1000();
		while (isNE(utrnufmIO.getStatuz(), varcom.endp)) {
			processUtrn2000();
		}
		drypDryprcRecInner.drypDryprcRec.set(wsaaDrypecRec);
	}

	protected void readUtrn1000(){
		start1010();
	}

	protected void start1010(){
		/* To read the first unprocessed record in UTRN file. */
		utrnufmIO.setRecKeyData(SPACES);
		utrnufmIO.setRecNonKeyData(SPACES);
		utrnufmIO.setStatuz(varcom.oK);
		utrnufmIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		utrnufmIO.setUnitVirtualFund(wsaaEntity);
		utrnufmIO.setMoniesDate(ZERO);
		utrnufmIO.setChdrnum(SPACES);
		utrnufmIO.setFunction(varcom.begn);
	}

	protected void processUtrn2000(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2010();
				case readNext2080:
					readNext2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start2010(){
		utrnufmIO.setFormat(utrnufmRec);
		SmartFileCode.execute(appVars, utrnufmIO);
		if(isNE(utrnufmIO.getStatuz(),varcom.oK)
				&&!isEQ(utrnufmIO.getStatuz(),varcom.endp)){
			drylogrec.params.set(utrnufmIO.getParams());
			drylogrec.statuz.set(utrnufmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(isNE(utrnufmIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
				||isNE(utrnufmIO.getUnitVirtualFund(), wsaaEntity)
				||isEQ(utrnufmIO.getStatuz(),varcom.endp)){
			utrnufmIO.statuz.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

		/* If the contract is not required to be processed update
		* the control total and read next record.*/
		if(isGT(utrnufmIO.getMoniesDate(), wsaaEffectiveDate)){
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.readNext2080);
		}
		callDiary3000();

		/*Since the contract is processed, update the Control
			  * Total for the record processed.*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

	}

	protected void readNext2080(){
		utrnufmIO.setFunction(varcom.nextr);
	}


	protected void callDiary3000(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3010();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start3010(){
		/*  * If the record corresponding to transaction code is
			  * found in T7508,  softlock the  particular contract
			  * and call the Diary Processing.*/
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.company.set(drypDryprcRecInner.drypCompany);
		sftlockrec.transaction.set(drypDryprcRecInner.drypBatctrcde);
		sftlockrec.user.set("99999");
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(utrnufmIO.getChdrnum());
		callProgram(Sftlock.class,sftlockrec.sftlockRec);

		if(isNE(sftlockrec.statuz,varcom.oK)
				&& isNE(sftlockrec.statuz, "LOCK")){
			drylogrec.params.set(sftlockrec.sftlockRec);
			drylogrec.statuz.set(sftlockrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if(isNE(sftlockrec.statuz, varcom.oK)){
			goTo(GotoLabel.exit3090);
		}
		/*
		 The deffered contracts  are written  in Diary
			  * accumulation file and will be  unlocked    by
			  * a separate subroutine just before COMMIT.
		*/
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(drypDryprcRecInner.drypThreadNumber);
		dacmIO.setCompany(drypDryprcRecInner.drypCompany);
		dacmIO.setRecformat(sftlockrec);
		dacmIO.setDataarea(sftlockrec.sftlockRec);
		dacmIO.setFunction(varcom.writr);
		dacmIO.setFormat(sftlockrec);
		SmartFileCode.execute(appVars, dacmIO);
		if(!isEQ(dacmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
		}

		/** The Diary is stamped for the processing of contract.
		 *
		   MOVE O-K                    TO DRYP-STATUZ.
           SET  ONLINE-MODE            TO TRUE.
          */
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypEntityType.set(wsaaDryenttp);
		drypDryprcRecInner.drypProcCode.set(wsaaProcess);
		drypDryprcRecInner.drypEntity.set(utrnufmIO.getChdrnum());
		callProgram(Dryproces.class,drypDryprcRecInner.drypDryprcRec);
		if(!isEQ(drypDryprcRecInner.drypStatuz,varcom.oK)){
			drylogrec.params.set(drypDryprcRecInner.drypDryprcRec);
			drylogrec.statuz.set(drypDryprcRecInner.drypStatuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
	private static final class DrypDryprcRecInner {

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
		private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
		private FixedLengthStringData drypCnttype1 = new FixedLengthStringData(3).isAPartOf(drypDetailInput1, 0);
		private FixedLengthStringData drypBillfreq1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 3);
		private FixedLengthStringData drypBillchnl1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 5);
		private FixedLengthStringData drypStatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 7);
		private FixedLengthStringData drypPstatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 9);
		private PackedDecimalData drypBtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 11);
		private PackedDecimalData drypPtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 16);
		private PackedDecimalData drypBillcd1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 21);
		private PackedDecimalData drypOccdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 46);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}


	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}


