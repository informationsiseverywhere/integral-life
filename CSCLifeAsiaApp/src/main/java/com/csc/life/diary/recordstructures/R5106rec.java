package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.quipoz.COBOLFramework.COBOLFunctions;

	 /*
      *(c) Copyright Continuum Corporation Ltd.  1986....1995.
      *    All rights reserved.  Continuum Confidential.
      *
      *    This copybook is used to map the R5106 detail fields
      *    onto the DRPT record for the Batch Diary System.
      *
      ***********************************************************************
      *                                                                     *
      * ......... New Version of the Amendment History.                     *
      *                                                                     *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 30/03/00  01/01   DRYAPL       Jacco Landskroon                     *
      *           Initial Version.                                          *
      *                                                                     *
      * 15/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
      *           Retrofit.                                                 *
      *                                                                     *
      **DD/MM/YY*************************************************************
      *
           03  R5106-DATA-AREA             REDEFINES
           		DRYR-GENAREA.
               05  R5106-FUNDAMT           PIC S9(13)V9(02).
               05  R5106-PRICEUSED         PIC S9(09)V9(05).
               05  R5106-NOFRUNTS          PIC S9(13)V9(02).
               05  R5106-PRICEDT           PIC X(10).
               05  R5106-DMDUNTS           PIC S9(16)V9(02).
               05  FILLER                  PIC X(420).
           03  R5106-SORT-KEY.
               05  R5106-VRTFND            PIC X(04).
               05  R5106-UNITYP            PIC X(01).
               05  R5106-CHDRNUM           PIC X(08).
               05  R5106-BATCTRCDE         PIC X(04).
               05  FILLER                  PIC X(21).
           03  R5106-REPORT-NAME           PIC X(10)
                                           VALUE 'R5106     '.
      */

public class R5106rec extends ExternalData {

	public FixedLengthStringData dataArea = new FixedLengthStringData(481);
	public ZonedDecimalData fundamt = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 0);
	public ZonedDecimalData priceused = new ZonedDecimalData(9, 5).isAPartOf(dataArea, 13);
	public ZonedDecimalData nofrunts = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 22);
	public FixedLengthStringData pricedt = new FixedLengthStringData(10).isAPartOf(dataArea, 35);
	public ZonedDecimalData dmdunts = new ZonedDecimalData(16, 2).isAPartOf(dataArea, 45);
	public ZonedDecimalData filler = new ZonedDecimalData(420, 0).isAPartOf(dataArea, 61, FILLER).setUnsigned();

	public FixedLengthStringData sortKey = new FixedLengthStringData(38);
	public FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(sortKey, 0);
	public FixedLengthStringData unityp = new FixedLengthStringData(1).isAPartOf(sortKey, 4);
	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 5);
	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(sortKey, 13);
	public FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(sortKey, 17, FILLER);

	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5106     ");

	@Override
	public void initialize() {
		COBOLFunctions.initialize(fundamt);
		COBOLFunctions.initialize(priceused);
		COBOLFunctions.initialize(nofrunts);
		COBOLFunctions.initialize(pricedt);
		COBOLFunctions.initialize(dmdunts);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(vrtfnd);
		COBOLFunctions.initialize(unityp);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(batctrcde);
		COBOLFunctions.initialize(filler1);
	}

	@Override
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			fundamt.isAPartOf(baseString, true);
   			priceused.isAPartOf(baseString, true);
   			nofrunts.isAPartOf(baseString, true);
   			pricedt.isAPartOf(baseString, true);
   			dmdunts.isAPartOf(baseString, true);
   			filler.isAPartOf(baseString, true);
   			vrtfnd.isAPartOf(baseString, true);
   			unityp.isAPartOf(baseString, true);
   			chdrnum.isAPartOf(baseString, true);
   			batctrcde.isAPartOf(baseString, true);
   			filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
		return baseString;
	}

}
