package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:50
 * Description:
 * Copybook name: R5359REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5359rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(496);
  	public FixedLengthStringData btdate = new FixedLengthStringData(10).isAPartOf(dataArea, 0);
  	public ZonedDecimalData linstamt = new ZonedDecimalData(11, 2).isAPartOf(dataArea, 9);
  	public ZonedDecimalData maxprem = new ZonedDecimalData(11, 2).isAPartOf(dataArea, 20);
  	public ZonedDecimalData susamt = new ZonedDecimalData(11, 2).isAPartOf(dataArea, 31);
  	public FixedLengthStringData filler = new FixedLengthStringData(454).isAPartOf(dataArea, 42, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(534);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 496);
  	public ZonedDecimalData payrseqno = new ZonedDecimalData(1, 0).isAPartOf(sortKey, 504).setUnsigned();
  	public FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(sortKey, 505, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5359     ");


	public void initialize() {
		COBOLFunctions.initialize(btdate);
		COBOLFunctions.initialize(linstamt);
		COBOLFunctions.initialize(maxprem);
		COBOLFunctions.initialize(susamt);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(payrseqno);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		btdate.isAPartOf(baseString, true);
    		linstamt.isAPartOf(baseString, true);
    		maxprem.isAPartOf(baseString, true);
    		susamt.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		payrseqno.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}