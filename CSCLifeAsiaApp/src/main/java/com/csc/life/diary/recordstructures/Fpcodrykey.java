package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:50
 * Description:
 * Copybook name: FPCODRYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Fpcodrykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData fpcodryFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData fpcodryKey = new FixedLengthStringData(256).isAPartOf(fpcodryFileKey, 0, REDEFINE);
  	public FixedLengthStringData fpcodryChdrcoy = new FixedLengthStringData(1).isAPartOf(fpcodryKey, 0);
  	public FixedLengthStringData fpcodryChdrnum = new FixedLengthStringData(8).isAPartOf(fpcodryKey, 1);
  	public FixedLengthStringData fpcodryLife = new FixedLengthStringData(2).isAPartOf(fpcodryKey, 9);
  	public FixedLengthStringData fpcodryCoverage = new FixedLengthStringData(2).isAPartOf(fpcodryKey, 11);
  	public FixedLengthStringData fpcodryRider = new FixedLengthStringData(2).isAPartOf(fpcodryKey, 13);
  	public PackedDecimalData fpcodryPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(fpcodryKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(fpcodryKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(fpcodryFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		fpcodryFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}