/*
 * File: Dry5353.java
 * Date: March 26, 2014 3:00:56 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5353.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Rdockey;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdbilTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.procedures.NlgcalcPojo;
import com.csc.life.newbusiness.procedures.NlgcalcUtils;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.dataaccess.TrwpTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.regularprocessing.recordstructures.Ddbtallrec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.life.terminationclaims.tablestructures.Td5j2rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Csdopt;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*                       COLLECTIONS.
*                       -----------
*
* This program collects any monies available from suspense
* accounts  to  pay  for  outstanding  instalments (LINSPF).
* When  instalments  are paid the appropriate record has its
* payment  flag  set to 'P' (Paid).  All accounting relevant
* to suspense  and  payment accounts is also handled in this
* program.
*
*    In  addition,  the  payment  of  instalments  can  also
* generate generic processing such as commission payment and
* unit   allocation   which  is  driven  by table parameters.
*
* Control totals used in this program:
*
*    01  -  No. of LINS  records read
*    02  -  No LINS recs collected
*    03  -  Tot. LINS amts collected
*    04  -  No of LINS not collected
*    05  -  Tot. LINS amts not collected
*    06  -  No LINS susp < cbillamt
*    07  -  No. LINS fail tolerance frequency
*    08  -  No. LINS CHDR locked
*    09  -  No  LINS invalid CHDR
*    10  -  No  LINS payrseqno error
*    11  -  No  DD collect subroutines missing
*    12  -  No  LINS invalid coverages
*
*****************************************************************
*
* </pre>
*/
public class Dry5353 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5353");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* Calculated amounts*/
	private ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(17, 2);
	private PackedDecimalData wsaaSuspCbillDiff = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaToleranceAllowed = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAmtPaid = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaImRatio = new PackedDecimalData(18, 9);
	private PackedDecimalData wsaaCntcurrReceived = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNetCbillamt = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaDivRequired = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaApaRequired = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaAcblSacscurbal = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaCommethNo = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData wsaaOldInstfrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaNextInstfrom = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaAplspto = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaWaiverAvail = new ZonedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotTax = new PackedDecimalData(17, 2);
		/* WSAA-TX-KEYS */
	private FixedLengthStringData wsaaTxLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTxCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTxRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaTxPlansfx = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaTxCrtable = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaPremSettleFlag = new FixedLengthStringData(1);
	private Validator premSettlement = new Validator(wsaaPremSettleFlag, "Y");

	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);

	private FixedLengthStringData wsaaToleranceChk = new FixedLengthStringData(1);
	private Validator toleranceLimit1 = new Validator(wsaaToleranceChk, "1");
	private Validator toleranceLimit2 = new Validator(wsaaToleranceChk, "2");

	private FixedLengthStringData wsaaAgtTerminateFlag = new FixedLengthStringData(1);
	private Validator agtTerminated = new Validator(wsaaAgtTerminateFlag, "Y");
	private Validator agtNotTerminated = new Validator(wsaaAgtTerminateFlag, "N");
		/*  Subscripts*/
	private PackedDecimalData wsaaGlSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(4, 0).init(ZERO);
	private ZonedDecimalData wsaaSubprogIx = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPrmtoln = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaMaxamt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaSfind = new FixedLengthStringData(1);
		/* WSAA-DATES */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler1 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler1, 6).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTday = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTomorrow = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrcoy = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(21);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqNum = new ZonedDecimalData(2, 0).isAPartOf(filler3, 0).setUnsigned();
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaRldgagnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAgntChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgagnt, 0);
	private FixedLengthStringData wsaaAgntLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 8);
	private FixedLengthStringData wsaaAgntCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 10);
	private FixedLengthStringData wsaaAgntRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 12);
	private FixedLengthStringData wsaaAgntPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 14);

		/* Status indicators*/
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaPayrseqnoError = new FixedLengthStringData(1).init("N");
	private Validator payrseqnoError = new Validator(wsaaPayrseqnoError, "Y");

	private FixedLengthStringData wsaaFreqFound = new FixedLengthStringData(1).init("N");
	private Validator freqFound = new Validator(wsaaFreqFound, "Y");

	private FixedLengthStringData wsaaCollection = new FixedLengthStringData(1);
	private Validator collectionNotDone = new Validator(wsaaCollection, "N");
	private Validator collectionDone = new Validator(wsaaCollection, "Y");

	private FixedLengthStringData wsaaLinsStatus = new FixedLengthStringData(1).init("N");
	private Validator linsBad = new Validator(wsaaLinsStatus, "N");
	private Validator linsGood = new Validator(wsaaLinsStatus, "Y");
		/* Arrays to store regularly-referenced data*/
	private static final int wsaaCovrSize = 500;

		/* WSAA-COVR-ARRAY */
	private FixedLengthStringData[] wsaaCovrRec = FLSInittedArray (500, 39);//IJTI-1727
	private FixedLengthStringData[] wsaaCovrKey = FLSDArrayPartOfArrayStructure(17, wsaaCovrRec, 0);
	private FixedLengthStringData[] wsaaCovrChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaCovrKey, 0);
	private FixedLengthStringData[] wsaaCovrLife = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 8);
	private FixedLengthStringData[] wsaaCovrCovrg = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 10);
	private FixedLengthStringData[] wsaaCovrRider = FLSDArrayPartOfArrayStructure(2, wsaaCovrKey, 12);
	private PackedDecimalData[] wsaaCovrPlanSuff = PDArrayPartOfArrayStructure(4, 0, wsaaCovrKey, 14);
	private FixedLengthStringData[] wsaaCovrData = FLSDArrayPartOfArrayStructure(22, wsaaCovrRec, 17);//IJTI-1727
	private FixedLengthStringData[] wsaaCovrCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCovrData, 0);
	private FixedLengthStringData[] wsaaCovrValid = FLSDArrayPartOfArrayStructure(1, wsaaCovrData, 4);
	private ZonedDecimalData[] wsaaCovrProratePrem = ZDArrayPartOfArrayStructure(17, 2, wsaaCovrData, 5);//IJTI-1727
	
		/*  Storage for T5645 table items.*/
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	private FixedLengthStringData wsaaFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaPrmtol = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaMaxAmount = new ZonedDecimalData(6, 2);

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(3).isAPartOf(wsaaT5667Key, 4);
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaT5667Key, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);
	private FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(wsaaT6654Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaTrcdeCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 0);
	private FixedLengthStringData wsaaCovrlnbCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 4);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysLinskey = new FixedLengthStringData(21).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysLinskey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysLinskey, 2);
	private ZonedDecimalData wsysInstfrom = new ZonedDecimalData(8, 0).isAPartOf(wsysLinskey, 13).setUnsigned();
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(76).isAPartOf(wsysSystemErrorParams, 21);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfq, 0, REDEFINE).setUnsigned();
	private static final int wsaaAgcmIxSize = 100;
	private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();

		/* WSAA-AGCM-SUMMARY */
	private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray (100, 8515);
	private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
	private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
	private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
	private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
	private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
	private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 17, wsaaAgcmRec, 15);
	private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3, UNSIGNED_TRUE);
	private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");
		/* Flag, indicate the previous LINS is successful collected
		 or not.                                                         */
	private String wsaaPrevLins = "";
		/* WSAA-LAST-COVER */
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLastLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLastCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLastRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaLastPlanSuff = new PackedDecimalData(4, 0);
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String e351 = "E351";
	private static final String e308 = "E308";
	private static final String e103 = "E103";
	private IntegerData wsaaCovrIx = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	private HdivTableDAM hdivIO = new HdivTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private TaxdbilTableDAM taxdbilIO = new TaxdbilTableDAM();
	private TrwpTableDAM trwpIO = new TrwpTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Rdockey wsaaRdockey = new Rdockey();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Csdopt csdopt = new Csdopt();
	private Ddbtallrec ddbtallrec = new Ddbtallrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Prasrec prasrec = new Prasrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Alocnorec alocnorec = new Alocnorec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T3695rec t3695rec = new T3695rec();
	private T5644rec t5644rec = new T5644rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5667rec t5667rec = new T5667rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6654rec t6654rec = new T6654rec();
	private T6687rec t6687rec = new T6687rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T5687rec t5687rec = new T5687rec();
	private T5534rec t5534rec = new T5534rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	
	//IJTI-1727 starts
	private boolean prmhldtrad = false;
	private boolean gstOnCommFlag = false;
	private boolean reinstflag = false;
	protected boolean isShortTerm = false;
	private Incrpf currIncrPro = null;
	private boolean incrFoundPro = false;
	private boolean isFoundPro = false;
	private boolean isEvntTrgrFlgOn = false;
	private boolean isDMSFlgOn = false;	
	private boolean riskPremflag = false;
	private Map<String, Itempf> ta524Map;
	private Map<String, List<Itempf>> td5j2Map = new HashMap<>();
	private Map<String, List<Itempf>> td5j1Map = new HashMap<>();
	private Map<String, List<Itempf>> t5567Map = new HashMap<>();
	private Td5j1rec td5j1rec = new Td5j1rec();
	private Td5j2rec td5j2rec = new Td5j2rec();
	private T5567rec t5567rec = new T5567rec();
	private Prmhpf prmhpf;
	private static final String TA85 = "TA85";
	private Ptrnpf ptrnpfReinstate;
	private Map<Integer, List<Incrpf>> incrDatedMap;
	private Map<String, List<Rskppf>> covrRiskPremMap = new HashMap<>();
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private IncrpfDAO incrpfDAO =  getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private PackedDecimalData wsaaWaiveAmtPaid = new PackedDecimalData(17, 2).init(ZERO);
    private PackedDecimalData wsaaCompErnInitCommGst = new PackedDecimalData(17, 2);
    private PackedDecimalData wsaaCompErnRnwlCommGst = new PackedDecimalData(17, 2);	
	private BigDecimal wsaaXPrmdepst;
	private ZonedDecimalData wsaapolFee = new ZonedDecimalData(8, 2);
	private Calprpmrec calprpmrec = new Calprpmrec();
	private Premiumrec premiumrec = new Premiumrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private AcblpfDAO acblpfDAO = getApplicationContext().getBean("acblpfDAO", AcblpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmDAO", AgcmpfDAO.class);
	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private NlgcalcUtils nlgcalcUtils = getApplicationContext().getBean("nlgcalcUtils", NlgcalcUtils.class);
	private List<Rskppf> insertRiskPremList = new ArrayList<>();
	//IJTI-1727 ends
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private static final String t6654 = "T6654";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit999a,
		exit999b,
		failTolerance1050,
		exit1090,
		advancePremDep1820,
		exit1890,
		skipBonusWorkbench2150,
		exit2190,
		exit2490,
		exit6090,
		b220Call,
		b280Next,
		b290Exit,
		b320Loop,
		b380Next,
		b390Exit,
		b60aNextRec,
		b60aExit,
		b710NextRec,
		b710Exit
	}

	public Dry5353() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void startProcessing100()
	{
		start110();
	}

protected void start110()
	{
		initialise200();
		if (!validContract.isTrue()) {
			finish4000();
			return ;
		}
		initializeFeatures(drypDryprcRecInner.drypCompany.trim());//IJTI-1727
		wsaaPrevLins = "Y";
		
		//IJTI-1727 starts
		Linspf linspf = new Linspf();
		linspf.setChdrcoy(drypDryprcRecInner.drypCompany.trim());
		linspf.setChdrnum(drypDryprcRecInner.drypEntity.trim());
		linspf.setValidflag("1");
		linspf.setPayflag("P");
		linspf.setBillchnl("N");
		List<Linspf> linspfList = linspfDAO.getLinspfForCollection(linspf);
		
		linspfList.forEach((lins)->{
			processLinsRecords(lins);
		});

		if(riskPremflag) {
			updateRecords();
		}
		
		//IJTI-1727 ends
		finish4000();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		wsaaApaRequired.set(ZERO);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		/*  Load Company Defaults                                          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(drypDryprcRecInner.drypCompany);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*    IF ITEM-STATUZ           NOT = O-K AND MRNF          <V73L01>*/
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTday.set(datcon1rec.intDate);
		/* Fetch the CHDR record for the entity parameters passed.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(Varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Check if contract valid*/
		validateContract300();
		if (!validContract.isTrue()) {
			drycntrec.contotNumber.set(controlTotalsInner.ct09);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		/* Get transaction description.*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(drypDryprcRecInner.drypCompany);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(drypDryprcRecInner.drypBatctrcde);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/* Initialise common LIFA, LIFR, RNLA and PTRN fields*/
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifacmvrec.lifacmvRec.set(SPACES);
		rnlallrec.rnlallRec.set(SPACES);
		lifacmvrec.jrnseq.set(0);
		//ILPI-158
//		rnlallrec.effdate.set(drypDryprcRecInner.drypRunDate);
//		rnlallrec.moniesDate.set(drypDryprcRecInner.drypRunDate);
		rnlallrec.effdate.set(drypDryprcRecInner.drypEffectiveDate);
		rnlallrec.moniesDate.set(drypDryprcRecInner.drypEffectiveDate);
		
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		lifrtrnrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifrtrnrec.rldgcoy.set(drypDryprcRecInner.drypBatccoy);
		lifrtrnrec.genlcoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypBatccoy);
		rnlallrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		rnlallrec.language.set(drypDryprcRecInner.drypLanguage);
		lifrtrnrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		rnlallrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		lifrtrnrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		rnlallrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		lifrtrnrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		rnlallrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		lifrtrnrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		rnlallrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		lifrtrnrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		rnlallrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		lifrtrnrec.rcamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifacmvrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifrtrnrec.user.set(0);
		lifacmvrec.user.set(0);
		rnlallrec.user.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		varcom.vrcmTime.set(getCobolTime());
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.threadNumber.set(drypDryprcRecInner.drypThreadNumber);
		lifrtrnrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		wsaaAplspto.set(ZERO);
		/* Load account codes*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.begn);
		wsaaT5645Sub.set(1);
		wsaaT5645Offset.set(1);
		while ( !(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT5645400();
		}

		/* Retrieve the suspense account sign from T3695 using*/
		/* the SUSPENSE sacstype from T5645.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(wsaaT5645Sacstype[1]);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		collectionNotDone.setTrue();
		/* Initialise these two fields since they're used to*/
		/* determine if another instalment collection is to be*/
		/* processed.*/
		wsaaOldInstfrom.set(varcom.vrcmMaxDate);
	}

	//IJTI-1727 starts
	private void initializeFeatures(String company) {
		gstOnCommFlag = FeaConfg.isFeatureExist(company, "CTISS007", appVars, "IT");
		prmhldtrad = FeaConfg.isFeatureExist(company, "CSOTH010", appVars, "IT");
		reinstflag = FeaConfg.isFeatureExist(company, "CSLRI003", appVars, "IT");
		riskPremflag = FeaConfg.isFeatureExist(company, "NBPRP094", appVars, "IT");
		isEvntTrgrFlgOn = FeaConfg.isFeatureExist("0", "SAFCF002", appVars, "IT");
		isDMSFlgOn =  FeaConfg.isFeatureExist("0", "SAFCF001", appVars, "IT");
		if(prmhldtrad) {
			ta524Map = itemDAO.getItemMap("IT", company, "TA524");
		}
		
		if(reinstflag){
        	td5j2Map = itemDAO.loadSmartTable("IT", company, "TD5J2");	
        	td5j1Map = itemDAO.loadSmartTable("IT", company, "TD5J1");	
        }
		
		if(riskPremflag) {
			t5567Map = itemDAO.loadSmartTable("IT", company, "T5567");
		}
	}
	
	//IJTI-1727 ends
protected void validateContract300()
	{
		start310();
	}

protected void start310()
	{
		/* For the contract retrieved we must look up the contract type*/
		/* details on T5688.*/
		/*  Read contract type details.*/
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK)
		&& isNE(itdmIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), Varcom.endp)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isLT(itdmIO.getItmto(), chdrlifIO.getOccdate())) {
			drylogrec.statuz.set(e308);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Move the GENAREA to the T5688 copybook so that the values*/
		/* are avaliable later in the program.*/
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Fetch Contract Statii.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Validate the statii of the contract*/
		wsaaValidContract.set("N");
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIx.toInt()], chdrlifIO.getStatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
		if (!validContract.isTrue()) {
			return ;
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnPremStat[wsaaIx.toInt()], chdrlifIO.getPstatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
	}

protected void loadT5645400()
	{
		start410();
	}

protected void start410()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(drypDryprcRecInner.drypCompany, itemIO.getItemcoy())
		|| isNE(itemIO.getItemtabl(), tablesInner.t5645)
		|| isNE(itemIO.getItemitem(), wsaaProg)
		|| isNE(itemIO.getStatuz(), Varcom.oK)) {
			if (isEQ(itemIO.getFunction(), Varcom.begn)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			else {
				itemIO.setStatuz(Varcom.endp);
				return ;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Sub.set(1);
		if (isGT(wsaaT5645Sub, wsaaT5645Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(tablesInner.t5645);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		while ( !(isGT(wsaaT5645Sub, 15))) {
			wsaaT5645Cnttot[wsaaT5645Offset.toInt()].set(t5645rec.cnttot[wsaaT5645Sub.toInt()]);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
			wsaaT5645Sub.add(1);
			wsaaT5645Offset.add(1);
		}

		itemIO.setFunction(Varcom.nextr);
	}

	protected void processLinsRecords(Linspf linspf)
	{
		/* Determine if there is another instalment for the same*/
		/* contract which requires collection. Multi-payers paying*/
		/* for the same instalment dates will be processed, but*/
		/* a change of instalment date will halt the processing.*/
		if (isNE(wsaaOldInstfrom, varcom.vrcmMaxDate)
		&& isGT(linspf.getInstfrom(), wsaaOldInstfrom)) {
			return ;
		}
		/* Set up the key for the SYSR- copybook should a system error*/
		/* for this instalment occur.*/
		wsysChdrcoy.set(linspf.getChdrcoy());//IJTI-1727
		wsysChdrnum.set(linspf.getChdrnum());//IJTI-1727
		wsysInstfrom.set(linspf.getInstfrom());//IJTI-1727
		drycntrec.contotNumber.set(controlTotalsInner.ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		if (isEQ(wsaaPrevLins, "N")) {
			/*  Log no. LINS  still short of the cbillamt.                     */
			drycntrec.contotNumber.set(controlTotalsInner.ct06);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			/*  Increment the total of outstanding premiums.                   */
			drycntrec.contotNumber.set(controlTotalsInner.ct05);
			drycntrec.contotValue.set(linspf.getInstamt06());//IJTI-1727
			d000ControlTotals();
			/*  Log no. LINS not collected.                                    */
			drycntrec.contotNumber.set(controlTotalsInner.ct04);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		readPayr600(linspf);//IJTI-1727
		readClrf700(linspf);//IJTI-1727
		/*  Calculate tax                                                  */
		b600GetTax(linspf);//IJTI-1727
		processFeatures(linspf);//IJTI-1727
		
		/*  Check for tax relief*/
		prasrec.taxrelamt.set(ZERO);
		wsaaNetCbillamt.set(linspf.getCbillamt());//IJTI-1727
		if (isNE(payrIO.getTaxrelmth(), SPACES)) {
			calcTaxRelief800(linspf);//IJTI-1727
		}
		/*  If waiver-holding is required, validate the amount required    */
		/*  is already in the holding account.                             */
		if (isNE(linspf.getInstamt05(), ZERO)) {
			waiverHolding7000(linspf);//IJTI-1727
			if (isGT(wsaaWaiverAvail, linspf.getInstamt05())) {//IJTI-1727
				/*  Insufficient waiver holding                                    */
				drycntrec.contotNumber.set(controlTotalsInner.ct13);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				/*  Log no. LINS not collected.                                    */
				drycntrec.contotNumber.set(controlTotalsInner.ct04);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				return ;
			}
		}
		/* Check money is available*/
		if (isEQ(wsaaT5645Sign[7], "-")) {
			compute(wsaaWaiverAvail, 2).set(mult(wsaaWaiverAvail, -1));
		}

		if (isGTE(wsaaWaiverAvail, wsaaNetCbillamt)) {
			wsaaSuspAvail.set(wsaaWaiverAvail);
			wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
		} else {
			suspenseAvail900(linspf);
			if (isGT(wsaaWaiverAvail, ZERO)) {
				wsaaSuspAvail.add(wsaaWaiverAvail);
				wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
			}
		}
		/*  If insufficient suspense, pull Advance Premium Deposit (APA)   */
		/*  available into suspense before checking for tolerance.         */
		/*  Check tolerance if there is insufficient money.                */
		linsGood.setTrue();
		if (isLTE(wsaaNetCbillamt, wsaaSuspAvail)) {
			linspf.setInstamt03(BigDecimal.ZERO);
		} else {
			cashDivAvail900a(linspf);//IJTI-1727
			if (isGT(wsaaNetCbillamt, wsaaSuspAvail)) {
				apaAvail900b(linspf);//IJTI-1727
			}
			
			wsaaWaiveAmtPaid.set(wsaaWaiverAvail);
	        wsaaAmtPaid.set(sub(wsaaSuspAvail,wsaaWaiveAmtPaid));
			checkAgent900c();
			calcTolerance1000(linspf);//IJTI-1727
		}
		if (linsGood.isTrue()) {
			update1500(linspf);//IJTI-1727
		}
		wsaaOldInstfrom.set(linspf.getInstfrom());//IJTI-1727
	}

	//IJTI-1727 starts
	private void processFeatures(Linspf linspf) {
		if (prmhldtrad && ta524Map.get(chdrlifIO.getCnttype().toString()) != null) {
			prmhpf = prmhpfDAO.getPrmhRecord(linspf.getChdrcoy(), linspf.getChdrnum());
		} else {
			prmhpf = null;
		}

		if (prmhpf != null && drypDryprcRecInner.drypRunDate.toInt() <= prmhpf.getTodate()) {
			wsaaNetCbillamt.set(linspf.getProramt().add(linspf.getProrcntfee()));
		} else {
			wsaaNetCbillamt.set(linspf.getCbillamt());
		}
		
		if(reinstflag) {
        	markProrated(linspf);
		}

	}
	
	private void markProrated(Linspf linspf) {
		List<Ptrnpf> ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(linspf.getChdrcoy(), linspf.getChdrnum());
		for (Ptrnpf ptrn : ptrnrecords) {
			if (isNE(ptrn.getBatctrcde(), TA85) && isNE(ptrn.getBatctrcde(), "B522")) {
				continue;
			}
			if (isEQ(ptrn.getBatctrcde(), "B522")) {
				break;
			}
			if (isEQ(ptrn.getBatctrcde(), TA85)) {
				if(readTd5j1AndCheckConditions(linspf)) {
					break;
				}
			}
		}
	}
	
	private boolean readTd5j1AndCheckConditions(Linspf linspf) {
		boolean isSubroutineEmpty = false;
		readTd5j1(chdrlifIO.getCnttype().trim());
		if (isNE(td5j1rec.td5j1Rec, SPACES)) {
			if (isEQ(td5j1rec.subroutine, SPACES))
				isSubroutineEmpty = true;
			else {
				isFoundPro = true;
				ptrnpfReinstate = ptrnpfDAO.getPtrnData(linspf.getChdrcoy(), linspf.getChdrnum(),
						TA85);
				incrDatedMap = new HashMap<Integer, List<Incrpf>>();
				incrDatedMap = incrpfDAO.getIncrDatedMap(drypDryprcRecInner.drypCompany.trim(),
						linspf.getChdrnum());
			}
		}
		
		return isSubroutineEmpty;
	}
	
	private void readTd5j1(String cnttype){
		if (td5j1Map.containsKey(cnttype)){	
			List<Itempf> itempfList = td5j1Map.get(cnttype);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				filterTd5j1ByDates(iterator.next());	
			}		
		}
		else {
			td5j1rec.td5j1Rec.set(SPACES);
		}	
	}
	
	private void filterTd5j1ByDates(Itempf itempf) {
		if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
			if ((Integer.parseInt(drypDryprcRecInner.drypRunDate.toString()) >= Integer
					.parseInt(itempf.getItmfrm().toString()))
					&& Integer.parseInt(drypDryprcRecInner.drypRunDate.toString()) <= Integer
							.parseInt(itempf.getItmto().toString())){
				td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
			}
		}else{
			td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
		}		
	}

	//IJTI-1727 ends

protected void readPayr600(Linspf linspf)//IJTI-1727
	{
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(linspf.getChdrcoy());
		payrIO.setChdrnum(linspf.getChdrnum());
		payrIO.setPayrseqno(linspf.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.params.set(payrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaAplspto.set(payrIO.getAplspto());
	}


protected void readClrf700(Linspf linspf)//IJTI-1727
	{
		clrfIO.setForepfx(chdrlifIO.getChdrpfx());
		clrfIO.setForecoy(linspf.getChdrcoy());
		wsaaChdrnum.set(linspf.getChdrnum());
		wsaaPayrseqno.set(linspf.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(clrfIO.getStatuz());
			drylogrec.params.set(clrfIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaPayrnum.set(clrfIO.getClntnum());
		wsaaPayrcoy.set(clrfIO.getClntcoy());
	}


protected void calcTaxRelief800(Linspf linspf)//IJTI-1727
	{
		/* Retrieve the tax relief subroutine.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(payrIO.getTaxrelmth());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6687rec.t6687Rec.set(itemIO.getGenarea());
		if (isEQ(t6687rec.taxrelsub, SPACES)) {
			return ;
		}
		prasrec.cnttype.set(chdrlifIO.getCnttype());
		prasrec.clntnum.set(clrfIO.getClntnum());
		prasrec.clntcoy.set(clrfIO.getClntcoy());
		prasrec.incomeSeqNo.set(payrIO.getIncomeSeqNo());
		prasrec.taxrelmth.set(payrIO.getTaxrelmth());
		prasrec.effdate.set(linspf.getBillcd());
		prasrec.company.set(linspf.getChdrcoy());
		prasrec.grossprem.set(linspf.getInstamt06());
		prasrec.grossprem.subtract(wsaaTotTax);
		prasrec.statuz.set(Varcom.oK);
		callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
		if (isNE(prasrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(prasrec.statuz);
			drylogrec.subrname.set(t6687rec.taxrelsub);
			wsysSysparams.set(prasrec.prascalcRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		zrdecplrec.amountIn.set(prasrec.taxrelamt);
		zrdecplrec.currency.set(linspf.getBillcurr());
		c000CallRounding();
		prasrec.taxrelamt.set(zrdecplrec.amountOut);
		compute(wsaaNetCbillamt, 2).set((sub(linspf.getCbillamt(), prasrec.taxrelamt)));
	}


protected void suspenseAvail900(Linspf linspf)//IJTI-1727
	{
		acblIO.setRldgacct(linspf.getChdrnum());
		acblIO.setRldgcoy(linspf.getChdrcoy());
		acblIO.setOrigcurr(linspf.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[1]);
		acblIO.setSacstyp(wsaaT5645Sacstype[1]);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK)
		&& isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(acblIO.getStatuz());
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaSuspAvail.set(0);
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(0, acblIO.getSacscurbal()));
			}
			else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
	}


protected void cashDivAvail900a(Linspf linspf)//IJTI-1727
	{
		try {
			doubleCheck910a();
			checkDivAvailable950a(linspf);
			checkDivAmount960a(linspf);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void doubleCheck910a()
	{
		wsaaDivRequired.set(ZERO);
		if (isLTE(wsaaNetCbillamt, wsaaSuspAvail)) {
			goTo(GotoLabel.exit999a);
		}
	}

protected void checkDivAvailable950a(Linspf linspf)//IJTI-1727
	{
		wsaaPremSettleFlag.set("N");
		hcsdIO.setChdrcoy(linspf.getChdrcoy());
		hcsdIO.setChdrnum(linspf.getChdrnum());
		hcsdIO.setLife("01");
		hcsdIO.setCoverage("01");
		hcsdIO.setRider("00");
		hcsdIO.setPlanSuffix(0);
		hcsdIO.setFunction(Varcom.readr);
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), Varcom.oK)
		&& isNE(hcsdIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(hcsdIO.getParams());
			drylogrec.statuz.set(hcsdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(hcsdIO.getStatuz(), Varcom.oK)) {
			csdopt.divOption.set(hcsdIO.getZdivopt());
			if (csdopt.csdPremSettlement.isTrue()) {
				wsaaPremSettleFlag.set("Y");
			}
		}
		if (!premSettlement.isTrue()) {
			goTo(GotoLabel.exit999a);
		}
	}

protected void checkDivAmount960a(Linspf linspf)//IJTI-1727
	{
		acblIO.setRldgcoy(linspf.getChdrcoy());
		acblIO.setRldgacct(linspf.getChdrnum());
		acblIO.setOrigcurr(linspf.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[34]);
		acblIO.setSacstyp(wsaaT5645Sacstype[34]);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK)
		&& isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(acblIO.getStatuz());
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaAcblSacscurbal.set(0);
		}
		else {
			if (isEQ(wsaaT5645Sign[34], "-")) {
				wsaaAcblSacscurbal.set(acblIO.getSacscurbal());
			}
			else {
				compute(wsaaAcblSacscurbal, 2).set(sub(0, acblIO.getSacscurbal()));
			}
		}
		compute(wsaaDivRequired, 2).set(sub(wsaaNetCbillamt, wsaaSuspAvail));
		if (isLT(wsaaAcblSacscurbal, wsaaDivRequired)) {
			wsaaDivRequired.set(wsaaAcblSacscurbal);
		}
		wsaaSuspAvail.add(wsaaDivRequired);
	}

protected void apaAvail900b(Linspf linspf)//IJTI-1727
	{
		try {
			doubleCheck910b();
			readPrincipalApa(linspf);
			readInterestAccrue(linspf);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void doubleCheck910b()
	{
		wsaaApaRequired.set(ZERO);
		if (isLTE(wsaaNetCbillamt, wsaaSuspAvail)) {
			goTo(GotoLabel.exit999b);
		}
		
		//IJTI-1727 starts
		wsaaXPrmdepst = BigDecimal.ZERO;
		if (isEQ(wsaaT5645Sacscode[37], SPACES) || isEQ(wsaaT5645Sacstype[37], SPACES)
				|| isEQ(wsaaT5645Sacscode[38], SPACES) || isEQ(wsaaT5645Sacstype[38], SPACES)) {
			drylogrec.statuz.set("G450");
			drylogrec.params.set("T5645");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		//IJTI-1727 ends
	}

	//IJTI-1727 starts
	private void readPrincipalApa(Linspf linspf) {
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgcoy(linspf.getChdrcoy());
		acblpf.setRldgacct(linspf.getChdrnum().trim()+"%");
		acblpf.setSacscode(wsaaT5645Sacscode[37].toString());
		acblpf.setSacstyp(wsaaT5645Sacstype[37].toString());
		List<Acblpf> acblpfList = acblpfDAO.findByCriteria(acblpf, "RLDGACCT ASC, ORIGCURR ASC, UNIQUE_NUMBER ASC",
				Arrays.asList("rldgcoy", "rldgacct", "sacscode", "sacstyp"), Arrays.asList("=", " like ", "=", "="));
		
		acblpfList.forEach((acbl) -> {
			if (isLT(acbl.getSacscurbal(), 0)) {
				acbl.setSacscurbal(acbl.getSacscurbal().negate());
			}
			wsaaXPrmdepst = wsaaXPrmdepst.add(acbl.getSacscurbal());
		});
	}
	
	private void readInterestAccrue(Linspf linspf) {
		Acblpf acblpf = new Acblpf();
		acblpf.setRldgcoy(linspf.getChdrcoy());
		acblpf.setRldgacct(linspf.getChdrnum().trim()+"%");
		acblpf.setSacscode(wsaaT5645Sacscode[38].toString());
		acblpf.setSacstyp(wsaaT5645Sacstype[38].toString());
		List<Acblpf> acblpfList = acblpfDAO.findByCriteria(acblpf, "RLDGACCT ASC, ORIGCURR ASC, UNIQUE_NUMBER ASC",
				Arrays.asList("rldgcoy", "rldgacct", "sacscode", "sacstyp"), Arrays.asList("=", " like ", "=", "="));
		
		acblpfList.forEach((acbl) -> {
			if (isLT(acbl.getSacscurbal(), 0)) {
				acbl.setSacscurbal(acbl.getSacscurbal().negate());
			}
			wsaaXPrmdepst = wsaaXPrmdepst.add(acbl.getSacscurbal());
		});
		
		compute(wsaaApaRequired, 2).set(sub(wsaaNetCbillamt, wsaaSuspAvail));
		if (isLT(wsaaXPrmdepst, wsaaApaRequired)) {
			wsaaApaRequired.set(wsaaXPrmdepst);
		}
		wsaaSuspAvail.add(wsaaApaRequired);
	}
	//IJTI-1727 ends

protected void checkAgent900c()
	{
		readAglf910c();
	}

	/**
	* <pre>
	* This paragraph checks for agent termination status. If
	* the agent is terminated at the point of collection, 2nd
	* premium shortfall limit is not applicable.
	* </pre>
	*/
protected void readAglf910c()
	{
		wsaaAgtTerminateFlag.set("N");
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrlifIO.getAgntcoy());
		aglfIO.setAgntnum(chdrlifIO.getAgntnum());
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(aglfIO.getStatuz());
			drylogrec.params.set(aglfIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isLT(aglfIO.getDtetrm(), drypDryprcRecInner.drypRunDate)
		|| isLT(aglfIO.getDteexp(), drypDryprcRecInner.drypRunDate)
		|| isGT(aglfIO.getDteapp(), drypDryprcRecInner.drypRunDate)) {
			wsaaAgtTerminateFlag.set("Y");
		}
	}

protected void calcTolerance1000(Linspf linspf)//IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1010(linspf);
					computeTolerance1020(linspf);
				case failTolerance1050:
					failTolerance1050(linspf);
				case exit1090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1010(Linspf linspf)//IJTI-1727
	{
		/* Fetch the tolerance limits.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT5667Curr.set(linspf.getBillcurr());
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			goTo(GotoLabel.failTolerance1050);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
		}
		/* Having found the table item, get the frequency.*/
		wsaaToleranceChk.set("1");
		wsaaFreqFound.set("N");
		for (wsaaIx.set(1); !(isGT(wsaaIx, 11)
		|| freqFound.isTrue()); wsaaIx.add(1)){
			if (isEQ(t5667rec.freq[wsaaIx.toInt()], linspf.getInstfreq())) {
				wsaaPrmtol.set(t5667rec.prmtol[wsaaIx.toInt()]);
				wsaaMaxAmount.set(t5667rec.maxAmount[wsaaIx.toInt()]);
				/*       MOVE T5667-SFIND (WSAA-IX)                      <S19FIX>*/
				/*                              TO WSAA-SFIND            <S19FIX>*/
				wsaaSfind.set(t5667rec.sfind);
				wsaaPrmtoln.set(t5667rec.prmtoln[wsaaIx.toInt()]);
				wsaaMaxamt.set(t5667rec.maxamt[wsaaIx.toInt()]);
				wsaaFreqFound.set("Y");
			}
		}
		if (!freqFound.isTrue()) {
			drycntrec.contotNumber.set(controlTotalsInner.ct07);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.failTolerance1050);
		}
	}

protected void computeTolerance1020(Linspf linspf)//IJTI-1727
	{
		/*  Using the tolerance entry found calculate the overall amount*/
		/*  to be collected.*/
		/*   COMPUTE WSAA-TOLERANCE-ALLOWED ROUNDED                       */
		/*                                = (WSAA-PRMTOL                  */
		/*                                * WSAA-NET-CBILLAMT) / 100.     */
		/*   IF WSAA-TOLERANCE-ALLOWED    > WSAA-MAX-AMOUNT               */
		/*      MOVE WSAA-MAX-AMOUNT     TO WSAA-TOLERANCE-ALLOWED        */
		/*   END-IF.                                                      */
		/*   COMPUTE WSAA-SUSP-CBILL-DIFF ROUNDED                         */
		/*                                = (WSAA-NET-CBILLAMT            */
		/*                                - WSAA-SUSP-AVAIL).             */
		if (toleranceLimit1.isTrue()) {
			compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtol, wsaaNetCbillamt)), 100));
			if (isGT(wsaaToleranceAllowed, wsaaMaxAmount)) {
				wsaaToleranceAllowed.set(wsaaMaxAmount);
			}
		}
		else {
			compute(wsaaToleranceAllowed, 3).setRounded(div((mult(wsaaPrmtoln, wsaaNetCbillamt)), 100));
			if (isGT(wsaaToleranceAllowed, wsaaMaxamt)) {
				wsaaToleranceAllowed.set(wsaaMaxamt);
			}
		}
		
		//IJTI-1727 starts
		if (isNE(wsaaToleranceAllowed, 0)) {
			zrdecplrec.amountIn.set(wsaaToleranceAllowed);
			zrdecplrec.currency.set(linspf.getBillcurr());
			c000CallRounding();
			wsaaToleranceAllowed.set(zrdecplrec.amountOut);
		}
		//IJTI-1727 ends
		compute(wsaaSuspCbillDiff, 3).setRounded((sub(wsaaNetCbillamt, wsaaSuspAvail)));
		/*  Update the suspense as being enough to cover the premium if*/
		/*  it is within the allowable tolerance.  Convert the CBILLAMT*/
		/*  (the billing currency's amount) for multi-currency contracts.*/
		/*  By-pass the outstanding instalment totals.*/
		if (isLTE(wsaaSuspCbillDiff, wsaaToleranceAllowed)) {
			wsaaToleranceAllowed.set(wsaaSuspCbillDiff);
			wsaaSuspAvail.add(wsaaSuspCbillDiff);
			if (isEQ(linspf.getCntcurr(), linspf.getBillcurr())) {
				linspf.setInstamt03(wsaaToleranceAllowed.getbigdata());
			}
			else {
				compute(wsaaImRatio, 10).setRounded(div(wsaaAmtPaid, wsaaNetCbillamt));
				compute(wsaaCntcurrReceived, 10).setRounded(mult(linspf.getInstamt06(), wsaaImRatio));
				//IJTI-1727 starts
				if (isNE(wsaaCntcurrReceived, 0)) {
					zrdecplrec.amountIn.set(wsaaCntcurrReceived);
					zrdecplrec.currency.set(linspf.getCntcurr());
					c000CallRounding();
					wsaaCntcurrReceived.set(zrdecplrec.amountOut);
				}
				setPrecision(linspf.getInstamt03(), 2);
				linspf.setInstamt03(sub(linspf.getInstamt06(), wsaaCntcurrReceived).getbigdata());
				//IJTI-1727 ends
			}
			goTo(GotoLabel.exit1090);
		}
		if (toleranceLimit1.isTrue()
		&& isNE(wsaaMaxamt, 0)) {
			if (agtNotTerminated.isTrue()
			|| (agtTerminated.isTrue()
			&& isEQ(wsaaSfind, "2"))) {
				wsaaToleranceChk.set("2");
				computeTolerance1020(linspf);//IJTI-1727
				return ;
			}
		}
	}

protected void failTolerance1050(Linspf linspf)//IJTI-1727
	{
		/* Log no. LINS  still short of the cbillamt.*/
		drycntrec.contotNumber.set(controlTotalsInner.ct06);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/* Log no. LINS not collected.*/
		drycntrec.contotNumber.set(controlTotalsInner.ct04);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		wsaaPrevLins = "N";
		/* Increment the total of outstanding premiums.*/
		drycntrec.contotNumber.set(controlTotalsInner.ct05);
		drycntrec.contotValue.set(linspf.getInstamt06());
		d000ControlTotals();
		wsaaToleranceAllowed.set(ZERO);
		linsBad.setTrue();
	}

protected void update1500(Linspf linspf)//IJTI-1727
	{
		linsrnlIO.setChdrnum(linspf.getChdrnum());
		linsrnlIO.setChdrcoy(linspf.getChdrcoy());
		linsrnlIO.setInstfrom(linspf.getInstfrom());
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		linsrnlIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(linsrnlIO.getStatuz());
			drylogrec.params.set(linsrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		updateChdr1600(linspf); //IJTI-1727
		callPremiumCollect1700(linspf); //IJTI-1727
		updateSuspense1800(linspf); //IJTI-1727
		individualLedger1900(linspf); //IJTI-1727
		covrsAcmvsLinsPtrn2000(linspf); //IJTI-1727
		writeLetter6100(linspf); //IJTI-1727
		updateHdiv3900(linspf); //IJTI-1727
		//IJTI-1727 starts
		if(riskPremflag) {
			updateRiskPremium();
		}
		//IJTI-1727 ends
}


protected void updateChdr1600(Linspf linspf) //IJTI-1727
	{
		chdrlifIO.setInstjctl(SPACES);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setPtdate(linsrnlIO.getInstto());
		chdrlifIO.setInstto(linsrnlIO.getInstto());
		chdrlifIO.setInstfrom(linspf.getInstfrom());
		setPrecision(chdrlifIO.getInsttot01(), 2);
		chdrlifIO.setInsttot01(add(chdrlifIO.getInsttot01(), linspf.getInstamt01()));
		setPrecision(chdrlifIO.getInsttot02(), 2);
		chdrlifIO.setInsttot02(add(chdrlifIO.getInsttot02(), linspf.getInstamt02()));
		setPrecision(chdrlifIO.getInsttot03(), 2);
		chdrlifIO.setInsttot03(add(chdrlifIO.getInsttot03(), linspf.getInstamt03()));
		setPrecision(chdrlifIO.getInsttot04(), 2);
		chdrlifIO.setInsttot04(add(chdrlifIO.getInsttot04(), linspf.getInstamt04()));
		setPrecision(chdrlifIO.getInsttot05(), 2);
		chdrlifIO.setInsttot05(add(chdrlifIO.getInsttot05(), linspf.getInstamt05()));
		setPrecision(chdrlifIO.getInsttot06(), 2);
		chdrlifIO.setInsttot06(add(chdrlifIO.getInsttot06(), linspf.getInstamt06()));
		setPrecision(chdrlifIO.getInsttot01(), 2);
		chdrlifIO.setInsttot01(sub(chdrlifIO.getInsttot01(), linspf.getInstamt03()));
		setPrecision(chdrlifIO.getOutstamt(), 2);
		chdrlifIO.setOutstamt(sub(chdrlifIO.getOutstamt(), linspf.getInstamt06()));
		chdrlifIO.setFunction(Varcom.writd);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


protected void callPremiumCollect1700(Linspf linspf) //IJTI-1727
	{

		/* Look up the subroutine starting with the contract type then*/
		/* if not found, use '***'.  If neither case is found on T6654,*/
		/* then no record is written, and processing continues.*/
		String key;
		if(BTPRO028Permission) {
			key = payrIO.getBillchnl().toString().trim() + chdrlifIO.getCnttype().toString().trim() + payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = chdrlifIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = chdrlifIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6654);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setStatuz(Varcom.oK);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(tablesInner.t6654);
			wsaaT6654Billchnl.set(payrIO.getBillchnl());
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				drycntrec.contotNumber.set(controlTotalsInner.ct11);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				return ;
			}
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t6654rec.collectsub, SPACES)) {
			return ;
		}
		ddbtallrec.ddbtRec.set(SPACES);
		ddbtallrec.statuz.set(Varcom.oK);
		ddbtallrec.payrcoy.set(wsaaPayrcoy);
		ddbtallrec.payrnum.set(wsaaPayrnum);
		ddbtallrec.mandref.set(payrIO.getMandref());
		ddbtallrec.billamt.set(linspf.getCbillamt());
		ddbtallrec.lastUsedDate.set(drypDryprcRecInner.drypRunDate);
		callProgram(t6654rec.collectsub, ddbtallrec.ddbtRec);
		if (isNE(ddbtallrec.statuz, Varcom.oK)) {
			wsysSysparams.set(ddbtallrec.ddbtRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(ddbtallrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void updateSuspense1800(Linspf linspf) //IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1810(linspf);
				case advancePremDep1820:
					advancePremDep1820(linspf);
				case exit1890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1810(Linspf linspf) //IJTI-1727
	{
		if (isEQ(wsaaToleranceAllowed, ZERO)) {
			if (isGTE(wsaaWaiveAmtPaid, wsaaNetCbillamt)) {
				lifrtrnrec.origamt.set(ZERO);
			} else {
				lifrtrnrec.origamt.set(sub(wsaaNetCbillamt,wsaaWaiveAmtPaid));
			}
		}
		else {
			lifrtrnrec.origamt.set(wsaaAmtPaid);
		}
		if (isEQ(lifrtrnrec.origamt, 0)) {
			goTo(GotoLabel.exit1890);
		}
		lifrtrnrec.rdocnum.set(drypDryprcRecInner.drypEntity);
		lifrtrnrec.tranref.set(drypDryprcRecInner.drypEntity);
		lifrtrnrec.jrnseq.set(0);
		lifrtrnrec.contot.set(wsaaT5645Cnttot[1]);
		lifrtrnrec.sacscode.set(wsaaT5645Sacscode[1]);
		lifrtrnrec.sacstyp.set(wsaaT5645Sacstype[1]);
		lifrtrnrec.glsign.set(wsaaT5645Sign[1]);
		lifrtrnrec.glcode.set(wsaaT5645Glmap[1]);
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.origcurr.set(linspf.getBillcurr());
		lifrtrnrec.effdate.set(linspf.getInstfrom());
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec.rldgacct.set(drypDryprcRecInner.drypEntity);
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, Varcom.oK)) {
			wsysSysparams.set(lifrtrnrec.lifrtrnRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(lifrtrnrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Pull Cash Dividend required.                                    */
		if (isEQ(wsaaDivRequired, 0)) {
			goTo(GotoLabel.advancePremDep1820);
		}
		lifrtrnrec.origamt.set(wsaaDivRequired);
		lifrtrnrec.rdocnum.set(linspf.getChdrnum());
		lifrtrnrec.tranref.set(linspf.getChdrnum());
		lifrtrnrec.jrnseq.set(0);
		lifrtrnrec.contot.set(wsaaT5645Cnttot[35]);
		lifrtrnrec.sacscode.set(wsaaT5645Sacscode[35]);
		lifrtrnrec.sacstyp.set(wsaaT5645Sacstype[35]);
		lifrtrnrec.glsign.set(wsaaT5645Sign[35]);
		lifrtrnrec.glcode.set(wsaaT5645Glmap[35]);
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.origcurr.set(linspf.getBillcurr());
		lifrtrnrec.effdate.set(linspf.getInstfrom());
		lifrtrnrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifrtrnrec.rldgacct.set(linspf.getChdrnum());
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, Varcom.oK)) {
			wsysSysparams.set(lifrtrnrec.lifrtrnRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(lifrtrnrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

protected void advancePremDep1820(Linspf linspf) //IJTI-1727
	{
		/* Pull Advance premium deposit required.                          */
		updateApa6000(linspf);
		if (isEQ(linspf.getCntcurr(), linspf.getBillcurr())) {
			return ;
		}
		/*  Do the following if the accounting currency and the billing*/
		/*  currency are different, starting with the accounting currency.*/
		if (isEQ(wsaaToleranceAllowed, 0)) {
			lifacmvrec.origamt.set(linspf.getInstamt06());
		}
		else {
			lifacmvrec.origamt.set(wsaaCntcurrReceived);
		}
		lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.tranref.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.effdate.set(linspf.getInstfrom());
		lifacmvrec.substituteCode[6].set(SPACES);
		/*  MOVE 0                      TO LIFA-JRNSEQ.                  */
		if (isNE(lifacmvrec.origamt, ZERO)) {
			wsaaGlSub.set(2);
			lifacmvrec.origcurr.set(linspf.getCntcurr());
			callLifacmv5000(linspf);
		}
		/*  ... then the billing currency,*/
		if (isEQ(wsaaToleranceAllowed, ZERO)) {
			lifacmvrec.origamt.set(linspf.getCbillamt());
		}
		else {
			lifacmvrec.origamt.set(wsaaAmtPaid);
		}
		if (isNE(lifacmvrec.origamt, ZERO)) {
			wsaaGlSub.set(15);
			lifacmvrec.origcurr.set(linspf.getBillcurr());
			callLifacmv5000(linspf);
		}
	}

protected void individualLedger1900(Linspf linspf) //IJTI-1727
	{
		wsaaToleranceAllowed.set(ZERO);
		/*  Post the Premium-due.*/
		lifacmvrec.effdate.set(linspf.getInstfrom());
		lifacmvrec.tranref.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
		/*   MOVE 0                      TO LIFA-JRNSEQ.                  */
		lifacmvrec.origcurr.set(payrIO.getCntcurr());
		if (isGT(linspf.getInstamt06(), ZERO)
		&& isEQ(t5688rec.revacc, "Y")) {
			wsaaGlSub.set(32);
			
			//IJTI-1727 starts
			if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
				lifacmvrec.origamt.set(linspf.getProramt().add(linspf.getProrcntfee()));
			} else {
				lifacmvrec.origamt.set(linspf.getInstamt06());
			}
			
			callLifacmv5000(linspf);
			//IJTI-1727 ends
		}
		/*  Post the Premium-Income.*/
		if (isGT(linspf.getInstamt01(), ZERO)
		&& (isNE(t5688rec.comlvlacc, "Y")
		&& isNE(t5688rec.revacc, "Y"))) {
			
			//IJTI-1727 starts
			if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
				lifacmvrec.origamt.set(linspf.getProramt());
			} else {
				lifacmvrec.origamt.set(linspf.getInstamt01());
			}
			//IJTI-1727 ends
			wsaaGlSub.set(3);
			callLifacmv5000(linspf); //IJTI-1727
		}
		/*  Post the Fees.*/
		if (isGT(linspf.getInstamt02(), ZERO)
		&& isNE(t5688rec.revacc, "Y")) {
			//IJTI-1727 starts
			if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
				lifacmvrec.origamt.set(linspf.getProrcntfee());
			} else {
				lifacmvrec.origamt.set(linspf.getInstamt02());
			}
			//IJTI-1727 ends
			wsaaGlSub.set(4);
			callLifacmv5000(linspf); //IJTI-1727
		}
		/*  Post the Tolerance.*/
		/*   IF LINSDRY-INSTAMT03         > ZEROES                        */
		/*      MOVE LINSDRY-INSTAMT03   TO LIFA-ORIGAMT                  */
		/*      MOVE 5                   TO WSAA-GL-SUB                   */
		/*      PERFORM 5000-CALL-LIFACMV                                 */
		/*   END-IF.                                                      */
		if (isGT(linspf.getInstamt03(), 0)) {
			lifacmvrec.origamt.set(linspf.getInstamt03());
			if (toleranceLimit1.isTrue()) {
				wsaaGlSub.set(5);
			}
			if (toleranceLimit2.isTrue()) {
				if (agtTerminated.isTrue()) {
					wsaaGlSub.set(5);
				}
				else {
					wsaaGlSub.set(36);
					lifacmvrec.rldgacct.set(chdrlifIO.getAgntnum());
				}
			}
			callLifacmv5000(linspf);
			if (isEQ(wsaaGlSub, 36)) {
				/* Override Commission                                             */
				if (isEQ(th605rec.indic, "Y")) {
					zorlnkrec.annprem.set(lifacmvrec.origamt);
					zorlnkrec.effdate.set(chdrlifIO.getOccdate());
					b500CallZorcompy();
				}
				lifacmvrec.rldgacct.set(linspf.getChdrnum());
			}
		}
		/*  Post the Stamp-Duty.*/
		if (isGT(linspf.getInstamt04(), ZERO)
		&& isNE(t5688rec.revacc, "Y")) {
			lifacmvrec.origamt.set(linspf.getInstamt04());
			wsaaGlSub.set(6);
			callLifacmv5000(linspf); //IJTI-1727
		}
		/*  Post Dividend Suspense utilized                                */
		if (isGT(wsaaDivRequired, 0)) {
			lifacmvrec.origamt.set(wsaaDivRequired);
			wsaaGlSub.set(34);
			callLifacmv5000(linspf); //IJTI-1727
		}
		/*  Post the Waiver-holding.*/
		if (isNE(linspf.getInstamt05(), ZERO)
		&& isNE(t5688rec.revacc, "Y")) {
			lifacmvrec.origamt.set(linspf.getInstamt05());
			wsaaGlSub.set(7);
			callLifacmv5000(linspf); //IJTI-1727
		}
		if (isNE(prasrec.taxrelamt, 0)) {
			lifacmvrec.origcurr.set(linspf.getBillcurr());
			lifacmvrec.origamt.set(prasrec.taxrelamt);
			/*    MOVE PRAS-INREVNUM       TO LIFA-RLDGACCT                 */
			lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			wsaaGlSub.set(19);
			callLifacmv5000(linspf); //IJTI-1727
		}
		/*  Post the calculated taxes                                      */
		wsaaTxLife.set(SPACES);
		wsaaTxCoverage.set(SPACES);
		wsaaTxRider.set(SPACES);
		/*  MOVE SPACES                 TO WSAA-TX-PLANSFX       <S19FIX>*/
		wsaaTxPlansfx.set(ZERO);
		wsaaTxCrtable.set(SPACES);
		b700PostTax(linspf); //IJTI-1727

	}


protected void covrsAcmvsLinsPtrn2000(Linspf linspf) //IJTI-1727
	{

		/* Initialize the AGCM arrays                                      */
		if (isEQ(th605rec.bonusInd, "Y")) {
			b100InitializeArrays();
		}
		/*  Create a posting for every COVR-INSTPREMs for the contract.*/
		if (isEQ(th605rec.bonusInd, "Y")) {
			wsaaAgcmIx.set(0);
		}
		wsaaCovrIx.set(1);
		wsaaLastChdrnum.set(SPACES);
		wsaaLastLife.set(SPACES);
		wsaaLastCoverage.set(SPACES);
		wsaaLastRider.set(SPACES);
		wsaaLastPlanSuff.set(ZERO);
		//IJTI-1727 starts
		Map<String, List<Covrpf>> covrpfMap = covrpfDAO.searchCovrlnb(drypDryprcRecInner.drypCompany.toString(),
				Arrays.asList(drypDryprcRecInner.drypEntity.toString()));
		
		if (covrpfMap != null) {
			List<Covrpf> covrpfList = covrpfMap.get(drypDryprcRecInner.drypEntity.toString());
			if (covrpfList != null) {
				for (Covrpf covrpf : covrpfList) {
					processCovrs2100(linspf, covrpf);
					wsaaCovrIx.add(1);
				}
			} else {
				drylogrec.params.set("Error Occurred while reading COVRPF");
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}

			/* VARYING WSAA-COVR-IX */
			/* FROM 1 */
			/* BY 1 */
			if (payrseqnoError.isTrue()) {
				drycntrec.contotNumber.set(controlTotalsInner.ct10);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				return;
			}
			/* MOVE 0 TO LIFA-JRNSEQ. */
			wsaaRldgacct.set(SPACES);

			Map<String, List<Agcmpf>> agcmpfMap = agcmpfDAO.searchAgcmrnl(drypDryprcRecInner.drypCompany.trim(),
					Collections.singletonList(drypDryprcRecInner.drypEntity.toString()));

			List<Agcmpf> agcmpfList = agcmpfMap.get(drypDryprcRecInner.drypEntity.toString());
			List<Agcmpf> agcmpfUpdateList = new ArrayList<>();
			if (agcmpfList != null) {
				for (Agcmpf agcmpf : agcmpfList) {
					readAgcm2400(linspf, covrpfList, agcmpf, agcmpfUpdateList);
				}
			}

			if (!agcmpfUpdateList.isEmpty()) {
				agcmpfDAO.updateAgcmByUniqNum(agcmpfUpdateList);
			}

			writePtrn3500(linspf);
			updateLinsPayr3600(linspf);
		} else {
			drylogrec.params.set("Error Occurred while reading COVRPF");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		//IJTI-1727 ends

	}


protected void processCovrs2100(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2110(linspf, covrpf);
				case skipBonusWorkbench2150:
					skipBonusWorkbench2150(linspf, covrpf);
				case exit2190:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2110(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		wsaaPayrseqnoError.set("N");
		/*  If the Payrseqno is not equal to that of the LINS then no*/
		/*  further processing must be carried out on this LINS.*/
		if (isNE(covrpf.getPayrseqno(), linspf.getPayrseqno())) {
			wsaaPayrseqnoError.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/* If the working storage array has been exceeded, error.*/
		if (isGT(wsaaCovrIx, wsaaCovrSize)) {
			drylogrec.statuz.set(h791);
			wsysSysparams.set("WSAA-COVR-REC");
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		
		processFeatureCslri003(linspf, covrpf); //IJTI-1727
		
		/* We will only store the coverage info when it passed the         */
		/* validation check.                                               */
		/* Store the CRTABLES of the coverages ready for posting in       */
		/* commission so we dont have to read the COVR again.  Store them */
		/* in ascending sequence.                                         */
		/*    MOVE COVRLNB-CHDRNUM  TO WSAA-COVR-CHDRNUM (WSAA-COVR-IX).   */
		/*    MOVE COVRLNB-LIFE     TO WSAA-COVR-LIFE    (WSAA-COVR-IX).   */
		/*    MOVE COVRLNB-COVERAGE TO WSAA-COVR-COVRG   (WSAA-COVR-IX).   */
		/*    MOVE COVRLNB-RIDER    TO WSAA-COVR-RIDER   (WSAA-COVR-IX).   */
		/*    MOVE COVRLNB-PLAN-SUFFIX                                     */
		/*                          TO WSAA-COVR-PLAN-SUFF (WSAA-COVR-IX). */
		/*    MOVE COVRLNB-CRTABLE  TO WSAA-COVR-CRTABLE   (WSAA-COVR-IX). */
		if (isGTE(linspf.getInstfrom(), covrpf.getCurrto())
		|| isLT(linspf.getInstfrom(), covrpf.getCurrfrom())) {
			goTo(GotoLabel.exit2190);
		}
		wsaaValidCoverage.set("N");
		wsaaCovrValid[wsaaCovrIx.toInt()].set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrpf.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrpf.getPstatcode())) {
						wsaaT5679Sub.set("13");
						wsaaValidCoverage.set("Y");
						wsaaCovrValid[wsaaCovrIx.toInt()].set("Y");
					}
				}
			}
		}
		if (!validCoverage.isTrue()) {
			drycntrec.contotNumber.set(controlTotalsInner.ct12);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(covrpf.getChdrnum(), wsaaLastChdrnum)
		&& isEQ(covrpf.getLife(), wsaaLastLife)
		&& isEQ(covrpf.getCoverage(), wsaaLastCoverage)
		&& isEQ(covrpf.getRider(), wsaaLastRider)
		&& isEQ(covrpf.getPlanSuffix(), wsaaLastPlanSuff)) {
			goTo(GotoLabel.exit2190);
		}
		/*  Store the CRTABLES of the coverages ready for posting in       */
		/*  commission so we dont have to read the COVR again.  Store them */
		/*  in ascending sequence.                                         */
		//IJTI-1727 starts
		int index = wsaaCovrIx.toInt();
		wsaaCovrChdrnum[index].set(covrpf.getChdrnum());
		wsaaLastChdrnum.set(covrpf.getChdrnum());
		wsaaCovrLife[index].set(covrpf.getLife());
		wsaaLastLife.set(covrpf.getLife());
		wsaaCovrCovrg[index].set(covrpf.getCoverage());
		wsaaLastCoverage.set(covrpf.getCoverage());
		wsaaCovrRider[index].set(covrpf.getRider());
		wsaaLastRider.set(covrpf.getRider());
		wsaaCovrPlanSuff[index].set(covrpf.getPlanSuffix());
		wsaaLastPlanSuff.set(covrpf.getPlanSuffix());
		wsaaCovrCrtable[index].set(covrpf.getCrtable());
		 if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
			 wsaaCovrProratePrem[index].set(covrpf.getProrateprem());
		 }
		//IJTI-1727 ends
		/*  For VALID coverages perform component level accounting if*/
		/*  necessary and if the COVRLNB-INSTPREM not = 0*/
		if (isEQ(t5688rec.comlvlacc, "Y")
		&& isNE(t5688rec.revacc, "Y")
		&& isNE(covrpf.getInstprem(), 0)) {
			wsaaGlSub.set(22);
			wsaaRldgChdrnum.set(covrpf.getChdrnum());
			wsaaRldgLife.set(covrpf.getLife());
			wsaaRldgCoverage.set(covrpf.getCoverage());
			wsaaRldgRider.set(covrpf.getRider());
			wsaaPlan.set(covrpf.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			
			//IJTI-1727 starts
			if (reinstflag && isShortTerm) {
				lifacmvrec.origamt.set(calprpmrec.prem);
			} else {
				if (isFoundPro && incrFoundPro) {
					lifacmvrec.origamt.set(currIncrPro.getNewinst());
				} else {
					lifacmvrec.origamt.set(covrpf.getInstprem());
				}
			}
			
			if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
				if(covrpf.getProrateprem() != null) {
					lifacmvrec.origamt.set(covrpf.getProrateprem());
				} else {
					lifacmvrec.origamt.set(BigDecimal.ZERO);
				}
			}
			//IJTI-1727 ends
			/*     MOVE 0                   TO LIFA-JRNSEQ*/
			lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
			lifacmvrec.substituteCode[6].set(covrpf.getCrtable());
			lifacmvrec.effdate.set(linspf.getInstfrom());
			callLifacmv5000(linspf); //IJTI-1727
		}
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.skipBonusWorkbench2150);
		}
		/* Bonus Workbench *                                               */
		/* Read the table T5687                                          */
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrpf.getCrtable());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK)
		&& isNE(itdmIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), Varcom.endp)
		|| isNE(itdmIO.getItemitem(), covrpf.getCrtable())
		|| isNE(itdmIO.getItemcoy(), covrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItempfx(), smtpfxcpy.item)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		if (isNE(covrpf.getInstprem(), ZERO)) {
			wsaaBillfq.set(payrIO.getBillfreq());
		}
		else {
			if (isNE(t5687rec.bbmeth, SPACES)) {
				itemIO.setDataArea(SPACES);
				itemIO.setItemcoy(covrpf.getChdrcoy());
				itemIO.setItempfx(smtpfxcpy.item);
				itemIO.setItemtabl(tablesInner.t5534);
				itemIO.setItemitem(t5687rec.bbmeth);
				itemIO.setFunction(Varcom.readr);
				itemIO.setFormat(formatsInner.itemrec);
				SmartFileCode.execute(appVars, itemIO);
				if (isNE(itemIO.getStatuz(), Varcom.oK)) {
					drylogrec.statuz.set(itdmIO.getStatuz());
					drylogrec.params.set(itdmIO.getParams());
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}
				t5534rec.t5534Rec.set(itemIO.getGenarea());
				wsaaBillfq.set(t5534rec.unitFreq);
				if (isNE(wsaaBillfq9, NUMERIC)) {
					drylogrec.params.set(t5687rec.bbmeth);
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}
				else {
					if (isEQ(wsaaBillfq9, ZERO)) {
						wsaaBillfq.set("01");
					}
				}
			}
			else {
				wsaaBillfq.set(payrIO.getBillfreq());
			}
		}
		if (isNE(covrpf.getInstprem(), ZERO)) {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory(covrpf);
			b300WriteArrays(linspf, covrpf);
		}
		else {
			wsaaAgcmIx.add(1);
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrpf.getChdrcoy());
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrpf.getChdrnum());
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrpf.getLife());
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrpf.getCoverage());
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrpf.getRider());
			b200PremiumHistory(covrpf);
		}
	}

	//IJTI-1727 starts
	private void processFeatureCslri003(Linspf linspf, Covrpf covrpf) {
		if (reinstflag && isFoundPro) {
			getIncr(linspf, covrpf);
			isShortTerm = false;
			checkTd5j2Term(covrpf.getCrtable());
			if (isShortTerm) {
				if (isLT(linspf.getInstto(), ptrnpfReinstate.getDatesub())) {
					isShortTerm = false;
					goTo(GotoLabel.exit2190);
				} else {
					callPreimumSubroutine(linspf, covrpf);
				}
			}
		}
	}
	
	private void getIncr(Linspf linspf, Covrpf covrpf) {
		incrFoundPro = false;
		int date = linspf.getInstfrom();
		if (incrDatedMap != null && incrDatedMap.containsKey(date)) {
			List<Incrpf> incrpfList = incrDatedMap.get(date);
			for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext();) {
				Incrpf incr = iterator.next();
				if (incr.getChdrnum().equals(covrpf.getChdrnum())
						&& incr.getLife().equals(covrpf.getLife())
						&& incr.getCoverage().equals(covrpf.getCoverage())
						&& incr.getRider().equals(covrpf.getRider())
						&& Integer.compare(incr.getPlnsfx(), covrpf.getPlanSuffix()) == 0) {
					currIncrPro = incr;
					incrFoundPro = true;
					break;
				}
			}
		}
	}
	
	private void checkTd5j2Term(String crtable) {
		readTd5j2(crtable);
		if (isNE(td5j2rec.td5j2Rec, SPACES) && isEQ(td5j2rec.shortTerm, "Y")) {
			isShortTerm = true;
		}
	}

	private void readTd5j2(String crtable) {
		if (td5j2Map.containsKey(crtable)) {
			List<Itempf> itempfList = td5j2Map.get(crtable);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				filterTd5j2ByDates(iterator.next());
			}
		} else {
			td5j2rec.td5j2Rec.set(SPACES);
		}
	}
	
	private void filterTd5j2ByDates(Itempf itempf) {
		if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
			String runDate = drypDryprcRecInner.drypRunDate.toString();
			if ((Integer.parseInt(runDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
					&& Integer.parseInt(runDate) <= Integer.parseInt(itempf.getItmto().toString())) {
				td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		} else {
			td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
	
	private void callPreimumSubroutine(Linspf linspf, Covrpf covrpf) {
		calprpmrec.prem.set(ZERO);
		calprpmrec.reinstDate.set(ptrnpfReinstate.getDatesub());
		calprpmrec.lastPTDate.set(linspf.getInstfrom());
		calprpmrec.nextPTDate.set(linspf.getInstto());
		calprpmrec.transcode.set(drypDryprcRecInner.drypBatctrcde);
		calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
		calprpmrec.prem.set(covrpf.getInstprem());
		calprpmrec.chdrcoy.set(linspf.getChdrcoy());
		calprpmrec.chdrnum.set(linspf.getChdrnum());
		callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec, null);
		if (isNE(calprpmrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(calprpmrec.statuz);
			drylogrec.params.set(calprpmrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

protected void skipBonusWorkbench2150(Linspf linspf, Covrpf covrpf)
	{
		/*  Post the calculated taxes                                      */
		wsaaTxLife.set(covrpf.getLife());
		wsaaTxCoverage.set(covrpf.getCoverage());
		wsaaTxRider.set(covrpf.getRider());
		wsaaTxPlansfx.set(covrpf.getPlanSuffix());
		wsaaTxCrtable.set(covrpf.getCrtable());
		b700PostTax(linspf);
		genericProcessing2200(linspf, covrpf);
	}

	//IJTI-1727 ends

protected void genericProcessing2200(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		wsaaAuthCode.set(drypDryprcRecInner.drypBatctrcde);
		wsaaCovrlnbCrtable.set(covrpf.getCrtable());
		/* Load the coverage switching details.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemitem(wsaaTrcdeCrtable);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			return ;
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
		for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx, 4)); wsaaSubprogIx.add(1)){
			if (isNE(t5671rec.subprog[wsaaSubprogIx.toInt()], SPACES)) {
				callGeneric2300(linspf, covrpf); //IJTI-1727
			}
		}
	}

protected void callGeneric2300(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		rnlallrec.company.set(drypDryprcRecInner.drypCompany);
		rnlallrec.chdrnum.set(drypDryprcRecInner.drypEntity);
		rnlallrec.life.set(covrpf.getLife());
		rnlallrec.coverage.set(covrpf.getCoverage());
		rnlallrec.rider.set(covrpf.getRider());
		rnlallrec.planSuffix.set(covrpf.getPlanSuffix());
		rnlallrec.crdate.set(covrpf.getCrrcd());
		rnlallrec.crtable.set(covrpf.getCrtable());
		rnlallrec.billcd.set(payrIO.getBillcd());
		rnlallrec.tranno.set(chdrlifIO.getTranno());
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[30]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[30]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[30]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[31]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[31]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[31]);
		}
		else {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[20]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[20]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[20]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[21]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[21]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[21]);
		}
		rnlallrec.cntcurr.set(payrIO.getCntcurr());
		rnlallrec.cnttype.set(chdrlifIO.getCnttype());
		rnlallrec.billfreq.set(payrIO.getBillfreq());
		rnlallrec.duedate.set(linspf.getInstfrom());
		rnlallrec.anbAtCcd.set(covrpf.getAnbAtCcd());
		/* Calculate the term left to run. It is needed in the generic*/
		/* processing routine to work out the initial unit discount factor*/
		datcon3rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.intDate2.set(covrpf.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			datcon3rec.freqFactorx.set(ZERO);
		}
		wsaaTerm.set(datcon3rec.freqFactorx);
		if (isNE(wsaaTermLeftRemain, 0)) {
			wsaaTerm.add(1);
		}
		rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
		/* Get the total premium from the temporary table for the coverage*/
		//IJTI-1727 starts
		if(prmhpf != null) {
			if(covrpf.getProrateprem() != null) {
				rnlallrec.covrInstprem.set(covrpf.getProrateprem());
			} else {
				rnlallrec.covrInstprem.set(BigDecimal.ZERO);
			}
		} else {
			rnlallrec.covrInstprem.set(covrpf.getInstprem());
		}
		//IJTI-1727 ends
		rnlallrec.totrecd.set(ZERO);
		callProgram(t5671rec.subprog[wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);
		if (isNE(rnlallrec.statuz, Varcom.oK)) {
			wsysSysparams.set(rnlallrec.rnlallRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(rnlallrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}
	
	//IJTI-1727 starts
	protected void readAgcm2400(Linspf linspf, List<Covrpf> covrpfList, Agcmpf agcmpf, List<Agcmpf> agcmpfUpdateList) {
		/* Do not process single premium AGCMs (PTDATE = zeroes). */
		if (agcmpf.getPtdate() == 0) {
			return;
		}

		if(checkAndSkipLARecords(covrpfList, agcmpf)){
			return;
		}

		/* Look up the COVRs for this AGCM from the array and the CRTABLE */
		/* for the commission processing */
		wsaaCovrIx.set(1);
		searchlabel1: {
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)) {
                if (isEQ(agcmpf.getChdrnum(), wsaaCovrChdrnum[wsaaCovrIx.toInt()])
                        && isEQ(agcmpf.getLife(), wsaaCovrLife[wsaaCovrIx.toInt()])
                        && isEQ(agcmpf.getCoverage(), wsaaCovrCovrg[wsaaCovrIx.toInt()])
                        && isEQ(agcmpf.getRider(), wsaaCovrRider[wsaaCovrIx.toInt()])
                        && isEQ(agcmpf.getPlanSuffix(), wsaaCovrPlanSuff[wsaaCovrIx.toInt()])) {
                    break searchlabel1;
                }
            }
			if (reinstflag && isFoundPro) {
				return;
			}
			drylogrec.statuz.set(e351);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(agcmpf.getChdrnum());
			stringVariable1.addExpression(agcmpf.getLife());
			stringVariable1.addExpression(agcmpf.getCoverage());
			stringVariable1.addExpression(agcmpf.getRider());
			stringVariable1.setStringInto(wsysSysparams);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Do not process AGCMs attached to components of invalid */
		/* status. */
		if (isNE(wsaaCovrValid[wsaaCovrIx.toInt()], "Y")) {
			return;
		}
		updateAgcm2500(linspf, agcmpf, agcmpfUpdateList);
	}
	//IJTI-1727 ends

	//IJTI-1727 starts
	private boolean checkAndSkipLARecords(List<Covrpf> covrpfList, Agcmpf agcmpf) {
		boolean isFound = false;
		boolean isSkip = false;
		for (Covrpf covrpf : covrpfList) {
			if (isEQ(agcmpf.getCoverage(), covrpf.getCoverage()) && isEQ(agcmpf.getRider(), covrpf.getRider())
					&& isEQ(agcmpf.getLife(), covrpf.getLife())
					&& isEQ(agcmpf.getPlanSuffix(), covrpf.getPlanSuffix())) {
				isFound = true;
				if ("LA".equals(covrpf.getStatcode())) {
					isSkip = true;
					break;
				}
			}
		}
		
		if(!isFound) {
			drylogrec.params.set("Covr record not found");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		
		return isSkip;
	}
	//IJTI-1727 ends

protected void updateAgcm2500(Linspf linspf, Agcmpf agcmpf, List<Agcmpf> agcmpfUpdateList) //IJTI-1727
	{
		wsaaBillfreq.set(payrIO.getBillfreq());
		comlinkrec.billfreq.set(payrIO.getBillfreq());
		//IJTI-1727 starts
		if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()) {
			compute(comlinkrec.instprem, 3).setRounded(wsaaCovrProratePrem[wsaaCovrIx.toInt()]);
		} else {
			compute(comlinkrec.instprem, 3).setRounded(div(agcmpf.getAnnprem(), wsaaBillfreqNum));
		}
		//IJTI-1727 ends
		comlinkrec.chdrnum.set(drypDryprcRecInner.drypEntity);
		comlinkrec.chdrcoy.set(drypDryprcRecInner.drypCompany);
		comlinkrec.language.set(drypDryprcRecInner.drypLanguage);
		comlinkrec.life.set(agcmpf.getLife());
		comlinkrec.agent.set(agcmpf.getAgntnum());
		comlinkrec.coverage.set(agcmpf.getCoverage());
		comlinkrec.annprem.set(agcmpf.getAnnprem());
		comlinkrec.rider.set(agcmpf.getRider());
		comlinkrec.planSuffix.set(agcmpf.getPlanSuffix());
		comlinkrec.crtable.set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
		comlinkrec.agentClass.set(agcmpf.getAgentClass());
		comlinkrec.icommtot.set(agcmpf.getInitcom());
		comlinkrec.icommpd.set(agcmpf.getCompay());
		comlinkrec.icommernd.set(agcmpf.getComern());
		comlinkrec.effdate.set(agcmpf.getEfdate());
		comlinkrec.currto.set(0);
		comlinkrec.targetPrem.set(0);
		comlinkrec.ptdate.set(linsrnlIO.getInstto());
		/*  Call the three types of commission routine from AGCM*/
		for (wsaaCommethNo.set(1); !(isGT(wsaaCommethNo, 3)); wsaaCommethNo.add(1)){
			setUpCommMethod2600(linspf, agcmpf);
		}
		/*  Write an ACMV for each CHDRNUM/AGNTNUM/COVR/COMMISSION AMOUNT*/
		postCommission2800(linspf, agcmpf);
		agcmpf.setPtdate(linsrnlIO.getInstto().toInt());
		agcmpfUpdateList.add(agcmpf);
	}


protected void setUpCommMethod2600(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		if (isEQ(wsaaCommethNo, 1)){
			comlinkrec.method.set(agcmpf.getBascpy());
		}
		else if (isEQ(wsaaCommethNo, 2)){
			comlinkrec.method.set(agcmpf.getSrvcpy());
		}
		else if (isEQ(wsaaCommethNo, 3)){
			comlinkrec.method.set(agcmpf.getRnwcpy());
		}
		/* Load the commission method.*/
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemitem(comlinkrec.method);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			return ;
		}
		else {
			t5644rec.t5644Rec.set(itemIO.getGenarea());
		}
		/*  Check initial commission for BASCPY method only.*/
		if (isEQ(wsaaCommethNo, 1)) {
			if (isNE(agcmpf.getCompay(), agcmpf.getInitcom())
			|| isNE(agcmpf.getComern(), agcmpf.getInitcom())) {
				callCommRoutine2700(linspf, agcmpf);
			}
		}
		else {
			if (isEQ(wsaaCommethNo, 3)
			&& isEQ(chdrlifIO.getRnwlsupr(), "Y")
			&& isGTE(payrIO.getPtdate(), chdrlifIO.getRnwlspfrom())
			&& isLTE(payrIO.getPtdate(), chdrlifIO.getRnwlspto())) {
				/*NEXT_SENTENCE*/
			}
			else {
				callCommRoutine2700(linspf, agcmpf);
			}
		}
	}

protected void callCommRoutine2700(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		//IJTI-1727 starts
		if(gstOnCommFlag) {
        	comlinkrec.gstAmount.set(0);		
		}
		//IJTI-1727 ends
		/*  Call the subroutine relating to the CLNK-METHOD*/
		callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(comlinkrec.statuz);
			drylogrec.params.set(comlinkrec.clnkallRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		//IJTI-1727 starts
		if (isNE(comlinkrec.payamnt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.payamnt);
			zrdecplrec.currency.set(linspf.getCntcurr());
			c000CallRounding();
			comlinkrec.payamnt.set(zrdecplrec.amountOut);
		}
		
		if (isNE(comlinkrec.erndamt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.erndamt);
			zrdecplrec.currency.set(linspf.getCntcurr());
			c000CallRounding();
			comlinkrec.erndamt.set(zrdecplrec.amountOut);
		}
		//IJTI-1727 ends
		
		/*  Basic Commission*/
		if (isEQ(wsaaCommethNo, 1)) {
			if (isEQ(agcmpf.getOvrdcat(), "O")) {
				wsaaOvrdBascpyDue.set(comlinkrec.payamnt);
				wsaaOvrdBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaOvrdBascpyPay, 2).set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
			else {
				wsaaBascpyDue.set(comlinkrec.payamnt);
				wsaaBascpyErn.set(comlinkrec.erndamt);
				//IJTI-1727 starts
				if (gstOnCommFlag && agcmpf.getInitCommGst() != null) {
					compute(wsaaCompErnInitCommGst, 3).setRounded(add(wsaaCompErnInitCommGst, comlinkrec.gstAmount));
					agcmpf.setInitCommGst(add(agcmpf.getInitCommGst(), wsaaCompErnInitCommGst).getbigdata());
					compute(wsaaBascpyPay, 2).setRounded(add(wsaaBascpyPay,
							(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount), comlinkrec.payamnt))));
				} else {
					compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay,
							(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount), comlinkrec.payamnt))));
				}
				//IJTI-1727 ends
			}
			setPrecision(agcmpf.getCompay(), 2);
			agcmpf.setCompay(add(agcmpf.getCompay(), comlinkrec.payamnt).getbigdata());
			setPrecision(agcmpf.getComern(), 2);
			agcmpf.setComern(add(agcmpf.getComern(), comlinkrec.erndamt).getbigdata());
		}
		/*  Service Commission*/
		if (isEQ(wsaaCommethNo, 2)) {
			wsaaSrvcpyDue.set(comlinkrec.payamnt);
			wsaaSrvcpyErn.set(comlinkrec.erndamt);
			setPrecision(agcmpf.getScmearn(), 2);
			agcmpf.setScmearn(add(agcmpf.getScmearn(), comlinkrec.erndamt).getbigdata());
			setPrecision(agcmpf.getScmdue(), 2);
			agcmpf.setScmdue(add(agcmpf.getScmdue(), comlinkrec.payamnt).getbigdata());
		}
		/*  Renewal Commission*/
		//IJTI-1727 starts
		if (!isDMSFlgOn && isEQ(wsaaCommethNo, 3)) {
			if(gstOnCommFlag) {
				wsaaRnwcpyDue.set(comlinkrec.erndamt);
			} else {
				wsaaRnwcpyDue.set(comlinkrec.payamnt);
			}
			wsaaRnwcpyErn.set(comlinkrec.erndamt);
			setPrecision(agcmpf.getRnlcdue(), 2);
			agcmpf.setRnlcdue(add(agcmpf.getRnlcdue(), comlinkrec.payamnt).getbigdata());
			setPrecision(agcmpf.getRnlcearn(), 2);
			agcmpf.setRnlcearn(add(agcmpf.getRnlcearn(), comlinkrec.erndamt).getbigdata());
			if (gstOnCommFlag && agcmpf.getRnwlCommGst() != null) {
				compute(wsaaCompErnRnwlCommGst, 3).setRounded(add(wsaaCompErnRnwlCommGst, comlinkrec.gstAmount));
				agcmpf.setRnwlCommGst(add(agcmpf.getRnwlCommGst(), wsaaCompErnRnwlCommGst).getbigdata());
			}
		}
		//IJTI-1727 ends
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench *                                               */
		/* Skip overriding commission                                      */
		if (isNE(agcmpf.getOvrdcat(), "B")) {
			return ;
		}
		if (isEQ(wsaaCommethNo, "1")){
			firstPrem.setTrue();
		}
		else if (isEQ(wsaaCommethNo, "3")){
			renPrem.setTrue();
		}
		else{
			return ;
		}
		if (isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmpf.getChdrcoy());
			zctnIO.setAgntnum(agcmpf.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium(agcmpf);
			setPrecision(zctnIO.getPremium(), 3);
			zctnIO.setPremium(div(wsaaAgcmPremium, wsaaBillfq9), true);
			if (isNE(zctnIO.getPremium(), 0)) { //IJTI-1727
				zrdecplrec.amountIn.set(zctnIO.getPremium());
				zrdecplrec.currency.set(linspf.getCntcurr());
				c000CallRounding();
				zctnIO.setPremium(zrdecplrec.amountOut);
			}
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmpf.getAnnprem(), 100), wsaaAgcmPremium), true);
			if (isNE(zctnIO.getSplitBcomm(), 0)) { //IJTI-1727
				zrdecplrec.amountIn.set(zctnIO.getSplitBcomm());
				zrdecplrec.currency.set(linspf.getCntcurr());
				c000CallRounding();
				zctnIO.setSplitBcomm(zrdecplrec.amountOut);
			}
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(chdrlifIO.getTranno());
			zctnIO.setTransCode(drypDryprcRecInner.drypBatctrcde);
			zctnIO.setEffdate(linspf.getInstfrom());
			zctnIO.setTrandate(wsaaTday);
			zctnIO.setFormat(formatsInner.zctnrec);
			zctnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(zctnIO.getStatuz());
				drylogrec.params.set(zctnIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
	}

protected void postCommission2800(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		/*  Initialise the ACMV area and move the contract type to its*/
		/*  relevant field (same for all calls to LIFACMV).*/
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		wsaaAgntChdrnum.set(wsaaCovrChdrnum[wsaaCovrIx.toInt()]);
		wsaaAgntLife.set(wsaaCovrLife[wsaaCovrIx.toInt()]);
		wsaaAgntCoverage.set(wsaaCovrCovrg[wsaaCovrIx.toInt()]);
		wsaaAgntRider.set(wsaaCovrRider[wsaaCovrIx.toInt()]);
		wsaaPlan.set(wsaaCovrPlanSuff[wsaaCovrIx.toInt()]);
		wsaaAgntPlanSuffix.set(wsaaPlansuff);
		postInitialCommission2900(linspf, agcmpf);
		postServiceCommission3000(linspf, agcmpf);
		postRenewalCommission3100(linspf, agcmpf);
		postOverridCommission3200(linspf, agcmpf);
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
		//IJTI-1727 starts
		if (gstOnCommFlag) {
			wsaaCompErnRnwlCommGst.set(0);
			wsaaCompErnInitCommGst.set(0);
		}	
		//IJTI-1727 ends
	}

protected void postInitialCommission2900(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		/*  Initial commission due*/
		if (isNE(wsaaBascpyDue, 0)) {
			/*     ADD +1                   TO LIFA-JRNSEQ                   */
			wsaaGlSub.set(8);
			wsaaRldgChdrnum.set(agcmpf.getChdrnum());
			wsaaRldgLife.set(agcmpf.getLife());
			wsaaRldgCoverage.set(agcmpf.getCoverage());
			wsaaRldgRider.set(agcmpf.getRider());
			wsaaPlan.set(agcmpf.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaBascpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			callLifacmv5000(linspf);
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				zorlnkrec.effdate.set(agcmpf.getEfdate());
				b500CallZorcompy();
				//IJTI-1727 starts
				if (gstOnCommFlag) {
					gstPostings(zorlnkrec.gstAmount.getbigdata(), agcmpf.getAgntnum(), linspf);
				}
				//IJTI-1727 ends
			}
		}
		/*  Initial commission earned*/
		if (isNE(wsaaBascpyErn, 0)) {
			/*     ADD +1                   TO LIFA-JRNSEQ                   */
			lifacmvrec.origamt.set(wsaaBascpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(23);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(9);
				lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv5000(linspf);//IJTI-1727
			//IJTI-1727 starts
			if (gstOnCommFlag) {
				gstPostings(wsaaCompErnInitCommGst.getbigdata(), agcmpf.getAgntnum(), linspf);
			}
			//IJTI-1727 ends
		}
		/*  Initial commission paid*/
		if (isNE(wsaaBascpyPay, 0)) {
			lifacmvrec.origamt.set(wsaaBascpyPay);
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(24);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(10);
				lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv5000(linspf); //IJTI-1727
		}
	}

	//IJTI-1727 starts
	protected void gstPostings(BigDecimal origamt, String agntNum, Linspf linspf) {	
		lifacmvrec.origamt.set(origamt);
		/* ADD +1 TO LIFA-JRNSEQ */	
	    if (isEQ(t5688rec.comlvlacc, "Y")) {
	        wsaaGlSub.set(39);
			lifacmvrec.rldgacct.set(wsaaRldgagnt);
	        lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
	    }
		lifacmvrec.tranref.set(agntNum);
		callLifacmv5000(linspf);
	}
	//IJTI-1727 ends

protected void postServiceCommission3000(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		/*  Service commission due*/
		if (isNE(wsaaSrvcpyDue, 0)) {
			
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			lifacmvrec.origamt.set(wsaaSrvcpyDue);
			wsaaGlSub.set(11);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			lifacmvrec.tranref.set(drypDryprcRecInner.drypEntity);
			callLifacmv5000(linspf);
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				zorlnkrec.effdate.set(agcmpf.getEfdate());
				b500CallZorcompy();
			}
		}
		/*  Service commission earned*/
		if (isNE(wsaaSrvcpyErn, 0)) {
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(25);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv5000(linspf);
		}
	}

protected void postRenewalCommission3100(Linspf linspf, Agcmpf agcmpf)
	{
		/*  Renewal commission due*/
		if (isNE(wsaaRnwcpyDue, 0)) {
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			wsaaGlSub.set(13);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaRnwcpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			lifacmvrec.tranref.set(drypDryprcRecInner.drypEntity);
			wsaaRldgChdrnum.set(agcmpf.getChdrnum());
			wsaaRldgLife.set(agcmpf.getLife());
			wsaaRldgCoverage.set(agcmpf.getCoverage());
			wsaaRldgRider.set(agcmpf.getRider());
			wsaaPlan.set(agcmpf.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			callLifacmv5000(linspf);
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmpf.getAnnprem());
				zorlnkrec.effdate.set(agcmpf.getEfdate());
				b500CallZorcompy();
				//IJTI-1727 starts
				if(gstOnCommFlag) {
    				gstPostings(zorlnkrec.gstAmount.getbigdata(),agcmpf.getAgntnum(), linspf);
    			}
				//IJTI-1727 ends
			}
		}
		/*  Renewal commission earned*/
		if (isNE(wsaaRnwcpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(26);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(14);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
			}
			lifacmvrec.tranref.set(agcmpf.getAgntnum());
			callLifacmv5000(linspf); //IJTI-1727
			//IJTI-1727 starts
			if(gstOnCommFlag) {
				gstPostings(wsaaCompErnRnwlCommGst.getbigdata(), agcmpf.getAgntnum(), linspf);
			}
			//IJTI-1727 ends
		}
	}

protected void postOverridCommission3200(Linspf linspf, Agcmpf agcmpf) //IJTI-1727
	{
		/*  Initial over-riding commission due*/
		if (isNE(wsaaOvrdBascpyDue, 0)) {
			/*     ADD   1                  TO LIFA-JRNSEQ                   */
			wsaaGlSub.set(16);
			lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
			lifacmvrec.rldgacct.set(agcmpf.getAgntnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.tranref.set(agcmpf.getCedagent());
			lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			callLifacmv5000(linspf);
		}
		/*  Initial over-riding commission earned*/
		if (isNE(wsaaOvrdBascpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(27);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(17);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(drypDryprcRecInner.drypEntity);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			callLifacmv5000(linspf);
		}
		/*  Initial over-riding commission paid*/
		if (isNE(wsaaOvrdBascpyPay, 0)) {
			/*     ADD  +1                  TO LIFA-JRNSEQ                   */
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			if (isEQ(t5688rec.comlvlacc, "Y")) {
				wsaaGlSub.set(28);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(wsaaCovrCrtable[wsaaCovrIx.toInt()]);
			}
			else {
				wsaaGlSub.set(18);
				lifacmvrec.tranref.set(agcmpf.getAgntnum());
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			}
			callLifacmv5000(linspf);
		}
	}

protected void writePtrn3500(Linspf linspf) //IJTI-1727
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setPtrneff(linspf.getInstfrom());
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		//IJTI-1727 starts
		if(isEQ(chdrlifIO.getNlgflg(), "Y")) {
			writeNLGRec();
		}
		//IJTI-1727 ends
	}

	//IJTI-1727 starts
	private void writeNLGRec() {
		Iterator<Nlgtpf> iteratorListNlg;
		List<Nlgtpf> nlgtpfList = nlgtpfDAO.readNlgtpf(ptrnIO.getChdrcoy().trim(), ptrnIO.getChdrnum().trim());
		if (!nlgtpfList.isEmpty()) {
			iteratorListNlg = nlgtpfList.iterator();
			while (iteratorListNlg.hasNext()) {
				Nlgtpf nlgtpf = iteratorListNlg.next();
				if (isEQ(nlgtpf.getEffdate(), ptrnIO.getPtrneff())
						&& toCallNlgcalc(nlgtpf)) {
					callNlgcaclUtil();
					break;
				}
			}

		}

	}
	
	private void callNlgcaclUtil() {
		NlgcalcPojo nlgcalcPojo = new NlgcalcPojo();
		nlgcalcPojo.setChdrcoy(ptrnIO.getChdrcoy().toString());
		nlgcalcPojo.setChdrnum(ptrnIO.getChdrnum().toString());
		nlgcalcPojo.setEffdate(ptrnIO.getPtrneff().toInt());
		nlgcalcPojo.setTranno(ptrnIO.getTranno().toInt());
		nlgcalcPojo.setBatcactyr(ptrnIO.getBatcactyr().toInt());
		nlgcalcPojo.setBatcactmn(ptrnIO.getBatcactmn().toInt());
		nlgcalcPojo.setBatctrcde(ptrnIO.getBatctrcde().toString());
		nlgcalcPojo.setBillfreq(chdrlifIO.getBillfreq().toString());
		nlgcalcPojo.setCnttype(chdrlifIO.getCnttype().toString());
		nlgcalcPojo.setLanguage(drypDryprcRecInner.drypLanguage.toString());
		nlgcalcPojo.setFrmdate(chdrlifIO.getOccdate().toInt());
		nlgcalcPojo.setTodate(varcom.vrcmMaxDate.toInt());
		nlgcalcPojo.setOccdate(chdrlifIO.getOccdate().toInt());
		nlgcalcPojo.setPtdate(chdrlifIO.getPtdate().toInt());
		nlgcalcPojo.setInputAmt(BigDecimal.ZERO);
		nlgcalcPojo.setUnpaidPrem(BigDecimal.ZERO);
		nlgcalcPojo.setOvduePrem(BigDecimal.ZERO);
		nlgcalcPojo.setFunction("COLCT");
		nlgcalcUtils.mainline(nlgcalcPojo);
		if (!Varcom.oK.toString().equals(nlgcalcPojo.getStatus())) {
			drylogrec.statuz.set(nlgcalcPojo.getStatus());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}
	
	private boolean toCallNlgcalc(Nlgtpf nlgtpf) {
		return (nlgtpf.getBatctrcde().equals("B521") && BigDecimal.ZERO.compareTo(nlgtpf.getAmnt04()) != 0)
				|| (nlgtpf.getBatctrcde().equals("B673")
						&& BigDecimal.ZERO.compareTo(nlgtpf.getAmnt03()) != 0);
	}
	//IJTI-1727 ends

protected void updateLinsPayr3600(Linspf linspf) //IJTI-1727
	{
		linsrnlIO.setPayflag("P");
		linsrnlIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(linsrnlIO.getStatuz());
			drylogrec.params.set(linsrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		
		//IJTI-1727 starts
		drycntrec.contotNumber.set(controlTotalsInner.ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		
		BigDecimal temptotal;
		if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate()){
			temptotal = linspf.getProramt().add(linspf.getProrcntfee());
		} else {
			temptotal = linspf.getInstamt06();
		}
		
		drycntrec.contotNumber.set(controlTotalsInner.ct03);
		drycntrec.contotValue.set(temptotal);
		d000ControlTotals();
        //IJTI-1727 ends
		
		setPrecision(payrIO.getTranno(), 0);
		payrIO.setTranno(add(payrIO.getTranno(), 1));
		payrIO.setPtdate(linsrnlIO.getInstto());
		setPrecision(payrIO.getOutstamt(), 2);
		payrIO.setOutstamt(sub(payrIO.getOutstamt(), linspf.getInstamt06()));
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.params.set(payrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		collectionDone.setTrue();
	}


protected void updateHdiv3900(Linspf linspf) //IJTI-1727
	{
		/* Write a record in HDIV with negative amount to denote           */
		/* cash dividend withdrawal if WSAA-DIV-REQUIRED > 0.              */
		if (isEQ(wsaaDivRequired, 0)) {
			return ;
		}
		hdisIO.setDataKey(SPACES);
		hdisIO.setChdrcoy(linspf.getChdrcoy());
		hdisIO.setChdrnum(linspf.getChdrnum());
		hdisIO.setLife("01");
		hdisIO.setCoverage("01");
		hdisIO.setRider("00");
		hdisIO.setPlanSuffix(ZERO);
		hdisIO.setFormat(formatsInner.hdisrec);
		hdisIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), Varcom.oK)
		&& isNE(hdisIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(hdisIO.getStatuz());
			drylogrec.params.set(hdisIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(hdisIO.getChdrcoy(), linspf.getChdrcoy())
		|| isNE(hdisIO.getChdrnum(), linspf.getChdrnum())
		|| isNE(hdisIO.getLife(), "01")
		|| isNE(hdisIO.getCoverage(), "01")
		|| isNE(hdisIO.getRider(), "00")
		|| isEQ(hdisIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(hdisIO.getStatuz());
			drylogrec.params.set(hdisIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		hdivIO.setParams(SPACES);
		hdivIO.setChdrcoy(linspf.getChdrcoy());
		hdivIO.setChdrnum(linspf.getChdrnum());
		hdivIO.setLife("01");
		hdivIO.setJlife("  ");
		hdivIO.setCoverage("01");
		hdivIO.setRider("00");
		hdivIO.setPlanSuffix(ZERO);
		hdivIO.setTranno(chdrlifIO.getTranno());
		hdivIO.setEffdate(drypDryprcRecInner.drypRunDate);
		hdivIO.setDivdAllocDate(drypDryprcRecInner.drypRunDate);
		hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
		hdivIO.setCntcurr(linspf.getCntcurr());
		setPrecision(hdivIO.getDivdAmount(), 2);
		hdivIO.setDivdAmount(mult(wsaaDivRequired, -1));
		hdivIO.setDivdRate(ZERO);
		hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
		hdivIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		hdivIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		hdivIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		hdivIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		hdivIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		hdivIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		hdivIO.setDivdType("C");
		hdivIO.setZdivopt(hcsdIO.getZdivopt());
		hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
		hdivIO.setDivdOptprocTranno(ZERO);
		hdivIO.setDivdCapTranno(ZERO);
		hdivIO.setDivdStmtNo(ZERO);
		hdivIO.setPuAddNbr(ZERO);
		hdivIO.setFormat(formatsInner.hdivrec);
		hdivIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, hdivIO);
		if (isNE(hdivIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(hdivIO.getStatuz());
			drylogrec.params.set(hdivIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


protected void finish4000()
	{
		start4010();
	}

protected void start4010()
	{
		/* Update the DRYP fields to determine next processing date.*/
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		/* Now that only one instalment period at a time is being collected*/
		/* ensure that the diary processing date is updated correctly.*/
		/* If the collection has been performed then we should have the*/
		/* PAYR information already.  Otherwise we need to do a quick read*/
		/* of the PAYR to obtain the info required by subsequent Diary*/
		/* programs.*/
		if (collectionDone.isTrue()) {
			/*CONTINUE_STMT*/
		}
		else {
			if (!validContract.isTrue()) {
				/*NEXT_SENTENCE*/
			}
			else {
				drypDryprcRecInner.processUnsuccesful.setTrue();
			}
		}
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypAplsupto.set(wsaaAplspto);
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		
		cleanUpData(); //IJTI-1727
	}

	//IJTI-1727 starts
	private void cleanUpData() {
		if(td5j2Map != null) {
			td5j2Map.clear();
			td5j2Map = null;
		}
		
		if(td5j1Map != null) {
			td5j1Map.clear();
			td5j1Map = null;
		}
		
		if(t5567Map != null) {
			t5567Map.clear();
			t5567Map = null;
		}
		
		if(incrDatedMap != null) {
			incrDatedMap.clear();
			incrDatedMap = null;
		}
		
		if(covrRiskPremMap != null) {
			covrRiskPremMap.clear();
			covrRiskPremMap = null;
		}
		
		if(insertRiskPremList != null) {
			insertRiskPremList.clear();
			insertRiskPremList = null;
		}
	}
	//IJTI-1727 ends

protected void callLifacmv5000(Linspf linspf) //IJTI-1727
	{

		/* This is the only place LIFACMV parameters are referenced.  LIFA*/
		/* fields that are changeable are set outside this section within*/
		/* a working storagwe variable.*/
		lifacmvrec.rdocnum.set(drypDryprcRecInner.drypEntity);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		/*  If the ACMV is being created for the COVR-INSTPREM, the*/
		/*  seventh table entries will be used.  Move the appropriate*/
		/*  value for the COVR posting.*/
		//IJTI-1727 starts
		if(reinstflag && isShortTerm ){
            lifacmvrec.effdate.set(ptrnpfReinstate.getDatesub());
        } else {
        	lifacmvrec.effdate.set(linspf.getInstfrom());
        }
		//IJTI-1727 ends
		varcom.vrcmTermid.set("9999");
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(0);
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub.toInt()]);
		/*    MOVE 'PSTW'                 TO LIFA-FUNCTION.                */
		lifacmvrec.function.set("PSTD");
		/* To avoid locking when updating the ACBLs of agent commission*/
		/* accounts, use the deferred method of updating the ACBLs.*/
		deferCheck5100();
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			wsysSysparams.set(lifacmvrec.lifacmvRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaRldgacct.set(SPACES);

	}


protected void deferCheck5100()
	{
		check5110();
	}

protected void check5110()
	{
		/* Check through all the parameters passed via DRYPRCREC to*/
		/* determine if the current LIFA-SACSCODE is one where the*/
		/* ACBL account should be deferred ( i.e. updated outside of*/
		/* this program to avoid locking problems).*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 25)); wsaaSub.add(1)){
			if (isEQ(drypDryprcRecInner.drypSystParm[wsaaSub.toInt()], lifacmvrec.sacscode)) {
				/*           MOVE 'NPSTW'         TO LIFA-FUNCTION                 */
				lifacmvrec.function.set("NPSTD");
				wsaaSub.set(26);
			}
		}
		/* No ACBL updates are to be deferred*/
		/*    IF  LIFA-FUNCTION            = 'PSTW'                        */
		if (isEQ(lifacmvrec.function, "PSTD")) {
			return ;
		}
		/* A deferred ACBL update has been found, so its time for action.*/
		/* Start by completeing the fields on the ACAGRNL data area.*/
		/* Once this is complete, write a new DACM record using the*/
		/* ACAGRNL data area.*/
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		/* OK, set up the DACM record, used for deferred processing*/
		/* within the batch diary system.*/
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(drypDryprcRecInner.drypThreadNumber);
		dacmIO.setCompany(drypDryprcRecInner.drypCompany);
		dacmIO.setRecformat(formatsInner.acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(formatsInner.dacmrec);
		dacmIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void updateApa6000(Linspf linspf) //IJTI-1727
	{
		try {
			checkAmt6010(linspf); //IJTI-1727
			params6020(linspf); //IJTI-1727
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	* This paragraph is required before the UPDATE-SUSPENSE so that
	* suspense will have enough money for the amount due. If there
	* has extract suspense for the amount due then NEXT SENTENCE. If
	* Adv Prem Deposit amount (APA) is required to pay the amount due
	* then move DELT to withdraw money from APA and put into suspense.
	* </pre>
	*/
protected void checkAmt6010(Linspf linspf) //IJTI-1727
	{
		initialize(rlpdlonrec.rec);
		if (isEQ(wsaaApaRequired, ZERO)) {
			goTo(GotoLabel.exit6090);
		}
		else {
			if (isGT(wsaaApaRequired, ZERO)) {
				rlpdlonrec.chdrcoy.set(linspf.getChdrcoy());
				rlpdlonrec.chdrnum.set(linspf.getChdrnum());
				rlpdlonrec.function.set(Varcom.delt);
				rlpdlonrec.prmdepst.set(wsaaApaRequired);
			}
			else {
				goTo(GotoLabel.exit6090);
			}
		}
	}

protected void params6020(Linspf linspf) //IJTI-1727
	{
		rlpdlonrec.pstw.set("PSTW");
		rlpdlonrec.chdrcoy.set(linspf.getChdrcoy());
		rlpdlonrec.chdrnum.set(linspf.getChdrnum());
		wsaaRdockey.rdocRdocpfx.set("CH");
		wsaaRdockey.rdocRdoccoy.set(linspf.getChdrcoy());
		wsaaRdockey.rdocRdocnum.set(linspf.getChdrnum());
		wsaaRdockey.rdocTranseq.set(lifacmvrec.jrnseq);
		rlpdlonrec.doctkey.set(wsaaRdockey);
		rlpdlonrec.effdate.set(linspf.getInstfrom());
		rlpdlonrec.currency.set(linspf.getBillcurr());
		rlpdlonrec.tranno.set(chdrlifIO.getTranno());
		rlpdlonrec.transeq.set(lifacmvrec.jrnseq);
		rlpdlonrec.longdesc.set(descIO.getLongdesc());
		rlpdlonrec.language.set(drypDryprcRecInner.drypLanguage);
		rlpdlonrec.prefix.set(drypDryprcRecInner.drypBatcpfx);
		rlpdlonrec.company.set(drypDryprcRecInner.drypBatccoy);
		rlpdlonrec.branch.set(drypDryprcRecInner.drypBatcbrn);
		rlpdlonrec.actyear.set(drypDryprcRecInner.drypBatcactyr);
		rlpdlonrec.actmonth.set(drypDryprcRecInner.drypBatcactmn);
		rlpdlonrec.trcde.set(drypDryprcRecInner.drypBatctrcde);
		rlpdlonrec.authCode.set(drypDryprcRecInner.drypBatctrcde);
		rlpdlonrec.batch.set(drypDryprcRecInner.drypBatcbatch);
		rlpdlonrec.time.set(varcom.vrcmTime);
		rlpdlonrec.date_var.set(varcom.vrcmDate);
		rlpdlonrec.user.set(0);
		rlpdlonrec.termid.set(varcom.vrcmTermid);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			drylogrec.params.set(rlpdlonrec.rec);
			drylogrec.statuz.set(rlpdlonrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		lifacmvrec.jrnseq.set(rlpdlonrec.transeq);
	}

protected void writeLetter6100(Linspf linspf) //IJTI-1727
	{
		start6110();
		readTr3846120(linspf);
	}

protected void start6110()
	{
		/* Read Table TR384 for get letter-type.                           */
		wsaaItemBatctrcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaItemCnttype.set(chdrlifIO.getCnttype());
	}

protected void readTr3846120(Linspf linspf) //IJTI-1727
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(linspf.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), Varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				readTr3846120(linspf);
				return ;
			}
			else {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/* Allocate next automatic Receipt Number.                         */
		alocnorec.function.set("NEXT ");
		alocnorec.prefix.set("RW");
		alocnorec.genkey.set(drypDryprcRecInner.drypBranch);
		alocnorec.company.set(linspf.getChdrcoy());
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, Varcom.oK)) {
			drylogrec.params.set(alocnorec.alocnoRec);
			drylogrec.statuz.set(alocnorec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  WRITE INTO TRWP FILE                                           */
		trwpIO.setChdrcoy(chdrlifIO.getChdrcoy());
		trwpIO.setChdrnum(chdrlifIO.getChdrnum());
		trwpIO.setEffdate(drypDryprcRecInner.drypRunDate);
		trwpIO.setTranno(chdrlifIO.getTranno());
		trwpIO.setValidflag("1");
		trwpIO.setRdocpfx("RW");
		trwpIO.setRdoccoy(linspf.getChdrcoy());
		trwpIO.setRdocnum(alocnorec.alocNo);
		trwpIO.setOrigcurr(linspf.getBillcurr());
		trwpIO.setOrigamt(linspf.getCbillamt());
		trwpIO.setFormat(formatsInner.trwprec);
		trwpIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, trwpIO);
		if (isNE(trwpIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(trwpIO.getParams());
			drylogrec.statuz.set(trwpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Get set-up parameter for call 'LETRQST'                         */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(drypDryprcRecInner.drypRunDate);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(linspf.getChdrcoy());
		letrqstrec.rdocnum.set(linspf.getChdrnum());
		letrqstrec.otherKeys.set(drypDryprcRecInner.drypLanguage);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, Varcom.oK)) {
			drylogrec.params.set(letrqstrec.params);
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void waiverHolding7000(Linspf linspf) //IJTI-1727
	{
		acblIO.setRldgacct(linspf.getChdrnum());
		acblIO.setRldgcoy(linspf.getChdrcoy());
		acblIO.setOrigcurr(linspf.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[7]);
		acblIO.setSacstyp(wsaaT5645Sacstype[7]);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK)
		&& isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(acblIO.getStatuz());
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaWaiverAvail.set(0);
		}
		else {
			wsaaWaiverAvail.set(acblIO.getSacscurbal());
		}
	}


protected void b100InitializeArrays()
	{
		/*B110-INIT*/
		for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx, wsaaAgcmIxSize)); wsaaAgcmIx.add(1)){
			wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
			for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)); wsaaAgcmIy.add(1)){
				wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
			}
		}
		/*B190-EXIT*/
	}

protected void b200PremiumHistory(Covrpf covrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b210Init(covrpf); //IJTI-1727
				case b220Call:
					b220Call(covrpf); //IJTI-1727
					b230Summary();
				case b280Next:
					b280Next();
				case b290Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b210Init(Covrpf covrpf) //IJTI-1727
	{
		/* Read the AGCM for differentiating the First & Renewal Year      */
		/* Premium                                                         */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(covrpf.getChdrcoy());
		agcmseqIO.setChdrnum(covrpf.getChdrnum());
		agcmseqIO.setLife(covrpf.getLife());
		agcmseqIO.setCoverage(covrpf.getCoverage());
		agcmseqIO.setRider(covrpf.getRider());
		agcmseqIO.setPlanSuffix(covrpf.getPlanSuffix());
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setFormat(formatsInner.agcmseqrec);
		agcmseqIO.setFunction(Varcom.begn);
	}

protected void b220Call(Covrpf covrpf) //IJTI-1727
	{
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(), Varcom.oK)
		&& isNE(agcmseqIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(agcmseqIO.getStatuz());
			drylogrec.params.set(agcmseqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(agcmseqIO.getStatuz(), Varcom.endp)
		|| isNE(agcmseqIO.getChdrcoy(), covrpf.getChdrcoy())
		|| isNE(agcmseqIO.getChdrnum(), covrpf.getChdrnum())
		|| isNE(agcmseqIO.getLife(), covrpf.getLife())
		|| isNE(agcmseqIO.getCoverage(), covrpf.getCoverage())
		|| isNE(agcmseqIO.getRider(), covrpf.getRider())) {
			agcmseqIO.setStatuz(Varcom.endp);
			goTo(GotoLabel.b290Exit);
		}
		/* Skip those irrelevant records                                   */
		if (isNE(agcmseqIO.getOvrdcat(), "B")
		|| isEQ(agcmseqIO.getPtdate(), ZERO)
		|| isEQ(agcmseqIO.getEfdate(), ZERO)
		|| isEQ(agcmseqIO.getEfdate(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.b280Next);
		}
	}

protected void b230Summary()
	{
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)
		|| isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)
		|| agcmFound.isTrue()); wsaaAgcmIy.add(1)){
			if (isEQ(agcmseqIO.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()])) {
				wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy, wsaaAgcmIySize)) {
				drylogrec.statuz.set(e103);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

protected void b280Next()
	{
		agcmseqIO.setFunction(Varcom.nextr);
		goTo(GotoLabel.b220Call);
	}

protected void b300WriteArrays(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b310Init();
				case b320Loop:
					b320Loop(linspf); //IJTI-1727
					b330Writ(linspf, covrpf); //IJTI-1727
				case b380Next:
					b380Next();
				case b390Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b310Init()
	{
		wsaaAgcmIy.set(1);
	}

protected void b320Loop(Linspf linspf) //IJTI-1727
	{
		if (isEQ(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			goTo(GotoLabel.b390Exit);
		}
		else {
			if (isGT(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], linspf.getInstfrom())) {
				goTo(GotoLabel.b380Next);
			}
		}
	}

protected void b330Writ(Linspf linspf, Covrpf covrpf) //IJTI-1727
	{
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(linspf.getInstfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		}
		else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrpf.getChdrcoy());
			zptnIO.setChdrnum(covrpf.getChdrnum());
			zptnIO.setLife(covrpf.getLife());
			zptnIO.setCoverage(covrpf.getCoverage());
			zptnIO.setRider(covrpf.getRider());
			zptnIO.setTranno(chdrlifIO.getTranno());
			setPrecision(zptnIO.getOrigamt(), 3);
			zptnIO.setOrigamt(div(wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaBillfq9), true);
			zptnIO.setTransCode(drypDryprcRecInner.drypBatctrcde);
			zptnIO.setEffdate(linspf.getInstfrom());
			zptnIO.setInstfrom(linspf.getInstfrom());
			zptnIO.setBillcd(linspf.getBillcd());
			zptnIO.setInstto(linspf.getInstto());
			zptnIO.setTrandate(wsaaTday);
			zptnIO.setZprflg(wsaaPremType);
			zptnIO.setFormat(formatsInner.zptnrec);
			zptnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zptnIO);
			if (isNE(zptnIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(zptnIO.getStatuz());
				drylogrec.params.set(zptnIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
	}

protected void b380Next()
	{
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.b320Loop);
	}

protected void b400LocatePremium(Agcmpf agcmpf) //IJTI-1727
	{
		/*B410-INIT*/
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa, wsaaAgcmIxSize)
		|| agcmFound.isTrue()); wsaaAgcmIa.add(1)){
			if (isEQ(agcmpf.getChdrcoy(), wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
			&& isEQ(agcmpf.getChdrnum(), wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
			&& isEQ(agcmpf.getLife(), wsaaAgcmLife[wsaaAgcmIa.toInt()])
			&& isEQ(agcmpf.getCoverage(), wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
			&& isEQ(agcmpf.getRider(), wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
				for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb, wsaaAgcmIySize)
				|| agcmFound.isTrue()); wsaaAgcmIb.add(1)){
					if (isEQ(agcmpf.getSeqno(), wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
						agcmFound.setTrue();
						wsaaAgcmPremium.set(wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
						break;
					}
				}
			}
		}
		/*B490-EXIT*/
	}

protected void b500CallZorcompy()
	{
		b510Start();
	}

protected void b510Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrlifIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrlifIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set(SPACES);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zorlnkrec.statuz);
			drylogrec.params.set(zorlnkrec.zorlnkRec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void b600GetTax(Linspf linspf) //IJTI-1727
	{
		/* Read table TR52D                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlifIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrlifIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		wsaaTotTax.set(ZERO);
		taxdbilIO.setStatuz(Varcom.oK);
		taxdbilIO.setChdrcoy(linspf.getChdrcoy());
		taxdbilIO.setChdrnum(linspf.getChdrnum());
		taxdbilIO.setInstfrom(linspf.getInstfrom());
		taxdbilIO.setLife(SPACES);
		taxdbilIO.setCoverage(SPACES);
		taxdbilIO.setRider(SPACES);
		taxdbilIO.setTrantype(SPACES);
		taxdbilIO.setPlansfx(ZERO);
		taxdbilIO.setFunction(Varcom.begn);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		while ( !(isEQ(taxdbilIO.getStatuz(), Varcom.endp))) {
			b60aProcessTax(linspf);
		}
	}


protected void b60aProcessTax(Linspf linspf) //IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b60aStart(linspf);
				case b60aNextRec:
					b60aNextRec();
				case b60aExit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b60aStart(Linspf linspf) //IJTI-1727
	{
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), Varcom.oK)
		&& isNE(taxdbilIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(taxdbilIO.getStatuz());
			drylogrec.params.set(taxdbilIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(taxdbilIO.getChdrcoy(), linspf.getChdrcoy())
		|| isNE(taxdbilIO.getChdrnum(), linspf.getChdrnum())
		|| isNE(taxdbilIO.getInstfrom(), linspf.getInstfrom())
		|| isEQ(taxdbilIO.getStatuz(), Varcom.endp)) {
			taxdbilIO.setStatuz(Varcom.endp);
			goTo(GotoLabel.b60aExit);
		}
		if (isEQ(taxdbilIO.getPostflg(), SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.b60aNextRec);
		}
		if (isNE(taxdbilIO.getTxabsind(1), "Y")) {
			wsaaTotTax.add(taxdbilIO.getTaxamt(1));
		}
		if (isNE(taxdbilIO.getTxabsind(2), "Y")) {
			wsaaTotTax.add(taxdbilIO.getTaxamt(2));
		}
		if (isNE(taxdbilIO.getTxabsind(3), "Y")) {
			wsaaTotTax.add(taxdbilIO.getTaxamt(3));
		}
	}

protected void b60aNextRec()
	{
		taxdbilIO.setFunction(Varcom.nextr);
	}

protected void b700PostTax(Linspf linspf) //IJTI-1727
	{
		taxdbilIO.setStatuz(Varcom.oK);
		taxdbilIO.setChdrcoy(linspf.getChdrcoy());
		taxdbilIO.setChdrnum(linspf.getChdrnum());
		taxdbilIO.setInstfrom(linspf.getInstfrom());
		taxdbilIO.setLife(wsaaTxLife);
		taxdbilIO.setCoverage(wsaaTxCoverage);
		taxdbilIO.setRider(wsaaTxRider);
		taxdbilIO.setPlansfx(wsaaTxPlansfx);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		taxdbilIO.setFunction(Varcom.begn);
		taxdbilIO.setFormat(formatsInner.taxdbilrec);
		while ( !(isEQ(taxdbilIO.getStatuz(), Varcom.endp))) {
			b710ProcPostTax(linspf);
		}
	}


protected void b710ProcPostTax(Linspf linspf) //IJTI-1727
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b710Start(linspf);
				case b710NextRec:
					b710NextRec();
				case b710Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void b710Start(Linspf linspf) //IJTI-1727
	{
		SmartFileCode.execute(appVars, taxdbilIO);
		if (isNE(taxdbilIO.getStatuz(), Varcom.oK)
		&& isNE(taxdbilIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(taxdbilIO.getStatuz());
			drylogrec.params.set(taxdbilIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(taxdbilIO.getStatuz(), Varcom.endp)) {
			goTo(GotoLabel.b710Exit);
		}
		if (isNE(taxdbilIO.getChdrcoy(), linspf.getChdrcoy())
		|| isNE(taxdbilIO.getChdrnum(), linspf.getChdrnum())
		|| isNE(taxdbilIO.getInstfrom(), linspf.getInstfrom())
		|| isNE(taxdbilIO.getLife(), wsaaTxLife)
		|| isNE(taxdbilIO.getCoverage(), wsaaTxCoverage)
		|| isNE(taxdbilIO.getRider(), wsaaTxRider)
		|| isNE(taxdbilIO.getPlansfx(), wsaaTxPlansfx)
		|| isEQ(taxdbilIO.getStatuz(), Varcom.endp)) {
			/* rewrite to release the last record read                       */
			/*                                                         <LA4758>*/
			/*            SET DRY-DATABASE-ERROR  TO TRUE              <LA4758>*/
			/*            PERFORM A000-FATAL-ERROR                     <LA4758>*/
			/*        END-IF                                           <LA4758>*/
			taxdbilIO.setStatuz(Varcom.endp);
			goTo(GotoLabel.b710Exit);
		}
		if (isEQ(taxdbilIO.getPostflg(), SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.b710NextRec);
		}
		/* Read table TR52E                                                */
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(linspf.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(subString(taxdbilIO.getTranref(), 1, 8));
		itdmIO.setItmfrm(taxdbilIO.getEffdate());
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), Varcom.oK))
		&& (isNE(itdmIO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getItemcoy(), linspf.getChdrcoy())
		&& isEQ(itdmIO.getItemtabl(), tablesInner.tr52e)
		&& isEQ(itdmIO.getItemitem(), subString(taxdbilIO.getTranref(), 1, 8))
		&& isNE(itdmIO.getStatuz(), Varcom.endp)) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
		/* Call tax subroutine                                             */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(taxdbilIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdbilIO.getChdrnum());
		txcalcrec.life.set(taxdbilIO.getLife());
		txcalcrec.coverage.set(taxdbilIO.getCoverage());
		txcalcrec.rider.set(taxdbilIO.getRider());
		txcalcrec.planSuffix.set(taxdbilIO.getPlansfx());
		txcalcrec.crtable.set(wsaaTxCrtable);
		if (isEQ(wsaaTxCrtable, SPACES)) {
			txcalcrec.cntTaxInd.set("Y");
		}
		txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		txcalcrec.register.set(chdrlifIO.getRegister());
		txcalcrec.tranno.set(chdrlifIO.getTranno());
		txcalcrec.taxrule.set(subString(taxdbilIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(subString(taxdbilIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdbilIO.getBaseamt());
		txcalcrec.effdate.set(taxdbilIO.getEffdate());
		txcalcrec.transType.set(taxdbilIO.getTrantype());
		txcalcrec.taxType[1].set(taxdbilIO.getTxtype01());
		txcalcrec.taxType[2].set(taxdbilIO.getTxtype02());
		txcalcrec.taxAmt[1].set(taxdbilIO.getTaxamt01());
		txcalcrec.taxAmt[2].set(taxdbilIO.getTaxamt02());
		txcalcrec.taxAbsorb[1].set(taxdbilIO.getTxabsind01());
		txcalcrec.taxAbsorb[2].set(taxdbilIO.getTxabsind02());
		txcalcrec.jrnseq.set(lifacmvrec.jrnseq);
		txcalcrec.batckey.set(drypDryprcRecInner.drypBatchKey);
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.language.set(drypDryprcRecInner.drypLanguage);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			drylogrec.params.set(txcalcrec.linkRec);
			drylogrec.statuz.set(txcalcrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		lifacmvrec.jrnseq.set(txcalcrec.jrnseq);
		/* Update TAXD record                                              */
		/*                                                         <LA4758>*/
		/*                                                         <LA4758>*/
		/*        SET DRY-DATABASE-ERROR  TO TRUE                  <LA4758>*/
		/*        PERFORM A000-FATAL-ERROR                         <LA4758>*/
		/*    END-IF.                                              <LA4758>*/
		taxdIO.setParams(SPACES);
		taxdIO.setRrn(taxdbilIO.getRrn());
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(Varcom.readd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(taxdIO.getStatuz());
			drylogrec.params.set(taxdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		taxdIO.setPostflg("P");
		taxdIO.setFunction(Varcom.writd);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(taxdIO.getStatuz());
			drylogrec.params.set(taxdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	/**
	* <pre>
	* Read next TAXD
	* </pre>
	*/
protected void b710NextRec()
	{
		taxdbilIO.setFunction(Varcom.nextr);
	}

protected void c000CallRounding()
	{
		/*C100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			a000FatalError();
		}
		/*C900-EXIT*/
	}

	private void updateRiskPremium() {
		Map<String, List<Rskppf>> rskppfMap = rskppfDAO.searchRskppf(drypDryprcRecInner.drypCompany.trim(),
				Arrays.asList(drypDryprcRecInner.drypEntity.toString()));
		if(rskppfMap != null && !rskppfMap.isEmpty()) {
			List<Rskppf> riskPremiumList = rskppfMap.get(drypDryprcRecInner.drypEntity.toString());
			readT5567();
			for (Rskppf rskppf : riskPremiumList) {
				BigDecimal tempRiskPrem = rskppf.getRiskprem();
				BigDecimal tempPolFee = wsaapolFee.getbigdata();
				premiumrec.cnttype.set(rskppf.getCnttype());
				premiumrec.crtable.set(rskppf.getCrtable());
				premiumrec.calcTotPrem.set(rskppf.getCovrinstprem());
				callProgram("RISKPREMIUM", premiumrec.premiumRec);
				if (drypDryprcRecInner.drypRunDate.toInt() >= rskppf.getDatefrm()
						&& drypDryprcRecInner.drypRunDate.toInt() <= rskppf.getDateto()) {
					rskppf.setRiskprem(premiumrec.riskPrem.getbigdata().add(tempRiskPrem));
					rskppf.setPolfee(rskppf.getPolfee().add(tempPolFee));
					if (covrRiskPremMap.containsKey(rskppf.getChdrnum())) {
						covrRiskPremMap.get(rskppf.getChdrnum()).add(rskppf);
					} else {
						List<Rskppf> rskList = new ArrayList<Rskppf>();
						rskList.add(rskppf);
						covrRiskPremMap.put(rskppf.getChdrnum(), rskList);
					}
				} else if (drypDryprcRecInner.drypRunDate.toInt() > rskppf.getDateto()) {
					rskppf.setRiskprem(premiumrec.riskPrem.getbigdata());
					rskppf.setPolfee(tempPolFee);
					setRiskPremData(rskppf);
				}
			}
		}
	}
	
	private void readT5567() {
		String itemitem = chdrlifIO.getCnttype().trim() + chdrlifIO.getCntcurr().trim();
		if (t5567Map.containsKey(itemitem)) {
			List<Itempf> t5567List = t5567Map.get(itemitem);
			for (Itempf t5567Item : t5567List) {
				t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));
			}
		} else {
			t5567rec.t5567Rec.set(SPACES);
		}

		wsaapolFee.set(ZERO);
		for (int i = 1; i <= 10; i++) {
			if (isEQ(t5567rec.billfreq[i], chdrlifIO.getBillfreq())) {
				wsaapolFee.set(t5567rec.cntfee[i]);
				break;
			}
		}
	}
	
	private void setRiskPremData(Rskppf rskppf) {
		rskppf.setEffDate(drypDryprcRecInner.drypRunDate.toInt());
		calculateDate(rskppf);
		insertRiskPremList.add(rskppf);
	}
	
	private void calculateDate(Rskppf rskppf) {
		Calendar proposalDate = Calendar.getInstance();
	    Calendar taxFromDate = Calendar.getInstance();
	    Calendar taxToDate = Calendar.getInstance();
		try {
			proposalDate.setTime(dateFormat.parse(String.valueOf(rskppf.getEffDate())));
		    int month=proposalDate.get(Calendar.MONTH)+1;
		    if(month>=7) {
		    	taxFromDate.set(proposalDate.get(Calendar.YEAR), 6, 1);
		    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
		    }
		    else { 
		    	taxFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 1);
		    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
		    }
		    rskppf.setDatefrm(Integer.parseInt(dateFormat.format(taxFromDate.getTime())));
		    rskppf.setDateto(Integer.parseInt(dateFormat.format(taxToDate.getTime())));
		    rskppf.setEffDate(drypDryprcRecInner.drypRunDate.toInt());
		}
		catch (ParseException e) {		
			drylogrec.statuz.set("Error");
			drylogrec.params.set("Exception occurred while parsing date");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}
	
	private void updateRecords() {
		if (covrRiskPremMap != null && !covrRiskPremMap.isEmpty()) {
			rskppfDAO.updateRiskPremium(covrRiskPremMap);
		}
		if (insertRiskPremList != null && !insertRiskPremList.isEmpty()) {
			rskppfDAO.insertRecords(insertRiskPremList);
		}
	}

	//IJTI-1727 ends
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
	private FixedLengthStringData t3695 = new FixedLengthStringData(6).init("T3695");
	private FixedLengthStringData t5644 = new FixedLengthStringData(6).init("T5644");
	private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
	private FixedLengthStringData t5671 = new FixedLengthStringData(6).init("T5671");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
	private FixedLengthStringData t5667 = new FixedLengthStringData(6).init("T5667");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
	private FixedLengthStringData t6654 = new FixedLengthStringData(6).init("T6654");
	private FixedLengthStringData t6687 = new FixedLengthStringData(6).init("T6687");
	private FixedLengthStringData tr384 = new FixedLengthStringData(6).init("TR384");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");
	private FixedLengthStringData t5534 = new FixedLengthStringData(6).init("T5534");
	private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
	private FixedLengthStringData tr52d = new FixedLengthStringData(6).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(6).init("TR52E");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData dacmrec = new FixedLengthStringData(10).init("DACMREC");
	private FixedLengthStringData acagrnlrec = new FixedLengthStringData(10).init("ACAGRNLREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData trwprec = new FixedLengthStringData(10).init("TRWPREC");
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData agcmseqrec = new FixedLengthStringData(10).init("AGCMSEQREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData taxdbilrec = new FixedLengthStringData(10).init("TAXDBILREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
}
