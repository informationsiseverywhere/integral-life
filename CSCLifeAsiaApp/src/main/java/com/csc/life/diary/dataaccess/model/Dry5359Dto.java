package com.csc.life.diary.dataaccess.model;

/**
 * Dry5359Dto - POJO For Flexible Premium Collection
 * 
 * @author svemula29
 *
 */
public class Dry5359Dto {
	private String chdrCoy;
	private String chdrNum;
	private int payrseqno;
	private int totalRecd;
	private int totalBilled;
	private int currFrom;
	private int currTo;
	private String validFlag;
	
	/**
	 * Constructor
	 */
	public Dry5359Dto() {
		super();
		// Calling default constructor
	}
	
	/**
	 * @return the chdrCoy
	 */
	public String getChdrCoy() {
		return chdrCoy;
	}
	
	/**
	 * @param chdrCoy
	 *            the chdrCoy to set
	 */
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}
	
	/**
	 * @return the chdrNum
	 */
	public String getChdrNum() {
		return chdrNum;
	}
	
	/**
	 * @param chdrNum
	 *            the chdrNum to set
	 */
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	/**
	 * @return the payrseqno
	 */
	public int getPayrseqno() {
		return payrseqno;
	}

	/**
	 * @param payrseqno
	 *            the payrseqno to set
	 */
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}

	/**
	 * @return the totalRecd
	 */
	public int getTotalRecd() {
		return totalRecd;
	}

	/**
	 * @param totalRecd
	 *            the totalRecd to set
	 */
	public void setTotalRecd(int totalRecd) {
		this.totalRecd = totalRecd;
	}

	/**
	 * @return the totalBilled
	 */
	public int getTotalBilled() {
		return totalBilled;
	}

	/**
	 * @param totalBilled
	 *            the totalBilled to set
	 */
	public void setTotalBilled(int totalBilled) {
		this.totalBilled = totalBilled;
	}

	/**
	 * @return the currFrom
	 */
	public int getCurrFrom() {
		return currFrom;
	}

	/**
	 * @param currFrom
	 *            the currFrom to set
	 */
	public void setCurrFrom(int currFrom) {
		this.currFrom = currFrom;
	}

	/**
	 * @return the currTo
	 */
	public int getCurrTo() {
		return currTo;
	}

	/**
	 * @param currTo
	 *            the currTo to set
	 */
	public void setCurrTo(int currTo) {
		this.currTo = currTo;
	}

	/**
	 * @return the validFlag
	 */
	public String getValidFlag() {
		return validFlag;
	}

	/**
	 * @param validFlag
	 *            the validFlag to set
	 */
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

					

}
