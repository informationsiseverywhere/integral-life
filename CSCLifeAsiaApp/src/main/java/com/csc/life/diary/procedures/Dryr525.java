/*
 * File: Dryr525.java
 * Date: December 3, 2013 2:25:34 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYR525.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diaryframework.parent.Maind;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.cashdividends.dataaccess.HsudTableDAM;
import com.csc.life.contractservicing.dataaccess.TpoldbtTableDAM;
import com.csc.life.contractservicing.tablestructures.Totloanrec;
import com.csc.life.general.tablestructures.T5611rec;
import com.csc.life.productdefinition.procedures.Vpusurc;
import com.csc.life.productdefinition.procedures.Vpxsurc;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.recordstructures.Vpxsurcrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.ChdrsurTableDAM;
import com.csc.life.terminationclaims.dataaccess.CnfsTableDAM;
import com.csc.life.terminationclaims.dataaccess.CovrclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.terminationclaims.procedures.P5084at;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.recordstructures.SurtaxRec;
import com.csc.life.terminationclaims.tablestructures.Tr691rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*       DIARY/400 Non-Forfeiture Surrender
*       ------------------------------------
* This program makes up part of the Diary System.
* Its function is to simulate the BR525 Batch process
* that is to make non-forfeiture surrender on policies.
*                                                                     *
* </pre>
*/
public class Dryr525 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR525");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaPlanSuffix = new ZonedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private String wsaaMaturityFlag = "";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaT5611Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5611Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5611Item, 0);
	private FixedLengthStringData wsaaT5611Pstatcode = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT5611Item, 6, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaStoredCurrency = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaHeldCurrLoans = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaClamamt = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaLoanValue = new PackedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaSurrenderRecStore = new FixedLengthStringData(150);

	private ZonedDecimalData wsaaCurrencySwitch = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator detailsSameCurrency = new Validator(wsaaCurrencySwitch, 0);
	private Validator detailsDifferent = new Validator(wsaaCurrencySwitch, 1);
	private Validator totalCurrDifferent = new Validator(wsaaCurrencySwitch, 2);
	private PackedDecimalData wsaaActvalue = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(18, 2).init(0);
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaRacdChdrcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaRacdChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaRacdLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRacdCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRacdRider = new FixedLengthStringData(2).init(SPACES);

	private ZonedDecimalData wsaaSwitch2 = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator firstTime = new Validator(wsaaSwitch2, 1);

	private FixedLengthStringData wsaaFirstTimeFlag = new FixedLengthStringData(1).init("Y");
	private Validator firstTimeThrough = new Validator(wsaaFirstTimeFlag, "Y");
	private PackedDecimalData wsaaEstimateTot = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaActualTot = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaPenaltyTot = new PackedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).init(SPACES);
	private String wsaaNoPrice = "";
	private FixedLengthStringData wsaaSumFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private Validator invalidContract = new Validator(wsaaValidContract, "N");

	private FixedLengthStringData wsaaValidCnfs = new FixedLengthStringData(1);
	private Validator validCnfs = new Validator(wsaaValidCnfs, "Y");
	private Validator invalidCnfs = new Validator(wsaaValidCnfs, "N");
	private static final String itemrec = "ITEMREC";
	private static final String cnfsrec = "CNFSREC";
	private static final String chdrsurrec = "CHDRSURREC";
	private static final String covrclmrec = "COVRCLMREC";
	private static final String tpoldbtrec = "TPOLDBTREC";
	private static final String surdclmrec = "SURDCLMREC";
	private static final String surhclmrec = "SURHCLMREC";
	private static final String hsudrec = "HSUDREC";
	private static final String t5611 = "T5611";
	private static final String tr691 = "TR691";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t6598 = "T6598";
		/* ERRORS */
	private static final String f294 = "F294";

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private ChdrsurTableDAM chdrsurIO = new ChdrsurTableDAM();
	private CnfsTableDAM cnfsIO = new CnfsTableDAM();
	private CovrclmTableDAM covrclmIO = new CovrclmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HsudTableDAM hsudIO = new HsudTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	private TpoldbtTableDAM tpoldbtIO = new TpoldbtTableDAM();
	private Varcom varcom = new Varcom();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Srcalcpy srcalcpy = new Srcalcpy();
	private T5687rec t5687rec = new T5687rec();
	private T6598rec t6598rec = new T6598rec();
	private T5679rec t5679rec = new T5679rec();
	private T5611rec t5611rec = new T5611rec();
	private Tr691rec tr691rec = new Tr691rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Totloanrec totloanrec = new Totloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private WsaaAtmodRecInner wsaaAtmodRecInner = new WsaaAtmodRecInner();
	private ExternalisedRules er = new ExternalisedRules();
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		finish180, 
		read2220
	}

	public Dryr525() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start110();
				case finish180: 
					finish180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start110()
	{
		initialise200();
		if (invalidCnfs.isTrue()) {
			goTo(GotoLabel.finish180);
		}
		getFirstComponent1000();
		if (invalidContract.isTrue()) {
			goTo(GotoLabel.finish180);
		}
		while ( !(isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			processComponents1100();
		}
		
		// IVE-801 - RUL Product - Surrender Tax Calculation - Integration with latest PA compatible models - START
		/*  Computation of tax amount to be imposed.                       */
		/*ILIFE-2397 Start */
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("SURTAX") && er.isCallExternal("SURTAX") && er.isExternalized(chdrsurIO.cnttype.toString(), covrclmIO.crtable.toString())))//ILIFE-4833
		{
			getTaxAmount2000();
		}
		else
		{
			SurtaxRec surtaxRec = new SurtaxRec();	
			surtaxRec.cnttype.set(chdrsurIO.cnttype);
			surtaxRec.cntcurr.set(chdrsurIO.cntcurr);
			surtaxRec.effectiveDate.set(srcalcpy.effdate);
			surtaxRec.occDate.set(srcalcpy.effdate);
			surtaxRec.actualAmount.set(wsaaActualTot);						
			callProgram("SURTAX", surtaxRec.surtaxRec);			
			wsaaTaxAmt.set(surtaxRec.taxAmount);
		}
		/*ILIFE-2397 End */
		// IVE-801 - RUL Product - Surrender Tax Calculation - Integration with latest PA compatible models - END

		/*  Get the value of any Loans held against this component.*/
		getLoanDetails2100();
		getPolicyDebt2200();
		surhclmIO.setDataArea(SPACES);
		surhclmIO.setPlanSuffix(wsaaPlanSuffix);
		surhclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surhclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surhclmIO.setLife(covrclmIO.getLife());
		surhclmIO.setJlife(covrclmIO.getJlife());
		surhclmIO.setCurrcd(chdrsurIO.getCntcurr());
		surhclmIO.setEffdate(cnfsIO.getEffdate());
		surhclmIO.setTranno(wsaaTranno);
		surhclmIO.setCnttype(chdrsurIO.getCnttype());
		surhclmIO.setReasoncd(SPACES);
		surhclmIO.setResndesc(SPACES);
		surhclmIO.setPolicyloan(wsaaHeldCurrLoans);
		surhclmIO.setTdbtamt(wsaaAtmodRecInner.wsaaTdbtamt);
		surhclmIO.setZrcshamt(wsaaAtmodRecInner.wsaaZrcshamt);
		surhclmIO.setOtheradjst(wsaaOtheradjst);
		surhclmIO.setTaxamt(wsaaTaxAmt);
		surhclmIO.setFunction(varcom.writr);
		surhclmIO.setFormat(surhclmrec);
		SmartFileCode.execute(appVars, surhclmIO);
		if (isNE(surhclmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(surhclmIO.getStatuz());
			drylogrec.params.set(surhclmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaAtmodRecInner.wsaaAtmodRec.set(SPACES);
		wsaaAtmodRecInner.wsaaAtdmBatchKey.set(drypDryprcRecInner.drypBatchKey);
		wsaaAtmodRecInner.wsaaAtdmLanguage.set(drypDryprcRecInner.drypLanguage);
		wsaaAtmodRecInner.wsaaAtdmCompany.set(drypDryprcRecInner.drypCompany);
		wsaaAtmodRecInner.wsaaPrimaryChdrnum.set(chdrsurIO.getChdrnum());
		wsaaAtmodRecInner.wsaaTransDate.set(varcom.vrcmDate);
		wsaaAtmodRecInner.wsaaTransTime.set(varcom.vrcmTime);
		wsaaAtmodRecInner.wsaaTransUser.set(0);
		wsaaAtmodRecInner.wsaaTransTermid.set(varcom.vrcmTerm);
		wsaaAtmodRecInner.wsaaAtdmStatuz.set(varcom.oK);
		callProgram(P5084at.class, wsaaAtmodRecInner.wsaaAtmodRec);
		if (isNE(wsaaAtmodRecInner.wsaaAtdmStatuz, varcom.oK)) {
			drylogrec.statuz.set(wsaaAtmodRecInner.wsaaAtdmStatuz);
			drylogrec.params.set(wsaaAtmodRecInner.wsaaAtmodRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Update CNFS*/
		cnfsIO.setValidflag("2");
		cnfsIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(cnfsIO.getParams());
			drylogrec.statuz.set(cnfsIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* As in P5084AT subroutine, this diary entity is unlocked, so lock*/
		/* this entity here again, as the Diary process will UNLK it later */
		sftlockrec.enttyp.set(drypDryprcRecInner.drypEntityType);
		sftlockrec.company.set(drypDryprcRecInner.drypCompany);
		sftlockrec.user.set(0);
		sftlockrec.transaction.set(drypDryprcRecInner.drypBatctrcde);
		sftlockrec.function.set("LOCK");
		sftlockrec.entity.set(drypDryprcRecInner.drypEntity);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			drylogrec.params.set(sftlockrec.sftlockRec);
			drylogrec.statuz.set(sftlockrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void finish180()
	{
		chdrsurIO.setDataKey(SPACES);
		chdrsurIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrsurIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrsurIO.getParams());
			drylogrec.statuz.set(chdrsurIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		finish4000();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		/* Retrieve the contract header and store the transaction number*/
		chdrsurIO.setDataKey(SPACES);
		chdrsurIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrsurIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrsurIO.setFunction(varcom.readr);
		chdrsurIO.setFormat(chdrsurrec);
		SmartFileCode.execute(appVars, chdrsurIO);
		if (isNE(chdrsurIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrsurIO.getParams());
			drylogrec.statuz.set(chdrsurIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		compute(wsaaTranno, 0).set(add(1, chdrsurIO.getTranno()));
		/* Retrieve the Contract Non-Forfeiture Record.*/
		validCnfs.setTrue();
		cnfsIO.setParams(SPACES);
		cnfsIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		cnfsIO.setChdrnum(drypDryprcRecInner.drypEntity);
		cnfsIO.setFunction(varcom.readr);
		cnfsIO.setFormat(cnfsrec);
		SmartFileCode.execute(appVars, cnfsIO);
		if (isNE(cnfsIO.getStatuz(), varcom.oK)
		&& isNE(cnfsIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(cnfsIO.getStatuz());
			drylogrec.params.set(cnfsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(cnfsIO.getStatuz(), varcom.mrnf)) {
			invalidCnfs.setTrue();
			return ;
		}
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmTermid.set("9999");
		/*    Read Table T5679*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	}

protected void getFirstComponent1000()
	{
		covr1010();
	}

protected void covr1010()
	{
		/* Begin on the coverage/rider record*/
		covrclmIO.setDataArea(SPACES);
		covrclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		covrclmIO.setChdrnum(chdrsurIO.getChdrnum());
		covrclmIO.setFunction(covrclmrec);
		covrclmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(covrclmIO.getStatuz());
			drylogrec.params.set(covrclmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(covrclmIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
			invalidContract.setTrue();
			return ;
		}
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
		wsaaPlanSuffix.set(covrclmIO.getPlanSuffix());
	}

protected void processComponents1100()
	{
		read1110();
	}

protected void read1110()
	{
		/* Read table T5687 to find the surrender calculation subroutine*/
		wsaaCrtable.set(covrclmIO.getCrtable());
		obtainSurrenderCalc1200();
		srcalcpy.currcode.set(covrclmIO.getPremCurrency());
		validateStatusesWp1300();
		if (isEQ(wsaaValidStatus, "Y")
		&& isLT(covrclmIO.getRiskCessDate(), cnfsIO.getEffdate())) {
			/* Nothing to do. */
		}
		srcalcpy.endf.set(SPACES);
		srcalcpy.ptdate.set(chdrsurIO.getPtdate());
		srcalcpy.planSuffix.set(covrclmIO.getPlanSuffix());
		if (isNE(covrclmIO.getRider(), "00")
		&& isNE(covrclmIO.getRider(), "  ")) {
			srcalcpy.currcode.set(covrclmIO.getPremCurrency());
		}
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.estimatedVal.set(ZERO);
		srcalcpy.actualVal.set(ZERO);
		srcalcpy.effdate.set(cnfsIO.getEffdate());
		srcalcpy.chdrChdrcoy.set(covrclmIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrclmIO.getChdrnum());
		srcalcpy.lifeLife.set(covrclmIO.getLife());
		srcalcpy.lifeJlife.set(covrclmIO.getJlife());
		srcalcpy.covrCoverage.set(covrclmIO.getCoverage());
		srcalcpy.covrRider.set(covrclmIO.getRider());
		srcalcpy.crtable.set(covrclmIO.getCrtable());
		srcalcpy.crrcd.set(covrclmIO.getCrrcd());
		srcalcpy.convUnits.set(covrclmIO.getConvertInitialUnits());
		srcalcpy.pstatcode.set(covrclmIO.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.polsum.set(chdrsurIO.getPolsum());
		srcalcpy.language.set(drypDryprcRecInner.drypLanguage);
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(chdrsurIO.getBillfreq());
		}
		while ( !(isEQ(srcalcpy.status, varcom.endp))) {
			callSurMethodWhole1600();
		}
		
		while ( !((isNE(wsaaStoredLife, covrclmIO.getLife()))
		|| (isNE(wsaaStoredRider, covrclmIO.getRider()))
		|| (isNE(wsaaStoredCoverage, covrclmIO.getCoverage()))
		|| isEQ(covrclmIO.getStatuz(), varcom.endp))) {
			findNextComponent1700();
		}
		
		wsaaStoredLife.set(covrclmIO.getLife());
		wsaaStoredCoverage.set(covrclmIO.getCoverage());
		wsaaStoredRider.set(covrclmIO.getRider());
		wsaaStoredCurrency.set(covrclmIO.getPremCurrency());
	}

protected void obtainSurrenderCalc1200()
	{
		read1210();
	}

protected void read1210()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(covrclmIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), wsaaCrtable)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(f294);
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setFormat(itemrec);
		itemIO.setItemtabl(t6598);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

protected void validateStatusesWp1300()
	{
		/*START*/
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			riskStatusCheckWp1400();
		}
		if (isEQ(wsaaValidStatus, "Y")) {
			wsaaValidStatus = "N";
			for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
			|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
				premStatusCheckWp1500();
			}
		}
		/*EXIT*/
	}

protected void riskStatusCheckWp1400()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridRiskStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridRiskStat[wsaaIndex.toInt()], covrclmIO.getStatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void premStatusCheckWp1500()
	{
		/*START*/
		if (isEQ(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.covPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.covPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		if (isGT(covrclmIO.getRider(), ZERO)) {
			if (isNE(t5679rec.ridPremStat[wsaaIndex.toInt()], SPACES)) {
				if ((isEQ(t5679rec.ridPremStat[wsaaIndex.toInt()], covrclmIO.getPstatcode()))) {
					wsaaValidStatus = "Y";
					return ;
				}
			}
		}
		/*EXIT*/
	}

protected void callSurMethodWhole1600()
	{
		read1610();
	}

protected void read1610()
	{
		srcalcpy.effdate.set(cnfsIO.getEffdate());
		srcalcpy.type.set("F");
		srcalcpy.singp.set(covrclmIO.getInstprem());
		/* If no surrender method found print zeros as surrender value*/
		if (isEQ(t6598rec.calcprog, SPACES)
		|| isEQ(wsaaValidStatus, "N")) {
			srcalcpy.status.set(varcom.endp);
			return ;
		}
		
		/*IVE-796 RUL Product - Partial Surrender Calculation started*/
		//callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
				
				if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t6598rec.calcprog.toString()))) 
				{
					callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
				}
				else
				{
			 		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
					Vpxsurcrec vpxsurcrec = new Vpxsurcrec();
					Vpmfmtrec vpmfmtrec = new Vpmfmtrec();

					vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);		
					vpxsurcrec.function.set("INIT");
					callProgram(Vpxsurc.class, vpmcalcrec.vpmcalcRec,vpxsurcrec);	//IO read
					srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
					vpxsurcrec.totalEstSurrValue.set(wsaaEstimateTot);
					vpmfmtrec.initialize();
					vpmfmtrec.amount02.set(wsaaEstimateTot);			

					callProgram(t6598rec.calcprog, srcalcpy.surrenderRec, vpmfmtrec, vpxsurcrec,chdrsurIO);//VPMS call
					
					
					if(isEQ(srcalcpy.type,"L"))
					{
						vpmcalcrec.linkageArea.set(srcalcpy.surrenderRec);	
						callProgram(Vpusurc.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
						srcalcpy.surrenderRec.set(vpmcalcrec.linkageArea);
						srcalcpy.status.set(varcom.endp);
					}
					else if(isEQ(srcalcpy.type,"C"))
					{
						srcalcpy.status.set(varcom.endp);
					}
					else if(isEQ(srcalcpy.type,"A") && vpxsurcrec.statuz.equals(varcom.endp))
					{
						srcalcpy.status.set(varcom.endp);
					}
					else
					{
						srcalcpy.status.set(varcom.oK);
					}
				}
				
				/*IVE-796 RUL Product - Partial Surrender Calculation end*/
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			drylogrec.statuz.set(srcalcpy.status);
			drylogrec.params.set(srcalcpy.surrenderRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isNE(srcalcpy.status, varcom.oK)
		&& isNE(srcalcpy.status, varcom.endp)
		&& isNE(srcalcpy.status, "NOPR")) {
			drylogrec.statuz.set(srcalcpy.status);
			drylogrec.params.set(srcalcpy.surrenderRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(srcalcpy.status, "NOPR")) {
			/* Nothing to do. */
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding5000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(srcalcpy.estimatedVal);
		callRounding5000();
		srcalcpy.estimatedVal.set(zrdecplrec.amountOut);
		/* Check the amount returned for being negative. In the case of*/
		/* SUM products this is possible and so set these values to zero.*/
		/* Note SUM products do not have to have the same PT & BT dates.*/
		checkT56111800();
		if (isNE(wsaaSumFlag, SPACES)) {
			if (isLT(srcalcpy.actualVal, ZERO)) {
				srcalcpy.actualVal.set(ZERO);
			}
		}
		surdclmIO.setParams(SPACES);
		surdclmIO.setPlanSuffix(wsaaPlanSuffix);
		surdclmIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		surdclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surdclmIO.setLife(covrclmIO.getLife());
		surdclmIO.setCrtable(covrclmIO.getCrtable());
		surdclmIO.setJlife(covrclmIO.getJlife());
		surdclmIO.setCoverage(covrclmIO.getCoverage());
		surdclmIO.setRider(covrclmIO.getRider());
		surdclmIO.setVirtualFund(srcalcpy.fund);
		surdclmIO.setShortds(srcalcpy.description);
		surdclmIO.setTranno(wsaaTranno);
		surdclmIO.setCurrcd(chdrsurIO.getCntcurr());
		surdclmIO.setEstMatValue(srcalcpy.estimatedVal);
		surdclmIO.setActvalue(srcalcpy.actualVal);
		surdclmIO.setFieldType(srcalcpy.type);
		surdclmIO.setFunction(varcom.writr);
		surdclmIO.setFormat(surdclmrec);
		SmartFileCode.execute(appVars, surdclmIO);
		if (isNE(surdclmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(surdclmIO.getStatuz());
			drylogrec.params.set(surdclmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* For every SURDCLM written, write a HSUD*/
		hsudIO.setParams(SPACES);
		hsudIO.setChdrcoy(surdclmIO.getChdrcoy());
		hsudIO.setChdrnum(surdclmIO.getChdrnum());
		hsudIO.setLife(surdclmIO.getLife());
		hsudIO.setCoverage(surdclmIO.getCoverage());
		hsudIO.setRider(surdclmIO.getRider());
		hsudIO.setPlanSuffix(surdclmIO.getPlanSuffix());
		hsudIO.setCrtable(surdclmIO.getCrtable());
		hsudIO.setJlife(surdclmIO.getJlife());
		hsudIO.setTranno(surdclmIO.getTranno());
		hsudIO.setHactval(srcalcpy.actualVal);
		hsudIO.setHemv(srcalcpy.estimatedVal);
		hsudIO.setHcnstcur(chdrsurIO.getCntcurr());
		hsudIO.setFieldType(srcalcpy.type);
		hsudIO.setFormat(hsudrec);
		hsudIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hsudIO);
		if (isNE(hsudIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hsudIO.getStatuz());
			drylogrec.params.set(hsudIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (firstTime.isTrue()) {
			wsaaSwitch2.set(0);
			wsaaStoredCurrency.set(chdrsurIO.getCntcurr());
			if (isEQ(srcalcpy.type, "C")) {
				compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
				compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
			}
			else {
				compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
			}
		}
		else {
			if (isEQ(chdrsurIO.getCntcurr(), wsaaStoredCurrency)) {
				if (isEQ(srcalcpy.description, "PENALTY")) {
					compute(wsaaActualTot, 2).set(sub(wsaaActualTot, srcalcpy.actualVal));
					compute(wsaaPenaltyTot, 2).set(add(wsaaPenaltyTot, srcalcpy.actualVal));
				}
				else {
					compute(wsaaActualTot, 2).set(add(wsaaActualTot, srcalcpy.actualVal));
				}
			}
			else {
				wsaaActualTot.set(ZERO);
				wsaaCurrencySwitch.set(1);
			}
		}
		/* Check the effective date of the surrender against the paid to*/
		/* date of the contract. If there are to be premiums paid or*/
		/* refunded then call the appropriate subroutine held on T5611.*/
		/* Note that if the item does not exist then the coverage does not*/
		/* use the SUM calculation package & is hence not applicable....*/
		if (isNE(srcalcpy.effdate, srcalcpy.ptdate)
		&& isNE(wsaaSumFlag, SPACES)
		&& isNE(t5611rec.calcprog, SPACES)) {
			checkPremadj1900();
		}
	}

protected void findNextComponent1700()
	{
		/*READ*/
		/* Read the next coverage/rider record.*/
		covrclmIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrclmIO);
		if (isNE(covrclmIO.getStatuz(), varcom.oK)
		&& isNE(covrclmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrclmIO.getParams());
			drylogrec.params.set(covrclmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(covrclmIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(covrclmIO.getChdrnum(), chdrsurIO.getChdrnum())
		|| isEQ(covrclmIO.getStatuz(), varcom.endp)) {
			covrclmIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkT56111800()
	{
		start3810();
	}

protected void start3810()
	{
		/* Read the Coverage Surrender/Paid-Up/Bonus parameter table.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemtabl(t5611);
		itdmIO.setItemcoy(srcalcpy.chdrChdrcoy);
		wsaaT5611Crtable.set(srcalcpy.crtable);
		wsaaT5611Pstatcode.set(srcalcpy.pstatcode);
		itdmIO.setItemitem(wsaaT5611Item);
		itdmIO.setItmfrm(srcalcpy.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			if (isNE(itdmIO.getStatuz(), varcom.endp)
			&& isEQ(itdmIO.getItemtabl(), t5611)
			&& isEQ(itdmIO.getItemcoy(), srcalcpy.chdrChdrcoy)
			&& isEQ(itdmIO.getItemitem(), wsaaT5611Item)) {
				t5611rec.t5611Rec.set(itdmIO.getGenarea());
				wsaaSumFlag.set("Y");
			}
			else {
				wsaaSumFlag.set(SPACES);
			}
		}
	}

protected void checkPremadj1900()
	{
		start1910();
	}

protected void start1910()
	{
		/* Call the surrender routine for premium adjustments. Note that*/
		/* for the moment this uses the same copy-book as the surrender*/
		/* routine, which is stored and then re-instated .........*/
		wsaaSurrenderRecStore.set(srcalcpy.surrenderRec);
		/* If this is a Single Premium Component then use a Billing*/
		/* frequency of '00'.*/
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			srcalcpy.billfreq.set("00");
		}
		else {
			srcalcpy.billfreq.set(chdrsurIO.getBillfreq());
		}
		srcalcpy.estimatedVal.set(covrclmIO.getInstprem());
		srcalcpy.actualVal.set(ZERO);
		callProgram(t5611rec.calcprog, srcalcpy.surrenderRec);
		if (isEQ(srcalcpy.status, varcom.bomb)) {
			drylogrec.params.set(srcalcpy.surrenderRec);
			drylogrec.statuz.set(srcalcpy.status);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		else {
			if (isNE(srcalcpy.status, varcom.oK)
			&& isNE(srcalcpy.status, varcom.endp)) {
				drylogrec.params.set(srcalcpy.surrenderRec);
				drylogrec.statuz.set(srcalcpy.status);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
		}
		zrdecplrec.amountIn.set(srcalcpy.actualVal);
		callRounding5000();
		srcalcpy.actualVal.set(zrdecplrec.amountOut);
		/* Note adjustments are subtracted from the total.*/
		/* The actual value here is the premium adjustment returned...*/
		wsaaOtheradjst.add(srcalcpy.actualVal);
		srcalcpy.surrenderRec.set(wsaaSurrenderRecStore);
	}

protected void getTaxAmount2000()
	{
		tax2010();
	}

protected void tax2010()
	{
		wsaaTaxAmt.set(0);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tr691);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrsurIO.getCnttype());
		stringVariable1.addExpression(chdrsurIO.getCntcurr());
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(tr691);
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***");
			stringVariable2.addExpression(chdrsurIO.getCntcurr());
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				return ;
			}
		}
		tr691rec.tr691Rec.set(itemIO.getGenarea());
		datcon3rec.intDate2.set(cnfsIO.getEffdate());
		datcon3rec.intDate1.set(chdrsurIO.getOccdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isGTE(datcon3rec.freqFactor, tr691rec.tyearno)) {
			return ;
		}
		if (isNE(tr691rec.pcnt, 0)) {
			compute(wsaaTaxAmt, 3).setRounded(mult(wsaaActualTot, (div(tr691rec.pcnt, 100))));
		}
		zrdecplrec.amountIn.set(wsaaTaxAmt);
		callRounding5000();
		wsaaTaxAmt.set(zrdecplrec.amountOut);
		if (isNE(tr691rec.flatrate, 0)) {
			wsaaTaxAmt.set(tr691rec.flatrate);
		}
	}

protected void getLoanDetails2100()
	{
		start2100();
	}

	/**
	* <pre>
	*  Get the details of all loans currently held against this
	*  Contract. If this is not the first component within the
	*  current Surrender transaction then need to read the SURD
	*  to get details of all previous surrender records for this
	*  transaction.
	*  If there are no records (there will be none for Unit Linked),
	*  then call TOTLOAN to get the current loan value. If there are
	*  records then sum up their values before calling TOTLOAN and
	*  subtracting the total value from the LOAN VALUE returned from
	*  TOTLOAN.
	*  Previous surrenders may have been done in a different
	*  currency to the present one and hence, a check should be
	*  made for this and a conversion done where necessary before
	*  the accumulation is done. Note that for the initial screen
	*  display, the currency will always be CHDR currency.
	*  Note also that TOTLOAN always returns details in the CHDR
	*  currency.
	* </pre>
	*/
protected void start2100()
	{
		surdclmIO.setDataArea(SPACES);
		surdclmIO.setChdrcoy(chdrsurIO.getChdrcoy());
		surdclmIO.setChdrnum(chdrsurIO.getChdrnum());
		surdclmIO.setLife(ZERO);
		surdclmIO.setCoverage(ZERO);
		surdclmIO.setRider(ZERO);
		surdclmIO.setPlanSuffix(ZERO);
		surdclmIO.setTranno(chdrsurIO.getTranno());
		surdclmIO.setFunction(varcom.begn);
		surdclmIO.setFormat(surdclmrec);
		surdclmIO.setStatuz(varcom.oK);
		wsaaLoanValue.set(0);
		/*  Read all SURD records currently unprocessed.*/
		while ( !(isEQ(surdclmIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, surdclmIO);
			if (isNE(surdclmIO.getStatuz(), varcom.oK)
			&& isNE(surdclmIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(surdclmIO.getParams());
				a000FatalError();
			}
			if (isNE(chdrsurIO.getChdrnum(), surdclmIO.getChdrnum())
			|| isNE(chdrsurIO.getChdrcoy(), surdclmIO.getChdrcoy())
			|| isEQ(surdclmIO.getStatuz(), varcom.endp)) {
				surdclmIO.setStatuz(varcom.endp);
			}
			else {
				if (isNE(surdclmIO.getCurrcd(), chdrsurIO.getCntcurr())) {
					readjustSurd2300();
				}
				else {
					wsaaActvalue.set(surdclmIO.getActvalue());
				}
				wsaaLoanValue.add(wsaaActvalue);
			}
			surdclmIO.setFunction(varcom.nextr);
		}
		
		/*  Apply the adjustment for any previous surrendered policies*/
		/*  where this is part of a multipolicy surrender.*/
		if (!firstTimeThrough.isTrue()) {
			wsaaLoanValue.add(wsaaOtheradjst);
		}
		/*  Read TOTLOAN to get value of loans for this contract.*/
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(cnfsIO.getEffdate());
		totloanrec.function.set("LOAN");
		totloanrec.language.set(drypDryprcRecInner.drypLanguage);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			drylogrec.params.set(totloanrec.totloanRec);
			drylogrec.statuz.set(totloanrec.statuz);
			a000FatalError();
		}
		compute(wsaaHeldCurrLoans, 2).set(add(totloanrec.principal, totloanrec.interest));
		if (isLT(wsaaLoanValue, wsaaHeldCurrLoans)) {
			wsaaHeldCurrLoans.subtract(wsaaLoanValue);
		}
		else {
			wsaaHeldCurrLoans.set(0);
		}
		totloanrec.totloanRec.set(SPACES);
		totloanrec.chdrcoy.set(chdrsurIO.getChdrcoy());
		totloanrec.chdrnum.set(chdrsurIO.getChdrnum());
		totloanrec.principal.set(ZERO);
		totloanrec.interest.set(ZERO);
		totloanrec.loanCount.set(ZERO);
		totloanrec.effectiveDate.set(cnfsIO.getEffdate());
		totloanrec.function.set("CASH");
		totloanrec.language.set(drypDryprcRecInner.drypLanguage);
		callProgram(Hrtotlon.class, totloanrec.totloanRec);
		if (isNE(totloanrec.statuz, varcom.oK)) {
			drylogrec.params.set(totloanrec.totloanRec);
			drylogrec.statuz.set(totloanrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		compute(wsaaAtmodRecInner.wsaaZrcshamt, 2).set(add(totloanrec.principal, totloanrec.interest));
		/*    Change the sign to reflect the Client's context.*/
		if (isLT(wsaaAtmodRecInner.wsaaZrcshamt, 0)) {
			compute(wsaaAtmodRecInner.wsaaZrcshamt, 0).set(mult(wsaaAtmodRecInner.wsaaZrcshamt, (-1)));
		}
	}

protected void getPolicyDebt2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2210();
				case read2220: 
					read2220();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2210()
	{
		wsaaAtmodRecInner.wsaaTdbtamt.set(ZERO);
		tpoldbtIO.setRecKeyData(SPACES);
		tpoldbtIO.setChdrcoy(chdrsurIO.getChdrcoy());
		tpoldbtIO.setChdrnum(chdrsurIO.getChdrnum());
		tpoldbtIO.setTranno(ZERO);
		tpoldbtIO.setFormat(tpoldbtrec);
		tpoldbtIO.setFunction(varcom.begn);
	}

protected void read2220()
	{
		SmartFileCode.execute(appVars, tpoldbtIO);
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		&& isNE(tpoldbtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(tpoldbtIO.getParams());
			drylogrec.statuz.set(tpoldbtIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isNE(tpoldbtIO.getStatuz(), varcom.oK)
		|| isNE(tpoldbtIO.getChdrcoy(), chdrsurIO.getChdrcoy())
		|| isNE(tpoldbtIO.getChdrnum(), chdrsurIO.getChdrnum())) {
			return ;
		}
		wsaaAtmodRecInner.wsaaTdbtamt.add(tpoldbtIO.getTdbtamt());
		tpoldbtIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read2220);
	}

protected void readjustSurd2300()
	{
		start2300();
	}

protected void start2300()
	{
		/*  Convert the SURD into the CHDR currency.*/
		conlinkrec.function.set("SURR");
		conlinkrec.statuz.set(SPACES);
		conlinkrec.currIn.set(surdclmIO.getCurrcd());
		conlinkrec.cashdate.set(varcom.vrcmMaxDate);
		conlinkrec.currOut.set(chdrsurIO.getCntcurr());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.amountIn.set(surdclmIO.getActvalue());
		conlinkrec.company.set(chdrsurIO.getChdrcoy());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			drylogrec.params.set(conlinkrec.clnk002Rec);
			drylogrec.statuz.set(conlinkrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		callRounding5000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaActvalue.set(conlinkrec.amountOut);
	}

protected void finish4000()
	{
		start4010();
	}

protected void start4010()
	{
		/* Update the DRYP fields to determine next processing date.*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.drypStatcode.set(chdrsurIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrsurIO.getPstatcode());
		drypDryprcRecInner.drypTranno.set(chdrsurIO.getTranno());
		drypDryprcRecInner.drypBtdate.set(chdrsurIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrsurIO.getPtdate());
		drypDryprcRecInner.drypCnttype.set(chdrsurIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrsurIO.getOccdate());
		drypDryprcRecInner.drypBillcd.set(chdrsurIO.getBillcd());
		drypDryprcRecInner.drypBillchnl.set(chdrsurIO.getBillchnl());
		drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrsurIO.getCntcurr());
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			a000FatalError();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-ATMOD-REC--INNER
 */
private static final class WsaaAtmodRecInner { 

	private FixedLengthStringData wsaaAtmodRec = new FixedLengthStringData(310);
	private FixedLengthStringData wsaaAtdmStatuz = new FixedLengthStringData(4).isAPartOf(wsaaAtmodRec, 0);
	private FixedLengthStringData wsaaAtdmBatchKey = new FixedLengthStringData(22).isAPartOf(wsaaAtmodRec, 4);
	private FixedLengthStringData wsaaBatcBatcpfx = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 0);
	private FixedLengthStringData wsaaBatcBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaAtdmBatchKey, 2);
	private FixedLengthStringData wsaaBatcBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 3);
	private ZonedDecimalData wsaaBatcBatcactyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaAtdmBatchKey, 5);
	private ZonedDecimalData wsaaBatcBatcactmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaAtdmBatchKey, 9);
	private FixedLengthStringData wsaaBatcBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaAtdmBatchKey, 11);
	private FixedLengthStringData wsaaBatcBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaAtdmBatchKey, 15);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaAtdmBatchKey, 20);
	private FixedLengthStringData wsaaAtdmLanguage = new FixedLengthStringData(1).isAPartOf(wsaaAtmodRec, 26);
	private FixedLengthStringData wsaaAtdmCompany = new FixedLengthStringData(1).isAPartOf(wsaaAtmodRec, 27);
	private FixedLengthStringData wsaaAtdmPrimaryKey = new FixedLengthStringData(36).isAPartOf(wsaaAtmodRec, 28);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaAtdmPrimaryKey, 0);
	private FixedLengthStringData wsaaAtdmPrintQueue = new FixedLengthStringData(10).isAPartOf(wsaaAtmodRec, 64);
	private FixedLengthStringData wsaaAtdmTransArea = new FixedLengthStringData(200).isAPartOf(wsaaAtmodRec, 74);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 4);
	private PackedDecimalData wsaaTransUser = new PackedDecimalData(6, 0).isAPartOf(wsaaAtdmTransArea, 8);
	private FixedLengthStringData wsaaTransTermid = new FixedLengthStringData(4).isAPartOf(wsaaAtdmTransArea, 12);
	private FixedLengthStringData filler1 = new FixedLengthStringData(184).isAPartOf(wsaaAtdmTransArea, 16, FILLER).init(SPACES);
	private FixedLengthStringData wsaaAtdmReqterm = new FixedLengthStringData(4).isAPartOf(wsaaAtmodRec, 274);
	private ZonedDecimalData wsaaTdbtamt = new ZonedDecimalData(18, 2).isAPartOf(wsaaAtmodRec, 278);
	private ZonedDecimalData wsaaZrcshamt = new ZonedDecimalData(14, 2).isAPartOf(wsaaAtmodRec, 296);
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
}
}
