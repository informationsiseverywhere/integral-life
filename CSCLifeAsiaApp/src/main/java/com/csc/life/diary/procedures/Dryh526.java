/*
 * File: Dryh526.java
 * Date: January 15, 2015 4:15:11 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYH526.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivdopTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.recordstructures.Hdvdoprec;
import com.csc.life.cashdividends.tablestructures.Th500rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.CovrbonTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrxTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(c) Copyright Continuum Corporation Ltd.  1986....1995.
 *    All rights reserved.  Continuum Confidential.
 *
 *REMARKS.
 *
 *   This program is responsible for performing the processing
 *   for the dividend option chosen by the policyholder after
 *   each dividend allocation. The actual processing is done in a
 *   subroutine held on TH500 and there is a different subroutine
 *   for each dividend option.
 *
 *
 *   PROCESSING
 *   ==========
 *
 *  -
 *
 *   Control totals:
 *     01 - No. HDIV recs read
 *     02 - No. HDIV recs processed
 *     03 - Total value of divd proc
 *
 *   Error Processing:
 *     If a system error move the error code into the DRYL-STATUZ
 *     If a database error move the XXXX-PARAMS to DRYL-PARAMS.
 *     Perform the A000-FATAL-ERROR section.
 *
 ****************************************************************** ****
 *                                                                     *
 * </pre>
 */
public class Dryh526 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH526");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator eof = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaTotDivd = new PackedDecimalData(11, 2).init(0);
	/* WSAA-HDIV-STORE */
	private PackedDecimalData wsaaHdivdopTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaHdivdopEffdate = new PackedDecimalData(8, 0).init(0);
	/* WSAA-HDPX */
	private FixedLengthStringData wsaaHdpxLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaHdpxJlife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaHdpxCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaHdpxRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaHdpxPlnsfx = new PackedDecimalData(4, 0);
	/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String covrrec = "COVRREC";
	private static final String covrbonrec = "COVRBONREC";
	private static final String hdivdoprec = "HDIVDOPREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String usrxrec = "USRXREC";
	/* TABLES */
	private static final String t5679 = "T5679";
	private static final String th500 = "TH500";
	/* ERRORS */
	private static final String z038 = "Z038";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrbonTableDAM covrbonIO = new CovrbonTableDAM();
	private HdivdopTableDAM hdivdopIO = new HdivdopTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UsrxTableDAM usrxIO = new UsrxTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private T5679rec t5679rec = new T5679rec();
	private Th500rec th500rec = new Th500rec();
	private Hdvdoprec hdvdoprec = new Hdvdoprec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
	private List<Hdivpf> hdivpfList;
	private Hdivpf hdivpfIO;
	private int hdivpfCount = 0;

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr280, 
		exit1290, 
		updateChdr3030, 
		exit3090
	}

	public Dryh526() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}



	protected void startProcessing100()
	{
		start110();
	}

	protected void start110()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		validateContract200();
		if (!validContract.isTrue()) {
			return ;
		}
		/*	hdivdopIO.setParams(SPACES);
		hdivdopIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hdivdopIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hdivdopIO.setPlanSuffix(ZERO);
		hdivdopIO.setFormat(hdivdoprec);
		hdivdopIO.setFunction(varcom.begn);
		while ( !(isEQ(hdivdopIO.getStatuz(), varcom.endp))) {
			readHdiv200();
		}*/
		Hdivpf hdivpf = new Hdivpf(); 
		hdivpf.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		hdivpf.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		hdivpfList = hdivpfDAO.searchHdivdopRecord(hdivpf);
		if(isEQ(hdivpfList.size(),ZERO))
		{
			wsaaEof.set("Y");
		}
		while ( !(isEQ(wsaaEof, "Y"))) {
			if(hdivpfList.size()>hdivpfCount)
			{
				hdivpfIO=hdivpfList.get(hdivpfCount);
				readHdiv200();
			}
		}

		if (!validCoverage.isTrue()) {
			return ;
		}
		process3000();
		finish1000();
	}

	protected void validateContract200()
	{
		start210();
	}

	protected void start210()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Read T5679 for valid status requirements for transactions.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*  Validate the contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
						|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrlifIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

	protected void readHdiv200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call210();
				case nextr280: 
					nextr280();
				case exit1290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void call210()
	{
		/*SmartFileCode.execute(appVars, hdivdopIO);
		if (isNE(hdivdopIO.getStatuz(), varcom.oK)
		&& isNE(hdivdopIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hdivdopIO.getStatuz());
			drylogrec.params.set(hdivdopIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(hdivdopIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(hdivdopIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(hdivdopIO.getStatuz(), varcom.endp)) {
			hdivdopIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1290);
		}*/
		if(hdivpfList.size() <= hdivpfCount)
		{
			wsaaEof.set("Y");
			goTo(GotoLabel.exit1290);
		}
		validateCoverage400();
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.nextr280);
		}
	}

	protected void nextr280()
	{
		//hdivdopIO.setFunction(varcom.nextr);
		hdivpfCount++;
		if(hdivpfList.size() <= hdivpfCount)
		{
			wsaaEof.set("Y");
			goTo(GotoLabel.exit1290);
		}
	}

	protected void validateCoverage400()
	{
		start410();
	}

	protected void start410()
	{
		/*  Read the coverage record and validate the status against*/
		/*  those on T5679.*/
		covrbonIO.setParams(SPACES);
		covrbonIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrbonIO.setChdrnum(hdivpfIO.getChdrnum());
		covrbonIO.setLife(hdivpfIO.getLife());
		covrbonIO.setCoverage(hdivpfIO.getCoverage());
		covrbonIO.setRider(hdivpfIO.getRider());
		covrbonIO.setPlanSuffix(hdivpfIO.getPlnsfx());
		covrbonIO.setFormat(covrbonrec);
		covrbonIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrbonIO);
		if (isNE(covrbonIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrbonIO.getParams());
			drylogrec.params.set(covrbonIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Validate the coverage status against T5679.*/
		wsaaValidCoverage.set("N");
		wsaaStatcode.set(covrbonIO.getStatcode());
		wsaaPstcde.set(covrbonIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateCovrStatus500();
		}
	}

	protected void validateCovrStatus500()
	{
		/*START*/
		/*  Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateCovrPremStatus600();
			}
		}
		/*EXIT*/
	}

	protected void validateCovrPremStatus600()
	{
		/*START*/
		/*  Validate coverage premium status*/
		if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
			wsaaSub.set(13);
			validCoverage.setTrue();
		}
		/*EXIT*/
	}

	protected void process3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case updateChdr3030: 
					updateChdr3030();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void update3010()
	{
		/* Replacement of SQL by COBOL*/
		/*		hdivdopIO.setParams(SPACES);
		hdivdopIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hdivdopIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hdivdopIO.setPlanSuffix(ZERO);
		hdivdopIO.setFunction(varcom.begn);
		procHdivdop5130();
		if (isNE(hdivdopIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(hdivdopIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(hdivdopIO.getStatuz(), varcom.endp)) {
			hdivdopIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3090);
		}*/
		wsaaEof.set("N");
		hdivpfCount = 0;
		hdivpfIO=hdivpfList.get(hdivpfCount);
		if(isNE(hdivpfIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(hdivpfIO.getChdrnum(), drypDryprcRecInner.drypEntity)
				|| (hdivpfList.size() <= hdivpfCount))
		{
			wsaaEof.set("Y");
			goTo(GotoLabel.exit3090);
		}
		wsaaHdpxLife.set(hdivpfIO.getLife());
		wsaaHdpxJlife.set(hdivpfIO.getJlife());
		wsaaHdpxCoverage.set(hdivpfIO.getCoverage());
		wsaaHdpxRider.set(hdivpfIO.getRider());
		wsaaHdpxPlnsfx.set(hdivpfIO.getPlnsfx());
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readh);
		procChdrlif5100();
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		covrIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covrIO.setLife(hdivpfIO.getLife());
		covrIO.setCoverage(hdivpfIO.getCoverage());
		covrIO.setRider(hdivpfIO.getRider());
		covrIO.setPlanSuffix(hdivpfIO.getPlnsfx());
		covrIO.setFunction(varcom.readr);
		procCovr5110();
		/*    Match for the TH500 item with the Cash Dividend option held*/
		/*    on HDPXPF*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(th500);
		itemIO.setItemitem(hdivpfIO.getZdivopt());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
				&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(z038);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th500rec.th500Rec.set(itemIO.getGenarea());
		/*    Skip option processing if no processing subroutine defined.*/
		if (isEQ(th500rec.subprog, SPACES)) {
			goTo(GotoLabel.updateChdr3030);
		}
		/*    Read all HDIVOPTs of same key up to PLNSFX to accumulate the*/
		/*    dividend amount*/
		wsaaTotDivd.set(0);

		/*while ( !(isEQ(hdivdopIO.getStatuz(), varcom.endp))) {
		wsaaTotDivd.add(hdivdopIO.getDivdAmount());
			    Put aside tranno and effective date
			wsaaHdivdopEffdate.set(hdivdopIO.getEffdate());
			wsaaHdivdopTranno.set(hdivdopIO.getTranno());
			drycntrec.contotNumber.set(ct01);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			hdivdopIO.setFunction(varcom.nextr);
			procHdivdop5130();
			if (isNE(hdivdopIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
			|| isNE(hdivdopIO.getChdrnum(), drypDryprcRecInner.drypEntity)
			|| isNE(hdivdopIO.getLife(), wsaaHdpxLife)
			|| isNE(hdivdopIO.getCoverage(), wsaaHdpxCoverage)
			|| isNE(hdivdopIO.getRider(), wsaaHdpxRider)
			|| isNE(hdivdopIO.getPlanSuffix(), wsaaHdpxPlnsfx)) {
				hdivdopIO.setStatuz(varcom.endp);
			}
	}*/
		while ( !(isEQ(wsaaEof, "Y"))) {
			if(hdivpfList.size()>hdivpfCount)
			{
				hdivpfIO=hdivpfList.get(hdivpfCount);
				wsaaTotDivd.add(PackedDecimalData.parseObject(hdivpfIO.getHdvamt()));
				wsaaHdivdopEffdate.set(hdivpfIO.getEffdate());
				wsaaHdivdopTranno.set(hdivdopIO.getTranno());
				drycntrec.contotNumber.set(ct01);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				hdivpfCount++;
				if (isNE(hdivpfIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
						|| isNE(hdivpfIO.getChdrnum(), drypDryprcRecInner.drypEntity)
						|| isNE(hdivpfIO.getLife(), wsaaHdpxLife)
						|| isNE(hdivpfIO.getCoverage(), wsaaHdpxCoverage)
						|| isNE(hdivpfIO.getRider(), wsaaHdpxRider)
						|| isNE(hdivpfIO.getPlnsfx(), wsaaHdpxPlnsfx)
						||(hdivpfList.size() <= hdivpfCount))
				{
					wsaaEof.set("Y");
				}
			}
		}

		/*    Prepare to call cash dividend option processing subroutine*/
		initialize(hdvdoprec.dividendRec);
		hdvdoprec.chdrChdrcoy.set(drypDryprcRecInner.drypCompany);
		hdvdoprec.chdrChdrnum.set(drypDryprcRecInner.drypEntity);
		hdvdoprec.lifeLife.set(wsaaHdpxLife);
		hdvdoprec.lifeJlife.set(wsaaHdpxJlife);
		hdvdoprec.covrCoverage.set(wsaaHdpxCoverage);
		hdvdoprec.covrRider.set(wsaaHdpxRider);
		hdvdoprec.plnsfx.set(wsaaHdpxPlnsfx);
		hdvdoprec.cntcurr.set(chdrlifIO.getCntcurr());
		hdvdoprec.cnttype.set(chdrlifIO.getCnttype());
		hdvdoprec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		hdvdoprec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		hdvdoprec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		hdvdoprec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		hdvdoprec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		hdvdoprec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		hdvdoprec.hdivTranno.set(wsaaHdivdopTranno);
		hdvdoprec.crtable.set(covrIO.getCrtable());
		hdvdoprec.newTranno.set(wsaaNewTranno);
		hdvdoprec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		hdvdoprec.hdivEffdate.set(wsaaHdivdopEffdate);
		hdvdoprec.divdOption.set(hdivpfIO.getZdivopt());
		hdvdoprec.divdAmount.set(wsaaTotDivd);
		hdvdoprec.language.set(drypDryprcRecInner.drypLanguage);
		/* To get User Name by user number*/
		usrxIO.setParams(SPACES);
		usrxIO.setUsernum(drypDryprcRecInner.drypUser);
		usrxIO.setFunction(varcom.readr);
		usrxIO.setFormat(usrxrec);
		SmartFileCode.execute(appVars, usrxIO);
		if (isEQ(usrxIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(usrxIO.getStatuz());
			drylogrec.params.set(usrxIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		hdvdoprec.userName.set(usrxIO.getUserid());
		/*    Call Dividend Option Processing Subroutine*/
		callProgram(th500rec.subprog, hdvdoprec.dividendRec);
		if (isNE(hdvdoprec.statuz, varcom.oK)) {
			drylogrec.params.set(hdvdoprec.dividendRec);
			drylogrec.statuz.set(hdvdoprec.statuz);
			a000FatalError();
		}
		/*    Update the processed HDIVDOP with new TRANNO as option*/
		/*    processed TRANNO*/
		/*	hdivdopIO.setStatuz(varcom.oK);
		while ( !(isEQ(hdivdopIO.getStatuz(), varcom.mrnf))) {
			hdivdopIO.setChdrcoy(drypDryprcRecInner.drypCompany);
			hdivdopIO.setChdrnum(drypDryprcRecInner.drypEntity);
			hdivdopIO.setLife(wsaaHdpxLife);
			hdivdopIO.setCoverage(wsaaHdpxCoverage);
			hdivdopIO.setRider(wsaaHdpxRider);
			hdivdopIO.setPlanSuffix(wsaaHdpxPlnsfx);
			hdivdopIO.setFunction(varcom.readh);
			procHdivdop5130();
			if (isNE(hdivdopIO.getStatuz(), varcom.mrnf)) {
				hdivdopIO.setDivdOptprocTranno(wsaaNewTranno);
				hdivdopIO.setFunction(varcom.rewrt);
				procHdivdop5130();
			}
		}    */
		hdivpfCount = 0;
		while (hdivpfList.size()>hdivpfCount)
		{
			if (isEQ(hdivpfIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
					&& isEQ(hdivpfIO.getChdrnum(), drypDryprcRecInner.drypEntity)
					&& isEQ(hdivpfIO.getLife(), wsaaHdpxLife)
					&& isEQ(hdivpfIO.getCoverage(), wsaaHdpxCoverage)
					&& isEQ(hdivpfIO.getRider(), wsaaHdpxRider)
					&& isEQ(hdivpfIO.getPlnsfx(), wsaaHdpxPlnsfx))
			{
				hdivpfIO.setHdvopttx(wsaaNewTranno.toInt());  	    		
				hdivpfDAO.updateHdivdopRecord(hdivpfIO)  ;
				hdivpfCount++;
			}
		}

	}

	protected void updateChdr3030()
	{
		/*    Update existing CHDR as history and write a new one.*/
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setFunction(varcom.rewrt);
		procChdrlif5100();
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.writr);
		procChdrlif5100();
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setValidflag("1");
		ptrnIO.setPtrneff(drypDryprcRecInner.drypEffectiveDate);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionTime(varcom.vrcmTimen);
		ptrnIO.setUser(999999);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*    Count no.of records processed in CT02 and total*/
		/*    dividend amount in CT03*/
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(wsaaTotDivd);
		d000ControlTotals();
	}

	protected void procChdrlif5100()
	{
		/*CHDRLIF*/
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void procCovr5110()
	{
		/*COVR*/
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void procHdivdop5130()
	{
		/*HDIVDOP*/
		hdivdopIO.setFormat(hdivdoprec);
		SmartFileCode.execute(appVars, hdivdopIO);
		if (isNE(hdivdopIO.getStatuz(), varcom.oK)
				&& isNE(hdivdopIO.getStatuz(), varcom.mrnf)
				&& isNE(hdivdopIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hdivdopIO.getStatuz());
			drylogrec.params.set(hdivdopIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void finish1000()
	{
		finishPara1000();
	}

	protected void finishPara1000()
	{
		/* Update the DRYP fields to determine next processing date.*/
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
	}
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner { 

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}
}
