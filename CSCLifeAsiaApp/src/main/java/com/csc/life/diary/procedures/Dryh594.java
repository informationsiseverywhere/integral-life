/*
 * File: Dryh594.java
 * Date: March 26, 2014 3:05:05 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYH594.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.regularprocessing.dataaccess.HbxtpntTableDAM;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   DIARY/400:  Premium Notice Creation (Renewals)
*   ----------------------------------------------
*
*   This is the Premium Notice Processing Subroutine written for
*   the DIARY/400.
*
*   This subroutine replicates the processing carried out by the
*   Batch Program - BH594.
*
*   100 - This is the main processing section called from MAIND.
*
*         For each invocation of this subroutine:
*
*             Perform 200-INITIALISE
*             Perform 300-PROCESS-BEXT
*             Perform 1000-FINISH
*
*         Exit Subroutine
*
*   200 - Initialises Working Storage Variables and does
*         Component level statii checks.
*
*   300-  Process BEXT section.
*
*         Perform 400-VALIDATION
*
*         If validation is passed
*         Perform 500-PROCESS-NOTICE
*         Perform 600-REWRITE-HBXTPNT
*
*   400-  Read T5729 using Product Type
*         If we get here then validation passed.
*
*   500-  Fill all ANNP-ANNPLL-REC Linkage fields
*         Call the Generic Anniversary Processing Subroutine
*         Add one year to the HBXTPNT Premium Notice date use DATC N2
*
*   600-  Rewrite the Contract Header with the new date
*
*  1000-  No processing currently in here.
*
*  Control Totals
*  --------------
*
*  1 - Total number of BEXTs read
*  2 - Total number of BEXTs updated
*  3 - Total number of Payments outstanding
*  4 - Total number of Fund Transactions Outstanding
*  5 - Number of Premium Notices Produced
*  6 - Number of Invalid Records
*  7 - Number of Premium Notices not ready for processing
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryh594 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH594");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaStmdte = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaOutstandingRecs = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaNogo = new FixedLengthStringData(1).init(SPACES);
	private Validator gonofurther = new Validator(wsaaNogo, "Y");
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String hbxtpntrec = "HBXTPNTREC";
	private static final String payrrec = "PAYRREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* ERRORS */
	private static final String f379 = "F379";
	private static final String g437 = "G437";
	private static final String t5729 = "T5729";
	private static final String tr384 = "TR384";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData wsaaValidBext = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidBext, "Y");
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private HbxtpntTableDAM hbxtpntIO = new HbxtpntTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Tr384rec tr384rec = new Tr384rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dryh594() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		initialise200();
		readChdr700();
		processBext300();
		if (gonofurther.isTrue()) {
			return ;
		}
		finish1000();
		/*EXIT*/
	}

protected void initialise200()
	{
		/*INITIALISE*/
		/* Initialise any Working Storage variables.*/
		wsaaNogo.set(SPACES);
		/*EXIT*/
	}

protected void processBext300()
	{
		process310();
	}

protected void process310()
	{
		hbxtpntIO.setDataArea(SPACES);
		hbxtpntIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hbxtpntIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hbxtpntIO.setInstfrom(drypDryprcRecInner.drypEffectiveDate);
		hbxtpntIO.setFunction(varcom.begn);
		hbxtpntIO.setFormat(hbxtpntrec);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)
		&& isNE(hbxtpntIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(hbxtpntIO.getStatuz(), varcom.oK)
		&& isEQ(hbxtpntIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		&& isEQ(hbxtpntIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			/*    AND HBXTPNT-BILFLAG      NOT = 'Y'                           */
			/*NEXT_SENTENCE*/
		}
		else {
			gonofurther.setTrue();
			return ;
		}
		if (isNE(hbxtpntIO.getBillchnl(), drypDryprcRecInner.drypSystParm[1])) {
			gonofurther.setTrue();
			return ;
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		validation400();
		if (gonofurther.isTrue()) {
			return ;
		}
		processNotice500();
		rewriteHbxtpnt600();
	}

protected void validation400()
	{
		validation410();
	}

protected void validation410()
	{
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(drypDryprcRecInner.drypCnttype);
		itdmIO.setItmfrm(hbxtpntIO.getInstfrom());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			return ;
		}
		if (isEQ(itdmIO.getItemitem(), hbxtpntIO.getCnttype())) {
			hbxtpntIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, hbxtpntIO);
			if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
				drylogrec.statuz.set(hbxtpntIO.getStatuz());
				drylogrec.params.set(hbxtpntIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			gonofurther.setTrue();
			return ;
		}
	}

protected void processNotice500()
	{
		start500();
	}

protected void start500()
	{
		/* If we get to here then all validation has been passed.*/
		/* Process the Premium Notice and update the HBXTPNT-BILFLAG*/
		/* to 'Y' as the notice has been printed*/
		/* Read and hold the record in the Buffer before re-writing it.*/
		hbxtpntIO.setFunction(varcom.readr);
		hbxtpntIO.setFormat(hbxtpntrec);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hbxtpntIO.getStatuz());
			drylogrec.params.set(hbxtpntIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(hbxtpntIO.getChdrcoy());
		payrIO.setChdrnum(hbxtpntIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(payrIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(f379);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(hbxtpntIO.getChdrcoy());
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(hbxtpntIO.getCnttype());
		stringVariable1.addExpression(drypDryprcRecInner.drypBatctrcde);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(g437);
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		letrqstrec.statuz.set(SPACES);
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(hbxtpntIO.getInstfrom());
		letrqstrec.clntcoy.set(hbxtpntIO.getCowncoy());
		letrqstrec.clntnum.set(hbxtpntIO.getCownnum());
		letrqstrec.rdocpfx.set(hbxtpntIO.getChdrpfx());
		letrqstrec.requestCompany.set(hbxtpntIO.getChdrcoy());
		letrqstrec.chdrcoy.set(hbxtpntIO.getChdrcoy());
		letrqstrec.rdoccoy.set(hbxtpntIO.getChdrcoy());
		letrqstrec.rdocnum.set(hbxtpntIO.getChdrnum());
		letrqstrec.chdrnum.set(hbxtpntIO.getChdrnum());
		letrqstrec.branch.set(hbxtpntIO.getCntbranch());
		letrqstrec.tranno.set(payrIO.getTranno());
		letcokcpy.ldDate.set(hbxtpntIO.getInstfrom());
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.params.set(letrqstrec.params);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void rewriteHbxtpnt600()
	{
		rewrite610();
	}

protected void rewrite610()
	{
		/* Rewrite HBXTPNT record.*/
		hbxtpntIO.setBilflag("Y");
		/*   Retrofiting for <V65L18> - to delete the BEXT*/
		hbxtpntIO.setFunction(varcom.writd);
		hbxtpntIO.setFormat(hbxtpntrec);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hbxtpntIO.getStatuz());
			drylogrec.params.set(hbxtpntIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void readChdr700()
	{
		/*READ*/
		/* Read contract header record using CHDRLIF.*/
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void finish1000()
	{
		finishPara1000();
	}

protected void finishPara1000()
	{
		/* Move in the DRYP- fields required*/
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
}
}
