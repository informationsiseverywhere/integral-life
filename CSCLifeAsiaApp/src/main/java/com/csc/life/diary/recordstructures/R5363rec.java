package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:51
 * Description:
 * Copybook name: R5363REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5363rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(494);
  	public ZonedDecimalData prmper = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 0);
  	public ZonedDecimalData billedp = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 16);
  	public ZonedDecimalData prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 33);
  	public ZonedDecimalData ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 50);
  	public ZonedDecimalData minovrpro = new ZonedDecimalData(3, 0).isAPartOf(dataArea, 67);
  	public FixedLengthStringData filler = new FixedLengthStringData(424).isAPartOf(dataArea, 70, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(526);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 494);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(sortKey, 502);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(sortKey, 504);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(sortKey, 506);
  	public ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(sortKey, 508);
  	public PackedDecimalData targto = new PackedDecimalData(8, 0).isAPartOf(sortKey, 512).setUnsigned();
  	public FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sortKey, 517, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5363     ");


	public void initialize() {
		COBOLFunctions.initialize(prmper);
		COBOLFunctions.initialize(billedp);
		COBOLFunctions.initialize(prmrcdp);
		COBOLFunctions.initialize(ovrminreq);
		COBOLFunctions.initialize(minovrpro);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(plnsfx);
		COBOLFunctions.initialize(targto);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		prmper.isAPartOf(baseString, true);
    		billedp.isAPartOf(baseString, true);
    		prmrcdp.isAPartOf(baseString, true);
    		ovrminreq.isAPartOf(baseString, true);
    		minovrpro.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		life.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		rider.isAPartOf(baseString, true);
    		plnsfx.isAPartOf(baseString, true);
    		targto.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}