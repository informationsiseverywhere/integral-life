/*
 * File: Dry6528.java
 * Date: March 26, 2014 3:03:45 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY6528.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.diary.dataaccess.dao.Dry6528DAO;
import com.csc.life.diary.dataaccess.model.Dry6528Dto;
import com.csc.life.diary.pojo.BenefitBilling;
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.VprcpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.dataaccess.model.Vprcpf;
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.recordstructures.MgmFeeRec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Benefit Billing Processing
* ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
*
* This program makes up part of the Diary System.
* Its function is to simulate the B6528 Batch process
* that is part of the Renewal Batch Schedule.
*
* NOTE that the splitter program B6527 is no longer required.
* Processing from both B6527 and B6528 have been amalgamated
* into this subroutine for the diary system.
*
* 100- this is the main processing section.
*
* 200- this section initialises working storage and
*      reads and validates the contract header details
*      for the entity number passed in via linkage.
*
* 300- this section contains the loop around the COVRDRY
*      records. The loop is ended when a change of key
*      occurs, the benefit billing date is set in the
*      future or an ENDP statuz has been reached.
*
*      Each COVRDRY record is validated against T5679
*      to ensure that the risk status is correct. Any
*      invalid records are ignored and a control total
*      updated.
*
*      Next, the program reads T5687 for the CRTABLE
*      and retrieves the benefit billing method. This
*      method is used to read T5534 to retrieve the
*      benefit billing subroutine to be performed.
*
*      Any COVRDRY records which result in a benefit
*      billing subroutine being spaces are ignored
*      and a control total updated.
*
*      On a successful completion of the T5534 subroutine,
*      the coverage is updated, incrementing the benefit
*      billing date by the frequency stored on T5534. A
*      control total is updated.
*
*      The function is set for reading the next COVRDRY
*      record.
*
* The next two sections are performed from the 100- section
* once all the COVRDRY processing is completed.
*
* 900- this section updates the contract header and writes
*      a PTRN record for the transaction.
*
* 1000- this section sets up the DRYP fields in the DRYPRCLNK
*       copybook required for the trigger procesing subroutine
*       DRYBENBIL. This will set the new trigger date to
*       the benefit billing date calculated in this program.
*
* Control Totals:
* ¯¯¯¯¯¯¯¯¯¯¯¯¯¯¯
*
* CT01  Coverage Read
* CT02  CHDRs updated
* CT03  COVRs updated
* CT04  Invalid coverages
* CT05  Future Benefit Billing date
* CT06  Benefit billing subroutine = spaces
*
****************************************************************** ****
*                                                                     *
 * </pre>
 */
public class Dry6528 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	/* NEW-ERRORS */
	private static final String E044 = "E044";
	private static final String H021 = "H021";
	private static final String E308 = "E308";
	/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT03 = 3;
	private static final int CT04 = 4;
	private static final int CT05 = 5;

	private static final String MGMFEE_ID = "CSULK003";
	private static final String T5679 = "T5679";

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY6528");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private ExternalisedRules er = new ExternalisedRules();

	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	/* COVX parameters */

	private FixedLengthStringData lastChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1);
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private FixedLengthStringData wsaaJustCommited = new FixedLengthStringData(1).init("N");
	private PackedDecimalData ix = new PackedDecimalData(3, 0);

	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTrandate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaMinfundWrnlt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMinfundInsuflt = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaIsufLetFlg = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaReadyToTrig = new FixedLengthStringData(1);

	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();

	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private T5679rec t5679rec = new T5679rec();
	private T5675rec t5675rec = new T5675rec();
	private T6598rec t6598rec = new T6598rec();
	private T5688rec t5688rec = new T5688rec();
	private Th506rec th506rec = new Th506rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private MgmFeeRec mgmFeeRec = new MgmFeeRec();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private VprcpfDAO vprcpfDAO = getApplicationContext().getBean("vprcpfDAO", VprcpfDAO.class);
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private Dry6528DAO dry6528DAO = getApplicationContext().getBean("dry6528DAO", Dry6528DAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();

	private BigDecimal wsaaLastcoi = BigDecimal.ZERO;
	private BigDecimal wsaaFundval = BigDecimal.ZERO;
	private BigDecimal wsaaTempval = BigDecimal.ZERO;

	private Covrpf covrIO = new Covrpf();
	private Map<String, List<Itempf>> t5675Map;
	private Map<String, Descpf> t1688DescMap;
	private Map<String, List<Itempf>> t6598Map;
	private Map<String, List<Itempf>> t5688Map;
	private Map<String, List<Itempf>> th506Map;
	private Map<String, List<Itempf>> tr384Map;
	private Map<String, List<Utrspf>> utrsMap;
	private Map<String, List<Vprcpf>> vprnudlMap;
	private Map<String, List<Hitspf>> hitsMap;
	private Map<String, List<Zrstpf>> zrstMap;
	private Map<String, List<Zrstpf>> zrsttraMap;
	private Map<String, List<Chdrpf>> chdrlifMap;
	private Map<String, List<Covrpf>> covrMap;
	private List<String> utrnrnlList;
	private List<String> hitrrnlList;
	private List<Covrpf> covrpfList;
	private Chdrpf chdrlifIO;
	private List<Chdrpf> updateChdrpfList;
	private List<Ptrnpf> insertPtrnpfList;
	private List<Covrpf> updateCovrpfList;
	private int ct01Value;
	private int ct02Value;
	private int ct03Value;
	private int ct04Value;
	private int ct05Value;
	private List<Utrspf> utrspf;
	private boolean mgmFeeFlag;
	private boolean advisorFeeCalc;
	private Map<String, Integer> chdrBtDateMap;

	private Map<String, List<Itempf>> t5687Map;
	private Map<String, List<Itempf>> t5534Map;

	private T5534rec t5534rec = new T5534rec();
	private T5687rec t5687rec = new T5687rec();

	public Dry6528() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry6528Dto> dry6528DtoList = readChunkRecord();
		for (Dry6528Dto dry6528Dto : dry6528DtoList) {
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dry6528Dto);
			}
		}
		updateDiaryData();
		commit3500();
	}

	protected void initialise1000() {
		wsspEdterror.set(Varcom.oK);
		insertPtrnpfList = new ArrayList<Ptrnpf>();
		varcom.vrcmDate.set(getCobolDate());
		advisorFeeCalc = FeaConfg.isFeatureExist(drypDryprcRecInner.drypCompany.toString(), "CSULK004", appVars, "IT");

		/* Read T5679 for valid statii. */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(T5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());

		String coy = drypDryprcRecInner.drypCompany.toString();

		t5675Map = itemDAO.loadSmartTable("IT", coy, "T5675");

		t1688DescMap = descDAO.getItems("IT", coy, "T1688", drypDryprcRecInner.drypLanguage.toString());

		t6598Map = itemDAO.loadSmartTable("IT", coy, "T6598");

		t5688Map = itemDAO.loadSmartTable("IT", coy, "T5688");

		th506Map = itemDAO.loadSmartTable("IT", coy, "TH506");
		tr384Map = itemDAO.loadSmartTable("IT", coy, "TR384");

		/* Load T5687 and T5534 into working storage for fast access. */
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		t5534Map = itemDAO.loadSmartTable("IT", coy, "T5534");

	}

	private List<Dry6528Dto> readChunkRecord() {
		List<Dry6528Dto> covrList = dry6528DAO.getCovrRecords(drypDryprcRecInner.drypCompany.toString(),
				drypDryprcRecInner.drypRunDate.toInt(), drypDryprcRecInner.drypEntity.toString());
		int i = 0;
		for (Dry6528Dto dry6528Dto : covrList) {
			BenefitBilling benBillingPojo = new BenefitBilling();
			t5687T5534Checks2530(dry6528Dto);
			benBillingPojo.setAdfeemth(t5534rec.adfeemth.trim());
			benBillingPojo.setJlPremMeth(t5534rec.jlPremMeth.trim());
			benBillingPojo.setPremMeth(t5534rec.premmeth.trim());
			benBillingPojo.setSubprog(t5534rec.subprog.trim());
			benBillingPojo.setSvMethod(t5534rec.svMethod.trim());
			benBillingPojo.setUnitFreq(t5534rec.unitFreq.trim());
			dry6528Dto.setBenefitBilling(benBillingPojo);
			covrList.set(i, dry6528Dto);
			i++;
		}

		if (!covrList.isEmpty()) {
			List<String> chdrnumSet = Collections.singletonList(drypDryprcRecInner.drypEntity.trim());
			String coy = drypDryprcRecInner.drypCompany.toString();
			if (!chdrnumSet.isEmpty()) {
				utrsMap = utrspfDAO.searchUtrsRecord(coy, chdrnumSet);
			}
			if (utrsMap != null && !utrsMap.isEmpty()) {
				List<String> vrtfndSet = new ArrayList<>();
				for (List<Utrspf> usList : utrsMap.values()) {
					for (Utrspf us : usList) {
						vrtfndSet.add(us.getUnitVirtualFund());
					}
				}
				vprnudlMap = vprcpfDAO.searchVprnudlRecord(drypDryprcRecInner.drypRunDate.toInt(), vrtfndSet, coy);
			}
			zrstMap = zrstpfDAO.searchZrstRecordByChdrnum(chdrnumSet, coy);
			hitsMap = hitspfDAO.searchHitsRecordByChdrcoy(coy, chdrnumSet);
			zrsttraMap = zrstpfDAO.searchZrsttraRecordByChdrnum(chdrnumSet, coy);
			utrnrnlList = utrnpfDAO.checkUtrnrnlRecordByChdrnum(coy, chdrnumSet);
			hitrrnlList = hitrpfDAO.checkHitrrnlRecordByChdrnum(coy, chdrnumSet);

			chdrlifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumSet);
			covrMap = covrpfDAO.searchCovrBenbilldate(coy, chdrnumSet);
			Covrpf tempcovrpf = new Covrpf();
			tempcovrpf.setChdrcoy(coy);
			tempcovrpf.setChdrnum(covrList.get(0).getChdrNum());
			covrpfList = covrpfDAO.searchCovrRecordForContract(tempcovrpf);
			if (advisorFeeCalc) {
				chdrBtDateMap = linspfDAO.searchTdayBillCDContractNos(chdrnumSet, coy);
			}
		}
		return covrList;
	}

	protected void t5687T5534Checks2530(Dry6528Dto dry6528Dto) {

		List<Itempf> t5687List = t5687Map.get(dry6528Dto.getCrTable());

		boolean isT5687ItemFound = false;
		for (Itempf itempf : t5687List) {
			if (itempf.getItmfrm().intValue() < dry6528Dto.getBenBillDate()) {
				t5687rec.t5687Rec.set(itempf.getGenareaString());
				isT5687ItemFound = true;
			}
		}

		if (!isT5687ItemFound) {
			drylogrec.params.set(dry6528Dto.getCrTable() + "not found in T5687");
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		List<Itempf> t5534List = t5534Map.get(t5687rec.bbmeth.trim());

		if (t5534List == null || t5534List.isEmpty()) {
			drylogrec.params.set(t5687rec.bbmeth.trim() + "not found in T5534");
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (t5534List.get(0) != null) {
			t5534rec.t5534Rec.set(t5534List.get(0).getGenareaString());
		} else {
			drylogrec.params.set(t5687rec.bbmeth.trim() + "not found in T5534");
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

	}

	protected void update3000(Dry6528Dto dry6528Dto) {
		if (isNE(dry6528Dto.getChdrNum(), lastChdrnum)) {
			if (isNE(lastChdrnum, SPACES) && validStatus.isTrue()) {
				triggerAllLetter4100();
			}
			readhChdr3100(dry6528Dto);
			chdrStatusCheck3200();
			if (validStatus.isTrue()) {
				rewriteChdr3150();
				writePtrn3400(dry6528Dto);
			} else {
				lastChdrnum.set(dry6528Dto.getChdrNum());
				return;
			}
		}
		if (!validStatus.isTrue()) {
			return;
		}
		ct03Value++;
		while (!(isGT(dry6528Dto.getBenBillDate(), drypDryprcRecInner.drypRunDate))) {
			callT5534Subprog3300(dry6528Dto);
			readhCovr3600(dry6528Dto);
			calcNewBbDate3700(dry6528Dto);
			rewriteCovr3800();
		}
		lastChdrnum.set(dry6528Dto.getChdrNum());

	}

	protected void readhChdr3100(Dry6528Dto dry6528Dto) {
		/* READH-CHDR */
		if (chdrlifMap != null && chdrlifMap.containsKey(dry6528Dto.getChdrNum())) {
			List<Chdrpf> chdrList = chdrlifMap.get(dry6528Dto.getChdrNum());
			for (Chdrpf c : chdrList) {
				if (c.getChdrcoy().toString().equals(dry6528Dto.getChdrCoy())) {
					wsaaOccdate.set(c.getOccdate());
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			drylogrec.params.set(dry6528Dto.getChdrNum());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void rewriteChdr3150() {
		/* REWRITE-CHDR */
		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
		if (updateChdrpfList == null) {
			updateChdrpfList = new ArrayList<Chdrpf>();
		}
		updateChdrpfList.add(chdrlifIO);
		/* EXIT */
	}

	protected void chdrStatusCheck3200() {
		/* CHDR-STATUS-CHECK */
		wsaaValidStatus.set("N");
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[ix.toInt()], chdrlifIO.getStatcode())) {
				ix.set(13);
				wsaaValidStatus.set("Y");
			}
		}
		if (!validStatus.isTrue()) {
			ct05Value++;
		}
		/* EXIT */
	}

	protected void callT5534Subprog3300(Dry6528Dto dry6528Dto) {

		/* Firstly, set up the param record with the values found */
		/* in the splitter program. (B6527) */
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(dry6528Dto.getChdrCoy());
		ubblallpar.chdrChdrnum.set(dry6528Dto.getChdrNum());
		ubblallpar.lifeLife.set(dry6528Dto.getLife());
		ubblallpar.lifeJlife.set(dry6528Dto.getJlife());
		ubblallpar.covrCoverage.set(dry6528Dto.getCoverage());
		ubblallpar.covrRider.set(dry6528Dto.getRider());
		ubblallpar.planSuffix.set(dry6528Dto.getPlanSuffix());
		ubblallpar.billfreq.set(dry6528Dto.getBenefitBilling().getUnitFreq());
		ubblallpar.cntcurr.set(dry6528Dto.getPremCurrency());
		/* MOVE BBLDAT TO UBBL-EFFDATE. */
		ubblallpar.effdate.set(dry6528Dto.getBenBillDate());
		ubblallpar.premMeth.set(dry6528Dto.getBenefitBilling().getPremMeth());
		ubblallpar.jlifePremMeth.set(dry6528Dto.getBenefitBilling().getJlPremMeth());
		ubblallpar.sumins.set(dry6528Dto.getSumins());
		ubblallpar.premCessDate.set(dry6528Dto.getPremCessDate());
		ubblallpar.crtable.set(dry6528Dto.getCrTable());
		ubblallpar.mortcls.set(dry6528Dto.getMortcls());
		ubblallpar.svMethod.set(dry6528Dto.getBenefitBilling().getSvMethod());
		ubblallpar.adfeemth.set(dry6528Dto.getBenefitBilling().getAdfeemth());
		/* Other values either available or found in this program. */
		ubblallpar.batccoy.set(drypDryprcRecInner.drypCompany);
		ubblallpar.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		ubblallpar.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		ubblallpar.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		ubblallpar.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		ubblallpar.batch.set(drypDryprcRecInner.drypBatcbatch);
		ubblallpar.billchnl.set(chdrlifIO.getBillchnl());
		ubblallpar.cnttype.set(chdrlifIO.getCnttype());
		ubblallpar.tranno.set(chdrlifIO.getTranno());
		ubblallpar.polsum.set(chdrlifIO.getPolsum());
		ubblallpar.ptdate.set(chdrlifIO.getPtdate());
		ubblallpar.polinc.set(chdrlifIO.getPolinc());
		ubblallpar.singp.set(ZERO);
		ubblallpar.chdrRegister.set(chdrlifIO.getReg());
		ubblallpar.language.set(drypDryprcRecInner.drypLanguage);
		ubblallpar.user.set(ZERO);
		ubblallpar.function.set("RENL");
		setupNewUbblFields5000(dry6528Dto);
		setAdvisorFeesRequired(dry6528Dto);
		/* IVE-795 RUL Product - Benefit Billing Calculation started */

		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable()
				&& ExternalisedRules.isCallExternal(dry6528Dto.getBenefitBilling().getSubprog())
				&& er.isExternalized(ubblallpar.cnttype.toString(), ubblallpar.crtable.toString()))) {
			callProgramX(dry6528Dto.getBenefitBilling().getSubprog(), ubblallpar.ubblallRec);//
		} else {
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxubblrec vpxubblrec = new Vpxubblrec();

			vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);
			vpxubblrec.function.set("INIT");
			callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec, vpxubblrec);
			ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
			Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
			vpmfmtrec.initialize();
			vpmfmtrec.date01.set(ubblallpar.effdate);
			vpmfmtrec.previousSum.set(ZERO);
			for (Covrpf c : covrpfList) {
				if (c.getCrtable().equals(dry6528Dto.getCrTable())) {
					vpmfmtrec.state.set(c.getZclstate());
					vpmfmtrec.riskComDate.set(c.getCurrfrom());
				}
			}
			mgmFeeFlag = FeaConfg.isFeatureExist(dry6528Dto.getChdrCoy().trim(), MGMFEE_ID, appVars, "IT");
			if (mgmFeeFlag) {
				initMgmFeeRec(dry6528Dto);
				callProgram(dry6528Dto.getBenefitBilling().getSubprog(), vpmfmtrec, mgmFeeRec, vpxubblrec);
			} else {
				callProgram(dry6528Dto.getBenefitBilling().getSubprog(), vpmfmtrec, ubblallpar.ubblallRec, vpxubblrec);
			}
			callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);

			if (isEQ(ubblallpar.statuz, SPACES))
				ubblallpar.statuz.set(Varcom.oK);
		}

		/* IVE-795 RUL Product - Benefit Billing Calculation end */
		if (isNE(ubblallpar.statuz, Varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("Sub:", SPACES);
			stringVariable1.addExpression(dry6528Dto.getBenefitBilling().getSubprog(), SPACES);
			stringVariable1.addExpression(" Parms: ");
			stringVariable1.addExpression(ubblallpar.ubblallRec);
			stringVariable1.setStringInto(drylogrec.params);
			drylogrec.statuz.set(ubblallpar.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

	}

	protected void writePtrn3400(Dry6528Dto dry6528Dto) {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(dry6528Dto.getChdrCoy());
		ptrnIO.setChdrnum(dry6528Dto.getChdrNum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate.toInt());
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmDate.set(getCobolDate());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		/* MOVE BSSC-EFFECTIVE-DATE TO PTRN-TRANSACTION-DATE. */
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setUserT(0);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate.toInt());
		ct04Value++;
		insertPtrnpfList.add(ptrnIO);
	}

	protected void commitPtrnBulkInsert() {
		boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
		if (!isInsertPtrnPF) {
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else
			insertPtrnpfList.clear();
	}

	protected void commit3500() {
		/* COMMIT */
		wsaaJustCommited.set("Y");
		commitControlTotals();
		if (updateChdrpfList != null) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(updateChdrpfList);
			updateChdrpfList.clear();
		}
		commitPtrnBulkInsert();
		if (updateCovrpfList != null) {
			covrpfDAO.updateCovrBillbendateRecord(updateCovrpfList);
			updateCovrpfList.clear();
		}
		/* EXIT */
	}

	private void commitControlTotals() {
		drycntrec.contotNumber.set(CT01);
		drycntrec.contotValue.set(ct01Value);
		d000ControlTotals();
		ct01Value = 0;

		drycntrec.contotNumber.set(CT02);
		drycntrec.contotValue.set(ct02Value);
		d000ControlTotals();
		ct02Value = 0;

		drycntrec.contotNumber.set(CT03);
		drycntrec.contotValue.set(ct03Value);
		d000ControlTotals();
		ct03Value = 0;

		drycntrec.contotNumber.set(CT04);
		drycntrec.contotValue.set(ct04Value);
		d000ControlTotals();
		ct04Value = 0;

		drycntrec.contotNumber.set(CT05);
		drycntrec.contotValue.set(ct05Value);
		d000ControlTotals();
		ct05Value = 0;
	}

	protected void readhCovr3600(Dry6528Dto dry6528Dto) {
		/* We need to read the Coverage/Rider after the call to the */
		/* generic subroutine as the subroutine will have updated */
		/* the coverage details. */
		if (covrMap != null && covrMap.containsKey(dry6528Dto.getChdrNum())) {
			List<Covrpf> covrList = covrMap.get(dry6528Dto.getChdrNum());
			for (Covrpf c : covrList) {
				if (c.getChdrcoy().equals(dry6528Dto.getChdrCoy()) && c.getLife().equals(dry6528Dto.getLife())
						&& c.getCoverage().equals(dry6528Dto.getCoverage())
						&& c.getRider().equals(dry6528Dto.getRider())
						&& c.getPlanSuffix() == dry6528Dto.getPlanSuffix()
						&& "1".equals(c.getValidflag())) {
					covrIO = c;
				}
			}
		}
		if (covrIO == null) {
			drylogrec.params.set(dry6528Dto.getChdrNum());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void calcNewBbDate3700(Dry6528Dto dry6528Dto) {

		/* Calculate COVR-BEN-BILL-DATE as COVR-BEN-BILL-DATE incremente */
		/* by the deduction frequency from T5534. */
		if (isNE(dry6528Dto.getBenefitBilling().getUnitFreq(), NUMERIC)) {
			dry6528Dto.getBenefitBilling().setUnitFreq("00");
		}
		/* MOVE UFREQ TO DTC2-FREQUENCY. */
		/* MOVE 1 TO DTC2-FREQ-FACTOR. */
		/* MOVE COVR-BEN-BILL-DATE TO DTC2-INT-DATE-1. */
		/* MOVE 0 TO DTC2-INT-DATE-2. */
		/* CALL 'DATCON2' USING DTC2-DATCON2-REC. */
		/* IF DTC2-STATUZ NOT = O-K */
		/* MOVE DTC2-DATCON2-REC TO SYSR-PARAMS */
		/* MOVE DTC2-STATUZ TO SYSR-STATUZ */
		/* PERFORM 600-FATAL-ERROR. */
		/* MOVE DTC2-INT-DATE-2 TO COVR-BEN-BILL-DATE. */
		datcon4rec.frequency.set(dry6528Dto.getBenefitBilling().getUnitFreq());
		datcon4rec.freqFactor.set(1);
		datcon4rec.intDate1.set(dry6528Dto.getBenBillDate());
		datcon4rec.intDate2.set(0);
		datcon4rec.billdayNum.set(wsaaOccDd);
		datcon4rec.billmonthNum.set(wsaaOccMm);
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon4rec.datcon4Rec);
			drylogrec.statuz.set(datcon4rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		covrIO.setBenBillDate(datcon4rec.intDate2.toInt());
		dry6528Dto.setBenBillDate(datcon4rec.intDate2.toInt());
	}

	protected void rewriteCovr3800() {
		/* REWRITE-COVR */
		if (updateCovrpfList == null) {
			updateCovrpfList = new ArrayList<>();
		}
		updateCovrpfList.add(covrIO);
		/* EXIT */
	}

	protected void triggerAllLetter4100() {
		wsaaFundval = BigDecimal.ZERO;
		wsaaReadyToTrig.set(SPACES);
		utrsFundval5500();
		hitsFundval5100();
		getLastcoi5200();
		readUtrn6600();
		readHitr6700();
		triggerInsuffLet4120();
	}

	protected void triggerInsuffLet4120() {
		if (isEQ(wsaaReadyToTrig, "N")) {
			return;
		}
		readTh5066000();
		if (isEQ(th506rec.nofbbwrn, ZERO) && isEQ(th506rec.nofbbisf, ZERO)) {
			return;
		}
		wsaaMinfundWrnlt.set(ZERO);
		wsaaMinfundInsuflt.set(ZERO);
		if (isNE(th506rec.nofbbisf, ZERO)) {
			insufLetProc6400();
			if (isEQ(wsaaIsufLetFlg, "Y")) {
				return;
			}
		}
		triggerWarnLet4130();
	}

	protected void triggerWarnLet4130() {
		if (isNE(th506rec.nofbbwrn, ZERO)) {
			warnLetProc6100();
		}
	}

	protected void setupNewUbblFields5000(Dry6528Dto dry6528Dto) {

		ubblallpar.occdate.set(chdrlifIO.getOccdate());
		/* Setup T5675 fields */
		List<Itempf> t5675List = t5675Map.get(ubblallpar.premMeth.trim());

		if (t5675List == null || t5675List.isEmpty()) {
			t5675rec.t5675Rec.set(SPACES);
		}

		if (t5675List != null && t5675List.get(0) != null) {
			t5675rec.t5675Rec.set(t5675List.get(0).getGenareaString());
			ubblallpar.premsubr.set(t5675rec.premsubr);
		} else {
			t5675rec.t5675Rec.set(SPACES);
		}

		/* Now search for Join Life */
		List<Itempf> t5675ListJL = t5675Map.get(ubblallpar.jlifePremMeth.trim());

		if (t5675ListJL == null || t5675ListJL.isEmpty()) {
			t5675rec.t5675Rec.set(SPACES);
		}

		if (t5675ListJL != null && t5675ListJL.get(0) != null) {
			t5675rec.t5675Rec.set(t5675ListJL.get(0).getGenareaString());
			ubblallpar.jpremsubr.set(t5675rec.premsubr);
		} else {
			t5675rec.t5675Rec.set(SPACES);
		}

		/* Setup T1688 field */

		Descpf t1688Desc = t1688DescMap.get(ubblallpar.batctrcde.trim());

		if (t1688Desc == null) {
			drylogrec.params.set("Record not found in T1688");
			drylogrec.statuz.set(E044);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {
			ubblallpar.trandesc.set(t1688Desc.getLongdesc());
		}

		/* Setup T6598 field */
		List<Itempf> t6598ItemList = t6598Map.get(ubblallpar.svMethod.trim());
		if (t6598ItemList == null || t6598ItemList.isEmpty()) {
			drylogrec.params.set("Record not found in T6598");
			drylogrec.statuz.set(H021);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(t6598ItemList.get(0).getGenarea()));
			ubblallpar.svCalcprog.set(t6598rec.calcprog);
		}
		/* Setup T5688 field */
		List<Itempf> t5688List = t5688Map.get(ubblallpar.cnttype.trim());

		boolean isT5688ItemFound = false;
		for (Itempf t5688Item : t5688List) {
			if (isGTE(ubblallpar.effdate, t5688Item.getItmfrm())) {
				t5688rec.t5688Rec.set(t5688Item.getGenareaString());
				ubblallpar.comlvlacc.set(t5688rec.comlvlacc);
				isT5688ItemFound = true;
				break;
			}
		}

		if (!isT5688ItemFound) {
			drylogrec.params.set("Record not found in T5687");
			drylogrec.statuz.set(E308);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void utrsFundval5500() {
		String lasContractNo = lastChdrnum.toString();
		if (utrsMap != null && utrsMap.containsKey(lasContractNo)) {
			List<Utrspf> utList = utrsMap.get(lasContractNo);
			for (Utrspf u : utList) {
				if (u.getChdrcoy().equals(drypDryprcRecInner.drypCompany.toString())) {
					BigDecimal unitBidPrice = BigDecimal.ZERO;
					if (vprnudlMap != null && vprnudlMap.containsKey(u.getUnitVirtualFund())) {
						List<Vprcpf> vprcpfList = vprnudlMap.get(u.getUnitVirtualFund());
						for (Vprcpf v : vprcpfList) {
							if (v.getUnitType().endsWith(u.getUnitType())) {
								unitBidPrice = v.getUnitBidPrice();
								break;
							}
						}
					}
					wsaaTempval = u.getCurrentUnitBal().multiply(unitBidPrice);
					wsaaFundval = wsaaFundval.add(wsaaTempval);
				}
			}
		}
	}

	protected void hitsFundval5100() {
		String lastContractNum = lastChdrnum.toString();
		if (hitsMap != null && hitsMap.containsKey(lastContractNum)) {
			List<Hitspf> hsList = hitsMap.get(lastContractNum);
			for (Hitspf h : hsList) {
				wsaaFundval = wsaaFundval.add(h.getZcurprmbal());
			}
		}
	}

	protected void getLastcoi5200() {
		wsaaLastcoi = BigDecimal.ZERO;
		wsaaTranno.set(ZERO);
		wsaaBatctrcde.set(SPACES);
		wsaaTrandate.set(ZERO);
		String lastContractNum = lastChdrnum.toString();
		if (zrstMap != null && zrstMap.containsKey(lastContractNum)) {
			for (Zrstpf z : zrstMap.get(lastContractNum)) {
				if (isEQ(z.getBatctrcde(), drypDryprcRecInner.drypBatctrcde)) {
					wsaaTranno.set(z.getTranno());
					wsaaBatctrcde.set(z.getBatctrcde());
					wsaaTrandate.set(z.getTrandate());
					continue15230();
				}
			}
		}
	}

	protected void continue15230() {
		String lastContractNum = lastChdrnum.toString();
		if (zrsttraMap != null && zrsttraMap.containsKey(lastContractNum)) {
			for (Zrstpf z : zrsttraMap.get(lastContractNum)) {
				if (z.getTranno() == wsaaTranno.toInt() && z.getBatctrcde().equals(wsaaBatctrcde.toString())
						&& z.getTrandate() == wsaaTrandate.toInt()) {
					wsaaLastcoi = wsaaLastcoi.add(z.getZramount01());
				}
			}
		}
	}

	protected void readTh5066000() {
		if (th506Map != null) {
			if (th506Map.containsKey(chdrlifIO.getCnttype())) {
				th506rec.th506Rec.set(StringUtil.rawToString(th506Map.get(chdrlifIO.getCnttype()).get(0).getGenarea()));
				return;
			} else if (th506Map.containsKey("***")) {
				th506rec.th506Rec.set(StringUtil.rawToString(th506Map.get("***").get(0).getGenarea()));
				return;
			}
		}
		drylogrec.params.set("th506:" + chdrlifIO.getCnttype());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();
	}

	protected void warnLetProc6100() {
		/* START */
		compute(wsaaMinfundWrnlt, 2).set(mult(wsaaLastcoi, th506rec.nofbbwrn));
		if (isLT(wsaaFundval, wsaaMinfundWrnlt)) {
			letcokcpy.isNoBb.set(th506rec.nofbbwrn);
			genWarnLetc6200();
		}
		/* EXIT */
	}

	protected void genWarnLetc6200() {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype());
		stringVariable1.addExpression("WLET");
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("***");
		stringVariable2.addExpression("WLET");
		if (tr384Map != null) {
			String stringVar1 = stringVariable1.toString();
			String stringVar2 = stringVariable2.toString();
			if (tr384Map.containsKey(stringVar1)) {

				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVar1).get(0).getGenarea()));
				continue6250();
			} else if (tr384Map.containsKey(stringVar2)) {

				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVar2).get(0).getGenarea()));
				continue6250();
			}
		}
	}

	protected void continue6250() {
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		getLetc6300();
	}

	protected void getLetc6300() {
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(drypDryprcRecInner.drypRunDate);
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letcokcpy.isLanguage.set(drypDryprcRecInner.drypLanguage);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.despnum.set(chdrlifIO.getDespnum());
		letrqstrec.trcde.set(drypDryprcRecInner.drypBatctrcde);
		letcokcpy.isFndamt.set(wsaaFundval);
		letcokcpy.isBbamt.set(wsaaLastcoi);
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, Varcom.oK)) {
			drylogrec.params.set(letrqstrec.params);
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void insufLetProc6400() {
		/* START */
		wsaaIsufLetFlg.set(SPACES);
		compute(wsaaMinfundInsuflt, 2).set(mult(wsaaLastcoi, th506rec.nofbbisf));
		if (isLT(wsaaFundval, wsaaMinfundInsuflt)) {
			wsaaIsufLetFlg.set("Y");
			letcokcpy.isNoBb.set(th506rec.nofbbisf);
			genInsufLetc6500();
		}
		/* EXIT */
	}

	protected void genInsufLetc6500() {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlifIO.getCnttype());
		stringVariable1.addExpression("ILET");
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("***");
		stringVariable2.addExpression("ILET");
		if (tr384Map != null) {
			String stringVar1 = stringVariable1.toString();
			String stringVar2 = stringVariable2.toString();
			if (tr384Map.containsKey(stringVar1)) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVar1).get(0).getGenarea()));
				continue6550();
			} else if (tr384Map.containsKey(stringVar2)) {
				tr384rec.tr384Rec.set(StringUtil.rawToString(tr384Map.get(stringVar2).get(0).getGenarea()));
				continue6550();
			}
		}
	}

	protected void continue6550() {
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		getLetc6300();
	}

	protected void readUtrn6600() {
		/* Check that the contract has no unprocessed URNRs pending. */
		if (utrnrnlList != null && utrnrnlList.contains(lastChdrnum.toString())) {
			wsaaReadyToTrig.set("N");
		}
	}

	protected void readHitr6700() {
		/* Check that the contract has no unprocessed URNRs pending. */
		if (hitrrnlList != null && hitrrnlList.contains(lastChdrnum.toString())) {
			wsaaReadyToTrig.set("N");
		}
	}

	private List<String> getCurrUnitBal(String coy, Dry6528Dto dry6528Dto) {
		List<String> addToList = new ArrayList<String>();
		utrspf = utrspfDAO.getUtrsRecord(coy, dry6528Dto.getChdrNum(), dry6528Dto.getLife(), dry6528Dto.getCoverage(),
				dry6528Dto.getRider());
		if (!(utrspf == null || utrspf.isEmpty())) {
			for (Utrspf utlist : utrspf) {
				if (utlist != null && utlist.getCurrentUnitBal() != null) {
					addToList.add(utlist.getCurrentUnitBal().toString());

				}
			}
		}
		return addToList;
	}

	private List<String> getUnitBidPrice(String coy, int effdate) {
		List<String> addToList = null;
		if (!(utrspf == null || utrspf.isEmpty())) {
			addToList = new ArrayList<String>();
			for (Utrspf utlist : utrspf) {
				if (utlist != null && utlist.getCurrentUnitBal() != null) {
					addToList.add(vprcpfDAO.getUnitBidPrice(coy, utlist.getUnitVirtualFund(), effdate).toString());

				}
			}
		}
		return addToList;
	}

	private void initMgmFeeRec(Dry6528Dto dry6528Dto) {

		mgmFeeRec.setFunction("RENL");
		mgmFeeRec.setChdrChdrcoy(dry6528Dto.getChdrCoy());
		mgmFeeRec.setChdrChdrnum(dry6528Dto.getChdrNum());
		mgmFeeRec.setLifeLife(dry6528Dto.getLife());
		mgmFeeRec.setLifeJlife(dry6528Dto.getJlife());
		mgmFeeRec.setCovrCoverage(dry6528Dto.getCoverage());
		mgmFeeRec.setCovrRider(dry6528Dto.getRider());
		mgmFeeRec.setPlanSuffix(dry6528Dto.getPlanSuffix());
		mgmFeeRec.setBillfreq(dry6528Dto.getBenefitBilling().getUnitFreq());
		mgmFeeRec.setCntcurr(dry6528Dto.getPremCurrency());
		mgmFeeRec.setEffdate(dry6528Dto.getBenBillDate());
		mgmFeeRec.setPremMeth(dry6528Dto.getBenefitBilling().getPremMeth());
		mgmFeeRec.setJlifePremMeth(dry6528Dto.getBenefitBilling().getJlPremMeth());
		mgmFeeRec.setSumins(dry6528Dto.getSumins());
		mgmFeeRec.setPremCessDate(dry6528Dto.getPremCessDate());
		mgmFeeRec.setCrtable(dry6528Dto.getCrTable());
		mgmFeeRec.setMortcls(dry6528Dto.getMortcls());
		mgmFeeRec.setSvMethod(dry6528Dto.getBenefitBilling().getSvMethod());
		mgmFeeRec.setAdfeemth(dry6528Dto.getBenefitBilling().getAdfeemth());
		mgmFeeRec.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		mgmFeeRec.setBatcbrn(drypDryprcRecInner.drypBatcbrn.toString());
		mgmFeeRec.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		mgmFeeRec.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		mgmFeeRec.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		mgmFeeRec.setBatch(drypDryprcRecInner.drypBatcbatch.toString());
		mgmFeeRec.setBillchnl(chdrlifIO.getBillchnl());
		mgmFeeRec.setCnttype(chdrlifIO.getCnttype());
		mgmFeeRec.setTranno(chdrlifIO.getTranno());
		mgmFeeRec.setPolsum2(chdrlifIO.getPolsum());
		mgmFeeRec.setPtdate(chdrlifIO.getPtdate());
		mgmFeeRec.setPolinc2(chdrlifIO.getPolinc());
		mgmFeeRec.setSingp(BigDecimal.ZERO);
		mgmFeeRec.setChdrRegister(chdrlifIO.getReg());
		mgmFeeRec.setLanguage(drypDryprcRecInner.drypLanguage.toString());
		mgmFeeRec.setUser(0);
		mgmFeeRec.setPolcurr(0);
		mgmFeeRec.setBenfunc("0");
		mgmFeeRec.setFirstPrem(BigDecimal.ZERO);
		List<String> currUnitbal = getCurrUnitBal(dry6528Dto.getChdrCoy(), dry6528Dto);
		mgmFeeRec.setCurUnitBal(currUnitbal);
		mgmFeeRec.setUnitBidPrice(getUnitBidPrice(dry6528Dto.getChdrCoy(), dry6528Dto.getBenBillDate()));
		mgmFeeRec.setNrFunds(currUnitbal.size());
	}

	protected void setAdvisorFeesRequired(Dry6528Dto dry6528Dto) {
		if (advisorFeeCalc) {
			Integer date = chdrBtDateMap.get(dry6528Dto.getChdrNum());
			if (date != null && date != 0) {
				if (isEQ(date, dry6528Dto.getBenBillDate())) {
					int i = 0;

					List<Utrspf> data = utrsMap.get(dry6528Dto.getChdrNum());
					if (data != null) {
						for (Utrspf utrspfdata : data) {
							if (dry6528Dto.getCoverage().equals(utrspfdata.getCoverage())
									&& dry6528Dto.getLife().equals(utrspfdata.getLife())
									&& dry6528Dto.getRider().equals(utrspfdata.getRider())
									&& vprnudlMap.containsKey(utrspfdata.getUnitVirtualFund())) {
								List<Vprcpf> vprcpfList = vprnudlMap.get(utrspfdata.getUnitVirtualFund());

								for (Vprcpf v : vprcpfList) {
									if (v.getUnitType().endsWith(utrspfdata.getUnitType())) {
										++i;

										ubblallpar.curdunitprice[i].set(v.getUnitBidPrice());
										ubblallpar.curdunitbal[i].set(utrspfdata.getCurrentUnitBal());
										break;
									}
								}
							}
						}
					}
					ubblallpar.nrFunds.set(i);
					ubblallpar.cntAdvisorFessReq.set("Y");
				} else {
					ubblallpar.cntAdvisorFessReq.set("N");
				}
			}
		} else {
			ubblallpar.cntAdvisorFessReq.set("N");
		}
	}

	private void updateDiaryData() {
		if (updateCovrpfList != null && updateCovrpfList.size() > 0) {
			drypDryprcRecInner.drypBbldate.set(covrIO.getBenBillDate());
		} else {
			drypDryprcRecInner.drypBbldate.set(varcom.vrcmMaxDate);
		}
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstcde());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		// ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}
}
