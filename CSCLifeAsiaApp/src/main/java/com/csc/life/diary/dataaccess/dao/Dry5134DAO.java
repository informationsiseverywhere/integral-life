package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry5134Dto;

/**
 * DAO to fetch records for pending auto increase.
 * 
 * @author hmahajan6
 *
 */
public interface Dry5134DAO {

	/**
	 * Gets records for pending auto increase.
	 * 
	 * @param company - Company number
	 * @param effDate - Effective date
	 * @param chdrNum - Contract number
	 * @return - Dry5134DTO
	 */
	public List<Dry5134Dto> getPendingAutoIncDetails(String company, String effDate, String chdrNum);
}
