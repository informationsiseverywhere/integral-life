/*
 * File: Dry5363.java
 * Date: March 26, 2014 3:03:31 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5363.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrudlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*            FLEXIBLE PREMIUM OVERDUE PROCESSING
*            -----------------------------------
*
* Flexible Premium overdue Processing processes the Flexible
* Premium Coverage File (FPCOPF) which are selected by B5363.
*
* This program is part of the Diary System.
* It includes the selection functionality from the splitter
* program.
*
* Control totals used in this program:
*
*    01  -  No. of FPCO records read
*    02  -  No. report entries written
*    03  -  No. of records of an invalid status
*
****************************************************************** ****
* </pre>
*/
public class Dry5363 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5363");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private static final String fpcorecrec = "FPCOREC";
	private static final String itemrec = "ITEMREC";
	private static final String covrudlrec = "COVRUDLREC";
	private static final String t1688 = "T1688";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

		/*  Control indicators*/
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private CovrudlTableDAM covrudlIO = new CovrudlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private T5679rec t5679rec = new T5679rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		pass2050, 
		nextr2080, 
		exit2090
	}

	public Dry5363() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		/*MAIN*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		initialise1000();
		while ( !(isEQ(fpcoIO.getStatuz(), varcom.endp))) {
			fpcoLoop2000();
		}
		
		close4000();
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Get transaction description.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(drypDryprcRecInner.drypCompany);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(drypDryprcRecInner.drypBatctrcde);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*  Load Contract statii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Initialise the COVR key fields..*/
		covrudlIO.setDataKey(SPACES);
		covrudlIO.setRecKeyData(SPACES);
		covrudlIO.setRecNonKeyData(SPACES);
		covrudlIO.setPlanSuffix(ZERO);
		/* Initialise the key fields for FPCO and the entity details*/
		/* passed via linkage.*/
		fpcoIO.setDataKey(SPACES);
		fpcoIO.setRecKeyData(SPACES);
		fpcoIO.setRecNonKeyData(SPACES);
		fpcoIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		fpcoIO.setChdrnum(drypDryprcRecInner.drypEntity);
		fpcoIO.setLife(ZERO);
		fpcoIO.setCoverage(ZERO);
		fpcoIO.setRider(ZERO);
		fpcoIO.setPlanSuffix(0);
		fpcoIO.setTargfrom(0);
		fpcoIO.setFormat(fpcorecrec);
		fpcoIO.setFunction(varcom.begn);
	}

protected void fpcoLoop2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case pass2050: 
					pass2050();
				case nextr2080: 
					nextr2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, fpcoIO);
		/* Check for database errors..*/
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(fpcoIO.getParams());
			drylogrec.statuz.set(fpcoIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Check for a change in entity or end of file...*/
		if (isNE(fpcoIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(fpcoIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(fpcoIO.getStatuz(), varcom.endp)) {
			fpcoIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		/* Only select those records where the PRMRCDP < OVRMINREQ*/
		if (isGTE(fpcoIO.getPremRecPer(), fpcoIO.getOverdueMin())) {
			goTo(GotoLabel.nextr2080);
		}
		/* Increment the control totals for the records selected..*/
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/* Check the statii on a change of record..*/
		if (isEQ(fpcoIO.getChdrcoy(), covrudlIO.getChdrcoy())
		&& isEQ(fpcoIO.getChdrnum(), covrudlIO.getChdrnum())
		&& isEQ(fpcoIO.getLife(), covrudlIO.getLife())
		&& isEQ(fpcoIO.getRider(), covrudlIO.getRider())
		&& isEQ(fpcoIO.getCoverage(), covrudlIO.getCoverage())
		&& isEQ(fpcoIO.getPlanSuffix(), covrudlIO.getPlanSuffix())) {
			goTo(GotoLabel.pass2050);
		}
		/* Read the COVR file to get the statii.*/
		covrudlIO.setDataKey(SPACES);
		covrudlIO.setRecKeyData(SPACES);
		covrudlIO.setRecNonKeyData(SPACES);
		covrudlIO.setChdrcoy(fpcoIO.getChdrcoy());
		covrudlIO.setChdrnum(fpcoIO.getChdrnum());
		covrudlIO.setLife(fpcoIO.getLife());
		covrudlIO.setRider(fpcoIO.getRider());
		covrudlIO.setCoverage(fpcoIO.getCoverage());
		covrudlIO.setPlanSuffix(fpcoIO.getPlanSuffix());
		covrudlIO.setFunction(varcom.readr);
		covrudlIO.setFormat(covrudlrec);
		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrudlIO.getParams());
			drylogrec.statuz.set(covrudlIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Validate the statii of the coverage.*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrudlIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrudlIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

protected void pass2050()
	{
		/* If it's not a valid record, forget any further processing*/
		/* update the relevent control total and exit the section.*/
		if (!validCoverage.isTrue()) {
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.nextr2080);
		}
		/* Valid record, report.*/
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		report3000();
	}

protected void nextr2080()
	{
		fpcoIO.setFunction(varcom.nextr);
	}

protected void report3000()
	{
		update3010();
	}

protected void update3010()
	{
		/* Set up details for report fields.*/
		dryrDryrptRecInner.r5363Prmper.set(fpcoIO.getTargetPremium());
		dryrDryrptRecInner.r5363Billedp.set(fpcoIO.getBilledInPeriod());
		dryrDryrptRecInner.r5363Prmrcdp.set(fpcoIO.getPremRecPer());
		dryrDryrptRecInner.r5363Ovrminreq.set(fpcoIO.getOverdueMin());
		dryrDryrptRecInner.r5363Minovrpro.set(fpcoIO.getMinOverduePer());
		/* Set up the report name.*/
		/* Set up the sort key details, then move to DRYR-SORTKEY.*/
		dryrDryrptRecInner.r5363Chdrnum.set(fpcoIO.getChdrnum());
		dryrDryrptRecInner.r5363Life.set(fpcoIO.getLife());
		dryrDryrptRecInner.r5363Coverage.set(fpcoIO.getCoverage());
		dryrDryrptRecInner.r5363Rider.set(fpcoIO.getRider());
		dryrDryrptRecInner.r5363Plnsfx.set(fpcoIO.getPlanSuffix());
		dryrDryrptRecInner.r5363Targto.set(fpcoIO.getTargto());
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5363DataArea = new FixedLengthStringData(495).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r5363Prmper = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 0);
	private ZonedDecimalData r5363Billedp = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 17);
	private ZonedDecimalData r5363Prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 34);
	private ZonedDecimalData r5363Ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 51);
	private ZonedDecimalData r5363Minovrpro = new ZonedDecimalData(3, 0).isAPartOf(r5363DataArea, 68);

	private FixedLengthStringData r5363SortKey = new FixedLengthStringData(32);
	private FixedLengthStringData r5363Chdrnum = new FixedLengthStringData(8).isAPartOf(r5363SortKey, 0);
	private FixedLengthStringData r5363Life = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 8);
	private FixedLengthStringData r5363Coverage = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 10);
	private FixedLengthStringData r5363Rider = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 12);
	private ZonedDecimalData r5363Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5363SortKey, 14);
	private PackedDecimalData r5363Targto = new PackedDecimalData(8, 0).isAPartOf(r5363SortKey, 18).setUnsigned();
	private FixedLengthStringData r5363ReportName = new FixedLengthStringData(10).init("R5363     ");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
}
