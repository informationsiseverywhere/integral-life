package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diaryframework.parent.Maind;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrpwTableDAM;
import com.csc.life.terminationclaims.dataaccess.PtsdclmTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.diary.recordstructures.Dryprcrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;



public class Dryr512 extends Maind {
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR512");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData formats = new FixedLengthStringData(70);
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData zrpwrec = new FixedLengthStringData(10).init("ZRPWREC");
	private FixedLengthStringData ptsdclmrec = new FixedLengthStringData(10).init("PTSDCLMREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData hitsrec = new FixedLengthStringData(10).init("HITSREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData tables = new FixedLengthStringData(5);
	private FixedLengthStringData t5646 = new FixedLengthStringData(5).isAPartOf(tables, 0).init("T5646");
	private FixedLengthStringData errors = new FixedLengthStringData(4);
	private FixedLengthStringData h068 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("H068");
	private ZonedDecimalData controlTotals = new ZonedDecimalData(2);
	private ZonedDecimalData ct01 = new ZonedDecimalData(2).isAPartOf(controlTotals, 0).init(01);
	
	
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaLextFound = new FixedLengthStringData(1).init("N");
	private Validator lextFound = new Validator(wsaaLextFound, "Y");

	private FixedLengthStringData wsaaFactorFound = new FixedLengthStringData(1).init("N");
	private Validator factorFound = new Validator(wsaaFactorFound, "Y");
	private PackedDecimalData wsaaPercRate = new PackedDecimalData(7, 4);
	private PackedDecimalData wsaaWithdrawalAmt = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewSumins1 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNewSumins2 = new PackedDecimalData(13, 2);
	private ZonedDecimalData indx = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTerm = new PackedDecimalData(3, 0);

	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ZrpwTableDAM zrpwIO = new ZrpwTableDAM();
	private PtsdclmTableDAM ptsdclmIO = new PtsdclmTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private T5646rec t5646rec = new T5646rec();
	private Varcom varcom = new Varcom();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Dryprcrec dryprcrec = new Dryprcrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr380,
		exit390,
		exit490,
		exit1090,
		exit1190,
		exit290,
		exit590
		
		
}
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			dryprcrec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	private void startProcessing100(){
		dryprcrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		wsspEdterror.set(varcom.oK);
		zrpwIO.setParams(SPACES);
		zrpwIO.setChdrcoy(dryprcrec.company);
		zrpwIO.setChdrnum(dryprcrec.entity);
		zrpwIO.setTranno(ZERO);
		zrpwIO.setFunction(varcom.begnh);
		zrpwIO.setFormat(zrpwrec);
		
			while(!isEQ(zrpwIO.getStatuz(),varcom.endp)){
				zrpwLoop200();
		}
			
	}
	
	

	private void zrpwLoop200(){
		SmartFileCode.execute(appVars, zrpwIO);
		if (isNE(zrpwIO.getStatuz(),varcom.oK)
				&& isNE(zrpwIO.getStatuz(),varcom.endp)) {
			drylogrec.params.set(zrpwIO.getParams());
			drylogrec.dryDatabaseError.setTrue();	
			a000FatalError();
		}
		
		if (isEQ(zrpwIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return;
		}
		wsspEdterror.set(varcom.oK);
		wsaaWithdrawalAmt.set(ZERO);
		wsaaPercRate.set(ZERO);
		wsaaNewSumins1.set(ZERO);
		wsaaNewSumins2.set(ZERO);
		ptsdclmIO.setParams(SPACES);
		ptsdclmIO.setChdrcoy(zrpwIO.getChdrcoy());
		ptsdclmIO.setChdrnum(zrpwIO.getChdrnum());
		ptsdclmIO.setTranno(zrpwIO.getTranno());
		ptsdclmIO.setPlanSuffix(zrpwIO.getPlanSuffix());
		ptsdclmIO.setCoverage(zrpwIO.getCoverage());
		ptsdclmIO.setRider(zrpwIO.getRider());
		ptsdclmIO.setLife(zrpwIO.getLife());
		ptsdclmIO.setFunction(varcom.begn);
		ptsdclmIO.setFormat(ptsdclmrec);
		while ( !(isEQ(ptsdclmIO.getStatuz(),varcom.endp))) {
			accumulateAmount300();
		}
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(zrpwIO.getChdrcoy());
		covrIO.setChdrnum(zrpwIO.getChdrnum());
		covrIO.setLife("01");
		covrIO.setPlanSuffix(zrpwIO.getPlanSuffix());
		covrIO.setCoverage(zrpwIO.getCoverage());
		covrIO.setRider(zrpwIO.getRider());
		covrIO.setFunction(varcom.begnh);
		covrIO.setFormat(covrrec);
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			processCovr1000();
		}
		zrpwIO.setFunction(varcom.delet);
		zrpwIO.setFormat(zrpwrec);
		SmartFileCode.execute(appVars, zrpwIO);
		if (isNE(zrpwIO.getStatuz(),varcom.oK)
		&& isNE(zrpwIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(zrpwIO.getStatuz());
			drylogrec.params.set(zrpwIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		
		zrpwIO.setFunction(varcom.nextr);
	}

	private void accumulateAmount300(){
		SmartFileCode.execute(appVars, ptsdclmIO);
		if (isNE(ptsdclmIO.getStatuz(),varcom.oK)
				&& isNE(ptsdclmIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(ptsdclmIO.getStatuz());
			drylogrec.params.set(ptsdclmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(ptsdclmIO.getFieldType(),"C")
		|| isEQ(ptsdclmIO.getFieldType(),"J")) {
			ptsdclmIO.setFunction(varcom.nextr);
			accumulateAmount300();
		}
		
		
		if ((isNE(ptsdclmIO.getChdrcoy(),zrpwIO.getChdrcoy()))
				|| (isNE(ptsdclmIO.getChdrnum(),zrpwIO.getChdrnum()))
				|| (isNE(ptsdclmIO.getTranno(),zrpwIO.getTranno()))
				|| (isNE(ptsdclmIO.getPlanSuffix(),zrpwIO.getPlanSuffix()))
				|| (isNE(ptsdclmIO.getCoverage(),zrpwIO.getCoverage()))
				|| (isNE(ptsdclmIO.getRider(),zrpwIO.getRider()))
				|| (isNE(ptsdclmIO.getLife(),zrpwIO.getLife()))
				|| (isEQ(ptsdclmIO.getStatuz(),varcom.endp))) {
					ptsdclmIO.setStatuz(varcom.endp);
					return;
				}
		wsaaWithdrawalAmt.add(ptsdclmIO.actvalue);
		readUtrs400();
	}

	private void readUtrs400()
	{
			utrsIO.setParams(SPACES);
			utrsIO.setChdrcoy(ptsdclmIO.getChdrcoy());
			utrsIO.setChdrnum(ptsdclmIO.getChdrnum());
			utrsIO.setLife(ptsdclmIO.getLife());
			utrsIO.setCoverage(ptsdclmIO.getCoverage());
			utrsIO.setRider(ptsdclmIO.getRider());
			utrsIO.setPlanSuffix(ptsdclmIO.getPlanSuffix());
			utrsIO.setUnitVirtualFund(ptsdclmIO.getVirtualFund());
			utrsIO.setUnitType(ptsdclmIO.getFieldType());
			utrsIO.setFunction(varcom.readr);
			utrsIO.setFormat(utrsrec);
			SmartFileCode.execute(appVars, utrsIO);
			
			if (isNE(utrsIO.getStatuz(),varcom.oK)
					&& isNE(utrsIO.getStatuz(),varcom.mrnf)) {
			drylogrec.statuz.set(utrsIO.getStatuz());
			drylogrec.params.set(utrsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();	
			a000FatalError();
		}
			if (isEQ(utrsIO.getStatuz(),varcom.mrnf)) {
			readHits600();
			return;

		}
		nowPrice500();
		compute(wsaaNewSumins2, 5).set(add((mult(utrsIO.getCurrentUnitBal(),vprnudlIO.getUnitBidPrice())),wsaaNewSumins2));
	}

	private void nowPrice500(){
		ufpricerec.set(SPACES);
		vprnudlIO.setDataKey(SPACES);
		vprnudlIO.setCompany(utrsIO.getChdrcoy());
		vprnudlIO.setUnitVirtualFund(utrsIO.getUnitVirtualFund());
		vprnudlIO.setUnitType(utrsIO.getUnitType());
		vprnudlIO.setEffdate(dryprcrec.runDate);
		vprnudlIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
		}
		if ((isNE(vprnudlIO.getCompany(),ptsdclmIO.getChdrcoy()))
				|| (isNE(vprnudlIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund()))
				|| (isNE(vprnudlIO.getUnitType(),utrsIO.getUnitType()))
				|| (isEQ(vprnudlIO.getStatuz(),varcom.endp))) {
					vprnudlIO.setFunction(varcom.nextp);
					lastPrice510();
				}
				return;
	}

	protected void lastPrice510()
	{
		/*If the transaction is Priced normally, read Price.*/
		SmartFileCode.execute(appVars, vprnudlIO);
		if (isNE(vprnudlIO.getStatuz(),varcom.oK)
		&& isNE(vprnudlIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
		}
		if ((isNE(vprnudlIO.getCompany(),ptsdclmIO.getChdrcoy()))
		|| (isNE(vprnudlIO.getUnitVirtualFund(),utrsIO.getUnitVirtualFund()))
		|| (isNE(vprnudlIO.getUnitType(),utrsIO.getUnitType()))
		|| (isEQ(vprnudlIO.getStatuz(),varcom.endp))) {
			vprnudlIO.setStatuz(varcom.endp);
		}
	}
	
	private void readHits600(){
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(ptsdclmIO.getChdrcoy());
		hitsIO.setChdrnum(ptsdclmIO.getChdrnum());
		hitsIO.setLife(ptsdclmIO.getLife());
		hitsIO.setCoverage(ptsdclmIO.getCoverage());
		hitsIO.setRider(ptsdclmIO.getRider());
		hitsIO.setPlanSuffix(ptsdclmIO.getPlanSuffix());
		hitsIO.setZintbfnd(ptsdclmIO.getVirtualFund());
		hitsIO.setFunction(varcom.readr);
		hitsIO.setFormat(hitsrec);
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(),varcom.oK)) {
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
		}
//		COMPUTE WSAA-NEW-SUMINS2    = HITS-ZCURPRMBAL + WSAA-NEW-SUMINS2;
		compute(wsaaNewSumins2, 2).set(add(hitsIO.getZcurprmbal(),wsaaNewSumins2));
	}

	private void processCovr1000(){
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
		}
		if (isNE(covrIO.getChdrcoy(),zrpwIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(),zrpwIO.getChdrnum())
		|| isNE(covrIO.getPlanSuffix(),zrpwIO.getPlanSuffix())
		|| isNE(covrIO.getCoverage(),zrpwIO.getCoverage())
		|| isNE(covrIO.getRider(),zrpwIO.getRider())
		|| isEQ(covrIO.getStatuz(),varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(covrIO.getValidflag(),"2")) {
			covrIO.setFunction(varcom.nextr);
			return ;
		}
		readLext1100();
		if (!lextFound.isTrue()) {
			readT56461200();
		}
		
		/* Update the old SUMINS with the new calculated SUMINS.*/
		while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
			compute(wsaaNewSumins1, 5).setRounded(sub(covrIO.getSumins(),(mult(wsaaWithdrawalAmt,wsaaPercRate))));
			if (isGT(wsaaNewSumins1,wsaaNewSumins2)) {
				covrIO.setSumins(wsaaNewSumins1);
			}
			else {
				covrIO.setSumins(wsaaNewSumins2);
			}
			covrIO.setFunction(varcom.rewrt);
			covrIO.setFormat(covrrec);
			SmartFileCode.execute(appVars, covrIO);
			
			if (isNE(covrIO.getStatuz(),varcom.oK)
					&& isNE(covrIO.getStatuz(),varcom.endp)) {
				drylogrec.statuz.set(covrIO.getStatuz());
				drylogrec.params.set(covrIO.getParams());
				a000FatalError();
					}
			/* There is a possibility that the UNITDEAL may be run after*/
			/* other transactions. Thus, we have to update the SUMINS of all*/
			/* the COVR records until the COVR-TRANNO = ZRPW-TRANNO.*/
			covrIO.setFunction(varcom.nextr);
			covrIO.setFormat(covrrec);
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(),varcom.oK)
					&& isNE(covrIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
				a000FatalError();
				}
			if (isLT(covrIO.getTranno(),zrpwIO.getTranno())
					|| isNE(covrIO.getChdrcoy(),zrpwIO.getChdrcoy())
					|| isNE(covrIO.getChdrnum(),zrpwIO.getChdrnum())
					|| isNE(covrIO.getRider(),zrpwIO.getRider())
					|| isNE(covrIO.getCoverage(),zrpwIO.getCoverage())
					|| isNE(covrIO.getPlanSuffix(),zrpwIO.getPlanSuffix())
					|| isEQ(covrIO.getStatuz(),varcom.endp)) {
						covrIO.setStatuz(varcom.endp);
					}
		}
	}

	private void readLext1100(){
		wsaaLextFound.set("N");
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(zrpwIO.getChdrcoy());
		lextIO.setChdrnum(zrpwIO.getChdrnum());
		lextIO.setLife(covrIO.getLife());
		lextIO.setRider(zrpwIO.getRider());
		lextIO.setCoverage(zrpwIO.getCoverage());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		lextIO.setFormat(lextrec);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
				&& isNE(lextIO.getStatuz(),varcom.endp)) {
			drylogrec.statuz.set(lextIO.getStatuz());
			drylogrec.params.set(lextIO.getParams());
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
				}
		
		if (isEQ(lextIO.getStatuz(),varcom.endp)
				|| isNE(lextIO.getChdrcoy(),zrpwIO.getChdrcoy())
				|| isNE(lextIO.getChdrnum(),zrpwIO.getChdrnum())
				|| isNE(lextIO.getRider(),zrpwIO.getRider())
				|| isNE(lextIO.getCoverage(),zrpwIO.getCoverage())
				|| isNE(lextIO.getLife(),covrIO.getLife())
				|| isEQ(lextIO.getZnadjperc(),0)) {
					lextIO.setStatuz(varcom.endp);
					return ;
				}
		wsaaLextFound.set("Y");
//		COMPUTE WSAA-PERC-RATE       = LEXT-ZNADJPERC / 100;
		compute(wsaaPercRate).set(div(lextIO.znadjperc,100));
	}

	private void readT56461200(){
		wsaaFactorFound.set("N");
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(dryprcrec.company);
		itdmIO.setItemtabl(t5646);
		itdmIO.setItemitem(covrIO.getCrtable());
		itdmIO.setItmfrm("19900101");
		itdmIO.setFunction(varcom.readr);
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
				&& isNE(itdmIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();		
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		
		t5646rec.t5646Rec.set(itdmIO.getGenarea());
		
		for (indx.set(1); !(isGT(indx,12)
				|| factorFound.isTrue()); indx.add(1)){
					if (isGTE(covrIO.getAnbAtCcd(), t5646rec.ageIssageFrm[indx.toInt()])
					&& isLTE(covrIO.getAnbAtCcd(), t5646rec.ageIssageTo[indx.toInt()])) {
						wsaaFactorFound.set("Y");
						wsaaPercRate.set(t5646rec.factorsa[indx.toInt()]);
					}
				}
	}

	
	
	
	@Override
	protected FixedLengthStringData getWsaaProg() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	private static final class DrypDryprcRecInner { 

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}
	
}
