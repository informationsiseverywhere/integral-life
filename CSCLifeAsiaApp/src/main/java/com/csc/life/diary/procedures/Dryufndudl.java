/*
 * File: Dryufndudl.java
 * Date: August 19, 2015 12:40:15 PM IST
 * Author: CSC
 *
 * Class transformed from DRYUFNDUDL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UfndudlTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufndudlkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the ACAGRNL Accumulation subroutine.
*
* It will update all Agent ACBL records.
*
****************************************************************** ****
*                                                                      *
* </pre>
*/
public class Dryufndudl extends SMARTCodeModel {

      private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("DRYACAGRNL");
      private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
       /* FORMATS */
      private String ufndudlrec = "UFNDUDLREC";

	  private UfndudlTableDAM ufndudlIO = new UfndudlTableDAM();
	  private PackedDecimalData wsaaNofunts = new PackedDecimalData(16, 5);
      private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
      private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
      private FixedLengthStringData lsaaDataArea = new FixedLengthStringData(1000);

      public Dryufndudl() {
       super();
      }

      /**
      * The mainline method is the default entry point of the program when called by other programs using the
      * Quipoz runtime framework.
      */
public void mainline(Object... parmArray)
      {
       lsaaDataArea = convertAndSetParam(lsaaDataArea, parmArray, 2);
       lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 1);
       lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
       try {
           mainLine100();
       }
       catch (COBOLExitProgramException e) {
       // Expected exception for control flow purposes
       }
      }

protected void mainLine100()
      {
       main110();
       exit190();
      }

protected void main110()
      {
       lsaaStatuz.set(Varcom.oK);
       ufndudlIO.setDataArea(lsaaDataArea);
       wsaaNofunts.set(ufndudlIO.nofunts);
       ufndudlIO.setFormat(ufndudlrec);
       ufndudlIO.setFunction(Varcom.readh);
       SmartFileCode.execute(appVars, ufndudlIO);

       if (isNE(ufndudlIO.getStatuz(), Varcom.oK)
       && isNE(ufndudlIO.getStatuz(), Varcom.mrnf)) {
           lsaaStatuz.set(ufndudlIO.getStatuz());
           exit190();
       }

       if (isEQ(ufndudlIO.getStatuz(), Varcom.oK)) {
		   ufndudlIO.nofunts.add(wsaaNofunts);
           ufndudlIO.setFunction(Varcom.rewrt);
       }else {
		   ufndudlIO.nofunts.set(wsaaNofunts);
           ufndudlIO.setFunction(Varcom.writr);

       }

       SmartFileCode.execute(appVars, ufndudlIO);
       if (isNE(ufndudlIO.getStatuz(), Varcom.oK)) {
           lsaaStatuz.set(ufndudlIO.getStatuz());
           exit190();
       }

      }

protected void exit190()
      {
       exitProgram();
      }
}