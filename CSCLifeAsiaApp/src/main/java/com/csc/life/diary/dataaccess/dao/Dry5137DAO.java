/**
 * 
 */
package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry5137Dto;

/**
 * DAO for Dry5137
 * 
 * @author aashish4
 * 
 */
public interface Dry5137DAO {
	/**
	 * Gets records for Actual AutoIncrese Process.
	 * 
	 * @param company   - Company Number
	 * @param effDate   - Effective Date
	 * @param chdrNo    - Contract Number
	 * @param validFlag - Valid Flag
	 * @return - List of ActualAutoIncrease records.
	 */
	public List<Dry5137Dto> getActualAutoIncrease(String company, String effDate, String chdrNo, String validFlag);
}
