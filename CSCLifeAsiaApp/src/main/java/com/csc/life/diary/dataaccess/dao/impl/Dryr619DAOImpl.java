package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dryr619DAO;
import com.csc.life.diary.dataaccess.model.Dryr619Dto;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation To Fetch Records For Anniversary Renewals Processing
 * 
 * @author ptrivedi8
 *
 */
public class Dryr619DAOImpl extends BaseDAOImpl<Dryr619Dto> implements Dryr619DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dryr619DAOImpl.class);

	/**
	 * Constructor
	 */
	public Dryr619DAOImpl() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see com.csc.life.diary.dataaccess.dao.Dryr619DAO#getCovrDetails(java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<Dryr619Dto> getCovrDetails(String company, String chdrNum, int effDate) {
		StringBuilder sql = new StringBuilder(
				"SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.CRTABLE, A.CRRCD,");
		sql.append(" A.STATCODE, A.CBCVIN, A.CURRFROM, A.INSTPREM, A.JLIFE, A.PCESTRM, A.PRMCUR, A.PSTATCODE,");
		sql.append(" A.RRTDAT, A.RCESTRM, A.SINGP, A.SUMINS, A.UNIQUE_NUMBER, A.VALIDFLAG, A.ZLINSTPREM, A.RCESDTE,");
		sql.append(" A.CBANPR FROM COVRPF A WHERE A.CHDRCOY = ? AND A.CHDRNUM = ? AND A.VALIDFLAG = ?");
		sql.append(" AND A.CBANPR <> ? AND A.CBANPR <= ? ");
		sql.append(" AND A.CBANPR < A.BCESDTE ORDER BY A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX");

		List<Dryr619Dto> dryr619DtoList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, company);
			ps.setString(2, chdrNum);
			ps.setString(3, "1");
			ps.setString(4, "0");
			ps.setInt(5, effDate);
			rs = ps.executeQuery();
			while (rs.next()) {
				Dryr619Dto dryr619Dto = new Dryr619Dto();
				dryr619Dto.setChdrCoy(rs.getString("CHDRCOY"));
				dryr619Dto.setChdrNum(rs.getString("CHDRNUM"));
				dryr619Dto.setLife(rs.getString("LIFE"));
				dryr619Dto.setCoverage(rs.getString("COVERAGE"));
				dryr619Dto.setRider(rs.getString("RIDER"));
				dryr619Dto.setPlanSuffix(rs.getInt("PLNSFX"));
				dryr619Dto.setCrTable(rs.getString("CRTABLE"));
				dryr619Dto.setCrrcd(rs.getInt("CRRCD"));
				dryr619Dto.setStatCode(rs.getString("STATCODE"));

				Covrpf covrpf = mapCovrDetails(rs);

				dryr619Dto.setCovrpf(covrpf);
				dryr619DtoList.add(dryr619Dto);
			}

		} catch (SQLException e) {
			LOGGER.error("Error occured when reading COVRPF data", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return dryr619DtoList;
	}

	
	private Covrpf mapCovrDetails(ResultSet rs) throws SQLException {
		Covrpf covrpf = new Covrpf();
		covrpf.setChdrcoy(rs.getString("CHDRCOY"));
		covrpf.setChdrnum(rs.getString("CHDRNUM"));
		covrpf.setLife(rs.getString("LIFE"));
		covrpf.setCoverage(rs.getString("COVERAGE"));
		covrpf.setRider(rs.getString("RIDER"));
		covrpf.setPlanSuffix(rs.getInt("PLNSFX"));
		covrpf.setCrtable(rs.getString("CRTABLE"));
		covrpf.setCrrcd(rs.getInt("CRRCD"));
		covrpf.setStatcode(rs.getString("STATCODE"));
		covrpf.setConvertInitialUnits(rs.getInt("CBCVIN"));
		covrpf.setCurrfrom(rs.getInt("CURRFROM"));
		covrpf.setInstprem(rs.getBigDecimal("INSTPREM"));
		covrpf.setJlife(rs.getString("JLIFE"));
		covrpf.setPremCessTerm(rs.getInt("PCESTRM"));
		covrpf.setPremCurrency(rs.getString("PRMCUR"));
		covrpf.setPstatcode(rs.getString("PSTATCODE"));
		covrpf.setRerateDate(rs.getInt("RRTDAT"));
		covrpf.setRiskCessTerm(rs.getInt("RCESTRM"));
		covrpf.setSingp(rs.getBigDecimal("SINGP"));
		covrpf.setSumins(rs.getBigDecimal("SUMINS"));
		covrpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
		covrpf.setValidflag(rs.getString("VALIDFLAG"));
		covrpf.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
		covrpf.setRiskCessDate(rs.getInt("RCESDTE"));
		covrpf.setAnnivProcDate(rs.getInt("CBANPR"));
		return covrpf;
	}

}
