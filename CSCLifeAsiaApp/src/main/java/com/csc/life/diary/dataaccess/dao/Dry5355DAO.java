/**
 * 
 */
package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry5355Dto;

/**
 * DAO for Dry5355
 * 
 * @author aashish4
 *
 */
public interface Dry5355DAO {

	/**
	 * Gets records for Overdue process
	 * 
	 * @param company - Company Number
	 * @param effDate - Effective Date
	 * @param chdrNo  - Contract Number
	 * @param ptdate  - Overdue Date
	 * @return - List of Overdue records
	 */
	public List<Dry5355Dto> getOverdue(String company, String effDate, String chdrNo, int ptdate);
}
