package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:50
 * Description:
 * Copybook name: R5361REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5361rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(439);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(dataArea, 0);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(4, 0).isAPartOf(dataArea, 1);
  	public ZonedDecimalData targfrom = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 5);
  	public ZonedDecimalData targto = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 13);
  	public ZonedDecimalData targetPremium = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 21);
  	public ZonedDecimalData premRecPer = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 38);
  	public ZonedDecimalData billedInPeriod = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 55);
  	public ZonedDecimalData overdueMin = new ZonedDecimalData(17, 2).isAPartOf(dataArea, 72);
  	public ZonedDecimalData annivProcDate = new ZonedDecimalData(8, 0).isAPartOf(dataArea, 89);
  	public FixedLengthStringData filler = new FixedLengthStringData(342).isAPartOf(dataArea, 97, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(477);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 439);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(sortKey, 447);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(sortKey, 449, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5361     ");


	public void initialize() {
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(planSuffix);
		COBOLFunctions.initialize(targfrom);
		COBOLFunctions.initialize(targto);
		COBOLFunctions.initialize(targetPremium);
		COBOLFunctions.initialize(premRecPer);
		COBOLFunctions.initialize(billedInPeriod);
		COBOLFunctions.initialize(overdueMin);
		COBOLFunctions.initialize(annivProcDate);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		life.isAPartOf(baseString, true);
    		planSuffix.isAPartOf(baseString, true);
    		targfrom.isAPartOf(baseString, true);
    		targto.isAPartOf(baseString, true);
    		targetPremium.isAPartOf(baseString, true);
    		premRecPer.isAPartOf(baseString, true);
    		billedInPeriod.isAPartOf(baseString, true);
    		overdueMin.isAPartOf(baseString, true);
    		annivProcDate.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}