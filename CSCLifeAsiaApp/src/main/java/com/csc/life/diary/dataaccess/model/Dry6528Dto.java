package com.csc.life.diary.dataaccess.model;

import java.math.BigDecimal;
import com.csc.life.diary.pojo.BenefitBilling;

/**
 * Dry6528Dto - POJO For Benefit Billing Processing
 * 
 * @author ptrivedi8
 *
 */
public class Dry6528Dto {

	/**
	 * Constructor
	 */
	public Dry6528Dto() {
		// Calling default constructor
	}

	private int benBillDate;
	private String chdrCoy;
	private String chdrNum;
	private String coverage;
	private int crrcd;
	private String crTable;
	private String jlife;
	private String life;
	private String mortcls;
	private int planSuffix;
	private int premCessDate;
	private String premCurrency;
	private String pstatCode;
	private String rider;
	private String statCode;
	private BigDecimal sumins;
	private BenefitBilling benefitBilling;

	/**
	 * @return the benBillDate
	 */
	public int getBenBillDate() {
		return benBillDate;
	}

	/**
	 * @param benBillDate
	 *            the benBillDate to set
	 */
	public void setBenBillDate(int benBillDate) {
		this.benBillDate = benBillDate;
	}

	/**
	 * @return the chdrCoy
	 */
	public String getChdrCoy() {
		return chdrCoy;
	}

	/**
	 * @param chdrCoy
	 *            the chdrCoy to set
	 */
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}

	/**
	 * @return the chdrNum
	 */
	public String getChdrNum() {
		return chdrNum;
	}

	/**
	 * @param chdrNum
	 *            the chdrNum to set
	 */
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * @param coverage
	 *            the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * @return the crrcd
	 */
	public int getCrrcd() {
		return crrcd;
	}

	/**
	 * @param crrcd
	 *            the crrcd to set
	 */
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	/**
	 * @return the crTable
	 */
	public String getCrTable() {
		return crTable;
	}

	/**
	 * @param crTable
	 *            the crTable to set
	 */
	public void setCrTable(String crTable) {
		this.crTable = crTable;
	}

	/**
	 * @return the jlife
	 */
	public String getJlife() {
		return jlife;
	}

	/**
	 * @param jlife
	 *            the jlife to set
	 */
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}

	/**
	 * @return the life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * @param life
	 *            the life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * @return the mortcls
	 */
	public String getMortcls() {
		return mortcls;
	}

	/**
	 * @param mortcls
	 *            the mortcls to set
	 */
	public void setMortcls(String mortcls) {
		this.mortcls = mortcls;
	}

	/**
	 * @return the planSuffix
	 */
	public int getPlanSuffix() {
		return planSuffix;
	}

	/**
	 * @param planSuffix
	 *            the planSuffix to set
	 */
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	/**
	 * @return the premCessDate
	 */
	public int getPremCessDate() {
		return premCessDate;
	}

	/**
	 * @param premCessDate
	 *            the premCessDate to set
	 */
	public void setPremCessDate(int premCessDate) {
		this.premCessDate = premCessDate;
	}

	/**
	 * @return the premCurrency
	 */
	public String getPremCurrency() {
		return premCurrency;
	}

	/**
	 * @param premCurrency
	 *            the premCurrency to set
	 */
	public void setPremCurrency(String premCurrency) {
		this.premCurrency = premCurrency;
	}

	/**
	 * @return the pstatCode
	 */
	public String getPstatCode() {
		return pstatCode;
	}

	/**
	 * @param pstatCode
	 *            the pstatCode to set
	 */
	public void setPstatCode(String pstatCode) {
		this.pstatCode = pstatCode;
	}

	/**
	 * @return the rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * @param rider
	 *            the rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * @return the statCode
	 */
	public String getStatCode() {
		return statCode;
	}

	/**
	 * @param statCode
	 *            the statCode to set
	 */
	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}

	/**
	 * @return the sumins
	 */
	public BigDecimal getSumins() {
		return sumins;
	}

	/**
	 * @param sumins
	 *            the sumins to set
	 */
	public void setSumins(BigDecimal sumins) {
		this.sumins = sumins;
	}

	/**
	 * @return the benefitBilling
	 */
	public BenefitBilling getBenefitBilling() {
		return benefitBilling;
	}

	/**
	 * @param benefitBilling
	 *            the benefitBilling to set
	 */
	public void setBenefitBilling(BenefitBilling benefitBilling) {
		this.benefitBilling = benefitBilling;
	}

}
