/*
 * File: Drybillin.java
 * Date: December 3, 2013 2:24:02 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYBILLIN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.List;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.BfrqpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Bfrqpf;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;

import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.model.Itempf;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* billing transactions.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Drybillin extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYBILLIN");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaAdjustedDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);
	private ZonedDecimalData wsaaT6654Leaddays = new ZonedDecimalData(3, 0);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String chdrmnarec = "CHDRMNAREC";
	
		/* TABLES */
	private static final String t6654 = "T6654";
	private static final String t5679 = "T5679";
	private static final String t5729 = "T5729";
	
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6654rec t6654rec = new T6654rec();
	private T5679rec t5679rec = new T5679rec();
	private T5729rec t5729rec = new T5729rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Varcom varcom = new Varcom();
	private Freqcpy freqcpy = new Freqcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private BfrqpfDAO bfrqpfDAO = getApplicationContext().getBean("bfrqpfDAO", BfrqpfDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private List<Bfrqpf> bfrqpfList;
	private Bfrqpf bfrqpf;
	private boolean isBfrqFound;//IBPTE-73
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	public Drybillin() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		/*MAIN*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		isBfrqFound = false;//IBPTE-73
		/* Determine if a billing transaction is needed.*/
		validate100();
		if (validStatus.isTrue()) {//true
			/* IBPTE-73 START */
			// This change is done to get billing trigger after APL
			bfrqCheck700();
			if (isBfrqFound) {//true..on billing date it is false
				drypDryprcRecInner.drypNxtprcdate.set(COBOLFunctions.add(drypDryprcRecInner.drypRunDate, 1));
			} else {
				getAdjustedDate400();
				drypDryprcRecInner.drypNxtprcdate.set(wsaaAdjustedDate);
			}
			/* IBPTE-73 END */
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		/*EXIT*/
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		checkT5729600();
		if (flexiblePremiumContract.isTrue()) {
			invalidStatus.setTrue();
			return ;
		}
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void getAdjustedDate400()
	{
		begn401();
	}

protected void begn401()
	{
		initialize(wsaaAdjustedDate);
		getChdr500();
		String key;
		if(BTPRO028Permission) {
			key = drypDryprcRecInner.drypBillchnl.toString().trim() + drypDryprcRecInner.drypCnttype.toString().trim() + chdrmnaIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = drypDryprcRecInner.drypBillchnl.toString().trim().concat(drypDryprcRecInner.drypCnttype.toString().trim()).concat("**").toString();
				if(!readT6654(key)) {
					key = drypDryprcRecInner.drypBillchnl.toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		wsaaT6654Billchnl.set(drypDryprcRecInner.drypBillchnl);
		wsaaT6654Cnttype.set(drypDryprcRecInner.drypCnttype);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6654Billchnl.set(drypDryprcRecInner.drypBillchnl);
			wsaaT6654Cnttype.set("***");
			itemIO.setStatuz(varcom.oK);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(t6654);
			itemIO.setItemitem(wsaaT6654Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		wsaaT6654Leaddays.set(t6654rec.leadDays);
		compute(wsaaT6654Leaddays, 0).set(mult(wsaaT6654Leaddays, -1));
		datcon2rec.freqFactor.set(wsaaT6654Leaddays);
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.intDate1.set(drypDryprcRecInner.drypBillcd);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(chdrmnaIO.getBillsupr(), "Y")) {
			if (isGTE(drypDryprcRecInner.drypBillcd, chdrmnaIO.getBillspfrom())
			&& isLT(drypDryprcRecInner.drypBillcd, chdrmnaIO.getBillspto())
			&& isLT(drypDryprcRecInner.drypRunDate, chdrmnaIO.getBillspto())) {
				/* If billing is suppressed then diary should trigger the next*/
				/* billing to be done the day after the billing suppression ends.*/
				wsaaAdjustedDate.set(chdrmnaIO.getBillspto());
			}
			else {
				wsaaAdjustedDate.set(datcon2rec.intDate2);
			}
		}
		else {
			wsaaAdjustedDate.set(datcon2rec.intDate2);
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	wsaaT6654Billchnl.set(drypDryprcRecInner.drypBillchnl);
	wsaaT6654Cnttype.set(drypDryprcRecInner.drypCnttype);
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}


protected void getChdr500()
	{
		/*START*/
		/* Read contract header record using CHDRMNA.*/
		chdrmnaIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrmnaIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrmnaIO.setFunction(varcom.readr);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrmnaIO.getStatuz());
			drylogrec.params.set(chdrmnaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/*EXIT*/
	}

protected void checkT5729600()
	{
		check610();
	}

protected void check610()
	{
		wsaaFlexPrem.set("N");
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(t5729);
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemitem(drypDryprcRecInner.drypCnttype.toString());
		itempf.setItmfrm(drypDryprcRecInner.drypOccdate.getbigdata());
		itempf.setItmto(drypDryprcRecInner.drypOccdate.getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				t5729rec.t5729Rec.set(StringUtil.rawToString(it.getGenarea()));	
				flexiblePremiumContract.setTrue();				
		}
		}
	}

protected void bfrqCheck700()
	{
		start710();
	}

protected void start710()
	{
		validStatus.setTrue();
		/* Check on the BFRQ file to see whether policy need to go through */
		/* the NFO APL Billing Freq change.                                */
		
		bfrqpf = new Bfrqpf();
		bfrqpf.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		bfrqpf.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		bfrqpf.setEffdate(drypDryprcRecInner.drypRunDate.toInt());
		
	
		bfrqpfList = bfrqpfDAO.searchBfrqpfRecord(bfrqpf);
		if(bfrqpfList!= null && bfrqpfList.size() > 0)
		{
			isBfrqFound = true; //IBPTE-73
		}
		
	}

/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
