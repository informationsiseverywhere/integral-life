package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import java.util.Map;
import com.csc.life.diary.dataaccess.model.Dryr613Dto;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;

/**
 * DAO for Dryr613
 * 
 * @author akash
 * 
 */
public interface Dryr613DAO {

	/**
	 * Gets records for ReRate Process.
	 * 
	 * @param company - Company Number
	 * @param effDate - Effective Date
	 * @param chdrNum - Contract Number
	 * @return - List of ReRate records.
	 */
	public List<Dryr613Dto> getReRate(String company, String effDate, String chdrNum);

	/**
	 * Reach COVR records for given contract numbers.
	 * 
	 * @param coy         - Company
	 * @param chdrnumList - List of contract numbers
	 * @return - Map of COVR records
	 */
	public Map<String, List<Covrpf>> searchCovrRecord(String coy, List<String> chdrnumList);

	/**
	 * Reach AGCM records for given contract numbers.
	 * 
	 * @param coy         - Company
	 * @param chdrnumList - List of contract numbers
	 * @return - Map of AGCM records
	 */
	public Map<String, List<Agcmpf>> getAgcmRecords(String coy, List<String> chdrnumList);

	/**
	 * Reach PCDDPF records for given contract numbers.
	 * 
	 * @param coy         - Company
	 * @param chdrnumList - List of contract numbers
	 * @return - Map of PCDDPF records
	 */
	public Map<String, List<Pcddpf>> getPcddpfRecords(String coy, List<String> chdrnum);

}
