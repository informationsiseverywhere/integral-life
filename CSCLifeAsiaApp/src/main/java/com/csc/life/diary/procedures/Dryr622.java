/*
 * File: Dryr622.java
 * Date: January 15, 2015 4:15:19 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYR622.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.Iterator;
import java.util.List;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.anticipatedendowment.dataaccess.ZraeTableDAM;
import com.csc.life.anticipatedendowment.procedures.Zrgetusr;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.anticipatedendowment.recordstructures.Zrgtusrrec;
import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
//import com.csc.life.diary.dataaccess.ZraedteTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.procedures.Crtloan;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.UsrxTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(C) Copyright CSC Corporation Limited 1986 - 1999.
 *    All rights reserved. CSC Confidential.
 *
 *                   Anticipated Endowment Release
 *                   -----------------------------
 * Overview
 * ________
 *
 * This program will perform all the processing for the selected
 * ZRAE contracts which are due for ant endow pay processing.
 * All contracts due for such processing are then processed
 * to have the coupon released. If the payment option is not
 * empty, the payment option will also be exercised.
 *
 *    Control totals maintained by this program are:
 *
 *      1. No of ZRAE rec read
 *      2. No of COVR invalid status
 *      3. No of ZRAE processed
 *      4. No of ZRAE blank payment option
 *      5. No of ZRAE payment made
 *
 *  Initialize
 *
 *  -  Read T5679 and T5645.
 *
 *  Process
 *
 *  -  Read ZRAEDTE to process anticipated endowment record.
 *
 *  -  Read and validate the COVR risk status and premium status
 *     against those obtained from T5679.
 *
 *  Update
 *
 *  - Read & hold the CHDR record using logical file CHDRLIF.
 *  - Calculate payment due.
 *  - Update contract header record with a new transtion number.
 *  - Update PTRN.
 *  - Create loan record.
 *  - Update ZRAE record with new next payment date.
 *
 *   Error Processing:
 *
 *     If a system error move the error code into the DRYL-STATUZ
 *     If a database error move the XXXX-PARAMS to DRYL-PARAMS.
 *     Perform the A000-FATAL-ERROR section.
 *
 ****************************************************************** ****
 *                                                                     *
 * </pre>
 */
public class Dryr622 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR622");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaPayIdx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCurrAmount = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaNextPaydate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaPayopt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPaymmeth = new FixedLengthStringData(1);
	private final int wsaaPayMax = 8;
	private PackedDecimalData wsaaPercent = new PackedDecimalData(5, 2);
	private ZonedDecimalData wsaaTransDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String usrxrec = "USRXREC";
	private static final String zraedterec = "ZRAEDTEREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String covrlnbrec = "COVRLNBREC";
	/* ERRORS */
	private static final String g450 = "G450";
	/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UsrxTableDAM usrxIO = new UsrxTableDAM();
	private ZraeTableDAM zraeIO = new ZraeTableDAM();
	//private ZraedteTableDAM zraedteIO = new ZraedteTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Subcoderec subcoderec = new Subcoderec();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();
	private Zrgtusrrec zrgtusrrec = new Zrgtusrrec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private T3629rec t3629rec = new T3629rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
	private Zraepf zraepf = new Zraepf(); 
	private ZraepfDAO zraepfDAO = getApplicationContext().getBean("zraepfDAO", ZraepfDAO.class);
	private List<Zraepf> zraepfList = null;
	private Iterator<Zraepf> zraepfIterator = null;

	/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
	public Dryr622() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		wsaaTransDate.set(getCobolDate());
		varcom.vrcmTime.set(getCobolTime());
		varcom.vrcmTermid.set("9999");
		initialise200();
		readZrae400();
		/*EXIT*/
		exitProgram();
	}

	protected void initialise200()
	{
		initialise210();
	}

	protected void initialise210()
	{
		/* Read T5679 for valid statii.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

	protected void readZrae400()
	{
		/*READ*/
		/*zraedteIO.setParams(SPACES);
		zraedteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		zraedteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		zraedteIO.setNextPaydate(ZERO);
		zraedteIO.setFormat(zraedterec);
		zraedteIO.setFunction(varcom.begn);
		while ( !(isEQ(zraedteIO.getStatuz(), varcom.endp))) {
			processZrae500();
		}*/
		/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
		zraepf.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		zraepf.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		zraepf.setNpaydate(0);
		zraepfList = zraepfDAO.getZraeData(zraepf);
		if(zraepfList==null || (zraepfList !=null && zraepfList.size()==0)){
			return;
		}
		zraepfIterator = zraepfList.iterator();
		while(zraepfIterator.hasNext()){
			processZrae500();
		}

		/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
		/*EXIT*/
	}

	protected void processZrae500()
	{
		/*process510();
	}

protected void process510()
	{*/
		/*SmartFileCode.execute(appVars, zraedteIO);
		if (isNE(zraedteIO.getStatuz(), varcom.oK)
		&& isNE(zraedteIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(zraedteIO.getStatuz());
			drylogrec.params.set(zraedteIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, zraedteIO.getChdrcoy())
		|| isNE(drypDryprcRecInner.drypEntity, zraedteIO.getChdrnum())
		|| isEQ(zraedteIO.getStatuz(), varcom.endp)
		|| isGT(zraedteIO.getNextPaydate(), drypDryprcRecInner.drypRunDate)) {
			zraedteIO.setStatuz(varcom.endp);
			return ;
		}*/
		/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
		zraepf = zraepfIterator.next();
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		processZrae2000();
		if (validCoverage.isTrue()) {
			updateZrae3000();
		}
		//zraedteIO.setFunction(varcom.nextr);
		/*ILIFE-3801  Improve the performance of Anticipated endownment release transaction*/
	}

	protected void processZrae2000()
	{
		start2000();
	}

	protected void start2000()
	{
		validateCovr2200();
		/*  No of COVRs with invalid statii*/
		if (!validCoverage.isTrue()) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		zraeIO.setDataKey(SPACES);
		zraeIO.setChdrcoy(zraepf.getChdrcoy());
		zraeIO.setChdrnum(zraepf.getChdrnum());
		zraeIO.setLife(zraepf.getLife());
		zraeIO.setCoverage(zraepf.getCoverage());
		zraeIO.setRider(zraepf.getRider());
		zraeIO.setPlanSuffix(zraepf.getPlnsfx());
		zraeIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(zraeIO.getParams());
			drylogrec.statuz.set(zraeIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void validateCovr2200()
	{
		start2200();
	}

	protected void start2200()
	{
		/* Read the coverage record and validate the status against*/
		/* those on T5679.*/
		/* Read in the relevant COVR record.*/
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setChdrcoy(zraepf.getChdrcoy());
		covrlnbIO.setChdrnum(zraepf.getChdrnum());
		covrlnbIO.setLife(zraepf.getLife());
		covrlnbIO.setCoverage(zraepf.getCoverage());
		covrlnbIO.setRider(zraepf.getRider());
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
				|| isNE(covrlnbIO.getValidflag(), "1")) {
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.statuz.set(covrlnbIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Check to see if coverage is of a valid status*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

	protected void updateZrae3000()
	{
		start3010();
	}

	protected void start3010()
	{
		readhChdrlif3100();
		calculatePaymentDue3200();
		updateContractHeader3300();
		writePtrn3400();
		createLoanRec3500();
		updateZraeRec3600();
		/* No of Payment Release Record processed*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/* Check the Cash Option attached to this Payments Due.*/
		/* If there is one then call that Cash Option sub-routine,*/
		/* otherwise skip this section.*/
		if (isEQ(wsaaPayopt, SPACES)) {
			/* No of Release Payment rec with empty option*/
			drycntrec.contotNumber.set(ct04);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		getBankcodeDetails3700();
		getContractDetails3710();
		getUserNumber3720();
		/* Setup the linkage variables before calling the Cash Option*/
		/* sub-routine.*/
		zrcshoprec.rec.set(SPACES);
		zrcshoprec.bankcode.set(t3629rec.bankcode);
		zrcshoprec.bankkey.set(zraeIO.getBankkey());
		zrcshoprec.bankacckey.set(zraeIO.getBankacckey());
		zrcshoprec.clntcoy.set(drypDryprcRecInner.drypFsuCompany);
		zrcshoprec.clntnum.set(zraeIO.getPayclt());
		zrcshoprec.paycurr.set(chdrlifIO.getCntcurr());
		zrcshoprec.cntcurr.set(chdrlifIO.getCntcurr());
		zrcshoprec.reqntype.set(wsaaPaymmeth);
		zrcshoprec.pymt.set(wsaaCurrAmount);
		zrcshoprec.tranref.set(zraeIO.getChdrnum());
		zrcshoprec.chdrnum.set(zraeIO.getChdrnum());
		zrcshoprec.chdrcoy.set(zraeIO.getChdrcoy());
		zrcshoprec.tranno.set(zraeIO.getTranno());
		zrcshoprec.effdate.set(drypDryprcRecInner.drypRunDate);
		zrcshoprec.language.set(drypDryprcRecInner.drypLanguage);
		zrcshoprec.trandesc.set(descIO.getLongdesc());
		zrcshoprec.user.set(zrgtusrrec.usernum);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(zraeIO.getChdrnum());
		wsaaRldgLoanno.set(crtloanrec.loanno);
		zrcshoprec.frmRldgacct.set(wsaaRldgacct);
		/* Use new procedure division copybook SUBCODE to perform the*/
		/* substitution in the GLCODE field.*/
		subcoderec.codeRec.set(SPACES);
		subcoderec.code[1].set(chdrlifIO.getCnttype());
		subcoderec.glcode.set(t5645rec.glmap03);
		subcode();
		zrcshoprec.glcode.set(subcoderec.glcode);
		zrcshoprec.sacscode.set(t5645rec.sacscode03);
		zrcshoprec.sacstyp.set(t5645rec.sacstype03);
		zrcshoprec.sign.set(t5645rec.sign03);
		zrcshoprec.cnttot.set(t5645rec.cnttot03);
		zrcshoprec.batcpfx.set(drypDryprcRecInner.drypBatcpfx);
		zrcshoprec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		zrcshoprec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		zrcshoprec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		zrcshoprec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		zrcshoprec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		zrcshoprec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		callProgram(wsaaPayopt, zrcshoprec.rec);
		if (isNE(zrcshoprec.statuz, varcom.oK)) {
			drylogrec.params.set(zrcshoprec.rec);
			drylogrec.statuz.set(zrcshoprec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* No of Release Payment made*/
		drycntrec.contotNumber.set(ct05);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	protected void readhChdrlif3100()
	{
		start3100();
	}

	protected void start3100()
	{
		/* Read and hold the CHDRLIF record.*/
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(zraepf.getChdrcoy());
		chdrlifIO.setChdrnum(zraepf.getChdrnum());
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void calculatePaymentDue3200()
	{
		start3210();
	}

	protected void start3210()
	{
		wsaaPayIdx.set(1);
		while ( !(isEQ(zraeIO.getZrduedte(wsaaPayIdx), zraeIO.getNextPaydate()))) {
			wsaaPayIdx.add(1);
		}

		if (isEQ(wsaaPayIdx, wsaaPayMax)) {
			wsaaNextPaydate.set(varcom.vrcmMaxDate);
		}
		else {
			compute(wsaaIdx, 0).set(add(wsaaPayIdx, 1));
			wsaaNextPaydate.set(zraeIO.getZrduedte(wsaaIdx));
			wsaaPayopt.set(zraeIO.getZrpayopt(wsaaIdx));
			wsaaPaymmeth.set(zraeIO.getPaymmeth(wsaaIdx));
		}
		wsaaPercent.set(zraeIO.getPrcnt(wsaaPayIdx));
		compute(wsaaCurrAmount, 2).set(div(mult(wsaaPercent, covrlnbIO.getSumins()), 100));
	}

	protected void updateContractHeader3300()
	{
		/*READ-CHDR*/
		/* Rewriting the current contract header record a new*/
		/* Transaction Number.*/
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

	protected void writePtrn3400()
	{
		start3400();
	}

	protected void start3400()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(drypDryprcRecInner.drypBatchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTransactionDate(wsaaTransDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(ZERO);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void createLoanRec3500()
	{
		start3510();
	}

	protected void start3510()
	{
		/* Get item description from T5645.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(zraeIO.getChdrcoy());
		descIO.setDesctabl(t5645);
		descIO.setDescitem(wsaaProg);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/* Set up linkage to CRTLOAN, which will write LOAN*/
		/* and ACMV records.*/
		crtloanrec.longdesc.set(descIO.getLongdesc());
		crtloanrec.chdrcoy.set(zraeIO.getChdrcoy());
		crtloanrec.chdrnum.set(zraeIO.getChdrnum());
		crtloanrec.cnttype.set(chdrlifIO.getCnttype());
		crtloanrec.loantype.set("E");
		crtloanrec.cntcurr.set(chdrlifIO.getCntcurr());
		crtloanrec.billcurr.set(chdrlifIO.getBillcurr());
		crtloanrec.tranno.set(chdrlifIO.getTranno());
		crtloanrec.occdate.set(chdrlifIO.getOccdate());
		crtloanrec.effdate.set(drypDryprcRecInner.drypRunDate);
		crtloanrec.authCode.set(drypDryprcRecInner.drypBatctrcde);
		crtloanrec.language.set(drypDryprcRecInner.drypLanguage);
		crtloanrec.outstamt.set(wsaaCurrAmount);
		crtloanrec.cbillamt.set(wsaaCurrAmount);
		crtloanrec.batchkey.set(drypDryprcRecInner.drypBatchKey);
		crtloanrec.sacscode01.set(t5645rec.sacscode01);
		crtloanrec.sacscode02.set(t5645rec.sacscode02);
		crtloanrec.sacstyp01.set(t5645rec.sacstype01);
		crtloanrec.sacstyp02.set(t5645rec.sacstype02);
		crtloanrec.glcode01.set(t5645rec.glmap01);
		crtloanrec.glcode02.set(t5645rec.glmap02);
		crtloanrec.glsign01.set(t5645rec.sign01);
		crtloanrec.glsign02.set(t5645rec.sign02);
		callProgram(Crtloan.class, crtloanrec.crtloanRec);
		if (isNE(crtloanrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(crtloanrec.statuz);
			drylogrec.params.set(crtloanrec.crtloanRec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void updateZraeRec3600()
	{
		rewriteOldZrae3610();
		writeNewZrae3625();
	}

	protected void rewriteOldZrae3610()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		zraeIO.setCurrto(datcon2rec.intDate2);
		zraeIO.setValidflag("2");
		zraeIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(zraeIO.getParams());
			drylogrec.statuz.set(zraeIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void writeNewZrae3625()
	{
		zraeIO.setNextPaydate(wsaaNextPaydate);
		zraeIO.setPaydte(wsaaPayIdx, drypDryprcRecInner.drypRunDate);
		zraeIO.setPaid(wsaaPayIdx, wsaaCurrAmount);
		setPrecision(zraeIO.getTotamnt(), 2);
		zraeIO.setTotamnt(add(zraeIO.getTotamnt(), wsaaCurrAmount));
		zraeIO.setTranno(chdrlifIO.getTranno());
		zraeIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		zraeIO.setCurrto(99999999);
		zraeIO.setValidflag("1");
		zraeIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zraeIO);
		if (isNE(zraeIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(zraeIO.getStatuz());
			drylogrec.params.set(zraeIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void getBankcodeDetails3700()
	{
		start3701();
	}

	protected void start3701()
	{
		/* Read table T3629 in order to obtain the bankcode*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(zraeIO.getChdrcoy());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(zraeIO.getPaycurr());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t3629rec.t3629Rec.set(itemIO.getGenarea());
		}
	}

	protected void getContractDetails3710()
	{
		start3711();
	}

	protected void start3711()
	{
		/* Ensure that the sacscode,type, GL account, sign and control*/
		/* all exist on T5645.*/
		if (isEQ(t5645rec.sacscode[1], SPACES)
				|| isEQ(t5645rec.sacstype[1], SPACES)
				|| isEQ(t5645rec.glmap[1], SPACES)
				|| isEQ(t5645rec.sign[1], SPACES)
				|| isEQ(t5645rec.cnttot[1], ZERO)) {
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(t5645);
			itemIO.setItemitem(wsaaProg);
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(g450);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Read T3695 to obtain the 'From' transaction description.*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(zraeIO.getChdrcoy());
		descIO.setDesctabl(t3695);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setDescitem(t5645rec.sacstype01);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(descIO.getParams());
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void getUserNumber3720()
	{
		start3721();
	}

	protected void start3721()
	{
		/* To get User Name by user number*/
		usrxIO.setParams(SPACES);
		usrxIO.setUsernum(drypDryprcRecInner.drypUser);
		usrxIO.setFunction(varcom.readr);
		usrxIO.setFormat(usrxrec);
		SmartFileCode.execute(appVars, usrxIO);
		if (isEQ(usrxIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(usrxIO.getStatuz());
			drylogrec.params.set(usrxIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Using ZRGETUSR sub-routine, get the User Number which is*/
		/* not available from the new batch programs.*/
		zrgtusrrec.rec.set(SPACES);
		zrgtusrrec.userid.set(usrxIO.getUserid());
		zrgtusrrec.function.set("USID");
		zrgtusrrec.usernum.set(0);
		callProgram(Zrgetusr.class, zrgtusrrec.rec);
		if (isNE(zrgtusrrec.statuz, varcom.oK)) {
			drylogrec.params.set(zrgtusrrec.rec);
			drylogrec.statuz.set(zrgtusrrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void subcode()
	{
		sbcd100Start();
	}

	protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner { 

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
	}
}
