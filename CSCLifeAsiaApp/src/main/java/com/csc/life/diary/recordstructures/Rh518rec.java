package com.csc.life.diary.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;

	 /*
		  *(C) Copyright CSC Corporation Limited 1986 - 2000.
		  *    All rights reserved. CSC Confidential.
		  *
		  *    This copybook is used to map the RH518 detail fields
		  *    onto the DRPT record for the Batch Diary System.
		  *
		  ***********************************************************************
		  *           AMENDMENT  HISTORY                                        *
		  ***********************************************************************
		  * DATE.... VSN/MOD  WORK UNIT    BY....                               *
		  *                                                                     *
		  *
		  ***********************************************************************
		  *                                                                     *
		  * ......... New Version of the Amendment History.                     *
		  *                                                                     *
		  ***********************************************************************
		  *           AMENDMENT  HISTORY                                        *
		  ***********************************************************************
		  * DATE.... VSN/MOD  WORK UNIT    BY....                               *
		  *                                                                     *
		  * 29/03/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
		  *           Initial Version.                                          *
		  *                                                                     *
		  **DD/MM/YY*************************************************************
		  *
		   03  RH518-DATA-AREA             REDEFINES
               DRYR-GENAREA.
               05  RH518-FUNDAMNT          PIC S9(15)V9(2).
               05  RH518-EFFDATE           PIC 9(8).
               05  RH518-FDBKIND           PIC X.
               05  FILLER                  PIC X(454).
           03  RH518-SORT-KEY.
               05  RH518-CHDRCOY           PIC X(01).
               05  RH518-ZINTBFND          PIC X(04).
               05  RH518-CHDRNUM           PIC X(08).
               05  RH518-BATCTRCDE         PIC X(04).
               05  FILLER                  PIC X(21).
           03  RH518-REPORT-NAME           PIC X(10)
                                           VALUE 'RH518     '.
		*/
	public class Rh518rec extends ExternalData {

		public FixedLengthStringData dataArea = new FixedLengthStringData(478);

		public ZonedDecimalData fundamnt = new ZonedDecimalData(15, 2).isAPartOf(dataArea, 0);

		public ZonedDecimalData effdate = new ZonedDecimalData(8).isAPartOf(dataArea, 15);

		public FixedLengthStringData fdbkind= new FixedLengthStringData(1).isAPartOf(dataArea, 23); 

		public FixedLengthStringData filler = new FixedLengthStringData(454).isAPartOf(dataArea, 24, FILLER_REDEFINE);

		public FixedLengthStringData sortKey = new FixedLengthStringData(38);

		public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(sortKey,0);

		public FixedLengthStringData zintbfnd = new FixedLengthStringData(4).isAPartOf(sortKey,1);

		public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey,5);
		
		public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(sortKey,13);

		public FixedLengthStringData filler1 = new FixedLengthStringData(21).isAPartOf(sortKey,17,FILLER_REDEFINE);

		public FixedLengthStringData reportName = new FixedLengthStringData(10).init("RH518");
		
		@Override
		public void initialize() {
			COBOLFunctions.initialize(fundamnt);
			COBOLFunctions.initialize(effdate);
			COBOLFunctions.initialize(fdbkind);
			COBOLFunctions.initialize(filler);
			COBOLFunctions.initialize(sortKey);
			COBOLFunctions.initialize(chdrcoy);
			COBOLFunctions.initialize(zintbfnd);
			COBOLFunctions.initialize(chdrnum);
			COBOLFunctions.initialize(batctrcde);
			COBOLFunctions.initialize(filler1);
			COBOLFunctions.initialize(reportName);
		}

		@Override
		public FixedLengthStringData getBaseString() {
			if (baseString == null) {
				baseString = new FixedLengthStringData(getLength());
				fundamnt.isAPartOf(baseString, true);
				effdate.isAPartOf(baseString, true);
				fdbkind.isAPartOf(baseString, true);
				filler.isAPartOf(baseString, true);
				sortKey.isAPartOf(baseString, true);
				chdrcoy.isAPartOf(baseString, true);
				zintbfnd.isAPartOf(baseString, true);
				chdrnum.isAPartOf(baseString, true);
				batctrcde.isAPartOf(baseString, true);
				filler1.isAPartOf(baseString, true);
				reportName.isAPartOf(baseString, true);
				baseString.resetIsAPartOfOffset();
			}
			return baseString;
		}
	}