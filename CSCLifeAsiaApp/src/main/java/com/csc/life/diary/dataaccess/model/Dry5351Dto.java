package com.csc.life.diary.dataaccess.model;

/**
 * Dry5351Dto - POJO For Revenue (Due-Date) Accounting
 * 
 * @author ptrivedi8
 *
 */
public class Dry5351Dto {

	private String chdrCoy;
	private String chdrNum;
	private int instFrom;
	private int payrSeqNo;

	/**
	 * Constructor
	 */
	public Dry5351Dto() {
		// Calling default constructor
	}

	/**
	 * @return the chdrCoy
	 */
	public String getChdrCoy() {
		return chdrCoy;
	}

	/**
	 * @param chdrCoy
	 *            the chdrCoy to set
	 */
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}

	/**
	 * @return the chdrNum
	 */
	public String getChdrNum() {
		return chdrNum;
	}

	/**
	 * @param chdrNum
	 *            the chdrNum to set
	 */
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	/**
	 * @return the instFrom
	 */
	public int getInstFrom() {
		return instFrom;
	}

	/**
	 * @param instFrom
	 *            the instFrom to set
	 */
	public void setInstFrom(int instFrom) {
		this.instFrom = instFrom;
	}

	/**
	 * @return the payrSeqNo
	 */
	public int getPayrSeqNo() {
		return payrSeqNo;
	}

	/**
	 * @param payrSeqNo
	 *            the payrSeqNo to set
	 */
	public void setPayrSeqNo(int payrSeqNo) {
		this.payrSeqNo = payrSeqNo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dry5351Dto [chdrCoy=" + chdrCoy + ", chdrNum=" + chdrNum + ", instFrom=" + instFrom + ", payrSeqNo="
				+ payrSeqNo + "]";
	}

}
