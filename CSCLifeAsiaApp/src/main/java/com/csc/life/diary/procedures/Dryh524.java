/*
 * File: Dryh524.java
 * Date: January 15, 2015 4:15:04 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYH524.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.LinkedList;
import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.Dryh524TempDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Dryh524DTO;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Conlog;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Conlogrec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
*
*
*REMARKS
*
*   This diary program process the HDIS based on the Next
*   Capitalisation Date(HCAPNDT).
*   When Dividend Interest is capitalisated, these files will be
*   affected,
*        HDIS - dividend transaction summary
*        HDIV - dividend transaction details
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Pre-load constantly referenced Tables T5645. Fetch CHDR
*    record, read T5688, T1688.
*  - Check if contract valid.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read HDISDRY.
*  - Valid converage status.
*
*  Update.
*
*  - Read HCSD(Cash dividend details)
*  - Read TH501 with cash dividend method in HCSD and effective
*    date = Next Capitalisation Date
*  - Read all HDIVINT via the keys from HDISDRY, and accumulate
*    the dividend amount into a working field.
*  - check if the total interest accumulated equals to the O/S
*    interest to be capitalised, if not skip processing on this
*    HDIS and log a message
*  - Read and hold the CHDRLIF(Contract header)
*  - Set NEW-TRANNO to CHDRLIF-TRANNO + 1
*  - Update related HDIV records, setting capitalised TRANNO to
*    NEW-TRANNO
*  - Invalid the current HDIS(Dividend & Interest Summary) and
*    write a new one with its O/S interest accumulated to its
*    dividend balance since last capitalisation date.  Re-set the
*    O/S interest value, set next cap date to last cap date and
*    calculate the new next cap date with the CAP DUE MM & DD and
*    TH501-FREQ for capitalisation using DATCON4
*  - if capitalisation amount is not zero, write accounting entrie ,
*         look for the posting level in T5688 for CNTTYPE
*         set up fields in LIFACMV linkage
*         map for the relevant GL details in T5645
*         call LIFACMV twice to post the dividend expense and
*         its opposite entry for payable.
* </pre>
*/
public class Dryh524 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaConlMsg = new FixedLengthStringData(71);
	private FixedLengthStringData wsaaHdisdryKey = new FixedLengthStringData(20).isAPartOf(wsaaConlMsg, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(wsaaConlMsg, 20, FILLER).init("Total interest to capitalise does not reconcile O/S");

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaCapInt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotDividend = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(4, 0).init(ZERO);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t5679 = "T5679";
		/* ERRORS */
	private static final String h791 = "H791";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
	//private HdisdryTableDAM hdisdryIO = new HdisdryTableDAM();
	private Dryh524TempDAO dryh524Dao = getApplicationContext().getBean("dryh524TempDAO",Dryh524TempDAO.class);
	private Dryh524DTO dryh524Dto ;
	private List<Dryh524DTO> dryh524List;
	//private HdivTableDAM hdivIO = new HdivTableDAM();
	//private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	//private HdivintTableDAM hdivintIO = new HdivintTableDAM();
	private HdivpfDAO hdivpfDao = getApplicationContext().getBean("hdivpfDAO",HdivpfDAO.class);
	private List<Hdivpf> hdivpfList;
	private List<Hdivpf> hdivcshList;
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Varcom varcom = new Varcom();
	private Conlogrec conlogrec = new Conlogrec();
	private Th501rec th501rec = new Th501rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T5679rec t5679rec = new T5679rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	//ILIFE-3790 by dpuhawan
	private String username;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next2080, 
		exit2090
	}

	public Dryh524() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		start110();
	}

protected void start110()
	{
		username = (String) ThreadLocalStore.get(ThreadLocalStore.USER);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		wsaaSystemDate.set(getCobolDate());
		initialise1000();
		/*  Check if contract valid.*/
		validateContract1500();
		if (!validContract.isTrue()) {
			return ;
		}
		/*  Set up the initial key for looping through the HDISDRY*/
		/*  records for the contract.*/
		/*hdisdryIO.setDataKey(SPACES);
		hdisdryIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hdisdryIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hdisdryIO.setLife(SPACES);
		hdisdryIO.setCoverage(SPACES);
		hdisdryIO.setRider(SPACES);
		hdisdryIO.setPlanSuffix(ZERO);
		hdisdryIO.setTranno(ZERO);
		hdisdryIO.setFormat(formatsInner.hdisdryrec);
		hdisdryIO.setFunction(varcom.begn);
		while ( !(isEQ(hdisdryIO.getStatuz(), varcom.endp))) {
			loopHdis2000();
		}*/
		dryh524List = dryh524Dao.getHdisHcsdRecords(drypDryprcRecInner.drypCompany.toString(), drypDryprcRecInner.drypEntity.toString());
		if(dryh524List==null || dryh524List.size()<1) {
			return;
		}
		dryh524Dto = new Dryh524DTO();
		for(Dryh524DTO dryh524:dryh524List) {
			dryh524Dto = dryh524;
			loopHdis2000();
		}
		
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*  Read T5645, load all account rules.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.begn);
		iy.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT5645300();
		}
		
		/*  Fetch the CHDR record for the entity parameters passed.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Read T5688, load contract processing rules.*/
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isLT(itdmIO.getItmto(), chdrlifIO.getOccdate())) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/*    Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.threadNumber.set(drypDryprcRecInner.drypThreadNumber);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(drypDryprcRecInner.drypLanguage);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}

protected void loadT5645300()
	{
		start310();
	}

protected void start310()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itemIO.getItemtabl(), t5645)
		|| isNE(itemIO.getItemitem(), wsaaProg)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		if (isGT(iy, wsaaT5645Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t5645);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		ix.set(1);
		while ( !(isGT(ix, 15))) {
			wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[ix.toInt()]);
			wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[ix.toInt()]);
			wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[ix.toInt()]);
			wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[ix.toInt()]);
			wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[ix.toInt()]);
			ix.add(1);
			iy.add(1);
		}
		
		itemIO.setFunction(varcom.nextr);
	}

protected void validateContract1500()
	{
		start1510();
	}

protected void start1510()
	{
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidContract.set("N");
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIx.toInt()], chdrlifIO.getStatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
		if (!validContract.isTrue()) {
			return ;
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnPremStat[wsaaIx.toInt()], chdrlifIO.getPstatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
	}

protected void loopHdis2000()
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: */
					begin2010();
				/*case next2080: 
					next2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
	}

protected void begin2010()
	{
		/*SmartFileCode.execute(appVars, hdisdryIO);
		if (isNE(hdisdryIO.getStatuz(), varcom.oK)
		&& isNE(hdisdryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hdisdryIO.getStatuz());
			drylogrec.params.set(hdisdryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(hdisdryIO.getStatuz(), varcom.endp)
		|| isNE(hdisdryIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(hdisdryIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			hdisdryIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}*/
		/*if (isGT(hdisdryIO.getNextCapDate(), hdisdryIO.getNextIntDate())
		|| isGT(hdisdryIO.getNextCapDate(), drypDryprcRecInner.drypRunDate)
		|| isEQ(hdisdryIO.getTranno(), wsaaNewTranno)) {
			goTo(GotoLabel.next2080);
		}*/
		if (isGT(dryh524Dto.getHcapndt(), dryh524Dto.getHintndt())
			|| isGT(dryh524Dto.getHcapndt(), drypDryprcRecInner.drypRunDate)
			|| isEQ(dryh524Dto.getTranno(), wsaaNewTranno)) {
				return;
		}
		/*  Valid coverage status.*/
		validateCoverage2200();
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.next2080);
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/*    Skip processing this HDIS if its O/S interest to capitalise*/
		/*    is zero*/
		//if (isEQ(hdisdryIO.getOsInterest(), 0)) {
		if (isEQ(dryh524Dto.getHintos(), 0)) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return;
		}
		update3000();
	}

/*protected void next2080()
	{
		hdisdryIO.setFunction(varcom.nextr);
	}
*/
protected void validateCoverage2200()
	{
		begin2210();
	}

protected void begin2210()
	{
		readCovr2300();
		/* Validate the coverage risk strtus against T5679.*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				wsaaT5679Sub.set(13);
				wsaaValidCoverage.set("Y");
			}
		}
		if (!validCoverage.isTrue()) {
			return ;
		}
		/* Validate the prem status against T5679.*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
				wsaaT5679Sub.set(13);
				wsaaValidCoverage.set("Y");
			}
		}
	}

protected void readCovr2300()
	{
		begin2310();
	}

protected void begin2310()
	{
		covrlnbIO.setParams(SPACES);
		/*covrlnbIO.setChdrcoy(hdisdryIO.getChdrcoy());
		covrlnbIO.setChdrnum(hdisdryIO.getChdrnum());
		covrlnbIO.setLife(hdisdryIO.getLife());
		covrlnbIO.setCoverage(hdisdryIO.getCoverage());
		covrlnbIO.setRider(hdisdryIO.getRider());
		covrlnbIO.setPlanSuffix(hdisdryIO.getPlanSuffix());*/
		covrlnbIO.setChdrcoy(dryh524Dto.getChdrcoy());
		covrlnbIO.setChdrnum(dryh524Dto.getChdrnum());
		covrlnbIO.setLife(dryh524Dto.getLife());
		covrlnbIO.setCoverage(dryh524Dto.getCoverage());
		covrlnbIO.setRider(dryh524Dto.getRider());
		covrlnbIO.setPlanSuffix(dryh524Dto.getPlnsfx());
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrlnbIO.getStatuz());
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/*hcsdIO.setChdrcoy(hdisdryIO.getChdrcoy());
		hcsdIO.setChdrnum(hdisdryIO.getChdrnum());
		hcsdIO.setLife(hdisdryIO.getLife());
		hcsdIO.setCoverage(hdisdryIO.getCoverage());
		hcsdIO.setRider(hdisdryIO.getRider());
		hcsdIO.setPlanSuffix(hdisdryIO.getPlanSuffix());
		hcsdIO.setFunction(varcom.readr);
		readHcsd5120();*/
		/*    Match for the TH501 item with the Cash Dividend method held*/
		/*    on HCSD and Next Capitalisation Date from array.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		//itdmIO.setItmfrm(hdisdryIO.getNextCapDate());
		itdmIO.setItmfrm(dryh524Dto.getHcapndt());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(dryh524Dto.getZcshdivmth());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), th501)
		|| isNE(itdmIO.getItemitem(), dryh524Dto.getZcshdivmth())) {
			itdmIO.setStatuz(varcom.endp);
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
		/*    Initialise the accumulate field for Interest to capitalise*/
		wsaaCapInt.set(0);
		/*    Read through all related HDIVs to accumulate for interest*/
		/*    amount to be capitalised.*/
		/*hdivintIO.setChdrcoy(hdisdryIO.getChdrcoy());
		hdivintIO.setChdrnum(hdisdryIO.getChdrnum());
		hdivintIO.setLife(hdisdryIO.getLife());
		hdivintIO.setCoverage(hdisdryIO.getCoverage());
		hdivintIO.setRider(hdisdryIO.getRider());
		hdivintIO.setPlanSuffix(hdisdryIO.getPlanSuffix());
		hdivintIO.setDivdIntCapDate(hdisdryIO.getNextCapDate());
		hdivintIO.setFunction(varcom.begn);
		procHdivint5140();
		if (isNE(hdivintIO.getChdrcoy(), hdisdryIO.getChdrcoy())
		|| isNE(hdivintIO.getChdrnum(), hdisdryIO.getChdrnum())
		|| isNE(hdivintIO.getLife(), hdisdryIO.getLife())
		|| isNE(hdivintIO.getCoverage(), hdisdryIO.getCoverage())
		|| isNE(hdivintIO.getRider(), hdisdryIO.getRider())
		|| isNE(hdivintIO.getPlanSuffix(), hdisdryIO.getPlanSuffix())
		|| isNE(hdivintIO.getDivdIntCapDate(), hdisdryIO.getNextCapDate())) {
			hdivintIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivintIO.getStatuz(), varcom.endp))) {
			wsaaCapInt.add(hdivintIO.getDivdAmount());
			hdivintIO.setFunction(varcom.nextr);
			procHdivint5140();
			if (isNE(hdivintIO.getChdrcoy(), hdisdryIO.getChdrcoy())
			|| isNE(hdivintIO.getChdrnum(), hdisdryIO.getChdrnum())
			|| isNE(hdivintIO.getLife(), hdisdryIO.getLife())
			|| isNE(hdivintIO.getCoverage(), hdisdryIO.getCoverage())
			|| isNE(hdivintIO.getRider(), hdisdryIO.getRider())
			|| isNE(hdivintIO.getPlanSuffix(), hdisdryIO.getPlanSuffix())
			|| isNE(hdivintIO.getDivdIntCapDate(), hdisdryIO.getNextCapDate())) {
				hdivintIO.setStatuz(varcom.endp);
			}
		}*/
		Hdivpf hdivpf = new Hdivpf();
		hdivpf.setChdrcoy(dryh524Dto.getChdrcoy());
		hdivpf.setChdrnum(dryh524Dto.getChdrnum());
		hdivpf.setLife(dryh524Dto.getLife());
		hdivpf.setCoverage(dryh524Dto.getCoverage());
		hdivpf.setRider(dryh524Dto.getRider());
		hdivpf.setPlnsfx(dryh524Dto.getPlnsfx());
		hdivpf.setHincapdt(dryh524Dto.getHcapndt());
		//ILIFE-3790 start by dpuhawan
		//hdivpfList = hdivpfDao.searchHdivpfRecord(hdivpf);
		hdivpfList = hdivpfDao.searchHdivintRecord(hdivpf);
		//ILIFE-3790 end
		if(hdivpfList.size()>0) {
			for(Hdivpf hdiv:hdivpfList) {
				wsaaCapInt.add(hdiv.getHdvamt().doubleValue());
		}
		}
		/*    If accumulated interest value do not reconcile to O/S*/
		/*    interest to be capitalised, skip this HDCX record*/
		//if (isNE(wsaaCapInt, hdisdryIO.getOsInterest())) {
		if (isNE(wsaaCapInt, dryh524Dto.getHintos())) {
			drycntrec.contotNumber.set(ct04);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsaaConlMsg.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			/*stringVariable1.addExpression(hdisdryIO.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hdisdryIO.getChdrnum());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hdisdryIO.getLife());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hdisdryIO.getCoverage());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(hdisdryIO.getRider());*/
			stringVariable1.addExpression(dryh524Dto.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dryh524Dto.getChdrnum());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dryh524Dto.getLife());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dryh524Dto.getCoverage());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dryh524Dto.getRider());
			stringVariable1.setStringInto(wsaaHdisdryKey);
			conlogrec.message.set(wsaaConlMsg);
			callProgram(Conlog.class, conlogrec.conlogRec);
			return ;
		}
		/*chdrlifIO.setChdrcoy(hdisdryIO.getChdrcoy());
		chdrlifIO.setChdrnum(hdisdryIO.getChdrnum());*/
		chdrlifIO.setChdrcoy(dryh524Dto.getChdrcoy());
		chdrlifIO.setChdrnum(dryh524Dto.getChdrnum());
		chdrlifIO.setFunction(varcom.readh);
		procChdrlif5100();
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		/*    Update HDIV records to denote their interest has been*/
		/*    capitalised.*/
		//hdivintIO.setStatuz(varcom.oK);
		/*while ( !(isEQ(hdivintIO.getStatuz(), varcom.mrnf))) {
			hdivintIO.setChdrcoy(hdisdryIO.getChdrcoy());
			hdivintIO.setChdrnum(hdisdryIO.getChdrnum());
			hdivintIO.setLife(hdisdryIO.getLife());
			hdivintIO.setCoverage(hdisdryIO.getCoverage());
			hdivintIO.setRider(hdisdryIO.getRider());
			hdivintIO.setPlanSuffix(hdisdryIO.getPlanSuffix());
			hdivintIO.setDivdIntCapDate(hdisdryIO.getNextCapDate());
			hdivintIO.setFunction(varcom.readh);
			procHdivint5140();
			if (isNE(hdivintIO.getStatuz(), varcom.mrnf)) {
				hdivintIO.setDivdCapTranno(wsaaNewTranno);
				hdivintIO.setFunction(varcom.rewrt);
				procHdivint5140();
			}
		}*/
		if(hdivpfList.size()>0) {
			//ILIFE-3790 Start by dpuhawan
			List<Hdivpf> hdivpfUpdateList;		
			hdivpfUpdateList =  new LinkedList<Hdivpf>(); 
			//ILIFE-3790 end
			for(Hdivpf hdiv:hdivpfList) {
				//ILIFE-3790 start by dpuhawan
				//hdiv.setUsrprf(drypDryprcRecInner.drypUser.toString());
				//hdiv.setJobnm(drypDryprcRecInner.drypUser.toString());
				hdiv.setUsrprf(username);
				hdiv.setJobnm(username);
				hdiv.setTranno(wsaaNewTranno.toInt());
				hdivpfUpdateList.add(hdiv);
		}
			//hdivpfDao.bulkUpdateHdivRecords(hdivpfList);  
			hdivpfDao.bulkUpdateHdivRecords(hdivpfUpdateList);
			//ILIFE-3790 end
		}
		/*    Update current HDIS to history and write a new one.*/
		/*hdisIO.setChdrcoy(hdisdryIO.getChdrcoy());
		hdisIO.setChdrnum(hdisdryIO.getChdrnum());
		hdisIO.setLife(hdisdryIO.getLife());
		hdisIO.setCoverage(hdisdryIO.getCoverage());
		hdisIO.setRider(hdisdryIO.getRider());
		hdisIO.setPlanSuffix(hdisdryIO.getPlanSuffix());*/
		hdisIO.setChdrcoy(dryh524Dto.getChdrcoy());
		hdisIO.setChdrnum(dryh524Dto.getChdrnum());
		hdisIO.setLife(dryh524Dto.getLife());
		hdisIO.setCoverage(dryh524Dto.getCoverage());
		hdisIO.setRider(dryh524Dto.getRider());
		hdisIO.setPlanSuffix(dryh524Dto.getPlnsfx());
		hdisIO.setFunction(varcom.readh);
		procHdis5130();
		/*    Read through all related HDIVs to accumulate for withdrawn*/
		/*    dividend since last capitalisation date*/
		wsaaTotDividend.set(ZERO);
		/*hdivcshIO.setChdrcoy(hdisdryIO.getChdrcoy());
		hdivcshIO.setChdrnum(hdisdryIO.getChdrnum());
		hdivcshIO.setLife(hdisdryIO.getLife());
		hdivcshIO.setCoverage(hdisdryIO.getCoverage());
		hdivcshIO.setRider(hdisdryIO.getRider());
		hdivcshIO.setPlanSuffix(hdisdryIO.getPlanSuffix());
		compute(wsaaLastCapDate, 0).set(add(1, hdisdryIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFunction(varcom.begn);
		procHdivcsh5150();
		if (isNE(hdivcshIO.getChdrcoy(), hdisdryIO.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(), hdisdryIO.getChdrnum())
		|| isNE(hdivcshIO.getLife(), hdisdryIO.getLife())
		|| isNE(hdivcshIO.getCoverage(), hdisdryIO.getCoverage())
		|| isNE(hdivcshIO.getRider(), hdisdryIO.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(), hdisdryIO.getPlanSuffix())) {
			hdivcshIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(hdivcshIO.getStatuz(), varcom.endp))) {
			wsaaTotDividend.add(hdivcshIO.getDivdAmount());
			hdivcshIO.setFunction(varcom.nextr);
			procHdivcsh5150();
			if (isNE(hdivcshIO.getChdrcoy(), hdisdryIO.getChdrcoy())
			|| isNE(hdivcshIO.getChdrnum(), hdisdryIO.getChdrnum())
			|| isNE(hdivcshIO.getLife(), hdisdryIO.getLife())
			|| isNE(hdivcshIO.getCoverage(), hdisdryIO.getCoverage())
			|| isNE(hdivcshIO.getRider(), hdisdryIO.getRider())
			|| isNE(hdivcshIO.getPlanSuffix(), hdisdryIO.getPlanSuffix())) {
				hdivcshIO.setStatuz(varcom.endp);
			}
		}*/
		hdivpf.setChdrcoy(dryh524Dto.getChdrcoy());
		hdivpf.setChdrnum(dryh524Dto.getChdrnum());
		hdivpf.setLife(dryh524Dto.getLife());
		hdivpf.setCoverage(dryh524Dto.getCoverage());
		hdivpf.setRider(dryh524Dto.getRider());
		hdivpf.setPlnsfx(dryh524Dto.getPlnsfx());
		compute(wsaaLastCapDate, 0).set(add(1, dryh524Dto.getHcapndt()));
		hdivpf.setHincapdt(wsaaLastCapDate.toInt());
		hdivcshList = hdivpfDao.getHdivcshRecords(hdivpf);
		if(hdivcshList!=null && hdivcshList.size()>0) {
			for(Hdivpf hdiv:hdivcshList) {
				wsaaTotDividend.add(hdiv.getHdvamt().doubleValue());
			}
		}
		
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		procHdis5130();
		hdisIO.setValidflag("1");
		hdisIO.setTranno(wsaaNewTranno);
		setPrecision(hdisIO.getBalSinceLastCap(), 2);
		hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), hdisIO.getOsInterest()));
		setPrecision(hdisIO.getBalSinceLastCap(), 2);
		hdisIO.setBalSinceLastCap(add(hdisIO.getBalSinceLastCap(), wsaaTotDividend));
		hdisIO.setOsInterest(0);
		hdisIO.setLastCapDate(hdisIO.getNextCapDate());
		/*  If next capitalisation date equals the cessation date of the*/
		/*  component, no need to find the next capitalisation date as*/
		/*  this should be the last time.*/
		if (isNE(hdisIO.getNextCapDate(), covrlnbIO.getRiskCessDate())) {
			datcon4rec.intDate1.set(hdisIO.getNextCapDate());
			wsaaHdisDate.set(hdisIO.getNextCapDate());
			datcon4rec.frequency.set(th501rec.freqcy01);
			datcon4rec.freqFactor.set(1);
			datcon4rec.billmonth.set(hdisIO.getCapDueMm());
			datcon4rec.billday.set(wsaaHdisDd);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			/*  If next capitalisation date passes the risk cessation date,*/
			/*  set it to risk cessation date, this is for maturity handling*/
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				drylogrec.statuz.set(datcon3rec.statuz);
				drylogrec.params.set(datcon3rec.datcon3Rec);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			if (isGT(datcon4rec.intDate2, covrlnbIO.getRiskCessDate())) {
				hdisIO.setNextCapDate(covrlnbIO.getRiskCessDate());
			}
			else {
				hdisIO.setNextCapDate(datcon4rec.intDate2);
			}
		}
		hdisIO.setFunction(varcom.writr);
		procHdis5130();
		/*    Now write accounting records for the capitalised interest if*/
		/*    it is not zero.*/
		if (isNE(wsaaCapInt, 0)) {
			writeAccounting3200();
		}
		/*    Update existing CHDR as history and write a new one.*/
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setFunction(varcom.rewrt);
		procChdrlif5100();
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.writr);
		procChdrlif5100();
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		/*ptrnIO.setChdrcoy(hdisdryIO.getChdrcoy());
		ptrnIO.setChdrnum(hdisdryIO.getChdrnum());*/
		ptrnIO.setChdrcoy(dryh524Dto.getChdrcoy());
		ptrnIO.setChdrnum(dryh524Dto.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setValidflag("1");
		//ptrnIO.setPtrneff(hdisdryIO.getNextCapDate());
		ptrnIO.setPtrneff(dryh524Dto.getHcapndt());
		ptrnIO.setTermid(SPACES);
		ptrnIO.setTransactionDate(wsaaSysDate);
		ptrnIO.setTransactionTime(varcom.vrcmTimen);
		ptrnIO.setUser(999999);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writeAccounting3200()
	{
		accounting3210();
	}

protected void accounting3210()
	{
		/*    Determine contract or component level accounting in T5688*/
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaCapInt);
		/*    Post against expenses.*/
		wsaaJrnseq.add(1);
		if (!componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(SPACES);
			/*wsaaPlan.set(hdisdryIO.getPlanSuffix());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hdisdryIO.getChdrnum());
			stringVariable1.addExpression(hdisdryIO.getLife());
			stringVariable1.addExpression(hdisdryIO.getCoverage());
			stringVariable1.addExpression(hdisdryIO.getRider());*/
			wsaaPlan.set(dryh524Dto.getPlnsfx());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(dryh524Dto.getChdrnum());
			stringVariable1.addExpression(dryh524Dto.getLife());
			stringVariable1.addExpression(dryh524Dto.getCoverage());
			stringVariable1.addExpression(dryh524Dto.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			//lifacmvrec.rldgacct.set(hdisdryIO.getChdrnum());
			lifacmvrec.rldgacct.set(dryh524Dto.getChdrnum());
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		x1000CallLifacmv();
		/*    Post opposite entry in payable.*/
		wsaaJrnseq.add(1);
		t5645Ix.set(2);
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		/*    If subaccount code and type of HDIS are not blank, set them*/
		/*    in writing ACMV*/
		/* Note:  subaccount code and type in HDIS is to denote the sub-*/
		/*        account balance when dividend paid future premium is*/
		/*        the dividend processing option, from which premium is*/
		/*        collected during billing.  The code and type is set*/
		/*        in HDOPPMST during ACMV posting.  That is on billing,*/
		/*        if an ACBL is detected for the contract with these code*/
		/*        and type, say LP DS, premium settlement dividend option*/
		/*        is assumed and premium will be called from it.*/
		if (isNE(hdisIO.getSacscode(), SPACES)
		&& isNE(hdisIO.getSacstyp(), SPACES)) {
			lifacmvrec.sacscode.set(hdisIO.getSacscode());
			lifacmvrec.sacstyp.set(hdisIO.getSacstyp());
		}
		lifacmvrec.substituteCode[6].set(SPACES);
		//lifacmvrec.rldgacct.set(hdisdryIO.getChdrnum());
		lifacmvrec.rldgacct.set(dryh524Dto.getChdrnum());
		x1000CallLifacmv();
		/*    Count no.of HDCXPF records processed in CT04 and total*/
		/*    capitalised interest amount in CT06*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct05);
		drycntrec.contotValue.set(wsaaCapInt);
		d000ControlTotals();
	}

protected void procChdrlif5100()
	{
		/*CHDR*/
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getParams());
			drylogrec.params.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void readHcsd5120()
	{
		/*HCSD*/
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hcsdIO.getParams());
			drylogrec.params.set(hcsdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void procHdis5130()
	{
		/*HDIS*/
		hdisIO.setFormat(formatsInner.hdisrec);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hdisIO.getParams());
			drylogrec.params.set(hdisIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

/*protected void procHdivint5140()
	{
		HDIVINT
		hdivintIO.setFormat(formatsInner.hdivintrec);
		SmartFileCode.execute(appVars, hdivintIO);
		if (isNE(hdivintIO.getStatuz(), varcom.oK)
		&& isNE(hdivintIO.getStatuz(), varcom.endp)
		&& isNE(hdivintIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(hdivintIO.getParams());
			drylogrec.params.set(hdivintIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		EXIT
	}*/

protected void procHdivcsh5150()
	{
		/*HDIVCSH*/
		/*hdivcshIO.setFormat(formatsInner.hdivcshrec);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(), varcom.oK)
		&& isNE(hdivcshIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hdivcshIO.getParams());
			drylogrec.params.set(hdivcshIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}*/
		/*EXIT*/
	}

protected void x1000CallLifacmv()
	{
		x1010Call();
	}

protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTD");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		/*lifacmvrec.rdocnum.set(hdisdryIO.getChdrnum());
		lifacmvrec.tranref.set(hdisdryIO.getChdrnum());*/
		lifacmvrec.rdocnum.set(dryh524Dto.getChdrnum());
		lifacmvrec.tranref.set(dryh524Dto.getChdrnum());
		lifacmvrec.tranno.set(wsaaNewTranno);
		//lifacmvrec.effdate.set(hdisdryIO.getNextCapDate());
		lifacmvrec.effdate.set(dryh524Dto.getHcapndt());
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hdisdryrec = new FixedLengthStringData(10).init("HDISDRYREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData hdivintrec = new FixedLengthStringData(10).init("HDIVINTREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData hdivcshrec = new FixedLengthStringData(10).init("HDIVCSHREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	
}
}
