/*
 * File: Dry5227.java
 * Date: January 15, 2015 4:14:14 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5227.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.life.diary.dataaccess.RegcdteTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6625rec;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*
*      OVERVIEW  OF THE PROGRAM
*      ========================
*
* The purpose  of this  process is  to produce  Certificate  of
* Existence Letter triggers.
*
* When the Certificate of Existence Date is equal to or
* earlier than the effective date plus the number of Lead Days
* of the batch run, a LETC record will be created for the
* Certificate of Existence.
*
* The Regular Payment record will be further screened by checking
* its payment status against the allowable codes on T6693.
*
* The Next Certificate of Existence date will be advanced by one
* frequency from T6625.
*
* The letter type for the LETC record will be found by reading the
* Automatic Letters table, T6634, with the contract type and the
* transaction code for this batch process.
*
* The status of the Regular Payment (REGP) record will be updated
* in accordance with the values held on T6693 for the current
* status, coverage code and the transaction.
*
* Control Totals
* ==============
*               CT01  : REGP's selected
*               CT02  : REGP's updated
*               CT03  : REGP's rejected
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5227 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5227");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* WSAA-FLAGS-SUBS-STORES-ETC */
	private String wsaaNewLetter = "";
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaRgpystat = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaStatusOk = new FixedLengthStringData(1);
	private Validator statusOk = new Validator(wsaaStatusOk, "Y");
	private Validator statusNotOk = new Validator(wsaaStatusOk, "N");

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaGeneric = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4).init("****");

	private FixedLengthStringData wsaaNotProcessPayment = new FixedLengthStringData(1).init("N");
	private Validator notProcessPayment = new Validator(wsaaNotProcessPayment, "Y");
	private PackedDecimalData wsaaEffectivePlusLeaddays = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaLastCertdate = new PackedDecimalData(8, 0).init(0);
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String regcdterec = "REGCDTEREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* ERRORS */
	private static final String h038 = "H038";
		/* TABLES */
	private static final String t6625 = "T6625";
	private static final String tr384 = "TR384";
	private static final String t6693 = "T6693";
	private static final String t5655 = "T5655";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegcdteTableDAM regcdteIO = new RegcdteTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Freqcpy freqcpy = new Freqcpy();
	private T5655rec t5655rec = new T5655rec();
	private T6693rec t6693rec = new T6693rec();
	private T6625rec t6625rec = new T6625rec();
	private Tr384rec tr384rec = new Tr384rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next580, 
		exit590
	}

	public Dry5227() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

  /**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		initialise200();
		readRegp400();
		finish4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaNotProcessPayment.set("N");
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrrgpIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void readRegp400()
	{
		/*READ*/
		regcdteIO.setParams(SPACES);
		regcdteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		regcdteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		regcdteIO.setCertdate(ZERO);
		regcdteIO.setFormat(regcdterec);
		regcdteIO.setFunction(varcom.begn);
		while ( !(isEQ(regcdteIO.getStatuz(), varcom.endp))) {
			processRegp500();
		}
		
		/*EXIT*/
	}

protected void processRegp500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					process510();
				case next580: 
					next580();
				case exit590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void process510()
	{
		SmartFileCode.execute(appVars, regcdteIO);
		if (isNE(regcdteIO.getStatuz(), varcom.oK)
		&& isNE(regcdteIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(regcdteIO.getStatuz());
			drylogrec.params.set(regcdteIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, regcdteIO.getChdrcoy())
		|| isNE(drypDryprcRecInner.drypEntity, regcdteIO.getChdrnum())
		|| isEQ(regcdteIO.getStatuz(), varcom.endp)) {
			regcdteIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit590);
		}
		getLeadDays600();
		if (isLT(wsaaEffectivePlusLeaddays, regcdteIO.getCertdate())) {
			goTo(GotoLabel.next580);
		}
		processRegp2000();
		if (notProcessPayment.isTrue()) {
			goTo(GotoLabel.next580);
		}
		else {
			updateRegp3000();
		}
	}

protected void next580()
	{
		regcdteIO.setFunction(varcom.nextr);
	}

protected void getLeadDays600()
	{
		start610();
	}

protected void start610()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5655);
		wsaaBatctrcde.set(drypDryprcRecInner.drypBatctrcde);
		itdmIO.setItemitem(wsaaT5655Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5655)
		|| isNE(itdmIO.getItemitem(), wsaaT5655Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5655);
			itdmIO.setItemitem(wsaaT5655Key);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(h038);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5655rec.t5655Rec.set(itdmIO.getGenarea());
		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaEffectivePlusLeaddays.set(datcon2rec.intDate2);
	}

protected void processRegp2000()
	{
		start2010();
	}

protected void start2010()
	{
		wsaaNewLetter = "N";
		statusNotOk.setTrue();
		wsaaRgpystat.set(SPACES);
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		convCertdate2500();
		checkStatii2100();
		if (statusOk.isTrue()) {
			wsaaNewLetter = "Y";
			dryrDryrptRecInner.r537004Exreport.set(SPACES);
		}
		else {
			dryrDryrptRecInner.r537004Exreport.set("Y");
			writeException2200();
			notProcessPayment.setTrue();
			drypDryprcRecInner.processUnsuccesful.setTrue();
		}
	}

protected void checkStatii2100()
	{
		starts2110();
	}

protected void starts2110()
	{
		/* The status of REGP is checked if present on T6693.  This is*/
		/* done twice. The first time with RGPYSTAT & CRTABLE as the*/
		/* item key, and the second (if not found) with **** as the*/
		/* item key. If, after the second check, the item has not been*/
		/* found, then set CT03 .*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regcdteIO.getRgpystat(), SPACES);
		stringVariable1.addExpression(regcdteIO.getCrtable(), SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itemIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regcdteIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6693)
		|| isNE(itdmIO.getItemitem(), wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			nextStatusCheck2300();
		}
		else {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
			t6693Search2400();
		}
	}

protected void writeException2200()
	{
		starts2210();
	}

protected void starts2210()
	{
		dryrDryrptRecInner.r537004Certdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r537004Chdrnum.set(regcdteIO.getChdrnum());
		dryrDryrptRecInner.r537004Life.set(regcdteIO.getLife());
		dryrDryrptRecInner.r537004Coverage.set(regcdteIO.getCoverage());
		dryrDryrptRecInner.r537004Rider.set(regcdteIO.getRider());
		dryrDryrptRecInner.r537004Rgpynum.set(regcdteIO.getRgpynum());
		dryrDryrptRecInner.r537004Pymt.set(regcdteIO.getPymt());
		dryrDryrptRecInner.r537004Currcd.set(regcdteIO.getCurrcd());
		dryrDryrptRecInner.r537004Prcnt.set(regcdteIO.getPrcnt());
		dryrDryrptRecInner.r537004Rgpytype.set(regcdteIO.getRgpytype());
		dryrDryrptRecInner.r537004Payreason.set(regcdteIO.getPayreason());
		dryrDryrptRecInner.r537004Rgpystat.set(regcdteIO.getRgpystat());
		dryrDryrptRecInner.r537004Regpayfreq.set(regcdteIO.getRegpayfreq());
		/* Set up the report name.*/
		/* Move up the sort key.*/
		/* Write the DRPT record.*/
		e000ReportRecords();
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void nextStatusCheck2300()
	{
		starts2310();
	}

protected void starts2310()
	{
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(regcdteIO.getRgpystat(), SPACES);
		stringVariable1.addExpression("****", SPACES);
		stringVariable1.setStringInto(wsaaItemitem);
		itemIO.setDataArea(SPACES);
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItmfrm(regcdteIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6693)
		|| isNE(itdmIO.getItemitem(), wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			statusNotOk.setTrue();
		}
		else {
			t6693rec.t6693Rec.set(itdmIO.getGenarea());
			t6693Search2400();
		}
	}

protected void t6693Search2400()
	{
		/*STARTS*/
		for (wsaaIndex.set(1); !(statusOk.isTrue()
		|| isGT(wsaaIndex, 12)); wsaaIndex.add(1)){
			if (isEQ(drypDryprcRecInner.drypBatctrcde, t6693rec.trcode[wsaaIndex.toInt()])) {
				wsaaRgpystat.set(t6693rec.rgpystat[wsaaIndex.toInt()]);
				statusOk.setTrue();
			}
		}
		/*EXIT*/
	}

protected void convCertdate2500()
	{
		/*STARTS*/
		datcon1rec.intDate.set(regcdteIO.getCertdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void updateRegp3000()
	{
		updates3000();
	}

protected void updates3000()
	{
		/* Call T6625 to get frequency.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6625);
		itdmIO.setItemitem(regcdteIO.getCrtable());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6625)
		|| isNE(itdmIO.getItemitem(), regcdteIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t6625rec.t6625Rec.set(itdmIO.getGenarea());
		}
		writeReportLine3100();
		updateRegp3200();
		if (isEQ(wsaaNewLetter, "Y")) {
			writeLetrqstLetter3300();
		}
	}

protected void writeReportLine3100()
	{
		write3100();
	}

protected void write3100()
	{
		dryrDryrptRecInner.r537004Certdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r537004Chdrnum.set(regcdteIO.getChdrnum());
		dryrDryrptRecInner.r537004Life.set(regcdteIO.getLife());
		dryrDryrptRecInner.r537004Coverage.set(regcdteIO.getCoverage());
		dryrDryrptRecInner.r537004Rider.set(regcdteIO.getRider());
		dryrDryrptRecInner.r537004Rgpynum.set(regcdteIO.getRgpynum());
		dryrDryrptRecInner.r537004Pymt.set(regcdteIO.getPymt());
		dryrDryrptRecInner.r537004Currcd.set(regcdteIO.getCurrcd());
		dryrDryrptRecInner.r537004Prcnt.set(regcdteIO.getPrcnt());
		dryrDryrptRecInner.r537004Rgpytype.set(regcdteIO.getRgpytype());
		dryrDryrptRecInner.r537004Payreason.set(regcdteIO.getPayreason());
		dryrDryrptRecInner.r537004Rgpystat.set(regcdteIO.getRgpystat());
		dryrDryrptRecInner.r537004Regpayfreq.set(regcdteIO.getRegpayfreq());
		/* Set up the report name.*/
		/* Move up the sort key.*/
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void updateRegp3200()
	{
		update3200();
	}

protected void update3200()
	{
		datcon2rec.freqFactor.set(t6625rec.frequency);
		datcon2rec.frequency.set(freqcpy.yrly);
		datcon2rec.intDate2.set(ZERO);
		datcon2rec.intDate1.set(regcdteIO.getCertdate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		regcdteIO.setRgpystat(wsaaRgpystat);
		regcdteIO.setCertdate(datcon2rec.intDate2);
		regcdteIO.setFunction(varcom.writd);
		regcdteIO.setFormat(regcdterec);
		SmartFileCode.execute(appVars, regcdteIO);
		if (isNE(regcdteIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regcdteIO.getParams());
			drylogrec.statuz.set(regcdteIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void writeLetrqstLetter3300()
	{
		write3300();
	}

protected void write3300()
	{
		/* Get CNTTYPE for use in the read of TR384.*/
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(regcdteIO.getChdrcoy());
		chdrrgpIO.setChdrnum(regcdteIO.getChdrnum());
		chdrrgpIO.setFormat(chdrrgprec);
		chdrrgpIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.params.set(chdrrgpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tr384);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrrgpIO.getCnttype(), SPACES);
		stringVariable1.addExpression(drypDryprcRecInner.drypBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* Setup and Write the Letter.*/
		getCntbranch3400();
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(regcdteIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(datcon1rec.intDate);
		letrqstrec.clntcoy.set(chdrrgpIO.getCowncoy());
		letrqstrec.clntnum.set(chdrrgpIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrrgpIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrrgpIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrrgpIO.getChdrnum());
		letrqstrec.tranno.set(chdrrgpIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.params.set(letrqstrec.params);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void getCntbranch3400()
	{
		/*CNTBRANCH*/
		chdrlifIO.setChdrcoy(regcdteIO.getChdrcoy());
		chdrlifIO.setChdrnum(regcdteIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void finish4000()
	{
		/*FINISH*/
		drypDryprcRecInner.drypCertdate.set(regcdteIO.getCertdate());
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r537004DataArea = new FixedLengthStringData(499).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r537004Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537004DataArea, 0);
	private ZonedDecimalData r537004Pymt = new ZonedDecimalData(17, 2).isAPartOf(r537004DataArea, 5);
	private FixedLengthStringData r537004Currcd = new FixedLengthStringData(3).isAPartOf(r537004DataArea, 22);
	private ZonedDecimalData r537004Prcnt = new ZonedDecimalData(7, 2).isAPartOf(r537004DataArea, 25);
	private FixedLengthStringData r537004Payreason = new FixedLengthStringData(2).isAPartOf(r537004DataArea, 32);
	private FixedLengthStringData r537004Rgpystat = new FixedLengthStringData(2).isAPartOf(r537004DataArea, 34);
	private FixedLengthStringData r537004Regpayfreq = new FixedLengthStringData(2).isAPartOf(r537004DataArea, 89);
	private FixedLengthStringData r537004Certdate = new FixedLengthStringData(10).isAPartOf(r537004DataArea, 155);

	private FixedLengthStringData r537004SortKey = new FixedLengthStringData(46);
	private FixedLengthStringData r537004Exreport = new FixedLengthStringData(1).isAPartOf(r537004SortKey, 0);
	private FixedLengthStringData r537004Chdrnum = new FixedLengthStringData(8).isAPartOf(r537004SortKey, 1);
	private FixedLengthStringData r537004Rgpytype = new FixedLengthStringData(2).isAPartOf(r537004SortKey, 9);
	private FixedLengthStringData r537004Life = new FixedLengthStringData(2).isAPartOf(r537004SortKey, 11);
	private FixedLengthStringData r537004Coverage = new FixedLengthStringData(2).isAPartOf(r537004SortKey, 13);
	private FixedLengthStringData r537004Rider = new FixedLengthStringData(2).isAPartOf(r537004SortKey, 15);
	private FixedLengthStringData r537004ReportName = new FixedLengthStringData(10).init("R5227     ");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
}
}
