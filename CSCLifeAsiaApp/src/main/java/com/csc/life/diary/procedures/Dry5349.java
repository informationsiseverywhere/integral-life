/*
 * File: Dry5349.java
 * Date: March 26, 2014 3:00:15 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5349.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.ClbaddbTableDAM;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.DdsupfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ddsupf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.cashdividends.dataaccess.HdivTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HcsdpfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hcsdpf;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.dao.PrmhpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Prmhpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.dataaccess.dao.FpcopfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Fpcopf;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.IncrTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.life.terminationclaims.recordstructures.Calprpmrec;
import com.csc.life.terminationclaims.tablestructures.Td5j1rec;
import com.csc.life.terminationclaims.tablestructures.Td5j2rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                   BILLING
*                   -------
* Overview
* ________
*
* This is the Diary version of LIFE billing.
*
* The following control totals are maintained within the program:
*
*       1 - PAYR records read
*       2 - PAYR records bill suppress'd
*       3 - PTRN records produced
*       4 - Total Amount Billed.
*       5 - LINS records created.
*       6 - Total amount on LINS records.
*       7 - CHDR records that have invalid statii.
*       8 - CHDR records that are 'locked'.
*       9 - Media records (BEXT) created. } Passed back from
*******10 - Total amount on BEXT records. }     BILLREQ1
*      10 - Total amount on BEXT records. }     DRYBILREQ
*
* NOTE: for the  Diary System the splitter is redundant
* and this program performs the selection checks on the
* contract that the splitter would have done.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5349 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5349");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);
	private ZonedDecimalData wsaaOldOutstamt = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaOverduePer = new ZonedDecimalData(11, 2);
	private ZonedDecimalData wsaaCovrInc = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaTotAmt = new ZonedDecimalData(15, 2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(64);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaItemTr384 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemTr384, 0);
	private FixedLengthStringData wsaaItemBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaItemTr384, 3);
	private PackedDecimalData wsaaOldBtdate = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaChdrBillcd = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaEffdatePlusCntlead = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaSuspAvail = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaOldBillcd = new ZonedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaOldNextdate = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaIncreaseDue = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaInstSub = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");
	private Validator invalidContract = new Validator(wsaaValidChdr, "N");

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private String wsaaGotPayrAtBtdate = "";
	private PackedDecimalData wsaaCashdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaAllDvdTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRunDvdTot = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTfrAmt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaShortfall = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremSusp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDvdSusp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNoOfHdis = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validCovr = new Validator(wsaaValidCovr, "Y");
	private Validator invalidCovr = new Validator(wsaaValidCovr, "N");

	//ILPI-258 starts
//	private FixedLengthStringData wsaaWopFound = new FixedLengthStringData(1).init("N");
//	private Validator wopFound = new Validator(wsaaWopFound, "Y");
	//ILPI-258 ends
	
	private PackedDecimalData wsaaTax = new PackedDecimalData(14, 2);//IJTI-1726
	private PackedDecimalData wsaaBillAmount = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaBillOutst = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaIncrBinstprem = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaIncrInstprem = new PackedDecimalData(17, 2).init(ZERO);
	private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private AcblTableDAM acblIO = new AcblTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClbaddbTableDAM clbaddbIO = new ClbaddbTableDAM();
	
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
//	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	
	private FprmTableDAM fprmIO = new FprmTableDAM();
	
	private HdivTableDAM hdivIO = new HdivTableDAM();
	
	private IncrTableDAM incrIO = new IncrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MandTableDAM mandIO = new MandTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Billreqrec billreqrec = new Billreqrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Prasrec prasrec = new Prasrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private T3620rec t3620rec = new T3620rec();
	private T3629rec t3629rec = new T3629rec();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T6654rec t6654rec = new T6654rec();
	private T6687rec t6687rec = new T6687rec();
	private T5729rec t5729rec = new T5729rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaHdisArrayInner wsaaHdisArrayInner = new WsaaHdisArrayInner();

	//ILPI-258
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private DdsupfDAO ddsupfDAO = getApplicationContext().getBean("ddsupfDAO", DdsupfDAO.class);
	private FpcopfDAO fpcopfDAO = getApplicationContext().getBean("fpcopfDAO", FpcopfDAO.class); 
	private HdispfDAO hdispfDAO = getApplicationContext().getBean("hdispfDAO", HdispfDAO.class); 
	private HcsdpfDAO hcsdpfDAO = getApplicationContext().getBean("hcsdpfDAO", HcsdpfDAO.class);
	private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
	private List<Itempf> itempfList;
	private Map<String, List<Itempf>> tr517Map;
	
	//IJTI-1726 starts
	private Map<String, Itempf> ta524Map;
	private Boolean premiumHolidayTrad = Boolean.FALSE;
	private PrmhpfDAO prmhpfDAO = getApplicationContext().getBean("prmhpfDAO", PrmhpfDAO.class);
	private Boolean billWritten = Boolean.FALSE;
	private Boolean reinstflag = Boolean.FALSE;
	private Boolean isFoundPro = Boolean.FALSE;
	private Boolean isFirstBill = Boolean.TRUE;
	private Boolean isShortTax = Boolean.FALSE; 
	private Map<String, List<Itempf>> td5j2ListMap;
	private Map<String, List<Itempf>> td5j1ListMap;
	private static final String TA85 = "TA85";
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ZonedDecimalData lastpayrPTD = new ZonedDecimalData(8, 0);
	private ZonedDecimalData lapsePTDate = new ZonedDecimalData(8, 0);
	private Ptrnpf ptrnpfReinstate = new Ptrnpf();
	private Map<Integer, List<Incrpf>> incrDatedMap = new HashMap<>();
	private Payrpf currPayrLif;
	private Prmhpf prmhpf;
	private ZonedDecimalData increaseprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData leaveprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData proratePrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	private Td5j2rec td5j2rec = new Td5j2rec();
	private Td5j1rec td5j1rec = new Td5j1rec();
	private Calprpmrec calprpmrec = new Calprpmrec();
	private BigDecimal reratedPremium;
	private Datcon3rec datcon3rec = new Datcon3rec();
	private LinspfDAO linspfDAO =  getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private Linspf linspf;
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	//IJTI-1726
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit390,
		exit3790
	}

	public Dry5349() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		//IJTI-1726 starts
		start();
		initializeFeatures(drypDryprcRecInner.drypCompany.trim());
		readAndProcessPayr();
		finish();
		//IJTI-1726 ends
	}

protected void start()//IJTI-1726
	{
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		reratedPremium = BigDecimal.ZERO;//IJTI-1726
		initialise200();
		/* Read the contract header and validate the status against those*/
		/* on T5679. If the status is invalid, perform a non-fatal error.*/
		readChdr500();
		validateChdr600();
		/* Check for invalid CHDR statii*/
		if (!validContract.isTrue()) {
			return ;
		}
		/* Read table TR52D.                                               */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlifIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		
		tr517Map = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString().trim(), "TR517");
		//ILPI-258 ends
     }

	//IJTI-1726 starts
	/**
	 * Initialize feature specific configurations
	 * 
	 * @param company - Company ID
	 */
	protected void initializeFeatures(String company) {
		premiumHolidayTrad = FeaConfg.isFeatureExist(company, "CSOTH010", appVars, "IT");
		if(premiumHolidayTrad) {
			ta524Map = itemDAO.getItemMap("IT", company, "TA524");
		}
		
		reinstflag = FeaConfg.isFeatureExist(company, "CSLRI003", appVars, "IT");
		if (reinstflag) {
			td5j2ListMap = itemDAO.loadSmartTable("IT", company, "TD5J2");
			td5j1ListMap = itemDAO.loadSmartTable("IT", company, "TD5J1");
		}
	}
	//IJTI-1726 ends
	
	

	protected void initialise200() {
		/* Read T5679 for valid statii. */
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5645 for Account Codes. */
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Read T3695 to check the sign. */
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
	}

	//IJTI-1726 starts
	/**
	 * Loop through Payrpf records and process them
	 */
	protected void readAndProcessPayr() {
		List<Payrpf> payrpfList = payrpfDAO.readPayrpfForBilling(drypDryprcRecInner.drypCompany.toString().trim(), drypDryprcRecInner.drypEntity.toString().trim());
		for(Payrpf payrpf : payrpfList){
			processPayr(payrpf);
		}
	}
	//IJTI-1726 ends

	/**
	 * Main processing of payer
	 * 
	 * @param payrpf - Payrpf record
	 */
	protected void processPayr(Payrpf payrpf) {
		try {
			drycntrec.contotNumber.set(controlTotalsInner.ct01);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			checkT5729700();
			calcLeadDays3600(payrpf.getBillchnl(), payrpf.getBillfreq());
			/*    Once we have calculated the lead days for this contract      */
			/*    type we can compare it to the BILLCD date to see if the      */
			/*    contract requires billing.                                   */
			if (isGT(payrpf.getBillcd(), wsaaEffdatePlusCntlead)) {
				return ;
			}
			/* If suppressed billing, check if the present bill date is within*/
			/* the suppressed period.*/
			if (isEQ(payrpf.getBillsupr(), "Y")) {
				if (isGTE(payrpf.getBillcd(), payrpf.getBillspfrom())
				&& isLT(payrpf.getBillcd(), payrpf.getBillspto())
				&& isLT(drypDryprcRecInner.drypRunDate, payrpf.getBillspto())) {
					drycntrec.contotNumber.set(controlTotalsInner.ct02);
					drycntrec.contotValue.set(1);
					d000ControlTotals();
					return ;
				}
			}
			processPayr1(payrpf);//IJTI-1726
			processPayr2(payrpf);//IJTI-1726
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}




	protected void readChdr500() {
		/* Read contract header record using CHDRLIF.*/
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(Varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaChdrBillcd.set(chdrlifIO.getBillcd());
	}


protected void validateChdr600()
	{
		/*VALIDATE*/
		/* Validate Contract status against T5679.*/
		invalidContract.setTrue();
		for (ix.set(1); !(isGT(ix, 12)
		|| validContract.isTrue()); ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[ix.toInt()], chdrlifIO.getStatcode())) {
				for (ix.set(1); !(isGT(ix, 12)
				|| validContract.isTrue()); ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[ix.toInt()], chdrlifIO.getPstatcode())) {
						validContract.setTrue();
					}
				}
			}
		}
		/*EXIT*/
	}

protected void checkT5729700()
	{
		check710();
	}

protected void check710()
	{
		wsaaFlexPrem.set("N");
		Itempf itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(tablesInner.t5729.toString());
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemitem(chdrlifIO.getCnttype().toString());
		itempf.setItmfrm(chdrlifIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlifIO.getOccdate().getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				t5729rec.t5729Rec.set(StringUtil.rawToString(it.getGenarea()));	
				flexiblePremiumContract.setTrue();				
		}
		}
		
		
	}

//ILPI-256

	/**
	 * Payer processing Part 1
	 * 
	 * @param payrpf - Payrpf record
	 */
	protected void processPayr1(Payrpf payrpf) {
		wsaaOldBtdate.set(payrpf.getBtdate());
		wsaaOldBillcd.set(payrpf.getBillcd());
		wsaaOldNextdate.set(payrpf.getNextdate());
		/* Save original amount as this is not to be updated for*/
		/* flexible premium contracts*/
		wsaaOldOutstamt.set(payrpf.getOutstamt());
	
		readClrf1000(payrpf);
		read1110(payrpf);
		includeApa1120(payrpf);
		
		processFeatureCsoth010(payrpf);//IJTI-1726
		List<Covrpf> covrpfList = covrpfDAO.readCovrForBilling(chdrlifIO.getChdrcoy().toString().trim(), chdrlifIO.getChdrnum().toString().trim());
		checkForWaiverOfPremium(covrpfList, payrpf);//IJTI-1726
		
		processFeatureNbprp011(payrpf);//IJTI-1726
		processFeatureCslri003(payrpf);//IJTI-1726
		
		/* Bill for one instalment only then return to the batch*/
		/* diary system. If the contract has a billing suppression*/
		/* set, do nothing except update control totals.*/
		if (isGT(payrpf.getBillcd(), wsaaChdrBillcd)
		|| (isGTE(payrpf.getBillcd(), payrpf.getBillspfrom())
		&& isLT(payrpf.getBillcd(), payrpf.getBillspto())
		&& isLT(drypDryprcRecInner.drypRunDate, payrpf.getBillspto()))) {
			return ;
		}
		/* No billing suppression is set on the contract*/
		/* Complete the details for billing for this instalment*/
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		payrpf.setTranno(chdrlifIO.getTranno().toInt());
		a1000GetCurrConvDate(payrpf);
		calcPayrPrem1200(payrpf);
		advanceBtdate1400(payrpf);
		calcTax4100(covrpfList, payrpf);//IJTI-1726
		automaticIncrease1500(payrpf);//IJTI-1726
		writeLins1700(covrpfList, payrpf);
		produceBextRecord1800(payrpf);
		writePtrn2700();
		//IJTI-1726 starts
		wsaaT6654Billchnl.set(payrpf.getBillchnl());
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		String T6654Key = payrpf.getBillchnl().trim() + chdrlifIO.getCnttype().toString().trim();
		if(BTPRO028Permission) {
			T6654Key = T6654Key.concat(payrpf.getBillfreq().trim());
		}
		if(isUpdateNlgt(T6654Key)) {
			callNlgcalc(payrpf);
		}
		//IJTI-1726 ends
		/*     Advance the old billed to date for subsequent billing*/
		wsaaOldBtdate.set(payrpf.getBtdate());
		wsaaOldNextdate.set(payrpf.getNextdate());
		/*     Add increase due and tax to outstanding amount              */
		//IJTI-1726 starts
		setPrecision(chdrlifIO.getOutstamt(), 2);
		chdrlifIO.setOutstamt(add(chdrlifIO.getOutstamt(), add(wsaaIncreaseDue, wsaaTax)));
		setPrecision(payrpf.getOutstamt(), 2);
		payrpf.setOutstamt(payrpf.getOutstamt()
				.add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble()).add(BigDecimal.valueOf(wsaaTax.toDouble()))));
		//IJTI-1726 ends
	}

	//IJTI-1726 starts
	private void checkForWaiverOfPremium(List<Covrpf> covrpfList, Payrpf payrpf) {
		/* Check for WOP component if TR52D tax code not = space           */
		if (isNE(payrpf.getSinstamt05(), ZERO)
		&& isNE(tr52drec.txcode, SPACES)) {
			for(Covrpf covrpf : covrpfList){
				List<Itempf> tr517ItemList = tr517Map.get(covrpf.getCrtable().trim());
				if(tr517ItemList!= null && !tr517ItemList.isEmpty()){
					Itempf tr517Item = tr517ItemList.get(0);
					if (tr517Item != null && tr517Item.getItmfrm().intValue() <= covrpf.getCrrcd()
							&& isEQ(covrpf.getValidflag(), "1")) {// IJTI-1726
						break;
					}
				}
			}
		}
	}
	/**
	 * Process feature CSOTH010
	 * 
	 * @param payrpf - Payer detail
	 */
	private void processFeatureCsoth010(Payrpf payrpf) {
		if(premiumHolidayTrad && ta524Map.get(chdrlifIO.getCnttype().trim()) != null) {
			prmhpf = prmhpfDAO.getPrmhRecord(payrpf.getChdrcoy(), payrpf.getChdrnum());
			if(prmhpf != null && drypDryprcRecInner.drypRunDate.toInt()<=prmhpf.getTodate() && !billWritten) {  
				wsaaOldBtdate.set(wsaaOldBillcd);
				payrpf.setBtdate(wsaaOldBtdate.toInt());
			}
		}
	}
	
	/**
	 * Process feature NBPRP011
	 * 
	 * @param payrpf - Payer detail
	 */
	private void processFeatureNbprp011(Payrpf payrpf) {
		boolean isbillday = FeaConfg.isFeatureExist(payrpf.getChdrcoy(), "NBPRP011", appVars, "IT");
		if (featureNbprp011Condition1(payrpf, isbillday)
				&& featureNbprp11Condition2(payrpf)) {
			wsaaChdrBillcd.set(drypDryprcRecInner.drypRunDate);
		}
	}
	
	private boolean featureNbprp011Condition1(Payrpf payrpf, boolean isbillday) {
		return isbillday && payrpf.getBillfreq() != null && isEQ(payrpf.getBillfreq(), "12");
	}
	
	private boolean featureNbprp11Condition2(Payrpf payrpf) {
		return isEQ(payrpf.getBillchnl(), "D") || isEQ(payrpf.getBillchnl(), "R")
				|| isEQ(payrpf.getBillchnl(), "A") || isEQ(payrpf.getBillchnl(), "B");
	}
	
	/**
	 * Process feature CSLRI003
	 * 
	 * @param payrpf - Payer detail
	 */
	private void processFeatureCslri003(Payrpf payrpf) {
		if(reinstflag){
			List<Ptrnpf> ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(payrpf.getChdrcoy(), payrpf.getChdrnum());
    		for(Ptrnpf ptrn : ptrnrecords ){
    		if(isNE(ptrn.getBatctrcde(),TA85) && isNE(ptrn.getBatctrcde(),"B521")){
    			continue;
    		}
    		if(isEQ(ptrn.getBatctrcde(),"B521")){
    			break;
    		}
    		if(isEQ(ptrn.getBatctrcde(),TA85)){
    			 isFoundPro = Boolean.TRUE;
    			 incrDatedMap = incrpfDAO.getIncrDatedMap(payrpf.getChdrcoy(), payrpf.getChdrnum());/* IJTI-1386 */
    			 ptrnpfReinstate  = ptrnpfDAO.getPtrnData(payrpf.getChdrcoy(), payrpf.getChdrnum(), TA85);
    			 getLastPTD(payrpf);
    			}
    		}
		}
	}
	
	protected void getLastPTD(Payrpf payrpf){
		lastpayrPTD.set(payrpf.getBillcd());
		ZonedDecimalData ptdlapse = new ZonedDecimalData(8, 0);
		ptdlapse.set(payrpf.getBillcd());
		lapsePTDate.set(payrpf.getBillcd());
		while(isGT(ptrnpfReinstate.getDatesub(),ptdlapse)){
			datcon2rec.datcon2Rec.set(SPACES);
			lapsePTDate.set(ptdlapse);
			datcon2rec.frequency.set(payrpf.getBillfreq());
			datcon2rec.intDate1.set(ptdlapse);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, Varcom.oK)) {
				drylogrec.params.set(datcon2rec.datcon2Rec);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			else {
				ptdlapse.set(datcon2rec.intDate2);
			}
		}
	}
	
	//IJTI-1726 ends

//ILPI-258
	
	/**
	 * Payer processing Part 2
	 * 
	 * @param payrpf - Payrpf record
	 */
	protected void processPayr2(Payrpf payrpf) {//IJTI-1726
		if (isGTE(payrpf.getBillcd(), payrpf.getBillspfrom())
		&& isLT(payrpf.getBillcd(), payrpf.getBillspto())
		&& isLT(drypDryprcRecInner.drypRunDate, payrpf.getBillspto())) {
			drycntrec.contotNumber.set(controlTotalsInner.ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}
		rewriteChdr2800();
		rewritePayr2900(payrpf);
		writeLetter4000();
		sendBillingNoticeSmsNotification();//IJTI-1726
		/*EXIT*/
	}

protected void readClrf1000(Payrpf payrpf)
	{
		/* Read the client role file using the contract header*/
		/* to get the client who is paying.*/
		clrfIO.setForepfx(fsupfxcpy.chdr);
		clrfIO.setForecoy(payrpf.getChdrcoy());
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(Varcom.readr);
		clrfIO.setFormat(formatsInner.clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(clrfIO.getStatuz());
			drylogrec.params.set(clrfIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

	}


protected void read1110(Payrpf payrpf)
	{
		wsaaSuspAvail.set(0);
		acblIO.setRldgcoy(payrpf.getChdrcoy());
		acblIO.setRldgacct(payrpf.getChdrnum());
		acblIO.setOrigcurr(payrpf.getBillcurr());
		acblIO.setSacscode(t5645rec.sacscode01);
		acblIO.setSacstyp(t5645rec.sacstype01);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK)
		&& isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(acblIO.getStatuz());
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Multiply the balance by the sign from T3695.*/
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaSuspAvail.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(ZERO, acblIO.getSacscurbal()));
			}
			else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
	}

	/**
	* <pre>
	* To include Advance Premium Deposit into premium suspense
	* account WSAA-SUSP-AVAIL. So that it will show the total suspense
	* available.
	* </pre>
	*/
protected void includeApa1120(Payrpf payrpf)
	{
		initialize(rlpdlonrec.rec);
		rlpdlonrec.function.set(Varcom.info);
		rlpdlonrec.chdrcoy.set(payrpf.getChdrcoy());
		rlpdlonrec.chdrnum.set(payrpf.getChdrnum());
		rlpdlonrec.prmdepst.set(ZERO);
		rlpdlonrec.language.set(drypDryprcRecInner.drypLanguage);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(rlpdlonrec.statuz);
			drylogrec.params.set(rlpdlonrec.rec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaSuspAvail.add(rlpdlonrec.prmdepst);
		/* Store LP S amount just read to WSAA-PREM-SUSP                   */
		wsaaPremSusp.set(wsaaSuspAvail);
		/* MOVE WSAA-PREM-SUSP         TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO WSAA-PREM-SUSP.               */
		if (isNE(wsaaPremSusp, 0)) {
			zrdecplrec.amountIn.set(wsaaPremSusp);
			zrdecplrec.currency.set(payrpf.getBillcurr());
			callRounding8000();
			wsaaPremSusp.set(zrdecplrec.amountOut);
		}
		/* Read ACBL for LC DS                                             */
		acblIO.setOrigcurr(chdrlifIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode03);
		acblIO.setSacstyp(t5645rec.sacstype03);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK)
		&& isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Multiply the balance by the sign from T3695.                    */
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsaaDvdSusp.set(ZERO);
			return ;
		}
		if (isEQ(acblIO.getSacscurbal(), ZERO)) {
			wsaaDvdSusp.set(ZERO);
			return ;
		}
		/* Read T3695 to check the sign.                                   */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(t5645rec.sacstype03);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		if (isEQ(t3695rec.sign, "-")) {
			setPrecision(acblIO.getSacscurbal(), 2);
			acblIO.setSacscurbal(mult(acblIO.getSacscurbal(), -1));
		}
		/* Convert if billing ccy and contract ccy differs.                */
		if (isNE(chdrlifIO.getBillcurr(), chdrlifIO.getCntcurr())) {
			conlinkrec.statuz.set(SPACES);
			conlinkrec.currIn.set(chdrlifIO.getCntcurr());
			conlinkrec.amountIn.set(acblIO.getSacscurbal());
			conlinkrec.cashdate.set(varcom.vrcmMaxDate);
			conlinkrec.currOut.set(chdrlifIO.getBillcurr());
			conlinkrec.company.set(chdrlifIO.getChdrcoy());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("REAL");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, "****")) {
				drylogrec.params.set(conlinkrec.clnk002Rec);
				drylogrec.statuz.set(conlinkrec.statuz);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaDvdSusp.set(conlinkrec.amountOut);
			/*     MOVE WSAA-DVD-SUSP          TO ZRDP-AMOUNT-IN            */
			/*     MOVE CHDRLIF-BILLCURR       TO ZRDP-CURRENCY             */
			/*     PERFORM 8000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT        TO WSAA-DVD-SUSP             */
			if (isNE(wsaaDvdSusp, 0)) {
				zrdecplrec.amountIn.set(wsaaDvdSusp);
				zrdecplrec.currency.set(chdrlifIO.getBillcurr());
				callRounding8000();
				wsaaDvdSusp.set(zrdecplrec.amountOut);
			}
		}
		else {
			wsaaDvdSusp.set(acblIO.getSacscurbal());
		}
		/* Consolidate total suspense                                      */
		wsaaSuspAvail.add(wsaaDvdSusp);
	}

protected void calcPayrPrem1200(Payrpf payrpf)
	{
		/* Check if the PAYR record has the correct premium for the*/
		/* instalment date, if not obtain it from history records*/
		wsaaOldBillcd.set(payrpf.getBillcd());
		wsaaOldNextdate.set(payrpf.getNextdate());
		wsaaGotPayrAtBtdate = "N";
		
		//IJTI-1726 starts
		if (isLT(payrpf.getBtdate(), payrpf.getEffdate())) {
			getPayerAtBtDate(payrpf);
		}
		
		if(reinstflag && isFoundPro){
			calcProrated(payrpf);
		}
		
		if (prmhpf != null && premiumHolidayTrad && !billWritten
				&& drypDryprcRecInner.drypRunDate.toInt() <= prmhpf.getTodate()) {
			Payrpf payrpfnew = new Payrpf();
			payrpfnew.setChdrnum(payrpf.getChdrnum());
			payrpfnew.setChdrcoy(payrpf.getChdrcoy());
			List<Payrpf> payrpfList = payrpfDAO.readPayrHist(payrpfnew);
	        for (Payrpf payr: payrpfList){
	        	wsaaGotPayrAtBtdate = "Y";
	        	currPayrLif = payr;
	        	break;
	        }
		}
		
		setInstPremium(payrpf);
		//IJTI-1726 ends
	}
	//IJTI-1726 starts
	private void getPayerAtBtDate(Payrpf payrpf) {
		Payrpf payrpfnew = new Payrpf();
		payrpfnew.setChdrnum(payrpf.getChdrnum());/* IJTI-1523 */
		payrpfnew.setChdrcoy(payrpf.getChdrcoy());/* IJTI-1523 */
		List<Payrpf> payrpfList = payrpfDAO.readPayrHist(payrpfnew);
		
		for (Payrpf payrHist : payrpfList) {
			if (isGTE(payrpf.getBtdate(), payrHist.getEffdate())) {
				wsaaGotPayrAtBtdate = "Y";
				currPayrLif = payrHist;
				break;
			}
		}
	}
	private void setInstPremium(Payrpf payrpf) {
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			setPremiumPayerAtBilledToDate(payrpf);
		} else {
			setPremium(payrpf);
		}
	}
	
	private void setPremiumPayerAtBilledToDate(Payrpf payrpf) {
		if (isFirstBill) {
			if (reinstflag && isFoundPro) {
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem, leaveprem)));
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getOutstamt(), wsaaInstPrem));
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			} else {
				payrpf.setOutstamt(currPayrLif.getOutstamt().add(currPayrLif.getSinstamt06()));
			}
			isFirstBill = Boolean.FALSE;
		} else {
			setPrecision(payrpf.getOutstamt(), 2);
			if (reinstflag && isFoundPro) {
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem, leaveprem)));
				compute(wsaaInstPrem, 2).set(add(payrpf.getOutstamt(), wsaaInstPrem));
				payrpf.setOutstamt(wsaaInstPrem.getbigdata());
			} else {
				payrpf.setOutstamt(payrpf.getOutstamt().add(currPayrLif.getSinstamt06()));
			}
		}
		chdrlifIO.setOutstamt(payrpf.getOutstamt());
	}
	
	private void setPremium(Payrpf payrpf) {
		setPrecision(payrpf.getOutstamt(), 2);
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(wsaaInstPrem, 2).set(add(payrpf.getOutstamt(), wsaaInstPrem));
			payrpf.setOutstamt(wsaaInstPrem.getbigdata());
		}
		else{
			payrpf.setOutstamt(payrpf.getOutstamt().add(payrpf.getSinstamt06()));
		}
		chdrlifIO.setOutstamt(payrpf.getOutstamt());
	}

	protected void calcProrated(Payrpf payrpf){
		readTd5j1(chdrlifIO.getCnttype().trim());
		if(isNE(td5j1rec.td5j1Rec,SPACES) && isEQ(td5j1rec.subroutine,SPACES)){
			return;	
		}
		 increaseprem.set(ZERO);
		 leaveprem.set(ZERO);
		 proratePrem.set(ZERO);
		 wsaaInstPrem.set(ZERO);
		if (isGTE(lapsePTDate, payrpf.getBtdate())) {
			List<Covrpf> covrpflist  = covrpfDAO.getCovrByComAndNum(payrpf.getChdrcoy(), payrpf.getChdrnum());
			for (Covrpf covrpf : covrpflist) {
				readTd5j2(covrpf.getCrtable());
				readIncr(covrpf, payrpf);
			}
		}
	}
	
	protected void readTd5j1(String cnttype) {
		if (td5j1ListMap.containsKey(cnttype)){	
			Iterator<Itempf> iterator = td5j1ListMap.get(cnttype).iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if(drypDryprcRecInner.drypRunDate.toInt() >= Integer.parseInt(itempf.getItmfrm().toString())
							&& drypDryprcRecInner.drypRunDate.toInt() <= Integer.parseInt(itempf.getItmto().toString())){
						td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					}
				}else{
					td5j1rec.td5j1Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				}				
			}		
		}
		else {
			td5j1rec.td5j1Rec.set(SPACES);
		}	
	}
	
	protected void readTd5j2(String crtable){
		if (td5j2ListMap.containsKey(crtable)){	
			Iterator<Itempf> iterator = td5j2ListMap.get(crtable).iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if(drypDryprcRecInner.drypRunDate.toInt()  >= Integer.parseInt(itempf.getItmfrm().toString()) 
							&& drypDryprcRecInner.drypRunDate.toInt()  <= Integer.parseInt(itempf.getItmto().toString())){
						td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
					}
				}else{
					td5j2rec.td5j2Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				}				
			}		
		}
		else {
			td5j2rec.td5j2Rec.set(SPACES);
		}	
	}
	
	protected void readIncr(Covrpf covrpf, Payrpf payrpf) {
		if (isNE(td5j2rec.td5j2Rec, SPACES) && isEQ(td5j2rec.shortTerm, "Y")) {
			compute(leaveprem, 2).add(covrpf.getInstprem().doubleValue());
			if (isEQ(lapsePTDate, payrpf.getBtdate())) {
				callSubroutine(covrpf, payrpf);
			}
			return;
		}

		Incrpf currIncr = null;
		int date = payrpf.getBtdate();
		boolean incrFound = false;
		if (incrDatedMap != null && incrDatedMap.containsKey(date)) {
			List<Incrpf> incrpfList = incrDatedMap.get(date);
			for (Iterator<Incrpf> iterator = incrpfList.iterator(); iterator.hasNext();) {
				Incrpf incr = iterator.next();
				if (isIncrMatched(incr, covrpf)) {
					currIncr = incr;
					incrFound = true;
					break;
				}
			}
		}

		if (incrFound) {
			compute(increaseprem, 2).add(sub(covrpf.getInstprem(), currIncr.getNewinst()));
		}
	}
	
	private boolean isIncrMatched(Incrpf incr, Covrpf covrpf) {
		return isIncrMatchedCondition1(incr, covrpf) && isIncrMatchedCondition2(incr, covrpf);
	}
	
	private boolean isIncrMatchedCondition1(Incrpf incr, Covrpf covrpf) {
		return incr.getChdrnum().equals(covrpf.getChdrnum()) && incr.getLife().equals(covrpf.getLife())
				&& incr.getCoverage().equals(covrpf.getCoverage());
	}
	
	private boolean isIncrMatchedCondition2(Incrpf incr, Covrpf covrpf) {
		return incr.getRider().equals(covrpf.getRider())
				&& Integer.compare(incr.getPlnsfx(), covrpf.getPlanSuffix()) == 0;
	}
	
	protected void callSubroutine(Covrpf covrpf, Payrpf payrpf){
		 ZonedDecimalData nextPTDate = callDatcon4500(payrpf);
		 calprpmrec.reinstDate.set(ptrnpfReinstate.getDatesub());
		 calprpmrec.lastPTDate.set(payrpf.getPtdate());
		 calprpmrec.nextPTDate.set(nextPTDate);
		 calprpmrec.transcode.set(drypDryprcRecInner.drypBatctrcde);
		 calprpmrec.uniqueno.set(covrpf.getUniqueNumber());
		 calprpmrec.prem.set(covrpf.getInstprem());
		 calprpmrec.chdrcoy.set(payrpf.getChdrcoy());
		 calprpmrec.chdrnum.set(payrpf.getChdrnum());
		 callProgram(td5j1rec.subroutine, calprpmrec.calprpmRec,null);
		 if (isNE(calprpmrec.statuz, Varcom.oK)) {
				drylogrec.params.set(calprpmrec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
		 }
		 reratedPremium = calprpmrec.prem.getbigdata();
		 proratePrem.add(calprpmrec.prem);
	}
	//IJTI-1726 ends
	
	protected ZonedDecimalData callDatcon4500(Payrpf payrpf) {
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(lastpayrPTD);
		datcon3rec.intDate2.set(ptrnpfReinstate.getDatesub());
		datcon3rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,"****")) {
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		ZonedDecimalData wsaaInstOutst = new ZonedDecimalData(2, 0).setUnsigned();
		wsaaInstOutst.set(datcon3rec.freqFactor);
		wsaaInstOutst.add(1);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(payrpf.getPtdate());
		datcon2rec.freqFactor.set(wsaaInstOutst);
		datcon2rec.frequency.set(payrpf.getBillfreq());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		return datcon2rec.intDate2;
	}



protected void advanceBtdate1400(Payrpf payrpf)
	{
		/* Advance the 'Billed to date' by one Frequency.*/
		/* The current saved BTDATE is used, so that the INSTFROM and*/
		/* BILLdates can be updated.*/
		datcon4rec.freqFactor.set(1);
		datcon2rec.freqFactor.set(1);
		datcon4rec.frequency.set(payrpf.getBillfreq());
		datcon2rec.frequency.set(payrpf.getBillfreq());
		datcon4rec.intDate1.set(payrpf.getBtdate());
		datcon2rec.intDate1.set(payrpf.getBtdate());
		datcon4rec.billday.set(payrpf.getDuedd());
		datcon4rec.billmonth.set(payrpf.getDuemm());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon4rec.statuz);
			drylogrec.params.set(datcon4rec.datcon4Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaOldBtdate.set(payrpf.getBtdate());//IJTI-1726
		payrpf.setBtdate(datcon4rec.intDate2.toInt());
		chdrlifIO.setBtdate(datcon4rec.intDate2);
		wsaaOldBillcd.set(payrpf.getBillcd());
		wsaaOldNextdate.set(payrpf.getNextdate());
		/* Advance the BILLING COMMENCEMENT DATE by one frequency*/
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(payrpf.getBillfreq());
		datcon4rec.intDate1.set(payrpf.getBillcd());
		datcon4rec.billday.set(payrpf.getBillday());
		datcon4rec.billmonth.set(payrpf.getBillmonth());
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon4rec.statuz);
			drylogrec.params.set(datcon4rec.datcon4Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		payrpf.setBillcd(datcon4rec.intDate2.toInt());
		chdrlifIO.setBillcd(datcon4rec.intDate2);
		compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t6654rec.leadDays));
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(payrpf.getBillcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		payrpf.setNextdate(datcon2rec.intDate2.toInt());
	}

protected void automaticIncrease1500(Payrpf payrpf)
	{
		/* Look up any automatic increases.*/
		wsaaIncreaseDue.set(ZERO);
		Incrpf incrpf = new Incrpf();//IJTI-1726
		incrpf.setChdrcoy(payrpf.getChdrcoy());
		incrpf.setChdrnum(payrpf.getChdrnum());
		List<Incrpf> incrpfList = incrpfDAO.searchIncrpfRecord(incrpf);
		for (Incrpf incr : incrpfList) {
			if (isLT(incr.getCrrcd(), payrpf.getBtdate())) {
				compute(wsaaIncreaseDue, 2).set(sub(add(wsaaIncreaseDue, incr.getNewinst()), incr.getLastInst()));
			}
		}
	
		/* Update Increase amount into PAYR OUTSTAMT                       */
		setPrecision(payrpf.getOutstamt(), 2);
		payrpf.setOutstamt(add(payrpf.getOutstamt(), wsaaIncreaseDue).getbigdata());
		chdrlifIO.setOutstamt(payrpf.getOutstamt());
		/* Log total amount billed.*/
		//IJTI-1726 starts
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			if (reinstflag && isFoundPro) {
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem, leaveprem)));
				compute(drycntrec.contotValue, 2).set(add(wsaaInstPrem, wsaaIncreaseDue));
			} else {
				compute(drycntrec.contotValue, 2).set(add(currPayrLif.getSinstamt06(), wsaaIncreaseDue));
			}
		}
		else {
			if (reinstflag && isFoundPro) {
				compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem, leaveprem)));
				compute(drycntrec.contotValue, 2).set(add(wsaaInstPrem, wsaaIncreaseDue));
			} else {
				compute(drycntrec.contotValue, 2).set(add(payrpf.getSinstamt06(), wsaaIncreaseDue));
			}
		}
		//IJTI-1726 ends
		drycntrec.contotValue.add(wsaaTax);
		drycntrec.contotNumber.set(controlTotalsInner.ct04);
		d000ControlTotals();
	}

	protected void writeLins1700(List<Covrpf> covrpfList, Payrpf payrpf) {
		//IJTI-1726 starts
		linspf = new Linspf();
		linspf.setInstjctl("");
		linspf.setDueflg("");
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			setLinsForPayerAtBtDate();
		}
		else {
			setLinsForPayerNotAtBtDate(payrpf);
		}
		wsaaBillAmount.set(linspf.getInstamt06());
		setPrecision(linspf.getInstamt06(), 2);
		linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaTax.toDouble())));
		/* Convert contract amount(total) to billing amount if contract*/
		/* currency is not the same as billing currency.*/
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())
		|| isEQ(linspf.getInstamt06(), ZERO)) {
			linspf.setCbillamt(linspf.getInstamt06());
		}
		else {
			conlinkrec.amountIn.set(linspf.getInstamt06());
			callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
			linspf.setCbillamt(BigDecimal.valueOf(conlinkrec.amountOut.getbigdata().doubleValue()));
		}
		/* If this is a flexible premium contract then we do not*/
		/* want to write a LINS record. We have come this far*/
		/* however as we do need to know linsrnl-cbillamt value*/
		/* to update the FPRM file. Also we maintain CT11 which*/
		/* logs the number of flexible premium billings:*/
		if (flexiblePremiumContract.isTrue()) {
			writeFpco3300(covrpfList, payrpf);
			drycntrec.contotNumber.set(controlTotalsInner.ct11);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return ;
		}
		linspf.setChdrnum(payrpf.getChdrnum());
		linspf.setChdrcoy(payrpf.getChdrcoy());
		linspf.setBillchnl(payrpf.getBillchnl());
		linspf.setPayrseqno(payrpf.getPayrseqno());
		linspf.setBranch(drypDryprcRecInner.drypBranch.trim());
		linspf.setTranscode(drypDryprcRecInner.drypBatctrcde.trim());
		linspf.setInstfreq(payrpf.getBillfreq());
		linspf.setCntcurr(payrpf.getCntcurr());
		linspf.setBillcurr(payrpf.getBillcurr());
		linspf.setInstto(payrpf.getBtdate());
		linspf.setMandref(payrpf.getMandref());
		setLinsForPremiumHoliday();
		linspf.setBillcd(wsaaOldBillcd.toInt());
		linspf.setAcctmeth(chdrlifIO.getAcctmeth().trim());
		linspf.setPayflag("O");
		linspf.setValidflag("1");
		/* Update the tax relief method on the LINS record*/
		/* if tax relief is applied.*/
		/* MOVE LINSRNL-INSTAMT06      TO PRAS-GROSSPREM.               */
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			prasrec.grossprem.set(wsaaBillAmount);
		}
		else {
			conlinkrec.amountIn.set(wsaaBillAmount);
			callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
			prasrec.grossprem.set(conlinkrec.amountOut);
		}
		calculateTaxRelief3200(payrpf);
		linspfDAO.insert(linspf);
		//IJTI-1726 ends
		/* Log number of LINS written*/
		drycntrec.contotNumber.set(controlTotalsInner.ct05);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/* Log total of this instalement*/
		drycntrec.contotValue.set(linspf.getInstamt06());
		drycntrec.contotNumber.set(controlTotalsInner.ct06);
		d000ControlTotals();
	}
	
	private void setLinsForPayerAtBtDate() {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt01(wsaaInstPrem.getbigdata());
		} else {
			linspf.setInstamt01(currPayrLif.getSinstamt01());
		}
		setPrecision(linspf.getInstamt01(), 2);
		linspf.setInstamt01(linspf.getInstamt01().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
		linspf.setInstamt02(currPayrLif.getSinstamt02());
		linspf.setInstamt03(currPayrLif.getSinstamt03());
		linspf.setInstamt04(currPayrLif.getSinstamt04());
		linspf.setInstamt05(currPayrLif.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt06(wsaaInstPrem.getbigdata());
		}
		else{
			linspf.setInstamt06(currPayrLif.getSinstamt06());
		}
		setPrecision(linspf.getInstamt06(), 2);
		linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
	}
	
	private void setLinsForPayerNotAtBtDate(Payrpf payrpf) {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt01(wsaaInstPrem.getbigdata());
		}
		else{
			linspf.setInstamt01(payrpf.getSinstamt01());
		}
		setPrecision(linspf.getInstamt01(), 2);
		linspf.setInstamt01(linspf.getInstamt01().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
		linspf.setInstamt02(payrpf.getSinstamt02());
		linspf.setInstamt03(payrpf.getSinstamt03());
		linspf.setInstamt04(payrpf.getSinstamt04());
		linspf.setInstamt05(payrpf.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			linspf.setInstamt06(wsaaInstPrem.getbigdata());
		} else {
			linspf.setInstamt06(payrpf.getSinstamt06());
		}
		
		setPrecision(linspf.getInstamt06(), 2);
		linspf.setInstamt06(linspf.getInstamt06().add(BigDecimal.valueOf(wsaaIncreaseDue.toDouble())));
	}
	
	private void setLinsForPremiumHoliday() {
		if (prmhpf != null && premiumHolidayTrad && !billWritten
				&& drypDryprcRecInner.drypRunDate.toInt() <= prmhpf.getTodate()) {
			linspf.setInstfrom(drypDryprcRecInner.drypRunDate.toInt());
			if(currPayrLif != null) {
				linspf.setProrcntfee(currPayrLif.getProrcntfee());
				linspf.setProramt(currPayrLif.getProramt());
			} else {
				linspf.setProrcntfee(BigDecimal.ZERO);
				linspf.setProramt(BigDecimal.ZERO);
			}
			billWritten = Boolean.TRUE;
		}
		else {
			linspf.setInstfrom(wsaaOldBtdate.toInt());
		}
	}

protected void produceBextRecord1800(Payrpf payrpf)
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3620);
		itemIO.setItemitem(payrpf.getBillchnl());
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		/* Convert contract amount to billing amount if contract currency*/
		/* is not the same as billing currency.*/
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			compute(prasrec.grossprem, 2).set(add(payrpf.getOutstamt(), wsaaIncreaseDue));
			calculateTaxRelief3200(payrpf);
			compute(conlinkrec.amountOut, 2).set(add(sub(payrpf.getOutstamt(), prasrec.taxrelamt), wsaaIncreaseDue));
			compute(wsaaBillOutst, 3).setRounded(add(conlinkrec.amountOut, wsaaTax));
		}
		else {
			if (isNE(payrpf.getOutstamt(), ZERO)) {
				compute(conlinkrec.amountIn, 2).set(add(payrpf.getOutstamt(), wsaaIncreaseDue));
				callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
				prasrec.grossprem.set(conlinkrec.amountOut);
				calculateTaxRelief3200(payrpf);
				conlinkrec.amountOut.subtract(prasrec.taxrelamt);
				wsaaBillOutst.set(conlinkrec.amountOut);
				conlinkrec.amountIn.set(wsaaTax);
				callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
				wsaaBillOutst.add(conlinkrec.amountIn);
			}
		}
		/* Do not produce bext if there is enough money in the suspense*/
		/* account to cover the instalment amount.*/
		/* IF  WSAA-SUSP-AVAIL         >= CLNK-AMOUNT-OUT               */
		if (isGTE(wsaaSuspAvail, wsaaBillOutst)) {
			if (isEQ(wsaaDvdSusp, ZERO)) {
				return ;
			}
			/* Transfer LP DS to LP S                                          */
			tfrDvdSusp3700(payrpf.getBillcd(), payrpf.getPtdate());
			return ;
		}
		if (isNE(t3620rec.ddind, SPACES)
		|| isNE(t3620rec.crcind, SPACES)) {
			getPayrBankAccount1900(payrpf.getMandref());
		}
		else {
			mandIO.setBankkey(SPACES);
			mandIO.setBankacckey(SPACES);
			mandIO.setMandstat(SPACES);
			clbaddbIO.setFacthous(SPACES);
		}
		readDishonours2000(payrpf.getMandref());
		/*    PERFORM 2100-CALL-BILLREQ1.                                  */
		callDrybilreq2100(payrpf);
	}

protected void getPayrBankAccount1900(String mandRef)
	{
		/* Check that a Mandate does not exist*/
		mandIO.setPayrcoy(clrfIO.getClntcoy());
		mandIO.setPayrnum(clrfIO.getClntnum());
		mandIO.setMandref(mandRef);
		mandIO.setFunction(Varcom.readr);
		mandIO.setFormat(formatsInner.mandrec);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(mandIO.getStatuz());
			drylogrec.params.set(mandIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Move the Bank Details for Direct Debit for later*/
		/* use on the BEXT file*/
		clbaddbIO.setClntpfx("CN");
		clbaddbIO.setClntcoy(clrfIO.getClntcoy());
		clbaddbIO.setClntnum(clrfIO.getClntnum());
		clbaddbIO.setBankkey(mandIO.getBankkey());
		clbaddbIO.setBankacckey(mandIO.getBankacckey());
		clbaddbIO.setFormat(formatsInner.clbaddbrec);
		clbaddbIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, clbaddbIO);
		if (isNE(clbaddbIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(clbaddbIO.getStatuz());
			drylogrec.params.set(clbaddbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void readDishonours2000(String mandRef)
	{
		/* Check if a previously dishonoured payment exists by reading*/
		/* DDSURNL with a BEGN. If there is a key break, move the*/
		/* original MANDSTAT to BEXT-MANDSTAT, move the later of the*/
		/* DRYP-RUN-DATE or BILLCD to BILLDATE, else move DDSURNL-         */
		/* MANDSTAT to BEXT-MANDSTAT and increment the BSSC-EFFECTIVE-*/
		/* DATE by DDSURNL-LAPDAY to give BEXT-BILLDATE.*/
		Ddsupf ddsupf = new Ddsupf();//IJTI-1726
		ddsupf.setPayrcoy(clrfIO.getClntcoy().toString());
		ddsupf.setPayrnum(clrfIO.getClntnum().toString());
		ddsupf.setMandref(mandRef);
		ddsupf.setBillcd(wsaaOldBillcd.toInt());
		
		List<Ddsupf> ddsupfList = ddsupfDAO.searchDdsupfRecord(ddsupf);
		
		//IJTI-1726 starts
		if(ddsupfList.isEmpty()) {
			billreqrec.mandstat.set(mandIO.getMandstat());
			if (isGT(drypDryprcRecInner.drypRunDate, wsaaOldBillcd)) {
				billreqrec.billdate.set(drypDryprcRecInner.drypRunDate);
			}
			else {
				billreqrec.billdate.set(wsaaOldBillcd);
			}  //IJTI-1726 ends
		} else {
			Ddsupf currDdsu = ddsupfList.get(0);
			billreqrec.mandstat.set(currDdsu.getMandstat());
	    	datcon2rec.freqFactor.set(currDdsu.getLapday());
	    	datcon2rec.frequency.set("DY");
	    	datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
	    	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	    	if (isNE(datcon2rec.statuz, Varcom.oK)) {
	    		drylogrec.statuz.set(datcon2rec.statuz);
				drylogrec.params.set(datcon2rec.datcon2Rec);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
	    	}
	    	billreqrec.billdate.set(datcon2rec.intDate2);    	
		}
	}

	/**
	* <pre>
	*2100-CALL-BILLREQ1 SECTION.
	* </pre>
	*/
protected void callDrybilreq2100(Payrpf payrpf)
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3629);
		itemIO.setItemitem(payrpf.getBillcurr());
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		billreqrec.bankcode.set(t3629rec.bankcode);
		billreqrec.user.set(0);
		billreqrec.contot01.set(0);
		billreqrec.contot02.set(0);
		billreqrec.instjctl.set(SPACES);
		billreqrec.payflag.set(SPACES);
		billreqrec.bilflag.set(SPACES);
		billreqrec.outflag.set(SPACES);
		billreqrec.supflag.set("N");
		billreqrec.company.set(drypDryprcRecInner.drypCompany);
		billreqrec.fsuco.set(drypDryprcRecInner.drypFsuCompany);
		billreqrec.branch.set(drypDryprcRecInner.drypBranch);
		billreqrec.language.set(drypDryprcRecInner.drypLanguage);
		billreqrec.effdate.set(drypDryprcRecInner.drypRunDate);
		billreqrec.acctyear.set(drypDryprcRecInner.drypBatcactyr);
		billreqrec.acctmonth.set(drypDryprcRecInner.drypBatcactmn);
		billreqrec.trancode.set(drypDryprcRecInner.drypBatctrcde);
		billreqrec.batch.set(drypDryprcRecInner.drypBatcbatch);
		billreqrec.modeInd.set("AT");
		billreqrec.termid.set(SPACES);
		billreqrec.time.set(ZERO);
		billreqrec.date_var.set(ZERO);
		billreqrec.tranno.set(chdrlifIO.getTranno());
		billreqrec.chdrpfx.set(chdrlifIO.getChdrpfx());
		billreqrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		billreqrec.chdrnum.set(chdrlifIO.getChdrnum());
		billreqrec.servunit.set(chdrlifIO.getServunit());
		billreqrec.cnttype.set(chdrlifIO.getCnttype());
		billreqrec.occdate.set(chdrlifIO.getOccdate());
		billreqrec.ccdate.set(chdrlifIO.getCcdate());
		billreqrec.instcchnl.set(chdrlifIO.getCollchnl());
		billreqrec.cownpfx.set(chdrlifIO.getCownpfx());
		billreqrec.cowncoy.set(chdrlifIO.getCowncoy());
		billreqrec.cownnum.set(chdrlifIO.getCownnum());
		billreqrec.cntbranch.set(chdrlifIO.getCntbranch());
		billreqrec.agntpfx.set(chdrlifIO.getAgntpfx());
		billreqrec.agntcoy.set(chdrlifIO.getAgntcoy());
		billreqrec.agntnum.set(chdrlifIO.getAgntnum());
		billreqrec.cntcurr.set(payrpf.getCntcurr());
		billreqrec.billcurr.set(payrpf.getBillcurr());
		billreqrec.ptdate.set(payrpf.getPtdate());
		billreqrec.instto.set(payrpf.getBtdate());
		billreqrec.instbchnl.set(payrpf.getBillchnl());
		billreqrec.billchnl.set(payrpf.getBillchnl());
		billreqrec.instfreq.set(payrpf.getBillfreq());
		billreqrec.grpscoy.set(payrpf.getGrupcoy());
		billreqrec.grpsnum.set(payrpf.getGrupnum());
		billreqrec.membsel.set(payrpf.getMembsel());
		billreqrec.mandref.set(payrpf.getMandref());
		billreqrec.payrpfx.set(clrfIO.getClntpfx());
		billreqrec.payrcoy.set(clrfIO.getClntcoy());
		billreqrec.payrnum.set(clrfIO.getClntnum());
		billreqrec.facthous.set(clbaddbIO.getFacthous());
		billreqrec.bankkey.set(mandIO.getBankkey());
		billreqrec.bankacckey.set(mandIO.getBankacckey());
		billreqrec.instfrom.set(wsaaOldBtdate);
		billreqrec.btdate.set(wsaaOldBtdate);
		billreqrec.duedate.set(wsaaOldBtdate);
		billreqrec.billcd.set(wsaaOldBillcd);
		billreqrec.nextdate.set(wsaaOldNextdate);
		billreqrec.sacscode01.set(t5645rec.sacscode01);
		billreqrec.sacstype01.set(t5645rec.sacstype01);
		billreqrec.glmap01.set(t5645rec.glmap01);
		billreqrec.glsign01.set(t5645rec.sign01);
		billreqrec.sacscode02.set(t5645rec.sacscode02);
		billreqrec.sacstype02.set(t5645rec.sacstype02);
		billreqrec.glmap02.set(t5645rec.glmap02);
		billreqrec.glsign02.set(t5645rec.sign02);
		/* If BILLREQ1 needs to know the PAYER NAME then it will be looked*/
		/* up using the PAYRPFX*/
		billreqrec.payername.set(SPACES);
		if (isNE(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			currNotEqualBillcurr2200(payrpf);
		}
		else {
			if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
				gotPayrAtBtdate(payrpf);//IJTI-1726
			}
			else {
				payr(payrpf);//IJTI-1726
			}
		}
		/*    CALL 'BILLREQ1'          USING BLRQ-BILLREQ-REC.             */
		callProgram(Drybilreq.class, billreqrec.billreqRec, drypDryprcRecInner.drypDryprcRec);
		if (isNE(billreqrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(billreqrec.statuz);
			drylogrec.params.set(billreqrec.billreqRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Log number of BEXT recs created.*/
		drycntrec.contotNumber.set(controlTotalsInner.ct09);
		drycntrec.contotValue.set(billreqrec.contot01);
		d000ControlTotals();
		/* Log total amount for this BEXT rec.*/
		drycntrec.contotNumber.set(controlTotalsInner.ct10);
		drycntrec.contotValue.set(billreqrec.contot02);
		d000ControlTotals();
	}

	//IJTI-1726 starts
	protected void gotPayrAtBtdate(Payrpf payrpf) {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(billreqrec.instamt01, 2).set((add(wsaaInstPrem, wsaaIncreaseDue)));
		}
		else{
			billreqrec.instamt01.set(currPayrLif.getSinstamt01());
		}
		billreqrec.instamt01.add(wsaaIncreaseDue);
		billreqrec.instamt02.set(currPayrLif.getSinstamt02());
		billreqrec.instamt03.set(currPayrLif.getSinstamt03());
		billreqrec.instamt04.set(currPayrLif.getSinstamt04());
		billreqrec.instamt05.set(currPayrLif.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(prasrec.grossprem, 2).set((add(wsaaInstPrem, wsaaIncreaseDue)));
		}
		else{
			compute(prasrec.grossprem, 2).set((add(currPayrLif.getSinstamt06(), wsaaIncreaseDue)));
		}
		calculateTaxRelief3200(payrpf);
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(billreqrec.instamt06, 2).set(add(sub(currPayrLif.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
		}
		else{
			compute(billreqrec.instamt06, 2).set(add(sub(currPayrLif.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
		}
		
		billreqrec.instamt06.add(wsaaTax);
		billreqrec.instamt07.set(ZERO);
		billreqrec.instamt08.set(ZERO);
		billreqrec.instamt09.set(ZERO);
		billreqrec.instamt10.set(ZERO);
		billreqrec.instamt11.set(ZERO);
		billreqrec.instamt12.set(ZERO);
		billreqrec.instamt13.set(ZERO);
		billreqrec.instamt14.set(ZERO);
		billreqrec.instamt15.set(ZERO);
	}
	
	protected void payr(Payrpf payrpf) {
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt01(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(billreqrec.instamt01, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
		}
		else{
			billreqrec.instamt01.set(payrpf.getSinstamt01());
		}
		billreqrec.instamt01.add(wsaaIncreaseDue);
		billreqrec.instamt02.set(payrpf.getSinstamt02());
		billreqrec.instamt03.set(payrpf.getSinstamt03());
		billreqrec.instamt04.set(payrpf.getSinstamt04());
		billreqrec.instamt05.set(payrpf.getSinstamt05());
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(prasrec.grossprem, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
		}
		else{
			compute(prasrec.grossprem, 2).set((add(payrpf.getSinstamt06(), wsaaIncreaseDue)));
		}
		calculateTaxRelief3200(payrpf);
		compute(billreqrec.instamt06, 2).set(add(sub(payrpf.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
		if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			compute(billreqrec.instamt06, 2).set(add(sub(wsaaInstPrem, prasrec.taxrelamt), wsaaIncreaseDue));
		}
		else{
			compute(billreqrec.instamt06, 2).set(add(sub(payrpf.getSinstamt06(), prasrec.taxrelamt), wsaaIncreaseDue));
		}
		billreqrec.instamt06.add(wsaaTax);
		billreqrec.instamt07.set(ZERO);
		billreqrec.instamt08.set(ZERO);
		billreqrec.instamt09.set(ZERO);
		billreqrec.instamt10.set(ZERO);
		billreqrec.instamt11.set(ZERO);
		billreqrec.instamt12.set(ZERO);
		billreqrec.instamt13.set(ZERO);
		billreqrec.instamt14.set(ZERO);
		billreqrec.instamt15.set(ZERO);
	}
	//IJTI-1726 ends

protected void currNotEqualBillcurr2200(Payrpf payrpf)
	{
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			for (wsaaInstSub.set(1); !(isGT(wsaaInstSub, 6)); wsaaInstSub.add(1)){
				convertOldInstamts2500(payrpf.getBillcurr(), payrpf.getCntcurr());
			}
		}
		else {
			for (wsaaInstSub.set(1); !(isGT(wsaaInstSub, 6)); wsaaInstSub.add(1)){
				convertInstamts2600(payrpf);
			}
		}
		prasrec.grossprem.set(billreqrec.instamt06);
		/* ADD  WSAA-INCREASE-DUE      TO PRAS-GROSSPREM.               */
		conlinkrec.amountIn.set(wsaaTax);
		callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
		prasrec.grossprem.subtract(conlinkrec.amountOut);
		calculateTaxRelief3200(payrpf);
		compute(billreqrec.instamt06, 2).set(add(sub(billreqrec.instamt06, prasrec.taxrelamt), wsaaIncreaseDue));
		billreqrec.instamt07.set(ZERO);
		billreqrec.instamt08.set(ZERO);
		billreqrec.instamt09.set(ZERO);
		billreqrec.instamt10.set(ZERO);
		billreqrec.instamt11.set(ZERO);
		billreqrec.instamt12.set(ZERO);
		billreqrec.instamt13.set(ZERO);
		billreqrec.instamt14.set(ZERO);
		billreqrec.instamt15.set(ZERO);
	}

protected void convertOldInstamts2500(String billCurr, String cntCurr)
	{
		/*START*/
		if (isEQ(currPayrLif.getSinsAmtList().get(wsaaInstSub.toInt()), ZERO)) {//IJTI-1726
			billreqrec.instamt[wsaaInstSub.toInt()].set(ZERO);
		}
		conlinkrec.amountIn.set(currPayrLif.getSinsAmtList().get(wsaaInstSub.toInt()));//IJTI-1726
		/* The increase is only added the first time.*/
		if (isEQ(wsaaInstSub, 1)) {
			compute(conlinkrec.amountIn, 2).add(add(wsaaTax, wsaaIncreaseDue));
		}
		if (isEQ(wsaaInstSub, 6)) {
			conlinkrec.amountIn.add(wsaaIncreaseDue);
		}
		callXcvrt3100(billCurr, cntCurr);
		billreqrec.instamt[wsaaInstSub.toInt()].set(conlinkrec.amountOut);
		/*EXIT*/
	}

protected void convertInstamts2600(Payrpf payrpf)
	{
		if (isEQ(payrpf.getSinsAmtList().get(wsaaInstSub.toInt()-1), ZERO)) {
			billreqrec.instamt[wsaaInstSub.toInt()].set(ZERO);
		}
		//IJTI-1726 starts
		if (isEQ(wsaaInstSub, 1)) {
			if (reinstflag && isFoundPro) {
				compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt01(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem, leaveprem)));
				conlinkrec.amountIn.set(wsaaInstPrem.getbigdata());
			} else {
				conlinkrec.amountIn.set(payrpf.getSinstamt01());
			}
		}
		//IJTI-1726 ends
		
		conlinkrec.amountIn.set(payrpf.getSinsAmtList().get(wsaaInstSub.toInt()-1));
		/* The increase is only added the first time.*/
		if (isEQ(wsaaInstSub, 1)) {
			conlinkrec.amountIn.add(wsaaIncreaseDue);
		}
		if (isEQ(wsaaInstSub, 1)) {
			compute(conlinkrec.amountIn, 2).add(add(wsaaTax, wsaaIncreaseDue));
		}
		callXcvrt3100(payrpf.getBillcurr(), payrpf.getCntcurr());
		billreqrec.instamt[wsaaInstSub.toInt()].set(conlinkrec.amountOut);
		/*EXIT*/
	}

protected void writePtrn2700()
	{
		writePtrn2710();
	}

protected void writePtrn2710()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setParams(SPACES);
		varcom.vrcmTermid.set("9999");
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		varcom.vrcmTime.set(getCobolTime());
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setUser(ZERO);
		ptrnIO.setDataKey(drypDryprcRecInner.drypBatchKey);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		if (flexiblePremiumContract.isTrue()) {
			ptrnIO.setPtrneff(wsaaOldBtdate);
		}
		else {
			ptrnIO.setPtrneff(linspf.getInstfrom());
		}
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Log the policy transaction records produced*/
		drycntrec.contotNumber.set(controlTotalsInner.ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void rewriteChdr2800()
	{
		/*START*/
		/* Update the CHDR bill supression details*/
		/* Restore outstanding amount if this is a flexible premium*/
		if (flexiblePremiumContract.isTrue()) {
			chdrlifIO.setOutstamt(wsaaOldOutstamt);
		}
		/* Rewrite contract header*/
		chdrlifIO.setFunction(Varcom.writd);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void rewritePayr2900(Payrpf payrpf)
	{
		/*START*/
		/* Restore original outstanding amount*/
		if (flexiblePremiumContract.isTrue()) {
			payrpf.setOutstamt(wsaaOldOutstamt.getbigdata());
		}
		//IJTI-1726 starts
		if (isEQ(wsaaGotPayrAtBtdate, "Y")){ 
			if(reinstflag && isFoundPro){
				compute(wsaaInstPrem, 2).set(add(currPayrLif.getSinstamt06(), proratePrem));
				compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
				payrpf.setSinstamt06(wsaaInstPrem.getbigdata());
			}
			else{
			payrpf.setSinstamt06(currPayrLif.getSinstamt06());
			}
		}
		else if(reinstflag && isFoundPro){
			compute(wsaaInstPrem, 2).set(add(payrpf.getSinstamt06(), proratePrem));
			compute(wsaaInstPrem, 2).set(sub(wsaaInstPrem, add(increaseprem,leaveprem)));
			payrpf.setSinstamt06(wsaaInstPrem.getbigdata());
		}
		//IJTI-1726 ends
		/* Rewrite the PAYR record*/
		payrpfDAO.updatePayrpfForBilling(payrpf);

		/*EXIT*/
	}


protected void callXcvrt3100(String billCurr, String cntCurr)
	{
		conlinkrec.currIn.set(cntCurr);
		conlinkrec.currOut.set(billCurr);
		conlinkrec.rateUsed.set(0);
		conlinkrec.amountOut.set(0);
		/*   MOVE DRYP-RUN-DATE          TO CLNK-CASHDATE.                */
		conlinkrec.cashdate.set(wsaaCashdate);
		conlinkrec.function.set("SURR");
		conlinkrec.company.set(drypDryprcRecInner.drypCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(conlinkrec.statuz);
			drylogrec.params.set(conlinkrec.clnk002Rec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(billCurr);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void calculateTaxRelief3200(Payrpf payrpf)
	{
		linspf.setTaxrelmth("");//IJTI-1726
		prasrec.taxrelamt.set(ZERO);
		if (payrpf.getTaxrelmth() == null || isEQ(payrpf.getTaxrelmth(), SPACES)) {
			return ;
		}
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(payrpf.getTaxrelmth());
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6687rec.t6687Rec.set(itemIO.getGenarea());
		if (isEQ(t6687rec.taxrelsub, SPACES)) {
			return ;
		}
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium......*/
		prasrec.clntnum.set(clrfIO.getClntnum());
		prasrec.clntcoy.set(clrfIO.getClntcoy());
		prasrec.incomeSeqNo.set(payrpf.getIncomeSeqNo());
		prasrec.cnttype.set(chdrlifIO.getCnttype());
		prasrec.taxrelmth.set(payrpf.getTaxrelmth());
		prasrec.effdate.set(payrpf.getBillcd());
		prasrec.company.set(payrpf.getChdrcoy());
		prasrec.statuz.set(Varcom.oK);
		callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
		if (isNE(prasrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(prasrec.statuz);
			drylogrec.params.set(prasrec.prascalcRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* MOVE PRAS-TAXRELAMT         TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO PRAS-TAXRELAMT.               */
		if (isNE(prasrec.taxrelamt, 0)) {
			zrdecplrec.amountIn.set(prasrec.taxrelamt);
			zrdecplrec.currency.set(payrpf.getBillcurr());
			callRounding8000();
			prasrec.taxrelamt.set(zrdecplrec.amountOut);
		}
		if (isNE(prasrec.taxrelamt, 0)) {
			linspf.setTaxrelmth(payrpf.getTaxrelmth());//IJTI-1726
		}
	}

//ILPI-258
protected void writeFpco3300(List<Covrpf> covrpfList, Payrpf payrpf)
	{
		/* Read all active coverages for the contract*/
		wsaaTotAmt.set(ZERO);
		wsaaOverduePer.set(ZERO);
		//ILPI-258 starts
		
		for(Covrpf covrpf : covrpfList){
			readCovrlnb3400(covrpf, payrpf.getBillcurr());
		}
		//ILPI-258 ends
	
		/* Read the FPRM record:*/
		fprmIO.setChdrnum(chdrlifIO.getChdrnum());
		fprmIO.setChdrcoy(chdrlifIO.getChdrcoy());
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			fprmIO.setPayrseqno(payrpf.getPayrseqno());
		}
		else {
			fprmIO.setPayrseqno(payrpf.getPayrseqno());
		}
		fprmIO.setFormat(formatsInner.fprmrec);
		fprmIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), Varcom.oK) && isNE(fprmIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(fprmIO.getParams());
			drylogrec.statuz.set(fprmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaTotAmt.set(add(wsaaTotAmt, payrpf.getSinstamt02()));//IJTI-1726
		/* Update total billed:*/
		setPrecision(fprmIO.getTotalBilled(), 2);
		fprmIO.setTotalBilled(add(fprmIO.getTotalBilled(), wsaaTotAmt));
		/* Log total billed:*/
		drycntrec.contotNumber.set(controlTotalsInner.ct12);
		drycntrec.contotValue.set(wsaaTotAmt);
		d000ControlTotals();
		setPrecision(fprmIO.getMinPrmReqd(), 3);
		fprmIO.setMinPrmReqd(add((mult(div(wsaaOverduePer, 100), wsaaTotAmt)), fprmIO.getMinPrmReqd()), true);
		/* MOVE FPRM-MIN-PRM-REQD      TO ZRDP-AMOUNT-IN.               */
		/* MOVE PAYR-BILLCURR          TO ZRDP-CURRENCY.                */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO FPRM-MIN-PRM-REQD.            */
		if (isNE(fprmIO.getMinPrmReqd(), 0)) {
			zrdecplrec.amountIn.set(fprmIO.getMinPrmReqd());
			zrdecplrec.currency.set(payrpf.getBillcurr());
			callRounding8000();
			fprmIO.setMinPrmReqd(zrdecplrec.amountOut);
		}
		/* Write the record back to FPRM file.*/
		fprmIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(fprmIO.getParams());
			drylogrec.statuz.set(fprmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

//ILPI-258
protected void readCovrlnb3400(Covrpf covrpf, String billCurr)
	{
	/* Check to see if coverage is of a valid status*/
	wsaaValidCoverage.set("N");
	if (isNE(covrpf.getValidflag(), "1")) {
		return ;
	}
	for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
		if (isEQ(t5679rec.covRiskStat[ix.toInt()], covrpf.getStatcode())) {
			for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
				if (isEQ(t5679rec.covPremStat[ix.toInt()], covrpf.getPstatcode())) {
					ix.set(13);
					wsaaValidCoverage.set("Y");
				}
			}
		}
	}
	/* If the coverage is not of a valid status read the next*/
	/* record for the contract:*/
	if (!validCoverage.isTrue()) {
		return ;
	}
	if (isEQ(covrpf.getInstprem(), 0)) {
		return ;
	}
	
	Incrpf incrpf = new Incrpf();
	incrpf.setChdrcoy(covrpf.getChdrcoy());
	incrpf.setChdrnum(covrpf.getChdrnum());
	incrpf.setPlnsfx(covrpf.getPlanSuffix());
	incrpf.setCoverage(covrpf.getCoverage());
	incrpf.setRider(covrpf.getRider());
	
	List<Incrpf> incrpfList  = incrpfDAO.searchIncrpfRecord(incrpf);//IJTI-1726
	
	if (!incrpfList.isEmpty()) {//IJTI-1726
		Incrpf resultIncrpf = incrpfList.get(0);
		if(resultIncrpf != null) {
			compute(wsaaCovrInc, 2).set(sub(resultIncrpf.getNewinst(), resultIncrpf.getLastInst()));
		} else {
			wsaaCovrInc.set(ZERO);
		}
	} else {
		wsaaCovrInc.set(ZERO);
	}
	/* Select FPCO record which is active and has not reached*/
	/* Target Premium*/
	Fpcopf fpcopf = new Fpcopf();//IJTI-1726
	fpcopf.setChdrcoy(covrpf.getChdrcoy());
	fpcopf.setChdrnum(covrpf.getChdrnum());
	fpcopf.setLife(covrpf.getLife());
	fpcopf.setCoverage(covrpf.getCoverage());
	fpcopf.setRider(covrpf.getRider());
	fpcopf.setPlnsfx(covrpf.getPlanSuffix());
	List<Fpcopf> fpcopfList = fpcopfDAO.searchFpcopfRecord(fpcopf);
	List<Fpcopf> fpcoUpdateList = new ArrayList<>(fpcopfList.size());
	//IJTI-1726 starts
	for (Fpcopf fpco : fpcopfList) {
		if (isGT(fpco.getBilledp(), fpco.getPrmper()) || isEQ(fpco.getBilledp(), fpco.getPrmper())) {
			return;
		}
		setPrecision(fpco.getBilledp(), 2);
		fpco.setBilledp(add(add(fpco.getBilledp(), covrpf.getInstprem()), wsaaCovrInc).getbigdata());
		wsaaOverduePer.set(fpco.getMinovrpro());
		setPrecision(fpco.getOvrminreq(), 3);
		fpco.setOvrminreq(
				add((mult(div(wsaaOverduePer, 100), (add(covrpf.getInstprem(), wsaaCovrInc)))), fpco.getOvrminreq())
						.getbigdata());
		/* MOVE FPCO-OVERDUE-MIN TO ZRDP-AMOUNT-IN. */
		/* MOVE PAYR-BILLCURR TO ZRDP-CURRENCY. */
		/* PERFORM 8000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO FPCO-OVERDUE-MIN. */
		if (isNE(fpco.getOvrminreq(), 0)) {
			zrdecplrec.amountIn.set(fpco.getOvrminreq());
			zrdecplrec.currency.set(billCurr);
			callRounding8000();
			fpco.setOvrminreq(zrdecplrec.amountOut.getbigdata());
		}
		fpcoUpdateList.add(fpcopf);
	}
	compute(wsaaTotAmt, 2).set(add(add(wsaaTotAmt, covrpf.getInstprem()), wsaaCovrInc));
	fpcopfDAO.updateFpcoPF(fpcoUpdateList);
	//IJTI-1726 ends
	}


protected void calcLeadDays3600(String billChrnl, String billfreq)
	{
		/*  Search the array to find the subroutine for the contract       */
		/*  required. If this is not found then the contract type (CNTTYPE)*/
		/*  is replaced by '***'.                                          */
		String key;
		if(BTPRO028Permission) {
			key = billChrnl.trim() + chdrlifIO.getCnttype().toString().trim() + billfreq.trim();
			if(!readT6654(key)) {
				key = billChrnl.trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = billChrnl.trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		wsaaT6654Billchnl.set(billChrnl);
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6654);
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
		/* Call 'DATCON2' to increment the effective date by the T6654     */
		/* LEAD DAYS.                                                      */
		datcon2rec.freqFactor.set(t6654rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaEffdatePlusCntlead.set(datcon2rec.intDate2);
	}

protected boolean readT6654(String key) {
	Itempf itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl(tablesInner.t6654.toString());
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void tfrDvdSusp3700(int billCd, int ptDate)
	{
		try {
			para3700(billCd);
			writeHdiv3720(ptDate);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para3700(int billCd)
	{
		/* Determine the shortfall from the premium suspense for billing   */
		/* COMPUTE WSAA-SHORTFALL = CLNK-AMOUNT-OUT - WSAA-PREM-SUSP.   */
		compute(wsaaShortfall, 3).setRounded(sub(wsaaBillOutst, wsaaPremSusp));
		/* Shortfall < or = zero, no billing generated                     */
		if (isLTE(wsaaShortfall, 0)) {
			goTo(GotoLabel.exit3790);
		}
		else {
			/* Shortfall > dividend suspense, transfer from dividend suspense  */
			/* Note that with tolerance limit, even the shortfall > dvd-susp,  */
			/* there is a chance no billing will be necessary.                 */
			if (isGT(wsaaShortfall, wsaaDvdSusp)) {
				wsaaTfrAmt.set(wsaaDvdSusp);
			}
			else {
				/* Dividend suspense > shortfall, transfer enough for dvd-susp     */
				/* no billing required.                                            */
				wsaaTfrAmt.set(wsaaShortfall);
			}
		}
		/* Set up common fields in LIFA once only                          */
		wsaaJrnseq.set(0);
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.rdocnum.set(chdrlifIO.getChdrnum());
		lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
		lifacmvrec.tranref.set(chdrlifIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(billCd);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.trandesc.set("Dividend Transfer");
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		/*  Compare contract currency to billing and post accordingly.     */
		if (isNE(chdrlifIO.getCntcurr(), chdrlifIO.getBillcurr())) {
			conlinkrec.statuz.set(SPACES);
			conlinkrec.currIn.set(chdrlifIO.getBillcurr());
			conlinkrec.amountIn.set(wsaaTfrAmt);
			conlinkrec.cashdate.set(varcom.vrcmMaxDate);
			conlinkrec.currOut.set(chdrlifIO.getCntcurr());
			conlinkrec.company.set(chdrlifIO.getChdrcoy());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("REAL");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, "****")) {
				drylogrec.params.set(conlinkrec.clnk002Rec);
				drylogrec.statuz.set(conlinkrec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			/*     MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN            */
			/*     MOVE CHDRLIF-CNTCURR        TO ZRDP-CURRENCY             */
			/*     PERFORM 8000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT           */
			if (isNE(conlinkrec.amountOut, 0)) {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				zrdecplrec.currency.set(chdrlifIO.getCntcurr());
				callRounding8000();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
			}
			/* Transfer out Dividend Suspense                                  */
			lifacmvrec.origamt.set(conlinkrec.amountOut);
			lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			postAcmvRecord3800();
			/* Transfer Dividend into Currency Exchange Account                */
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			postAcmvRecord3800();
			/* Transfer out Dividend Suspense from Currency Exchange Account   */
			lifacmvrec.origamt.set(wsaaTfrAmt);
			lifacmvrec.origcurr.set(chdrlifIO.getBillcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode05);
			lifacmvrec.sacstyp.set(t5645rec.sacstype05);
			lifacmvrec.glcode.set(t5645rec.glmap05);
			lifacmvrec.glsign.set(t5645rec.sign05);
			lifacmvrec.contot.set(t5645rec.cnttot05);
			postAcmvRecord3800();
			/* Transfer Dividend Suspense into Premium Suspense                */
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			postAcmvRecord3800();
			/* Set WSAA-TFR-AMT to converted amount in contract currency       */
			wsaaTfrAmt.set(conlinkrec.amountOut);
		}
		else {
			/* Transfer out Dividend Suspense                                  */
			lifacmvrec.origamt.set(wsaaTfrAmt);
			lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			postAcmvRecord3800();
			/* Transfer Dividend Suspense into Premium Suspense                */
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
			postAcmvRecord3800();
		}
	}

protected void writeHdiv3720(int ptDate)
	{
		/* Now write HDIV to denote the withdrawal of dividend at coverage */
		/* level.  So spread the amount across all the coverages according */
		/* to their share.                                                 */
		initialize(wsaaHdisArrayInner.wsaaHdisArray);
		
		Hdispf hdispf = new Hdispf();
		hdispf.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		hdispf.setChdrnum(chdrlifIO.getChdrnum().toString());
		List<Hdispf> hdispfList = hdispfDAO.searchHdispfRecord(hdispf);
		//IJTI-1726 starts
		for (Hdispf hdis : hdispfList) {
			processHdis3900(hdis);
		}
		//IJTI-1726 ends
		
		wsaaIdx.set(ZERO);
		wsaaAllDvdTot.set(ZERO);

		/* Set total no. of HDIS read                                      */
		wsaaNoOfHdis.set(wsaaIdx);
		/* Then calculate share of each coverage on the dividend           */
		wsaaIdx.set(ZERO);
		while ( !(isGTE(wsaaIdx, wsaaNoOfHdis))) {
			wsaaIdx.add(1);
			if (isEQ(wsaaIdx, wsaaNoOfHdis)) {
				compute(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 2).set(sub(wsaaTfrAmt, wsaaRunDvdTot));
			}
			else {
				compute(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 2).set(mult(wsaaTfrAmt, (div(wsaaHdisArrayInner.wsaaDvdTot[wsaaIdx.toInt()], wsaaAllDvdTot))));
				wsaaRunDvdTot.add(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()]);
			}
			/*     MOVE WSAA-DVD-SHARE(WSAA-IDX) TO ZRDP-AMOUNT-IN          */
			/*     MOVE CHDRLIF-CNTCURR        TO ZRDP-CURRENCY             */
			/*     PERFORM 8000-CALL-ROUNDING                               */
			/*     MOVE ZRDP-AMOUNT-OUT        TO WSAA-DVD-SHARE(WSAA-IDX)  */
			if (isNE(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], 0)) {
				zrdecplrec.amountIn.set(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()]);
				zrdecplrec.currency.set(chdrlifIO.getCntcurr());
				callRounding8000();
				wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()].set(zrdecplrec.amountOut);
			}
		}

		/* Write withdrawn amount for each coverage                        */
		wsaaIdx.set(ZERO);
		while ( !(isGTE(wsaaIdx, wsaaNoOfHdis))) {
			wsaaIdx.add(1);
			hdivIO.setDataArea(SPACES);
			hdivIO.setChdrcoy(chdrlifIO.getChdrcoy());
			hdivIO.setChdrnum(chdrlifIO.getChdrnum());
			hdivIO.setLife(wsaaHdisArrayInner.wsaaHdisLife[wsaaIdx.toInt()]);
			hdivIO.setJlife(wsaaHdisArrayInner.wsaaHdisJlife[wsaaIdx.toInt()]);
			hdivIO.setCoverage(wsaaHdisArrayInner.wsaaHdisCoverage[wsaaIdx.toInt()]);
			hdivIO.setRider(wsaaHdisArrayInner.wsaaHdisRider[wsaaIdx.toInt()]);
			hdivIO.setPlanSuffix(wsaaHdisArrayInner.wsaaHdisPlnsfx[wsaaIdx.toInt()]);
			hdivIO.setTranno(chdrlifIO.getTranno());
			hdivIO.setEffdate(ptDate);
			hdivIO.setDivdAllocDate(ptDate);
			hdivIO.setDivdIntCapDate(wsaaHdisArrayInner.wsaaNextCapDate[wsaaIdx.toInt()]);
			hdivIO.setCntcurr(chdrlifIO.getCntcurr());
			setPrecision(hdivIO.getDivdAmount(), 0);
			hdivIO.setDivdAmount(mult(wsaaHdisArrayInner.wsaaDvdShare[wsaaIdx.toInt()], -1));
			hdivIO.setDivdRate(ZERO);
			hdivIO.setDivdRtEffdt(varcom.vrcmMaxDate);
			hdivIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
			hdivIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
			hdivIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
			hdivIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
			hdivIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
			hdivIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
			hdivIO.setDivdType("C");
			hdivIO.setZdivopt(wsaaHdisArrayInner.wsaaZdivopt[wsaaIdx.toInt()]);
			hdivIO.setZcshdivmth(wsaaHdisArrayInner.wsaaZcshdivmth[wsaaIdx.toInt()]);
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFormat(formatsInner.hdivrec);
			hdivIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, hdivIO);
			if (isNE(hdivIO.getStatuz(), Varcom.oK)) {
				drylogrec.params.set(hdivIO.getParams());
				drylogrec.statuz.set(hdivIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}

	}

protected void postAcmvRecord3800()
	{
		post3810();
	}

protected void post3810()
	{
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		/*    MOVE 'PSTW'                 TO LIFA-FUNCTION.        <DRYAPL>*/
		lifacmvrec.function.set("PSTD");
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void processHdis3900(Hdispf hdis)
	{

		/* Skip validflag not '1'                                          */
		if (isNE(hdis.getValidflag(), "1")) {
			return ;
		}
		wsaaIdx.add(1);
		wsaaHdisArrayInner.wsaaHdisLife[wsaaIdx.toInt()].set(hdis.getLife());
		wsaaHdisArrayInner.wsaaHdisCoverage[wsaaIdx.toInt()].set(hdis.getCoverage());
		wsaaHdisArrayInner.wsaaHdisRider[wsaaIdx.toInt()].set(hdis.getRider());
		wsaaHdisArrayInner.wsaaHdisPlnsfx[wsaaIdx.toInt()].set(hdis.getPlnsfx());
		wsaaHdisArrayInner.wsaaHdisJlife[wsaaIdx.toInt()].set(hdis.getJlife());
		wsaaHdisArrayInner.wsaaNextCapDate[wsaaIdx.toInt()].set(hdis.getHcapndt());
		
		Hcsdpf hcsdpf = new Hcsdpf();
		hcsdpf.setChdrcoy(hdis.getChdrcoy());
		hcsdpf.setChdrnum(hdis.getChdrnum());
		hcsdpf.setChdrnum(hdis.getLife());
		hcsdpf.setChdrnum(hdis.getCoverage());
		hcsdpf.setChdrnum(hdis.getRider());
		hcsdpf.setPlnsfx(0);
		
		List<Hcsdpf> hcsdpfList = hcsdpfDAO.searchHcsdpfRecord(hcsdpf);
		//IJTI-1726 starts
		for (Hcsdpf hcsd : hcsdpfList) {
			wsaaHdisArrayInner.wsaaZdivopt[wsaaIdx.toInt()].set(hcsd.getZdivopt());
			wsaaHdisArrayInner.wsaaZcshdivmth[wsaaIdx.toInt()].set(hcsd.getZcshdivmth());
		}
		//IJTI-1726 ends
		
		
		wsaaHdisArrayInner.wsaaDvdTot[wsaaIdx.toInt()].add(PackedDecimalData.parseObject(hdis.getHdvbalst()));
		wsaaAllDvdTot.add(PackedDecimalData.parseObject(hdis.getHdvbalst()));//IJTI-1726
		/*    Read through HDIVCSH from last interest date of HDIS,        */
		/*    calculate interest on the withdrawn dividend, accumulate     */
		/*    to the total dividend of the coverage                        */
		Hdivpf hdivpf = new Hdivpf();
		hdivpf.setChdrcoy(hdis.getChdrcoy());
		hdivpf.setChdrnum(hdis.getChdrnum());
		hdivpf.setLife(hdis.getLife());
		hdivpf.setCoverage(hdis.getCoverage());
		hdivpf.setRider(hdis.getRider());
		hdivpf.setPlnsfx(hdis.getPlnsfx());
		compute(wsaaLastCapDate, 0).set(add(1, hdis.getHcapldt()));
		hdivpf.setHincapdt(wsaaLastCapDate.toInt());
		
		
		List<Hdivpf> hdivpfList = hdivpfDAO.searchHdivpfRecord(hdivpf);
		if (hdivpfList != null && hdivpfList.size() > 0) {
			for (Hdivpf hdiv : hdivpfList) {
				wsaaHdisArrayInner.wsaaDvdTot[wsaaIdx.toInt()].add(PackedDecimalData.parseObject(hdiv.getHdvamt()));
			}
		}
	}

protected void writeLetter4000()
	{
		start4000();
		readTr3844020();
	}

protected void start4000()
	{
		/* Read Table TR384 for get letter-type.                           */
		wsaaItemBatctrcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaItemCnttype.set(chdrlifIO.getCnttype());
	}

protected void readTr3844020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.tr384);
		itemIO.setItemitem(wsaaItemTr384);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If record not found then read again using generic key.          */
		if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isEQ(itemIO.getStatuz(), Varcom.mrnf)
			&& isNE(wsaaItemCnttype, "***")) {
				wsaaItemCnttype.set("***");
				readTr3844020();
				return ;
			}
			else {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*   Get set-up parameter for call 'HLETRQS'                   */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlifIO.getChdrcoy());
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(drypDryprcRecInner.drypRunDate);
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.otherKeys.set(drypDryprcRecInner.drypLanguage);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, Varcom.oK)) {
			/*      MOVE LETRQST-PARAMS     TO DRYL-PARAMS                   */
			/*      MOVE LETRQST-STATUZ     TO DRYL-STATUZ                   */
			/*      SET  DRY-DATABASE-ERROR TO TRUE                          */
			/*      PERFORM A000-FATAL-ERROR                                 */
			return ;
		}
	}

//ILPI-258
protected void calcTax4100(List<Covrpf> covrpfList, Payrpf payrpf)
	{
		wsaaTax.set(0);
		if (flexiblePremiumContract.isTrue()
		|| isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}

		
		String crTable = "";
		for(Covrpf covrpf : covrpfList){
			crTable = covrpf.getCrtable();
			processCovrTax4200(covrpf, payrpf);
		}
	
		if (isGT(payrpf.getSinstamt02(), 0)) {
			processCtfeeTax4300(crTable, payrpf);
		}
		setPrecision(payrpf.getOutstamt(), 2);
		payrpf.setOutstamt(add(payrpf.getOutstamt(), wsaaTax).getbigdata());
		setPrecision(chdrlifIO.getOutstamt(), 2);
		chdrlifIO.setOutstamt(add(chdrlifIO.getOutstamt(), wsaaTax));
	}

//ILPI-258
protected void processCovrTax4200(Covrpf covrpf, Payrpf payrpf)
	{
		/* Calculate tax on premiums                                       */
		wsaaValidCovr.set("N");

		/* Check to see CURRFROM <= PAYR-BTDATE & CURRTO > PAYR-BTDATE     */
		if (isLTE(covrpf.getCurrfrom(), payrpf.getBtdate())
		&& isGT(covrpf.getCurrto(), payrpf.getBtdate())) {
			/*NEXT_SENTENCE*/
		}
		else {
			return ;
		}
		/* If installment premium = zero, exit                             */
		if (isEQ(covrpf.getInstprem(), ZERO)) {
			return ;
		}
		//IJTI-1726 starts
		if (reinstflag && isFoundPro) {
			isShortTax = Boolean.FALSE;
			readTd5j2(covrpf.getCrtable());/* IJTI-1386 */
			if (isNE(td5j2rec.td5j2Rec, SPACES) && isEQ(td5j2rec.shortTerm, "Y")) {
				if (isEQ(lapsePTDate, wsaaOldBtdate)) {
					isShortTax = Boolean.TRUE;
				} else {
					return;
				}
			}
		}
		//IJTI-1726 ends
		/* Check to see if coverage is of a valid status                   */
		if (isNE(covrpf.getValidflag(), "1")) {
			return ;
		}
		invalidCovr.setTrue();
		for (ix.set(1); !(isGT(ix, 12)
		|| validCovr.isTrue()); ix.add(1)){
			if (isEQ(t5679rec.covRiskStat[ix.toInt()], covrpf.getStatcode())) {
				for (ix.set(1); !(isGT(ix, 12)
				|| validCovr.isTrue()); ix.add(1)){
					if (isEQ(t5679rec.covPremStat[ix.toInt()], covrpf.getPstatcode())) {
						validCovr.setTrue();
					}
				}
			}
		}
		/*  If the coverage is not of a valid status read the next         */
		/*  record for the contract:                                       */
		if (!validCovr.isTrue()) {
			return ;
		}
		/*  Read table TR52E.                                              */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set(covrpf.getCrtable());
		a1200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			a1200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a1200ReadTr52e();
		}
		/*  Check if component found in INCR where INCR-CRRCD < PAYR-BTDATE*/
		/*  and validflag = '1'                                            */
		Boolean incrFoundPro = Boolean.FALSE;//IJTI-1726
		incrIO.setDataArea(SPACES);
		incrIO.setChdrcoy(covrpf.getChdrcoy());
		incrIO.setChdrnum(covrpf.getChdrnum());
		incrIO.setLife(covrpf.getLife());
		incrIO.setCoverage(covrpf.getCoverage());
		incrIO.setRider(covrpf.getRider());
		incrIO.setPlanSuffix(covrpf.getPlanSuffix());
		incrIO.setFormat(formatsInner.incrrec);
		incrIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, incrIO);
		if (isNE(incrIO.getStatuz(), Varcom.oK)
		&& isNE(incrIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(incrIO.getParams());
			drylogrec.statuz.set(incrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(incrIO.getChdrcoy(), covrpf.getChdrcoy())
		&& isEQ(incrIO.getChdrnum(), covrpf.getChdrnum())
		&& isEQ(incrIO.getLife(), covrpf.getLife())
		&& isEQ(incrIO.getCoverage(), covrpf.getCoverage())
		&& isEQ(incrIO.getRider(), covrpf.getRider())
		&& isEQ(incrIO.getPlanSuffix(), covrpf.getPlanSuffix())
		&& isNE(incrIO.getStatuz(), Varcom.endp)
		&& isEQ(incrIO.getValidflag(), "1")
		&& isLT(incrIO.getCrrcd(), payrpf.getBtdate())) {
			incrFoundPro = Boolean.TRUE;//IJTI-1726
			compute(wsaaIncrBinstprem, 3).setRounded(sub(incrIO.getZbnewinst(), incrIO.getZblastinst()));
			compute(wsaaIncrInstprem, 3).setRounded(sub(incrIO.getNewinst(), incrIO.getLastInst()));
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.transType.set("PREM");
		txcalcrec.chdrcoy.set(covrpf.getChdrcoy());
		txcalcrec.chdrnum.set(covrpf.getChdrnum());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		wsaaCntCurr.set(chdrlifIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.amountIn.set(ZERO);
		//IJTI-1726 starts
		if(isFoundPro){
			txcalcrec.life.set(covrpf.getLife());
			txcalcrec.coverage.set(covrpf.getCoverage());
			txcalcrec.rider.set(covrpf.getRider());
			txcalcrec.planSuffix.set(covrpf.getPlanSuffix());
			txcalcrec.crtable.set(covrpf.getCrtable());
			txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		}
		if (isEQ(tr52erec.zbastyp, "Y")) {
			if(isFoundPro && incrFoundPro){
				compute(txcalcrec.amountIn, 3).setRounded(add(incrIO.getZbnewinst(), wsaaIncrInstprem));
			}
			else{
				compute(txcalcrec.amountIn, 3).setRounded(add(covrpf.getZbinstprem(), wsaaIncrBinstprem));
			}
		}
		else {
			if (isShortTax) {
				compute(txcalcrec.amountIn, 3)
						.setRounded(add(reratedPremium, wsaaIncrInstprem));
			} else {
				if (isFoundPro && incrFoundPro) {
					compute(txcalcrec.amountIn, 3).setRounded(add(incrIO.getNewinst(), wsaaIncrInstprem));
				} else {
					compute(txcalcrec.amountIn, 3).setRounded(add(covrpf.getInstprem(), wsaaIncrInstprem));
				}
			}
		}
		if(isShortTax){
			txcalcrec.effdate.set(ptrnpfReinstate.getDatesub());
		} else {
			txcalcrec.effdate.set(wsaaOldBtdate);
		}
		//IJTI-1726 ends
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			drylogrec.params.set(txcalcrec.linkRec);
			drylogrec.statuz.set(txcalcrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/*  Create TAXD record                                             */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(covrpf.getChdrcoy());
		taxdIO.setChdrnum(covrpf.getChdrnum());
		taxdIO.setTrantype("PREM");
		taxdIO.setLife(covrpf.getLife());
		taxdIO.setCoverage(covrpf.getCoverage());
		taxdIO.setRider(covrpf.getRider());
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setInstto(payrpf.getBtdate());
		taxdIO.setBillcd(wsaaOldBillcd);
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrlifIO.getTranno());
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(taxdIO.getParams());
			drylogrec.statuz.set(taxdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

//ILPI-258
protected void processCtfeeTax4300(String crTable, Payrpf payrpf)
	{
		/* Calculate tax on contract fee.                                  */
		/* Read table TR52E.                                               */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set(crTable);
		a1200ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			a1200ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a1200ReadTr52e();
		}
		/*  If TR52E tax indicator2 not 'Y', do not calculate tax          */
		if (isNE(tr52erec.taxind02, "Y")) {
//			covrlnbIO.setFunction(varcom.nextr);
			return ;
		}
		/*  Set values to tax linkage and call tax subroutine              */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.transType.set("CNTF");
		txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		wsaaCntCurr.set(chdrlifIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.txcode.set(tr52drec.txcode);
		txcalcrec.effdate.set(wsaaOldBtdate);
		if (isEQ(wsaaGotPayrAtBtdate, "Y")) {
			txcalcrec.amountIn.set(currPayrLif.getSinstamt02());//IJTI-1726 ends
		}
		else {
			txcalcrec.amountIn.set(payrpf.getSinstamt02());
		}
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			drylogrec.params.set(txcalcrec.linkRec);
			drylogrec.statuz.set(txcalcrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
		/* Create TAXD record                                              */
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrlifIO.getChdrcoy());
		taxdIO.setChdrnum(chdrlifIO.getChdrnum());
		taxdIO.setTrantype("CNTF");
		taxdIO.setLife(SPACES);
		taxdIO.setCoverage(SPACES);
		taxdIO.setRider(SPACES);
		taxdIO.setPlansfx(ZERO);
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setInstfrom(txcalcrec.effdate);
		taxdIO.setInstto(payrpf.getBtdate());
		taxdIO.setBillcd(wsaaOldBillcd);
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		taxdIO.setTranref(wsaaTranref);
		taxdIO.setTranno(chdrlifIO.getTranno());
		taxdIO.setBaseamt(txcalcrec.amountIn);
		taxdIO.setTaxamt01(txcalcrec.taxAmt[1]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[2]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[1]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[2]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[1]);
		taxdIO.setTxtype02(txcalcrec.taxType[2]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg(SPACES);
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(taxdIO.getParams());
			drylogrec.statuz.set(taxdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			a000FatalError();
		}
		/*EXIT*/
	}

protected void a1000GetCurrConvDate(Payrpf payrpf)
	{
		/* If Contract and Billing currency is not the same :              */
		/* Subtract the lead days from the current BILLCD to get a date    */
		/* for currency conversion. This date will be used when calling    */
		/* XCVRT subroutine. For normal case this date = DRYP-RUN-DATE     */
		wsaaCashdate.set(varcom.vrcmMaxDate);
		if (isEQ(payrpf.getCntcurr(), payrpf.getBillcurr())) {
			return ;
		}
		compute(datcon2rec.freqFactor, 0).set(sub(ZERO, t6654rec.leadDays));
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(payrpf.getBillcd());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaCashdate.set(datcon2rec.intDate2);
		if (isLT(wsaaCashdate, chdrlifIO.getOccdate())) {
			wsaaCashdate.set(chdrlifIO.getOccdate());
		}
	}




protected void a1200ReadTr52e()
	{
		a1210Start();
	}

protected void a1210Start()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		Itempf itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(tablesInner.tr52e.toString());
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemitem(wsaaTr52eKey.toString());
		BigDecimal oldBtDate = wsaaOldBtdate.getbigdata();
		itempf.setItmfrm(oldBtDate);
		itempf.setItmto(oldBtDate);
		
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				tr52erec.tr52eRec.set(StringUtil.rawToString(it.getGenarea()));				
		}
		}
		
		
	}

	//IJTI-1726 starts
	private Boolean isUpdateNlgt(String item) {
		Boolean isUpdateNlgt = Boolean.FALSE;
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6654);
		itemIO.setItemitem(item);
		itemIO.setFunction(Varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
		&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		
		if(isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			isUpdateNlgt = Boolean.FALSE;
		}
		
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		
		if (t6654rec.daexpy01.toInt() == 0 && t6654rec.daexpy02.toInt() == 0 && t6654rec.daexpy03.toInt() == 0) {
			isUpdateNlgt = Boolean.TRUE;
		}
		return isUpdateNlgt;
	}
	
	/**
	 * Calls Nlgcalc
	 * 
	 * @param payrpf - Payrpf record
	 */
	private void callNlgcalc(Payrpf payrpf) {
		nlgcalcrec.chdrcoy.set(ptrnIO.getChdrcoy());
		nlgcalcrec.chdrnum.set(ptrnIO.getChdrnum());
		nlgcalcrec.tranno.set(ptrnIO.getTranno());
		nlgcalcrec.effdate.set(ptrnIO.getPtrneff());
		nlgcalcrec.batcactyr.set(ptrnIO.getBatcactyr());
		nlgcalcrec.batcactmn.set(ptrnIO.getBatcactmn());
		nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
		nlgcalcrec.cnttype.set(chdrlifIO.getCnttype());
		nlgcalcrec.language.set(drypDryprcRecInner.drypLanguage);
		nlgcalcrec.frmdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.occdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.todate.set(varcom.vrcmMaxDate);
		nlgcalcrec.ptdate.set(chdrlifIO.getPtdate());
		if (isLT(payrpf.getBtdate(), payrpf.getEffdate())) {
			nlgcalcrec.inputAmt.set(currPayrLif.getSinstamt01());
		} else {
			nlgcalcrec.inputAmt.set(payrpf.getSinstamt01());
		}
		nlgcalcrec.inputAmt.add(wsaaIncreaseDue);
		compute(nlgcalcrec.inputAmt, 0).set(mult(-1, nlgcalcrec.inputAmt));
		nlgcalcrec.function.set("OVDUE");
		callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
		if (isNE(nlgcalcrec.status, Varcom.oK)) {
			drylogrec.statuz.set(nlgcalcrec.status);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}
	
	/**
	 * Injection point for Billing Notice SMS Notification.
	 */
	protected void sendBillingNoticeSmsNotification() {
		//default blank implementation for Billing Notice SMS Notification
	}
	//IJTI-1726 ends

	protected void finish()	{
		tr517Map.clear();
		tr517Map = null;
		/* Update the DRYP fields to determine next processing date.*/
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypAplsupto.set(ZERO);
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
	private FixedLengthStringData mandrec = new FixedLengthStringData(10).init("MANDREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData clbaddbrec = new FixedLengthStringData(10).init("CLBADDBREC");
	private FixedLengthStringData ddsurnlrec = new FixedLengthStringData(10).init("DDSURNLREC");
	private FixedLengthStringData incrrgprec = new FixedLengthStringData(10).init("INCRRGPREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
	private FixedLengthStringData payrlifrec = new FixedLengthStringData(10).init("PAYRLIFREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	private FixedLengthStringData incrrec = new FixedLengthStringData(10).init("INCRREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
	private FixedLengthStringData t3629 = new FixedLengthStringData(6).init("T3629");
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
}
/*
 * Class transformed  from Data Structure WSAA-HDIS-ARRAY--INNER
 */
private static final class WsaaHdisArrayInner {

	private FixedLengthStringData wsaaHdisArray = new FixedLengthStringData(6138);
	private FixedLengthStringData[] wsaaHdisRec = FLSArrayPartOfStructure(99, 62, wsaaHdisArray, 0);
	private FixedLengthStringData[] wsaaHdisLife = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 0);
	private FixedLengthStringData[] wsaaHdisCoverage = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 2);
	private FixedLengthStringData[] wsaaHdisRider = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 4);
	private FixedLengthStringData[] wsaaHdisJlife = FLSDArrayPartOfArrayStructure(2, wsaaHdisRec, 6);
	private ZonedDecimalData[] wsaaHdisPlnsfx = ZDArrayPartOfArrayStructure(4, 0, wsaaHdisRec, 8, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaZdivopt = FLSDArrayPartOfArrayStructure(4, wsaaHdisRec, 12);
	private FixedLengthStringData[] wsaaZcshdivmth = FLSDArrayPartOfArrayStructure(4, wsaaHdisRec, 16);
	private ZonedDecimalData[] wsaaNextCapDate = ZDArrayPartOfArrayStructure(8, 0, wsaaHdisRec, 20);
	private ZonedDecimalData[] wsaaDvdTot = ZDArrayPartOfArrayStructure(17, 7, wsaaHdisRec, 28);
	private ZonedDecimalData[] wsaaDvdShare = ZDArrayPartOfArrayStructure(17, 7, wsaaHdisRec, 45);
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
}