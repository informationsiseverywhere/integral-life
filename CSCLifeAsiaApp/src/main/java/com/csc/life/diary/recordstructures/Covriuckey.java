package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:45
 * Description:
 * Copybook name: COVRIUCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covriuckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covriucFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covriucKey = new FixedLengthStringData(256).isAPartOf(covriucFileKey, 0, REDEFINE);
  	public FixedLengthStringData covriucChdrcoy = new FixedLengthStringData(1).isAPartOf(covriucKey, 0);
  	public FixedLengthStringData covriucChdrnum = new FixedLengthStringData(8).isAPartOf(covriucKey, 1);
  	public PackedDecimalData covriucInitUnitCancDate = new PackedDecimalData(8, 0).isAPartOf(covriucKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(covriucKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covriucFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covriucFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}