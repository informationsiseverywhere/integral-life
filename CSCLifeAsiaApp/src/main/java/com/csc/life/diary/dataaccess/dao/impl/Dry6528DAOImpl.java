package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry6528DAO;
import com.csc.life.diary.dataaccess.model.Dry6528Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation To Fetch Records For Benefit Billing Processing
 * 
 * @author ptrivedi8
 *
 */
public class Dry6528DAOImpl extends BaseDAOImpl<Dry6528Dto> implements Dry6528DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dry6528DAOImpl.class);

	/**
	 * Constructor
	 */
	public Dry6528DAOImpl() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.life.diary.dataaccess.dao.Dry6528DAO#getCovrRecords(java.lang.String,
	 * int, java.lang.String)
	 */
	@Override
	public List<Dry6528Dto> getCovrRecords(String company, int effDate, String chdrNum) {

		StringBuilder sql = new StringBuilder("SELECT  CHDRCOY, CHDRNUM,");
		sql.append(" LIFE, COVERAGE, RIDER, PLNSFX, JLIFE, PRMCUR, BBLDAT, CRRCD,");
		sql.append(" PCESDTE, MORTCLS, CRTABLE, STATCODE, PSTATCODE, SUMINS FROM COVRPF WHERE");
		sql.append(" CHDRCOY = ? AND BCESDTE > BBLDAT AND BBLDAT <> ? AND VALIDFLAG = ?");
		sql.append(" AND BBLDAT <= ? AND CHDRNUM = ? ORDER BY CHDRCOY, CHDRNUM");

		List<Dry6528Dto> dry6528DtoList = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, company);
			ps.setInt(2, 0);
			ps.setString(3, "1");
			ps.setInt(4, effDate);
			ps.setString(5, chdrNum);
			rs = ps.executeQuery();
			while (rs.next()) {
				Dry6528Dto dry6528Dto = new Dry6528Dto();
				dry6528Dto.setChdrCoy(rs.getString("CHDRCOY"));
				dry6528Dto.setChdrNum(rs.getString("CHDRNUM"));
				dry6528Dto.setLife(rs.getString("LIFE"));
				dry6528Dto.setCoverage(rs.getString("COVERAGE"));
				dry6528Dto.setRider(rs.getString("RIDER"));
				dry6528Dto.setPlanSuffix(rs.getInt("PLNSFX"));
				dry6528Dto.setJlife(rs.getString("JLIFE"));
				dry6528Dto.setPremCurrency(rs.getString("PRMCUR"));
				dry6528Dto.setBenBillDate(rs.getInt("BBLDAT"));
				dry6528Dto.setCrrcd(rs.getInt("CRRCD"));
				dry6528Dto.setPremCessDate(rs.getInt("PCESDTE"));
				dry6528Dto.setMortcls(rs.getString("MORTCLS"));
				dry6528Dto.setCrTable(rs.getString("CRTABLE"));
				dry6528Dto.setStatCode(rs.getString("STATCODE"));
				dry6528Dto.setPstatCode(rs.getString("PSTATCODE"));
				dry6528Dto.setSumins(rs.getBigDecimal("SUMINS"));

				dry6528DtoList.add(dry6528Dto);
			}
		} catch (SQLException e) {
			LOGGER.error("Error occured when reading COVRPF records", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dry6528DtoList;
	}

}
