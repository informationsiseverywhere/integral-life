/*
 * File: Zbwagny.java
 * Date: 30 August 2009 2:55:15
 * Author: Quipoz Limited
 * 
 * Class transformed from ZBWAGNY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.recordstructures.Zbwagnyrec;
import com.csc.life.agents.tablestructures.Tr663rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.dataaccess.BprdTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*                       BONUS WORKBENCH COMPANY
*                      =========================
* INITIALISATION
* --------------
* (1) IN THE 000-MAINLINE SECTION, INITIALIZE THE ZBWA-STATUZ
*     & ZBWA-AGENCY AND ZBWA-AGENCY-DESC AS 'EROR',SPACES AND
*     SPACES RESPECTIVELY.
* (2) THEN, DETERMINE THE ZBWA-FUNCTION; IF 'POL', READ THE
*     POLICY HEADER FILE CHDRENQ TO GET THE AGENT NUMBER.
*     AFTERWARDS, READ AGLF & AGNT TO RETRIEVE THE AGENT TYPE
*     AND COMMISSION CLASS.              WSSP
* (3) READ THE TABLE TR663 BY THE KEY OF THE SIGN ON COMPANY
* (4) DETERMINE THE AGENCY PARAMETER TO DEFINE THE AGENCY AND
*     ITS DESCRIPTION.
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Zbwagny extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "ZBWAGNY";
	private String wsaaEror = "EROR";
		/* WSAA-AGENT */
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).init(SPACES);
	private String chdrenqrec = "CHDRENQREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String tr663 = "TR663";
	private String t5696 = "T5696";
	private String tt518 = "TT518";
	private String t1692 = "T1692";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Process Schedule Rules Multi Thread Batc*/
	private BprdTableDAM bprdIO = new BprdTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr663rec tr663rec = new Tr663rec();
	private Varcom varcom = new Varcom();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Zbwagnyrec zbwagnyrec = new Zbwagnyrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		seExit8090, 
		dbExit8190
	}

	public Zbwagny() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zbwagnyrec.bwagnyRec = convertAndSetParam(zbwagnyrec.bwagnyRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		zbwagnyrec.statuz.set(wsaaEror);
		zbwagnyrec.agency.set(SPACES);
		zbwagnyrec.agencyDesc.set(SPACES);
		if (isEQ(zbwagnyrec.function,"POL")){
			getAgentnumBypolicynum1000();
			getAgentinfor2000();
		}
		else if (isEQ(zbwagnyrec.function,"AGT")){
			wsaaAgntnum.set(zbwagnyrec.input);
			getAgentinfor2000();
		}
		else{
			zbwagnyrec.statuz.set("INVF");
			goTo(GotoLabel.exit090);
		}
		readTr6633000();
		zbwagnyrec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void getAgentnumBypolicynum1000()
	{
		/*PARA*/
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setChdrcoy(zbwagnyrec.signonCompany);
		chdrenqIO.setChdrnum(zbwagnyrec.input);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
		}
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		dbError8100();
		wsaaAgntnum.set(chdrenqIO.getAgntnum());
	}

protected void getAgentinfor2000()
	{
		para2000();
	}

protected void para2000()
	{
		agntIO.setDataArea(SPACES);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(zbwagnyrec.signonCompany);
		agntIO.setAgntnum(wsaaAgntnum);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntIO.getParams());
			syserrrec.statuz.set(agntIO.getStatuz());
			dbError8100();
		}
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(zbwagnyrec.signonCompany);
		aglfIO.setAgntnum(wsaaAgntnum);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			dbError8100();
		}
	}

protected void readTr6633000()
	{
		para3000();
	}

protected void para3000()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(zbwagnyrec.signonCompany);
		itemIO.setItemtabl(tr663);
		itemIO.setItemitem(zbwagnyrec.signonCompany);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		tr663rec.tr663Rec.set(itemIO.getGenarea());
		if (isEQ(tr663rec.zparam,"AC")){
			zbwagnyrec.agency.set(aglfIO.getAracde());
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(t5696);
			descIO.setDescitem(aglfIO.getAracde());
			descioCall3100();
			zbwagnyrec.agencyDesc.set(descIO.getLongdesc());
		}
		else if (isEQ(tr663rec.zparam,"SU")){
			zbwagnyrec.agency.set(aglfIO.getTsalesunt());
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(tt518);
			descIO.setDescitem(aglfIO.getTsalesunt());
			descioCall3100();
			zbwagnyrec.agencyDesc.set(descIO.getLongdesc());
		}
		else if (isEQ(tr663rec.zparam,"BR")){
			zbwagnyrec.agency.set(agntIO.getAgntbr());
			descIO.setDataKey(SPACES);
			descIO.setDesctabl(t1692);
			descIO.setDescitem(agntIO.getAgntbr());
			descioCall3100();
			zbwagnyrec.agencyDesc.set(descIO.getLongdesc());
		}
	}

protected void descioCall3100()
	{
		description3110();
	}

protected void description3110()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(zbwagnyrec.signonCompany);
		descIO.setLanguage(zbwagnyrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			dbError8100();
		}
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		zbwagnyrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		zbwagnyrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
