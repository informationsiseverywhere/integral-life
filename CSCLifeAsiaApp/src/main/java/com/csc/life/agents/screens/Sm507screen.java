package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm507screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm507ScreenVars sv = (Sm507ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm507screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm507ScreenVars screenVars = (Sm507ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.agnum.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.agtydesc.setClassString("");
		screenVars.agbrdesc.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.repname.setClassString("");
		screenVars.accountType.setClassString("");
		screenVars.exclAgmt.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.reportag.setClassString("");
		screenVars.gacc.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.tydesc.setClassString("");
		screenVars.reportton.setClassString("");
		screenVars.repsel.setClassString("");
		screenVars.agntfm.setClassString("");
		screenVars.descn.setClassString("");
		screenVars.mlprcind.setClassString("");
		screenVars.currfromDisp.setClassString("");
		screenVars.currtoDisp.setClassString("");
		screenVars.mlparorc.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.acdes01.setClassString("");
		screenVars.acctype.setClassString("");
		screenVars.acdes02.setClassString("");
	}

/**
 * Clear all the variables in Sm507screen
 */
	public static void clear(VarModel pv) {
		Sm507ScreenVars screenVars = (Sm507ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.agnum.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.agtydesc.clear();
		screenVars.agbrdesc.clear();
		screenVars.aradesc.clear();
		screenVars.repname.clear();
		screenVars.accountType.clear();
		screenVars.exclAgmt.clear();
		screenVars.aracde.clear();
		screenVars.reportag.clear();
		screenVars.gacc.clear();
		screenVars.agntbr.clear();
		screenVars.agtype.clear();
		screenVars.tydesc.clear();
		screenVars.reportton.clear();
		screenVars.repsel.clear();
		screenVars.agntfm.clear();
		screenVars.descn.clear();
		screenVars.mlprcind.clear();
		screenVars.currfromDisp.clear();
		screenVars.currfrom.clear();
		screenVars.currtoDisp.clear();
		screenVars.currto.clear();
		screenVars.mlparorc.clear();
		screenVars.agntsel.clear();
		screenVars.acdes01.clear();
		screenVars.acctype.clear();
		screenVars.acdes02.clear();
	}
}
