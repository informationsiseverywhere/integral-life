/******************** */
/*Author  :Liwei      */
/*Purpose :curd data for agpfpf       */
/*Date    :2018.10.26           */ 
package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.AgpypfDAO;
import com.csc.life.agents.dataaccess.model.Agpypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgpypfDAOImpl extends BaseDAOImpl<Agpypf> implements AgpypfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgpypfDAOImpl.class);
	
	public void insertAgpypfRecord(Agpypf agpypf) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO AGPYAGT( RLDGCOY, SACSCODE, RLDGACCT, ORIGCURR, SACSTYP, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, ");
		sb.append(" BATCBATCH,RDOCNUM, TRANNO, JRNSEQ, ORIGAMT, TRANREF, TRANDESC, CRATE, ACCTAMT, GENLCOY, GENLCUR, GLCODE, GLSIGN, POSTYEAR, POSTMONTH,");
		sb.append("  EFFDATE, RCAMT, FRCDATE, SUPRFLG, TRDT,  TRTM, USER_T, TERMID, USRPRF, JOBNM, DATIME) ");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		try {
			ps.setString(1, agpypf.getRldgcoy());
			ps.setString(2, agpypf.getSacscode());
			ps.setString(3, agpypf.getRldgacct());
			ps.setString(4, agpypf.getOrigcurr());
			ps.setString(5, agpypf.getSacstyp());
			ps.setString(6, agpypf.getBatccoy());
			ps.setString(7, agpypf.getBatcbrn());
			ps.setInt(8, agpypf.getBatcactyr());
			ps.setInt(9, agpypf.getBatcactmn());
			ps.setString(10, agpypf.getBatctrcde());
			ps.setString(11, agpypf.getBatcbatch());
			ps.setString(12, agpypf.getRdocnum());
			ps.setInt(13, agpypf.getTranno());
			ps.setInt(14, agpypf.getJrnseq());
			ps.setBigDecimal(15, agpypf.getOrigamt());
			ps.setString(16, agpypf.getTranref());
			ps.setString(17, agpypf.getTrandesc());
			ps.setFloat(18, agpypf.getCrate());
			ps.setBigDecimal(19, agpypf.getAcctamt());
			ps.setString(20, agpypf.getGenlcoy());
			ps.setString(21, agpypf.getGenlcur());
			ps.setString(22, agpypf.getGlcode());
			ps.setString(23, agpypf.getGlsign());
			ps.setString(24, agpypf.getPostyear());
			ps.setString(25, agpypf.getPostmonth());
			ps.setInt(26, agpypf.getEffdate());
			ps.setFloat(27, agpypf.getRcamt());
			ps.setInt(28, agpypf.getFrcdate());
			ps.setString(29, agpypf.getSuprflg());
			ps.setInt(30, agpypf.getTrdt());
			ps.setInt(31, agpypf.getTrtm());
			ps.setInt(32, agpypf.getUserT());
			ps.setString(33, agpypf.getTermid());
			ps.setString(34, getUsrprf());
			ps.setString(35, getJobnm());
			ps.setTimestamp(36,new Timestamp(System.currentTimeMillis()));
			executeUpdate(ps);
		} catch (SQLException e) {
			LOGGER.error("insertAgpypfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

}
