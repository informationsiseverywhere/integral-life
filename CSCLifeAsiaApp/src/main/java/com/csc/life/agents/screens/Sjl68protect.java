package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT Date: 23 July 2020 Author: tsaxena3
 */
@SuppressWarnings("unchecked")
public class Sjl68protect extends ScreenRecord {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] { 4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { -1, -1, -1, -1 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sjl68ScreenVars sv = (Sjl68ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.sjl68protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	@SuppressWarnings("unused")
	public static void clearClassString(VarModel pv) {
		Sjl68ScreenVars screenVars = (Sjl68ScreenVars) pv;
	}

	/**
	 * Clear all the variables in Sjl68protect
	 */
	@SuppressWarnings("unused")
	public static void clear(VarModel pv) {
		Sjl68ScreenVars screenVars = (Sjl68ScreenVars) pv;
	}
}
