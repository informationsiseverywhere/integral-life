package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:04
 * Description:
 * Copybook name: T5692REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5692rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5692Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData ages = new FixedLengthStringData(12).isAPartOf(t5692Rec, 0);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(4, 3, 0, ages, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public FixedLengthStringData incmrates = new FixedLengthStringData(20).isAPartOf(t5692Rec, 12);
  	public ZonedDecimalData[] incmrate = ZDArrayPartOfStructure(4, 5, 2, incmrates, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(incmrates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData incmrate01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData incmrate02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData incmrate03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData incmrate04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public FixedLengthStringData inprempcs = new FixedLengthStringData(20).isAPartOf(t5692Rec, 32);
  	public ZonedDecimalData[] inprempc = ZDArrayPartOfStructure(4, 5, 2, inprempcs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(inprempcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData inprempc01 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData inprempc02 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 5);
  	public ZonedDecimalData inprempc03 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 10);
  	public ZonedDecimalData inprempc04 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 15);
  	public FixedLengthStringData rwcmrates = new FixedLengthStringData(20).isAPartOf(t5692Rec, 52);
  	public ZonedDecimalData[] rwcmrate = ZDArrayPartOfStructure(4, 5, 2, rwcmrates, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(rwcmrates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData rwcmrate01 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData rwcmrate02 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 5);
  	public ZonedDecimalData rwcmrate03 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 10);
  	public ZonedDecimalData rwcmrate04 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 15);
  	public FixedLengthStringData reprempcs = new FixedLengthStringData(20).isAPartOf(t5692Rec, 72);
  	public ZonedDecimalData[] reprempc = ZDArrayPartOfStructure(4, 5, 2, reprempcs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(reprempcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData reprempc01 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData reprempc02 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 5);
  	public ZonedDecimalData reprempc03 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 10);
  	public ZonedDecimalData reprempc04 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 15);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(408).isAPartOf(t5692Rec, 92, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5692Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5692Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}