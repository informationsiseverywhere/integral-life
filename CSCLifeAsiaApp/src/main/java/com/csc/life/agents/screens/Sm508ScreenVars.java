package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SM508
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm508ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(154);
	public FixedLengthStringData dataFields = new FixedLengthStringData(90).isAPartOf(dataArea, 0);
	public FixedLengthStringData acdes = DD.acdes.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(16).isAPartOf(dataArea, 90);
	public FixedLengthStringData acdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData descErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 106);
	public FixedLengthStringData[] acdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] descOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(134);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(36).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agmvty = DD.agmvty.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData accountType = DD.atype.copy().isAPartOf(subfileFields,2);
	public ZonedDecimalData benCessDate = DD.bcesdte.copyToZonedDecimal().isAPartOf(subfileFields,4);
	public FixedLengthStringData reportags = new FixedLengthStringData(24).isAPartOf(subfileFields, 12);
	public FixedLengthStringData[] reportag = FLSArrayPartOfStructure(3, 8, reportags, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(reportags, 0, FILLER_REDEFINE);
	public FixedLengthStringData reportag01 = DD.reportag.copy().isAPartOf(filler,0);
	public FixedLengthStringData reportag02 = DD.reportag.copy().isAPartOf(filler,8);
	public FixedLengthStringData reportag03 = DD.reportag.copy().isAPartOf(filler,16);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 36);
	public FixedLengthStringData agmvtyErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData atypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData bcesdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData reportagsErr = new FixedLengthStringData(12).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData[] reportagErr = FLSArrayPartOfStructure(3, 4, reportagsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(reportagsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData reportag01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData reportag02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData reportag03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 60);
	public FixedLengthStringData[] agmvtyOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] atypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] bcesdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData reportagsOut = new FixedLengthStringData(36).isAPartOf(outputSubfile, 36);
	public FixedLengthStringData[] reportagOut = FLSArrayPartOfStructure(3, 12, reportagsOut, 0);
	public FixedLengthStringData[][] reportagO = FLSDArrayPartOfArrayStructure(12, 1, reportagOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(reportagsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] reportag01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] reportag02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] reportag03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 132);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData benCessDateDisp = new FixedLengthStringData(10);

	public LongData Sm508screensflWritten = new LongData(0);
	public LongData Sm508screenctlWritten = new LongData(0);
	public LongData Sm508screenWritten = new LongData(0);
	public LongData Sm508protectWritten = new LongData(0);
	public GeneralTable sm508screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sm508screensfl;
	}

	public Sm508ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {agmvty, benCessDate, reportag01, reportag02, reportag03, accountType};
		screenSflOutFields = new BaseData[][] {agmvtyOut, bcesdteOut, reportag01Out, reportag02Out, reportag03Out, atypeOut};
		screenSflErrFields = new BaseData[] {agmvtyErr, bcesdteErr, reportag01Err, reportag02Err, reportag03Err, atypeErr};
		screenSflDateFields = new BaseData[] {benCessDate};
		screenSflDateErrFields = new BaseData[] {bcesdteErr};
		screenSflDateDispFields = new BaseData[] {benCessDateDisp};

		screenFields = new BaseData[] {agntnum, desc, agtype, acdes};
		screenOutFields = new BaseData[][] {agntnumOut, descOut, agtypeOut, acdesOut};
		screenErrFields = new BaseData[] {agntnumErr, descErr, agtypeErr, acdesErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sm508screen.class;
		screenSflRecord = Sm508screensfl.class;
		screenCtlRecord = Sm508screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sm508protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sm508screenctl.lrec.pageSubfile);
	}
}
