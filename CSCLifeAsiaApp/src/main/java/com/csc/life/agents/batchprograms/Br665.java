/*
 * File: Br665.java
 * Date: 29 August 2009 22:33:45
 * Author: Quipoz Limited
 * 
 * Class transformed from BR665.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwagny;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwagtrec;
import com.csc.life.agents.recordstructures.Zbwagnyrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Agent Details extraction of appointment,termination,
*   reinstatement
*********************************************************
*
*   In INPUT-OUTPUT SECTION,  specify the output file
*   ZBNWPF in which the agent maintenance records are written.
*
*   Initialise
*     - In 1000-INITIALISE SECTION We override the Database file ZBNWPF
*       for records written.
*
*   Read
*     - In 2000-READ-FILE SECTION, fetch every record  from
*       the cursor and then process those required records.
*
*     - In 2500-EDIT SECTION,for those maintenance records,
*       check whether the transaction number is 1.
*       We only process  the appointment record
*       (i.e. Transaction number is 1)
*
*   Edit
*     - In 3000-UPDATE SECTION, set (via the agent file AGNT
*       and client file CLNT) and write the agent detail record
*       for appointment, termination and reinstatement.
*
*   Update
*     - In 4000-UPDATE SECTION, we close the output file ZBNWPF
*       and copy the file to the FTP library which is given in
*       process definition.
*
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of records skipped
*     03  -  Number of agents appointed
*     04  -  Number of agents terminated
*     05  -  Number of agents reinstated
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Br665 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlmacfpf1rs = null;
	private java.sql.PreparedStatement sqlmacfpf1ps = null;
	private java.sql.Connection sqlmacfpf1conn = null;
	private String sqlmacfpf1 = "";
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR665");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
		/* The following variables are defined by user*/
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
		/* WSAA-AGNT-MOVEMENT */
	private FixedLengthStringData wsaaMaintenance = new FixedLengthStringData(1).init("A");
	private FixedLengthStringData wsaaTermination = new FixedLengthStringData(1).init("T");
	private FixedLengthStringData wsaaReinstatement = new FixedLengthStringData(1).init("R");
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
		/* ERRORS */
	private static final String ml03 = "ML03";
	private static final String descrec = "DESCREC";
	private static final String aglfrec = "AGLFREC";
	private static final String clntrec = "CLNTREC";
		/* TABLES */
	private static final String tr654 = "TR654";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler6 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler6, 5);

		/* SQL-MACFPF */
	private FixedLengthStringData macfrec = new FixedLengthStringData(18);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(macfrec, 0);
	private PackedDecimalData sqlEffdate = new PackedDecimalData(8, 0).isAPartOf(macfrec, 8);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(macfrec, 13);
	private FixedLengthStringData sqlAgmvty = new FixedLengthStringData(2).isAPartOf(macfrec, 16);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Zbnwagtrec zbnwagtrec = new Zbnwagtrec();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();
	private Zbwagnyrec zbwagnyrec = new Zbwagnyrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090
	}

	public Br665() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		defineCursor1060();
	}

protected void initialise1010()
	{
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bprdIO.getCompany());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaZbnwFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		zbnwpf.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void defineCursor1060()
	{
		/*  Define the query required by declaring a cursor*/
		sqlmacfpf1 = " SELECT  AGNTNUM, EFFDATE, TRANNO, AGMVTY" +
" FROM   " + getAppVars().getTableNameOverriden("MACFPF") + " " +
" WHERE EFFDATE = ?" +
" AND AGNTCOY = ?" +
" AND (AGMVTY = ?" +
" OR AGMVTY = ?" +
" OR AGMVTY = ?)" +
" ORDER BY AGNTNUM, TRANNO, EFFDATE";
		sqlerrorflag = false;
		try {
			sqlmacfpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.agents.dataaccess.MacfpfTableDAM());
			sqlmacfpf1ps = getAppVars().prepareStatementEmbeded(sqlmacfpf1conn, sqlmacfpf1, "MACFPF");
			getAppVars().setDBNumber(sqlmacfpf1ps, 1, wsaaEffectiveDate);
			getAppVars().setDBString(sqlmacfpf1ps, 2, wsaaCompany);
			getAppVars().setDBString(sqlmacfpf1ps, 3, wsaaMaintenance);
			getAppVars().setDBString(sqlmacfpf1ps, 4, wsaaTermination);
			getAppVars().setDBString(sqlmacfpf1ps, 5, wsaaReinstatement);
			sqlmacfpf1rs = getAppVars().executeQuery(sqlmacfpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlmacfpf1rs)) {
				getAppVars().getDBObject(sqlmacfpf1rs, 1, sqlAgntnum);
				getAppVars().getDBObject(sqlmacfpf1rs, 2, sqlEffdate);
				getAppVars().getDBObject(sqlmacfpf1rs, 3, sqlTranno);
				getAppVars().getDBObject(sqlmacfpf1rs, 4, sqlAgmvty);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/* Check record is required for processing.*/
		/* Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		if (isEQ(sqlAgmvty,wsaaMaintenance)
		&& isNE(sqlTranno,1)) {
			wsspEdterror.set(SPACES);
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		agntIO.setParams(SPACES);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(bprdIO.getCompany());
		agntIO.setAgntnum(sqlAgntnum);
		/* Read AGNT  in  order to obtain the agnt type*/
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		clntIO.setClntpfx(agntIO.getClntpfx());
		clntIO.setClntcoy(agntIO.getClntcoy());
		clntIO.setClntnum(agntIO.getClntnum());
		clntIO.setFunction(varcom.readr);
		clntIO.setFormat(clntrec);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			syserrrec.statuz.set(clntIO.getStatuz());
			fatalError600();
		}
		/*    Read AGLF to get AGLF-DTEAPP*/
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(bprdIO.getCompany());
		aglfIO.setAgntnum(sqlAgntnum);
		aglfIO.setFunction(varcom.readr);
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*  Update database records.*/
		if (isEQ(sqlAgmvty,wsaaMaintenance)){
			appointment3100();
		}
		else if (isEQ(sqlAgmvty,wsaaTermination)){
			termination3200();
		}
		else if (isEQ(sqlAgmvty,wsaaReinstatement)){
			reinstatement3300();
		}
		else{
			syserrrec.params.set(macfrec);
			syserrrec.statuz.set(ml03);
			fatalError600();
		}
	}

protected void appointment3100()
	{
		zbnwagtrec.bnwagtRec.set(SPACES);
		zbnwagtrec.bnwagtTrcde.set("CR");
		zbnwagtrec.bnwagtSource.set("LIFE/ASIA");
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("AGT");
		zbwcmpyrec.input.set(sqlAgntnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwagtrec.bnwagtCompany.set(SPACES);
		}
		zbwagnyrec.bwagnyRec.set(SPACES);
		zbwagnyrec.signonCompany.set(bprdIO.getCompany());
		zbwagnyrec.language.set(bsscIO.getLanguage());
		zbwagnyrec.function.set("AGT");
		zbwagnyrec.input.set(sqlAgntnum);
		callProgram(Zbwagny.class, zbwagnyrec.bwagnyRec);
		if (isEQ(zbwagnyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtAgency.set(zbwagnyrec.agency);
		}
		else {
			zbnwagtrec.bnwagtAgency.set(SPACES);
		}
		zbnwagtrec.bnwagtAgntnum.set(sqlAgntnum);
		zbnwagtrec.bnwagtAgntlnm.set(clntIO.getSurname());
		zbnwagtrec.bnwagtAgntfnm.set(clntIO.getGivname());
		if (isEQ(clntIO.getClttype(),"P")) {
			zbnwagtrec.bnwagtAgnttyp.set("I");
		}
		else {
			zbnwagtrec.bnwagtAgnttyp.set("O");
		}
		zbnwagtrec.bnwagtAddr01.set(clntIO.getCltaddr01());
		zbnwagtrec.bnwagtAddr02.set(clntIO.getCltaddr02());
		zbnwagtrec.bnwagtAddr03.set(clntIO.getCltaddr03());
		zbnwagtrec.bnwagtCity.set(clntIO.getCltaddr04());
		zbnwagtrec.bnwagtState.set(clntIO.getCltaddr05());
		zbnwagtrec.bnwagtZip.set(clntIO.getCltpcode());
		a300GetCountryCode();
		wsaaInptDate.set(clntIO.getCltdob());
		a100DateConversion();
		zbnwagtrec.bnwagtDob.set(wsaaConvDate);
		zbnwagtrec.bnwagtTaxid.set(clntIO.getClntnum());
		wsaaInptDate.set(aglfIO.getDteapp());
		a100DateConversion();
		zbnwagtrec.bnwagtStrtdate.set(wsaaConvDate);
		zbnwagtrec.bnwagtTrmdate.set("99/99/9999");
		wsaaInptDate.set(wsaaEffectiveDate);
		a100DateConversion();
		zbnwagtrec.bnwagtProcdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.zbnwpfRecord.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct03);
		callContot001();
	}

protected void termination3200()
	{
		zbnwagtrec.bnwagtRec.set(SPACES);
		zbnwagtrec.bnwagtTrcde.set("UPD");
		zbnwagtrec.bnwagtSource.set("LIFE/ASIA");
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("AGT");
		zbwcmpyrec.input.set(sqlAgntnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwagtrec.bnwagtCompany.set(SPACES);
		}
		zbwagnyrec.bwagnyRec.set(SPACES);
		zbwagnyrec.signonCompany.set(bprdIO.getCompany());
		zbwagnyrec.language.set(bsscIO.getLanguage());
		zbwagnyrec.function.set("AGT");
		zbwagnyrec.input.set(sqlAgntnum);
		callProgram(Zbwagny.class, zbwagnyrec.bwagnyRec);
		if (isEQ(zbwagnyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtAgency.set(zbwagnyrec.agency);
		}
		else {
			zbnwagtrec.bnwagtAgency.set(SPACES);
		}
		zbnwagtrec.bnwagtAgntnum.set(sqlAgntnum);
		zbnwagtrec.bnwagtAgntlnm.set(clntIO.getSurname());
		zbnwagtrec.bnwagtAgntfnm.set(clntIO.getGivname());
		if (isEQ(clntIO.getClttype(),"P")) {
			zbnwagtrec.bnwagtAgnttyp.set("I");
		}
		else {
			zbnwagtrec.bnwagtAgnttyp.set("O");
		}
		zbnwagtrec.bnwagtAddr01.set(clntIO.getCltaddr01());
		zbnwagtrec.bnwagtAddr02.set(clntIO.getCltaddr02());
		zbnwagtrec.bnwagtAddr03.set(clntIO.getCltaddr03());
		zbnwagtrec.bnwagtCity.set(clntIO.getCltaddr04());
		zbnwagtrec.bnwagtState.set(clntIO.getCltaddr05());
		zbnwagtrec.bnwagtZip.set(clntIO.getCltpcode());
		a300GetCountryCode();
		wsaaInptDate.set(clntIO.getCltdob());
		a100DateConversion();
		zbnwagtrec.bnwagtDob.set(wsaaConvDate);
		zbnwagtrec.bnwagtTaxid.set(clntIO.getClntnum());
		wsaaInptDate.set(aglfIO.getDteapp());
		a100DateConversion();
		zbnwagtrec.bnwagtStrtdate.set(wsaaConvDate);
		wsaaInptDate.set(aglfIO.getDtetrm());
		a100DateConversion();
		zbnwagtrec.bnwagtTrmdate.set(wsaaConvDate);
		wsaaInptDate.set(wsaaEffectiveDate);
		a100DateConversion();
		zbnwagtrec.bnwagtProcdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.zbnwpfRecord.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct04);
		callContot001();
	}

protected void reinstatement3300()
	{
		zbnwagtrec.bnwagtRec.set(SPACES);
		zbnwagtrec.bnwagtTrcde.set("UPD");
		zbnwagtrec.bnwagtSource.set("LIFE/ASIA");
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("AGT");
		zbwcmpyrec.input.set(sqlAgntnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwagtrec.bnwagtCompany.set(SPACES);
		}
		zbwagnyrec.bwagnyRec.set(SPACES);
		zbwagnyrec.signonCompany.set(bprdIO.getCompany());
		zbwagnyrec.language.set(bsscIO.getLanguage());
		zbwagnyrec.function.set("AGT");
		zbwagnyrec.input.set(sqlAgntnum);
		callProgram(Zbwagny.class, zbwagnyrec.bwagnyRec);
		if (isEQ(zbwagnyrec.statuz,varcom.oK)) {
			zbnwagtrec.bnwagtAgency.set(zbwagnyrec.agency);
		}
		else {
			zbnwagtrec.bnwagtAgency.set(SPACES);
		}
		zbnwagtrec.bnwagtAgntnum.set(sqlAgntnum);
		zbnwagtrec.bnwagtAgntlnm.set(clntIO.getSurname());
		zbnwagtrec.bnwagtAgntfnm.set(clntIO.getGivname());
		if (isEQ(clntIO.getClttype(),"P")) {
			zbnwagtrec.bnwagtAgnttyp.set("I");
		}
		else {
			zbnwagtrec.bnwagtAgnttyp.set("O");
		}
		zbnwagtrec.bnwagtAddr01.set(clntIO.getCltaddr01());
		zbnwagtrec.bnwagtAddr02.set(clntIO.getCltaddr02());
		zbnwagtrec.bnwagtAddr03.set(clntIO.getCltaddr03());
		zbnwagtrec.bnwagtCity.set(clntIO.getCltaddr04());
		zbnwagtrec.bnwagtState.set(clntIO.getCltaddr05());
		zbnwagtrec.bnwagtZip.set(clntIO.getCltpcode());
		a300GetCountryCode();
		wsaaInptDate.set(clntIO.getCltdob());
		a100DateConversion();
		zbnwagtrec.bnwagtDob.set(wsaaConvDate);
		zbnwagtrec.bnwagtTaxid.set(clntIO.getClntnum());
		wsaaInptDate.set(aglfIO.getDteapp());
		a100DateConversion();
		zbnwagtrec.bnwagtStrtdate.set(wsaaConvDate);
		zbnwagtrec.bnwagtTrmdate.set("99/99/9999");
		wsaaInptDate.set(wsaaEffectiveDate);
		a100DateConversion();
		zbnwagtrec.bnwagtProcdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.zbnwpfRecord.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct05);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlmacfpf1conn, sqlmacfpf1ps, sqlmacfpf1rs);
		/*  Close any open files.*/
		zbnwpf.close();
		/*  Put the file to FTP library for download*/
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaStatuz);
			stringVariable1.addExpression(bprdIO.getRunLibrary());
			stringVariable1.addExpression(wsaaZbnwFn);
			stringVariable1.addExpression(wsaaFtpLibrary);
			stringVariable1.addExpression(wsaaFtpFile);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void a100DateConversion()
	{
		/*A101-CONVERSION*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A109-EXIT*/
	}

protected void a200PackString()
	{
		/*A201-PACK*/
		wsaaString.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(zbnwagtrec.bnwagtTrcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtSource, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtCompany, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAgency, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAgntnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAgntlnm, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAgntfnm, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAgnttyp, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAddr01, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAddr02, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtAddr03, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtCity, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtState, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtZip, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtCountry, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller1, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller2, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller3, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller4, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller5, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller6, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtFiller7, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtDob, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtTaxid, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtStrtdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtTrmdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwagtrec.bnwagtProcdate, " ");
		stringVariable1.setStringInto(wsaaString);
		/*A209-EXIT*/
	}

protected void a300GetCountryCode()
	{
		a310Get();
	}

protected void a310Get()
	{
		if (isNE(clntIO.getCtrycode(),SPACES)) {
			descIO.setDataArea(SPACES);
			descIO.setDesccoy(bprdIO.getCompany());
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesctabl(tr654);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setDescitem(clntIO.getCtrycode());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isNE(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(descIO.getStatuz());
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				zbnwagtrec.bnwagtCountry.set(descIO.getShortdesc());
			}
			else {
				zbnwagtrec.bnwagtCountry.set(SPACES);
			}
		}
		else {
			zbnwagtrec.bnwagtCountry.set(SPACES);
		}
	}
}
