package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl68ScreenVars extends SmartVarModel{
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(237);
	public FixedLengthStringData dataFields = new FixedLengthStringData(109).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 39);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields, 44);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields, 46);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields, 76);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields, 79);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 109);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 141);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);	
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData sjl68screenWritten = new LongData(0);
	public LongData sjl68protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl68ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {			
		
		fieldIndMap.put(agntbrOut, new String[] { "01", "11", "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut, new String[] { "02", "22", "-02", null, null, null, null, null, null, null, null, null });
		
		screenFields = new BaseData[] { company, tabl, item, longdesc, agntbr, agbrdesc, aracde, aradesc };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, agntbrOut, agbrdescOut, aracdeOut, aradescOut };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, agntbrErr, agbrdescErr, aracdeErr, aradescErr };
				
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl68screen.class;
		protectRecord = Sjl68protect.class;
	}
}
