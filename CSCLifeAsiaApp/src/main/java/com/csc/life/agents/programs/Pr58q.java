package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.procedures.Tr58qpt;
import com.csc.life.agents.screens.Sr58qScreenVars;
import com.csc.life.agents.tablestructures.Tr58qrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr58q  extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58Q");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();

	private FixedLengthStringData errors = new FixedLengthStringData(8);
private FixedLengthStringData F838 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("F838");


        /*********************************************
         * Add the Table Data Record - should always be Txxxxrec 
		 */
	private Tr58qrec tr58qrec = new Tr58qrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58qScreenVars sv = ScreenProgram.getScreenVars( Sr58qScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr58q() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58q", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialise1010();
				readRecord1031();
				moveToScreen1040();
			}
			case generalArea1045: {
				generalArea1045();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		sv.itmfrm.set(itemIO.getItmfrm());
		sv.itmto.set(itemIO.getItmto());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr58qrec.tr58qRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}

	}
protected void generalArea1045()
{
	
	if (isEQ(itemIO.getItmfrm(),0)) {
		sv.itmfrm.set(varcom.vrcmMaxDate);
	}
	else {
		sv.itmfrm.set(itemIO.getItmfrm());
	}
	if (isEQ(itemIO.getItmto(),0)) {
		sv.itmto.set(varcom.vrcmMaxDate);
	}
	else {
		sv.itmto.set(itemIO.getItmto());
	}
	
	sv.anntarprm.set(tr58qrec.anntarprm);
	sv.blprem.set(tr58qrec.blprem);
	sv.premst.set(tr58qrec.premst);
	sv.zlocamt.set(tr58qrec.zlocamt);
	sv.znadjperc01.set(tr58qrec.znadjperc01);
	sv.znadjperc02.set(tr58qrec.znadjperc02);

}


protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}


}
protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	goTo(GotoLabel.preExit);
}


protected void screenEdit2000()
{
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(wsspcomn.flag,"I"))
	{
          return;
        }
        //*********************************

        //******** if any errors found set this 
        if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	
}

protected void update3000()
{
         if (isEQ(wsspcomn.flag,"I")) {
           return;
         }
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")) {
		wsaaUpdateFlag = "Y";
	}
	checkChanges3100();
	if (isNE(wsaaUpdateFlag,"Y")) {
           return;
	}
	updatePrimaryRecord3050();
	updateRecord3055();

}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tr58qrec.tr58qRec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		if (isNE(sv.anntarprm, tr58qrec.anntarprm)) {
			tr58qrec.anntarprm.set(sv.anntarprm);
			wsaaUpdateFlag = "Y";	
		}
		if (isNE(sv.blprem, tr58qrec.blprem)) {
			tr58qrec.blprem.set(sv.blprem);
			wsaaUpdateFlag = "Y";	
		}
		if (isNE(sv.premst, tr58qrec.premst)) {
			tr58qrec.premst.set(sv.premst);
			wsaaUpdateFlag = "Y";	
		}
		if (isNE(sv.zlocamt, tr58qrec.zlocamt)) {
			tr58qrec.zlocamt.set(sv.zlocamt);
			wsaaUpdateFlag = "Y";	
		}
		if (isNE(sv.znadjperc01, tr58qrec.znadjperc01)) {
			tr58qrec.znadjperc01.set(sv.znadjperc01);
			wsaaUpdateFlag = "Y";	
		}
		if (isNE(sv.znadjperc02, tr58qrec.znadjperc02)) {
			tr58qrec.znadjperc02.set(sv.znadjperc02);
			wsaaUpdateFlag = "Y";	
		}

	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr58qpt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
