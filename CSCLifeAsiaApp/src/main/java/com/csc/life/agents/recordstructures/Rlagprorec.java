package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:02
 * Description:
 * Copybook name: RLAGPROREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rlagprorec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(116);
  	public FixedLengthStringData prog = new FixedLengthStringData(10).isAPartOf(rec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rec, 10);
  	public FixedLengthStringData dataKey = new FixedLengthStringData(38).isAPartOf(rec, 14);
  	public FixedLengthStringData agntcoy = new FixedLengthStringData(2).isAPartOf(dataKey, 0);
  	public FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(dataKey, 2);
  	public FixedLengthStringData mlagttyp = new FixedLengthStringData(2).isAPartOf(dataKey, 10);
  	public ZonedDecimalData datefrm = new ZonedDecimalData(8, 0).isAPartOf(dataKey, 12).setUnsigned();
  	public ZonedDecimalData dateto = new ZonedDecimalData(8, 0).isAPartOf(dataKey, 20).setUnsigned();
  	public FixedLengthStringData out = new FixedLengthStringData(10).isAPartOf(dataKey, 28);
  	public FixedLengthStringData fillerOut = new FixedLengthStringData(10).isAPartOf(out, 0, REDEFINE);
  	public FixedLengthStringData numofmbrOut = new FixedLengthStringData(1).isAPartOf(fillerOut, 0);
  	public FixedLengthStringData mlperpp01Out = new FixedLengthStringData(1).isAPartOf(fillerOut, 1);
  	public FixedLengthStringData mldirpp01Out = new FixedLengthStringData(1).isAPartOf(fillerOut, 2);
  	public FixedLengthStringData mlgrppp01Out = new FixedLengthStringData(1).isAPartOf(fillerOut, 3);
  	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(fillerOut, 4, FILLER);
  	public FixedLengthStringData dataArea = new FixedLengthStringData(64).isAPartOf(rec, 52);
  	public FixedLengthStringData acctMnthyer = new FixedLengthStringData(12).isAPartOf(dataArea, 0);
  	public ZonedDecimalData acctYearfrm = new ZonedDecimalData(4, 0).isAPartOf(acctMnthyer, 0).setUnsigned();
  	public ZonedDecimalData acctMnthfrm = new ZonedDecimalData(2, 0).isAPartOf(acctMnthyer, 4).setUnsigned();
  	public ZonedDecimalData acctYearto = new ZonedDecimalData(4, 0).isAPartOf(acctMnthyer, 6).setUnsigned();
  	public ZonedDecimalData acctMnthto = new ZonedDecimalData(2, 0).isAPartOf(acctMnthyer, 10).setUnsigned();
  	public FixedLengthStringData agnt = new FixedLengthStringData(52).isAPartOf(dataArea, 12);
  	public ZonedDecimalData numofmbr = new ZonedDecimalData(7, 0).isAPartOf(agnt, 0).setUnsigned();
  	public ZonedDecimalData mlperpp01 = new ZonedDecimalData(15, 2).isAPartOf(agnt, 7);
  	public ZonedDecimalData mldirpp01 = new ZonedDecimalData(15, 2).isAPartOf(agnt, 22);
  	public ZonedDecimalData mlgrppp01 = new ZonedDecimalData(15, 2).isAPartOf(agnt, 37);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}