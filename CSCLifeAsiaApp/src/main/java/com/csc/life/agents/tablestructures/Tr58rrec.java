package com.csc.life.agents.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr58rrec extends ExternalData {

  	// public FixedLengthStringData Tr58rRec = new FixedLengthStringData(35);
	
	public FixedLengthStringData tr58rRec = new FixedLengthStringData(35); 

        
public FixedLengthStringData companys = new FixedLengthStringData(1).isAPartOf(tr58rRec,0);
public FixedLengthStringData[] company = FLSArrayPartOfStructure(1, 1, companys, 0);

public FixedLengthStringData items = new FixedLengthStringData(8).isAPartOf(tr58rRec,1);
public FixedLengthStringData[] item = FLSArrayPartOfStructure(8, 1, items, 0);

public ZonedDecimalData itmfrm = new ZonedDecimalData(8,0).isAPartOf(tr58rRec,9); 
public ZonedDecimalData itmfrmCCyy = new ZonedDecimalData(4,0).isAPartOf(itmfrm , 0).setUnsigned(); 
public ZonedDecimalData itmfrmMm = new ZonedDecimalData(2,0).isAPartOf(itmfrm , 4).setUnsigned(); 
public ZonedDecimalData itmfrmDd = new ZonedDecimalData(2,0).isAPartOf(itmfrm , 6).setUnsigned();

		
public ZonedDecimalData itmto = new ZonedDecimalData(8,0).isAPartOf(tr58rRec,17); 
public ZonedDecimalData itmtoCCyy = new ZonedDecimalData(4,0).isAPartOf(itmto , 0).setUnsigned(); 
public ZonedDecimalData itmtoMm = new ZonedDecimalData(2,0).isAPartOf(itmto , 4).setUnsigned(); 
public ZonedDecimalData itmtoDd = new ZonedDecimalData(2,0).isAPartOf(itmto , 6).setUnsigned();

		
public ZonedDecimalData minpcnt = new ZonedDecimalData(5,2).isAPartOf(tr58rRec,25);

public FixedLengthStringData tabls = new FixedLengthStringData(5).isAPartOf(tr58rRec,30);
public FixedLengthStringData[] tabl = FLSArrayPartOfStructure(5, 1, tabls, 0);
private static final Logger LOGGER = LoggerFactory.getLogger(Tr58rrec.class);//IJTI-318
		
    public String[] fieldnamelist={"companys","items","itmfrms","itmtos","minpcnts","tabls"};
		
	public Tr58rrec(){
		LOGGER.info("test- DE:Tr58rrec constuctor called");//IJTI-318
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr58rRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr58rRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
	}
}
