package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:57
 * Description:
 * Copybook name: T5565REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5565rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5565Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData anbAtCcds = new FixedLengthStringData(30).isAPartOf(t5565Rec, 0);
  	public ZonedDecimalData[] anbAtCcd = ZDArrayPartOfStructure(10, 3, 0, anbAtCcds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(anbAtCcds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData anbAtCcd01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData anbAtCcd02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData anbAtCcd03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData anbAtCcd04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData anbAtCcd05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData anbAtCcd06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData anbAtCcd07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData anbAtCcd08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public ZonedDecimalData anbAtCcd09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData anbAtCcd10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
  	public FixedLengthStringData prcnts = new FixedLengthStringData(450).isAPartOf(t5565Rec, 30);
  	public ZonedDecimalData[] prcnt = ZDArrayPartOfStructure(90, 5, 2, prcnts, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(450).isAPartOf(prcnts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData prcnt01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData prcnt02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData prcnt03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData prcnt04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData prcnt05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData prcnt06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData prcnt07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData prcnt08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData prcnt09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData prcnt10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData prcnt11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public ZonedDecimalData prcnt12 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 55);
  	public ZonedDecimalData prcnt13 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 60);
  	public ZonedDecimalData prcnt14 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 65);
  	public ZonedDecimalData prcnt15 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 70);
  	public ZonedDecimalData prcnt16 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 75);
  	public ZonedDecimalData prcnt17 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 80);
  	public ZonedDecimalData prcnt18 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData prcnt19 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 90);
  	public ZonedDecimalData prcnt20 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 95);
  	public ZonedDecimalData prcnt21 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 100);
  	public ZonedDecimalData prcnt22 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 105);
  	public ZonedDecimalData prcnt23 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 110);
  	public ZonedDecimalData prcnt24 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 115);
  	public ZonedDecimalData prcnt25 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 120);
  	public ZonedDecimalData prcnt26 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 125);
  	public ZonedDecimalData prcnt27 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 130);
  	public ZonedDecimalData prcnt28 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 135);
  	public ZonedDecimalData prcnt29 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 140);
  	public ZonedDecimalData prcnt30 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 145);
  	public ZonedDecimalData prcnt31 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 150);
  	public ZonedDecimalData prcnt32 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 155);
  	public ZonedDecimalData prcnt33 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 160);
  	public ZonedDecimalData prcnt34 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 165);
  	public ZonedDecimalData prcnt35 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 170);
  	public ZonedDecimalData prcnt36 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 175);
  	public ZonedDecimalData prcnt37 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 180);
  	public ZonedDecimalData prcnt38 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 185);
  	public ZonedDecimalData prcnt39 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 190);
  	public ZonedDecimalData prcnt40 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 195);
  	public ZonedDecimalData prcnt41 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 200);
  	public ZonedDecimalData prcnt42 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 205);
  	public ZonedDecimalData prcnt43 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 210);
  	public ZonedDecimalData prcnt44 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 215);
  	public ZonedDecimalData prcnt45 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 220);
  	public ZonedDecimalData prcnt46 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 225);
  	public ZonedDecimalData prcnt47 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 230);
  	public ZonedDecimalData prcnt48 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 235);
  	public ZonedDecimalData prcnt49 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 240);
  	public ZonedDecimalData prcnt50 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 245);
  	public ZonedDecimalData prcnt51 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 250);
  	public ZonedDecimalData prcnt52 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 255);
  	public ZonedDecimalData prcnt53 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 260);
  	public ZonedDecimalData prcnt54 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 265);
  	public ZonedDecimalData prcnt55 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 270);
  	public ZonedDecimalData prcnt56 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 275);
  	public ZonedDecimalData prcnt57 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 280);
  	public ZonedDecimalData prcnt58 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 285);
  	public ZonedDecimalData prcnt59 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 290);
  	public ZonedDecimalData prcnt60 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 295);
  	public ZonedDecimalData prcnt61 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 300);
  	public ZonedDecimalData prcnt62 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 305);
  	public ZonedDecimalData prcnt63 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 310);
  	public ZonedDecimalData prcnt64 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 315);
  	public ZonedDecimalData prcnt65 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 320);
  	public ZonedDecimalData prcnt66 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 325);
  	public ZonedDecimalData prcnt67 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 330);
  	public ZonedDecimalData prcnt68 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 335);
  	public ZonedDecimalData prcnt69 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 340);
  	public ZonedDecimalData prcnt70 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 345);
  	public ZonedDecimalData prcnt71 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 350);
  	public ZonedDecimalData prcnt72 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 355);
  	public ZonedDecimalData prcnt73 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 360);
  	public ZonedDecimalData prcnt74 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 365);
  	public ZonedDecimalData prcnt75 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 370);
  	public ZonedDecimalData prcnt76 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 375);
  	public ZonedDecimalData prcnt77 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 380);
  	public ZonedDecimalData prcnt78 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 385);
  	public ZonedDecimalData prcnt79 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 390);
  	public ZonedDecimalData prcnt80 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 395);
  	public ZonedDecimalData prcnt81 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 400);
  	public ZonedDecimalData prcnt82 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 405);
  	public ZonedDecimalData prcnt83 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 410);
  	public ZonedDecimalData prcnt84 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 415);
  	public ZonedDecimalData prcnt85 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 420);
  	public ZonedDecimalData prcnt86 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 425);
  	public ZonedDecimalData prcnt87 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 430);
  	public ZonedDecimalData prcnt88 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 435);
  	public ZonedDecimalData prcnt89 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 440);
  	public ZonedDecimalData prcnt90 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 445);
  	public FixedLengthStringData termMaxs = new FixedLengthStringData(18).isAPartOf(t5565Rec, 480);
   public PackedDecimalData[] termMax = PDArrayPartOfStructure(9, 3, 0, termMaxs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(termMaxs, 0, FILLER_REDEFINE);
   public PackedDecimalData termMax01 = new PackedDecimalData(3, 0).isAPartOf(filler2, 0);
   public PackedDecimalData termMax02 = new PackedDecimalData(3, 0).isAPartOf(filler2, 2);
   public PackedDecimalData termMax03 = new PackedDecimalData(3, 0).isAPartOf(filler2, 4);
   public PackedDecimalData termMax04 = new PackedDecimalData(3, 0).isAPartOf(filler2, 6);
   public PackedDecimalData termMax05 = new PackedDecimalData(3, 0).isAPartOf(filler2, 8);
   public PackedDecimalData termMax06 = new PackedDecimalData(3, 0).isAPartOf(filler2, 10);
   public PackedDecimalData termMax07 = new PackedDecimalData(3, 0).isAPartOf(filler2, 12);
   public PackedDecimalData termMax08 = new PackedDecimalData(3, 0).isAPartOf(filler2, 14);
   public PackedDecimalData termMax09 = new PackedDecimalData(3, 0).isAPartOf(filler2, 16);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(t5565Rec, 498, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5565Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5565Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}