package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5035screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5035ScreenVars sv = (S5035ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5035screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5035ScreenVars screenVars = (S5035ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.agnum.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.exclAgmt.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.agtydesc.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.agbrdesc.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.repsel.setClassString("");
		screenVars.repname.setClassString("");
		screenVars.ovcpc.setClassString("");
		screenVars.paysel.setClassString("");
		screenVars.payenme.setClassString("");
		screenVars.paymth.setClassString("");
		screenVars.pymdesc.setClassString("");
		screenVars.payfrq.setClassString("");
		screenVars.pyfdesc.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.minsta.setClassString("");
		screenVars.dtetrmDisp.setClassString("");
		screenVars.bcmtab.setClassString("");
		screenVars.bcmdesc.setClassString("");
		screenVars.scmtab.setClassString("");
		screenVars.scmdsc.setClassString("");
		screenVars.rcmtab.setClassString("");
		screenVars.rcmdesc.setClassString("");
		screenVars.agentClass.setClassString("");
		screenVars.agclsd.setClassString("");
		screenVars.ocmtab.setClassString("");
		screenVars.ocmdesc.setClassString("");
		screenVars.clientind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.bctind.setClassString("");
		screenVars.tagd.setClassString("");
		screenVars.zrorind.setClassString("");
		screenVars.tlaglicno.setClassString("");
		screenVars.tlicexpdtDisp.setClassString("");
		screenVars.tagsusind.setClassString("");
		screenVars.tsalesunt.setClassString("");
		screenVars.tsalesdsc.setClassString("");
		screenVars.tagsusdesc.setClassString("");
		screenVars.qualification.setClassString("");//ILJ-4
		//TMLII-281 AG-01-002 START
		screenVars.zrecruit.setClassString("");
		screenVars.agtname.setClassString("");
		screenVars.agncysalic.setClassString("");
		//TMLII-281 AG-01-002 END

		//ICIL-12 start
		screenVars.agentrefcode.setClassString("");
		screenVars.agencymgr.setClassString("");
		screenVars.distrctcode.setClassString("");
		screenVars.bnkinternalrole.setClassString("");
		screenVars.banceindicatorsel.setClassString("");		
		//ICIL-12 end
		screenVars.agncysel.setClassString("");
		screenVars.agncydesc.setClassString("");
		screenVars.salebranch.setClassString("");
		screenVars.salebranchdes.setClassString("");
		
	}

/**
 * Clear all the variables in S5035screen
 */
	public static void clear(VarModel pv) {
		S5035ScreenVars screenVars = (S5035ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.agnum.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.exclAgmt.clear();
		screenVars.agtype.clear();
		screenVars.agtydesc.clear();
		screenVars.agntbr.clear();
		screenVars.agbrdesc.clear();
		screenVars.aracde.clear();
		screenVars.aradesc.clear();
		screenVars.repsel.clear();
		screenVars.repname.clear();
		screenVars.ovcpc.clear();
		screenVars.paysel.clear();
		screenVars.payenme.clear();
		screenVars.paymth.clear();
		screenVars.pymdesc.clear();
		screenVars.payfrq.clear();
		screenVars.pyfdesc.clear();
		screenVars.currcode.clear();
		screenVars.minsta.clear();
		screenVars.dtetrmDisp.clear();
		screenVars.dtetrm.clear();
		screenVars.bcmtab.clear();
		screenVars.bcmdesc.clear();
		screenVars.scmtab.clear();
		screenVars.scmdsc.clear();
		screenVars.rcmtab.clear();
		screenVars.rcmdesc.clear();
		screenVars.agentClass.clear();
		screenVars.agclsd.clear();
		screenVars.ocmtab.clear();
		screenVars.ocmdesc.clear();
		screenVars.clientind.clear();
		screenVars.ddind.clear();
		screenVars.bctind.clear();
		screenVars.tagd.clear();
		screenVars.qualification.clear();//ILJ-4
		screenVars.zrorind.clear();
		screenVars.tlaglicno.clear();
		screenVars.tlicexpdtDisp.clear();
		screenVars.tlicexpdt.clear();
		screenVars.tagsusind.clear();
		screenVars.tsalesunt.clear();
		screenVars.tsalesdsc.clear();
		screenVars.tagsusdesc.clear();
		//TMLII-281 AG-01-002 START
		screenVars.zrecruit.clear();
		screenVars.agtname.clear();
		screenVars.agncysalic.clear();
		//TMLII-281 AG-01-002 END
		//ICIL-12 start
		screenVars.agentrefcode.clear();
		screenVars.agencymgr.clear();
		screenVars.distrctcode.clear();
		screenVars.bnkinternalrole.clear();
		screenVars.banceindicatorsel.clear();	
		//ICIL-12 end
		screenVars.agncysel.clear();
		screenVars.agncydesc.clear();
		screenVars.salebranch.clear();
		screenVars.salebranchdes.clear();
		

	}
}
