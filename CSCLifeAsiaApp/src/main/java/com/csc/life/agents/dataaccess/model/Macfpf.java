/*********************  */
/*Author  :Liwei        */
/*Purpose :macfpf model */
/*Date    :2018.10.26   */ 
package com.csc.life.agents.dataaccess.model;


public class Macfpf  {
	private String agntnum ;
	private int effdate;
	private String agmvty;
	private String mlprvsup01;
	private String mlprvsup02;
	private String mlprvsup03;
	private String mlprvsup04;
	private String zrptga;
	private String zrptgb;
	private String zrptgc;
	private String zrptgd;
	private int tranno;
	private String mlagttyp;
	private String mlparagt;
	private int currfrom;
	private int currto;
	private int mlparorc;
	private String userProfile;
	private String jobName;
	private String datime;
	private String agntcoy;
	
	public Macfpf() {
		agntnum = "";
		effdate = 0;
		agmvty = "";
		mlprvsup01 = "";
		mlprvsup02 = "";
		mlprvsup03 = "";
		mlprvsup04 = "";
		zrptga = "";
		zrptgb = "";
		zrptgc = "";
		zrptgd = "";
		tranno = 0;
		mlagttyp = "";
		mlparagt = "";
		currfrom = 0;
		currto = 0;
		mlparorc = 0;
		userProfile = "";
		jobName = "";
		datime = "";
		agntcoy = "";
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getAgmvty() {
		return agmvty;
	}
	public void setAgmvty(String agmvty) {
		this.agmvty = agmvty;
	}
	public String getMlprvsup01() {
		return mlprvsup01;
	}
	public void setMlprvsup01(String mlprvsup01) {
		this.mlprvsup01 = mlprvsup01;
	}
	public String getMlprvsup02() {
		return mlprvsup02;
	}
	public void setMlprvsup02(String mlprvsup02) {
		this.mlprvsup02 = mlprvsup02;
	}
	public String getMlprvsup03() {
		return mlprvsup03;
	}
	public void setMlprvsup03(String mlprvsup03) {
		this.mlprvsup03 = mlprvsup03;
	}
	public String getMlprvsup04() {
		return mlprvsup04;
	}
	public void setMlprvsup04(String mlprvsup04) {
		this.mlprvsup04 = mlprvsup04;
	}
	public String getZrptga() {
		return zrptga;
	}
	public void setZrptga(String zrptga) {
		this.zrptga = zrptga;
	}
	public String getZrptgb() {
		return zrptgb;
	}
	public void setZrptgb(String zrptgb) {
		this.zrptgb = zrptgb;
	}
	public String getZrptgc() {
		return zrptgc;
	}
	public void setZrptgc(String zrptgc) {
		this.zrptgc = zrptgc;
	}
	public String getZrptgd() {
		return zrptgd;
	}
	public void setZrptgd(String zrptgd) {
		this.zrptgd = zrptgd;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public String getMlagttyp() {
		return mlagttyp;
	}
	public void setMlagttyp(String mlagttyp) {
		this.mlagttyp = mlagttyp;
	}
	public String getMlparagt() {
		return mlparagt;
	}
	public void setMlparagt(String mlparagt) {
		this.mlparagt = mlparagt;
	}
	public int getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(int currfrom) {
		this.currfrom = currfrom;
	}
	public int getCurrto() {
		return currto;
	}
	public void setCurrto(int currto) {
		this.currto = currto;
	}
	public int getMlparorc() {
		return mlparorc;
	}
	public void setMlparorc(int mlparorc) {
		this.mlparorc = mlparorc;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	
	
	

}
