package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.life.agents.dataaccess.ZmbwpfTableDAM;


/**
 * 	
 * File: ZmbwTableDAM.java
 * Date: Wed Jul 17 16:29:09 SGT 2013
 * Class transformed from ZMBW.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZmbwTableDAM extends ZmbwpfTableDAM {

	public ZmbwTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZMBW");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
		KEYCOLUMNS = 
				 "AGNTCOY, "+
				 "AGNTNUM";
		
     
		QUALIFIEDCOLUMNS = 
				 "AGNTCOY, "+
				 "AGNTNUM, "+
				 "BATCACTMN, "+
				 "BATCACTYR, "+
				 "COMTOT, "+
				 "DATIME, "+
				 "GENLCUR, "+
				 "JOBNM, "+
				 "PRCENT, "+
				 "USRPRF, "+
				 "UNIQUE_NUMBER"; 
         
		            
		
		ORDERBY = 
				 "AGNTCOY ASC, " +
				 "AGNTNUM ASC, " +
				"UNIQUE_NUMBER DESC"; 
		            
		
		REVERSEORDERBY = 
				 "AGNTCOY DESC, " +
				 "AGNTNUM DESC, " +
				"UNIQUE_NUMBER ASC"; 
		
		setStatics();
		
		qualifiedColumns = new BaseData[] {  
				 agntcoy, 
				 agntnum, 
				 batcactmn, 
				 batcactyr, 
				 comtot, 
				 datime, 
				 genlcur, 
				 jobName, 
				 prcent, 
				 userProfile, 
				 unique_number 
 };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}
	
	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA

	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set( 
				 getAgntcoy().toInternal() +
				 getAgntnum().toInternal() +
				 keyFiller.toInternal() );
			
		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());
			
			 what = ExternalData.chop(what, agntcoy); 
			 what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	
		
	 private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1); 
	 private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8); 


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
		
		nonKeyFiller10.setInternal(agntcoy.toInternal()); 
		nonKeyFiller20.setInternal(agntnum.toInternal()); 

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(782);
		
		nonKeyData.set(
		
		nonKeyFiller10.toInternal() +
		nonKeyFiller20.toInternal() +
 		getBatcactmn().toInternal() +
		getBatcactyr().toInternal() +
		getComtot().toInternal() +
		getDatime().toInternal() +
		getGenlcur().toInternal() +
		getJobName().toInternal() +
		getPrcent().toInternal() +
		getUserProfile().toInternal()

		);
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());
			
			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, comtot);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, prcent);
			what = ExternalData.chop(what, userProfile);

			return what;
		}
	}
	
	
	public FixedLengthStringData getAgntcoy(){
		return agntcoy;
	}

	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}

	public FixedLengthStringData getAgntnum(){
		return agntnum;
	}

	public void setAgntnum(Object what) {
		agntnum.set(what);
	}

	public PackedDecimalData getBatcactmn(){
		return batcactmn;
	}

	public void setBatcactmn(Object what) {
		batcactmn.set(what);
	}

	public PackedDecimalData getBatcactyr(){
		return batcactyr;
	}

	public void setBatcactyr(Object what) {
		batcactyr.set(what);
	}

	public PackedDecimalData getComtot(){
		return comtot;
	}

	public void setComtot(Object what) {
		comtot.set(what);
	}

	public FixedLengthStringData getDatime(){
		return datime;
	}

	public void setDatime(Object what) {
		datime.set(what);
	}

	public FixedLengthStringData getGenlcur(){
		return genlcur;
	}

	public void setGenlcur(Object what) {
		genlcur.set(what);
	}

	public FixedLengthStringData getJobName(){
		return jobName;
	}

	public void setJobName(Object what) {
		jobName.set(what);
	}

	public PackedDecimalData getPrcent(){
		return prcent;
	}

	public void setPrcent(Object what) {
		prcent.set(what);
	}

	public FixedLengthStringData getUserProfile(){
		return userProfile;
	}

	public void setUserProfile(Object what) {
		userProfile.set(what);
	}


	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {
	 	
		agntcoy.clear();
		agntnum.clear();

	  	keyFiller.clear();
	}

	public void clearRecNonKeyData() {
		
		nonKeyFiller10.clear(); 
		nonKeyFiller20.clear(); 
		batcactmn.clear();
		batcactyr.clear();
		comtot.clear();
		datime.clear();
		genlcur.clear();
		jobName.clear();
		prcent.clear();
		userProfile.clear();

	}


}
