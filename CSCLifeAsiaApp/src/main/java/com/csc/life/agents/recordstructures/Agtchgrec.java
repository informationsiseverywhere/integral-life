package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:11
 * Description:
 * Copybook name: AGTCHGREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agtchgrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData agentRec = new FixedLengthStringData(91);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(agentRec, 0);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(agentRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(agentRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(agentRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(agentRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(agentRec, 17);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(agentRec, 19);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(agentRec, 22);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(agentRec, 24);
  	public FixedLengthStringData agntnumNew = new FixedLengthStringData(8).isAPartOf(agentRec, 27);
  	public FixedLengthStringData agntnumOld = new FixedLengthStringData(8).isAPartOf(agentRec, 35);
  	public FixedLengthStringData servCommFlag = new FixedLengthStringData(1).isAPartOf(agentRec, 43);
  	public FixedLengthStringData initCommFlag = new FixedLengthStringData(1).isAPartOf(agentRec, 44);
  	public FixedLengthStringData rnwlCommFlag = new FixedLengthStringData(1).isAPartOf(agentRec, 45);
  	public FixedLengthStringData orCommFlag = new FixedLengthStringData(1).isAPartOf(agentRec, 46);
  	public FixedLengthStringData updateFlag = new FixedLengthStringData(1).isAPartOf(agentRec, 47);
  	public FixedLengthStringData batckey = new FixedLengthStringData(21).isAPartOf(agentRec, 48);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batckey, 0);
  	public FixedLengthStringData prefix = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData actyear = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData actmonth = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData trcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 19, FILLER);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(agentRec, 69);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(agentRec, 73).setUnsigned();
  	public PackedDecimalData transactionDate = new PackedDecimalData(8, 0).isAPartOf(agentRec, 79);
  	public ZonedDecimalData transactionTime = new ZonedDecimalData(6, 0).isAPartOf(agentRec, 84).setUnsigned();
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(agentRec, 90);


	public void initialize() {
		COBOLFunctions.initialize(agentRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agentRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}