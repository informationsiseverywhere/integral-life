package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm605screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm605ScreenVars sv = (Sm605ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm605screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm605ScreenVars screenVars = (Sm605ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.mlagttyp01.setClassString("");
		screenVars.mlagttyp02.setClassString("");
		screenVars.mlagttyp03.setClassString("");
		screenVars.mlagttyp04.setClassString("");
		screenVars.mlagttyp05.setClassString("");
		screenVars.mlagttyp06.setClassString("");
		screenVars.mlprcind01.setClassString("");
		screenVars.mlprcind02.setClassString("");
		screenVars.mlprcind03.setClassString("");
		screenVars.mlprcind04.setClassString("");
		screenVars.mlprcind05.setClassString("");
		screenVars.mlprcind06.setClassString("");
		screenVars.mlagttyp07.setClassString("");
		screenVars.mlagttyp08.setClassString("");
		screenVars.mlagttyp09.setClassString("");
		screenVars.mlagttyp10.setClassString("");
		screenVars.mlagttyp11.setClassString("");
		screenVars.mlagttyp12.setClassString("");
		screenVars.mlagttyp13.setClassString("");
		screenVars.mlagttyp14.setClassString("");
		screenVars.mlagttyp15.setClassString("");
		screenVars.mlagttyp16.setClassString("");
		screenVars.mlagttyp17.setClassString("");
		screenVars.mlagttyp18.setClassString("");
		screenVars.mlperpp01.setClassString("");
		screenVars.toYear01.setClassString("");
		screenVars.mlagtprd01.setClassString("");
		screenVars.mlgrppp01.setClassString("");
		screenVars.toYear02.setClassString("");
		screenVars.mlperpp02.setClassString("");
		screenVars.mlagtprd02.setClassString("");
		screenVars.mlgrppp02.setClassString("");
		screenVars.toYear03.setClassString("");
		screenVars.mlperpp03.setClassString("");
		screenVars.mlagtprd03.setClassString("");
		screenVars.mlgrppp03.setClassString("");
		screenVars.toYear04.setClassString("");
		screenVars.mlperpp04.setClassString("");
		screenVars.mlagtprd04.setClassString("");
		screenVars.mlgrppp04.setClassString("");
		screenVars.toYear05.setClassString("");
		screenVars.mlperpp05.setClassString("");
		screenVars.mlagtprd05.setClassString("");
		screenVars.mlgrppp05.setClassString("");
	}

/**
 * Clear all the variables in Sm605screen
 */
	public static void clear(VarModel pv) {
		Sm605ScreenVars screenVars = (Sm605ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.mlagttyp01.clear();
		screenVars.mlagttyp02.clear();
		screenVars.mlagttyp03.clear();
		screenVars.mlagttyp04.clear();
		screenVars.mlagttyp05.clear();
		screenVars.mlagttyp06.clear();
		screenVars.mlprcind01.clear();
		screenVars.mlprcind02.clear();
		screenVars.mlprcind03.clear();
		screenVars.mlprcind04.clear();
		screenVars.mlprcind05.clear();
		screenVars.mlprcind06.clear();
		screenVars.mlagttyp07.clear();
		screenVars.mlagttyp08.clear();
		screenVars.mlagttyp09.clear();
		screenVars.mlagttyp10.clear();
		screenVars.mlagttyp11.clear();
		screenVars.mlagttyp12.clear();
		screenVars.mlagttyp13.clear();
		screenVars.mlagttyp14.clear();
		screenVars.mlagttyp15.clear();
		screenVars.mlagttyp16.clear();
		screenVars.mlagttyp17.clear();
		screenVars.mlagttyp18.clear();
		screenVars.mlperpp01.clear();
		screenVars.toYear01.clear();
		screenVars.mlagtprd01.clear();
		screenVars.mlgrppp01.clear();
		screenVars.toYear02.clear();
		screenVars.mlperpp02.clear();
		screenVars.mlagtprd02.clear();
		screenVars.mlgrppp02.clear();
		screenVars.toYear03.clear();
		screenVars.mlperpp03.clear();
		screenVars.mlagtprd03.clear();
		screenVars.mlgrppp03.clear();
		screenVars.toYear04.clear();
		screenVars.mlperpp04.clear();
		screenVars.mlagtprd04.clear();
		screenVars.mlgrppp04.clear();
		screenVars.toYear05.clear();
		screenVars.mlperpp05.clear();
		screenVars.mlagtprd05.clear();
		screenVars.mlgrppp05.clear();
	}
}
