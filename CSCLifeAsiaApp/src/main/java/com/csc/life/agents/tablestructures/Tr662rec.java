package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR662REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr662rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr662Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zdstrchnl = new FixedLengthStringData(6).isAPartOf(tr662Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(494).isAPartOf(tr662Rec, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr662Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr662Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}