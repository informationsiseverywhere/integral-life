package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:51
 * Class transformed from AGPD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpdTableDAM extends AgpdpfTableDAM {

	public AgpdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGPD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "EFFDATE"
		             + ", AGNTCOY"
		             + ", AGNTNUM"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "EFFDATE, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "AGENTNAME, " +
		            "CHDRNUM, " +
		            "OWNERNAME, " +
		            "AREADESC, " +
		            "DTEAPP, " +
		            "BILLFREQ, " +
		            "OCCDATE, " +
		            "ACCTMNTH, " +
		            "ACCTYR, " +
		            "ORIGCURR, " +
		            "DTETRM, " +
		            "XCOMD, " +
		            "INITCOM, " +
		            "RNLCDUE, " +
		            "ORANNL, " +
		            "SINGPRM, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "EFFDATE DESC, " +
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "EFFDATE ASC, " +
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               effdate,
                               agntcoy,
                               agntnum,
                               agentname,
                               chdrnum,
                               ownername,
                               areadesc,
                               dteapp,
                               billfreq,
                               occdate,
                               acctmnth,
                               acctyr,
                               origcurr,
                               dtetrm,
                               xcomd,
                               initcom,
                               rnlcdue,
                               orannl,
                               singlePremium,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(42);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getEffdate().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(effdate.toInternal());
	nonKeyFiller20.setInternal(agntcoy.toInternal());
	nonKeyFiller30.setInternal(agntnum.toInternal());
	nonKeyFiller50.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(235);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getAgentname().toInternal()
					+ nonKeyFiller50.toInternal()
					+ getOwnername().toInternal()
					+ getAreadesc().toInternal()
					+ getDteapp().toInternal()
					+ getBillfreq().toInternal()
					+ getOccdate().toInternal()
					+ getAcctmnth().toInternal()
					+ getAcctyr().toInternal()
					+ getOrigcurr().toInternal()
					+ getDtetrm().toInternal()
					+ getXcomd().toInternal()
					+ getInitcom().toInternal()
					+ getRnlcdue().toInternal()
					+ getOrannl().toInternal()
					+ getSinglePremium().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, agentname);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, ownername);
			what = ExternalData.chop(what, areadesc);
			what = ExternalData.chop(what, dteapp);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, acctmnth);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, xcomd);
			what = ExternalData.chop(what, initcom);
			what = ExternalData.chop(what, rnlcdue);
			what = ExternalData.chop(what, orannl);
			what = ExternalData.chop(what, singlePremium);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getAgentname() {
		return agentname;
	}
	public void setAgentname(Object what) {
		agentname.set(what);
	}	
	public FixedLengthStringData getOwnername() {
		return ownername;
	}
	public void setOwnername(Object what) {
		ownername.set(what);
	}	
	public FixedLengthStringData getAreadesc() {
		return areadesc;
	}
	public void setAreadesc(Object what) {
		areadesc.set(what);
	}	
	public PackedDecimalData getDteapp() {
		return dteapp;
	}
	public void setDteapp(Object what) {
		setDteapp(what, false);
	}
	public void setDteapp(Object what, boolean rounded) {
		if (rounded)
			dteapp.setRounded(what);
		else
			dteapp.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getAcctmnth() {
		return acctmnth;
	}
	public void setAcctmnth(Object what) {
		setAcctmnth(what, false);
	}
	public void setAcctmnth(Object what, boolean rounded) {
		if (rounded)
			acctmnth.setRounded(what);
		else
			acctmnth.set(what);
	}	
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}	
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}	
	public PackedDecimalData getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Object what) {
		setDtetrm(what, false);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			dtetrm.setRounded(what);
		else
			dtetrm.set(what);
	}	
	public PackedDecimalData getXcomd() {
		return xcomd;
	}
	public void setXcomd(Object what) {
		setXcomd(what, false);
	}
	public void setXcomd(Object what, boolean rounded) {
		if (rounded)
			xcomd.setRounded(what);
		else
			xcomd.set(what);
	}	
	public PackedDecimalData getInitcom() {
		return initcom;
	}
	public void setInitcom(Object what) {
		setInitcom(what, false);
	}
	public void setInitcom(Object what, boolean rounded) {
		if (rounded)
			initcom.setRounded(what);
		else
			initcom.set(what);
	}	
	public PackedDecimalData getRnlcdue() {
		return rnlcdue;
	}
	public void setRnlcdue(Object what) {
		setRnlcdue(what, false);
	}
	public void setRnlcdue(Object what, boolean rounded) {
		if (rounded)
			rnlcdue.setRounded(what);
		else
			rnlcdue.set(what);
	}	
	public PackedDecimalData getOrannl() {
		return orannl;
	}
	public void setOrannl(Object what) {
		setOrannl(what, false);
	}
	public void setOrannl(Object what, boolean rounded) {
		if (rounded)
			orannl.setRounded(what);
		else
			orannl.set(what);
	}	
	public PackedDecimalData getSinglePremium() {
		return singlePremium;
	}
	public void setSinglePremium(Object what) {
		setSinglePremium(what, false);
	}
	public void setSinglePremium(Object what, boolean rounded) {
		if (rounded)
			singlePremium.setRounded(what);
		else
			singlePremium.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		effdate.clear();
		agntcoy.clear();
		agntnum.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		agentname.clear();
		nonKeyFiller50.clear();
		ownername.clear();
		areadesc.clear();
		dteapp.clear();
		billfreq.clear();
		occdate.clear();
		acctmnth.clear();
		acctyr.clear();
		origcurr.clear();
		dtetrm.clear();
		xcomd.clear();
		initcom.clear();
		rnlcdue.clear();
		orannl.clear();
		singlePremium.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}