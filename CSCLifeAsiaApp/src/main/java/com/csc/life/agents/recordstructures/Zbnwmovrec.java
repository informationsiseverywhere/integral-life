package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:13
 * Description:
 * Copybook name: ZBNWMOVREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwmovrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwmovRec = new FixedLengthStringData(118);
  	public FixedLengthStringData bnwmovTrcde = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 0);
  	public FixedLengthStringData bnwmovSource = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 3);
  	public FixedLengthStringData bnwmovCompany = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 13);
  	public FixedLengthStringData bnwmovAgntnum = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 16);
  	public FixedLengthStringData bnwmovProfile = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 26);
  	public FixedLengthStringData bnwmovLevel = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 29);
  	public FixedLengthStringData bnwmovCnttype = new FixedLengthStringData(6).isAPartOf(bnwmovRec, 32);
  	public FixedLengthStringData bnwmovPlan = new FixedLengthStringData(6).isAPartOf(bnwmovRec, 38);
  	public FixedLengthStringData bnwmovRegion = new FixedLengthStringData(5).isAPartOf(bnwmovRec, 44);
  	public FixedLengthStringData bnwmovTranno = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 49);
  	public FixedLengthStringData bnwmovRevlvl = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 59);
  	public FixedLengthStringData bnwmovRevcode = new FixedLengthStringData(1).isAPartOf(bnwmovRec, 62);
  	public FixedLengthStringData bnwmovEffdate = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 63);
  	public FixedLengthStringData bnwmovSagntnum = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 73);
  	public FixedLengthStringData bnwmovSprofile = new FixedLengthStringData(3).isAPartOf(bnwmovRec, 83);
  	public FixedLengthStringData bnwmovStrtdate = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 86);
  	public FixedLengthStringData bnwmovTrmdate = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 96);
  	public FixedLengthStringData bnwmovReason = new FixedLengthStringData(2).isAPartOf(bnwmovRec, 106);
  	public FixedLengthStringData bnwmovChgdate = new FixedLengthStringData(10).isAPartOf(bnwmovRec, 108);


	public void initialize() {
		COBOLFunctions.initialize(bnwmovRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwmovRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}