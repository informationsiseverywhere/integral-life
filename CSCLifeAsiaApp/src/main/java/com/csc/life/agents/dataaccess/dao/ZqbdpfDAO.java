package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.GenericDAO;
import com.csc.smart400framework.dataaccess.model.Zqbdpf;

public interface ZqbdpfDAO extends GenericDAO<Zqbdpf,Long> {
	public void save(Zqbdpf zqbd);
	public void update(Zqbdpf zqbd);
	public List<Zqbdpf> findByCriteria(Zqbdpf zqbdpf);
	public List<Zqbdpf> findByAgentCriteria(Zqbdpf zqbdpf);
}
