package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:10
 * Description:
 * Copybook name: AGPYAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agpyagtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agpyagtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agpyagtKey = new FixedLengthStringData(64).isAPartOf(agpyagtFileKey, 0, REDEFINE);
  	public FixedLengthStringData agpyagtRldgcoy = new FixedLengthStringData(1).isAPartOf(agpyagtKey, 0);
  	public FixedLengthStringData agpyagtRldgacct = new FixedLengthStringData(16).isAPartOf(agpyagtKey, 1);
  	public FixedLengthStringData agpyagtOrigcurr = new FixedLengthStringData(3).isAPartOf(agpyagtKey, 17);
  	public FixedLengthStringData agpyagtTranref = new FixedLengthStringData(30).isAPartOf(agpyagtKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(14).isAPartOf(agpyagtKey, 50, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agpyagtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agpyagtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}