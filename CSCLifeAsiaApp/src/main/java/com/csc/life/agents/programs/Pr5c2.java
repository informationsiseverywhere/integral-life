package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;


import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.agents.screens.Sr5c2ScreenVars;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr5c2 extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5C2");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sr5c2ScreenVars sv = ScreenProgram.getScreenVars( Sr5c2ScreenVars.class);
	private Zctxpf zctxpf=new Zctxpf();
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	
	public Pr5c2() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5c2", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

	try {
		super.mainline();
	} catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes

	}
}
protected void initialise1000()
{
	try {
		initialise1010();
	}
	catch (GOTOException e){
	}
}
protected void initialise1010()
{
	sv.dataArea.set(SPACES);
		  sv.cnfrm.set("N");
}



protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}

}
protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}


protected void screenEdit2000(){
	screenIo2010();

}

protected void screenIo2010(){
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz, "KILL")) {
		return;
	}
	
}
protected void update3000()
{
	try {
		update3010();  
		
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void update3010(){

    	if (isEQ(scrnparams.statuz,varcom.kill)) {
		return ;
	}
	UpdateZctx3200();
	}


protected void UpdateZctx3200(){
	if(isEQ(sv.cnfrm,"Y")){
		zctxpf=zctxpfDAO.getCacheObject(zctxpf);
		boolean check=zctxpfDAO.updateRecord(zctxpf);
	}
	
}

protected void whereNext4000()
{
	nextProgram4010();
}

protected void nextProgram4010()
{
	wsspcomn.programPtr.add(1);
	wsspcomn.nextprog.set(wsaaProg);
}




}
