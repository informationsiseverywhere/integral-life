package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl21 Date: 03 February 2020 Author: vdivisala
 */
public class Sjl21ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(160);
	public FixedLengthStringData dataFields = new FixedLengthStringData(48).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 14);
	public FixedLengthStringData cmgwdattyp = DD.cmgwdattyp.copy().isAPartOf(dataFields, 44);
	public FixedLengthStringData cmgwreason = DD.cmgwreason.copy().isAPartOf(dataFields, 45);
	public FixedLengthStringData cmgwcngcls = DD.cmgwcngcls.copy().isAPartOf(dataFields, 47);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 48);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cmgwdattypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cmgwreasonErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cmgwcngclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 76);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cmgwdattypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cmgwreasonOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cmgwcngclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl21screenWritten = new LongData(0);
	public LongData Sjl21protectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sjl21ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected final void initialiseScreenVars() {
		fieldIndMap.put(cmgwdattypOut, new String[] { "05", "25", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(cmgwreasonOut, new String[] { "06", "26", "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(cmgwcngclsOut, new String[] { "07", "27", "-07", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, cmgwdattyp, cmgwreason, cmgwcngcls };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, cmgwdattypOut, cmgwreasonOut, cmgwcngclsOut };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, cmgwdattypErr, cmgwreasonErr, cmgwcngclsErr };
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl21screen.class;
		protectRecord = Sjl21protect.class;
	}
}