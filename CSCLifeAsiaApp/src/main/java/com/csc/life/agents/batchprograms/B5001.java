/*
 * File: B5001.java
 * Date: 29 August 2009 20:50:57
 * Author: Quipoz Limited
 *
 * Class transformed from B5001.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


import com.csc.fsu.agents.dataaccess.dao.AgentReportpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgqfpfDAO;
import com.csc.fsu.agents.dataaccess.model.AgentReportpf;
import com.csc.fsu.agents.dataaccess.model.Agqfpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcgpfTableDAM;
import com.csc.life.agents.recordstructures.Agtchgrec;
import com.csc.life.agents.recordstructures.P5001par;
import com.csc.life.agents.reports.Rr505Report;
import com.csc.life.agents.tablestructures.T6688rec;
import com.csc.life.agents.tablestructures.Tjl02rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ErorpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Agcgpf;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Erorpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* B5001 - Agent Portfolio Change Batch Processing
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* Overview
* ~~~~~~~~
* The main processing  of  this  program  is  to  alter  all  Agent
* Commission records for an agent to a new agent for all contracts.
* The program is driven by a parameter screen which will require
* the entry of selection criteria:-
*
*      Agent From:    _____
*      Agent To:      _____
*
*      Initial Commission(Y/N):
*      Servicing Commission(Y/N):
*      Renewal Commission(Y/N):
*      Change Servicing Agent(Y/N):
*
* OPNQRYF  is  to be used in the CL program to perform a pre-select
* on the AGCGPF for the old agent number.
*
*
* C5001
* ~~~~~
* Create a standard CL program.
*
* A series of CPYF commands will gather together all the
* records from the AGCMPF, CHDRPF and PCDDPF files for the
* agent to be changed - giving the AGCGPF file.
*
* Use OPNQRYF on AGCGPF to select specific records with the
* selection criteria as follows:-
*
*           AGNTNUM   =    Agent Number From
*           CHDRCOY   =    Batch Header Company
*           CNTBRANCH =    Batch Contract Branch
*
* Unique  Key  on  the  OPNQRYF will select first record only, this
* will then mean only one call to the batch program per contract.
*
* Call B5001
*
* For each AGCG record passed from the CL program (limited  to  one
* per contract)  the following batch processing must occur:-
*
*
* B5001
* ~~~~~
*      Open AGCGPF.
*
*      Perform Initialise.
*
*      Open a new batch.
*
*      Set BATD-FUNCTION        to 'OPEN'.
*      Set BATD-TRANID          to PARM-TRANID.
*      Set BATD-COMPANY         to PARM-COMPANY.
*      Set BATD-BRANCH          to PARM-BATCBRANCH.
*      Set BATD-ACTYEAR         to PARM-ACCTYEAR.
*      Set BATD-ACTMONTH        to PARM-ACCTMONTH.
*      Set BATD-TRCDE           to PARM-TRANSCODE.
*      Call 'BATCDOR'      using BATD-BATCDOR-REC.
*      Perform normal error handling.
*
*      Perform AGCG-Processing until EOF.
*
*      Close batch.
*
*      Set BATD-FUNCTION        to 'CLOSE'.
*      Set BATD-TRANID          to PARM-TRANID.
*      Set BATD-COMPANY         to PARM-COMPANY.
*      Set BATD-BRANCH          to PARM-BATCBRANCH.
*      Set BATD-ACTYEAR         to PARM-ACCTYEAR.
*      Set BATD-ACTMONTH        to PARM-ACCTMONTH.
*      Set BATD-TRCDE           to PARM-TRANSCODE.
*      Call 'BATCDOR'      using BATD-BATCDOR-REC.
*      Perform normal error handling.
*
*      Close AGCGPF.
*
*
* Initialise Section.
* ~~~~~~~~~~~~~~~~~~~
*      Set P5001-PARM-RECORD to CONJ-PARAMS.
*      Get Todays Date via DATCON1 call.
*      Set any WSAA fields or flags.
*
*
* AGCG-Processing Section.
* ~~~~~~~~~~~~~~~~~~~~~~~~
* For each AGCG record read, do the following:-
*
*      Perform Get-Statcode.
*
*      Perform Read-T5679.
*
*      If STATCODE of the contract is not valid
*         Go To Exit.
*
*      Perform Read-T6688.
*
*      If 'T6688-AGTCHGSUB' = spaces
*         Go To Exit.
*
*      Perform Soft-Lock-CHDR.
*
*      If 'P5001-COMIND'        = 'Y'
*          Move 'Y' to the contract update flag
*      Else
*          Move 'N' to the contract update flag.
*      This flag is then checked in the proposal
*      subroutine to determine whether it should change
*      the servicing agent or not.
*      If the servicing agent is changed, this flag
*      is set to 'U'.
*
*      Perform Call-Subroutine.
*
*      If 'P5001-COMIND         = 'N' or
*         Contract Update Flag  = 'U'
*         Go To Release-Lock.
*
*      Perform Update-CHDR.
*
*      Perform Housekeeping.
*
* Release-Lock.
*
*      Set SFTL-FUNCTION        to 'UNLK'.
*      Set SFTL-COMPANY         to PARM-COMPANY.
*      Set SFTL-ENTTYP          to 'CH'.
*      Set SFTL-ENTITY          to CHDRNUM.
*      Set SFTL-TRANSACTION     to PARM-TRANSCODE.
*      Set SFTL-USER            to PARM-USER.
*      Call 'SFTLOCK'     using SFTL-SFTLOCK-REC.
*      Perform normal error handling.
*
*
* Get-Statcode Section.
* ~~~~~~~~~~~~~~~~~~~~~
*      Set CHDRLIF-DATA-AREA    to spaces.
*      Set CHDRLIF-CHDRCOY      to CHDRCOY.
*      Set CHDRLIF-CHDRNUM      to CHDRNUM.
*      Set CHDRLIF-FUNCTION     to READR.
*      Call 'CHDRLIFIO'   using CHDRLIF-PARAMS.
*      Perform normal error handling.
*
*      Set WSAA-STATCODE to CHDRLIF-STATCODE.
*
*      Add 1 to CHRDLIF-TRANNO giving WSAA-TRANNO.
*
*
* Read-T5679 Section.
* ~~~~~~~~~~~~~~~~~~~
*      Read T5679 with the PARM-TRANSCODE as the key.
*
*      Perform normal error handling.
*
*      T5679 will contain the valid statuses for a contract.
*      If the contract has a valid STATCODE then we can go
*      ahead and perform any processing.
*
*
* Read-T6688 Section.
* ~~~~~~~~~~~~~~~~~~~
*      Read T6688 with the WSAA-STATCODE as the key.
*
*      Perform normal error handling.
*
*      T6688 will contain the subroutine which needs to be called
*      to do any extra processing.
*
*
* Soft-Lock-CHDR Section.
* ~~~~~~~~~~~~~~~~~~~~~~~
*      Set SFTL-FUNCTION        to 'LOCK'.
*      Set SFTL-COMPANY         to PARM-COMPANY.
*      Set SFTL-ENTTYP          to 'CH'.
*      Set SFTL-ENTITY          to CHDRNUM.
*      Set SFTL-TRANSACTION     to PARM-TRANSCODE.
*      Set SFTL-USER            to PARM-USER.
*      Call 'SFTLOCK'     using SFTL-SFTLOCK-REC.
*      Perform normal error handling.
*
*
* Call-Subroutine Section.
* ~~~~~~~~~~~~~~~~~~~~~~~~
* In order to call the subroutine, we need to set up the
* linkage (AGTCHGREC) as follows:-
*
*      AGTCHGREC
*      ~~~~~~~~~
*      Set AGCG-STATUZ          to O-K.
*      Set AGCG-CHDRCOY         to CHDRNUM.
*      Set AGCG-CHDRNUM         to CHDRCOY.
*      Set AGCG-LIFE            to spaces.
*      Set AGCG-COVERAGE        to spaces.
*      Set AGCG-RIDER           to spaces.
*      Set AGCG-SEQNO           to zeroes.
*      Set AGCG-PLAN-SUFFIX     to zeroes.
*      Set AGCG-TRANNO          to WSAA-TRANNO.
*      Set AGCG-AGNTNUM-OLD     to P5001-AGENTFROM.
*      Set AGCG-AGNTNUM-NEW     to P5001-AGENTTO.
*      Set AGCG-INIT-COMM-FLAG  to P5001-INITFLG.
*      Set AGCG-RNWL-COMM-FLAG  to P5001-RNWLFLG.
*      Set AGCG-SERV-COMM-FLAG  to P5001-SERVFLG.
*      Set AGCG-BATCKEY         to BATD-BATCKEY.
*      Set AGCG-TERMID          to VRCM-TERMID.
*      Set AGCG-USER            to PARM-USER.
*      Set AGCG-TRANSACTION-DATE to Accepted Date.
*      Set AGCG-TRANSACTION-TIME to Accepted Time.
*      Call 'T6688-AGTCHGSUB'  using AGCG-AGENT-REC.
*      Perform normal error handling.
*
*
* Update-CHDR Section.
* ~~~~~~~~~~~~~~~~~~~~
*      Set CHDRLIF-DATA-AREA    to spaces.
*      Set CHDRLIF-CHDRCOY      to CHDRCOY.
*      Set CHDRLIF-CHDRNUM      to CHDRNUM.
*      Set CHDRLIF-FUNCTION     to READH.
*      Call 'CHDRLIFIO'   using CHDRLIF-PARAMS.
*      Perform normal error handling.
*
*      Set CHDRLIF-VALIDFLAG    to '2'
*      Set CHDRLIF-FUNCTION     to REWRT.
*      Call 'CHDRLIFIO'   using CHDRLIF-PARAMS.
*      Perform normal error handling.
*
*      Set CHDRLIF-AGNTNUM      to P5001-AGENTTO.
*      Set CHDRLIF-TRANNO       to WSAA-TRANNO.
*      Set CHDRLIF-VALIDFLAG    to '1'.
*      Set CHDRLIF-FUNCTION     to WRITR.
*      Call 'CHDRLIFIO'   using CHDRLIF-PARAMS.
*      Perform normal error handling.
*
*
* Housekeeping Section.
* ~~~~~~~~~~~~~~~~~~~~~
*      Set PTRN-DATA-AREA       to Spaces.
*      Set PTRN-TRANNO          to WSAA-TRANNO.
*      Set PTRN-TRANSACTION-DATE to Accepted Transaction Date.
*      Set PTRN-TRANSACTION-TIME to Accepted Transaction Time.
*      Set PTRN-BATCPFX         to BATD-PREFIX.
*      Set PTRN-BATCCOY         to PARM-COMPANY.
*      Set PTRN-BATCBRN         to PARM-BATCBRANCH.
*      Set PTRN-BATCACTYR       to PARM-ACCTYEAR.
*      Set PTRN-BATCACTMN       to PARM-ACCTMONTH.
*      Set PTRN-BATCTRCDE       to PARM-TRANSCODE.
*      Set PTRN-BATCBATCH       to BATD-BATCH.
*      Set PTRN-CHDRPFX         to 'CH'.
*      Set PTRN-CHDRCOY         to CHDRLIF-CHDRCOY.
*      Set PTRN-CHDRNUM         to CHDRLIF-CHDRNUM.
*      Set PTRN-PTRNEFF         to PARM-EFFDATE.
*      Set PTRN-TERMID          to VRCM-TERMID.
*      Set PTRN-USER            to PARM-USER.
*      Set PTRN-FORMAT          to PTRNREC.
*      Set PTRN-FUNCTION        to WRITR.
*      Call 'PTRNIO'       using PTRN-PARAMS.
*      Perform normal error handling.
*
*      Set BCUP-BATCUP-REC      to spaces.
*      Set BCUP-TRANCNT         to '1'.
*      Set BCUP-ETREQCNT        to zeroes.
*      Set BCUP-SUB             to zeroes.
*      Set BCUP-BCNT            to zeroes.
*      Set BCUP-BVAL            to zeroes.
*      Set BCUP-ASCNT           to zeroes.
*      Set BCUP-BATCPFX         to BATD-PREFIX.
*      Set BCUP-BATCCOY         to PARM-COMPANY.
*      Set BCUP-BATCBRN         to PARM-BATCBRANCH.
*      Set BCUP-BATCACTYR       to PARM-ACCTYEAR.
*      Set BCUP-BATCACTMN       to PARM-ACCTMONTH.
*      Set BCUP-BATCTRCDE       to PARM-TRANSCODE.
*      Set BCUP-BATCBATCH       to BATD-BATCH.
*      Set BCUP-FUNCTION        to WRITS
*      Call 'BATCUP'  using BCUP-BATCUP-REC.
*      Perform normal error handling.
*
*
*      The Agent Change Batch Job will now produce a Report of
*      all Contracts transfered, showing the old image and the
*      new image. The Report name is RR505.
*
*
*****************************************************************
* </pre>
*/
public class B5001 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AgcgpfTableDAM agcgpf = new AgcgpfTableDAM();
	private Rr505Report printerFile = new Rr505Report();
	private AgcgpfTableDAM agcgpfRec = new AgcgpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5001");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStat = "";
	private String endOfFile = "";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
		/* ERRORS */
	private static final String f321 = "F321";

	private FixedLengthStringData wsaaOldTemp = new FixedLengthStringData(1800);
	private FixedLengthStringData[] wsaaAgent = FLSArrayPartOfStructure(30, 8, wsaaOldTemp, 0);
	private FixedLengthStringData[] wsaaCat = FLSArrayPartOfStructure(30, 1, wsaaOldTemp, 240);
	private ZonedDecimalData[] wsaaPay = ZDArrayPartOfStructure(30, 17, 2, wsaaOldTemp, 270, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaPaid = ZDArrayPartOfStructure(30, 17, 2, wsaaOldTemp, 780, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaEarn = ZDArrayPartOfStructure(30, 17, 2, wsaaOldTemp, 1290, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaXx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData tmp = new ZonedDecimalData(2, 0).setUnsigned();

	
	private String wsaaWriteFile = "";
	private String endOfFile2 = "";
	private FixedLengthStringData wsaaCoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaServAgent = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page headings                                  */
	private FixedLengthStringData rr505H01 = new FixedLengthStringData(89);
	private FixedLengthStringData rr505h01O = new FixedLengthStringData(89).isAPartOf(rr505H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rr505h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rr505h01O, 1);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rr505h01O, 31);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rr505h01O, 41);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rr505h01O, 43);
	private FixedLengthStringData agnt03 = new FixedLengthStringData(8).isAPartOf(rr505h01O, 73);
	private FixedLengthStringData agnt04 = new FixedLengthStringData(8).isAPartOf(rr505h01O, 81);

	private FixedLengthStringData rr505D03 = new FixedLengthStringData(16);
	private FixedLengthStringData rr505d03O = new FixedLengthStringData(16).isAPartOf(rr505D03, 0);
	private FixedLengthStringData servagnt01 = new FixedLengthStringData(8).isAPartOf(rr505d03O, 0);
	private FixedLengthStringData servagnt02 = new FixedLengthStringData(8).isAPartOf(rr505d03O, 8);
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String agcmchgrec = "AGCMCHGREC";
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6688 = "T6688";
	private static final String t1693 = "T1693";
	private static final String t1692 = "T1692";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Agtchgrec agtchgrec = new Agtchgrec();
	private P5001par p5001par = new P5001par();
	private T5679rec t5679rec = new T5679rec();
	private T6688rec t6688rec = new T6688rec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Rr505D02Inner rr505D02Inner = new Rr505D02Inner();
	private Iterator<Agcgpf> agcgpfIterator;
	private Agcgpf agcgpfEntity;
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmDAO",AgcmpfDAO.class);
	private AgqfpfDAO agqfpfDAO = getApplicationContext().getBean("agqfpfDAO", AgqfpfDAO.class);
	protected ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected AgentReportpfDAO agentReportDAO = getApplicationContext().getBean("agentReportpfDAO", AgentReportpfDAO.class);
	private ErorpfDAO erorpfDAO = getApplicationContext().getBean("erorpfDAO", ErorpfDAO.class);
	private List<Agcmpf> agcmpfList = new ArrayList<>();
	private boolean japanLocPermission = false;
	protected List<Itempf> itempfList = null;
	private static final String TJL02 = "TJL02";
	private Tjl02rec tjl02rec = new Tjl02rec();
	protected Itempf itempf = null;
	private boolean validAgentFlag=true;
	private boolean checkLicnsExpiryFlag=false;
	private List<AgentReportpf> agentReportpfList=null;
	private AgentReportpf agentReportpf=null;
	protected List<Itempf> t6579List = null;
	private Erorpf erorpf=null;
	private static final String VULCERT = "VULCERT";
	private static final String FXCERT = "FXCERT";
	private int count=0;
	private int sftcount=0;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		read1100,
		rlseLock1500,
		exit1900,
		a200Exit
	}

	public B5001() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		agcgpfIterator = (Iterator<Agcgpf>) parmArray[1];
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		go110();
		go120();
		setUpHeading140();
		closeBatch170();
		out185();
	}

protected void go110()
	{
		agcgpf.openInput();
		printerFile.openOutput();
		initialise200();
	}

protected void go120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void setUpHeading140()
	{
		a050MainHeading();
		/*PROCESS*/
		while ( !(isEQ(endOfFile,"Y"))) {
			mainProcessing1000();
		}

	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void out185()
	{
		agcgpf.close();
		printerFile.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
		datcon1250();
	}

protected void initialise210()
	{
		p5001par.parmRecord.set(conjobrec.params);
		endOfFile = "N";
		wsaaSub.set(ZERO);
		varcom.vrcmTime.set(getCobolTime());
	}

protected void datcon1250()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			conerrrec.params.set(datcon1rec.datcon1Rec);
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		wsaaTransactionDate.set(datcon1rec.intDate);
		/*EXIT*/
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case read1100:
					read1100();
				case rlseLock1500:
					rlseLock1500();
				case exit1900:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read1100()
	{
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			goTo(GotoLabel.read1100);
		}
		//smalchi2 for ILIFE-1105 STARTS
		//agcgpf.read(agcgpfRec);
		//if (agcgpf.isAtEnd()) {
			//endOfFile = "Y";
			//goTo(GotoLabel.exit1900);
		//}
		if(agcgpfIterator.hasNext())
		{
			agcgpfEntity=agcgpfIterator.next();
		}
		else {
			endOfFile = "Y";
			goTo(GotoLabel.exit1900);
		}
		
		
		
		wsaaCoy.set(agcgpfEntity.getChdrcoy());
		wsaaChdrnum.set(agcgpfEntity.getChdrnum());
		
		//ENDS
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		getStatcode2000();
		readT56793000();
		if (isEQ(wsaaValidStat,"N")) {
			goTo(GotoLabel.exit1900);
		}
		readT66884000();
		if (isEQ(t6688rec.agtchgsub,SPACES)) {
			goTo(GotoLabel.exit1900);
		}
		softlockChdr5000();
		if (isEQ(p5001par.comind,"Y")) {
			agtchgrec.updateFlag.set("Y");
		}
		else {
			agtchgrec.updateFlag.set("N");
		}
		wsaaWriteFile = "N";
		wsaaServAgent.set(chdrlifIO.getAgntnum());
		a100PrintRoutine();
		japanLocPermission = FeaConfg.isFeatureExist("9", "AGMTN019", appVars, "IT");
		if(japanLocPermission) {
			if(count==0)
			writeAgentReport();
			count++;
		}else{
			callSubroutine6000();
		}
		
		/* If the CHDR record has already been updated, e.g. Proposals, the*/
		/* update flag will have been set within the subroutine.*/
		if ((isEQ(agtchgrec.updateFlag,"U"))) {
			goTo(GotoLabel.rlseLock1500);
		}
		if(!japanLocPermission) {
			updateChdr7000();
		}
		wsaaWriteFile = "Y";
		a100PrintRoutine();
		if(!japanLocPermission) {
			housekeeping8000();
		}
	}

protected void rlseLock1500()
	{
		releaseSoftlock9000();
	}

protected void getStatcode2000()
	{
		go2100();
	}

protected void go2100()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(agcgpfEntity.getChdrcoy());
		chdrlifIO.setChdrnum(agcgpfEntity.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
		wsaaStatcode.set(chdrlifIO.getStatcode());
		compute(wsaaTranno, 0).set(add(1,chdrlifIO.getTranno()));
	}

protected void readT56793000()
	{
		go3100();
	}

protected void go3100()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(runparmrec.transcode);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(f321);
			databaseError006();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStat = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			checkStat3500();
		}
	}

protected void checkStat3500()
	{
		/*GO*/
		if (isEQ(wsaaStatcode,t5679rec.cnRiskStat[wsaaSub.toInt()])) {
			wsaaSub.set(13);
			wsaaValidStat = "Y";
		}
		/*EXIT*/
	}

protected void readT66884000()
	{
			go4100();
		}

protected void go4100()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6688);
		itemIO.setItemitem(wsaaStatcode);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			t6688rec.agtchgsub.set(SPACES);
			return ;
		}
		t6688rec.t6688Rec.set(itemIO.getGenarea());
	}

protected void softlockChdr5000()
	{
		go5100();
	}

protected void go5100()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(agcgpfEntity.getChdrnum());
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
	}

protected void callSubroutine6000()
	{
		go6100();
	}

protected void go6100()
	{
		agtchgrec.statuz.set(varcom.oK);
		agtchgrec.chdrcoy.set(agcgpfEntity.getChdrcoy());
		agtchgrec.chdrnum.set(agcgpfEntity.getChdrnum());
		agtchgrec.life.set(SPACES);
		agtchgrec.coverage.set(SPACES);
		agtchgrec.rider.set(SPACES);
		agtchgrec.seqno.set(ZERO);
		agtchgrec.planSuffix.set(ZERO);
		agtchgrec.tranno.set(wsaaTranno);
		agtchgrec.agntnumOld.set(p5001par.agentfrom);
		agtchgrec.agntnumNew.set(p5001par.agentto);
		agtchgrec.initCommFlag.set(p5001par.initflg);
		agtchgrec.rnwlCommFlag.set(p5001par.rnwlflg);
		agtchgrec.servCommFlag.set(p5001par.servflg);
		agtchgrec.orCommFlag.set(p5001par.zrorind);
		agtchgrec.batckey.set(batcdorrec.batchkey);
		agtchgrec.termid.set(varcom.vrcmTermid);
		agtchgrec.user.set(runparmrec.user);
		agtchgrec.transactionDate.set(wsaaTransactionDate);
		agtchgrec.transactionTime.set(varcom.vrcmTime);
		agtchgrec.language.set(runparmrec.language);
		callProgram(t6688rec.agtchgsub, agtchgrec.agentRec);
		if (isNE(agtchgrec.statuz,varcom.oK)) {
			conerrrec.params.set(agtchgrec.agentRec);
			conerrrec.statuz.set(agtchgrec.statuz);
			systemError005();
		}
	}

protected void updateChdr7000()
	{
		go7100();
		rewrt7110();
		writr7120();
	}

protected void go7100()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(agcgpfEntity.getChdrcoy());
		chdrlifIO.setChdrnum(agcgpfEntity.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
	}

protected void rewrt7110()
	{
		chdrlifIO.setCurrto(runparmrec.effdate);
		chdrlifIO.setValidflag("2");
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
	}

protected void writr7120()
	{
		if (isEQ(p5001par.comind,"Y")) {
			chdrlifIO.setAgntnum(p5001par.agentto);
		}
		chdrlifIO.setCurrfrom(runparmrec.effdate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setValidflag("1");
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(chdrlifIO.getParams());
			conerrrec.statuz.set(chdrlifIO.getStatuz());
			databaseError006();
		}
		if (isEQ(p5001par.comind, "Y")) {
			callBldenrl7100();
		}
	}

protected void callBldenrl7100()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlifIO.getChdrpfx());
		bldenrlrec.company.set(chdrlifIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlifIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			conerrrec.params.set(bldenrlrec.bldenrlrec);
			conerrrec.statuz.set(bldenrlrec.statuz);
			databaseError006();
		}
		/*EXIT*/
	}

protected void housekeeping8000()
	{
		writePtrn8100();
		updateBatch8500();
	}

protected void writePtrn8100()
	{
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(runparmrec.company);
		ptrnIO.setBatcbrn(runparmrec.batcbranch);
		ptrnIO.setBatcactyr(runparmrec.acctyear);
		ptrnIO.setBatcactmn(runparmrec.acctmonth);
		ptrnIO.setBatctrcde(runparmrec.transcode);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setPtrneff(runparmrec.effdate);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(runparmrec.user);
		ptrnIO.setDatesub(runparmrec.effdate);
		ptrnIO.setValidflag("1");
		String userid = ((SMARTAppVars)SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(userid);//PINNACLE-2954
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(ptrnIO.getParams());
			conerrrec.statuz.set(ptrnIO.getStatuz());
			databaseError006();
		}
	}

protected void updateBatch8500()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(runparmrec.company);
		batcuprec.batcbrn.set(runparmrec.batcbranch);
		batcuprec.batcactyr.set(runparmrec.acctyear);
		batcuprec.batcactmn.set(runparmrec.acctmonth);
		batcuprec.batctrcde.set(runparmrec.transcode);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
	}

protected void releaseSoftlock9000()
	{
	if(japanLocPermission) {
		if(sftcount==0) {
		go9100();
		}
	}else {
		go9100();
	}
	}

protected void go9100()
	{
		sftlockrec.function.set("UNLK");
		sftlockrec.company.set(runparmrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(agcgpfEntity.getChdrnum());
		sftlockrec.transaction.set(runparmrec.transcode);
		sftlockrec.user.set(runparmrec.user);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			conerrrec.params.set(sftlockrec.sftlockRec);
			conerrrec.statuz.set(sftlockrec.statuz);
			systemError005();
		}
	}

protected void a050MainHeading()
	{
		a052SetUpHeadingCompany();
		a054SetUpHeadingBranch();
		a056SetUpHeadingDates();
	}

protected void a052SetUpHeadingCompany()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(runparmrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			conerrrec.statuz.set(descIO.getStatuz());
			systemError005();
		}
		rh01Company.set(runparmrec.company);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void a054SetUpHeadingBranch()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(runparmrec.batcbranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			conerrrec.statuz.set(descIO.getStatuz());
			systemError005();
		}
		rh01Branch.set(runparmrec.batcbranch);
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void a056SetUpHeadingDates()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		agnt03.set(p5001par.agentfrom);
		agnt04.set(p5001par.agentto);
		/*EXIT*/
	}

protected void a100PrintRoutine()
	{
		a100Go();
	}

protected void a100Go()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr505h01(rr505H01, indicArea);
			wsaaOverflow.set("N");
		}
		if (isEQ(wsaaWriteFile,"N")) {
			initialize(wsaaOldTemp);
		}
		endOfFile2 = "N";
		wsaaXx.set(0);
		agcmpfList = agcmpfDAO.readAgcmpfData(chdrlifIO.getChdrcoy().toString(),chdrlifIO.getChdrnum().toString(),chdrlifIO.getAgntnum().toString());
		for(Agcmpf agcmpfobj : agcmpfList ) {
			a200ReadAgcm(agcmpfobj);
		}

		/* Print Agent Servicing                                           */
		if (isEQ(wsaaWriteFile,"Y")) {
			//servagnt01.set(chdrlifIO.getAgntnum());
			// Ticket #4594 [Report issue - For job L2AGENTCHG the details updated are incorrect] is Fixed
			servagnt01.set(p5001par.agentto);
			servagnt02.set(wsaaServAgent);
			printerFile.printRr505d03(rr505D03, indicArea);
		}
	}

protected void a200ReadAgcm(Agcmpf agcmpf)
	{
		try {
			a200Para(agcmpf);
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a200Para(Agcmpf agcmpf)
	{
		/* If processing Before Image, (when WSAA-WRITE-FILE = 'N')        */
		/* data stored first in working storage until End of File          */
		/* If processing After  Image, (when WSAA-WRITE-FILE = 'Y')        */
		/* data printed directly from  AGCMCHG file with Before            */
		/* Image also printed coming from working storage                  */
		wsaaXx.add(1);
 		if(agcmpfList.isEmpty())
		{
			agcmpf.setAgntnum(" ");
			agcmpf.setOvrdcat(" ");
			agcmpf.setInitcom(BigDecimal.ZERO);
			agcmpf.setCompay(BigDecimal.ZERO);
			agcmpf.setComern(BigDecimal.ZERO);
			if (isEQ(wsaaWriteFile,"N")) {
				endOfFile2 = "Y";
				goTo(GotoLabel.a200Exit);
			}
		}
 		if (isEQ(wsaaWriteFile,"Y")) {
			if (isEQ(wsaaAgent[wsaaXx.toInt()],SPACES)) {
				endOfFile2 = "Y";
			}
			if (agcmpfList.isEmpty() && isEQ(endOfFile2,"Y")) {
				goTo(GotoLabel.a200Exit);
			}
			//rr505D02Inner.agnt01.set(agcmchgIO.getAgntnum());
			// Ticket #4594 [Report issue - For job L2AGENTCHG the details updated are incorrect] is Fixed
			rr505D02Inner.agnt01.set(p5001par.agentto);

			rr505D02Inner.ovrdcat01.set(wsaaCat[wsaaXx.toInt()]);
			rr505D02Inner.initcom01.set(wsaaPay[wsaaXx.toInt()]);
			rr505D02Inner.compay01.set(wsaaPaid[wsaaXx.toInt()]);
			rr505D02Inner.comern01.set(wsaaEarn[wsaaXx.toInt()]);
			//ilife-1622 Starts
			rr505D02Inner.agnt02.set(wsaaAgent[wsaaXx.toInt()]);
			rr505D02Inner.ovrdcat02.set(wsaaCat[wsaaXx.toInt()]);
			rr505D02Inner.initcom02.set(wsaaPay[wsaaXx.toInt()]);
			rr505D02Inner.compay02.set(wsaaPaid[wsaaXx.toInt()]);
			rr505D02Inner.comern02.set(wsaaEarn[wsaaXx.toInt()]);
			if (isEQ(wsaaXx,1)) {
				rr505D02Inner.chdrnum01.set(wsaaChdrnum);
			}
			else {
				rr505D02Inner.chdrnum01.set(SPACES);
			}
			printerFile.printRr505d02(rr505D02Inner.rr505D02, indicArea);
			tmp.setSubtract(tmp, 1);
			//ilife-1622 ends
		}
		else {
			endOfFile2 = "Y";
			wsaaAgent[wsaaXx.toInt()].set(agcmpf.getAgntnum());
			wsaaCat[wsaaXx.toInt()].set(agcmpf.getOvrdcat());
			wsaaPay[wsaaXx.toInt()].set(agcmpf.getInitcom());
			wsaaPaid[wsaaXx.toInt()].set(agcmpf.getCompay());
 			wsaaEarn[wsaaXx.toInt()].set(agcmpf.getComern());
 			tmp.add(1);
		}
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRr505h01(rr505H01, indicArea);
			wsaaOverflow.set("N");
		}
		
	}

	private void writeAgentReport() {
		StringBuffer qfDesc = new StringBuffer("");
		agentReportpfList =new LinkedList<>();
		ChdrpfDAO factory = DAOFactory.getChdrpfDAO();
		List<Chdrpf> chdrpfList = factory.findContractsByAgent(p5001par.agentfrom.toString(),'2', "CH", readT5679());
		chdrpfList.forEach(l -> {
			agentReportpf= new AgentReportpf();
			// Check contract present in TJL02
			itempfList = itempfDAO.getAllItemitem("IT", runparmrec.company.toString(), TJL02,
					l.getCnttype());/* IJTI-1523 */
			// If contract not present in TJL02 write data in AgentReport table
			if (null == itempfList || itempfList.isEmpty()) {
				agentReportpf.setCompany(chdrlifIO.getChdrcoy().toByte());
				agentReportpf.setChdrnum(l.getChdrnum());
				agentReportpf.setOldAgent(Integer.parseInt(p5001par.agentfrom.toString()));
				agentReportpf.setNewAgent(Integer.parseInt(p5001par.agentto.toString()));
				agentReportpf.setEffectiveDate(runparmrec.effdate.toInt());
				agentReportpf.setProcessingDate(runparmrec.effdate.toInt());
				agentReportpf.setUsername(p5001par.username.toString());
				agentReportpf.setRemarks(qfDesc.toString());
				agentReportpf.setIsExtracted("N");
				agentReportpfList.add(agentReportpf);
					if(sftcount==0) {
					go9100();
					sftcount++;
					}
					agcgpfEntity.setChdrnum(agentReportpf.getChdrnum());
					callSubroutine6000();
					updateChdr7000();
					housekeeping8000();
			} else {
				// get data from TJL02 table-start
				tjl02rec.tjl02Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
				List<String> qfList = new ArrayList<>();
				Map<String, String> qfMapDesc = new HashMap<>();
				for (int i = 0; i <= tjl02rec.qlfy.length - 1; i++) {
					if (null != tjl02rec.qlfy[i] && !tjl02rec.qlfy[i].toString().trim().isEmpty()) {
						qfList.add(tjl02rec.qlfy[i].toString());
						qfMapDesc.put(tjl02rec.qlfy[i].toString(), tjl02rec.qlfydesc[i].toString());
					}
				} // get data from TJL02 table-ends
				if (null != p5001par.agentto.toString() && !p5001par.agentto.toString().isEmpty()) { // Compare TJL02 to AGQFPF-start
					List<String> qfList1 = agqfpfDAO.getQualificationListbyAgntNum(p5001par.agentto.toString());
					if ("Y".equals(p5001par.comind.toString()) || "Y".equals(p5001par.initflg.toString())
							|| "Y".equals(p5001par.servflg.toString()) || "Y".equals(p5001par.rnwlflg.toString()))
						agcmpfList = agcmpfDAO.readAgcmpfData(l.getChdrcoy().toString(), l.getChdrnum(),
								p5001par.agentfrom.toString());/* IJTI-1523 */
					qfList.sort(Comparator.naturalOrder());
					qfList1.sort(Comparator.naturalOrder());
					List<Agqfpf> agqfpfList=agqfpfDAO.getAgqfListbyAgentNumber(p5001par.agentto.toString());
					qfList.forEach(ql->{
						agqfpfList.forEach(crt->{
							if(ql.equals(crt.getQualification())) {
								if(crt.getQualification().trim().equals(FXCERT) && isLT(crt.getDateend(), wsaaTransactionDate) || isEQ(crt.getDateend(),SPACE)) {
									qfDesc.append(p5001par.agentto.toString() + " ");
									qfDesc.append(getErrorMsg("jl19").trim());
									qfDesc.append("; ");
									checkLicnsExpiryFlag=true;
								}else if(crt.getQualification().trim().equals(VULCERT) && isLT(crt.getDateend(), wsaaTransactionDate) || isEQ(crt.getDateend(),SPACE)) {
									qfDesc.append(p5001par.agentto.toString() + " ");
									qfDesc.append(getErrorMsg("jl20").trim());
									checkLicnsExpiryFlag=true;
								}
							}
						});
					});
					for (int i = 0; i < qfList.size(); i++) {
						if (!qfList1.contains(qfList.get(i))) {
							if (i == 0) {
								qfDesc.append(p5001par.agentto.toString() + " ");
								if (qfList.get(i).trim().equals(FXCERT)) {
									qfDesc.append(getErrorMsg("jl11").trim());
								} else if (qfList.get(i).trim().equals(VULCERT)) {  
									qfDesc.append(getErrorMsg("jl12").trim());
								}
							} else {
								qfDesc.append("; " + p5001par.agentto.toString() + " ");
								if (qfList.get(i).trim().equals(FXCERT)) {
									qfDesc.append(getErrorMsg("jl11").trim());
								} else if (qfList.get(i).trim().equals(VULCERT)) {
									qfDesc.append(getErrorMsg("jl12").trim());
								}
							}
							validAgentFlag=false;
						}
					}
				}
				agentReportpf.setCompany(chdrlifIO.getChdrcoy().toByte());
				agentReportpf.setChdrnum(l.getChdrnum());
				agentReportpf.setOldAgent(Integer.parseInt(p5001par.agentfrom.toString()));
				if(checkLicnsExpiryFlag || !validAgentFlag) {
					agentReportpf.setNewAgent(Integer.parseInt(p5001par.agentfrom.toString()));
				}else {
				agentReportpf.setNewAgent(Integer.parseInt(p5001par.agentto.toString()));
				}
				agentReportpf.setEffectiveDate(runparmrec.effdate.toInt());
				agentReportpf.setProcessingDate(runparmrec.effdate.toInt());
				agentReportpf.setUsername(p5001par.username.toString());
				agentReportpf.setRemarks(qfDesc.toString());
				agentReportpf.setIsExtracted("N");
				agentReportpfList.add(agentReportpf);
				qfDesc.delete(0, qfDesc.length());
				if(!checkLicnsExpiryFlag && validAgentFlag ) {
					if(sftcount==0) {
					go9100();
					sftcount++;
					}
					agcgpfEntity.setChdrnum(agentReportpf.getChdrnum());
					callSubroutine6000();
					updateChdr7000();
					housekeeping8000();
					}
				checkLicnsExpiryFlag=false;
			}
		});
		agentReportDAO.saveAgentData(agentReportpfList);
	}
	private List<String> readT5679(){
		t6579List = itempfDAO.getAllItemitem("IT", runparmrec.company.toString(), "T5679",
				runparmrec.transcode.toString());
		t5679rec.t5679Rec.set(StringUtil.rawToString(t6579List.get(0).getGenarea()));
		List<String> contractStatusList = new ArrayList<>();
		for (int i = 0; i <= t5679rec.cnRiskStat.length - 1; i++) {
			if (null != t5679rec.cnRiskStat[i] && !t5679rec.cnRiskStat[i].toString().trim().isEmpty()) {
				contractStatusList.add(t5679rec.cnRiskStat[i].toString());

			}
		} 
		return contractStatusList;
	}
	private String getErrorMsg(String errorCode) {
		erorpf=new Erorpf();
		erorpf.setErorcoy("");
		erorpf.setErorpfx("ER");
		erorpf.setErorlang("E");
		erorpf.setEroreror(errorCode);
		String erordesc=erorpfDAO.getErrorMessage(erorpf);
		return erordesc;
	}
/*
 * Class transformed  from Data Structure RR505-D02--INNER
 */
private static final class Rr505D02Inner {

		/*  Detail line -                                                  */
	private FixedLengthStringData rr505D02 = new FixedLengthStringData(128);
	private FixedLengthStringData rr505d02O = new FixedLengthStringData(128).isAPartOf(rr505D02, 0);
	private FixedLengthStringData chdrnum01 = new FixedLengthStringData(8).isAPartOf(rr505d02O, 0);
	private FixedLengthStringData agnt01 = new FixedLengthStringData(8).isAPartOf(rr505d02O, 8);
	private FixedLengthStringData ovrdcat01 = new FixedLengthStringData(1).isAPartOf(rr505d02O, 16);
	private ZonedDecimalData initcom01 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 17);
	private ZonedDecimalData compay01 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 34);
	private FixedLengthStringData agnt02 = new FixedLengthStringData(8).isAPartOf(rr505d02O, 51);
	private FixedLengthStringData ovrdcat02 = new FixedLengthStringData(1).isAPartOf(rr505d02O, 59);
	private ZonedDecimalData initcom02 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 60);
	private ZonedDecimalData compay02 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 77);
	private ZonedDecimalData comern01 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 94);
	private ZonedDecimalData comern02 = new ZonedDecimalData(17, 2).isAPartOf(rr505d02O, 111);
}
}
