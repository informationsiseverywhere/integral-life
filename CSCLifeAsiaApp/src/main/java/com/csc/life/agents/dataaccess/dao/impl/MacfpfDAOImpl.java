/******************** */
/*Author  :Liwei      */
/*Purpose :curd data for Macfpf       */
/*Date    :2018.10.26           */ 
package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.MacfpfDAO;
import com.csc.life.agents.dataaccess.model.Macfpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class MacfpfDAOImpl extends BaseDAOImpl<Macfpf> implements MacfpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(MacfpfDAOImpl.class);

	public List<Macfpf> searchMacfpfRecord(String agntcoy,String agntnum,int effdate) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder();
		sb.append("SELECT AGNTCOY,AGNTNUM,EFFDATE,AGMVTY,ZRPTGA,ZRPTGB,ZRPTGC,ZRPTGD,MLAGTTYP FROM MACFPRD WHERE 1=1 AND AGNTCOY=? AND AGNTNUM= ?  ");
		if(effdate != -1){
			sb.append("AND EFFDATE<=? ");
		}
		sb.append("ORDER BY AGNTCOY ASC, AGNTNUM ASC, EFFDATE DESC, UNIQUE_NUMBER DESC");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		List<Macfpf> list = new ArrayList<Macfpf>();
		Macfpf macfpf = null;
		try {
			ps.setString(1, agntcoy);
			ps.setString(2, agntnum);
			if(effdate != -1){
				ps.setInt(3, effdate);
			}
			rs = executeQuery(ps);
			while (rs.next()) {
				macfpf = new Macfpf();
				macfpf.setAgntcoy(agntcoy);
				macfpf.setAgntnum(agntnum);
				macfpf.setEffdate(rs.getInt(3));
				macfpf.setAgmvty(rs.getString(4));
				macfpf.setZrptga(rs.getString(5));
				macfpf.setZrptgb(rs.getString(6));
				macfpf.setZrptgc(rs.getString(7));
				macfpf.setZrptgd(rs.getString(8));
				macfpf.setMlagttyp(rs.getString(9));
				list.add(macfpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchMacfpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	}

	
}