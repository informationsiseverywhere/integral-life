package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr58trec extends ExternalData {

  public FixedLengthStringData tr58tRec = new FixedLengthStringData(500); 

	public ZonedDecimalData effperd = new ZonedDecimalData(6,0).isAPartOf(tr58tRec,0);

	public ZonedDecimalData months = new ZonedDecimalData(2,0).isAPartOf(tr58tRec,6); 
	
public FixedLengthStringData chdrtypes = new FixedLengthStringData(90).isAPartOf(tr58tRec,8);
public FixedLengthStringData[] chdrtype = FLSArrayPartOfStructure(30,3,chdrtypes, 0);
public FixedLengthStringData chdrtype1 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 0);

public FixedLengthStringData chdrtype2 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 3);

public FixedLengthStringData chdrtype3 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 6);

public FixedLengthStringData chdrtype4 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 9);

public FixedLengthStringData chdrtype5 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 12);

public FixedLengthStringData chdrtype6 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 15);

public FixedLengthStringData chdrtype7 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 18);

public FixedLengthStringData chdrtype8 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 21);

public FixedLengthStringData chdrtype9 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 24);

public FixedLengthStringData chdrtype10 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 27);

public FixedLengthStringData chdrtype11 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 30);

public FixedLengthStringData chdrtype12 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 33);

public FixedLengthStringData chdrtype13 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 36);

public FixedLengthStringData chdrtype14 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 39);

public FixedLengthStringData chdrtype15 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 42);

public FixedLengthStringData chdrtype16 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 45);

public FixedLengthStringData chdrtype17 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 48);

public FixedLengthStringData chdrtype18 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 51);

public FixedLengthStringData chdrtype19 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 54);

public FixedLengthStringData chdrtype20 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 57);

public FixedLengthStringData chdrtype21 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 60);

public FixedLengthStringData chdrtype22 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 63);

public FixedLengthStringData chdrtype23 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 66);

public FixedLengthStringData chdrtype24 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 69);

public FixedLengthStringData chdrtype25 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 72);

public FixedLengthStringData chdrtype26 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 75);

public FixedLengthStringData chdrtype27 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 78);

public FixedLengthStringData chdrtype28 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 81);

public FixedLengthStringData chdrtype29 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 84);

public FixedLengthStringData chdrtype30 = new FixedLengthStringData(3).isAPartOf(chdrtypes, 87);
//Ticket #TMLII-1918
public FixedLengthStringData crtables = new FixedLengthStringData(200).isAPartOf(tr58tRec, 98);
public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(50, 4, crtables, 0);
  public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(crtables,0); 
  public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(crtables,4); 
  public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(crtables,8); 
  public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(crtables,12); 
  public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(crtables,16); 
  public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(crtables,20); 
  public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(crtables,24); 
  public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(crtables,28); 
  public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(crtables,32); 
  public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(crtables,36); 
  public FixedLengthStringData crtable11 = new FixedLengthStringData(4).isAPartOf(crtables,40); 
  public FixedLengthStringData crtable12 = new FixedLengthStringData(4).isAPartOf(crtables,44); 
  public FixedLengthStringData crtable13 = new FixedLengthStringData(4).isAPartOf(crtables,48); 
  public FixedLengthStringData crtable14 = new FixedLengthStringData(4).isAPartOf(crtables,52); 
  public FixedLengthStringData crtable15 = new FixedLengthStringData(4).isAPartOf(crtables,56); 
  public FixedLengthStringData crtable16 = new FixedLengthStringData(4).isAPartOf(crtables,60); 
  public FixedLengthStringData crtable17 = new FixedLengthStringData(4).isAPartOf(crtables,64); 
  public FixedLengthStringData crtable18 = new FixedLengthStringData(4).isAPartOf(crtables,68); 
  public FixedLengthStringData crtable19 = new FixedLengthStringData(4).isAPartOf(crtables,72); 
  public FixedLengthStringData crtable20 = new FixedLengthStringData(4).isAPartOf(crtables,76); 
  public FixedLengthStringData crtable21 = new FixedLengthStringData(4).isAPartOf(crtables,80); 
  public FixedLengthStringData crtable22 = new FixedLengthStringData(4).isAPartOf(crtables,84); 
  public FixedLengthStringData crtable23 = new FixedLengthStringData(4).isAPartOf(crtables,88); 
  public FixedLengthStringData crtable24 = new FixedLengthStringData(4).isAPartOf(crtables,92); 
  public FixedLengthStringData crtable25 = new FixedLengthStringData(4).isAPartOf(crtables,96); 
  public FixedLengthStringData crtable26 = new FixedLengthStringData(4).isAPartOf(crtables,100); 
  public FixedLengthStringData crtable27 = new FixedLengthStringData(4).isAPartOf(crtables,104); 
  public FixedLengthStringData crtable28 = new FixedLengthStringData(4).isAPartOf(crtables,108); 
  public FixedLengthStringData crtable29 = new FixedLengthStringData(4).isAPartOf(crtables,112); 
  public FixedLengthStringData crtable30 = new FixedLengthStringData(4).isAPartOf(crtables,116); 
  public FixedLengthStringData crtable31 = new FixedLengthStringData(4).isAPartOf(crtables,120); 
  public FixedLengthStringData crtable32 = new FixedLengthStringData(4).isAPartOf(crtables,124); 
  public FixedLengthStringData crtable33 = new FixedLengthStringData(4).isAPartOf(crtables,128); 
  public FixedLengthStringData crtable34 = new FixedLengthStringData(4).isAPartOf(crtables,132); 
  public FixedLengthStringData crtable35 = new FixedLengthStringData(4).isAPartOf(crtables,136); 
  public FixedLengthStringData crtable36 = new FixedLengthStringData(4).isAPartOf(crtables,140); 
  public FixedLengthStringData crtable37 = new FixedLengthStringData(4).isAPartOf(crtables,144); 
  public FixedLengthStringData crtable38 = new FixedLengthStringData(4).isAPartOf(crtables,148); 
  public FixedLengthStringData crtable39 = new FixedLengthStringData(4).isAPartOf(crtables,152); 
  public FixedLengthStringData crtable40 = new FixedLengthStringData(4).isAPartOf(crtables,156); 
  public FixedLengthStringData crtable41 = new FixedLengthStringData(4).isAPartOf(crtables,160); 
  public FixedLengthStringData crtable42 = new FixedLengthStringData(4).isAPartOf(crtables,164); 
  public FixedLengthStringData crtable43 = new FixedLengthStringData(4).isAPartOf(crtables,168); 
  public FixedLengthStringData crtable44 = new FixedLengthStringData(4).isAPartOf(crtables,172); 
  public FixedLengthStringData crtable45 = new FixedLengthStringData(4).isAPartOf(crtables,176); 
  public FixedLengthStringData crtable46 = new FixedLengthStringData(4).isAPartOf(crtables,180); 
  public FixedLengthStringData crtable47 = new FixedLengthStringData(4).isAPartOf(crtables,184); 
  public FixedLengthStringData crtable48 = new FixedLengthStringData(4).isAPartOf(crtables,188); 
  public FixedLengthStringData crtable49 = new FixedLengthStringData(4).isAPartOf(crtables,192); 
  public FixedLengthStringData crtable50 = new FixedLengthStringData(4).isAPartOf(crtables,196); 

public FixedLengthStringData filler = new FixedLengthStringData(202).isAPartOf(tr58tRec, 298, FILLER_REDEFINE); 


		
    public String[] fieldnamelist={"chdrtype01s","chdrtype02s","chdrtype03s","chdrtype04s","chdrtype05s","chdrtype06s","chdrtype07s","chdrtype08s","chdrtype09s","chdrtype10s","chdrtype11s","chdrtype12s","chdrtype13s","chdrtype14s","chdrtype15s","chdrtype16s","chdrtype17s","chdrtype18s","chdrtype19s","chdrtype20s","chdrtype21s","chdrtype22s","chdrtype23s","chdrtype24s","chdrtype25s","chdrtype26s","chdrtype27s","chdrtype28s","chdrtype29s","chdrtype30s","effperds","months",	 "crtable01s","crtable02s","crtable03s","crtable04s","crtable05s","crtable06s","crtable07s","crtable08s","crtable09s","crtable10s","crtable11s","crtable12s","crtable13s","crtable14s","crtable15s","crtable16s","crtable17s","crtable18s","crtable19s","crtable20s","crtable21s","crtable22s","crtable23s","crtable24s","crtable25s","crtable26s","crtable27s","crtable28s","crtable29s","crtable30s","crtable31s","crtable32s","crtable33s","crtable34s","crtable35s","crtable36s","crtable37s","crtable38s","crtable39s","crtable40s","crtable41s","crtable42s","crtable43s","crtable44s","crtable45s","crtable46s","crtable47s","crtable48s","crtable49s","crtable50s"};
  //Ticket #TMLII-1918
	public Tr58trec(){
		
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr58tRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr58tRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return(fieldnamelist.clone());//IJTI-316
	}
}
