package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:40
 * Description:
 * Copybook name: ACMVAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvagtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvagtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvagtKey = new FixedLengthStringData(64).isAPartOf(acmvagtFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvagtBatccoy = new FixedLengthStringData(1).isAPartOf(acmvagtKey, 0);
  	public FixedLengthStringData acmvagtBatcbrn = new FixedLengthStringData(2).isAPartOf(acmvagtKey, 1);
  	public PackedDecimalData acmvagtBatcactyr = new PackedDecimalData(4, 0).isAPartOf(acmvagtKey, 3);
  	public PackedDecimalData acmvagtBatcactmn = new PackedDecimalData(2, 0).isAPartOf(acmvagtKey, 6);
  	public FixedLengthStringData acmvagtBatctrcde = new FixedLengthStringData(4).isAPartOf(acmvagtKey, 8);
  	public FixedLengthStringData acmvagtBatcbatch = new FixedLengthStringData(5).isAPartOf(acmvagtKey, 12);
  	public FixedLengthStringData acmvagtRldgacct = new FixedLengthStringData(16).isAPartOf(acmvagtKey, 17);
  	public FixedLengthStringData acmvagtOrigcurr = new FixedLengthStringData(3).isAPartOf(acmvagtKey, 33);
  	public FixedLengthStringData acmvagtSacscode = new FixedLengthStringData(2).isAPartOf(acmvagtKey, 36);
  	public FixedLengthStringData acmvagtSacstyp = new FixedLengthStringData(2).isAPartOf(acmvagtKey, 38);
  	public PackedDecimalData acmvagtTranno = new PackedDecimalData(5, 0).isAPartOf(acmvagtKey, 40);
  	public FixedLengthStringData acmvagtRdocnum = new FixedLengthStringData(9).isAPartOf(acmvagtKey, 43);
  	public PackedDecimalData acmvagtJrnseq = new PackedDecimalData(3, 0).isAPartOf(acmvagtKey, 52);
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(acmvagtKey, 54, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvagtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvagtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}