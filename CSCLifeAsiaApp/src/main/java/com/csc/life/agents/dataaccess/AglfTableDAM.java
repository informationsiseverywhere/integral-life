package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AglfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:33
 * Class transformed from AGLF.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AglfTableDAM extends AglfpfTableDAM {

	public AglfTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGLF");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "DTEAPP, " +
		            "DTETRM, " +
		            "TRMCDE, " +
		            "DTEEXP, " +
		            "DTEPAY, " +
		            "CURRCODE, " +
		            "PFTFLG, " +
		            "RASFLG, " +
		            "BCMTAB, " +
		            "RCMTAB, " +
		            "SCMTAB, " +
		            "OCMTAB, " +
		            "AGCLS, " +
		            "REPORTAG, " +
		            "OVCPC, " +
		            "TAXMETH, " +
		            "IRDNO, " +
		            "TAXCDE, " +
		            "TAXALW, " +
		            "SPRSCHM, " +
		            "SPRPRC, " +
		            "PAYCLT, " +
		            "PAYMTH, " +
		            "PAYFRQ, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "INTCRD, " +
		            "FIXPRC, " +
		            "BMAFLG, " +
		            "EXCAGR, " +
		            "HSELN, " +
		            "COMLN, " +
		            "CARLN, " +
		            "OFFRENT, " +
		            "OTHLN, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "ARACDE, " +
		            "MINSTA, " +
		            "ZRORCODE, " +
		            "EFFDATE, " +
		            "TAGSUSIND, " +
		            "TLAGLICNO, " +
		            "TLICEXPDT, " +
		            "TCOLPRCT, " +
		            "TCOLMAX, " +
		            "TSALESUNT, " +
		            "PRDAGENT, " +
		            "AGCCQIND, " +
		          //TMLII-281 AG-01-002
		            "ZRECRUIT, " +
		            "VALIDFLAG, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               dteapp,
                               dtetrm,
                               trmcde,
                               dteexp,
                               dtepay,
                               currcode,
                               pftflg,
                               rasflg,
                               bcmtab,
                               rcmtab,
                               scmtab,
                               ocmtab,
                               agentClass,
                               reportag,
                               ovcpc,
                               taxmeth,
                               irdno,
                               taxcde,
                               taxalw,
                               sprschm,
                               sprprc,
                               payclt,
                               paymth,
                               payfrq,
                               facthous,
                               bankkey,
                               bankacckey,
                               intcrd,
                               fixprc,
                               bmaflg,
                               exclAgmt,
                               houseLoan,
                               computerLoan,
                               carLoan,
                               officeRent,
                               otherLoans,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               aracde,
                               minsta,
                               zrorcode,
                               effdate,
                               tagsusind,
                               tlaglicno,
                               tlicexpdt,
                               tcolprct,
                               tcolmax,
                               tsalesunt,
                               prdagent,
                               agccqind,
                               //TMLII-281 AG-01-002
                               zrecruit,
                               validflag,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(agntcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(289);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getDteapp().toInternal()
					+ getDtetrm().toInternal()
					+ getTrmcde().toInternal()
					+ getDteexp().toInternal()
					+ getDtepay().toInternal()
					+ getCurrcode().toInternal()
					+ getPftflg().toInternal()
					+ getRasflg().toInternal()
					+ getBcmtab().toInternal()
					+ getRcmtab().toInternal()
					+ getScmtab().toInternal()
					+ getOcmtab().toInternal()
					+ getAgentClass().toInternal()
					+ getReportag().toInternal()
					+ getOvcpc().toInternal()
					+ getTaxmeth().toInternal()
					+ getIrdno().toInternal()
					+ getTaxcde().toInternal()
					+ getTaxalw().toInternal()
					+ getSprschm().toInternal()
					+ getSprprc().toInternal()
					+ getPayclt().toInternal()
					+ getPaymth().toInternal()
					+ getPayfrq().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getIntcrd().toInternal()
					+ getFixprc().toInternal()
					+ getBmaflg().toInternal()
					+ getExclAgmt().toInternal()
					+ getHouseLoan().toInternal()
					+ getComputerLoan().toInternal()
					+ getCarLoan().toInternal()
					+ getOfficeRent().toInternal()
					+ getOtherLoans().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getAracde().toInternal()
					+ getMinsta().toInternal()
					+ getZrorcode().toInternal()
					+ getEffdate().toInternal()
					+ getTagsusind().toInternal()
					+ getTlaglicno().toInternal()
					+ getTlicexpdt().toInternal()
					+ getTcolprct().toInternal()
					+ getTcolmax().toInternal()
					+ getTsalesunt().toInternal()
					+ getPrdagent().toInternal()
					+ getAgccqind().toInternal()
					//TMLII-281 AG-01-002
					+ getZrecruit().toInternal()
					+ getValidflag().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, dteapp);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, trmcde);
			what = ExternalData.chop(what, dteexp);
			what = ExternalData.chop(what, dtepay);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, pftflg);
			what = ExternalData.chop(what, rasflg);
			what = ExternalData.chop(what, bcmtab);
			what = ExternalData.chop(what, rcmtab);
			what = ExternalData.chop(what, scmtab);
			what = ExternalData.chop(what, ocmtab);
			what = ExternalData.chop(what, agentClass);
			what = ExternalData.chop(what, reportag);
			what = ExternalData.chop(what, ovcpc);
			what = ExternalData.chop(what, taxmeth);
			what = ExternalData.chop(what, irdno);
			what = ExternalData.chop(what, taxcde);
			what = ExternalData.chop(what, taxalw);
			what = ExternalData.chop(what, sprschm);
			what = ExternalData.chop(what, sprprc);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, paymth);
			what = ExternalData.chop(what, payfrq);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, intcrd);
			what = ExternalData.chop(what, fixprc);
			what = ExternalData.chop(what, bmaflg);
			what = ExternalData.chop(what, exclAgmt);
			what = ExternalData.chop(what, houseLoan);
			what = ExternalData.chop(what, computerLoan);
			what = ExternalData.chop(what, carLoan);
			what = ExternalData.chop(what, officeRent);
			what = ExternalData.chop(what, otherLoans);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, minsta);
			what = ExternalData.chop(what, zrorcode);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, tagsusind);
			what = ExternalData.chop(what, tlaglicno);
			what = ExternalData.chop(what, tlicexpdt);
			what = ExternalData.chop(what, tcolprct);
			what = ExternalData.chop(what, tcolmax);
			what = ExternalData.chop(what, tsalesunt);
			what = ExternalData.chop(what, prdagent);
			what = ExternalData.chop(what, agccqind);
			//TMLII-281 AG-01-002
			what = ExternalData.chop(what, zrecruit);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getDteapp() {
		return dteapp;
	}
	public void setDteapp(Object what) {
		setDteapp(what, false);
	}
	public void setDteapp(Object what, boolean rounded) {
		if (rounded)
			dteapp.setRounded(what);
		else
			dteapp.set(what);
	}	
	public PackedDecimalData getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Object what) {
		setDtetrm(what, false);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			dtetrm.setRounded(what);
		else
			dtetrm.set(what);
	}	
	public FixedLengthStringData getTrmcde() {
		return trmcde;
	}
	public void setTrmcde(Object what) {
		trmcde.set(what);
	}	
	public PackedDecimalData getDteexp() {
		return dteexp;
	}
	public void setDteexp(Object what) {
		setDteexp(what, false);
	}
	public void setDteexp(Object what, boolean rounded) {
		if (rounded)
			dteexp.setRounded(what);
		else
			dteexp.set(what);
	}	
	public PackedDecimalData getDtepay() {
		return dtepay;
	}
	public void setDtepay(Object what) {
		setDtepay(what, false);
	}
	public void setDtepay(Object what, boolean rounded) {
		if (rounded)
			dtepay.setRounded(what);
		else
			dtepay.set(what);
	}	
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}	
	public FixedLengthStringData getPftflg() {
		return pftflg;
	}
	public void setPftflg(Object what) {
		pftflg.set(what);
	}	
	public FixedLengthStringData getRasflg() {
		return rasflg;
	}
	public void setRasflg(Object what) {
		rasflg.set(what);
	}	
	public FixedLengthStringData getBcmtab() {
		return bcmtab;
	}
	public void setBcmtab(Object what) {
		bcmtab.set(what);
	}	
	public FixedLengthStringData getRcmtab() {
		return rcmtab;
	}
	public void setRcmtab(Object what) {
		rcmtab.set(what);
	}	
	public FixedLengthStringData getScmtab() {
		return scmtab;
	}
	public void setScmtab(Object what) {
		scmtab.set(what);
	}	
	public FixedLengthStringData getOcmtab() {
		return ocmtab;
	}
	public void setOcmtab(Object what) {
		ocmtab.set(what);
	}	
	public FixedLengthStringData getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(Object what) {
		agentClass.set(what);
	}	
	public FixedLengthStringData getReportag() {
		return reportag;
	}
	public void setReportag(Object what) {
		reportag.set(what);
	}	
	public PackedDecimalData getOvcpc() {
		return ovcpc;
	}
	public void setOvcpc(Object what) {
		setOvcpc(what, false);
	}
	public void setOvcpc(Object what, boolean rounded) {
		if (rounded)
			ovcpc.setRounded(what);
		else
			ovcpc.set(what);
	}	
	public FixedLengthStringData getTaxmeth() {
		return taxmeth;
	}
	public void setTaxmeth(Object what) {
		taxmeth.set(what);
	}	
	public FixedLengthStringData getIrdno() {
		return irdno;
	}
	public void setIrdno(Object what) {
		irdno.set(what);
	}	
	public FixedLengthStringData getTaxcde() {
		return taxcde;
	}
	public void setTaxcde(Object what) {
		taxcde.set(what);
	}	
	public PackedDecimalData getTaxalw() {
		return taxalw;
	}
	public void setTaxalw(Object what) {
		setTaxalw(what, false);
	}
	public void setTaxalw(Object what, boolean rounded) {
		if (rounded)
			taxalw.setRounded(what);
		else
			taxalw.set(what);
	}	
	public FixedLengthStringData getSprschm() {
		return sprschm;
	}
	public void setSprschm(Object what) {
		sprschm.set(what);
	}	
	public PackedDecimalData getSprprc() {
		return sprprc;
	}
	public void setSprprc(Object what) {
		setSprprc(what, false);
	}
	public void setSprprc(Object what, boolean rounded) {
		if (rounded)
			sprprc.setRounded(what);
		else
			sprprc.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getPaymth() {
		return paymth;
	}
	public void setPaymth(Object what) {
		paymth.set(what);
	}	
	public FixedLengthStringData getPayfrq() {
		return payfrq;
	}
	public void setPayfrq(Object what) {
		payfrq.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public PackedDecimalData getIntcrd() {
		return intcrd;
	}
	public void setIntcrd(Object what) {
		setIntcrd(what, false);
	}
	public void setIntcrd(Object what, boolean rounded) {
		if (rounded)
			intcrd.setRounded(what);
		else
			intcrd.set(what);
	}	
	public PackedDecimalData getFixprc() {
		return fixprc;
	}
	public void setFixprc(Object what) {
		setFixprc(what, false);
	}
	public void setFixprc(Object what, boolean rounded) {
		if (rounded)
			fixprc.setRounded(what);
		else
			fixprc.set(what);
	}	
	public FixedLengthStringData getBmaflg() {
		return bmaflg;
	}
	public void setBmaflg(Object what) {
		bmaflg.set(what);
	}	
	public FixedLengthStringData getExclAgmt() {
		return exclAgmt;
	}
	public void setExclAgmt(Object what) {
		exclAgmt.set(what);
	}	
	public FixedLengthStringData getHouseLoan() {
		return houseLoan;
	}
	public void setHouseLoan(Object what) {
		houseLoan.set(what);
	}	
	public FixedLengthStringData getComputerLoan() {
		return computerLoan;
	}
	public void setComputerLoan(Object what) {
		computerLoan.set(what);
	}	
	public FixedLengthStringData getCarLoan() {
		return carLoan;
	}
	public void setCarLoan(Object what) {
		carLoan.set(what);
	}	
	public FixedLengthStringData getOfficeRent() {
		return officeRent;
	}
	public void setOfficeRent(Object what) {
		officeRent.set(what);
	}	
	public FixedLengthStringData getOtherLoans() {
		return otherLoans;
	}
	public void setOtherLoans(Object what) {
		otherLoans.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}	
	public PackedDecimalData getMinsta() {
		return minsta;
	}
	public void setMinsta(Object what) {
		setMinsta(what, false);
	}
	public void setMinsta(Object what, boolean rounded) {
		if (rounded)
			minsta.setRounded(what);
		else
			minsta.set(what);
	}	
	public FixedLengthStringData getZrorcode() {
		return zrorcode;
	}
	public void setZrorcode(Object what) {
		zrorcode.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getTagsusind() {
		return tagsusind;
	}
	public void setTagsusind(Object what) {
		tagsusind.set(what);
	}	
	public FixedLengthStringData getTlaglicno() {
		return tlaglicno;
	}
	public void setTlaglicno(Object what) {
		tlaglicno.set(what);
	}	
	public PackedDecimalData getTlicexpdt() {
		return tlicexpdt;
	}
	public void setTlicexpdt(Object what) {
		setTlicexpdt(what, false);
	}
	public void setTlicexpdt(Object what, boolean rounded) {
		if (rounded)
			tlicexpdt.setRounded(what);
		else
			tlicexpdt.set(what);
	}	
	public PackedDecimalData getTcolprct() {
		return tcolprct;
	}
	public void setTcolprct(Object what) {
		setTcolprct(what, false);
	}
	public void setTcolprct(Object what, boolean rounded) {
		if (rounded)
			tcolprct.setRounded(what);
		else
			tcolprct.set(what);
	}	
	public PackedDecimalData getTcolmax() {
		return tcolmax;
	}
	public void setTcolmax(Object what) {
		setTcolmax(what, false);
	}
	public void setTcolmax(Object what, boolean rounded) {
		if (rounded)
			tcolmax.setRounded(what);
		else
			tcolmax.set(what);
	}	
	public FixedLengthStringData getTsalesunt() {
		return tsalesunt;
	}
	public void setTsalesunt(Object what) {
		tsalesunt.set(what);
	}	
	public FixedLengthStringData getPrdagent() {
		return prdagent;
	}
	public void setPrdagent(Object what) {
		prdagent.set(what);
	}	
	public FixedLengthStringData getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(Object what) {
		agccqind.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	
	//TMLII-281 AG-01-002 START
	public void setZrecruit(Object what) {
		zrecruit.set(what);
	}	
	public FixedLengthStringData getZrecruit() {
		return zrecruit;
	}
	//TMLII-281 AG-01-002 END

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		dteapp.clear();
		dtetrm.clear();
		trmcde.clear();
		dteexp.clear();
		dtepay.clear();
		currcode.clear();
		pftflg.clear();
		rasflg.clear();
		bcmtab.clear();
		rcmtab.clear();
		scmtab.clear();
		ocmtab.clear();
		agentClass.clear();
		reportag.clear();
		ovcpc.clear();
		taxmeth.clear();
		irdno.clear();
		taxcde.clear();
		taxalw.clear();
		sprschm.clear();
		sprprc.clear();
		payclt.clear();
		paymth.clear();
		payfrq.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		intcrd.clear();
		fixprc.clear();
		bmaflg.clear();
		exclAgmt.clear();
		houseLoan.clear();
		computerLoan.clear();
		carLoan.clear();
		officeRent.clear();
		otherLoans.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		aracde.clear();
		minsta.clear();
		zrorcode.clear();
		effdate.clear();
		tagsusind.clear();
		tlaglicno.clear();
		tlicexpdt.clear();
		tcolprct.clear();
		tcolmax.clear();
		tsalesunt.clear();
		prdagent.clear();
		agccqind.clear();
		//TMLII-281 AG-01-002
		zrecruit.clear();
		validflag.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}