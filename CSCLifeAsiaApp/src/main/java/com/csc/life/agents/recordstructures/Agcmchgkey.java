package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:59
 * Description:
 * Copybook name: AGCMCHGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmchgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmchgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmchgKey = new FixedLengthStringData(64).isAPartOf(agcmchgFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmchgChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmchgKey, 0);
  	public FixedLengthStringData agcmchgChdrnum = new FixedLengthStringData(8).isAPartOf(agcmchgKey, 1);
  	public FixedLengthStringData agcmchgAgntnum = new FixedLengthStringData(8).isAPartOf(agcmchgKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(agcmchgKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmchgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmchgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}