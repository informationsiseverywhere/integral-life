package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl27rec Date: 03 February 2020 Author: vdivisala
 */
public class Tjl27rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl27Rec = new FixedLengthStringData(4);
	public FixedLengthStringData cmgwcolcat = DD.cmgwcolcat.copy().isAPartOf(tjl27Rec, 0);
	public FixedLengthStringData cmgwcpdte = DD.cmgwcpdte.copy().isAPartOf(tjl27Rec, 1);
	public FixedLengthStringData cmgwcprot = DD.cmgwcprot.copy().isAPartOf(tjl27Rec, 3);

	public void initialize() {
		COBOLFunctions.initialize(tjl27Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl27Rec.isAPartOf(baseString, Boolean.TRUE);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}