package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl63screensfl extends Subfile {


	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl63ScreenVars sv = (Sjl63ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl63screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl63screensfl, 
			sv.Sjl63screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl63ScreenVars sv = (Sjl63ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl63screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl63ScreenVars sv = (Sjl63ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl63screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl63screensflWritten.gt(0))
		{
			sv.sjl63screensfl.setCurrentIndex(0);
			sv.Sjl63screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl63ScreenVars sv = (Sjl63ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl63screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl63ScreenVars screenVars = (Sjl63ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.aclntsel.setFieldName("aclntsel");
				screenVars.acltname.setFieldName("acltname");
				screenVars.agntnum.setFieldName("agntnum");				
				screenVars.gender.setFieldName("gender");				
				screenVars.dobDisp.setFieldName("dobDisp");
				screenVars.agnttyp.setFieldName("agnttyp");
				screenVars.liscno.setFieldName("liscno");
				screenVars.appdateDisp.setFieldName("appdateDisp");
				screenVars.termdateDisp.setFieldName("termdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.aclntsel.set(dm.getField("aclntsel"));
			screenVars.acltname.set(dm.getField("acltname"));
			screenVars.agntnum.set(dm.getField("agntnum"));
			screenVars.gender.set(dm.getField("gender"));				
			screenVars.dobDisp.set(dm.getField("dobDisp"));
			screenVars.agnttyp.set(dm.getField("agnttyp"));
			screenVars.liscno.set(dm.getField("liscno"));
			screenVars.appdateDisp.set(dm.getField("appdateDisp"));
			screenVars.termdateDisp.set(dm.getField("termdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl63ScreenVars screenVars = (Sjl63ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.aclntsel.setFieldName("aclntsel");
				screenVars.acltname.setFieldName("acltname");
				screenVars.agntnum.setFieldName("agntnum");				
				screenVars.gender.setFieldName("gender");				
				screenVars.dobDisp.setFieldName("dobDisp");
				screenVars.agnttyp.setFieldName("agnttyp");
				screenVars.liscno.setFieldName("liscno");
				screenVars.appdateDisp.setFieldName("appdateDisp");
				screenVars.termdateDisp.setFieldName("termdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("aclntsel").set(screenVars.aclntsel);
			dm.getField("acltname").set(screenVars.acltname);
			dm.getField("agntnum").set(screenVars.agntnum);
			dm.getField("gender").set(screenVars.gender);
			dm.getField("dobDisp").set(screenVars.dobDisp);
			dm.getField("agnttyp").set(screenVars.agnttyp);
			dm.getField("liscno").set(screenVars.liscno);
			dm.getField("appdateDisp").set(screenVars.appdateDisp);
			dm.getField("termdateDisp").set(screenVars.termdateDisp);
		}

	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl63screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl63ScreenVars screenVars = (Sjl63ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.aclntsel.clearFormatting();
		screenVars.acltname.clearFormatting();
		screenVars.agntnum.clearFormatting();
		screenVars.gender.clearFormatting();				
		screenVars.dobDisp.clearFormatting();
		screenVars.agnttyp.clearFormatting();
		screenVars.liscno.clearFormatting();
		screenVars.appdateDisp.clearFormatting();
		screenVars.termdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl63ScreenVars screenVars = (Sjl63ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.aclntsel.setClassString("");
		screenVars.acltname.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.gender.setClassString("");				
		screenVars.dobDisp.setClassString("");
		screenVars.agnttyp.setClassString("");
		screenVars.liscno.setClassString("");
		screenVars.appdateDisp.setClassString("");
		screenVars.termdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sjl63screensfl
 */
	public static void clear(VarModel pv) {
		Sjl63ScreenVars screenVars = (Sjl63ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.aclntsel.clear();
		screenVars.acltname.clear();
		screenVars.agntnum.clear();		
		screenVars.gender.clear();				
		screenVars.dobDisp.clear();
		screenVars.agnttyp.clear();
		screenVars.liscno.clear();
		screenVars.appdateDisp.clear();
		screenVars.termdateDisp.clear();	
	}
}