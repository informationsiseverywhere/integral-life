package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgcspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:41
 * Class transformed from AGCSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgcspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 90;
	public FixedLengthStringData agcsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agcspfRecord = agcsrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agcsrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agcsrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(agcsrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(agcsrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(agcsrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(agcsrec);
	public PackedDecimalData payamt = DD.payamt.copy().isAPartOf(agcsrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(agcsrec);
	public PackedDecimalData frcdate = DD.frcdate.copy().isAPartOf(agcsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agcsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agcsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agcsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgcspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgcspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgcspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgcspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgcspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGCSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"CURRCODE, " +
							"PAYCLT, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"PAYAMT, " +
							"EFFDATE, " +
							"FRCDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     currcode,
                                     payclt,
                                     sacscode,
                                     sacstyp,
                                     payamt,
                                     effdate,
                                     frcdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		currcode.clear();
  		payclt.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		payamt.clear();
  		effdate.clear();
  		frcdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgcsrec() {
  		return agcsrec;
	}

	public FixedLengthStringData getAgcspfRecord() {
  		return agcspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgcsrec(what);
	}

	public void setAgcsrec(Object what) {
  		this.agcsrec.set(what);
	}

	public void setAgcspfRecord(Object what) {
  		this.agcspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agcsrec.getLength());
		result.set(agcsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}