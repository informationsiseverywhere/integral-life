package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:32
 * Description:
 * Copybook name: MACFAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfagtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfagtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfagtKey = new FixedLengthStringData(64).isAPartOf(macfagtFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfagtAgntcoy = new FixedLengthStringData(1).isAPartOf(macfagtKey, 0);
  	public FixedLengthStringData macfagtAgntnum = new FixedLengthStringData(8).isAPartOf(macfagtKey, 1);
  	public PackedDecimalData macfagtEffdate = new PackedDecimalData(8, 0).isAPartOf(macfagtKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(macfagtKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfagtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfagtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}