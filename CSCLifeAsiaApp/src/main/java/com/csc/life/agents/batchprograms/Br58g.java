package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.financials.dataaccess.dao.model.Cheqpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ZldbTableDAM;
import com.csc.life.agents.dataaccess.ZldbbymTableDAM;
import com.csc.life.agents.dataaccess.dao.ZldbpfDAO;
import com.csc.life.agents.dataaccess.model.Zldbpf;
import com.csc.life.agents.tablestructures.Tr58qrec;
import com.csc.life.agents.tablestructures.Tr58rrec;
import com.csc.life.productdefinition.batchprograms.Br646;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Br58g extends Mainb{

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR58G");
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private SQLException sqlca = new SQLException();
	private boolean btchpr01Permission = false; //BTCHPR01
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	
	private String esql = "ESQL";
	
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
		
	private ZonedDecimalData wsaapymtpc  =new ZonedDecimalData(5,2);
	private PackedDecimalData wsaabonusamt =new PackedDecimalData(17,2);
	private PackedDecimalData wsaaretroamt =new PackedDecimalData(17,2);
	private PackedDecimalData wsaatempamt =new PackedDecimalData(17,2);
	private PackedDecimalData wsaabnpaid =new PackedDecimalData(18,2);
	
	private FixedLengthStringData wsaaTr58rArray = new FixedLengthStringData(45900);
	private FixedLengthStringData[] wsaaTr58rRec = FLSArrayPartOfStructure(90, 510, wsaaTr58rArray, 0);
	private FixedLengthStringData[] wsaaTr58rKey = FLSDArrayPartOfArrayStructure(2, wsaaTr58rRec, 0);
	private FixedLengthStringData[] wsaaTr58ragtype = FLSDArrayPartOfArrayStructure(2, wsaaTr58rKey, 0, SPACES); 
	private FixedLengthStringData[] wsaaTr58rData = FLSDArrayPartOfArrayStructure(508, wsaaTr58rRec, 2);	
	private PackedDecimalData[] wsaaTr58ritmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTr58rData, 0);
	private FixedLengthStringData[] wsaaTr58rgenarea = FLSDArrayPartOfArrayStructure(500, wsaaTr58rData, 8);
	private int wsaaTr58rSize = 90;
	private ZonedDecimalData wsaaTr58rIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	private FixedLengthStringData wsaaTr58qArray = new FixedLengthStringData(45900);
	private FixedLengthStringData[] wsaaTr58qRec = FLSArrayPartOfStructure(90, 510, wsaaTr58qArray, 0);
	private FixedLengthStringData[] wsaaTr58qKey = FLSDArrayPartOfArrayStructure(2, wsaaTr58qRec, 0);
	private FixedLengthStringData[] wsaaTr58qagtype = FLSDArrayPartOfArrayStructure(2, wsaaTr58qKey, 0, SPACES); 
	private FixedLengthStringData[] wsaaTr58qData = FLSDArrayPartOfArrayStructure(508, wsaaTr58qRec, 2);	
	private PackedDecimalData[] wsaaTr58qitmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaTr58qData, 0);
	private FixedLengthStringData[] wsaaTr58qgenarea = FLSDArrayPartOfArrayStructure(500, wsaaTr58qData, 8);
	private int wsaaTr58qSize = 90;
	
	private ZonedDecimalData wsaaTr58qIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	
	
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private ZldbbymTableDAM zldbbymIO = new ZldbbymTableDAM();
	private ZldbTableDAM zldbIO = new ZldbTableDAM();
	
	private FixedLengthStringData wsaaT5645Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5645Prog = new FixedLengthStringData(5).isAPartOf(wsaaT5645Item, 0);
	private FixedLengthStringData filler4 = new FixedLengthStringData(3).isAPartOf(wsaaT5645Item, 5, FILLER).init(SPACES);

	private String t5645 = "T5645";
	private String tr58r = "TR58R";
	private String t1688=  "T1688";
	private String tr58q=  "TR58Q";
	
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Tr58rrec tr58rrec = new Tr58rrec();
	private Tr58qrec tr58qrec = new Tr58qrec();
	
	private String zldbrec="ZLDBREC";
	private String zldbbymrec="ZLDBBYMREC";
	
	private int acmvjrnseq1=0;
	private int acmvjrnseq2=0;

	private FixedLengthStringData wsaaValidAgType = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidAgType, "Y");
	private ZldbpfDAO zldbDAO = getApplicationContext().getBean("zldbDAO", ZldbpfDAO.class);
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	private Map<String, List<Itempf>> tr58rListMap;
	private Map<String, List<Itempf>> tr58qListMap;
	private String strEffDate;
	
	
	
	private SQLException sqlcazldb = new SQLException();
	private java.sql.ResultSet sqlzldbpf1rs = null;
	private java.sql.PreparedStatement sqlzldbpf1ps = null;
	private java.sql.Connection sqlzldbpf1conn = null;
	private String sqlzldbpf1 = "";
	private Zldbpf zldbpfUpdate;	
	
	private List<Zldbpf> zldbpfList;
	private Zldbpf zldbpfDataSelect;
	
	int stmtcnt = 0;
	Zldbpf zldbpfretro;
	private List<Zldbpf> zldbpfListretro;
	private List<Zldbpf> zldbpfInsList;
	private List<Zldbpf> zldbpfUpdList;
	private boolean noRecordFound;
	private static final Logger LOGGER = LoggerFactory.getLogger(Br58g.class);		
	/*
	 * Batch program to read from ZLDBBYM logical view for current Batch accounting year month record. Calculate the Leader bonus and post to Subsidiary Ledger Account Movement (ACMVPF).
	 */
	
	
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	private void process()
	{
		super.mainline();
	}
	
	@Override
	protected void initialise1000() {
		
		wsspEdterror.set(varcom.oK);
		noRecordFound = false;
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		wsaaT5645Item.set(SPACES);
		wsaaT5645Prog.set(wsaaProg);
		itemIO.setItemitem(wsaaT5645Item);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)){
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());

		/*itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tr58r);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setFunction(varcom.begn);
		wsaaTr58rIx.set(1);
		
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),bprdIO.getCompany())
			|| isNE(itdmIO.getItemtabl(),tr58r)) {
				itdmIO.setStatuz(varcom.endp);	
			}
			else{
				tr58rrec.tr58rRec.set(itdmIO.getGenarea());
				wsaaTr58ragtype[wsaaTr58rIx.toInt()].set(itdmIO.getItemitem());
				wsaaTr58ritmfrm[wsaaTr58rIx.toInt()].set(itdmIO.getItmfrm());
				wsaaTr58rgenarea[wsaaTr58rIx.toInt()].set(tr58rrec.tr58rRec);
				itdmIO.setFunction(varcom.nextr);
				wsaaTr58rIx.add(1);
			}
			if (isGT(wsaaTr58rIx,wsaaTr58rSize)) {
				syserrrec.statuz.set("H791");
				syserrrec.params.set(tr58r);
				fatalError600();
			}
		}*/
		tr58rListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "TR58R");
		String keyItemitem = bsscIO.language.toString().trim().concat(bsprIO.getCompany().toString().trim());
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (tr58rListMap.containsKey(keyItemitem)){	
			itempfList = tr58rListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr58rrec.tr58rRec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					}
				}else{
					tr58rrec.tr58rRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(tr58rrec.tr58rRec);
			syserrrec.statuz.set("H791");
			fatalError600();		
		}
	/*	itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tr58q);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setFunction(varcom.begn);
		wsaaTr58qIx.set(1);
		
		while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				fatalError600();
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)
			|| isNE(itdmIO.getItemcoy(),bprdIO.getCompany())
			|| isNE(itdmIO.getItemtabl(),tr58q)) {		
				itdmIO.setStatuz(varcom.endp);
			}
			else{
				tr58qrec.tr58qRec.set(itdmIO.getGenarea());
				wsaaTr58qagtype[wsaaTr58qIx.toInt()].set(itdmIO.getItemitem());
				wsaaTr58qitmfrm[wsaaTr58qIx.toInt()].set(itdmIO.getItmfrm());
				wsaaTr58qgenarea[wsaaTr58qIx.toInt()].set(tr58qrec.tr58qRec);
				itdmIO.setFunction(varcom.nextr);
				wsaaTr58qIx.add(1);
			}
			if (isGT(wsaaTr58qIx,wsaaTr58qSize)) {
				syserrrec.statuz.set("H791");
				syserrrec.params.set(tr58q);
				fatalError600();
			}
		}		*/
		tr58qListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), "TR58Q");
		keyItemitem = bsscIO.language.toString().trim().concat(bsprIO.getCompany().toString().trim());
		itempfList = new ArrayList<Itempf>();
		 itemFound = false;
		if (tr58qListMap.containsKey(keyItemitem)){	
			itempfList = tr58qListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					if((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()) )
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())){
						tr58qrec.tr58qRec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					}
				}else{
					tr58qrec.tr58qRec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		if (!itemFound) {
			syserrrec.params.set(tr58qrec.tr58qRec);
			syserrrec.statuz.set("H791");
			fatalError600();		
		}
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);

		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1688);
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.postyear.set(SPACE);
		lifacmvrec.postmonth.set(SPACE);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(SPACE);
		lifacmvrec.suprflag.set(SPACE);
		lifacmvrec.genlcoy.set(bsprIO.getCompany());
		lifacmvrec.genlcur.set(SPACE);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.threadNumber.set(ZERO);
		lifacmvrec.substituteCode[06].set(SPACES);
		stmtcnt = 0;
		zldbpfInsList = new ArrayList<Zldbpf>();
		zldbpfUpdList = new ArrayList<Zldbpf>();
		zldbpfList = zldbDAO.getZldbpfList(bsscIO.getAcctYear().toInt(),bsscIO.getAcctMonth().toInt(),"","",1);
		
		
	}
	
	protected void readFile2000()
	{
					readFile2010();
					endOfFile2080();
		
	}
	protected void readFile2010()
	{
		
		
		
			
			if(zldbpfList==null || (zldbpfList!=null && zldbpfList.size()==0))
			{
				endOfFile2080();
				noRecordFound = true;
			}	
			if (zldbpfList.size() > 0 && stmtcnt < zldbpfList.size()) {
				
					
					zldbpfUpdate = new Zldbpf();
					zldbpfUpdate.setAgntcoy(zldbpfList.get(stmtcnt).getAgntcoy());
					zldbpfUpdate.setAgntnum(zldbpfList.get(stmtcnt).getAgntnum());
					zldbpfUpdate.setBatcactmn(zldbpfList.get(stmtcnt).getBatcactmn());
					zldbpfUpdate.setBatcactyr(zldbpfList.get(stmtcnt).getBatcactyr());
					zldbpfUpdate.setBlprem(zldbpfList.get(stmtcnt).getBlprem());
					zldbpfUpdate.setBpayty(zldbpfList.get(stmtcnt).getBpayty());
					zldbpfUpdate.setPrmamt(zldbpfList.get(stmtcnt).getPrmamt());
					zldbpfUpdate.setPremst(zldbpfList.get(stmtcnt).getPremst());
					zldbpfUpdate.setNetpre(zldbpfList.get(stmtcnt).getNetpre());
					zldbpfUpdate.setProcflg(zldbpfList.get(stmtcnt).getProcflg());
				
			}
			
			endOfFile2080();
	}
	

	protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}
	
	@Override
	protected void edit2500() {
		stmtcnt++;
		wsspEdterror.set(varcom.oK);		
	}
	
	protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}
	protected void update3010()
	{	
		wsaabonusamt.set(ZERO);
		wsaaretroamt.set(ZERO);
		wsaatempamt.set(add(zldbpfUpdate.getNetpre(),zldbpfUpdate.getPrmamt()).toString());
		if(isLT(zldbpfUpdate.getNetpre(),0)){
			if(isGTE(wsaatempamt, zldbpfUpdate.getBlprem())){
				retroCalc3100();
				wsaabonusamt.set(add(zldbpfUpdate.getBpayty(),wsaaretroamt).toString());
			}
			else 
			{
				if(isGTE(zldbpfUpdate.getPrmamt(),zldbpfUpdate.getBlprem())){
					wsaabonusamt.set(zldbpfUpdate.getBpayty());
				}
			   else 
			   {
				   if(isLTE(zldbpfUpdate.getPrmamt(),zldbpfUpdate.getPremst())){
					  wsaabonusamt.set(ZERO);
				   }
				   else
				    {
					   if(isGT(zldbpfUpdate.getPrmamt(),ZERO)){
						   //wsaabonusamt.setRounded(mult(div(sub(zldbbymIO.getPrmamt(),zldbbymIO.getPremst()),sub(zldbbymIO.getBlprem(),zldbbymIO.getPremst())), zldbbymIO.getBpayty()).toString());	
						   double achieve_Collected = Double.parseDouble(sub(zldbpfUpdate.getPrmamt(),zldbpfUpdate.getPremst()).toString().trim());
						   double target_Collected = Double.parseDouble(sub(zldbpfUpdate.getBlprem(),zldbpfUpdate.getPremst()).toString().trim());
						   double leader_Bonus = Double.parseDouble(zldbpfUpdate.getBpayty().toString().trim());
						   wsaabonusamt.setRounded((achieve_Collected/target_Collected)*leader_Bonus);
					   }
					   else{
						   wsaabonusamt.set(ZERO);
					   }
				    }
			   }
			}
		}
		else{
			if(isGTE(wsaatempamt,zldbpfUpdate.getBlprem())){ //Blprem: Target Collected per month
				wsaabonusamt.set(zldbpfUpdate.getBpayty());  //Bpayty: Leader Bonus per month
			}
			else{
				 if(isLTE(wsaatempamt,zldbpfUpdate.getPremst())){ //Premst: Minimum Target Collected FYP 
				 	 wsaabonusamt.set(ZERO);
				 }
			     else {
				    if(isGT(wsaatempamt,ZERO)){
					     //wsaabonusamt.setRounded(mult(div(sub(wsaatempamt,zldbbymIO.getPremst()),sub(zldbbymIO.getBlprem(),zldbbymIO.getPremst()) ), zldbbymIO.getBpayty()).toString());	
				    	double achieve_Collected  = Double.parseDouble(sub(wsaatempamt,zldbpfUpdate.getPremst()).toString().trim());
				    	double target_Collected   = Double.parseDouble(sub(zldbpfUpdate.getBlprem(),zldbpfUpdate.getPremst()).toString().trim());
				    	double leader_Bonus = Double.parseDouble(zldbpfUpdate.getBpayty().toString().trim());
					    wsaabonusamt.setRounded((achieve_Collected/target_Collected)*leader_Bonus);	
				    }
				    else{
						   wsaabonusamt.set(ZERO);
					   }
				 }
			}
		}
		wsaaValidAgType.set("N");
		searchlabel1:{
			for (wsaaTr58rIx.set(1); !(isGT(wsaaTr58rIx,wsaaTr58rSize)); wsaaTr58rIx.add(1)){
				if (isEQ(wsaaTr58rKey[wsaaTr58rIx.toInt()],zldbpfUpdate.getAgtype())) {
					if (isGTE(bsscIO.getEffectiveDate(),wsaaTr58ritmfrm[wsaaTr58rIx.toInt()])) {
						tr58rrec.tr58rRec.set(wsaaTr58rgenarea[wsaaTr58rIx.toInt()]);
						wsaaValidAgType.set("Y");
						break searchlabel1;
					}
				}
			}
		}
		if (!validContract.isTrue()) {
			syserrrec.params.set(tr58r);  //indicate Tr58r Item not found
			fatalError600(); 
		}
		searchlabel1:{
			for (wsaaTr58qIx.set(1); !(isGT(wsaaTr58qIx,wsaaTr58qSize)); wsaaTr58qIx.add(1)){
				if (isEQ(wsaaTr58qKey[wsaaTr58qIx.toInt()],zldbpfUpdate.getAgtype())) {
					if (isGTE(bsscIO.getEffectiveDate(),wsaaTr58qitmfrm[wsaaTr58qIx.toInt()])) {
						tr58qrec.tr58qRec.set(wsaaTr58qgenarea[wsaaTr58qIx.toInt()]);
						wsaaValidAgType.set("Y");
						break searchlabel1;
					}
				}
			}
		}
		if (!validContract.isTrue()) {
			syserrrec.params.set(tr58q); //indicate Tr58q Item not found
			fatalError600();  
		}
		if(isGTE(zldbpfUpdate.getPrcnt(),tr58rrec.minpcnt)){  //Prcnt is calculate on the basis of zagpIO
			wsaapymtpc.set(tr58qrec.znadjperc01); // znadjperc01     >= PR criteria payment 100%
		}
		else{
			wsaapymtpc.set(tr58qrec.znadjperc02); // znadjperc02     < PR criteria payment 0%
		}
		wsaabnpaid.setRounded(div(mult(wsaabonusamt,wsaapymtpc),100).toString());
		
		wsaabonusamt.setRounded(div(mult(wsaabonusamt,wsaapymtpc),100).toString());
	
		//zldbbymIO.setFunction(varcom.readh);
		//zldbbymIO.setFormat(zldbbymrec);
		zldbpfDataSelect = zldbDAO.readZldbpfData(bsscIO.getAcctYear().toInt(),bsscIO.getAcctMonth().toInt(),"","",1);
	   
		//SmartFileCode.execute(appVars, zldbbymIO);
		zldbpfUpdate.setNetpre(zldbpfDataSelect.getNetpre().add(zldbpfDataSelect.getPrmamt()));
		zldbpfUpdate.setNetpre(zldbpfDataSelect.getNetpre().subtract(zldbpfDataSelect.getBlprem()));
		//zldbpfUpdate.setNetpre(((zldbpfDataSelect.getNetpre().add(zldbpfDataSelect.getPrmamt()).subtract(zldbpfDataSelect.getBlprem().toBigInteger()))));
		zldbpfUpdate.setBonusamt(wsaabonusamt.getbigdata());
		zldbpfUpdate.setBnpaid(wsaabnpaid.getbigdata());
		if(zldbpfDataSelect==null || (zldbpfDataSelect!=null && zldbpfDataSelect.toString().length()==0))
		{
			zldbpfInsList.add(zldbpfUpdate);//zldbDAO.insertZldbPFRecord(zldbpfUpdate);
		}
		
		else{			
			
			zldbpfUpdList.add(zldbpfUpdate);//zldbDAO.updateZldbPF(zldbpfUpdate);
		}
		
			//SmartFileCode.execute(appVars, zldbbymIO);
		//	if (isNE(zldbbymIO.getStatuz(),varcom.oK)) {
			//	syserrrec.params.set(zldbbymIO.getParams());
			//	syserrrec.statuz.set(zldbbymIO.getStatuz());
			//	fatalError600();
			//}
			if(isNE(wsaabnpaid,ZERO)){				
				postAcmv3200();
			}
		
		
		//zldbbymIO.setFunction(varcom.nextr);
		
		
	}
	protected void  retroCalc3100(){
		
		try {	
			retroCalc3110();
		}
		catch (GOTOException e){
			
		}
	}
	
	protected void  retroCalc3110(){
		
		if(isEQ(bsscIO.getAcctMonth(),"1")){        
			return;
		}		
		zldbpfretro = null;//IJTI-320
		
		
		
		zldbpfListretro = zldbDAO.getZldbpfList(zldbpfUpdate.getBatcactyr(),1,zldbpfUpdate.getAgntcoy(),zldbpfUpdate.getAgntnum(),0);
		
		for(Zldbpf zldbpfCalc:zldbpfListretro) {		
			
			zldbpfretro = new Zldbpf();//IJTI-320
			zldbpfretro.setAgntcoy(zldbpfCalc.getAgntcoy());
			zldbpfretro.setAgntnum(zldbpfCalc.getAgntnum());
			zldbpfretro.setBatcactmn(zldbpfCalc.getBatcactmn());
			zldbpfretro.setBatcactyr(zldbpfCalc.getBatcactyr());
			zldbpfretro.setBlprem(zldbpfCalc.getBlprem());
			zldbpfretro.setBpayty(zldbpfCalc.getBpayty());
			zldbpfretro.setPrmamt(zldbpfCalc.getPrmamt());
			zldbpfretro.setPremst(zldbpfCalc.getPremst());
			zldbpfretro.setNetpre(zldbpfCalc.getNetpre());
			zldbpfretro.setProcflg(zldbpfCalc.getProcflg());
		
	
			
			if (isEQ(zldbpfretro.getBatcactmn(),zldbpfUpdate.getBatcactmn())) {
				break;
			}
			if(isLT(zldbpfretro.getBatcactmn(),zldbpfUpdate.getBatcactmn())
			   && isGT(zldbpfretro.getBlprem(),zldbpfretro.getPremst()) //Premst: Minimum Target Collected FYP 
			   && isGT(zldbpfretro.getBpayty(),zldbpfretro.getBonusamt())
			   && isLT(zldbpfretro.getNetpre(),0)
			   && isEQ(zldbpfretro.getProcflg(),SPACE)){
				wsaaretroamt.set(add(wsaaretroamt,sub(zldbpfretro.getBpayty(),zldbpfretro.getBonusamt())).toString()); 
				
			//	zldbIO.setFunction(varcom.readh);
				//zldbIO.setFormat(zldbrec);
				
				zldbpfretro = zldbDAO.readZldbpfData(bsscIO.getAcctYear().toInt(),bsscIO.getAcctMonth().toInt(),"","",1);
				
			//	SmartFileCode.execute(appVars, zldbIO);
				if(zldbpfretro == null)
				{
					zldbpfInsList.add(zldbpfretro);
				}
				else{
					zldbpfretro.setProcflg("Y");
					//zldbIO.setFunction(varcom.rewrt);
					//zldbIO.setFormat(zldbrec);
					//SmartFileCode.execute(appVars, zldbIO);
					//if (isNE(zldbIO.getStatuz(),varcom.oK)) {
						//syserrrec.params.set(zldbIO.getParams());
					//	fatalError600();
					//}
					zldbpfUpdList.add(zldbpfUpdate);
				 }
			}	
			//zldbIO.setFunction(varcom.nextr);
		}
		
	}
	protected void postAcmv3200()
	{	
		lifacmvrec.rdocnum.set(zldbpfDataSelect.getAgntnum());
		lifacmvrec.jrnseq.set(acmvjrnseq1++);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(zldbpfDataSelect.getGenlcur());
		lifacmvrec.origamt.set(wsaabnpaid);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rldgcoy.set(zldbpfDataSelect.getAgntcoy());
		lifacmvrec.rldgacct.set(zldbpfDataSelect.getAgntnum());
		lifacmvrec.tranref.set(SPACE);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
		lifacmvrec.ind.set("D");
			lifacmvrec.prefix.set("AG");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError600();
		}
		lifacmvrec.jrnseq.set(acmvjrnseq2++);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.origcurr.set(zldbpfDataSelect.getGenlcur());
		lifacmvrec.origamt.set(wsaabnpaid);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
		lifacmvrec.ind.set("D");
			lifacmvrec.prefix.set("AG");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
	}

	protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/	
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
	protected void close4000() {
		lsaaStatuz.set(varcom.oK);
		
		zldbpfInsList.clear();
		zldbpfInsList = null;	
		zldbpfUpdList.clear();
		zldbpfUpdList = null;
	}
	protected void commit3500() {	
		if (!noRecordFound){
			commitZldbpfBulkInsert();
			commitZldbpfBulkUpdate();
		}
	}
	
	protected void commitZldbpfBulkInsert(){	 
		 boolean isInsertCheqPF = zldbDAO.insertZldbPFRecord(zldbpfInsList);	
		if (!isInsertCheqPF) {
			LOGGER.error("Insert ZldbPF record failed.");
			fatalError600();
		}else zldbpfInsList.clear();
	}
	
	protected void commitZldbpfBulkUpdate(){	 
		 boolean isInsertCheqPF = zldbDAO.updateZldbPF(zldbpfUpdList);	
		if (!isInsertCheqPF) {
			LOGGER.error("Update ZldbPF record failed.");
			fatalError600();
		}else zldbpfUpdList.clear();
	}
	
	
	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}
	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}
	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}
	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}
	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}
	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}
	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}
	protected void restart0900() {	
	}
	protected void rollback3600() {	
	}
	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}
	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}
	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}
	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}
	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

}
