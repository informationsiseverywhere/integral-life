package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN Date: 03 February 2020 Author: vdivisala
 */
@SuppressWarnings("unchecked")
public class Sjl27screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 22, 3, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sjl27ScreenVars sv = (Sjl27ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl27screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sjl27ScreenVars screenVars = (Sjl27ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.cmgwcolcat.setClassString("");
		screenVars.cmgwcpdte.setClassString("");
		screenVars.cmgwcprot.setClassString("");
	}

	/**
	 * Clear all the variables in Sjl27screen
	 */
	public static void clear(VarModel pv) {
		Sjl27ScreenVars screenVars = (Sjl27ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.cmgwcolcat.clear();
		screenVars.cmgwcpdte.clear();
		screenVars.cmgwcprot.clear();
	}
}