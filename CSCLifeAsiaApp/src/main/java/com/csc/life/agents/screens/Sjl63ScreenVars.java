package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl63protect;
import com.csc.life.agents.screens.Sjl63screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;


public class Sjl63ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(729);
	public FixedLengthStringData dataFields = new FixedLengthStringData(361).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields, 198);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(dataFields, 199);
	public FixedLengthStringData agncysel = DD.agncysel.copy().isAPartOf(dataFields,219);
	public FixedLengthStringData agncydesc = DD.cltname.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData regnum = DD.agncyRegnum.copy().isAPartOf(dataFields,274);
	public ZonedDecimalData srdate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,287);
	public ZonedDecimalData endate = DD.enddate.copyToZonedDecimal().isAPartOf(dataFields,295);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(dataFields,303);
	public FixedLengthStringData agclntsel = DD.clntsel.copy().isAPartOf(dataFields,304);
	public FixedLengthStringData agcltname = DD.cltname.copy().isAPartOf(dataFields,314);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(92).isAPartOf(dataArea, 361);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData agncyselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData agncydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData endateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData agclntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData agcltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(276).isAPartOf(dataArea, 453); 
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] agncyselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] agncydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] endateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] agclntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] agcltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(273);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(110).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData aclntsel = DD.clntsel.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData acltname = DD.cltname.copy().isAPartOf(subfileFields,11); 
	public FixedLengthStringData agntnum = DD.agntsel.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData gender = DD.cltsex.copy().isAPartOf(subfileFields,68);
	public ZonedDecimalData dob = DD.cltdob.copyToZonedDecimal().isAPartOf(subfileFields,69);
	public FixedLengthStringData agnttyp = DD.agtype.copy().isAPartOf(subfileFields,77);
	public FixedLengthStringData liscno = DD.tlaglicno.copy().isAPartOf(subfileFields,79);
	public ZonedDecimalData appdate = DD.dteapp.copyToZonedDecimal().isAPartOf(subfileFields,94);
	public ZonedDecimalData termdate = DD.dtetrm.copyToZonedDecimal().isAPartOf(subfileFields,102);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(40).isAPartOf(subfileArea, 110);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData aclntselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData acltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData genderErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData agnttypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData liscnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData appdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData termdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(120).isAPartOf(subfileArea, 150);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] aclntselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] acltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] genderOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] agnttypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] liscnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] appdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] termdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 270);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData endateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData srdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);
	public FixedLengthStringData appdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData termdateDisp = new FixedLengthStringData(10);
	
	public FixedLengthStringData action1 = new FixedLengthStringData(1); 

	
	public LongData Sjl63screensflWritten = new LongData(0);
	public LongData Sjl63screenctlWritten = new LongData(0);
	public LongData Sjl63screenWritten = new LongData(0);
	public LongData Sjl63protectWritten = new LongData(0);
	public GeneralTable sjl63screensfl = new GeneralTable(AppVars.getInstance());
	
	public boolean hasSubfile() {
		return false;
	}

	public Sjl63ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		
		fieldIndMap.put(clntselOut,
				new String[] { "01", "02" , "-01", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "03", "04" , "-03", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "05", "06" , "-05", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "07", "08" , "-07", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "09", "10" , "-09", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "11", "12" , "-11", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "13", "14" , "-13", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "15", "16" , "-15", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypeOut,
				new String[] { "17", "18" , "-17", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldesOut,
				new String[] { "19", "20" , "-19", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "21", "22" , "-21", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "23", "24" , "-23", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "25", "26" , "-25", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(actionOut,
				new String[] { "29", "30" , "-29", null , null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] { clntsel, cltname, company, brnchcd, brnchdesc, aracde, aradesc, levelno, leveltype, leveldes, saledept, saledptdes, agtype,action,userid,agncysel,agncydesc,regnum,srdate,endate,validflag,agclntsel,agcltname};
		screenOutFields = new BaseData[][] { clntselOut, cltnameOut, companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, levelnoOut, leveltypeOut, leveldesOut, 
											 saledeptOut, saledptdesOut,agtypeOut,actionOut, useridOut,agncyselOut,agncydescOut,regnumOut,srdateOut,endateOut,validflagOut,agclntselOut,agcltnameOut};
		screenErrFields = new BaseData[] { clntselErr, cltnameErr, companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, levelnoErr, leveltypeErr, leveldesErr, 
											 saledeptErr, saledptdesErr,agtypeErr,actionErr, useridErr,agncyselErr,agncydescErr,regnumErr,srdateErr,endateErr,validflagErr,agclntselErr,agcltnameErr};
						

		screenSflFields = new BaseData[] {slt,aclntsel, acltname, agntnum, gender, dob, agnttyp, liscno, appdate, termdate};
		screenSflOutFields = new BaseData[][] {sltOut, aclntselOut, acltnameOut, agntnumOut, genderOut, dobOut, agnttypOut, liscnoOut, appdateOut, termdateOut };
		screenSflErrFields = new BaseData[] {sltErr, aclntselErr, acltnameErr, agntnumErr, genderErr, dobErr, agnttypErr, liscnoErr, appdateErr, termdateErr };
		
		screenDateFields = new BaseData[] {};
		screenSflDateFields = new BaseData[] {dob, appdate, termdate,srdate,endate};
		screenSflDateErrFields = new BaseData[] {dobErr, appdateErr, termdateErr,srdateErr,endateErr};
		screenSflDateDispFields = new BaseData[] {dobDisp, appdateDisp, termdateDisp,srdateDisp,endateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorSflInds = errorSubfile;
		screenSflRecord = Sjl63screensfl.class;
		screenCtlRecord = Sjl63screenctl.class;
		initialiseSubfileArea();
		screenRecord = Sjl63screen.class;
		protectRecord = Sjl63protect.class;
		}
		
		public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl63screenctl.lrec.pageSubfile);
		}
}
