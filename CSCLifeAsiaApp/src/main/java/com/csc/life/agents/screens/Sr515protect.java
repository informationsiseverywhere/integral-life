package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for PROTECT
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr515protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr515ScreenVars sv = (Sr515ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr515protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr515ScreenVars screenVars = (Sr515ScreenVars)pv;
	}

/**
 * Clear all the variables in Sr515protect
 */
	public static void clear(VarModel pv) {
		Sr515ScreenVars screenVars = (Sr515ScreenVars) pv;
	}
}
