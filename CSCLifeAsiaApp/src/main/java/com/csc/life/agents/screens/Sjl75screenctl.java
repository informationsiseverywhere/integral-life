package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl75screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl75screensfl";
		lrec.subfileClass = Sjl75screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 21;
		lrec.pageSubfile = 11;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 6, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl75ScreenVars sv = (Sjl75ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl75screenctlWritten, sv.Sjl75screensflWritten, av, sv.Sjl75screensfl, ind2, ind3, pv);
	}
	
	
	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl75ScreenVars screenVars = (Sjl75ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.company.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.agbrdesc.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.levelno.setClassString("");
		screenVars.leveltyp.setClassString("");
		screenVars.leveldesc.setClassString("");
		screenVars.saledept.setClassString("");
		screenVars.saledptdes.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.indxflg.setClassString("");
	}

/**
 * Clear all the variables in Sjl75screenctl
 */
	public static void clear(VarModel pv) {
		Sjl75ScreenVars screenVars = (Sjl75ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.clntsel.clear();
		screenVars.company.clear();
		screenVars.agntbr.clear();
		screenVars.agbrdesc.clear();
		screenVars.aracde.clear();
		screenVars.aradesc.clear();
		screenVars.levelno.clear();
		screenVars.leveltyp.clear();
		screenVars.leveldesc.clear();
		screenVars.saledept.clear();
		screenVars.saledptdes.clear();
		screenVars.clntname.clear();
		screenVars.indxflg.clear();
	}
}
