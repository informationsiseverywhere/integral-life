/*
 * File: Zrfbpf.java
 * Date: July 20, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
public class Zrfbpf implements Serializable {
	private long uniqueNumber;
	private Character agntcoy;
	private String zrecruit;
	private String agntnum;
	private BigDecimal batcactyr;
	private BigDecimal batcactmn;
	private String agtype;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private Integer effdate;
	private Integer tranno;
	private BigDecimal acctamt;
	private String origcurr;
	private BigDecimal bonusamt;
	private BigDecimal prcdate;
	private BigDecimal jobno;	
	private String userProfile;
	private String jobName;
	private Timestamp datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Character getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Character character) {
		this.agntcoy = character;
	}
	public String getZrecruit() {
		return zrecruit;
	}
	public void setZrecruit(String zrecruit) {
		this.zrecruit = zrecruit;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public BigDecimal getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(BigDecimal batcactyr) {
		this.batcactyr = batcactyr;
	}
	public BigDecimal getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(BigDecimal batcactmn) {
		this.batcactmn = batcactmn;
	}
	public String getAgtype() {
		return agtype;
	}
	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getEffdate() {
		return effdate;
	}
	public void setEffdate(Integer effdate) {
		this.effdate = effdate;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public BigDecimal getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getBonusamt() {
		return bonusamt;
	}
	public void setBonusamt(BigDecimal bonusamt) {
		this.bonusamt = bonusamt;
	}
	public BigDecimal getPrcdate() {
		return prcdate;
	}
	public void setPrcdate(BigDecimal prcdate) {
		this.prcdate = prcdate;
	}
	public BigDecimal getJobno() {
		return jobno;
	}
	public void setJobno(BigDecimal jobno) {
		this.jobno = jobno;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	

		
}
