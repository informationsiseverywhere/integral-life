/*
 * File: P5037.java
 * Date: 29 August 2009 23:59:07
 * Author: Quipoz Limited
 *
 * Class transformed from P5037.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.S5037ScreenVars;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                   AGENT BANK ACCOUNT DETAILS.
*                   ===========================
*
* Initialise
* ----------
*
*  Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*  information. Load the screen from the record read.  Store the
*  current  payee  (or if blank the client), the agency currency
*  and factoring house in hidden screen fields.
*
*  If the current bank number  and account number are not blank,
*  look up the client  bank  account details (CLBL). Cross check
*  that the client for the bank details found is the same as the
*  "hidden" (see above) payee. If  they  are not the same, blank
*  out  the current  factoring  house,  bank/branch  number  and
*  account number. (In this case,  the payee has changed, so the
*  old bank account details cannot be applicable.)
*
*  Otherwise (details not blank  and  are  for  correct client),
*  look up the description of the bank/branch (BABR).
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  is  "I")  protect the screen
*  prior to output.
*
*  If the screen  is  "KILLed" or when in enquiry mode, skip the
*  validation.
*
*  Validate the individual fields according to the rules defined
*  in the screen and field help.
*
*  Cross check the account details  read  (as  part of the above
*  validation). The client  number  from  these  account details
*  must  be the same as  the  "hidden"  payee.  If  the  account
*  details are for a  single currency (currency not blank), this
*  currency must be the same as the agency currency (NB - do not
*  check   against   the  "hidden"  currency  as  this  can  get
*  overwritten be the account number window).
*
*  If "CALC" was pressed,  re-display  the screen to display the
*  bank/branch and account descriptions.
*
* Updating
* --------
*
*  Skip the updating  if  "KILL"  was  pressed  or if in enquiry
*  mode.
*
*  Store the following in the agency header (AGLF):
*            Bank/branch code,
*            Account number,
*            Factoring house.
*
* Next Program
* ------------
*
*  Add one to the pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5037 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5037");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaBankkey = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaBankdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 0);
	private FixedLengthStringData wsaaBranchdesc = new FixedLengthStringData(30).isAPartOf(wsaaBankkey, 30);
		/* ERRORS */
	private String e182 = "E182";
	private String e695 = "E695";
	private String f906 = "F906";
	private String g599 = "G599";
	private String g900 = "G900";
	private String t745 = "T745";
		/* FORMATS */
	private String aglfrec = "AGLFREC";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Logical File: Client/Bank Account Record*/
	private ClblTableDAM clblIO = new ClblTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5037ScreenVars sv = ScreenProgram.getScreenVars( S5037ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		bankBranchDesc1120,
		exit1190,
		preExit,
		exit2090,
		exit3090
	}

	public P5037() {
		super();
		screenVars = sv;
		new ScreenModel("S5037", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		readAgntlag1030();
		loadHiddenFields1040();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		/*RETRV-AGLF*/
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void readAgntlag1030()
	{
		agntlagIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

protected void loadHiddenFields1040()
	{
		if (isEQ(aglfIO.getPayclt(),SPACES)) {
			sv.payrnum.set(agntlagIO.getClntnum());
			sv.numsel.set(agntlagIO.getClntnum());
		}
		else {
			sv.payrnum.set(aglfIO.getPayclt());
			sv.numsel.set(aglfIO.getPayclt());
		}
		if (isNE(aglfIO.getCurrcode(),SPACES)) {
			sv.currcode.set(aglfIO.getCurrcode());
		}
		sv.facthous.set(aglfIO.getFacthous());
		/*CHECK-FOR-BANK-DETS*/
		if (isNE(aglfIO.getBankkey(),SPACES)
		&& isNE(aglfIO.getBankacckey(),SPACES)) {
			sv.bankkey.set(aglfIO.getBankkey());
			sv.bankacckey.set(aglfIO.getBankacckey());
			bankDetails1100();
		}
		/*EXIT*/
	}

protected void bankDetails1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readAccountDets1110();
				}
				case bankBranchDesc1120: {
					bankBranchDesc1120();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAccountDets1110()
	{
		if (isEQ(sv.bankkey,SPACES)) {
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(sv.bankacckey,SPACES)) {
			goTo(GotoLabel.bankBranchDesc1120);
		}
		clblIO.setDataKey(SPACES);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(),varcom.oK)
		&& isNE(clblIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			fatalError600();
		}
		if (isEQ(clblIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(g599);
			sv.bankacckeyErr.set(g599);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			aglfIO.setBankkey(SPACES);
			aglfIO.setBankacckey(SPACES);
			aglfIO.setFacthous(SPACES);
			goTo(GotoLabel.exit1190);
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		if (isEQ(aglfIO.getCurrcode(),SPACES)) {
			aglfIO.setCurrcode(clblIO.getCurrcode());
			sv.currcode.set(clblIO.getCurrcode());
		}
	}

protected void bankBranchDesc1120()
	{
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isEQ(babrIO.getStatuz(),varcom.mrnf)) {
			sv.bankkeyErr.set(f906);
		}
		if (isNE(babrIO.getStatuz(),varcom.oK)
		&& isNE(babrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		wsaaBankkey.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsaaBankdesc);
		sv.branchdesc.set(wsaaBranchdesc);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			scrnparams.function.set(varcom.prot);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateFields2030();
			redisplayOnCalc2040();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateFields2030()
	{
		bankDetails1100();
		if (isEQ(sv.bankkey,SPACES)
		&& isEQ(sv.bankacckey,SPACES)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.bankkey,SPACES)) {
			sv.bankkeyErr.set(g900);
		}
		if (isEQ(sv.bankacckey,SPACES)) {
			sv.bankacckeyErr.set(e182);
		}
		if (isNE(clblIO.getCurrcode(),SPACES)
		&& isNE(clblIO.getCurrcode(),aglfIO.getCurrcode())) {
			sv.bankkeyErr.set(e695);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set(t745);
		}
	}

protected void redisplayOnCalc2040()
	{
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			keepsAglf3020();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")
		|| isEQ(scrnparams.statuz,"KILL")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void keepsAglf3020()
	{
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setBankkey(sv.bankkey);
		aglfIO.setBankacckey(sv.bankacckey);
		aglfIO.setFacthous(sv.facthous);
		aglfIO.setFunction("KEEPS");
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
