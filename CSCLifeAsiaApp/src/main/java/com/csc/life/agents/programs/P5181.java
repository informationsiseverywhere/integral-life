/*
 * File: P5181.java
 * Date: December 3, 2013 2:56:30 AM ICT
 * Author: CSC
 *
 * Class transformed from P5181.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.life.agents.screens.S5181ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.LifemjaTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.dataaccess.IjnlTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.VprnudlTableDAM;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5551rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*                    INTEREST JOURNALS.
*                    ==================
*
*Interest Journals will enable the input of information to
*change the fund amount for a contract's fund and type.
*
*The Journals will create ITRN records  which will be processed
*through the Batch Schedule run, to add and subtract amounts as
*appropriate within a component's funds.
*
*Initialise
*----------
*
*  - Clear the subfile ready for loading.
*
*  - The details of the  contract  being  enquired  upon will be
*  stored in the CHDRMJA  I/O  module.  Retrieve the details and
*  set up the header portion of the screen.
*
*  Look up the following descriptions and names:
*
*       Contract Type, (CNTTYPE) - long description from T5688,
*
*       Contract  Status,  (STATCODE)  -  short description from
*       T3623,
*
*       Premium  Status,  (PSTATCODE)  -  short description from
*       T3588,
*
*       The owner's client (CLTS) details.
*
*       The joint  owner's  client (CLTS) details if they exist.
*
*  - The details of the  component  to  which  the  journal will
*  relate  are  stored  in  the  COVR  I/O module.  Retrieve the
*  details and complete the header portion of the screen.
*
*  - Load the subfile as follows:
*
*       Load any existing Interest Journal up to a maximum of one
*       full page. For each record, add to the subfile and set a
*       flag in a 'hidden' field to indicate that the record was
*       loaded from the database (required during update).
*
*       If the first page is  not  full, add the number of blank
*       records  required  to complete  a  full  page.  Set  the
*       subfile more indicator to  'Y'.  Set  a  flag to signify
*       first page.
*
*
*Validation
*----------
*
*  If  in  enquiry  mode   (WSSP-FLAG   =   'J')  move  PROT  to
*  SCRN-FUNCTION prior to output.
*
*  After screen display;
*
*  - If SCRN-STATUZ = KILL  or  in enquiry  mode,  exit  to 3000
*       section.
*
*  - If SCRN-STATUZ =  CALC  redisplay  the  screen, display the
*       summary details for each subfile record.
*
*  - If SCRN-STATUZ = ROLD  and  First  page  flag  set ON, then
*       error with code F498 and redisplay screen.
*    Else set working storage flag to signify ROLD.
*
*  - Sequentially  read through the subflie and validate the
*    subfile record.
*
*       For each altered record;
*
*       - Check if all the fields for a record have been blanked
*       out, i.e. Deleted. Move 'D' to hidden field.
*
*       - Check if any fields for a new record have been blanked
*       out. Move 'X' to hidden field.
*
*       - Check  if any  fields  have  changed  on  an  existing
*       Journal record. Move 'U' to hidden field.
*
*       - If this is a new entry (hidden field = space) move 'A'
*       to hidden field.
*
*       - Validate Fund and Fund type against table T5515.
*
*       - Check the Fund and Fund Type against the Fund  Summary
*         record.  If no Fund Summary record exists for the fund
*         being   journalled  to  or  from,  display  an  error.
*         Otherwise,  move  the  Fund  Summary  holdings  to the
*         screen.
*
*Updating
*--------
*
*  All existing Int. journal records must be updated or deleted
*  and  any  new  records  added. Updating will provide distinct
*  ITRN records for Interest Batch processing.
*
*  - If SCRN-STATUZ = KILL  or  in  enquiry mode,  exit  to 4000
*  section.
*
*  We must update the records for a page at a time. If there are
*  several  pages  they must request rolldown or rollup in order
*  to  key  the  data. This will be handled in the 4000 section,
*  which  will  display the next screen after having updated the
*  previous subfile records. Therefore
*
*       - Perform a  section  to  action the record according to
*       the hidden field for the  whole  of  the  subfile  page,
*       increasing  the  relative   record  number  sequentially
*       through the subfile records.
*
*       - Use this to read each record directly.
*
*       - On each record, look at the hidden action field.
*
*            * If the hidden field is equal to spaces or 'X' or
*                 'N', go on to the next record.
*
*            * If the hidden  field is equal  to 'D', delete the
*                 Interest journal record.
*
*            * If the hidden  field is  equal to 'U', update the
*                 existing Interest Journal record with the
*                 screen fields.
*
*            * If the hidden field  is  equal  to  'A', move the
*                 screen  fields to the Interest Journal record
*                 write a new Interest Journal record.
*
*Next Program
*------------
*
*ROLLUP:
*
*  If the Rollup flag is set  ON,  then set first page flag OFF.
*  Provided that the Interest Journal component key is the same
*  as the Coverage component key and that we have not reached the
*  end of the Interest Journal file, load the next screen.
*
*ROLLDOWN:
*
*  If the Rolldown flag  is  set ON, then move the stored record
*  key from the last screen to the Interest Journal key and begin
*  reading the file. If this is sucessful, read the Interest
*  Journal file backwards until the screen has been filled.
*
*  However, if we reach the end of the Interest Journal file
*  or pick up a Interest Journal record for another  component
*  or contract, load the first page.
*
*NEXT:
*
*  If the Rollup or  Rolldown flags are set ON, switch them off,
*  reset the screen and exit.
*
*  Otherwise,
*
*      - Increment the program pointer and exit.
*
****************************************************************** ****
* </pre>
*/
public class P5181 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5181");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaNoMoreJrnls = "";
	private PackedDecimalData wsaaJournal = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaRrn = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaT5671Found = "N";
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLine = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPageNo = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaConcatT6647key = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaSeqnbr = new PackedDecimalData(3, 0).setUnsigned();

		/* WSBB-STACK-ARRAY */
	private FixedLengthStringData wsbbVrtfndArray = new FixedLengthStringData(48);
	private FixedLengthStringData[] wsbbVrtfnd = FLSArrayPartOfStructure(12, 4, wsbbVrtfndArray, 0);

	private FixedLengthStringData wsbbFundtypeArray = new FixedLengthStringData(12);
	private FixedLengthStringData[] wsbbFundtype = FLSArrayPartOfStructure(12, 1, wsbbFundtypeArray, 0);

	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(12);
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(12, 1, wsbbChangeArray, 0);

		/* WSAA-FUND-CURR-ARRAY */
	private FixedLengthStringData[] wsaaFundCurrRec = FLSInittedArray (10, 7);
	private FixedLengthStringData[] wsaaFund = FLSDArrayPartOfArrayStructure(4, wsaaFundCurrRec, 0);
	private FixedLengthStringData[] wsaaFundCurr = FLSDArrayPartOfArrayStructure(3, wsaaFundCurrRec, 4);

		/* The following work area is to hold the COVR key for Whole
		 Plan selection.*/
	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaCovrChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaCovrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 1);
	private FixedLengthStringData wsaaCovrLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 9);
	private FixedLengthStringData wsaaCovrCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 11);
	private FixedLengthStringData wsaaCovrRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 13);
	private FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(wsaaCovrKey, 15, FILLER).init(SPACES);
		/* WSAA-ROLL-FLGS */
	private String wsaaRolu = "";
	private String wsaaRold = "";
		/* Flag to say whether it is a Plan or an individual Policy select*/
	private String wsaaPlanPolicyFlag = "";
		/* Flag to handle section exits if the coverage/rider is not a
		 Unit linked component.*/
	private String wsaaNoUlnkFlag = "";
		/* Calc pressed <cf5>.*/
	private String wsaaCalcOnFlag = "";
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaT5551Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5551Part1 = new FixedLengthStringData(5).isAPartOf(wsaaT5551Item, 0);
	private FixedLengthStringData wsaaT5551Part2 = new FixedLengthStringData(3).isAPartOf(wsaaT5551Item, 5);

	private FixedLengthStringData wsaaTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTranscd = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTranCrtable, 4);
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t6647 = "T6647";
	private static final String t5688 = "T5688";
	private static final String t5551 = "T5551";
	private static final String t5671 = "T5671";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String ijnlrec = "IJNLREC   ";
	private static final String hitsrec = "HITSREC   ";
	private IntegerData wsaaFundIx = new IntegerData();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private IjnlTableDAM ijnlIO = new IjnlTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifemjaTableDAM lifemjaIO = new LifemjaTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private VprnudlTableDAM vprnudlIO = new VprnudlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T6647rec t6647rec = new T6647rec();
	private T5688rec t5688rec = new T5688rec();
	private T5551rec t5551rec = new T5551rec();
	private T5671rec t5671rec = new T5671rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5181ScreenVars sv = ScreenProgram.getScreenVars( S5181ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private WsaaFirstJrnlAreaInner wsaaFirstJrnlAreaInner = new WsaaFirstJrnlAreaInner();
	
	//ILIFE-8137 
    private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private List<Covrpf> covrpfList;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		redisplay2080,
		exit2090,
		exit2190,
		updateOrAdd2270,
		exit2290,
		exit2490,
		exit3190,
		exit5090,
		noMoreJournals5125,
		addSubfileRecord5130
	}

	public P5181() {
		super();
		screenVars = sv;
		new ScreenModel("S5181", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			contractHeader1020();
			validUnitLinked1035();
			headerToScreen1040();
			headingsContd1050();
			loadSubfile1060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		wsaaRrn.set(ZERO);
		wsaaSub.set(ZERO);
		initialize(wsaaFirstJrnlAreaInner.wsaaJrnlKey);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.zcurprmbal.set(ZERO);
		sv.fundAmount.set(ZERO);
		sv.hseqno.set(ZERO);
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		sv.effdate.set(wsspsmart.effdate);
	}

protected void contractHeader1020()
	{
		/* Retrieve contract header information.*/
		callChdrmja6100();
		/*CONTRACT-COVERAGE*/
		/*    Retrieve contract header information.*/
		callCovrmja6150();
		/*    Release contract header information.*/
		covrpfDAO.deleteCacheObject(covrpf);
		//callCovrmja6150();
	}

protected void validUnitLinked1035()
	{
		/* Validate changes to any of the subfile fields.*/
		wsaaNoUlnkFlag = "N";
		ulnkIO.setDataArea(SPACES);
		ulnkIO.setChdrcoy(chdrpf.getChdrcoy());
		ulnkIO.setChdrnum(chdrpf.getChdrnum());
		ulnkIO.setLife(covrpf.getLife());
		if (isEQ(covrpf.getJlife(), SPACES)) {
			ulnkIO.setJlife("00");
		}
		else {
			ulnkIO.setJlife(covrpf.getJlife());
		}
		ulnkIO.setCoverage(covrpf.getCoverage());
		ulnkIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			if (isEQ(chdrpf.getPolinc(), 1)) {
				ulnkIO.setPlanSuffix(ZERO);
			}
			else {
				ulnkIO.setPlanSuffix(ZERO);
			}
		}
		else {
			ulnkIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		ulnkIO.setFunction("READR");
		callUlnk6200();
		if (isEQ(ulnkIO.getStatuz(), varcom.mrnf)) {
			wsaaNoUlnkFlag = "Y";
			goTo(GotoLabel.exit1090);
		}
	}

protected void headerToScreen1040()
	{
		/* Load the screen header details.*/
		sv.chdrnum.set(chdrpf.getChdrnum());
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setDesctabl(t5687);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		callDesc6250();
	}

protected void headingsContd1050()
	{
		/* Contract header fields to screen.*/
		sv.numpols.set(chdrpf.getPolinc());
		sv.cntcurr.set(chdrpf.getCntcurr());
		sv.instprem.set(chdrpf.getSinstamt06());
		sv.ptdate.set(chdrpf.getPtdate());
		sv.life.set(covrpf.getLife());
		sv.coverage.set(covrpf.getCoverage());
		sv.rider.set(covrpf.getRider());
		/* Find the associated contract Life details.*/
		lifemjaIO.setDataArea(SPACES);
		lifemjaIO.setChdrnum(chdrpf.getChdrnum());
		lifemjaIO.setChdrcoy(chdrpf.getChdrcoy());
		lifemjaIO.setLife(covrpf.getLife());
		lifemjaIO.setJlife("00");
		lifemjaIO.setFunction("READR");
		callLifemja6300();
		/* Move the the life number to the screen and read the clients*/
		/* file for the name.*/
		sv.lifenum.set(lifemjaIO.getLifcnum());
		cltsIO.setClntnum(lifemjaIO.getLifcnum());
		/* Look up the contract details of the client owner (CLTS) and*/
		/* format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		callClts6350();
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void loadSubfile1060()
	{
		/* Loading the Subfile*/
		wsaaCovrKey.set(""+covrpf.getChdrcoy()+covrpf.getChdrnum()+covrpf.getLife().trim()+covrpf.getCoverage().trim()+covrpf.getRider());
		if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
			covrpf.setPlanSuffix(9999);
			//covrmjaIO.setFunction(varcom.begn);
			covrpfList=covrpfDAO.searchCovrRecordForContract(covrpf);
			sv.plnsfxOut[varcom.nd.toInt()].set("Y");
			wsaaPlanPolicyFlag = "Y";
		}
		else {
			sv.planSuffix.set(covrpf.getPlanSuffix());
		}
		/*    Check whether component level accounting is required by*/
		/*     reading T5688 - this will affect the postings in the*/
		/*     3200 section.*/
		readTableT56881500();
		readT5671T55511600();
		policyLoad5000();
	}

protected void readTableT56881500()
	{
		read1510();
	}

protected void read1510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrpf.getCnttype());
		itdmIO.setItmfrm(chdrpf.getOccdate());
		itdmIO.setFunction(varcom.begn);
		callItdm6400();
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrpf.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrpf.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(errorsInner.e308);
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT5671T55511600()
	{
		para1610();
	}

protected void para1610()
	{
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaTranscd.set(wsaaBatckey.batcBatctrcde);
		wsaaCrtable.set(covrpf.getCrtable());
		itemIO.setItemitem(wsaaTranCrtable);
		itemIO.setFunction(varcom.readr);
		callItem6450();
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5551Item.set(SPACES);
		wsaaT5551Item.set(SPACES);
		wsaaT5671Found = "N";
		for (x.set(1); !(isGT(x, 4)
		|| isEQ(wsaaT5671Found, "Y")); x.add(1)){
			if (isEQ(t5671rec.pgm[x.toInt()], wsaaProg)) {
				if (isEQ(t5671rec.edtitm[x.toInt()], SPACES)) {
					scrnparams.errorCode.set(errorsInner.f025);
					wsspcomn.edterror.set("Y");
					x.set(5);
				}
				else {
					wsaaT5551Part1.set(t5671rec.edtitm[x.toInt()]);
					wsaaT5671Found = "Y";
				}
			}
		}
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
			t5551rec.t5551Rec.set(SPACES);
			return ;
		}
		/* Get T5551 to obtain correct available fund list default*/
		itdmIO.setDataArea(SPACES);
		t5551rec.alfnds.set(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5551);
		itdmIO.setItmfrm(chdrpf.getOccdate());
		wsaaT5551Part2.set(chdrpf.getCntcurr());
		itdmIO.setItemitem(wsaaT5551Item);
		callItdm6400();
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(), t5551)
		|| isNE(itdmIO.getItemitem(), wsaaT5551Item)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			t5551rec.t5551Rec.set(SPACES);
		}
		else {
			t5551rec.t5551Rec.set(itdmIO.getGenarea());
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		scrnparams.statuz.set(varcom.oK);
		wsspcomn.edterror.set(varcom.oK);
		/* If the component selected is not a unit linked component then*/
		/*   exit each section until the 4000-section, which will return*/
		/*   to the component selection screen with an error.*/
		if (isEQ(wsaaNoUlnkFlag, "Y")) {
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag, "J")
		|| isEQ(wsaaT5671Found, "N")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validateScreen2010();
					checkForErrors2050();
				case redisplay2080:
					redisplay2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsaaT5671Found, "N")) {
			scrnparams.errorCode.set(errorsInner.f025);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.redisplay2080);
		}
	}

protected void validateScreen2010()
	{
		/* If terminating processing or enquiry go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(wsspcomn.flag, "J")) {
			goTo(GotoLabel.exit2090);
		}
		/* If cf9 pressed then redisplay the screen and associated Utrs*/
		/* record.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsaaCalcOnFlag = "Y";
		}
		else {
			wsaaCalcOnFlag = "N";
		}
		/* If roll down requested and we are on the first page then error.*/
		/* We also send error when trying to page up to an empty page*/
		/* to avoid decimal data error on attempted read of ITRN.*/
		if (isEQ(scrnparams.statuz, varcom.rold)) {
			if (isEQ(wsaaPageNo, 1)
			|| isEQ(wsaaFirstJrnlAreaInner.wsaaJrnlChdrnum, SPACES)) {
				scrnparams.errorCode.set(errorsInner.f498);
			}
			else {
				wsaaRold = "Y";
			}
		}
		/* If roll up requested and no more journals to be displayed then*/
		/* error.*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaRolu = "Y";
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SUBFILE*/
		/* Validate all entries in the subfile.*/
		for (scrnparams.subfileRrn.set(1); !(isEQ(scrnparams.statuz, varcom.mrnf)
		|| isEQ(wsspcomn.edterror, "Y")); scrnparams.subfileRrn.add(1)){
			validateSubfile2100();
		}
		/* HITR are now checked regardless of the refresh request in*/
		/* the 2200 validation section.*/
		if (isEQ(wsaaCalcOnFlag, "Y")) {
			wsspcomn.edterror.set("Y");
			scrnparams.subfileRrn.set(sv.subfilePage);
		}
	}

protected void redisplay2080()
	{
		if (isNE(scrnparams.statuz, "CALC")) {
			return ;
		}
		else {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2100()
	{
		try {
			readNextModifiedRecord2110();
			updateErrorIndicators2130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readNextModifiedRecord2110()
	{
		/* read down the subfile to find the next modified record.*/
		scrnparams.function.set(varcom.sread);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.mrnf)) {
			goTo(GotoLabel.exit2190);
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		/*FIELD-VALIDATIONS*/
		/* Validate changes to any of the subfile fields.*/
		fieldValidations2200();
	}

protected void updateErrorIndicators2130()
	{
		/* SUPD is performed on each record NOT just when errors occur,*/
		/*   it is used to update the S5181-updteflag for processing in*/
		/*   the 3000 section.*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* Interest Summary records should always be displayed as*/
		/* validation must be done to ensure that units exist for the*/
		/* fund being journalled to or from.*/
		if (isEQ(wsspcomn.edterror, varcom.oK)) {
			displayHits2400();
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void fieldValidations2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					delete2210();
					fundAmountCheck2225();
					fundCheck2230();
					fundtypeCheck2240();
				case updateOrAdd2270:
					updateOrAdd2270();
				case exit2290:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void delete2210()
	{
		/* Check if fields have been blanked out i.e. deleted.*/
		if (isEQ(sv.unitVirtualFund, SPACES)
		&& isEQ(sv.zrectyp, SPACES)
		&& isEQ(sv.fundAmount, ZERO)
		&& (isEQ(sv.updteflag, "N")
		|| isEQ(sv.updteflag, "U"))) {
			sv.updteflag.set("D");
			goTo(GotoLabel.exit2290);
		}
		/*BLANK*/
		/* Fields have been blanked out for a new record*/
		if (isEQ(sv.unitVirtualFund, SPACES)
		&& isEQ(sv.zrectyp, SPACES)
		&& isEQ(sv.fundAmount, ZERO)
		&& (isEQ(sv.updteflag, " ")
		|| isEQ(sv.updteflag, "X"))) {
			sv.updteflag.set("X");
			goTo(GotoLabel.exit2290);
		}
		if (isEQ(sv.unitVirtualFund, SPACES)
		&& isEQ(sv.zrectyp, SPACES)
		&& isEQ(sv.fundAmount, ZERO)
		&& isEQ(sv.updteflag, "D")) {
			goTo(GotoLabel.exit2290);
		}
	}

protected void fundAmountCheck2225()
	{
		/* If fund-amount has not been entered - error.*/
		if (isNE(wsaaCalcOnFlag, "Y")
		&& isNE(sv.unitVirtualFund, SPACES)
		&& isNE(sv.zrectyp, SPACES)
		&& isEQ(sv.fundAmount, ZERO)){
			sv.fundamntErr.set(errorsInner.e199);
			goTo(GotoLabel.exit2290);
		}
		else if (isEQ(sv.unitVirtualFund, SPACES)
		&& isNE(sv.zrectyp, SPACES)){
			sv.vrtfndErr.set(errorsInner.h365);
			goTo(GotoLabel.exit2290);
		}
	}

protected void fundCheck2230()
	{
		/* Read the Table T5515 to find the Fund currency.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(sv.unitVirtualFund);
		itdmIO.setFunction("BEGN");
		callItdm6400();
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5515)
		|| isNE(itdmIO.getItemitem(), sv.unitVirtualFund)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			sv.vrtfndErr.set(errorsInner.g695);
			goTo(GotoLabel.updateOrAdd2270);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
	}

protected void fundtypeCheck2240()
	{
		/* Error if the fund is not interest bearing.*/
		if (isNE(t5515rec.zfundtyp, "D")) {
			sv.vrtfndErr.set(errorsInner.e343);
		}
		/* Error if invalid fund-type specified.*/
		if (isNE(sv.zrectyp, "I")
		&& isNE(sv.zrectyp, "P")) {
			sv.zrectypErr.set(errorsInner.e217);
		}
		/*  As we read T5515 for each fund found on the screen, we*/
		/*  must store the currency.*/
		for (wsaaFundIx.set(1); !(isGT(wsaaFundIx, 10)); wsaaFundIx.add(1)){
			if (isEQ(wsaaFund[wsaaFundIx.toInt()], itdmIO.getItemitem())) {
				wsaaFundIx.set(11);
			}
			else {
				if (isEQ(wsaaFund[wsaaFundIx.toInt()], SPACES)) {
					wsaaFund[wsaaFundIx.toInt()].set(itdmIO.getItemitem());
					wsaaFundCurr[wsaaFundIx.toInt()].set(t5515rec.currcode);
					wsaaFundIx.set(11);
				}
			}
		}
	}

protected void updateOrAdd2270()
	{
		/* The subfile record has been changed so update the hidden*/
		/* flag.*/
		if (isEQ(sv.updteflag, "N")) {
			sv.updteflag.set("U");
		}
		if (isEQ(sv.updteflag, SPACES)) {
			sv.updteflag.set("A");
		}
	}

protected void displayHits2400()
	{
		try {
			readSubfileRec2410();
			hitsRecord2430();
			hitsToScreen2440();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readSubfileRec2410()
	{
		/* The subfile record has not been changed.*/
		if (isEQ(sv.unitVirtualFund, SPACES)
		&& isEQ(sv.zrectyp, SPACES)) {
			goTo(GotoLabel.exit2490);
		}
	}

protected void hitsRecord2430()
	{
		/* Read each Int. journal found for a corresponding HITS summary*/
		/* record. This will enable the user to see what the current*/
		/* fund balances for a fund and type are set at.*/
		hitsIO.setDataArea(SPACES);
		hitsIO.setChdrcoy(chdrpf.getChdrcoy());
		hitsIO.setChdrnum(chdrpf.getChdrnum());
		hitsIO.setLife(covrpf.getLife());
		hitsIO.setCoverage(covrpf.getCoverage());
		hitsIO.setRider(covrpf.getRider());
		if (isLTE(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
			hitsIO.setPlanSuffix(ZERO);
		}
		else {
			hitsIO.setPlanSuffix(covrpf.getPlanSuffix());
		}
		hitsIO.setZintbfnd(sv.unitVirtualFund);
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.readr);
		callHits6500();
	}

protected void hitsToScreen2440()
	{
		/* Move ZEROES to screen if NOT found.*/
		if (isEQ(hitsIO.getStatuz(), varcom.mrnf)) {
			sv.zcurprmbal.set(ZERO);
			sv.zrectypErr.set(errorsInner.hl10);
			sv.vrtfndErr.set(errorsInner.hl10);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/* Move balances to screen if found.*/
		if (isEQ(chdrpf.getPolinc(), 1)) {
			sv.zcurprmbal.set(hitsIO.getZcurprmbal());
		}
		else {
			if (isEQ(chdrpf.getPolinc(), chdrpf.getPolsum())) {
				if (isEQ(wsaaPlanPolicyFlag, "Y")) {
					sv.zcurprmbal.set(hitsIO.getZcurprmbal());
				}
				else {
					compute(sv.zcurprmbal, 3).setRounded(div(hitsIO.getZcurprmbal(), chdrpf.getPolsum()));
				}
			}
			else {
				if (isGT(covrpf.getPlanSuffix(), chdrpf.getPolsum())) {
					sv.zcurprmbal.set(hitsIO.getZcurprmbal());
				}
				else {
					compute(sv.zcurprmbal, 3).setRounded(div(hitsIO.getZcurprmbal(), chdrpf.getPolsum()));
				}
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* If termination of processing or enquiry or the component is*/
		/*   not a unit linked component then go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(wsspcomn.flag, "J")
		|| isEQ(wsaaNoUlnkFlag, "Y")) {
			return ;
		}
		/* Perform updates for all subfile records up to a maximum of*/
		/* one page.*/
		for (wsaaRrn.set(1); !(isGT(wsaaRrn, sv.subfilePage)); wsaaRrn.add(1)){
			updateJournals3100();
		}
		/*EXIT*/
	}

protected void updateJournals3100()
	{
		try {
			readSubfile3110();
			noUpdates3120();
			setItrnKey3130();
			deleteIjnl3140();
			addIjnl3150();
			updateIjnl3160();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readSubfile3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void noUpdates3120()
	{
		/* The subfile record has not been changed do no more processing*/
		/* but if you are on the last record read it anyway so that you*/
		/* are pointing at the right IJNL record if you want to rollup*/
		if (isEQ(sv.updteflag, "X")) {
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(sv.updteflag, SPACES)
		|| isEQ(sv.updteflag, "N")) {
			if (isEQ(scrnparams.subfileRrn, sv.subfilePage)
			&& isNE(sv.unitVirtualFund, SPACES)) {
				ijnlIO.setRecKeyData(SPACES);
				ijnlIO.setRecNonKeyData(SPACES);
				ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
				ijnlIO.setChdrnum(chdrpf.getChdrnum());
				ijnlIO.setLife(covrpf.getLife());
				ijnlIO.setCoverage(covrpf.getCoverage());
				ijnlIO.setRider(covrpf.getRider());
				ijnlIO.setPlanSuffix(covrpf.getPlanSuffix());
				ijnlIO.setSeqno(sv.hseqno);
				ijnlIO.setZintbfnd(sv.unitVirtualFund);
				ijnlIO.setZrectyp(sv.zrectyp);
				ijnlIO.setFunction(varcom.begn);
				callIjnl6550();
			}
			goTo(GotoLabel.exit3190);
		}
	}

protected void setItrnKey3130()
	{
		/* Set up key for updating or writing a Interest Journal.*/
		ijnlIO.setRecKeyData(SPACES);
		ijnlIO.setRecNonKeyData(SPACES);
		ijnlIO.setSeqno(ZERO);
		ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
		ijnlIO.setChdrnum(chdrpf.getChdrnum());
		ijnlIO.setLife(covrpf.getLife());
		ijnlIO.setCoverage(covrpf.getCoverage());
		ijnlIO.setRider(covrpf.getRider());
		ijnlIO.setPlanSuffix(covrpf.getPlanSuffix());
		ijnlIO.setZintbfnd(wsbbVrtfnd[wsaaRrn.toInt()]);
		ijnlIO.setZrectyp(wsbbFundtype[wsaaRrn.toInt()]);
		ijnlIO.setSeqno(sv.hseqno);
	}

protected void deleteIjnl3140()
	{
		/* If the update is a delete.*/
		if (isEQ(sv.updteflag, "D")) {
			ijnlIO.setFunction(varcom.readh);
			callIjnl6550();
			ijnlIO.setFormat(ijnlrec);
			ijnlIO.setFunction(varcom.delet);
			callIjnl6550();
			goTo(GotoLabel.exit3190);
		}
	}

protected void addIjnl3150()
	{
		/* If the update is an addition.*/
		if (isEQ(sv.updteflag, "A")) {
			addIjnl3200();
			if (isEQ(wsaaRrn, 1)) {
				wsaaFirstJrnlAreaInner.wsaaJrnlKey.set(ijnlIO.getRecKeyData());
			}
			goTo(GotoLabel.exit3190);
		}
	}

protected void updateIjnl3160()
	{
		/* If the update is an update.*/
		if (isEQ(sv.updteflag, "U")) {
			ijnlIO.setFunction(varcom.readh);
			callIjnl6550();
			ijnlIO.setTranno(add(chdrpf.getTranno(), 1));
			ijnlIO.setZintbfnd(sv.unitVirtualFund);
			ijnlIO.setZrectyp(sv.zrectyp);
			ijnlIO.setFundAmount(sv.fundAmount);
			ijnlIO.setFormat(ijnlrec);
			ijnlIO.setFunction(varcom.rewrt);
			callIjnl6550();
		}
	}

protected void addIjnl3200()
	{
		accRulesTable3210();
		procSeqNo3220();
		ijnlFile3240();
	}

protected void accRulesTable3210()
	{
		/* IJNL sub account code, type and account from line 13 of*/
		/* T5645 using program name as the key.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItemcoy(covrpf.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		callItem6450();
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void procSeqNo3220()
	{
		/*    We need to find the Processing Sequence Number for this*/
		/*    transaction. This is held on T6647 which we access with*/
		/*    a key of Batch transaction code concatenated with contract*/
		/*    type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrpf.getChdrcoy());
		itdmIO.setItemtabl(t6647);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde);
		stringVariable1.addExpression(chdrpf.getCnttype());
		stringVariable1.setStringInto(itdmIO.getItemitem());
		wsaaConcatT6647key.set(itdmIO.getItemitem());
		itdmIO.setItmfrm(covrpf.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		callItdm6400();
		if (isNE(itdmIO.getItemcoy(), chdrpf.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isNE(itdmIO.getItemitem(), wsaaConcatT6647key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(errorsInner.g230);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void ijnlFile3240()
	{
		/* Set up all fields for the creation of a transaction for*/
		/* a Journal entry.*/
		ijnlIO.setChdrcoy(covrpf.getChdrcoy());
		ijnlIO.setChdrnum(covrpf.getChdrnum());
		ijnlIO.setLife(covrpf.getLife());
		ijnlIO.setCoverage(covrpf.getCoverage());
		ijnlIO.setRider(covrpf.getRider());
		ijnlIO.setPlanSuffix(covrpf.getPlanSuffix());
		ijnlIO.setZintbfnd(sv.unitVirtualFund);
		ijnlIO.setZrectyp(sv.zrectyp);
		wsaaSeqno.add(1);
		ijnlIO.setSeqno(wsaaSeqno);
		setPrecision(ijnlIO.getTranno(), 0);
		ijnlIO.setTranno(add(chdrpf.getTranno(), 1));
		ijnlIO.setBatccoy(wsspcomn.company);
		ijnlIO.setBatcbrn(wsspcomn.branch);
		ijnlIO.setBatcactyr(wsspcomn.acctyear);
		ijnlIO.setBatcactmn(wsspcomn.acctmonth);
		ijnlIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ijnlIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ijnlIO.setInciNum(ZERO);
		ijnlIO.setInciPerd01(ZERO);
		ijnlIO.setInciPerd02(ZERO);
		ijnlIO.setInciprm01(ZERO);
		ijnlIO.setInciprm02(ZERO);
		ijnlIO.setSurrenderPercent(ZERO);
		ijnlIO.setFundAmount(sv.fundAmount);
		ijnlIO.setEffdate(sv.effdate);
		ijnlIO.setCrtable(covrpf.getCrtable());
		ijnlIO.setCntcurr(chdrpf.getCntcurr());
		ijnlIO.setFeedbackInd(" ");
		ijnlIO.setContractAmount(ZERO);
		ijnlIO.setFundRate(1);
		/*  Search the currency array loaded in the validation*/
		/*  section, to write to IJNL.*/
		wsaaFundIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaFundIx, wsaaFundCurrRec.length); wsaaFundIx.add(1)){
				if (isEQ(wsaaFund[wsaaFundIx.toInt()], sv.unitVirtualFund)) {
					ijnlIO.setFundCurrency(wsaaFundCurr[wsaaFundIx.toInt()]);
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
		/*    MOVE ZEROS                  TO IJNL-FUND-AMOUNT.*/
		/* Check for Component level accounting & act accordingly*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			ijnlIO.setSacscode(t5645rec.sacscode02);
			ijnlIO.setSacstype(t5645rec.sacstype02);
			ijnlIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			ijnlIO.setSacscode(t5645rec.sacscode01);
			ijnlIO.setSacstype(t5645rec.sacstype01);
			ijnlIO.setGenlcde(t5645rec.glmap01);
		}
		ijnlIO.setContractType(chdrpf.getCnttype());
		ijnlIO.setTriggerModule(SPACES);
		ijnlIO.setTriggerKey(SPACES);
		ijnlIO.setProcSeqNo(t6647rec.procSeqNo);
		ijnlIO.setSvp(1);
		ijnlIO.setFormat(ijnlrec);
		ijnlIO.setFunction(varcom.writr);
		callIjnl6550();
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* If the component selected is not a unit linked component then*/
		/*   exit, sending an error message to P6351 component select.*/
		/* ERROR - An entry has been placed on T5671 Program switching*/
		/*         for a Coverage/Rider which is not Unit Linked.*/
		if (isEQ(wsaaNoUlnkFlag, "Y")) {
			wsspcomn.msgarea.set("Component is NOT Unit Linked: T5671 entry invalid");
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			return ;
		}
		/* If roll up requested set first page flag off.*/
		/* If the Key has NOT changed then there are more Journals to be*/
		/*   loaded in which case we will clear the subfile and load a new*/
		/*   page of journals.*/
		if (isEQ(wsaaRolu, "Y")) {
			wsaaPageNo.add(1);
			clearSubfile4300();
			wsaaLine.set(1);
			while ( !(isGT(wsaaLine, sv.subfilePage))) {
				roluSubfileLoad4100();
			}

		}
		/* If roll down requested move the key of the last record of the*/
		/*   previous page and start loading the page from there backwards*/
		/* If roll down requested start with the first key on the page*/
		/* you are reading and read backwards for one page.  Then read*/
		/* forwards from that point and fill up the subfile.  If there*/
		/* is less than a page of data to display, start at the begining*/
		/* of IJNL again.*/
		if (isEQ(wsaaRold, "Y")) {
			ijnlIO.setParams(SPACES);
			ijnlIO.setRecKeyData(wsaaFirstJrnlAreaInner.wsaaJrnlKey);
			ijnlIO.setFunction(varcom.begn);
			callIjnl6550();
			/* Clear the subfile for a fresh page and set the subfile number*/
			/* and read back one page.*/
			clearSubfile4300();
			wsaaPageNo.subtract(1);
			ijnlIO.setFunction(varcom.nextp);
			wsaaSub1.set(1);
			while ( !(isEQ(wsaaSub1, sv.subfilePage)
			|| isNE(ijnlIO.getChdrcoy(), chdrpf.getChdrcoy())
			|| isNE(ijnlIO.getChdrnum(), chdrpf.getChdrnum())
			|| isEQ(ijnlIO.getStatuz(), varcom.endp))) {
				callIjnl6550();
				wsaaSub1.add(1);
			}

			roldSubfileLoad4200();
		}
		/* Clear the Roll up and Roll down flags for future use.*/
		/* Redisplay the screen.*/
		if (isEQ(wsaaRolu, "Y")
		|| isEQ(wsaaRold, "Y")) {
			wsaaRolu = "N";
			wsaaRold = "N";
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/* If we are processing a Whole Plan selection then we must*/
		/*   process all Policies for the selected Coverage/Rider,*/
		/*   Looping back to the 2000-section after having loaded the*/
		/*   screen for the next Policy*/
		/* Else we exit to the next program in the stack.*/
		if (isEQ(wsaaPlanPolicyFlag, "Y")) {
			//covrmjaIO.setFunction(varcom.nextr);
			clearSubfile4300();
			policyLoad5000();
			if (!covrpf.equals(null)) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
			}
			/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
				wsspcomn.nextprog.set(wsaaProg);
				wsspcomn.programPtr.add(1);
			}*/
			else {
				wsspcomn.nextprog.set(scrnparams.scrname);
			}
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
		}
	}

protected void roluSubfileLoad4100()
	{
		/*ROLU-SUBFILE-LOAD*/
		/* If the key has NOT changed then load a record into the subfile*/
		/* Else there are no more journals to process so clear subfile*/
		/*   record.*/
		if (isNE(ijnlIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(ijnlIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(ijnlIO.getLife(), covrpf.getLife())
		|| isNE(ijnlIO.getCoverage(), covrpf.getCoverage())
		|| isNE(ijnlIO.getRider(), covrpf.getRider())
		|| isNE(ijnlIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isEQ(ijnlIO.getStatuz(), varcom.endp)) {
			wsaaNoMoreJrnls = "Y";
			initialize(sv.subfileFields);
		}
		else {
			loadSubfile4400();
		}
		/*ADD-SUBFILE-RECORD*/
		/* Add record to the subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaLine.add(1);
		/*EXIT*/
	}

protected void roldSubfileLoad4200()
	{
		roldSubfileLoad4210();
	}

protected void roldSubfileLoad4210()
	{
		/* Read the previous transaction until a key change or the*/
		/* screen page has been filled.*/
		/* If key changes then we will have to clear the subfile and start*/
		/*   again but at the start of the file this time.*/
		if (isNE(ijnlIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(ijnlIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(ijnlIO.getLife(), covrpf.getLife())
		|| isNE(ijnlIO.getCoverage(), covrpf.getCoverage())
		|| isNE(ijnlIO.getRider(), covrpf.getRider())
		|| isNE(ijnlIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isEQ(ijnlIO.getStatuz(), varcom.endp)) {
			ijnlIO.setParams(SPACES);
			ijnlIO.setRecKeyData(SPACES);
			ijnlIO.setRecNonKeyData(SPACES);
			ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
			ijnlIO.setChdrnum(chdrpf.getChdrnum());
			ijnlIO.setLife(covrpf.getLife());
			ijnlIO.setCoverage(covrpf.getCoverage());
			ijnlIO.setRider(covrpf.getRider());
			ijnlIO.setPlanSuffix(covrpf.getPlanSuffix());
			ijnlIO.setZintbfnd(SPACES);
			ijnlIO.setZrectyp(SPACES);
			ijnlIO.setSeqno(ZERO);
			ijnlIO.setFunction(varcom.begn);
			callIjnl6550();
			/* This means we are now loading the first page of the whole*/
			/*   subfile. ROLU will load each line for us.*/
			wsaaPageNo.set(1);
		}
		wsaaLine.set(1);
		while ( !(isGT(wsaaLine, sv.subfilePage))) {
			roluSubfileLoad4100();
		}

	}

protected void clearSubfile4300()
	{
		/*CLEAR-SUBFILE*/
		initialize(sv.subfileFields);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void loadSubfile4400()
	{
		loadScreen4410();
		rolu4410();
	}

protected void loadScreen4410()
	{
		/* Load the Journal details to the screen.*/
		sv.unitVirtualFund.set(ijnlIO.getZintbfnd());
		wsbbVrtfnd[wsaaSub.toInt()].set(ijnlIO.getZintbfnd());
		sv.zrectyp.set(ijnlIO.getZrectyp());
		wsbbFundtype[wsaaSub.toInt()].set(ijnlIO.getZrectyp());
		sv.hseqno.set(ijnlIO.getSeqno());
		sv.fundAmount.set(ijnlIO.getFundAmount());
		sv.updteflag.set("N");
		sv.zcurprmbal.set(ZERO);
	}

protected void rolu4410()
	{
		/* Read sequentially through the Transactions.*/
		ijnlIO.setFunction(varcom.begn);
		callIjnl6550();
		if (isEQ(wsaaLine, 1)) {
			wsaaFirstJrnlAreaInner.wsaaJrnlKey.set(ijnlIO.getRecKeyData());
		}
		ijnlIO.setFunction(varcom.nextr);
		callIjnl6550();
		/*EXIT*/
	}

protected void policyLoad5000()
	{
		try {
			policyCovr5010();
			searchForItrn5020();
			firstPage5030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void policyCovr5010()
	{
		/* This section will load the sufile records for each policy, as*/
		/*   the coverage/rider can be selected for the whole Plan this*/
		/*   section is designed to load each of the policies in turn for*/
		/*   the coverage/rider type.*/
		/* If a policy is selected this section will only be performed*/
		/*   once within the 1000-section.*/
		/* If whole plan is selected then this section will be performed*/
		/*   for all the broken out policies and the summarised record*/
		/*   within the 4000-section.*/
		if (isEQ(wsaaPlanPolicyFlag, "Y")) {
			readCovr5200();
			for (Covrpf covr : covrpfList) {
				if (covr == null) {
					goTo(GotoLabel.exit5090);
				}
				else {
					/* The header is set to show which selection is being actioned.*/
					if (isEQ(covrpf.getPlanSuffix(), ZERO)) {
						sv.planSuffix.set(chdrpf.getPolsum());
						sv.plnsfxOut[varcom.hi.toInt()].set("Y");
					}
					else {
						sv.planSuffix.set(covrpf.getPlanSuffix());
					}
				}
				covrpf.setPlanSuffix(covr.getPlanSuffix());
			}
			/*if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
				goTo(GotoLabel.exit5090);
			}*/
			
		}
	}

protected void searchForItrn5020()
	{
		/* Store the first subfile record key for later use if a*/
		/* ROLD is pressed.*/
		ijnlIO.setRecKeyData(SPACES);
		ijnlIO.setSeqno(ZERO);
		ijnlIO.setChdrcoy(chdrpf.getChdrcoy());
		ijnlIO.setChdrnum(chdrpf.getChdrnum());
		ijnlIO.setLife(covrpf.getLife());
		ijnlIO.setCoverage(covrpf.getCoverage());
		ijnlIO.setRider(covrpf.getRider());
		ijnlIO.setPlanSuffix(covrpf.getPlanSuffix());
		ijnlIO.setZintbfnd(SPACES);
		ijnlIO.setZrectyp(SPACES);
		ijnlIO.setFunction("BEGN");
	}

protected void firstPage5030()
	{
		/* Load the first page into the subfile.*/
		wsaaSub.set(ZERO);
		wsaaNoMoreJrnls = "N";
		scrnparams.subfileRrn.set(1);
		scrnparams.subfileMore.set("Y");
		wsaaPageNo.set("1");
		for (wsaaSub.set(1); !(isGT(wsaaSub, sv.subfilePage)); wsaaSub.add(1)){
			loadSubfile5100();
		}
	}

protected void loadSubfile5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					ijnlJournals5110();
					ijnlToScreen5120();
				case noMoreJournals5125:
					noMoreJournals5125();
				case addSubfileRecord5130:
					addSubfileRecord5130();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void ijnlJournals5110()
	{
		/* Read for any existing Interest journals.*/
		if (isEQ(wsaaNoMoreJrnls, "Y")) {
			goTo(GotoLabel.noMoreJournals5125);
		}
		callIjnl6550();
		ijnlIO.setFunction(varcom.nextr);
		if (isNE(ijnlIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(ijnlIO.getChdrnum(), chdrpf.getChdrnum())
		|| isNE(ijnlIO.getLife(), covrpf.getLife())
		|| isNE(ijnlIO.getCoverage(), covrpf.getCoverage())
		|| isNE(ijnlIO.getRider(), covrpf.getRider())
		|| isNE(ijnlIO.getPlanSuffix(), covrpf.getPlanSuffix())
		|| isEQ(ijnlIO.getStatuz(), varcom.endp)) {
			wsaaNoMoreJrnls = "Y";
			goTo(GotoLabel.noMoreJournals5125);
		}
		/* Store a reference to each record in Working Storage.*/
		wsbbVrtfnd[wsaaSub.toInt()].set(ijnlIO.getZintbfnd());
		wsbbFundtype[wsaaSub.toInt()].set(ijnlIO.getZrectyp());
		/* On the first record store the key for use in ROLD request.*/
		if (isEQ(wsaaSub, 1)) {
			wsaaFirstJrnlAreaInner.wsaaJrnlKey.set(ijnlIO.getRecKeyData());
		}
	}

protected void ijnlToScreen5120()
	{
		/* Move Interest Journal details to screen if found.*/
		sv.unitVirtualFund.set(ijnlIO.getZintbfnd());
		wsbbVrtfnd[wsaaSub.toInt()].set(ijnlIO.getZintbfnd());
		sv.zrectyp.set(ijnlIO.getZrectyp());
		wsbbFundtype[wsaaSub.toInt()].set(ijnlIO.getZrectyp());
		sv.hseqno.set(ijnlIO.getSeqno());
		sv.fundAmount.set(ijnlIO.getFundAmount());
		sv.updteflag.set("N");
		goTo(GotoLabel.addSubfileRecord5130);
	}

protected void noMoreJournals5125()
	{
		/* If there are no more journals move spaces to the subfile*/
		/* lines to allow input of new data.*/
		initialize(sv.subfileFields);
	}

protected void addSubfileRecord5130()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5181", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void readCovr5200()
	{
		/*READ-COVRMJA*/
	//ILIFE-8137
		covrpfList = covrpfDAO.getCovrByComAndNum(covrpf.getChdrcoy(),covrpf.getChdrnum());
		for (Covrpf covr:covrpfList) {
			if (isNE(covr.getChdrcoy(),wsaaCovrChdrcoy)
					|| isNE(covr.getChdrnum(),wsaaCovrChdrnum)
					|| isNE(covr.getLife(),wsaaCovrLife)
					|| isNE(covr.getCoverage(),wsaaCovrCoverage)
					|| isNE(covr.getRider(),wsaaCovrRider)
					|| (!covrpfList.isEmpty())) {
						return;
					}
		}
		/*callCovrmja6150();
		if (isNE(covrmjaIO.getChdrcoy(), wsaaCovrChdrcoy)
		|| isNE(covrmjaIO.getChdrnum(), wsaaCovrChdrnum)
		|| isNE(covrmjaIO.getLife(), wsaaCovrLife)
		|| isNE(covrmjaIO.getCoverage(), wsaaCovrCoverage)
		|| isNE(covrmjaIO.getRider(), wsaaCovrRider)
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}*/
		/*EXIT*/
	}

protected void callChdrmja6100()
	{
		/*CALL*/
		//ILIFE-8137
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if(null==chdrpf) {
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrmjaIO.getParams());
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(chdrmjaIO.getChdrcoy().toString(), chdrmjaIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void callCovrmja6150()
	{
		/*CALL*/
		//ILIFE-8137
		covrpf = covrpfDAO.getCacheObject(covrpf);
		if(null==covrpf) {
			covrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
		}
		else {
			covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
					covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
			if(null==covrpf) {
				fatalError600();
			}
		else {
			covrpfDAO.setCacheObject(covrpf);
			}
		}
	}
		/*SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}*/
		/*EXIT*/
	}

protected void callUlnk6200()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)
		&& isNE(ulnkIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(ulnkIO.getStatuz());
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callDesc6250()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill(" ");
		}
		/*EXIT*/
	}

protected void callLifemja6300()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, lifemjaIO);
		if (isNE(lifemjaIO.getStatuz(), varcom.oK)
		&& isNE(lifemjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(lifemjaIO.getStatuz());
			syserrrec.params.set(lifemjaIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClts6350()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callItdm6400()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callItem6450()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callHits6500()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, hitsIO);
		if (isNE(hitsIO.getStatuz(), varcom.oK)
		&& isNE(hitsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hitsIO.getStatuz());
			syserrrec.params.set(hitsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callIjnl6550()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, ijnlIO);
		if (isNE(ijnlIO.getStatuz(), varcom.oK)
		&& isNE(ijnlIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ijnlIO.getStatuz());
			syserrrec.params.set(ijnlIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-FIRST-JRNL-AREA--INNER
 */
private static final class WsaaFirstJrnlAreaInner {
		/* WSAA-FIRST-JRNL-AREA */
	private FixedLengthStringData wsaaFirstJrnlKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaJrnlKey = new FixedLengthStringData(64).isAPartOf(wsaaFirstJrnlKey, 0, REDEFINE);
	private FixedLengthStringData wsaaJrnlChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaJrnlKey, 0);
	private FixedLengthStringData wsaaJrnlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaJrnlKey, 1);
	private FixedLengthStringData wsaaJrnlLife = new FixedLengthStringData(2).isAPartOf(wsaaJrnlKey, 9);
	private FixedLengthStringData wsaaJrnlCoverage = new FixedLengthStringData(2).isAPartOf(wsaaJrnlKey, 11);
	private FixedLengthStringData wsaaJrnlRider = new FixedLengthStringData(2).isAPartOf(wsaaJrnlKey, 13);
	private PackedDecimalData wsaaJrnlPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsaaJrnlKey, 15);
	private FixedLengthStringData wsaaJrnlVrtfnd = new FixedLengthStringData(4).isAPartOf(wsaaJrnlKey, 18);
	private FixedLengthStringData wsaaJrnlFundtype = new FixedLengthStringData(1).isAPartOf(wsaaJrnlKey, 22);
	private PackedDecimalData wsaaJrnlTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaJrnlKey, 23);
	private FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(wsaaJrnlKey, 26, FILLER);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData f025 = new FixedLengthStringData(4).init("F025");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData e217 = new FixedLengthStringData(4).init("E217");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData g695 = new FixedLengthStringData(4).init("G695");
	private FixedLengthStringData g230 = new FixedLengthStringData(4).init("G230");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData hl10 = new FixedLengthStringData(4).init("HL10");
	private FixedLengthStringData e343 = new FixedLengthStringData(4).init("E343");
	private FixedLengthStringData e199 = new FixedLengthStringData(4).init("E199");
	private FixedLengthStringData h365 = new FixedLengthStringData(4).init("H365");
}
}
