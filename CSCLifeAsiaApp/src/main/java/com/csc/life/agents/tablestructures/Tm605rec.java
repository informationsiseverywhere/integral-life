package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:54
 * Description:
 * Copybook name: TM605REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tm605rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tm605Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData mlagtprds = new FixedLengthStringData(85).isAPartOf(tm605Rec, 0);
  	public ZonedDecimalData[] mlagtprd = ZDArrayPartOfStructure(5, 17, 2, mlagtprds, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(85).isAPartOf(mlagtprds, 0, FILLER_REDEFINE);
  	public ZonedDecimalData mlagtprd01 = new ZonedDecimalData(17, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData mlagtprd02 = new ZonedDecimalData(17, 2).isAPartOf(filler, 17);
  	public ZonedDecimalData mlagtprd03 = new ZonedDecimalData(17, 2).isAPartOf(filler, 34);
  	public ZonedDecimalData mlagtprd04 = new ZonedDecimalData(17, 2).isAPartOf(filler, 51);
  	public ZonedDecimalData mlagtprd05 = new ZonedDecimalData(17, 2).isAPartOf(filler, 68);
  	public FixedLengthStringData mlagttyps = new FixedLengthStringData(36).isAPartOf(tm605Rec, 85);
  	public FixedLengthStringData[] mlagttyp = FLSArrayPartOfStructure(18, 2, mlagttyps, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(mlagttyps, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mlagttyp01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData mlagttyp02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData mlagttyp03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData mlagttyp04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData mlagttyp05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData mlagttyp06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData mlagttyp07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData mlagttyp08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData mlagttyp09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData mlagttyp10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData mlagttyp11 = new FixedLengthStringData(2).isAPartOf(filler1, 20);
  	public FixedLengthStringData mlagttyp12 = new FixedLengthStringData(2).isAPartOf(filler1, 22);
  	public FixedLengthStringData mlagttyp13 = new FixedLengthStringData(2).isAPartOf(filler1, 24);
  	public FixedLengthStringData mlagttyp14 = new FixedLengthStringData(2).isAPartOf(filler1, 26);
  	public FixedLengthStringData mlagttyp15 = new FixedLengthStringData(2).isAPartOf(filler1, 28);
  	public FixedLengthStringData mlagttyp16 = new FixedLengthStringData(2).isAPartOf(filler1, 30);
  	public FixedLengthStringData mlagttyp17 = new FixedLengthStringData(2).isAPartOf(filler1, 32);
  	public FixedLengthStringData mlagttyp18 = new FixedLengthStringData(2).isAPartOf(filler1, 34);
  	public FixedLengthStringData mlgrppps = new FixedLengthStringData(85).isAPartOf(tm605Rec, 121);
  	public ZonedDecimalData[] mlgrppp = ZDArrayPartOfStructure(5, 17, 2, mlgrppps, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(mlgrppps, 0, FILLER_REDEFINE);
  	public ZonedDecimalData mlgrppp01 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData mlgrppp02 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 17);
  	public ZonedDecimalData mlgrppp03 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 34);
  	public ZonedDecimalData mlgrppp04 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 51);
  	public ZonedDecimalData mlgrppp05 = new ZonedDecimalData(17, 2).isAPartOf(filler2, 68);
  	public FixedLengthStringData mlperpps = new FixedLengthStringData(85).isAPartOf(tm605Rec, 206);
  	public ZonedDecimalData[] mlperpp = ZDArrayPartOfStructure(5, 17, 2, mlperpps, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(85).isAPartOf(mlperpps, 0, FILLER_REDEFINE);
  	public ZonedDecimalData mlperpp01 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData mlperpp02 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 17);
  	public ZonedDecimalData mlperpp03 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 34);
  	public ZonedDecimalData mlperpp04 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 51);
  	public ZonedDecimalData mlperpp05 = new ZonedDecimalData(17, 2).isAPartOf(filler3, 68);
  	public FixedLengthStringData mlprcinds = new FixedLengthStringData(6).isAPartOf(tm605Rec, 291);
  	public FixedLengthStringData[] mlprcind = FLSArrayPartOfStructure(6, 1, mlprcinds, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(6).isAPartOf(mlprcinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mlprcind01 = new FixedLengthStringData(1).isAPartOf(filler4, 0);
  	public FixedLengthStringData mlprcind02 = new FixedLengthStringData(1).isAPartOf(filler4, 1);
  	public FixedLengthStringData mlprcind03 = new FixedLengthStringData(1).isAPartOf(filler4, 2);
  	public FixedLengthStringData mlprcind04 = new FixedLengthStringData(1).isAPartOf(filler4, 3);
  	public FixedLengthStringData mlprcind05 = new FixedLengthStringData(1).isAPartOf(filler4, 4);
  	public FixedLengthStringData mlprcind06 = new FixedLengthStringData(1).isAPartOf(filler4, 5);
  	public FixedLengthStringData toYears = new FixedLengthStringData(10).isAPartOf(tm605Rec, 297);
  	public ZonedDecimalData[] toYear = ZDArrayPartOfStructure(5, 2, 0, toYears, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(toYears, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toYear01 = new ZonedDecimalData(2, 0).isAPartOf(filler5, 0);
  	public ZonedDecimalData toYear02 = new ZonedDecimalData(2, 0).isAPartOf(filler5, 2);
  	public ZonedDecimalData toYear03 = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4);
  	public ZonedDecimalData toYear04 = new ZonedDecimalData(2, 0).isAPartOf(filler5, 6);
  	public ZonedDecimalData toYear05 = new ZonedDecimalData(2, 0).isAPartOf(filler5, 8);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(193).isAPartOf(tm605Rec, 307, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tm605Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tm605Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}