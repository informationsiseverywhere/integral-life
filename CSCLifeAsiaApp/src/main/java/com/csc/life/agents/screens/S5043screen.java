package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5043screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
/*	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; */
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5043ScreenVars sv = (S5043ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S5043screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5043ScreenVars screenVars = (S5043ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.action.setClassString("");*/
		ScreenRecord.setClassStringFormatting(pv);
	
	}

/**
 * Clear all the variables in S5043screen
 */
	public static void clear(VarModel pv) {
		S5043ScreenVars screenVars = (S5043ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.agntsel.clear();
		screenVars.action.clear();*/
		ScreenRecord.clear(pv);
	}
}
