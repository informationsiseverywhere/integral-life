package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5040screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 15;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {9, 22, 3, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5040ScreenVars sv = (S5040ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5040screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5040screensfl, 
			sv.S5040screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5040ScreenVars sv = (S5040ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5040screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5040ScreenVars sv = (S5040ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5040screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5040screensflWritten.gt(0))
		{
			sv.s5040screensfl.setCurrentIndex(0);
			sv.S5040screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5040ScreenVars sv = (S5040ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5040screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5040ScreenVars screenVars = (S5040ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.reportag.setFieldName("reportag");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.agentname.setFieldName("agentname");
				screenVars.ovcpc.setFieldName("ovcpc");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.reportag.set(dm.getField("reportag"));
			screenVars.clntnum.set(dm.getField("clntnum"));
			screenVars.agentname.set(dm.getField("agentname"));
			screenVars.ovcpc.set(dm.getField("ovcpc"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5040ScreenVars screenVars = (S5040ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.reportag.setFieldName("reportag");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.agentname.setFieldName("agentname");
				screenVars.ovcpc.setFieldName("ovcpc");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("reportag").set(screenVars.reportag);
			dm.getField("clntnum").set(screenVars.clntnum);
			dm.getField("agentname").set(screenVars.agentname);
			dm.getField("ovcpc").set(screenVars.ovcpc);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5040screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5040ScreenVars screenVars = (S5040ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.reportag.clearFormatting();
		screenVars.clntnum.clearFormatting();
		screenVars.agentname.clearFormatting();
		screenVars.ovcpc.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5040ScreenVars screenVars = (S5040ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.reportag.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.ovcpc.setClassString("");
	}

/**
 * Clear all the variables in S5040screensfl
 */
	public static void clear(VarModel pv) {
		S5040ScreenVars screenVars = (S5040ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.reportag.clear();
		screenVars.clntnum.clear();
		screenVars.agentname.clear();
		screenVars.ovcpc.clear();
	}
}
