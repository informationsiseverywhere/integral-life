package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl57ScreenVars;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl57 extends ScreenProgCS{
		
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl57.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL57");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl57ScreenVars sv = ScreenProgram.getScreenVars(Sjl57ScreenVars.class);
	List<Agncypf> agncypfList = new ArrayList<>();
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private String Sjl57 = "Sjl57";  
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private Hierpf hierpf = new Hierpf();
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private Clntpf clntpf = null;
	private Map<String,Descpf> descMap = new HashMap<>();
	
	private static final String JL91 = "JL91";
	
	boolean isExisted = true;
	private static final String SALESREP = "Sales Representative";
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";
	private static final String TJL76 = "TJL76";
	
	private List<Descpf> tjl76List;
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);

	public Pjl57() {
		super();
		screenVars = sv;
		new ScreenModel(Sjl57, AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}
	
	
	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
		initialise1010();
	}


protected void initialise1010()
	{
		/* IF WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                    */
		/*      GO TO 1090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		sv.indxflg.set(SPACES);
		sv.select.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(Varcom.sclr);
		screenIo9000();
		sv.company.set(wsspcomn.company);
		sv.levelno.set(wsspcomn.chdrCownnum);
		sv.leveltyp.set("1");
		loadLongDesc();
		initialise1020();
		//display the list area
		getAgencyResultsList1030();
	}
	
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(TJL70, "1");
		
		descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(TJL70)) {
			sv.leveldesc.set(SALESREP);
		} else {
			sv.leveldesc.set(descMap.get(TJL70).getLongdesc());
		}

	}
	
	private void initialise1020() {

		if (isEQ(wsspcomn.chdrCownnum, SPACE)) {
			sv.levelnoErr.set(JL91);
			return;
		}
		
		wsaaSalediv.set(SPACES);
		
		
		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.clntname.set(clntpf.getSurname() + " " + clntpf.getGivname());
			}
			
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.agntbr.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();
						
			}
			if (null != agsdpf.getLevelno())
				sv.levelno.set(agsdpf.getLevelno());
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltyp.set(agsdpf.getLevelclass());
				loadLongDesc();
			}
		} else {
			sv.levelnoErr.set(JL91);
		}

	}
	
	protected Clntpf cltsioCall2700(String clntNum) {

		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), clntNum);
	}
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.agntbr.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		
		
		descMap= descdao.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.agbrdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.agbrdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}	

	}
	

	private void getAgencyResultsList1030(){
	
		wsaaroleCode.set("SR");
		wsaahierCode.set(sv.levelno.trim());
	
		agncypfList = agncypfDAO.getAgncybyClient(wsspcomn.company.toString(), wsaaKey.toString().trim());
		try {
			if(agncypfList!=null && !agncypfList.isEmpty()){
				wsaaCount.set(agncypfList.size());
				for(int iy=0 ; iy<agncypfList.size() ; iy++) {
				Clntpf clntpf1 = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),agncypfList.get(iy).getClntnum().trim());
				sv.cltname.set(clntpf1.getGivname() + " " + clntpf1.getSurname());
				sv.select.set(SPACES);
				sv.agncynum.set(agncypfList.get(iy).getAgncynum());
				sv.regnum.set(agncypfList.get(iy).getRegnum());

				tjl76List = descdao.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76, 
						agncypfList.get(iy).getBrnch().trim(),wsspcomn.language.toString());
		    	
		    	if (tjl76List.isEmpty()) {
					sv.regclass.set(agncypfList.get(iy).getBrnch());
		    	} else {
			    	for (Descpf descItem : tjl76List) {
						sv.regclass.set(descItem.getLongdesc());		
					}
		    	}
				sv.startDate.set(agncypfList.get(iy).getSrdate());
				sv.dateend.set(agncypfList.get(iy).getEndate());
				addToSubfile1500();
				wsaaCount.minusminus();
			}
			}
		}	catch (Exception ex) {
			// TODO Auto-generated catch block
			LOGGER.error("Failed to get records from agncypf:", ex);
		}
	}

	protected void addToSubfile1500()
	{
		/*ADD-LINE*/
		scrnparams.function.set(Varcom.sadd);
		processScreen(Sjl57, sv);
		if ((isNE(scrnparams.statuz, Varcom.oK))
		&& (isNE(scrnparams.statuz, Varcom.endp))) {
		syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		/* IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'                  */
		/*      MOVE O-K               TO WSSP-EDTERROR                 */
		/*      GO TO 2090-EXIT.                                        */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		/*SCREEN-IO*/
		return ;
	}
@Override
protected void screenEdit2000()
	{
		screenIo2010();
		validateSubfile2600();
 	}


	/**
	* <pre>
	*2000-PARA.
	* </pre>
	*/
protected void screenIo2010()
	{
	
	wsspcomn.edterror.set(Varcom.oK);
	if (isEQ(scrnparams.statuz, "KILL")) {
		wsspcomn.edterror.set(Varcom.oK);
		exit2090();
	}
	 if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
			exit2090();
	}
		
}

protected void exit2090(){
	
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	/*EXIT*/
}
protected void validateSubfile2600()
{
	validation2610();
	updateErrorIndicators2670();
	readNextModifiedRecord2680();
}

protected void validation2610()
{
	 	if (isEQ(scrnparams.statuz, Varcom.kill)) {
	 		return;
	 	}
	 	scrnparams.function.set(Varcom.srnch);
		screenIo9000();

}


protected void updateErrorIndicators2670()
{
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	scrnparams.function.set(Varcom.supd);
	processScreen(Sjl57, sv);
	if (isNE(scrnparams.statuz, Varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	if (isNE(sv.errorSubfile, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void readNextModifiedRecord2680()
{
	scrnparams.function.set(Varcom.srnch);
	processScreen(Sjl57, sv);
	if (isNE(scrnparams.statuz, Varcom.oK)
	&& isNE(scrnparams.statuz, Varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}

@Override
protected void update3000()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
	}

/**
 * Getting Checked check box values and perform operation 
 */
protected void getCheckedScreenRecord2080()
{
	
	if(isNE(sv.subfileArea, SPACES)){
		scrnparams.function.set(Varcom.sstrt);
		a1000Scrnio();
		
		List<String> deleteList = new ArrayList<String>();
		while (isEQ(scrnparams.statuz, Varcom.oK)) {
			if (isEQ(sv.select, "1")) {
				deleteList.add(sv.agncynum.trim());
			}
			scrnparams.function.set(Varcom.srdn);
			a1000Scrnio();
			}
		
		if(deleteList.size() > 0){
			for(String delete: deleteList)
			{
				updateHierpf3010(delete);
			}
		}
		
		sv.select.set(SPACES);
	}
}

protected void a1000Scrnio(){
	processScreen(Sjl57, sv);
	if (isNE(scrnparams.statuz, Varcom.oK)
			&& isNE(scrnparams.statuz, Varcom.mrnf)
			&& isNE(scrnparams.statuz, Varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
}

protected void updateHierpf3010(String delete)
{
		wsaaroleCode.set("AN");
		wsaahierCode.set(delete.trim());
		hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim(),"1","1","0");
		Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
		if (isExists.isPresent()) {
			if(isEQ(hierpf.getRegisclass(),"3"))
			{
				hierpf.setActivestatus("2");
				hierpf.setApprovestatus("1");
			}
			else
				hierpf.setApprovestatus("1");
			try {
				hierpfDAO.updateHierpf(hierpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update HIERPF :", ex);
			}
		}	
		
}

protected void screenIo9000()
{
	/*BEGIN*/
	processScreen(Sjl57, sv);
	if (isNE(scrnparams.statuz, Varcom.oK)
			&& isNE(scrnparams.statuz, Varcom.endp)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	/*EXIT*/
}



	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/

@Override
protected void whereNext4000()
{

	nextProgram4020();
	/*NEXT-PROGRAM
	EXIT*/
}


protected void nextProgram4020()
{
	
	getCheckedScreenRecord2080();
	wsspcomn.programPtr.add(1);

}


}
