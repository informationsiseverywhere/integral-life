/*
 * File: Screen Variables for Sr60d
 * Date: 29 Nov 2016 6:45:10
 * Author: pmujavadiya
 * 
 * 
 * Copyright (2013) CSC Asia, all rights reserved.
 */

package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sr60dScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(744);
	public FixedLengthStringData dataFields = new FixedLengthStringData(328).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData lifedesc = DD.lifedesc.copy().isAPartOf(dataFields,49);
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal().isAPartOf(dataFields,89);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(dataFields,97);
	public ZonedDecimalData zsgtpct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,105);
	public ZonedDecimalData zoerpct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,110);
	public ZonedDecimalData zdedpct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,115);
	public ZonedDecimalData zundpct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,120);
	public ZonedDecimalData zspspct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,125);
	public ZonedDecimalData totprcnt = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,130);
	public ZonedDecimalData zsgtamt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,135);
	public ZonedDecimalData zoeramt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,152);
	public ZonedDecimalData zdedamt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,169);
	public ZonedDecimalData zundamt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,186);
	public ZonedDecimalData zspsamt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,203);
	public ZonedDecimalData totamnt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,220);
	public ZonedDecimalData zslrysamt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,237);//ILIFE-7345
	public ZonedDecimalData zslryspct = DD.zsgtpct.copyToZonedDecimal().isAPartOf(dataFields,254); //ILIFE-7345
//  ILIFE-7916 Start
	public FixedLengthStringData s290NoticeReceived = DD.s290NoticeReceived.copy().isAPartOf(dataFields,259);
	public ZonedDecimalData riskPrem = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,260);
	public ZonedDecimalData tottax = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,277);
	public ZonedDecimalData coContriPmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,294);
	public ZonedDecimalData receivedFromAto = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,311);
  //   ILIFE-7916 End
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 328);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData lifedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData datefrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData zsgtpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData zoerpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData zdedpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData zundpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData zspspctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData totprcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData zsgtamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData zoeramtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData zdedamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData zundamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData zspsamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData zslrysamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);//ILIFE-7345
	public FixedLengthStringData zslryspctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);//ILIFE-7345
	
//  ILIFE-7916 Start
	public FixedLengthStringData s290NoticeReceivedErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData riskPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData tottaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData coContriPmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData receivedFromAtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	//   ILIFE-7916 End
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea,432);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] lifedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] datefrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] zsgtpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] zoerpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] zdedpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] zundpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] zspspctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] totprcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] zsgtamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] zoeramtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] zdedamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] zundamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] zspsamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] zslrysamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);//ILIFE-7345
	public FixedLengthStringData[] zslryspctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);//ILIFE-7345
//  ILIFE-7916 Start
	public FixedLengthStringData[] s290NoticeReceivedOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] riskPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] tottaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] coContriPmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] receivedFromAtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
//   ILIFE-7916 End
	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData datefrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);
	
	public LongData Sr60dscreenWritten = new LongData(0);
	public LongData Sr60dprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr60dScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
		fieldIndMap.put(datefrmOut,new String[] {"2","3","-2","4",null, null, null, null, null, null, null, null});
		fieldIndMap.put(datetoOut,new String[] {"5","6","-5","7",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsgtpctOut,new String[] {"8","9","-8","10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zoerpctOut,new String[] {"11","12","-11","13",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdedpctOut,new String[] {"14","15","-14","16",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zundpctOut,new String[] {"17","18","-17","19",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totprcntOut,new String[] {"20","21","-20","22",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zspspctOut,new String[] {"23","24","-23","25",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totamntOut,new String[] {"26","27","-26","28",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zslryspctOut,new String[] {"29","30","31","32",null, null, null, null, null, null, null, null});//ILIFE-7345
	//  ILIFE-7916 Start
			fieldIndMap.put(s290NoticeReceivedOut,new String[] {"33","34","35","36",null, null, null, null, null, null, null, null});//ILIFE-7345
			fieldIndMap.put(riskPremOut,new String[] {"37","38","39","40",null, null, null, null, null, null, null, null});
			fieldIndMap.put(tottaxOut,new String[] {"41","42","43","44",null, null, null, null, null, null, null, null});
			fieldIndMap.put(coContriPmtOut,new String[] {"45","46","47","48",null, null, null, null, null, null, null, null});
			fieldIndMap.put(receivedFromAtoOut,new String[] {"49","50","51","52",null, null, null, null, null, null, null, null});
			
	   //   ILIFE-7916 End
	screenFields = new BaseData[] {chdrnum, cnttype, ctypdesc, lifenum, lifedesc, datefrm, dateto, zsgtpct, zoerpct, zdedpct, zundpct, zspspct, totprcnt , zsgtamt, zoeramt, zdedamt, zundamt, zspsamt, totamnt,zslrysamt,zslryspct,s290NoticeReceived,riskPrem,tottax,coContriPmt,receivedFromAto};
	screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypdescOut, lifenumOut, lifedescOut, datefrmOut, datetoOut, zsgtpctOut, zoerpctOut, zdedpctOut, zundpctOut, zspspctOut, totprcntOut, zsgtamtOut, zoeramtOut, zdedamtOut, zundamtOut, zspsamtOut, totamntOut,zslrysamtOut,zslryspctOut,s290NoticeReceivedOut,riskPremOut,tottaxOut,coContriPmtOut,receivedFromAtoOut};
	screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypdescErr, lifenumErr, lifedescErr, datefrmErr, datetoErr, zsgtpctErr, zoerpctErr, zdedpctErr, zundpctErr, zspspctErr, totprcntErr , zsgtamtErr, zoeramtErr, zdedamtErr, zundamtErr, zspsamtErr, totamntErr,zslrysamtErr,zslryspctErr,s290NoticeReceivedErr,riskPremErr,tottaxErr,coContriPmtErr,receivedFromAtoErr};
	
	screenDateFields = new BaseData[] {datefrm, dateto};
	screenDateErrFields = new BaseData[] {datefrmErr, datetoErr};
	screenDateDispFields = new BaseData[] {datefrmDisp,datetoDisp};

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr60dscreen.class;
	protectRecord = Sr60dprotect.class;

	}

 


}
