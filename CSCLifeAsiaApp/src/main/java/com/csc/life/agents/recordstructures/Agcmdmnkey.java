package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:59
 * Description:
 * Copybook name: AGCMDMNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmdmnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmdmnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmdmnKey = new FixedLengthStringData(64).isAPartOf(agcmdmnFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmdmnChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmdmnKey, 0);
  	public FixedLengthStringData agcmdmnChdrnum = new FixedLengthStringData(8).isAPartOf(agcmdmnKey, 1);
  	public FixedLengthStringData agcmdmnAgntnum = new FixedLengthStringData(8).isAPartOf(agcmdmnKey, 9);
  	public FixedLengthStringData agcmdmnLife = new FixedLengthStringData(2).isAPartOf(agcmdmnKey, 17);
  	public FixedLengthStringData agcmdmnCoverage = new FixedLengthStringData(2).isAPartOf(agcmdmnKey, 19);
  	public FixedLengthStringData agcmdmnRider = new FixedLengthStringData(2).isAPartOf(agcmdmnKey, 21);
  	public PackedDecimalData agcmdmnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmdmnKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmdmnKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmdmnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmdmnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}