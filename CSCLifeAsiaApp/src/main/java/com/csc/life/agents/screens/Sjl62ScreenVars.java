package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl62protect;
import com.csc.life.agents.screens.Sjl62screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;


public class Sjl62ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(695);
	public FixedLengthStringData dataFields = new FixedLengthStringData(343).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData aclntsel = DD.clntsel.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData acltname = DD.cltname.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData agncynum = DD.agncynum.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData regnum = DD.agncyRegnum.copy().isAPartOf(dataFields,263);
	public ZonedDecimalData srdate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,276);
	public ZonedDecimalData endate = DD.enddate.copyToZonedDecimal().isAPartOf(dataFields,284);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(dataFields,292);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(dataFields, 293);
	public FixedLengthStringData agtdesc = DD.agtydesc.copy().isAPartOf(dataFields, 313);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 343);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData aclntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData acltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData agncynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData endateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData agtdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 431); 
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] aclntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] acltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] agncynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] endateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] agtdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(246);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(147).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData status = DD.salestatus.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData regclass = DD.brnch.copy().isAPartOf(subfileFields,11); 
	public ZonedDecimalData regdate = DD.occdate.copyToZonedDecimal().isAPartOf(subfileFields,15);
	public FixedLengthStringData reasonreg = DD.reasoncd.copy().isAPartOf(subfileFields,23);
	public FixedLengthStringData resndetl = DD.resndetl.copy().isAPartOf(subfileFields,27);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 147);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData statusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData regclassErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData regdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData reasonregErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData resndetlErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 171);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] statusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] regclassOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] regdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] reasonregOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] resndetlOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 243);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	public FixedLengthStringData srdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData endateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData regdateDisp = new FixedLengthStringData(10);
	
	public LongData Sjl62screensflWritten = new LongData(0);
	public LongData Sjl62screenctlWritten = new LongData(0);
	public LongData Sjl62screenWritten = new LongData(0);
	public LongData Sjl62protectWritten = new LongData(0);
	public GeneralTable sjl62screensfl = new GeneralTable(AppVars.getInstance());
	
	public boolean hasSubfile() {
		return false;
	}

	public Sjl62ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		
		fieldIndMap.put(clntselOut,
				new String[] { "01", "02" , "-01", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "03", "04" , "-03", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "05", "06" , "-05", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "07", "08" , "-07", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "09", "10" , "-09", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "11", "12" , "-11", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "13", "14" , "-13", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "15", "16" , "-15", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypeOut,
				new String[] { "17", "18" , "-17", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldesOut,
				new String[] { "19", "20" , "-19", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "21", "22" , "-21", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "23", "24" , "-23", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "25", "26" , "-25", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(sltOut,
				new String[] { "27", "28" , "-27", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(aclntselOut,
				new String[] { "29", "30" , "-29", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(acltnameOut,
				new String[] { "31", "32" , "-31", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(agncynumOut,
				new String[] { "33", "34" , "-33", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(regnumOut,
				new String[] { "35", "36" , "-35", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(srdateOut,
				new String[] { "37", "38" , "-37", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(endateOut,
				new String[] { "39", "40" , "-39", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(validflagOut,
				new String[] { "41", "42" , "-41", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(useridOut,
				new String[] { "43", "44" , "-43", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(agtdescOut,
				new String[] { "45", "46" , "-45", null , null, null, null, null, null, null, null, null});
		
		
		screenFields = new BaseData[] { clntsel, cltname, company, brnchcd, brnchdesc, aracde, aradesc, levelno, leveltype, leveldes, saledept, saledptdes, agtype, 
										aclntsel, acltname, agncynum, regnum, srdate, endate, validflag, userid, agtdesc };
		screenOutFields = new BaseData[][] { clntselOut, cltnameOut, companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, levelnoOut, leveltypeOut, leveldesOut, 
											 saledeptOut, saledptdesOut, agtypeOut, aclntselOut, acltnameOut, agncynumOut, regnumOut, srdateOut, endateOut, validflagOut, useridOut, agtdescOut };
		screenErrFields = new BaseData[] { clntselErr, cltnameErr, companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, levelnoErr, leveltypeErr, leveldesErr, 
											 saledeptErr, saledptdesErr, agtypeErr, aclntselErr, acltnameErr, agncynumErr, regnumErr, srdateErr, endateErr, validflagErr, useridErr, agtdescErr };
						
		screenSflFields = new BaseData[] {slt, status, regclass, regdate, reasonreg, resndetl };
		screenSflOutFields = new BaseData[][] {sltOut, statusOut, regclassOut, regdateOut, reasonregOut, resndetlOut };
		screenSflErrFields = new BaseData[] {sltErr, statusErr, regclassErr, regdateErr, reasonregErr, resndetlErr };
		
		screenDateFields = new BaseData[] {srdate, endate};
		screenDateErrFields = new BaseData[] {srdateErr, endateErr};
		screenDateDispFields = new BaseData[] {srdateDisp, endateDisp};
		screenSflDateFields = new BaseData[] {regdate};
		screenSflDateErrFields = new BaseData[] {regdateErr};
		screenSflDateDispFields = new BaseData[] {regdateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorSflInds = errorSubfile;
		screenSflRecord = Sjl62screensfl.class;
		screenCtlRecord = Sjl62screenctl.class;
		initialiseSubfileArea();
		screenRecord = Sjl62screen.class;
		protectRecord = Sjl62protect.class;
		}
		
		public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl62screenctl.lrec.pageSubfile);
		}
}
