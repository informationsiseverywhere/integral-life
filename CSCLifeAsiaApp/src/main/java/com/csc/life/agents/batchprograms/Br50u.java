/*
 * File: Br50u.java
 * Date: December 3, 2013 2:08:30 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR50U.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.accounting.procedures.Glsubst;
import com.csc.fsu.accounting.recordstructures.Glsubstrec;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.financials.dataaccess.PreqTableDAM;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Chkbkcd;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Chkbkcdrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.life.agents.dataaccess.AgcsTableDAM;
import com.csc.life.agents.dataaccess.AgptpfTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.TatwTableDAM;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.agents.tablestructures.T6657rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.AgpypfSearchDAO;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.AgypfSearchDAOImpl;
import com.csc.smart400framework.dataaccess.model.Agpypf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   New Agent Batch Payments
*
*   This program will be used in batch L2NEWAGTPY to repalce B5403
*   which is used in old batch L2AGENTPAY and L2AGORSPAY.
*   The basic procedure division logic is to process the records
*   selected into Temporary Extract File AGPTPF.After post payment 
*   AGPY record will be deleted.
*
*   Initialise
*     - initialize parameters.
*
*    Read
*     - read file AGPTPF.
*
*    Perform  until End of File
*
*      Edit
*       - Get related agent infos.
*
*      Update
*       - Enquiry AGPYAGT and accumulate original amounts for all
*         currencies.
*       - Create cheque.
*       - Create payment header and dissections.
*       - Delete AGPY records
*
*      Read next file record
*
*    End Perform
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   The  following  control  totals are maintained within this
*  program:
*
*            01 - Agents - Manual Cheque
*            02 - Pay Amount - Manual Cheque
*            03 - Agents - Computer Cheque
*            04 - Pay Amount - Computer Cheque
*            05 - Agents - Bank Transfer
*            06 - Pay Amount - Bank Transfer
*            07 - Requistions created
*            08 - Requistions amount
*            09 - Extract records written
*            10 - Extract amount written
*            11 - No. of Headers written
*            12 - Total of Headers written
*            13 - Total number of agent payment not created
*            14 - Total amount of agent payment not created
*            15 - Total withholding tax amount
*            16 - Total TDEDUCT amount
*            17 - Total COLLTR amount
*            18 - Total TDCCHRG amount
*            19 - No. extract recs deleted
*            20 - No. AGCS record write
*            21 - Amount for Consolidate
*
****************************************************************** ****
* </pre>
*/
public class Br50u extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AgptpfTableDAM agptpf = new AgptpfTableDAM();
	private AgptpfTableDAM agptpfRec = new AgptpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR50U");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaAgptFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAgptFn, 0, FILLER).init("AGPT");
	private FixedLengthStringData wsaaAgptRunid = new FixedLengthStringData(2).isAPartOf(wsaaAgptFn, 4);
	private ZonedDecimalData wsaaAgptJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAgptFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaTransDesc680 = new FixedLengthStringData(30).init(SPACES);
	private static final String wsaaLongconfname = "";

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGenlpfx = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGenlcoy = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGenlcur = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlcode = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private PackedDecimalData wsaaTotalAgntComm = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotOrigAmt = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaOrigamt = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDissAmount = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalDeduct = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaLifaTotOrig = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaLifaTotAcct = new PackedDecimalData(15, 2).init(ZERO);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaCommTotalArray = new FixedLengthStringData(540);
	private FixedLengthStringData[] wsaaCommTotal = FLSArrayPartOfStructure(20, 27, wsaaCommTotalArray, 0);
	private FixedLengthStringData[] wsaaCommSacscode = FLSDArrayPartOfArrayStructure(2, wsaaCommTotal, 0);
	private FixedLengthStringData[] wsaaCommSacstyp = FLSDArrayPartOfArrayStructure(2, wsaaCommTotal, 2);
	private FixedLengthStringData[] wsaaCommGlcode = FLSDArrayPartOfArrayStructure(14, wsaaCommTotal, 4);
	private PackedDecimalData[] wsaaCommOrigamt = PDArrayPartOfArrayStructure(17, 2, wsaaCommTotal, 18);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaRdockey = new FixedLengthStringData(9);

	private FixedLengthStringData wsaaNewCurr = new FixedLengthStringData(1).init("Y");
	private Validator newCurrency = new Validator(wsaaNewCurr, "Y");

	private ZonedDecimalData wsaaProcessLevel = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private Validator endOfAgent = new Validator(wsaaProcessLevel, new ValueRange(2,3));

	private FixedLengthStringData wsaaBankTran = new FixedLengthStringData(1).init("N");
	private Validator bankTran = new Validator(wsaaBankTran, "Y");

	private FixedLengthStringData wsaaAutoCheq = new FixedLengthStringData(1).init("N");
	private Validator autoCheq = new Validator(wsaaAutoCheq, "Y");

	private FixedLengthStringData wsaaManCheq = new FixedLengthStringData(1).init("N");
	private Validator manCheq = new Validator(wsaaManCheq, "Y");

	private FixedLengthStringData wsaaOnAcct = new FixedLengthStringData(1).init("N");
	private Validator onAcct = new Validator(wsaaOnAcct, "Y");

	private FixedLengthStringData wsaaPaymthMissing = new FixedLengthStringData(1).init("N");
	private Validator paymthMissing = new Validator(wsaaPaymthMissing, "Y");
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String e141 = "E141";
	private static final String e192 = "E192";
	private static final String e193 = "E193";
	private static final String e289 = "E289";
	private static final String g447 = "G447";
		/* TABLES */
	private static final String t6657 = "T6657";
	private static final String t5690 = "T5690";
	private static final String t5645 = "T5645";
	private static final String t3688 = "T3688";
	private static final String t3672 = "T3672";
	private static final String t3629 = "T3629";
	private static final String t1688 = "T1688";
	private static final String t3583 = "T3583";
	private static final String tr393 = "TR393";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData commIndex = new IntegerData();
	private AgcsTableDAM agcsIO = new AgcsTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	//private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PreqTableDAM preqIO = new PreqTableDAM();
	private TatwTableDAM tatwIO = new TatwTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5690rec t5690rec = new T5690rec();
	private T3629rec t3629rec = new T3629rec();
	private T3672rec t3672rec = new T3672rec();
	private T3688rec t3688rec = new T3688rec();
	private T6657rec t6657rec = new T6657rec();
	private Tr393rec tr393rec = new Tr393rec();
	private Itemkey wsaaItemkey = new Itemkey();
	private Upperrec upperrec = new Upperrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Alocnorec alocnorec = new Alocnorec();
	private Chkbkcdrec chkbkcdrec = new Chkbkcdrec();
	private Glkeyrec glkeyrec = new Glkeyrec();
	private Glsubstrec glsubstrec = new Glsubstrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaAgptInner wsaaAgptInner = new WsaaAgptInner();
	
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Descpf>  t3583List;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Map<String, List<Itempf>> T5645ListMap;
	private Map<String, List<Itempf>> T5690ListMap;
	private Map<String, List<Itempf>> T3672ListMap;
	private Map<String, List<Itempf>> t6657ListMap;
	private Map<String, List<Itempf>> t3688ListMap;
	private Map<String, List<Itempf>> tr393ListMap;
	private FixedLengthStringData sDesc = new FixedLengthStringData(20);
	private AgpypfSearchDAO agpypfDAO =  getApplicationContext().getBean("agpypfsearchDAO", AgpypfSearchDAO.class);
	List<Agpypf> agpypfList=new ArrayList<>();
	private Agpypf agpypf = null;

	 
	 
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		read3110, 
		para3220, 
		read3230, 
		reRead3872, 
		exit3879, 
		start3961, 
		exit3969, 
		callAgpyagtio3990
	}

	public Br50u() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		varcom.vrcmTranid.set(batcdorrec.tranid);
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of AGPTPF.*/
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaAgptRunid.set(bprdIO.getSystemParam04());
		wsaaAgptJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(AGPTPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaAgptFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		agptpf.openInput();
		wsaaDate.set(getCobolDate());
		wsaaTime.set(getCobolTime());
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		readT56451200();
		gtdsLongdesc1300();
	}

protected void readT56451200()
	{
		//para1210();
		//readT56451220();
		
		T5645ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), t5645);
		String keyItemitem = wsaaProg.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (T5645ListMap.containsKey(keyItemitem.trim())){	
			itempfList = T5645ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					 
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					 
				}else{
					t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
	}

protected void para1210()
	{
		//itemIO.setDataKey(SPACES);
		//itemIO.setItempfx("IT");
		//itemIO.setItemcoy(bsprIO.getCompany());
		//itemIO.setItemtabl(t5645);
		//itemIO.setItemitem(wsaaProg);
		//itemIO.setFunction(varcom.readr);
	}

protected void readT56451220()
	{
		//SmartFileCode.execute(appVars, itemIO);
		//if (isNE(itemIO.getStatuz(), varcom.oK)) {
			//syserrrec.params.set(itemIO.getParams());
			//syserrrec.statuz.set(itemIO.getStatuz());
			//fatalError600();
		//}
		//t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void gtdsLongdesc1300()
	{
		para1310();
		check1320();
	}

protected void para1310()
	{
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(bsprIO.getCompany());
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(bprdIO.getAuthCode());
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
	}

protected void check1320()
	{
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFile2010();
	}

protected void readFile2010()
	{
		agptpf.readNext(agptpfRec);
		if (agptpf.isAtEnd()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*    No of records read*/
		/*    MOVE CT11                   TO CONT-TOTNO.                   */
		/*    MOVE 1                      TO CONT-TOTVAL.                  */
		/*    PERFORM 001-CALL-CONTOT.                                     */
		wsaaAgptInner.agptRldgcoy.set(agptpfRec.rldgcoy);
		wsaaAgptInner.agptRldgacct.set(agptpfRec.rldgacct);
		wsaaAgptInner.agptSacscurbal.set(agptpfRec.sacscurbal);
		wsaaAgptInner.agptCurrcode.set(agptpfRec.currcode);
		wsaaAgptInner.agptAgccqind.set(agptpfRec.agccqind);
		wsaaAgptInner.agptPayclt.set(agptpfRec.payclt);
		wsaaAgptInner.agptFacthous.set(agptpfRec.facthous);
		wsaaAgptInner.agptBankkey.set(agptpfRec.bankkey);
		wsaaAgptInner.agptBankacckey.set(agptpfRec.bankacckey);
		wsaaAgptInner.agptWhtax.set(agptpfRec.whtax);
		wsaaAgptInner.agptTcolbal.set(agptpfRec.tcolbal);
		wsaaAgptInner.agptTdeduct.set(agptpfRec.tdeduct);
		wsaaAgptInner.agptTdcchrg.set(agptpfRec.tdcchrg);
		wsaaAgptInner.agptPaymth.set(agptpfRec.paymth);
	}

protected void edit2500()
	{
		read2510();
	}

protected void read2510()
	{
		wsspEdterror.set(varcom.oK);
		agntIO.setAgntnum(wsaaAgptInner.agptRldgacct);
		agntIO.setAgntcoy(wsaaAgptInner.agptRldgcoy);
		agntIO.setAgntpfx("AG");
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agntIO.getParams());
			syserrrec.statuz.set(agntIO.getStatuz());
			fatalError600();
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(agntIO.getClntcoy());
		cltsIO.setClntnum(agntIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		getAgentPayMethod2600();
		if (paymthMissing.isTrue()) {
			return ;
		}
		wsaaLifaTotOrig.set(ZERO);
		wsaaTotalDeduct.set(ZERO);
		wsaaLifaTotAcct.set(ZERO);
		wsaaProcessLevel.set(ZERO);
		wsaaCommTotalArray.set(SPACES);
		for (commIndex.set(1); !(isGT(commIndex, 20)); commIndex.add(1)){
			wsaaCommOrigamt[commIndex.toInt()].set(0);
		}
		wsaaNewCurr.set("N");
		//agpyagtIO.setRldgcoy(bsprIO.getCompany());
		//agpyagtIO.setRldgacct(wsaaAgptInner.agptRldgacct);
		//agpyagtIO.setOrigcurr(SPACES);
	}

protected void getAgentPayMethod2600()
	{
		start2610();
		checkPayDetls2620();
	}

protected void start2610()
	{
		wsaaBankTran.set("N");
		wsaaAutoCheq.set("N");
		wsaaManCheq.set("N");
		wsaaOnAcct.set("N");
	}

protected void checkPayDetls2620()
	{
		/*    Using the T5690 XDS, access T3672 for further details about*/
		/*    the payment type found on T5690.*/
	/*	itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5690);
		itemIO.setItemitem(wsaaAgptInner.agptPaymth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		*/
		//san
		
		T5690ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), t5690);
		String keyItemitem = wsaaAgptInner.agptPaymth.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (T5690ListMap.containsKey(keyItemitem.trim())){	
			itempfList = T5690ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0)
				{
						t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
				}
				else
				{
						t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		
		
		//t5690rec.t5690Rec.set(itemIO.getGenarea());
		//itemIO.setItemcoy(bsprIO.getCompany());
		//itemIO.setItemtabl(t3672);
		//itemIO.setItemitem(t5690rec.paymentMethod);
		//itemIO.setItemseq(SPACES);
		//itemIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, itemIO);
		//if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			//conlogrec.error.set(g447);
			//conlogrec.params.set(wsaaAgptInner.agptRldgacct);
			//callConlog003();
			//wsaaPaymthMissing.set("Y");
			//return ;
		//}
		//if (isNE(itemIO.getStatuz(), varcom.oK)) {
			//syserrrec.params.set(itemIO.getParams());
			//syserrrec.statuz.set(itemIO.getStatuz());
			//fatalError600();
		//}
		//t3672rec.t3672Rec.set(itemIO.getGenarea());
		
		T3672ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), t3672);
		keyItemitem = t5690rec.paymentMethod.toString();
		itempfList = new ArrayList<Itempf>();
		itemFound = false;
		if (T3672ListMap.containsKey(keyItemitem.trim())){	
			itempfList = T3672ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					 
					t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					 
				}else{
					t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) {
			conlogrec.error.set(g447);
			conlogrec.params.set(wsaaAgptInner.agptRldgacct);
			callConlog003();
			wsaaPaymthMissing.set("Y");
			return ;
		}
		
		
		if (isEQ(t3672rec.onactflg, "Y")) {
			wsaaOnAcct.set("Y");
		}
		else {
			if (isEQ(t3672rec.bankaccreq, "Y")) {
				wsaaBankTran.set("Y");
			}
			else {
				if (isEQ(t3672rec.chqNumReq, "Y")
				&& isEQ(t3672rec.immPostInd, "N")) {
					wsaaManCheq.set("Y");
				}
				else {
					wsaaAutoCheq.set("Y");
				}
			}
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		/*  If first time begin at Agent and store details.*/
		bgnNewAgnt3100();
		for (Agpypf agpf : agpypfList) 
		 {      
			tranPerAgnt3200(agpf);
			//createPaymentHeader3800(agpf);
		}
		
		createCheqRec3700();
		createPaymentHeader3800();
		createPaymentDissec3900();
		deleteAgpy3990();
		/*EXIT*/
	}

protected void bgnNewAgnt3100()
	{
	/*	GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					process3100();
				case read3110: 
					read3110();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
	
		 }*/
	agpypfList = agpypfDAO.loadDataByAgentCriteria(bsprIO.getCompany().toString().trim(), wsaaAgptInner.agptRldgacct.toString());

	}
protected void process3100()
	{
		//agpyagtIO.setFunction(varcom.begn);
		//agpyagtIO.setFormat(formatsInner.agpyagtrec);
	}

protected void read3110()
	{
		//SmartFileCode.execute(appVars, agpyagtIO);
		//if (isNE(agpyagtIO.getStatuz(), varcom.oK)
		//&& isNE(agpyagtIO.getStatuz(), varcom.endp)) {
			//syserrrec.params.set(agpyagtIO.getParams());
			//syserrrec.statuz.set(agpyagtIO.getStatuz());
			//fatalError600();
		//}
		//if (isEQ(agpyagtIO.getStatuz(), varcom.endp)
		//|| isNE(agpyagtIO.getRldgcoy(), bsprIO.getCompany())
		//|| isNE(agpyagtIO.getRldgacct(), wsaaAgptInner.agptRldgacct)) {
			//wsaaProcessLevel.set(2);
			//return ;
		//}
		/* Skip Commission Effective Date > BSSC-EFFECTIVE-DATE*/
		//if (isGT(agpyagtIO.getEffdate(), bsscIO.getEffectiveDate())) {
			//agpyagtIO.setFunction(varcom.nextr);
			//goTo(GotoLabel.read3110);
		//}
		//wsaaProcessLevel.set(0);
		//wsaaNewCurr.set("Y");
	}

protected void tranPerAgnt3200(Agpypf agpf)
	{
		if (isLTE(agpf.getEffdate(), bsscIO.getEffectiveDate())) 
		{
			para3210(agpf);			 
			para3220(agpf);			
		}
			 
		 
	}

	/**
	* <pre>
	***  Accumulate Original amounts for all currencies
	* </pre>
	*/
protected void para3210(Agpypf agpf)
	{
		if (isNE(agpf.getOrigcurr(), wsaaAgptInner.agptCurrcode)
		&& isNE(wsaaAgptInner.agptCurrcode, SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaOrigamt.set(agpf.getOrigamt());
			para3220(agpf);		
		}
		/*   Set up parameters for Currency conversion*/
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
		conlinkrec.currIn.set(agpf.getOrigcurr());
		conlinkrec.currOut.set(wsaaAgptInner.agptCurrcode);
		conlinkrec.amountIn.set(agpf.getOrigamt());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/*    MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/*    MOVE AGPT-CURRCODE          TO ZRDP-CURRENCY.                */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, ZERO)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(wsaaAgptInner.agptCurrcode);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaOrigamt.set(conlinkrec.amountOut);
	}

protected void para3220(Agpypf agpf)
	{
		currConvLdgrUpdate3300(agpf);
		acumCommPayable3400(agpf);
		/*    Control Total 9 - Number of extract records written*/
		contotrec.totno.set(controlTotalsInner.ct09);
		contotrec.totval.set(1);
		callContot001();
		/*    Control Total 10 - Amount of extract records written.*/
		contotrec.totno.set(controlTotalsInner.ct10);
		contotrec.totval.set(wsaaOrigamt);
		callContot001();
	}

protected void read3230()
	{
		//agpyagtIO.setFunction(varcom.nextr);
		//SmartFileCode.execute(appVars, agpyagtIO);
		//if (isNE(agpyagtIO.getStatuz(), varcom.oK)
		//&& isNE(agpyagtIO.getStatuz(), varcom.endp)) {
			//syserrrec.params.set(agpyagtIO.getParams());
			//syserrrec.statuz.set(agpyagtIO.getStatuz());
			//fatalError600();
		//}
		//if (isEQ(agpyagtIO.getStatuz(), varcom.endp)
	//	|| isNE(agpyagtIO.getRldgcoy(), bsprIO.getCompany())
		//|| isNE(agpyagtIO.getRldgacct(), wsaaAgptInner.agptRldgacct)) {
			//wsaaProcessLevel.set(2);
		//	return ;
		//}
		/* Skip Commission Effective Date > BSSC-EFFECTIVE-DATE*/
		//if (isGT(agpyagtIO.getEffdate(), bsscIO.getEffectiveDate())) {
			//goTo(GotoLabel.read3230);
		//}
	}

protected void currConvLdgrUpdate3300(Agpypf agpf)
	{
		para3300(agpf);
	}

protected void para3300(Agpypf agpf)
	{
		/* Currency Conversion will not be performed in all cases. The*/
		/* following is list of entries that should be produced :*/
		/* Agent's Pay Currency = Contract Currency*/
		/* e.g.*/
		/*                         DR            CR*/
		/* GBP      LA  IC      INITCOMMPAY                    (1)*/
		/* GBP      LA  PD                    COMMPAID         (2)*/
		/* Agent's Pay Currency = GBP*/
		/* Contract Currency    = USD*/
		/* This is a multi-currency contract so currency conversion*/
		/* entries have to be written.*/
		/* e.g.*/
		/*                         DR            CR*/
		/* GBP      LA  IC      INITCOMMPAY                      (3)*/
		/* GBP      LA  CY                    CURRCONV           (4)*/
		/* USD      LA  CY      CURRCONV                         (5)*/
		/* USD      LA  PD                    COMMPAID           (6)*/
		/* Debit the Commission payable account. This will be the*/
		/* reversal of the original ACMV. This is entry (1) and (3) in*/
		/* the above example.*/
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.rldgcoy.set(agpf.getRldgcoy());
		lifacmvrec.genlcoy.set(agpf.getGenlcoy());
		lifacmvrec.batckey.set(batcdorrec.batchkey);
		lifacmvrec.rdocnum.set(agpf.getRdocnum());
		lifacmvrec.tranno.set(0);
		lifacmvrec.jrnseq.set(agpf.getJrnseq());
		lifacmvrec.sacscode.set(agpf.getSacscode());
		lifacmvrec.sacstyp.set(agpf.getSacstyp());
		lifacmvrec.glcode.set(agpf.getGlcode());
		if (isEQ(agpf.getGlsign(), "+")) {
			lifacmvrec.glsign.set("-");
		}
		else {
			lifacmvrec.glsign.set("+");
		}
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(agpf.getOrigcurr());
		lifacmvrec.rldgacct.set(agpf.getRldgacct());
		lifacmvrec.origamt.set(agpf.getOrigamt());
		lifacmvrec.acctamt.set(agpf.getAcctamt());
		lifacmvrec.genlcur.set(agpf.getGenlcur());
		lifacmvrec.crate.set(agpf.getCrate());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.postyear.set(batcdorrec.actyear);
		lifacmvrec.postmonth.set(batcdorrec.actmonth);
		lifacmvrec.trandesc.set(wsaaTransDesc680);
		lifacmvrec.tranref.set(agpf.getTranref());
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		lifacmvrec.user.set(varcom.vrcmUser);
		varcom.vrcmTranid.set(batcdorrec.tranid);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.substituteCode[1].set(t5690rec.paymentMethod);
		/*    MOVE AGPYAGT-BATCCOY        TO LIFA-BATCCOY.                 */
		/*    MOVE AGPYAGT-BATCBRN        TO LIFA-BATCBRN.                 */
		/*    MOVE AGPYAGT-BATCACTYR      TO LIFA-BATCACTYR.               */
		/*    MOVE AGPYAGT-BATCACTMN      TO LIFA-BATCACTMN.               */
		/*    MOVE AGPYAGT-BATCTRCDE      TO LIFA-BATCTRCDE.               */
		/*    MOVE AGPYAGT-BATCBATCH      TO LIFA-BATCBATCH.               */
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		/* Agent Currency = Contract Currency or Agent Currncy not*/
		/* specified (commission paid in contract currency).*/
		if (isEQ(agpf.getOrigcurr(), wsaaAgptInner.agptCurrcode)
		|| isEQ(wsaaAgptInner.agptCurrcode, SPACES)) {
			if (isEQ(lifacmvrec.glsign, "-")) {
				wsaaLifaTotOrig.subtract(lifacmvrec.origamt);
				wsaaLifaTotAcct.subtract(lifacmvrec.acctamt);
			}
			else {
				wsaaLifaTotOrig.add(lifacmvrec.origamt);
				wsaaLifaTotAcct.add(lifacmvrec.acctamt);
			}
			return ;
		}
		/* Multi-Currency*/
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO LIFA-ORIGAMT.                 */
		/* MOVE CLNK-AMOUNT-OUT        TO LIFA-RCAMT.                   */
		lifacmvrec.origamt.set(lifacmvrec.acctamt);
		lifacmvrec.rcamt.set(lifacmvrec.acctamt);
		/*    PERFORM 9100-KEEP-CHDR-CURR.*/
		lifacmvrec.origcurr.set(wsaaAgptInner.agptCurrcode);
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.statuz);
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			fatalError600();
		}
		if (isEQ(lifacmvrec.glsign, "-")) {
			wsaaLifaTotOrig.subtract(lifacmvrec.origamt);
			wsaaLifaTotAcct.subtract(lifacmvrec.acctamt);
		}
		else {
			wsaaLifaTotAcct.add(lifacmvrec.acctamt);
			wsaaLifaTotOrig.add(lifacmvrec.origamt);
		}
	}

protected void acumCommPayable3400(Agpypf agpf)
	{
		start3400(agpf);
	}

protected void start3400(Agpypf agpf)
	{
		commIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(commIndex, wsaaCommTotal.length); commIndex.add(1)){
				if (isEQ(wsaaCommSacscode[commIndex.toInt()], SPACES)) {
					if (isEQ(agpf.getGlsign(), "-")) {
						compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt, -1));
					}
					wsaaCommOrigamt[commIndex.toInt()].set(wsaaOrigamt);
					wsaaCommSacscode[commIndex.toInt()].set(agpf.getSacscode());
					wsaaCommSacstyp[commIndex.toInt()].set(agpf.getSacstyp());
					wsaaCommGlcode[commIndex.toInt()].set(agpf.getGlcode());
					break searchlabel1;
				}
				if (isEQ(wsaaCommSacscode[commIndex.toInt()], agpf.getSacscode())
				&& isEQ(wsaaCommSacstyp[commIndex.toInt()], agpf.getSacstyp())) {
					if (isEQ(agpf.getGlsign(), "-")) {
						compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt, -1));
					}
					wsaaCommOrigamt[commIndex.toInt()].add(wsaaOrigamt);
					break searchlabel1;
				}
			}
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void createCheqRec3700()
	{
		para3700();
	}

protected void para3700()
	{
		wsaaTotalAgntComm.set(wsaaAgptInner.agptSacscurbal);
		tatwIO.setParams(SPACES);
		tatwIO.setOrigamt(wsaaTotalAgntComm);
		wsaaTotalAgntComm.subtract(wsaaAgptInner.agptWhtax);
		wsaaTotalAgntComm.subtract(wsaaAgptInner.agptTcolbal);
		wsaaTotalDeduct.set(wsaaAgptInner.agptWhtax);
		wsaaTotalDeduct.add(wsaaAgptInner.agptTcolbal);
		wsaaTotalDeduct.add(wsaaAgptInner.agptTdeduct);
		wsaaLifaTotOrig.subtract(wsaaTotalDeduct);
		wsaaLifaTotAcct.subtract(wsaaTotalDeduct);
		if (isNE(wsaaAgptInner.agptWhtax, ZERO)) {
			moveToLifa9100();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.statuz);
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
			tatwIO.setAgntcoy(wsaaAgptInner.agptRldgcoy);
			tatwIO.setAgntnum(wsaaAgptInner.agptRldgacct);
			tatwIO.setEffdate(bsscIO.getEffectiveDate());
			tatwIO.setValidflag("1");
			tatwIO.setOrigcurr(lifacmvrec.origcurr);
			tatwIO.setTaxamt(wsaaAgptInner.agptWhtax);
			tatwIO.setFormat(formatsInner.tatwrec);
			tatwIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, tatwIO);
			if (isNE(tatwIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(tatwIO.getParams());
				syserrrec.statuz.set(tatwIO.getStatuz());
				fatalError600();
			}
			contotrec.totno.set(controlTotalsInner.ct15);
			contotrec.totval.set(wsaaAgptInner.agptWhtax);
			callContot001();
		}
		if (isNE(wsaaAgptInner.agptTcolbal, ZERO)) {
			moveToLifa9200();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.statuz);
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
			contotrec.totno.set(controlTotalsInner.ct17);
			contotrec.totval.set(wsaaAgptInner.agptTcolbal);
			callContot001();
		}
		if (isNE(wsaaLifaTotOrig, ZERO)) {
			moveToLifa9300();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.statuz);
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
		}
		if (isNE(wsaaAgptInner.agptTdeduct, 0)) {
			contotrec.totno.set(controlTotalsInner.ct16);
			contotrec.totval.set(wsaaAgptInner.agptTdeduct);
			callContot001();
		}
		if (isNE(wsaaAgptInner.agptTdcchrg, ZERO)) {
			moveToLifa9400();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz, varcom.oK)) {
				syserrrec.params.set(lifacmvrec.statuz);
				syserrrec.params.set(lifacmvrec.lifacmvRec);
				syserrrec.statuz.set(lifacmvrec.statuz);
				fatalError600();
			}
			contotrec.totno.set(controlTotalsInner.ct18);
			contotrec.totval.set(wsaaAgptInner.agptTdcchrg);
			callContot001();
			wsaaTotalAgntComm.subtract(wsaaAgptInner.agptTdcchrg);
		}
		if (isNE(wsaaTotalAgntComm, ZERO)) {
			zrdecplrec.amountIn.set(wsaaTotalAgntComm);
			zrdecplrec.currency.set(wsaaAgptInner.agptCurrcode);
			a000CallRounding();
			wsaaTotalAgntComm.set(zrdecplrec.amountOut);
		}
		/* Control total no.13 is now the total amount of agent payments*/
		/* which are each less than 1 currency unit and hence do not have*/
		/* payments created for them; no.14 is the total number of agents*/
		/* whose commission each totals less than 1 unit.*/
		if (isLT(wsaaTotalAgntComm, 1)
		&& isNE(wsaaTotalAgntComm, 0)) {
			contotrec.totno.set(controlTotalsInner.ct13);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(controlTotalsInner.ct14);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
			return ;
		}
		/*   Bypass the rest of instructions when Comm is 0*/
		/*   in order to avoid creating a record in CHEQPF.*/
		if (isEQ(wsaaTotalAgntComm, 0)) {
			return ;
		}
		if (isEQ(wsaaAgptInner.agptAgccqind, "Y")
		&& isNE(wsaaAgptInner.agptPayclt, SPACES)) {
			return ;
		}
		cheqIO.setParams(SPACES);
		if (manCheq.isTrue()) {
			/*    Control total No. 1 - Number of Manual Cheque Requisitions*/
			contotrec.totno.set(controlTotalsInner.ct01);
			contotrec.totval.set(1);
			callContot001();
			/*    Control total No. 2 - Total amount of Man cheq requisitions*/
			contotrec.totno.set(controlTotalsInner.ct02);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		if (autoCheq.isTrue()) {
			/*    Control total No. 3 - Number of Cheque Requisitions*/
			contotrec.totno.set(controlTotalsInner.ct03);
			contotrec.totval.set(1);
			callContot001();
			/*    Control total No. 4 - Total amount of cheque requisitions*/
			contotrec.totno.set(controlTotalsInner.ct04);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		if (bankTran.isTrue()) {
			/*    Control total No. 5 - Number of bank transfers created*/
			contotrec.totno.set(controlTotalsInner.ct05);
			contotrec.totval.set(1);
			callContot001();
			/*    Control total No. 6 - Total amount of bank transfers*/
			contotrec.totno.set(controlTotalsInner.ct06);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		//itemIO.setDataKey(SPACES);
		//itemIO.setItemcoy(bsprIO.getCompany());
		//itemIO.setItemtabl(t6657);
		//itemIO.setItempfx("IT");
		//if (isNE(wsaaAgptInner.agptCurrcode, SPACES)) {
			//itemIO.setItemitem(wsaaAgptInner.agptCurrcode);
		//}
		//else {
			//itemIO.setItemitem(agpyagtIO.getOrigcurr());
		//}
		
		
		t6657ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), t6657);
		String keyItemitem;
		if (isNE(wsaaAgptInner.agptCurrcode, SPACES)) 
		{
			keyItemitem = wsaaAgptInner.agptCurrcode.toString();
		}
		else 
		{
			keyItemitem=agpyagtIO.getOrigcurr().toString();
		}
	 
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t6657ListMap.containsKey(keyItemitem.trim())){	
			itempfList = t6657ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					 
					t6657rec.t6657Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					 
				}else{
					t6657rec.t6657Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		
		//itemIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, itemIO);
		//if (isNE(itemIO.getStatuz(), varcom.oK)) {
			//syserrrec.params.set(itemIO.getParams());
			//syserrrec.statuz.set(itemIO.getStatuz());
			//fatalError600();
		//}
		//t6657rec.t6657Rec.set(itemIO.getGenarea());
		cheqIO.setReqnbcde(t6657rec.bankcode);
		//itemIO.setItempfx("IT");
		//itemIO.setItemcoy(bsprIO.getCompany());
		//itemIO.setItemtabl(t3688);
		//itemIO.setItemitem(t6657rec.bankcode);
		//itemIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, itemIO);
		//if (isNE(itemIO.getStatuz(), varcom.oK)) {
			//syserrrec.params.set(itemIO.getParams());
			//fatalError600();
		//}
		//t3688rec.t3688Rec.set(itemIO.getGenarea());
		
		
		t3688ListMap = itemDAO.loadSmartTable("IT", bsprIO.getCompany().toString(), t3688);
		keyItemitem=t6657rec.bankcode.toString();
		itempfList = new ArrayList<Itempf>();
		itemFound = false;
		if (t3688ListMap.containsKey(keyItemitem.trim())){	
			itempfList = t3688ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0){
					 
					t3688rec.t3688Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;
					 
				}else{
					t3688rec.t3688Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		 	
		if (!itemFound) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		
		cheqIO.setPaycurr(t3688rec.bcurr);
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set(fsupfxcpy.reqn);
		alocnorec.company.set(bsprIO.getCompany());
		alocnorec.genkey.set(SPACES);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, varcom.oK)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		cheqIO.setReqnno(alocnorec.alocNo);
		cheqIO.setReqncoy(bsprIO.getCompany());
		cheqIO.setClntcoy(agntIO.getClntcoy());
		/* Check for existing PAYEE/CLIENT details & use if found.*/
		if (isNE(wsaaAgptInner.agptPayclt, SPACES)) {
			cheqIO.setClntnum01(wsaaAgptInner.agptPayclt);
		}
		else {
			cheqIO.setClntnum01(agntIO.getClntnum());
		}
		/* If the pay client is spaces default the requistion client to*/
		/* the client number defining the agent.*/
		if (isEQ(wsaaAgptInner.agptPayclt, SPACES)) {
			cheqIO.setClntnum01(cltsIO.getClntnum());
		}
		if ((isEQ(varcom.vrcmUser, ZERO))
		|| (isEQ(varcom.vrcmUser, SPACES))) {
			cheqIO.setUser(ZERO);
			cheqIO.setAuthuser(ZERO);
		}
		else {
			cheqIO.setUser(varcom.vrcmUser);
			cheqIO.setAuthuser(varcom.vrcmUser);
		}
		cheqIO.setClttype01(cltsIO.getClttype());
		readTr3933750();
		if (isEQ(cltsIO.getClttype(), "C")) {
			cheqIO.setSurnam01(wsaaLongconfname);
			cheqIO.setSalNInit01(SPACES);
		}
		else {
			cheqIO.setSurnam01(cltsIO.getSurname());
			if (isEQ(tr393rec.salutind, "S")) {
				sDesc.set(SPACES);
				descT35833770();
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(sDesc, "  ");
				stringVariable1.addExpression(" ", "  ");
				stringVariable1.addExpression(cltsIO.getInitials(), "  ");
				stringVariable1.setStringInto(cheqIO.getSalNInit01());
			}
			else {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
				stringVariable2.addExpression(" ", "  ");
				stringVariable2.addExpression(cltsIO.getInitials(), "  ");
				stringVariable2.setStringInto(cheqIO.getSalNInit01());
			}
		}
		cheqIO.setSurnam01(cltsIO.getSurname());
		upperrec.name.set(cltsIO.getSurname());
		upperrec.statuz.set(SPACES);
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz, varcom.oK)) {
			cheqIO.setCapname(cltsIO.getSurname());
		}
		else {
			cheqIO.setCapname(upperrec.name);
		}
		cheqIO.setCheqdupn(SPACES);
		cheqIO.setRequser(varcom.vrcmUser);
		/* The following 2 fields are now filled a bit further down.*/
		cheqIO.setFacthous(wsaaAgptInner.agptFacthous);
		cheqIO.setBankkey(wsaaAgptInner.agptBankkey);
		cheqIO.setBankacckey(wsaaAgptInner.agptBankacckey);
		cheqIO.setAppruser(varcom.vrcmUser);
		cheqIO.setPaydate(bsscIO.getEffectiveDate());
		cheqIO.setBranch(batcdorrec.branch);
		cheqIO.setPresdate(ZERO);
		cheqIO.setStmtpage(ZERO);
		cheqIO.setArchdate(ZERO);
		cheqIO.setTransactionDate(wsaaDate);
		cheqIO.setAuthdate(bsscIO.getEffectiveDate());
		cheqIO.setApprdte(bsscIO.getEffectiveDate());
		cheqIO.setReqdate(bsscIO.getEffectiveDate());
		cheqIO.setTransactionTime(wsaaTime);
		cheqIO.setApprtime(wsaaTime);
		cheqIO.setReqtime(wsaaTime);
		cheqIO.setAuthtime(wsaaTime);
		cheqIO.setProcind("AU");
		cheqIO.setReqntype(t5690rec.paymentMethod);
		cheqIO.setPresflag("N");
		cheqIO.setPayamt(wsaaTotalAgntComm);
		cheqIO.setZprnno(ZERO);
		cheqIO.setFormat(formatsInner.cheqrec);
		cheqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, cheqIO);
		if (isNE(cheqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cheqIO.getParams());
			syserrrec.statuz.set(cheqIO.getStatuz());
			fatalError600();
		}
	}

protected void readTr3933750()
	{
		start3751();
	}

protected void start3751()
	{
		//itemIO.setParams(SPACES);
		//itemIO.setItempfx("IT");
		//itemIO.setItemcoy(bsprIO.getFsuco());
		//itemIO.setItemtabl(tr393);
		//itemIO.setItemitem(bsprIO.getFsuco());
		//itemIO.setFormat(formatsInner.itemrec);
		//itemIO.setFunction(varcom.readr);
		//SmartFileCode.execute(appVars, itemIO);
		//if (isNE(itemIO.getStatuz(), varcom.oK)
		//&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			//syserrrec.params.set(itemIO.getParams());
			//syserrrec.statuz.set(itemIO.getStatuz());
			//fatalError600();
		//}
		//tr393rec.tr393Rec.set(itemIO.getGenarea());
		
		
		tr393ListMap = itemDAO.loadSmartTable("IT", bsprIO.getFsuco().toString(), tr393);
		String keyItemitem=bsprIO.getFsuco().toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (tr393ListMap.containsKey(keyItemitem.trim()))
		{	
			itempfList = tr393ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) 
			{
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if(Integer.parseInt(itempf.getItmfrm().toString()) > 0)
				{
						tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));					
						itemFound = true;					 
				}
				else
				{
					tr393rec.tr393Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;					
				}				
			}		
		}
		
		if (!itemFound) 
		{
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		
	}

protected void descT35833770()
	{
		start3771();
	}

protected void start3771()
	{
		upperrec.upperRec.set(SPACES);
		upperrec.name.set(cltsIO.getSalutl());
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz, varcom.oK)) {
			wsaaItemitem.set(cltsIO.getSalutl());
		}
		else {
			wsaaItemitem.set(upperrec.name);
		}
		/*descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getFsuco());
		descIO.setDesctabl(t3583);
		descIO.setDescitem(wsaaItemitem);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}*/
		

		t3583List = descDAO.getItemByDescItem("IT", bsprIO.getFsuco().toString(), t3583, wsaaItemitem.toString().trim(),
				bsscIO.getLanguage().toString());/* IJTI-1523 */
		boolean itemFound = false;
		for (Descpf descItem : t3583List) {
			if (descItem.getDescitem().trim().equals(wsaaItemitem.toString().trim())  )
			{
				itemFound=true;
				sDesc.set(descItem.getShortdesc());
				break;
			}
		}
		 
	}

protected void createPaymentHeader3800()
	{
		para3800();
	}

protected void para3800()
	{
		if (isLT(wsaaTotalAgntComm, 1)) {
			return ;
		}
		if (isEQ(wsaaAgptInner.agptAgccqind, "Y")
		&& isNE(wsaaAgptInner.agptPayclt, SPACES)) {
			return ;
		}
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(batcdorrec.actmonth);
		preqIO.setPostyear(batcdorrec.actyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(bsprIO.getCompany());
		preqIO.setRdocnum(cheqIO.getReqnno());
		preqIO.setRldgcoy(bsprIO.getCompany());
		chkbkcdrec.function.set("REQNBANK");
		chkbkcdrec.bankcode.set(t6657rec.bankcode);
		chkbkcdrec.user.set(varcom.vrcmUser);
		chkbkcdrec.company.set(bsprIO.getCompany());
		callProgram(Chkbkcd.class, chkbkcdrec.chkbkcdRec);
		if (isNE(chkbkcdrec.statuz, varcom.oK)) {
			syserrrec.params.set(chkbkcdrec.chkbkcdRec);
			syserrrec.statuz.set(chkbkcdrec.statuz);
			fatalError600();
		}
		wsaaGenlkey.set(chkbkcdrec.genlkey);
		/* Restore the GLCODE details from CHKBKCD because for the*/
		/* specified bank code there is a designated GLCODE for it.*/
		preqIO.setGenlcoy(wsaaGenlcoy);
		preqIO.setGlcode(wsaaGlcode);
		preqIO.setGlsign(chkbkcdrec.sign);
		/*    Don't use BKCK GLCODE or SIGN. These must come from the*/
		/*    General Ledger Dissection table via GLKEY and GLSUBST.*/
		getGlcodeSignContot3850();
		preqIO.setCnttot(glkeyrec.contot);
		preqIO.setSacscode(t3672rec.sacscode);
		preqIO.setSacstyp(t3672rec.sacstyp);
		preqIO.setRldgacct(t6657rec.bankcode);
		if (isEQ(wsaaAgptInner.agptCurrcode, SPACES)) {
			preqIO.setOrigccy(agpypf.getOrigcurr());
		}
		else {
			preqIO.setOrigccy(wsaaAgptInner.agptCurrcode);
		}
		if (isNE(wsaaLastOrigCcy, preqIO.getOrigccy())) {
			getCurrencyRate3870();
		}
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate, wsaaTotalAgntComm));
		/*    MOVE PREQ-ACCTAMT           TO ZRDP-AMOUNT-IN.               */
		/*    MOVE PREQ-GENLCUR           TO ZRDP-CURRENCY.                */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO PREQ-ACCTAMT.                 */
		if (isNE(preqIO.getAcctamt(), ZERO)) {
			zrdecplrec.amountIn.set(preqIO.getAcctamt());
			zrdecplrec.currency.set(preqIO.getGenlcur());
			a000CallRounding();
			preqIO.setAcctamt(zrdecplrec.amountOut);
		}
		preqIO.setOrigamt(wsaaTotalAgntComm);
		wsaaJrnseq.set(ZERO);
		preqIO.setJrnseq(ZERO);
		preqIO.setEffdate(bsscIO.getEffectiveDate());
		preqIO.setTrandesc(cltsIO.getSurname());
		preqIO.setBranch(batcdorrec.branch);
		preqIO.setTaxcat(SPACES);
		preqIO.setFrcdate(ZERO);
		preqIO.setRcamt(ZERO);
		preqIO.setTransactionDate(wsaaDate);
		preqIO.setTransactionTime(wsaaTime);
		preqIO.setUser(varcom.vrcmUser);
		preqIO.setFormat(formatsInner.preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(preqIO.getParams());
			syserrrec.statuz.set(preqIO.getStatuz());
			fatalError600();
		}
		/*    Control total 11 - Number of header records written.         */
		contotrec.totno.set(controlTotalsInner.ct11);
		contotrec.totval.set(1);
		callContot001();
		/*    Control total 12 - Amount of header records written          */
		contotrec.totno.set(controlTotalsInner.ct12);
		contotrec.totval.set(wsaaTotalAgntComm);
		callContot001();
	}

protected void getGlcodeSignContot3850()
	{
		start3851();
	}

protected void start3851()
	{
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(bsprIO.getCompany());
		glkeyrec.trancode.set(bprdIO.getAuthCode());
		glkeyrec.sacscode.set(t3672rec.sacscode);
		glkeyrec.sacstyp.set(t3672rec.sacstyp);
		glkeyrec.ledgkey.set(SPACES);
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isEQ(glkeyrec.statuz, "NFND")) {
			glkeyrec.statuz.set(e141);
		}
		else {
			if (isEQ(glkeyrec.statuz, "MRNF")) {
				glkeyrec.statuz.set(e192);
			}
			else {
				if (isEQ(glkeyrec.statuz, "NCUR")) {
					glkeyrec.statuz.set(e193);
				}
				else {
					if (isEQ(glkeyrec.statuz, "BCTL")) {
						glkeyrec.statuz.set(e289);
					}
				}
			}
		}
		if (isNE(glkeyrec.statuz, varcom.oK)) {
			syserrrec.params.set(glkeyrec.glkeyRec);
			syserrrec.statuz.set(glkeyrec.statuz);
			fatalError600();
		}
		glsubstrec.glsubstRec.set(SPACES);
		glsubstrec.function.set("SBALL");
		glsubstrec.company.set(glkeyrec.company);
		glsubstrec.glacct.set(glkeyrec.genlAccount);
		glsubstrec.branch.set(batcdorrec.branch);
		glsubstrec.bankcode.set(t6657rec.bankcode);
		if (isEQ(wsaaAgptInner.agptCurrcode, SPACES)) {
			glsubstrec.currency.set(agpyagtIO.getOrigcurr());
		}
		else {
			glsubstrec.currency.set(wsaaAgptInner.agptCurrcode);
		}
		callProgram(Glsubst.class, glsubstrec.glsubstRec);
		if (isNE(glsubstrec.statuz, varcom.oK)) {
			syserrrec.params.set(glsubstrec.glsubstRec);
			syserrrec.statuz.set(glsubstrec.statuz);
			fatalError600();
		}
	}

protected void getCurrencyRate3870()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3870();
				case reRead3872: 
					reRead3872();
					findRate3875();
				case exit3879: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3870()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(preqIO.getOrigccy());
	}

protected void reRead3872()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), "****")) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLastOrigCcy.set(preqIO.getOrigccy());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate, ZERO)
		|| isGT(wsaaX, 7))) {
			findRate3875();
		}
		
		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.reRead3872);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}
		else {
			goTo(GotoLabel.exit3879);
		}
	}

protected void findRate3875()
	{
		if (isGTE(bsscIO.getEffectiveDate(), t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(bsscIO.getEffectiveDate(), t3629rec.todate[wsaaX.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void createPaymentDissec3900()
	{
		/*PARA*/
		if (isLT(wsaaTotalAgntComm, 1)) {
			return ;
		}
		for (commIndex.set(1); !(isEQ(wsaaCommSacscode[commIndex.toInt()], SPACES)
		|| isGT(commIndex, 20)); commIndex.add(1)){
			createDissec3960();
		}
		/*EXIT*/
	}

protected void createDissec3960()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3960();
				case start3961: 
					start3961();
				case exit3969: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3960()
	{
		if (isEQ(wsaaTotalDeduct, ZERO)) {
			goTo(GotoLabel.start3961);
		}
		compute(wsaaDissAmount, 2).set(mult(wsaaCommOrigamt[commIndex.toInt()], -1));
		if (isLTE(wsaaDissAmount, wsaaTotalDeduct)) {
			wsaaTotalDeduct.subtract(wsaaDissAmount);
			wsaaCommOrigamt[commIndex.toInt()].set(ZERO);
			goTo(GotoLabel.exit3969);
		}
		else {
			/*       Here, WSAA-COMM-ORIGAMT is negative value (credit amount).*/
			/*       Thus, we should use function ADD to deduct the Deduction*/
			/*       amount from WSAA-COMM-ORIGAMT.*/
			wsaaCommOrigamt[commIndex.toInt()].add(wsaaTotalDeduct);
			wsaaTotalDeduct.set(ZERO);
		}
	}

protected void start3961()
	{
		if (isEQ(wsaaAgptInner.agptAgccqind, "Y")
		&& isNE(wsaaAgptInner.agptPayclt, SPACES)) {
			createAgcs3980();
			return ;
		}
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(batcdorrec.actmonth);
		preqIO.setPostyear(batcdorrec.actyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(bsprIO.getCompany());
		preqIO.setRdocnum(cheqIO.getReqnno());
		preqIO.setRldgcoy(bsprIO.getCompany());
		preqIO.setRldgacct(agntIO.getAgntnum());
		if (isEQ(wsaaAgptInner.agptCurrcode, SPACES)) {
			preqIO.setOrigccy(agpyagtIO.getOrigcurr());
		}
		else {
			preqIO.setOrigccy(wsaaAgptInner.agptCurrcode);
		}
		preqIO.setSacscode(t5645rec.sacscode02);
		preqIO.setSacstyp(t5645rec.sacstype02);
		preqIO.setGlcode(t5645rec.glmap02);
		preqIO.setGlsign(t5645rec.sign02);
		preqIO.setCnttot(t5645rec.cnttot02);
		preqIO.setGenlcoy(bsprIO.getCompany());
		setPrecision(preqIO.getOrigamt(), 2);
		preqIO.setOrigamt(mult(wsaaCommOrigamt[commIndex.toInt()], -1));
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate, preqIO.getOrigamt()));
		/*    MOVE PREQ-ACCTAMT           TO ZRDP-AMOUNT-IN.               */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO PREQ-ACCTAMT.                 */
		if (isNE(preqIO.getAcctamt(), ZERO)) {
			zrdecplrec.amountIn.set(preqIO.getAcctamt());
			zrdecplrec.currency.set(preqIO.getGenlcur());
			a000CallRounding();
			preqIO.setAcctamt(zrdecplrec.amountOut);
		}
		wsaaJrnseq.add(1);
		preqIO.setJrnseq(wsaaJrnseq);
		preqIO.setEffdate(bsscIO.getEffectiveDate());
		preqIO.setBranch(batcdorrec.branch);
		preqIO.setTrandesc(cltsIO.getSurname());
		preqIO.setTaxcat(SPACES);
		preqIO.setRcamt(ZERO);
		preqIO.setFrcdate(ZERO);
		preqIO.setTransactionDate(wsaaDate);
		preqIO.setTransactionTime(wsaaTime);
		preqIO.setUser(varcom.vrcmUser);
		contotrec.totno.set(controlTotalsInner.ct07);
		contotrec.totval.set(1);
		callContot001();
		/*    Control total 8  - Total amount of requistions written*/
		contotrec.totno.set(controlTotalsInner.ct08);
		contotrec.totval.set(preqIO.getOrigamt());
		callContot001();
		preqIO.setFormat(formatsInner.preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(preqIO.getParams());
			syserrrec.statuz.set(preqIO.getStatuz());
			fatalError600();
		}
	}

protected void createAgcs3980()
	{
		create3981();
	}

protected void create3981()
	{
		agcsIO.setDataArea(SPACES);
		agcsIO.setAgntcoy(wsaaAgptInner.agptRldgcoy);
		agcsIO.setAgntnum(wsaaAgptInner.agptRldgacct);
		if (isNE(wsaaAgptInner.agptCurrcode, SPACES)) {
			agcsIO.setCurrcode(wsaaAgptInner.agptCurrcode);
		}
		else {
			agcsIO.setCurrcode(agpyagtIO.getOrigcurr());
		}
		agcsIO.setPayclt(wsaaAgptInner.agptPayclt);
		agcsIO.setSacscode(wsaaCommSacscode[commIndex.toInt()]);
		agcsIO.setSacstyp(wsaaCommSacstyp[commIndex.toInt()]);
		compute(wsaaCommOrigamt[commIndex.toInt()], 2).set(mult(wsaaCommOrigamt[commIndex.toInt()], -1));
		if (isNE(wsaaCommOrigamt[commIndex.toInt()], ZERO)) {
			zrdecplrec.amountIn.set(wsaaCommOrigamt[commIndex.toInt()]);
			zrdecplrec.currency.set(wsaaAgptInner.agptCurrcode);
			a000CallRounding();
			wsaaCommOrigamt[commIndex.toInt()].set(zrdecplrec.amountOut);
		}
		agcsIO.setPayamt(wsaaCommOrigamt[commIndex.toInt()]);
		agcsIO.setEffdate(bsscIO.getEffectiveDate());
		agcsIO.setFrcdate(varcom.vrcmMaxDate);
		agcsIO.setFunction(varcom.writr);
		agcsIO.setFormat(formatsInner.agcsrec);
		SmartFileCode.execute(appVars, agcsIO);
		if (isNE(agcsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcsIO.getParams());
			syserrrec.statuz.set(agcsIO.getStatuz());
			fatalError600();
		}
		/*    Control Total 20 - Number of AGCS record written             */
		contotrec.totno.set(controlTotalsInner.ct20);
		contotrec.totval.set(1);
		callContot001();
		/*    Control Total 21 - Amount for Consolidate                    */
		contotrec.totno.set(controlTotalsInner.ct21);
		contotrec.totval.set(agcsIO.getPayamt());
		callContot001();
	}

protected void deleteAgpy3990()
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3990();
				case callAgpyagtio3990: 
					callAgpyagtio3990();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}*/
	
	 
	
	int no_of_rec= agpypfDAO.deleteAgypTemp(wsaaAgptInner.agptRldgcoy.toString(), wsaaAgptInner.agptRldgacct.toString(),wsaaAgptInner.agptCurrcode.toString());
	
	}

protected void start3990()
	{
		//agpyagtIO.setParams(SPACES);
		//agpyagtIO.setRldgcoy(wsaaAgptInner.agptRldgcoy);
		//agpyagtIO.setRldgacct(wsaaAgptInner.agptRldgacct);
		//agpyagtIO.setOrigcurr(wsaaAgptInner.agptCurrcode);
		//agpyagtIO.setFormat(formatsInner.agpyagtrec);
		//ag/pyagtIO.setFunction(varcom.begn);
	}

protected void callAgpyagtio3990()
	{
		/*SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(), varcom.oK)
		&& isNE(agpyagtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agpyagtIO.getParams());
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agpyagtIO.getStatuz(), varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(), wsaaAgptInner.agptRldgcoy)
		|| isNE(agpyagtIO.getRldgacct(), wsaaAgptInner.agptRldgacct)
		|| isNE(agpyagtIO.getOrigcurr(), wsaaAgptInner.agptCurrcode)) {
			return ;
		}
		if (isGT(agpyagtIO.getEffdate(), bsscIO.getEffectiveDate())) {
			agpyagtIO.setFunction(varcom.nextr);
			goTo(GotoLabel.callAgpyagtio3990);
		}
		agpyagtIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agpyagtIO.getParams());
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			fatalError600();
		}
		contotrec.totno.set(controlTotalsInner.ct19);
		contotrec.totval.set(1);
		callContot001();
		agpyagtIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgpyagtio3990);*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close all files, and delete the override function*/
		agptpf.close();
		wsaaQcmdexc.set("DLTOVR FILE(AGPTPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void moveToLifa9100()
	{
		para9100();
	}

protected void para9100()
	{
		lifacmvrec.origamt.set(wsaaAgptInner.agptWhtax);
		lifacmvrec.acctamt.set(wsaaAgptInner.agptWhtax);
		lifacmvrec.rldgcoy.set(wsaaAgptInner.agptRldgcoy);
		lifacmvrec.rldgacct.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.rdocnum.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.tranref.set(SPACES);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	}

protected void moveToLifa9200()
	{
		para9200();
	}

protected void para9200()
	{
		lifacmvrec.origamt.set(wsaaAgptInner.agptTcolbal);
		lifacmvrec.acctamt.set(wsaaAgptInner.agptTcolbal);
		lifacmvrec.rldgcoy.set(wsaaAgptInner.agptRldgcoy);
		lifacmvrec.rldgacct.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.rdocnum.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.tranref.set(SPACES);
		lifacmvrec.glcode.set(t5645rec.glmap08);
		lifacmvrec.glsign.set(t5645rec.sign08);
		lifacmvrec.sacscode.set(t5645rec.sacscode08);
		lifacmvrec.sacstyp.set(t5645rec.sacstype08);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	}

protected void moveToLifa9300()
	{
		para9300();
	}

protected void para9300()
	{
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origamt.set(wsaaLifaTotOrig);
		lifacmvrec.acctamt.set(wsaaLifaTotAcct);
		lifacmvrec.rldgcoy.set(wsaaAgptInner.agptRldgcoy);
		lifacmvrec.rldgacct.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.rdocnum.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.rcamt.set(wsaaLifaTotOrig);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.tranref.set(SPACES);
	}

protected void moveToLifa9400()
	{
		param9400();
	}

protected void param9400()
	{
		lifacmvrec.glcode.set(t5645rec.glmap11);
		lifacmvrec.glsign.set(t5645rec.sign11);
		lifacmvrec.sacscode.set(t5645rec.sacscode11);
		lifacmvrec.sacstyp.set(t5645rec.sacstype11);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origamt.set(wsaaAgptInner.agptTdcchrg);
		lifacmvrec.acctamt.set(wsaaAgptInner.agptTdcchrg);
		lifacmvrec.rldgcoy.set(wsaaAgptInner.agptRldgcoy);
		lifacmvrec.rldgacct.set(wsaaAgptInner.agptRldgacct);
		lifacmvrec.rdocnum.set(wsaaAgptInner.agptBankkey);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.tranref.set(SPACES);
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-AGPT--INNER
 */
private static final class WsaaAgptInner { 
		/* WSAA-AGPT 
		 AGPTREC */
	private FixedLengthStringData agptRldgcoy = new FixedLengthStringData(1);
	private FixedLengthStringData agptRldgacct = new FixedLengthStringData(16);
	private PackedDecimalData agptSacscurbal = new PackedDecimalData(17, 2);
	private FixedLengthStringData agptProcflg = new FixedLengthStringData(1);
	private FixedLengthStringData agptTagsusind = new FixedLengthStringData(1);
	private FixedLengthStringData agptTaxcde = new FixedLengthStringData(2);
	private FixedLengthStringData agptCurrcode = new FixedLengthStringData(3);
	private FixedLengthStringData agptAgccqind = new FixedLengthStringData(1);
	private FixedLengthStringData agptPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData agptFacthous = new FixedLengthStringData(2);
	private FixedLengthStringData agptBankkey = new FixedLengthStringData(10);
	private FixedLengthStringData agptBankacckey = new FixedLengthStringData(20);
	private PackedDecimalData agptWhtax = new PackedDecimalData(13, 2);
	private PackedDecimalData agptTcolbal = new PackedDecimalData(14, 2);
	private PackedDecimalData agptTdeduct = new PackedDecimalData(12, 2);
	private PackedDecimalData agptTdcchrg = new PackedDecimalData(14, 2);
	private FixedLengthStringData agptPaymth = new FixedLengthStringData(2);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData agpyagtrec = new FixedLengthStringData(10).init("AGPYAGTREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData cheqrec = new FixedLengthStringData(10).init("CHEQREC   ");
	private FixedLengthStringData preqrec = new FixedLengthStringData(10).init("PREQREC   ");
	private FixedLengthStringData tatwrec = new FixedLengthStringData(10).init("TATWREC");
	private FixedLengthStringData agcsrec = new FixedLengthStringData(10).init("AGCSREC");
}
/*
 * Class transformed  from Data Structure CONTROL-TOTALS--INNER
 */
private static final class ControlTotalsInner { 
		/* CONTROL-TOTALS */
	private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
	private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
	private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
	private ZonedDecimalData ct14 = new ZonedDecimalData(2, 0).init(14).setUnsigned();
	private ZonedDecimalData ct15 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
	private ZonedDecimalData ct16 = new ZonedDecimalData(2, 0).init(16).setUnsigned();
	private ZonedDecimalData ct17 = new ZonedDecimalData(2, 0).init(17).setUnsigned();
	private ZonedDecimalData ct18 = new ZonedDecimalData(2, 0).init(18).setUnsigned();
	private ZonedDecimalData ct19 = new ZonedDecimalData(2, 0).init(19).setUnsigned();
	private ZonedDecimalData ct20 = new ZonedDecimalData(2, 0).init(20).setUnsigned();
	private ZonedDecimalData ct21 = new ZonedDecimalData(2, 0).init(21).setUnsigned();
}
}
