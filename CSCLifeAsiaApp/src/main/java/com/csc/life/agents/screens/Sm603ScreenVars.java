package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM603
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm603ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(567);
	public FixedLengthStringData dataFields = new FixedLengthStringData(103).isAPartOf(dataArea, 0);
	public FixedLengthStringData actives = new FixedLengthStringData(3).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] active = FLSArrayPartOfStructure(3, 1, actives, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(actives, 0, FILLER_REDEFINE);
	public FixedLengthStringData active01 = DD.active.copy().isAPartOf(filler,0);
	public FixedLengthStringData active02 = DD.active.copy().isAPartOf(filler,1);
	public FixedLengthStringData active03 = DD.active.copy().isAPartOf(filler,2);
	public FixedLengthStringData agtypes = new FixedLengthStringData(20).isAPartOf(dataFields, 3);
	public FixedLengthStringData[] agtype = FLSArrayPartOfStructure(10, 2, agtypes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(agtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtype01 = DD.agtype.copy().isAPartOf(filler1,0);
	public FixedLengthStringData agtype02 = DD.agtype.copy().isAPartOf(filler1,2);
	public FixedLengthStringData agtype03 = DD.agtype.copy().isAPartOf(filler1,4);
	public FixedLengthStringData agtype04 = DD.agtype.copy().isAPartOf(filler1,6);
	public FixedLengthStringData agtype05 = DD.agtype.copy().isAPartOf(filler1,8);
	public FixedLengthStringData agtype06 = DD.agtype.copy().isAPartOf(filler1,10);
	public FixedLengthStringData agtype07 = DD.agtype.copy().isAPartOf(filler1,12);
	public FixedLengthStringData agtype08 = DD.agtype.copy().isAPartOf(filler1,14);
	public FixedLengthStringData agtype09 = DD.agtype.copy().isAPartOf(filler1,16);
	public FixedLengthStringData agtype10 = DD.agtype.copy().isAPartOf(filler1,18);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,24);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,32);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,40);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData mlagttyps = new FixedLengthStringData(20).isAPartOf(dataFields, 78);
	public FixedLengthStringData[] mlagttyp = FLSArrayPartOfStructure(10, 2, mlagttyps, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(mlagttyps, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlagttyp01 = DD.mlagttyp.copy().isAPartOf(filler2,0);
	public FixedLengthStringData mlagttyp02 = DD.mlagttyp.copy().isAPartOf(filler2,2);
	public FixedLengthStringData mlagttyp03 = DD.mlagttyp.copy().isAPartOf(filler2,4);
	public FixedLengthStringData mlagttyp04 = DD.mlagttyp.copy().isAPartOf(filler2,6);
	public FixedLengthStringData mlagttyp05 = DD.mlagttyp.copy().isAPartOf(filler2,8);
	public FixedLengthStringData mlagttyp06 = DD.mlagttyp.copy().isAPartOf(filler2,10);
	public FixedLengthStringData mlagttyp07 = DD.mlagttyp.copy().isAPartOf(filler2,12);
	public FixedLengthStringData mlagttyp08 = DD.mlagttyp.copy().isAPartOf(filler2,14);
	public FixedLengthStringData mlagttyp09 = DD.mlagttyp.copy().isAPartOf(filler2,16);
	public FixedLengthStringData mlagttyp10 = DD.mlagttyp.copy().isAPartOf(filler2,18);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 103);
	public FixedLengthStringData activesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] activeErr = FLSArrayPartOfStructure(3, 4, activesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(activesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData active01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData active02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData active03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData agtypesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] agtypeErr = FLSArrayPartOfStructure(10, 4, agtypesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(agtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtype01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData agtype02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData agtype03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData agtype04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData agtype05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData agtype06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData agtype07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData agtype08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData agtype09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData agtype10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData mlagttypsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] mlagttypErr = FLSArrayPartOfStructure(10, 4, mlagttypsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(mlagttypsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlagttyp01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData mlagttyp02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData mlagttyp03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData mlagttyp04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData mlagttyp05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData mlagttyp06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData mlagttyp07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData mlagttyp08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData mlagttyp09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData mlagttyp10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 219);
	public FixedLengthStringData activesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] activeOut = FLSArrayPartOfStructure(3, 12, activesOut, 0);
	public FixedLengthStringData[][] activeO = FLSDArrayPartOfArrayStructure(12, 1, activeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(36).isAPartOf(activesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] active01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] active02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] active03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData agtypesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(10, 12, agtypesOut, 0);
	public FixedLengthStringData[][] agtypeO = FLSDArrayPartOfArrayStructure(12, 1, agtypeOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(agtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agtype01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] agtype02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] agtype03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] agtype04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] agtype05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] agtype06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] agtype07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] agtype08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] agtype09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] agtype10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData mlagttypsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] mlagttypOut = FLSArrayPartOfStructure(10, 12, mlagttypsOut, 0);
	public FixedLengthStringData[][] mlagttypO = FLSDArrayPartOfArrayStructure(12, 1, mlagttypOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(mlagttypsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlagttyp01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] mlagttyp02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] mlagttyp03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] mlagttyp04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] mlagttyp05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] mlagttyp06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] mlagttyp07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] mlagttyp08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] mlagttyp09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] mlagttyp10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sm603screenWritten = new LongData(0);
	public LongData Sm603protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm603ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, active01, agtype01, agtype02, agtype03, agtype04, active02, mlagttyp01, mlagttyp02, mlagttyp03, mlagttyp04, agtype05, agtype06, agtype07, agtype08, agtype09, agtype10, mlagttyp05, mlagttyp06, mlagttyp07, mlagttyp08, mlagttyp09, mlagttyp10, active03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, active01Out, agtype01Out, agtype02Out, agtype03Out, agtype04Out, active02Out, mlagttyp01Out, mlagttyp02Out, mlagttyp03Out, mlagttyp04Out, agtype05Out, agtype06Out, agtype07Out, agtype08Out, agtype09Out, agtype10Out, mlagttyp05Out, mlagttyp06Out, mlagttyp07Out, mlagttyp08Out, mlagttyp09Out, mlagttyp10Out, active03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, active01Err, agtype01Err, agtype02Err, agtype03Err, agtype04Err, active02Err, mlagttyp01Err, mlagttyp02Err, mlagttyp03Err, mlagttyp04Err, agtype05Err, agtype06Err, agtype07Err, agtype08Err, agtype09Err, agtype10Err, mlagttyp05Err, mlagttyp06Err, mlagttyp07Err, mlagttyp08Err, mlagttyp09Err, mlagttyp10Err, active03Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm603screen.class;
		protectRecord = Sm603protect.class;
	}

}
