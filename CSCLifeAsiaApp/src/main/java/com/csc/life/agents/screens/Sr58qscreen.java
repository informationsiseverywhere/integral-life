package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr58qscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {0, 0, 0, 0}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr58qScreenVars sv = (Sr58qScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr58qscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr58qScreenVars screenVars = ( Sr58qScreenVars )pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.anntarprm.setClassString("");
		screenVars.blprem.setClassString("");
		screenVars.premst.setClassString("");
		screenVars.zlocamt.setClassString("");
		screenVars.znadjperc01.setClassString("");
		screenVars.znadjperc02.setClassString("");
	}
	
	/**
	 * Clear all the variables in S2500screen
	 */
	public static void clear(VarModel pv) {
		Sr58qScreenVars screenVars = (Sr58qScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.anntarprm.clear();
		screenVars.blprem.clear();
		screenVars.premst.clear();
		screenVars.zlocamt.clear();
		screenVars.znadjperc01.clear();
		screenVars.znadjperc02.clear();
	}

}
