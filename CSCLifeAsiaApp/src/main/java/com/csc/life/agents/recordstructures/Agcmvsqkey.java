package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:03
 * Description:
 * Copybook name: AGCMVSQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmvsqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmvsqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmvsqKey = new FixedLengthStringData(64).isAPartOf(agcmvsqFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmvsqChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmvsqKey, 0);
  	public FixedLengthStringData agcmvsqChdrnum = new FixedLengthStringData(8).isAPartOf(agcmvsqKey, 1);
  	public FixedLengthStringData agcmvsqAgntnum = new FixedLengthStringData(8).isAPartOf(agcmvsqKey, 9);
  	public FixedLengthStringData agcmvsqLife = new FixedLengthStringData(2).isAPartOf(agcmvsqKey, 17);
  	public FixedLengthStringData agcmvsqCoverage = new FixedLengthStringData(2).isAPartOf(agcmvsqKey, 19);
  	public FixedLengthStringData agcmvsqRider = new FixedLengthStringData(2).isAPartOf(agcmvsqKey, 21);
  	public PackedDecimalData agcmvsqPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmvsqKey, 23);
  	public PackedDecimalData agcmvsqSeqno = new PackedDecimalData(2, 0).isAPartOf(agcmvsqKey, 26);
  	public FixedLengthStringData agcmvsqOvrdcat = new FixedLengthStringData(1).isAPartOf(agcmvsqKey, 28);
  	public FixedLengthStringData filler = new FixedLengthStringData(35).isAPartOf(agcmvsqKey, 29, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmvsqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmvsqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}