package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5564
 * @version 1.0 generated on 30/08/09 06:44
 * @author Quipoz
 */
public class S5564ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(660);
	public FixedLengthStringData dataFields = new FixedLengthStringData(180).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData freqs = new FixedLengthStringData(24).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] freq = FLSArrayPartOfStructure(12, 2, freqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData freq01 = DD.freq.copy().isAPartOf(filler,0);
	public FixedLengthStringData freq02 = DD.freq.copy().isAPartOf(filler,2);
	public FixedLengthStringData freq03 = DD.freq.copy().isAPartOf(filler,4);
	public FixedLengthStringData freq04 = DD.freq.copy().isAPartOf(filler,6);
	public FixedLengthStringData freq05 = DD.freq.copy().isAPartOf(filler,8);
	public FixedLengthStringData freq06 = DD.freq.copy().isAPartOf(filler,10);
	public FixedLengthStringData freq07 = DD.freq.copy().isAPartOf(filler,12);
	public FixedLengthStringData freq08 = DD.freq.copy().isAPartOf(filler,14);
	public FixedLengthStringData freq09 = DD.freq.copy().isAPartOf(filler,16);
	public FixedLengthStringData freq10 = DD.freq.copy().isAPartOf(filler,18);
	public FixedLengthStringData freq11 = DD.freq.copy().isAPartOf(filler,20);
	public FixedLengthStringData freq12 = DD.freq.copy().isAPartOf(filler,22);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,33);
	public FixedLengthStringData itmkeys = new FixedLengthStringData(96).isAPartOf(dataFields, 41);
	public FixedLengthStringData[] itmkey = FLSArrayPartOfStructure(12, 8, itmkeys, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(itmkeys, 0, FILLER_REDEFINE);
	public FixedLengthStringData itmkey01 = DD.itmkey.copy().isAPartOf(filler1,0);
	public FixedLengthStringData itmkey02 = DD.itmkey.copy().isAPartOf(filler1,8);
	public FixedLengthStringData itmkey03 = DD.itmkey.copy().isAPartOf(filler1,16);
	public FixedLengthStringData itmkey04 = DD.itmkey.copy().isAPartOf(filler1,24);
	public FixedLengthStringData itmkey05 = DD.itmkey.copy().isAPartOf(filler1,32);
	public FixedLengthStringData itmkey06 = DD.itmkey.copy().isAPartOf(filler1,40);
	public FixedLengthStringData itmkey07 = DD.itmkey.copy().isAPartOf(filler1,48);
	public FixedLengthStringData itmkey08 = DD.itmkey.copy().isAPartOf(filler1,56);
	public FixedLengthStringData itmkey09 = DD.itmkey.copy().isAPartOf(filler1,64);
	public FixedLengthStringData itmkey10 = DD.itmkey.copy().isAPartOf(filler1,72);
	public FixedLengthStringData itmkey11 = DD.itmkey.copy().isAPartOf(filler1,80);
	public FixedLengthStringData itmkey12 = DD.itmkey.copy().isAPartOf(filler1,88);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,137);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 180);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData freqsErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] freqErr = FLSArrayPartOfStructure(12, 4, freqsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(freqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData freq01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData freq02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData freq03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData freq04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData freq05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData freq06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData freq07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData freq08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData freq09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData freq10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData freq11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData freq12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmkeysErr = new FixedLengthStringData(48).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData[] itmkeyErr = FLSArrayPartOfStructure(12, 4, itmkeysErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(48).isAPartOf(itmkeysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData itmkey01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData itmkey02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData itmkey03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData itmkey04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData itmkey05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData itmkey06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData itmkey07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData itmkey08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData itmkey09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData itmkey10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData itmkey11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData itmkey12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 300);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData freqsOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] freqOut = FLSArrayPartOfStructure(12, 12, freqsOut, 0);
	public FixedLengthStringData[][] freqO = FLSDArrayPartOfArrayStructure(12, 1, freqOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(144).isAPartOf(freqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] freq01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] freq02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] freq03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] freq04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] freq05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] freq06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] freq07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] freq08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] freq09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] freq10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] freq11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] freq12Out = FLSArrayPartOfStructure(12, 1, filler4, 132);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData itmkeysOut = new FixedLengthStringData(144).isAPartOf(outputIndicators, 180);
	public FixedLengthStringData[] itmkeyOut = FLSArrayPartOfStructure(12, 12, itmkeysOut, 0);
	public FixedLengthStringData[][] itmkeyO = FLSDArrayPartOfArrayStructure(12, 1, itmkeyOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(144).isAPartOf(itmkeysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] itmkey01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] itmkey02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] itmkey03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] itmkey04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] itmkey05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] itmkey06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] itmkey07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] itmkey08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] itmkey09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] itmkey10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] itmkey11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] itmkey12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5564screenWritten = new LongData(0);
	public LongData S5564protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5564ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, freq01, itmkey01, freq02, itmkey02, freq03, itmkey03, freq04, itmkey04, freq05, itmkey05, freq06, itmkey06, freq07, itmkey07, freq08, itmkey08, freq09, itmkey09, freq10, itmkey10, freq11, itmkey11, freq12, itmkey12};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, freq01Out, itmkey01Out, freq02Out, itmkey02Out, freq03Out, itmkey03Out, freq04Out, itmkey04Out, freq05Out, itmkey05Out, freq06Out, itmkey06Out, freq07Out, itmkey07Out, freq08Out, itmkey08Out, freq09Out, itmkey09Out, freq10Out, itmkey10Out, freq11Out, itmkey11Out, freq12Out, itmkey12Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, freq01Err, itmkey01Err, freq02Err, itmkey02Err, freq03Err, itmkey03Err, freq04Err, itmkey04Err, freq05Err, itmkey05Err, freq06Err, itmkey06Err, freq07Err, itmkey07Err, freq08Err, itmkey08Err, freq09Err, itmkey09Err, freq10Err, itmkey10Err, freq11Err, itmkey11Err, freq12Err, itmkey12Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5564screen.class;
		protectRecord = S5564protect.class;
	}

}
