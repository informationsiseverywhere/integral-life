package com.csc.life.agents.dataaccess.dao;
 
import com.csc.life.agents.dataaccess.model.Zrgapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZrgapfDAO extends BaseDAO<Zrgapf> {
	public Zrgapf getZrgapfRecord(String Agntcoy, String Agntnum);

   
}