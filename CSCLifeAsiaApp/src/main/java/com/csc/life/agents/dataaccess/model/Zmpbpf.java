/*
 * File: Zmpbpf.java
 * Date: July 21, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zmpbpf implements Serializable {
	private long uniqueNumber;
	private BigDecimal acctamt;
	private String agntcoy;
	private String agntnum;
	private BigDecimal batcactmn;
	private BigDecimal batcactyr;
	private String batctrcde;
	private BigDecimal bonusamt;
	private String chdrnum;
	private BigDecimal comtot;
	private String coverage;
	private Timestamp datime;
	private BigDecimal effdate;
	private String jobName;
	private BigDecimal jobno;	
	private String life;
    private String origcurr;
	private BigDecimal prcdate;
	private BigDecimal prcent;
	private BigDecimal prcnt;
	private String rider;
	private BigDecimal tranno;
	private String userProfile;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public BigDecimal getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(BigDecimal acctamt) {
		this.acctamt = acctamt;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public BigDecimal getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(BigDecimal batcactmn) {
		this.batcactmn = batcactmn;
	}
	public BigDecimal getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(BigDecimal batcactyr) {
		this.batcactyr = batcactyr;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
	public BigDecimal getBonusamt() {
		return bonusamt;
	}
	public void setBonusamt(BigDecimal bonusamt) {
		this.bonusamt = bonusamt;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public BigDecimal getComtot() {
		return comtot;
	}
	public void setComtot(BigDecimal comtot) {
		this.comtot = comtot;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	public BigDecimal getEffdate() {
		return effdate;
	}
	public void setEffdate(BigDecimal effdate) {
		this.effdate = effdate;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public BigDecimal getJobno() {
		return jobno;
	}
	public void setJobno(BigDecimal jobno) {
		this.jobno = jobno;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(String origcurr) {
		this.origcurr = origcurr;
	}
	public BigDecimal getPrcdate() {
		return prcdate;
	}
	public void setPrcdate(BigDecimal prcdate) {
		this.prcdate = prcdate;
	}
	public BigDecimal getPrcent() {
		return prcent;
	}
	public void setPrcent(BigDecimal prcent) {
		this.prcent = prcent;
	}
	public BigDecimal getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(BigDecimal prcnt) {
		this.prcnt = prcnt;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public BigDecimal getTranno() {
		return tranno;
	}
	public void setTranno(BigDecimal tranno) {
		this.tranno = tranno;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	
					
}
