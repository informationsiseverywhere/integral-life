/*
 * File: Br655.java
 * Date: 29 August 2009 22:33:09
 * Author: Quipoz Limited
 * 
 * Class transformed from BR655.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.dataaccess.ZbupTableDAM;
import com.csc.life.agents.reports.Rr655Report;
import com.csc.life.agents.tablestructures.Tr660rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Geterror;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.CobolUnstring;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Bonus Workbench Bonus Upload
*   ****************************
*   This batch job will decode the flat file and write records to ZBUP.
*   During the loading of records, the validity of agent number,contract
*   number and bonus workbench bonus plan id should be checked.An
*   exception report RR655 is written if there is any error detected.
*   The error record will not be added to ZBUP so it won't be picked up
*   for posting later.
*
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of records skipped
*     03  -  Number of records written
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br655 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlzbnwpf1rs = null;
	private java.sql.PreparedStatement sqlzbnwpf1ps = null;
	private java.sql.Connection sqlzbnwpf1conn = null;
	private String sqlzbnwpf1 = "";
	private Rr655Report printerFile = new Rr655Report();
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(999);
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR655");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaBwAmtArrayGrp = new FixedLengthStringData(11);
	private FixedLengthStringData[] wsaaBwAmtArray = FLSArrayPartOfStructure(11, 1, wsaaBwAmtArrayGrp, 0);

		/* WSAA-STOP-GRP */
	private FixedLengthStringData wsaaStop = new FixedLengthStringData(3);
	private Validator wsaaStopYes = new Validator(wsaaStop, "YES");

		/* WSAA-MINUS-GRP */
	private FixedLengthStringData wsaaMinus = new FixedLengthStringData(3);
	private Validator wsaaMinusYes = new Validator(wsaaMinus, "YES");

		/* WSAA-DEC-GRP */
	private FixedLengthStringData wsaaDec = new FixedLengthStringData(3);
	private Validator wsaaDecYes = new Validator(wsaaDec, "YES");
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-INT-DEC-GRP */
	private FixedLengthStringData wsaaIntDec03 = new FixedLengthStringData(13);
	private ZonedDecimalData wsaaIntN = new ZonedDecimalData(11, 0).isAPartOf(wsaaIntDec03, 0).setUnsigned();
	private ZonedDecimalData wsaaDecN = new ZonedDecimalData(2, 0).isAPartOf(wsaaIntDec03, 11).setUnsigned();
	private ZonedDecimalData wsaaIntDec = new ZonedDecimalData(13, 2).isAPartOf(wsaaIntDec03, 0, REDEFINE).setUnsigned();
	private PackedDecimalData wsaaIntDecSigned = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaIntStart = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIntEnd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIntLen = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDecStart = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDecEnd = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaDecLen = new ZonedDecimalData(2, 0).setUnsigned();
		/*  Define the variables for database file overriding use.*/
	private ZonedDecimalData wsaaAmt = new ZonedDecimalData(12, 2);
	private ZonedDecimalData wsaaPrcdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPrcdateR = new FixedLengthStringData(8).isAPartOf(wsaaPrcdate, 0, REDEFINE);
	private ZonedDecimalData wsaaPrcdtCcyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrcdateR, 0).setUnsigned();
	private ZonedDecimalData wsaaPrcdtMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrcdateR, 4).setUnsigned();
	private ZonedDecimalData wsaaPrcdtDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrcdateR, 6).setUnsigned();
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaLang = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaStat = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaEror = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaBonusType = new FixedLengthStringData(2).init(SPACES);
		/*  Define the variables to load the table items of TR660 into
		  working storage.*/
	//private static final int wsaaTr660Size = 50;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaTr660Size = 1000;

		/* WSAA-TR660-ARRAY */
	private FixedLengthStringData[] wsaaTr660Rec = FLSInittedArray (1000, 10);
	private FixedLengthStringData[] wsaaTr660Key = FLSDArrayPartOfArrayStructure(8, wsaaTr660Rec, 0);
	private FixedLengthStringData[] wsaaTr660Zbnplnid = FLSDArrayPartOfArrayStructure(8, wsaaTr660Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaTr660Data = FLSDArrayPartOfArrayStructure(2, wsaaTr660Rec, 8);
	private FixedLengthStringData[] wsaaTr660Zbntyp = FLSDArrayPartOfArrayStructure(2, wsaaTr660Data, 0);
	private static final String descrec = "DESCREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String aglfrec = "AGLFREC";
	private static final String zbuprec = "ZBUPREC";
		/* ERRORS */
	private static final String g772 = "G772";
	private static final String h791 = "H791";
	private static final String f917 = "F917";
	private static final String rl62 = "RL62";
	private static final String t1693 = "T1693";
	private static final String tr660 = "TR660";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

		/* SQL-ZBNWPF */
	private FixedLengthStringData zbnwrec1 = new FixedLengthStringData(1500);
	private FixedLengthStringData sqlZbnwrec = new FixedLengthStringData(1500).isAPartOf(zbnwrec1, 0);

		/*  Control indicators*/
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page headings*/
	//ILIFE-1158 (L2BONPOST Aborted) vchawda Start
	private FixedLengthStringData rr655H01 = new FixedLengthStringData(31);
	private FixedLengthStringData rr655h01O = new FixedLengthStringData(31).isAPartOf(rr655H01, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rr655h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rr655h01O, 1);
	//ILIFE-1158 End
		/*  Detail line - add as many detail and total lines as required.
		              - use redefines to save WS space where applicable.*/
	//ILIFE-1158 (L2BONPOST Aborted) vchawda Start 
	private FixedLengthStringData rr655D01 = new FixedLengthStringData(76);
	private FixedLengthStringData rr655d01O = new FixedLengthStringData(76).isAPartOf(rr655D01, 0);
	private FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(rr655d01O, 0);
	private FixedLengthStringData trdate = new FixedLengthStringData(10).isAPartOf(rr655d01O, 8);
	private FixedLengthStringData zbnplnid = new FixedLengthStringData(8).isAPartOf(rr655d01O, 18);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr655d01O, 26);
	private ZonedDecimalData amtfld = new ZonedDecimalData(12, 2).isAPartOf(rr655d01O, 34);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rr655d01O, 46);
	//ILIFE-1158 End
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaTr660Ix = new IntegerData();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZbupTableDAM zbupIO = new ZbupTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr660rec tr660rec = new Tr660rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private WsaaBwRecInner wsaaBwRecInner = new WsaaBwRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090
	}

	public Br655() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		defineCursor1060();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		/*  Prepare the overriding command.*/
		/*    STRING                                                       */
		/*                              DELIMITED BY SIZE                  */
		/*        BPRD-SYSTEM-PARAM01   DELIMITED BY SPACE                 */
		/*        '/'                   DELIMITED BY SIZE                  */
		/*        BPRD-SYSTEM-PARAM02   DELIMITED BY SPACE                 */
		/*        ') SEQONLY(*YES 1000)'                                   */
		/*                              DELIMITED BY SIZE                  */
		/*                              INTO WSAA-QCMDEXC                  */
		/*    END-STRING.                                                  */
		/*    CALL 'QCMDEXC' USING WSAA-QCMDEXC WSAA-QCMDEXC-LENGTH.       */
		/*  Load the bonus plan table.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tr660);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");

		wsaaTr660Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			loadTr6601100();
		}
		
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		company.set(bsprIO.getCompany());
		companynm.set(descIO.getLongdesc());
	}

protected void defineCursor1060()
	{
		/*  Define the query required by declaring a cursor*/
		sqlzbnwpf1 = " SELECT  *" +
" FROM   " + getAppVars().getTableNameOverriden("ZBNWPF") + " ";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlzbnwpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.agents.dataaccess.ZbnwpfTableDAM());
			sqlzbnwpf1ps = getAppVars().prepareStatementEmbeded(sqlzbnwpf1conn, sqlzbnwpf1, "ZBNWPF");
			sqlzbnwpf1rs = getAppVars().executeQuery(sqlzbnwpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void loadTr6601100()
	{
			start1100();
		}

protected void start1100()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),tr660)) {
			if (isEQ(itemIO.getFunction(),varcom.begn)) {
				tr660rec.tr660Rec.set(SPACES);
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}
		if (isGT(wsaaTr660Ix,wsaaTr660Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(tr660);
			fatalError600();
		}
		tr660rec.tr660Rec.set(itemIO.getGenarea());
		wsaaTr660Zbntyp[wsaaTr660Ix.toInt()].set(tr660rec.zbntyp);
		wsaaTr660Zbnplnid[wsaaTr660Ix.toInt()].set(itemIO.getItemitem());
		itemIO.setFunction(varcom.nextr);
		wsaaTr660Ix.add(1);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch every record from the cursor and then process those requ*/
		/*   records.*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzbnwpf1rs)) {
				getAppVars().getDBObject(sqlzbnwpf1rs, 1, sqlZbnwrec);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		/* Decode the record read from the upload file, then check*/
		/* the existence of agent,contract and bonus plan id.*/
		CobolUnstring uv1 = CobolUnstring.getInstance(sqlZbnwrec, new com.quipoz.COBOLFramework.constants.ConstantALL(";"));
		uv1.unstring(wsaaBwRecInner.wsaaBwCompany, wsaaBwRecInner.wsaaBwPolicyId, wsaaBwRecInner.wsaaBwProdGrp, wsaaBwRecInner.wsaaBwWrtAgt, wsaaBwRecInner.wsaaBwAgt, wsaaBwRecInner.wsaaBwAgtProfile, wsaaBwRecInner.wsaaBwWrtAgtProfile, wsaaBwRecInner.wsaaBwWrtAgtRole, wsaaBwRecInner.wsaaBwPrcdt, wsaaBwRecInner.wsaaBwAmt, wsaaBwRecInner.wsaaBwRevcode, wsaaBwRecInner.wsaaBwCat, wsaaBwRecInner.wsaaBwReason, wsaaBwRecInner.wsaaBwJournal, wsaaBwRecInner.wsaaBwReport, wsaaBwRecInner.wsaaBwEarning, wsaaBwRecInner.wsaaBwSyscode, wsaaBwRecInner.wsaaBwFeecode, wsaaBwRecInner.wsaaBwDuration, wsaaBwRecInner.wsaaBwCredit, wsaaBwRecInner.wsaaBwPlanid, wsaaBwRecInner.wsaaBwFiller);
		/*  Process uploaded value*/
		if (isEQ(wsaaBwRecInner.wsaaBwPrcdt, NUMERIC)) {
			wsaaPrcdtCcyy.set(wsaaBwRecInner.wsaaBwPrcdtCcyy);
			wsaaPrcdtMm.set(wsaaBwRecInner.wsaaBwPrcdtMm);
			wsaaPrcdtDd.set(wsaaBwRecInner.wsaaBwPrcdtDd);
		}
		else {
			wsaaPrcdate.set(varcom.vrcmMaxDate);
		}
		wsaaStop.set(SPACES);
		wsaaMinus.set(SPACES);
		wsaaDec.set(SPACES);
		wsaaIntStart.set(ZERO);
		wsaaIntEnd.set(ZERO);
		wsaaDecStart.set(ZERO);
		wsaaDecEnd.set(ZERO);
		wsaaBwAmtArrayGrp.set(wsaaBwRecInner.wsaaBwAmt);
		a400Move();
		/*   IF WSAA-BW-CREDIT           = 'C'                            */
		/*      MOVE WSAA-BW-AMT-9       TO WSAA-AMT                      */
		/*   ELSE                                                         */
		/*      COMPUTE WSAA-AMT         = WSAA-BW-AMT-9 * -1             */
		/*   END-IF.                                                      */
		if (isEQ(wsaaBwRecInner.wsaaBwCredit, "C")) {
			wsaaAmt.set(wsaaIntDecSigned);
		}
		else {
			compute(wsaaAmt, 2).set(mult(wsaaIntDecSigned, -1));
		}
		/*  check if agent and contract exist*/
			chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(wsaaBwRecInner.wsaaBwPolicyId);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			wsaaEror.set(f917);
			writeExceptionReport2600();
			wsspEdterror.set(SPACES);
			return ;
		}
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(bsprIO.getCompany());
		aglfIO.setAgntnum(wsaaBwRecInner.wsaaBwAgt);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(),varcom.mrnf)) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			wsaaEror.set(g772);
			writeExceptionReport2600();
			wsspEdterror.set(SPACES);
			return ;
		}
		/*  Check if bonus plan exists*/
		wsaaBonusType.set(SPACES);
		ArraySearch as1 = ArraySearch.getInstance(wsaaTr660Rec);
		as1.setIndices(wsaaTr660Ix);
		as1.addSearchKey(wsaaTr660Key, wsaaBwRecInner.wsaaBwPlanid, true);
		if (as1.binarySearch()) {
			wsaaBonusType.set(wsaaTr660Zbntyp[wsaaTr660Ix.toInt()]);
		}
		else {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			wsaaEror.set(rl62);
			writeExceptionReport2600();
			wsspEdterror.set(SPACES);
			return ;
		}
		wsspEdterror.set(varcom.oK);
	}

protected void writeExceptionReport2600()
	{
		writeHeader2610();
		writeDetail2650();
	}

protected void writeHeader2610()
	{
		if (newPageReq.isTrue()) {
			printerFile.printRr655h01(rr655H01, indicArea);
			wsaaOverflow.set("N");
		}
	}

protected void writeDetail2650()
	{
		wsaaDesc.set(SPACES);
		wsaaLang.set(bsscIO.getLanguage());
		callProgram(Geterror.class, wsaaEror, wsaaLang, wsaaDesc, wsaaStat);
		if (isNE(wsaaStat,varcom.oK)) {
			wsaaDesc.fill("?");
		}
		initialize(rr655D01);
		agntnum.set(wsaaBwRecInner.wsaaBwAgt);
		chdrnum.set(wsaaBwRecInner.wsaaBwPolicyId);
		datcon1rec.intDate.set(wsaaPrcdate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isEQ(datcon1rec.statuz,varcom.oK)) {
			trdate.set(datcon1rec.extDate);
		}
		else {
			trdate.fill("?");
		}
		amtfld.set(wsaaAmt);
		zbnplnid.set(wsaaBwRecInner.wsaaBwPlanid);
		longdesc.set(wsaaDesc);
		printerFile.printRr655d01(rr655D01, indicArea);
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/*  The uploaded record has passed all validation and it should be*/
		/*  written to ZBUP.*/
		zbupIO.setParams(SPACES);
		zbupIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		zbupIO.setChdrnum(chdrlnbIO.getChdrnum());
		zbupIO.setAgntcoy(aglfIO.getAgntcoy());
		zbupIO.setAgntnum(aglfIO.getAgntnum());
		zbupIO.setOrigamt(wsaaAmt);
		zbupIO.setOrigcurr(chdrlnbIO.getCntcurr());
		zbupIO.setPrcdate(wsaaPrcdate);
		zbupIO.setZbnplnid(wsaaBwRecInner.wsaaBwPlanid);
		zbupIO.setZbntyp(wsaaBonusType);
		zbupIO.setFormat(zbuprec);
		zbupIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zbupIO);
		if (isNE(zbupIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zbupIO.getStatuz());
			syserrrec.params.set(zbupIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlzbnwpf1conn, sqlzbnwpf1ps, sqlzbnwpf1rs);
		/*  Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a400Move()
	{
		a400Begin();
	}

protected void a400Begin()
	{
		for (wsaaIx.set(1); !(isGT(wsaaIx, 11)
		|| wsaaStopYes.isTrue()); wsaaIx.add(1)){
			a410S1();
		}
		compute(wsaaIntLen, 0).set(add(sub(wsaaIntEnd, wsaaIntStart), 1));
		if (isGT(wsaaIntLen, ZERO)) {
			wsaaIntN.set(subString(wsaaBwRecInner.wsaaBwAmt, wsaaIntStart, wsaaIntLen));
		}
		else {
			wsaaIntN.set(ZERO);
		}
		wsaaDecN.set(ZERO);
		if (wsaaDecYes.isTrue()) {
			compute(wsaaDecLen, 0).set(add(sub(wsaaDecEnd, wsaaDecStart), 1));
			if (isGT(wsaaDecLen, 2)) {
				wsaaDecLen.set(2);
			}
			if (isGT(wsaaDecLen, 0)) {
				wsaaDecN.set(subString(wsaaBwRecInner.wsaaBwAmt, wsaaDecStart, wsaaDecLen));
			}
		}
		if (wsaaMinusYes.isTrue()) {
			compute(wsaaIntDecSigned, 2).set(mult(wsaaIntDec, -1));
		}
		else {
			wsaaIntDecSigned.set(wsaaIntDec);
		}
	}

protected void a410S1()
	{
		a410Begin();
	}

protected void a410Begin()
	{
		if (isEQ(wsaaIx, 1)) {
			if (isEQ(wsaaBwAmtArray[1], "-")) {
				wsaaIntStart.set(2);
				wsaaMinus.set("YES");
			}
			else {
				wsaaIntStart.set(1);
			}
			return ;
		}
		if (isEQ(wsaaBwAmtArray[wsaaIx.toInt()], ".")) {
			compute(wsaaIntEnd, 0).set(sub(wsaaIx, 1));
			compute(wsaaDecStart, 0).set(add(wsaaIx, 1));
			wsaaDec.set("YES");
			return ;
		}
		if (isEQ(wsaaBwAmtArray[wsaaIx.toInt()], " ")) {
			if (wsaaDecYes.isTrue()) {
				compute(wsaaDecEnd, 0).set(sub(wsaaIx, 1));
			}
			else {
				compute(wsaaIntEnd, 0).set(sub(wsaaIx, 1));
			}
			wsaaStop.set("YES");
			return ;
		}
		if (isNE(wsaaBwAmtArray[wsaaIx.toInt()], NUMERIC)) {
			syserrrec.syserrStatuz.set("NUMR");
			fatalError600();
		}
		if (isEQ(wsaaIx, 11)) {
			if (wsaaDecYes.isTrue()) {
				wsaaDecEnd.set(11);
			}
			else {
				wsaaIntEnd.set(11);
			}
		}
	}
/*
 * Class transformed  from Data Structure WSAA-BW-REC--INNER
 */
private static final class WsaaBwRecInner { 
		/* WSAA-BW-REC */
	private FixedLengthStringData wsaaBwCompany = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBwPolicyId = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBwProdGrp = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBwWrtAgt = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBwAgt = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBwAgtProfile = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBwWrtAgtProfile = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBwWrtAgtRole = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBwPrcdt = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaBwPrcdt9 = new FixedLengthStringData(8).isAPartOf(wsaaBwPrcdt, 0, REDEFINE);
	private ZonedDecimalData wsaaBwPrcdtMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBwPrcdt9, 0).setUnsigned();
	private ZonedDecimalData wsaaBwPrcdtDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBwPrcdt9, 2).setUnsigned();
	private ZonedDecimalData wsaaBwPrcdtCcyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaBwPrcdt9, 4).setUnsigned();
	private FixedLengthStringData wsaaBwAmt = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaBwRevcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBwCat = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBwReason = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBwJournal = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBwReport = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBwEarning = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBwSyscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBwFeecode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBwDuration = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBwCredit = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBwPlanid = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBwFiller = new FixedLengthStringData(1);
}
}
