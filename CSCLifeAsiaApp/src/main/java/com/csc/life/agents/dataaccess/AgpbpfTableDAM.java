package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpbpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:41
 * Class transformed from AGPBPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpbpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 152;
	public FixedLengthStringData agpbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agpbpfRecord = agpbrec;
	
	public FixedLengthStringData rldgcoy = DD.rldgcoy.copy().isAPartOf(agpbrec);
	public FixedLengthStringData rldgacct = DD.rldgacct.copy().isAPartOf(agpbrec);
	public PackedDecimalData sacscurbal = DD.sacscurbal.copy().isAPartOf(agpbrec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(agpbrec);
	public FixedLengthStringData tagsusind = DD.tagsusind.copy().isAPartOf(agpbrec);
	public FixedLengthStringData taxcde = DD.taxcde.copy().isAPartOf(agpbrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(agpbrec);
	public FixedLengthStringData agccqind = DD.agccqind.copy().isAPartOf(agpbrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(agpbrec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(agpbrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(agpbrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(agpbrec);
	public PackedDecimalData whtax = DD.whtax.copy().isAPartOf(agpbrec);
	public PackedDecimalData tcolbal = DD.tcolbal.copy().isAPartOf(agpbrec);
	public PackedDecimalData tdeduct = DD.tdeduct.copy().isAPartOf(agpbrec);
	public PackedDecimalData tdcchrg = DD.tdcchrg.copy().isAPartOf(agpbrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(agpbrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agpbrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agpbrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agpbrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgpbpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgpbpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgpbpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgpbpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpbpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgpbpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpbpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGPBPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"RLDGCOY, " +
							"RLDGACCT, " +
							"SACSCURBAL, " +
							"PROCFLG, " +
							"TAGSUSIND, " +
							"TAXCDE, " +
							"CURRCODE, " +
							"AGCCQIND, " +
							"PAYCLT, " +
							"FACTHOUS, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"WHTAX, " +
							"TCOLBAL, " +
							"TDEDUCT, " +
							"TDCCHRG, " +
							"PAYMTH, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     rldgcoy,
                                     rldgacct,
                                     sacscurbal,
                                     procflg,
                                     tagsusind,
                                     taxcde,
                                     currcode,
                                     agccqind,
                                     payclt,
                                     facthous,
                                     bankkey,
                                     bankacckey,
                                     whtax,
                                     tcolbal,
                                     tdeduct,
                                     tdcchrg,
                                     paymth,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		rldgcoy.clear();
  		rldgacct.clear();
  		sacscurbal.clear();
  		procflg.clear();
  		tagsusind.clear();
  		taxcde.clear();
  		currcode.clear();
  		agccqind.clear();
  		payclt.clear();
  		facthous.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		whtax.clear();
  		tcolbal.clear();
  		tdeduct.clear();
  		tdcchrg.clear();
  		paymth.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgpbrec() {
  		return agpbrec;
	}

	public FixedLengthStringData getAgpbpfRecord() {
  		return agpbpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgpbrec(what);
	}

	public void setAgpbrec(Object what) {
  		this.agpbrec.set(what);
	}

	public void setAgpbpfRecord(Object what) {
  		this.agpbpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agpbrec.getLength());
		result.set(agpbrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}