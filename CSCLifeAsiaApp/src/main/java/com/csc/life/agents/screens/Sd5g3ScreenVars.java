package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sd5g3ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(127);
	
	public FixedLengthStringData subfileFields = new FixedLengthStringData(61).isAPartOf(subfileArea, 0);
	public FixedLengthStringData salelicensetype = DD.salelicensetype.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData tlaglicno = DD.tlaglicno.copy().isAPartOf(subfileFields,30);
	public PackedDecimalData frmdate = DD.frmdate.copy().isAPartOf(subfileFields,45);
	public PackedDecimalData todate = DD.todate.copy().isAPartOf(subfileFields,53);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 61);
	public FixedLengthStringData salelicensetypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData tlaglicnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData frmdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData todateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 77);
	public FixedLengthStringData[] salelicensetypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] tlaglicnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] frmdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] todateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 125);
	
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData Sd5g3screensflWritten = new LongData(0);
	public LongData Sd5g3screenctlWritten = new LongData(0);
	public LongData Sd5g3screenWritten = new LongData(0);
	public LongData Sd5g3protectWritten = new LongData(0);
	public GeneralTable sd5g3screensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData frmdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData todateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sd5g3screensfl;
	}

	public Sd5g3ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(salelicensetypeOut,new String[] {"64","31", "-64","32", null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {salelicensetype, tlaglicno,frmdate,todate};
		screenSflOutFields = new BaseData[][] {salelicensetypeOut, tlaglicnoOut,frmdateOut,todateOut};
		screenSflErrFields = new BaseData[] {salelicensetypeErr, tlaglicnoErr,frmdateErr,todateErr};
		screenSflDateFields = new BaseData[] {frmdate,todate};
		screenSflDateErrFields = new BaseData[] {frmdateErr,todateErr};
		screenSflDateDispFields = new BaseData[] {frmdateDisp,todateDisp};

		screenFields = new BaseData[] {clntsel, cltname, agnum, agtype, agtydesc};
		screenOutFields = new BaseData[][] {clntselOut, cltnameOut, agnumOut, agtypeOut, agtydescOut};
		screenErrFields = new BaseData[] {clntselErr, cltnameErr, agnumErr, agtypeErr, agtydescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sd5g3screen.class;
		screenSflRecord = Sd5g3screensfl.class;
		screenCtlRecord = Sd5g3screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sd5g3protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sd5g3screenctl.lrec.pageSubfile);
	}
}
