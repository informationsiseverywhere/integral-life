package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.CltrelnDTO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.agents.screens.Sjl72ScreenVars;
import com.csc.life.agents.tablestructures.Tjl68rec;
import com.csc.smart.dataaccess.dao.UsrdDAO;
import com.csc.smart.dataaccess.model.Usrdpf;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl72 extends ScreenProgCS{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl72.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL72");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private Sjl72ScreenVars sv = ScreenProgram.getScreenVars(Sjl72ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private UsrdDAO usrdDAO = getApplicationContext().getBean("usrdDAO" , UsrdDAO.class);
	private Usrdpf usrdpf= new Usrdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private CltrelnDTO cltrelnDTO;
	private Map<String,Descpf> descMap = new HashMap<>();
	private List<Agsdpf> agsdpfList = new ArrayList<Agsdpf>();
	private Hierpf hierpf = new Hierpf();
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private List<Hierpf> hierpfList = new ArrayList<Hierpf>();

	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private static final String E186 = "E186";	
	private static final String JL91 = "JL91";
	private static final String HL01 = "HL01";
	private static final String F393 = "F393";
	private static final String F782 = "F782";
	private static final String JL94 = "JL94";
	private static final String E020 = "E020";
	private static final String JL85 = "JL85";
	private static final String JL86 = "JL86";
	private static final String JL84 = "JL84";
	private static final String JL87 = "JL87";
	private static final String E058 = "E058";
	private static final String JL89 = "JL89";
	
	private Tjl68rec tjl68rec = new Tjl68rec();	
	boolean isExisted = true;
	private static final String SALESREP = "Sales Representative";
	private static final String SALESTATUS = "Active";
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";
	private static final String TJL69 = "TJL69";

	private String wsaaGennum;
	
	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private Tr386rec tr386rec = new Tr386rec();	
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	public ZonedDecimalData date1 = new ZonedDecimalData(8, 0);

	public Pjl72() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl72", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}

	@Override
	protected void initialise1000() {
		
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		sv.moddate.set(varcom.vrcmMaxDate);
		sv.deldate.set(varcom.vrcmMaxDate);
		a7050Begin();
		initialise1010();
		protect1020();
	}


	protected void initialise1010() {
		
		if (isEQ(wsspcomn.flag, "A")) {
			sv.levelno.set(wsspcomn.chdrCownnum);
			sv.leveltyp.set("1");
			loadLongDesc();
		} else if (isEQ(wsspcomn.flag, "B") || isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "N")) {
			initialise1020();
		}
	}
	
	protected void a7050Begin()
	{
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if (isEQ(wsspcomn.sbmaction, "A") || isEQ(wsspcomn.sbmaction, "N")) {
			sv.scrndesc.set(tr386rec.progdesc[1].toString());
		} else if (isEQ(wsspcomn.sbmaction, "B")) {
			sv.scrndesc.set(tr386rec.progdesc[2].toString());
		}
		else if (isEQ(wsspcomn.sbmaction, "C")) {
			sv.scrndesc.set(tr386rec.progdesc[3].toString());
		}
	}
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(TJL70, "1");
		itemMap.put(TJL69, "1");
		
		descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(TJL70)) {
			sv.leveldesc.set(SALESREP);
		} else {
			sv.leveldesc.set(descMap.get(TJL70).getLongdesc());
		}
		
		if (!descMap.containsKey(TJL69)) {
			sv.salestatus.set(SALESTATUS);
		} else {
			sv.salestatus.set(descMap.get(TJL69).getLongdesc());
		}
	}
	
	private void initialise1020() {

		if (isEQ(wsspcomn.chdrCownnum, SPACE)) {
			sv.levelnoErr.set(JL91);
			return;
		}
		
		wsaaSalediv.set(SPACES);
		
		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			wsaaGennum=agsdpf.getGenerationno();
			
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getSurname() + " " + clntpf.getGivname());
			}
			
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.agntbr.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();
						
			}
			if (null != agsdpf.getLevelno())
				sv.levelno.set(agsdpf.getLevelno());
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltyp.set(agsdpf.getLevelclass());
				loadLongDesc();
			}
			if (null != agsdpf.getAgentclass())
				sv.agtype.set(agsdpf.getAgentclass());
			if (null != agsdpf.getUserid())
				sv.userid.set(agsdpf.getUserid());
		} else {
			sv.levelnoErr.set(JL91);
		}

	}
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.agntbr.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		
		
		descMap= descdao.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.agbrdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.agbrdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		

	}
	
	protected void readTjl68()
	{
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(TJL68);
		itempf.setItemitem(sv.saledept.trim());
	    itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(TJL68).concat(wsaaProg.toString()));
			fatalError600();
		}else
		tjl68rec.tjl68Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
	}

	@SuppressWarnings("static-access")
	private void protect1020() {
		if (isEQ(wsspcomn.flag, "A")) {
			sv.leveltypOut[Varcom.pr.toInt()].set("Y");
			sv.leveldescOut[Varcom.pr.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
			
		} else if (isEQ(wsspcomn.flag, "B")) {
			sv.leveltypOut[Varcom.pr.toInt()].set("Y");
			sv.leveldescOut[Varcom.pr.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("N");
			sv.moddateOut[Varcom.nd.toInt()].set("N");
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
		} else if (isEQ(wsspcomn.flag, "C")) {
			sv.leveltypOut[Varcom.pr.toInt()].set("Y");
			sv.leveldescOut[Varcom.pr.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("N");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.deldateOut[Varcom.nd.toInt()].set("N");
			sv.clntselOut[Varcom.pr.toInt()].set("Y");
			sv.cltnameOut[Varcom.pr.toInt()].set("Y");
			sv.agntbrOut[Varcom.pr.toInt()].set("Y");
			sv.aracdeOut[Varcom.pr.toInt()].set("Y");
			sv.saledeptOut[Varcom.pr.toInt()].set("Y");
			sv.agtypeOut[Varcom.pr.toInt()].set("Y");
			sv.useridOut[Varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "N")) {
			sv.leveltypOut[Varcom.pr.toInt()].set("Y");
			sv.leveldescOut[Varcom.pr.toInt()].set("Y");
			sv.clntselOut[Varcom.pr.toInt()].set("Y");
			sv.cltnameOut[Varcom.pr.toInt()].set("Y");
			sv.agntbrOut[Varcom.pr.toInt()].set("Y");
			sv.aracdeOut[Varcom.pr.toInt()].set("Y");
			sv.saledeptOut[Varcom.pr.toInt()].set("Y");
			sv.agtypeOut[Varcom.pr.toInt()].set("Y");
			sv.useridOut[Varcom.pr.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
		}
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
	}
	
	@Override
	protected void screenEdit2000() {

		sv.salestatus.set(SALESTATUS);
		
		loadDesc();
		setClntName();
		validate2010();
		
		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	private void setClntName() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(sv.clntsel.toString());
		Optional<Clntpf> op = Optional.ofNullable(clntpf);

		if (op.isPresent()) {
			sv.cltname.set(clntpf.getSurname() + " " + clntpf.getGivname());
		}
	}

	private void validate2010()
	{
		if (isEQ(wsspcomn.flag, "N")) {	
			return;
		}
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
		
		if (isEQ(wsspcomn.flag, "A") || isEQ(wsspcomn.flag, "B")) 
		{
			if (isEQ(sv.clntsel, SPACE))
				sv.clntselErr.set(E186);
			
			if (isEQ(sv.agntbr, SPACE))
				sv.agntbrErr.set(E186);
			
			if (isEQ(sv.saledept, SPACE))
				sv.saledeptErr.set(E186);
			
			if (isEQ(sv.agtype, SPACE))
				sv.agtypeErr.set(E186);
			
			if (isEQ(sv.userid, SPACE))
				sv.useridErr.set(E186);

			
			if (isNE(sv.saledept, SPACE) && isNE(sv.agntbr, SPACE))
			{
				readTjl68();
			
				if(isNE(sv.agntbr,tjl68rec.agntbr)) {
					sv.agntbrErr.set(JL85);
					sv.saledeptErr.set(JL85);
				}
			
				if(isNE(tjl68rec.aracde,SPACES) && (isNE(sv.aracde,tjl68rec.aracde) && isNE(sv.aracde, SPACE))){
					sv.aracdeErr.set(JL86);
					sv.saledeptErr.set(JL86);
				}
			}
			
			
			
			if (isNE(sv.userid, SPACE))
			{				
				usrdpf = usrdDAO.getUserid(sv.userid.trim());
				
				
				if (null == usrdpf)
				{
					sv.useridErr.set(E020);
				}

				if(null !=usrdpf)
				{
					
					 if(isEQ(usrdpf.getValidflag(),"2"))
						{
							sv.useridErr.set(JL94);
						}
					 
					SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");			
					date1.set(yyyyMMdd.format(usrdpf.getUenddate()));
					
					if(isGT(wsaaToday, date1)) 
						sv.useridErr.set(JL94);
					
				}

				List<Agsdpf> agsdpfList;
				
				agsdpfList = agsdpfDAO.getAgsdpfDatabyUserid(sv.userid.trim(),sv.levelno.trim());
				if(!agsdpfList.isEmpty())
				{
					sv.useridErr.set(JL87);

				}		
				
			}
			
		}
		
		if (isEQ(wsspcomn.flag, "A") && isNE(sv.clntsel, SPACE)) 
		{
			Agsdpf agsd = agsdpfDAO.getAgsdpfData(sv.clntsel.toString().trim());           
			Optional<Agsdpf> isExists = Optional.ofNullable(agsd);
			
			if (isExists.isPresent()) {
				
				sv.clntselErr.set(JL84);
				
				List<Clrrpf> clrrpfList1;
				clrrpfList1=clrrpfDAO.findClientHistByClntnum(sv.clntsel.toString().trim());
				for(Clrrpf clrrpf1 :clrrpfList1)
				{
					
					if (((isEQ(clrrpf1.getClrrrole().trim(),"SR") && isEQ(clrrpf1.getForepfx().trim(),"SR")) 
							|| (isEQ(clrrpf1.getClrrrole().trim(),"SB") && isEQ(clrrpf1.getForepfx().trim(),"SB")) 
							|| (isEQ(clrrpf1.getClrrrole().trim(),"SS") && isEQ(clrrpf1.getForepfx().trim(),"SS")))
						&& isEQ(clrrpf1.getUsed2b().trim(),"Y") )
					{
						sv.clntselErr.set(SPACE);
													
					}
				}
				
			
            }
		}
		
			
		if (isEQ(wsspcomn.flag, "B"))
		{
			if (isEQ(sv.reasonmod, SPACE))
				sv.reasonmodErr.set(E186);
			
			if (isEQ(sv.moddate, varcom.vrcmMaxDate) || isEQ(sv.moddate, SPACE))
				sv.moddateErr.set(E186);
			
			if (isNE(sv.moddate, varcom.vrcmMaxDate) && isGT(sv.moddate, wsaaToday)) 
				sv.moddateErr.set(HL01);

			if(isNE(sv.clntsel.toString().trim(),agsdpf.getClientno().trim()) && isNE(sv.clntsel, SPACE))
			{
				Agsdpf agsd = agsdpfDAO.getAgsdpfData(sv.clntsel.toString().trim());           
				Optional<Agsdpf> isExists = Optional.ofNullable(agsd);
				if (isExists.isPresent()) {
					
					sv.clntselErr.set(JL84);
					
					List<Clrrpf> clrrpfList1;
					clrrpfList1=clrrpfDAO.findClientHistByClntnum(sv.clntsel.toString().trim());
					for(Clrrpf clrrpf1 :clrrpfList1)
					{
						
						if (((isEQ(clrrpf1.getClrrrole().trim(),"SR") && isEQ(clrrpf1.getForepfx().trim(),"SR")) 
								|| (isEQ(clrrpf1.getClrrrole().trim(),"SB") && isEQ(clrrpf1.getForepfx().trim(),"SB")) 
								|| (isEQ(clrrpf1.getClrrrole().trim(),"SS") && isEQ(clrrpf1.getForepfx().trim(),"SS")))
							&& isEQ(clrrpf1.getUsed2b().trim(),"Y") )
						{
							sv.clntselErr.set(SPACE);
														
						}
					}
					
				
	            }
        
            }
		}
		
		if ((isEQ(wsspcomn.flag, "A") || isEQ(wsspcomn.flag, "B")) && isNE(sv.clntsel, SPACE))
		{
				clntpf = new Clntpf();
				clntpf.setClntpfx("CN");
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntnum(sv.clntsel.toString().trim()); 
				clntpf = clntpfDAO.selectActiveClient(clntpf);
			
				Optional<Clntpf> op1 = Optional.ofNullable(clntpf);

				if (!op1.isPresent()) 
				{	
					sv.clntselErr.set(F393);
				}
				else
				{
					if(clntpf.getCltstat().equals("DC"))
					{
						sv.clntselErr.set(F782);
					}
					
					else
					{
						if(!clntpf.getCltstat().equals("AC"))
						sv.clntselErr.set(JL89);
					}
				}
			
				clntpf = new Clntpf();
				clntpf = cltsioCall2700(sv.clntsel.toString());
				Optional<Clntpf> op = Optional.ofNullable(clntpf);

				if (isNE(sv.clntsel, SPACE) && !op.isPresent())
				sv.clntselErr.set(E058);
			
			
		}
		
		if (isEQ(wsspcomn.flag, "C")){
			
			if (isEQ(sv.reasondel, SPACE))
				sv.reasondelErr.set(E186);
			
			if (isEQ(sv.deldate, varcom.vrcmMaxDate) || isEQ(sv.deldate, SPACE))
				sv.deldateErr.set(E186);
			
			if (isNE(sv.deldate, varcom.vrcmMaxDate) && isGT(sv.deldate, wsaaToday)) 
				sv.deldateErr.set(HL01);
		}

		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

	}
	
	protected Clntpf cltsioCall2700(String clntNum) {

		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), clntNum);
	}
	
	@Override
	protected void update3000() {
		
		if (isEQ(wsspcomn.flag, "B")){
			updateDatabase3020();
			updateClntpf();
		}
		else if (isEQ(wsspcomn.flag, "C"))
			updateDatabase3030();
		else if (isEQ(wsspcomn.flag, "A")) {
			updateDatabase3010();
			updateClntpf();
			insertClrrpf();
		}
		else if (isEQ(wsspcomn.flag, "N")){
			updateDatabase3040();
		}
	}

	// Insert
	private void updateDatabase3010() {
		
		if(isEQ(sv.aracde,SPACES))
			wsaaArea.set("  ");	
		else	
			wsaaArea.set(sv.aracde.trim());
		
		wsaaBranch.set(sv.agntbr);
		wsaaDeptcode.set(sv.saledept);

		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass("1");
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString());
		agsdpf.setUserid(sv.userid.toString().trim());
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("1");
		agsdpf.setRegisclass("1");
		agsdpf.setEffectivedt(wsaaToday.toInt());
		agsdpf.setReasoncd(SPACE);
		agsdpf.setReasondtl(SPACE);
		agsdpf.setGenerationno("1");

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in Agsdpf :", ex);
		}

	}

	// Modify
	private void updateDatabase3020() {

		agsdpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			int wsaaGen=Integer.parseInt(agsdpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
		}
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("2");
		try {
			agsdpfDAO.updateAgsdpf(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update AGSDPF :", ex);
		}
		
		if(isEQ(sv.aracde,SPACES))
			wsaaArea.set("  ");	
		else	
			wsaaArea.set(sv.aracde.trim());
		
		wsaaBranch.set(sv.agntbr.trim());
		wsaaDeptcode.set(sv.saledept.trim());
		
		
		if(isNE(agsdpf.getClientno().trim(),sv.clntsel.trim()))
		{
			clrrpfDAO.updateClrrUsed2b("Y","9",agsdpf.getClientno().trim(),agsdpf.getLevelno().trim());
			insertClrrpf();
			
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno().trim());
			clntpfDAO.updateClntRoleflag(37, "", " ", clntpf);
			
		}
		
		agsdpf= new Agsdpf();
		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass("1");
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString().trim());
		agsdpf.setUserid(sv.userid.toString().trim());
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("1");
		agsdpf.setRegisclass("2");
		agsdpf.setEffectivedt(sv.moddate.toInt());
		agsdpf.setReasoncd(sv.reasonmod.toString());
		agsdpf.setReasondtl(sv.resndesc.toString().trim());
		agsdpf.setGenerationno(wsaaGennum);

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in AGSDPF :", ex);
		}
	}

	// Delete
	private void updateDatabase3030() {

		agsdpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			int wsaaGen=Integer.parseInt(agsdpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
		}
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("2");
		try {
			agsdpfDAO.updateAgsdpf(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update AGSDPF :", ex);
		}
		
		if(isNE(agsdpf.getClientno().trim(),sv.clntsel.trim()))
		{
			clrrpfDAO.updateClrrUsed2b("Y","9",agsdpf.getClientno().trim(),agsdpf.getLevelno().trim());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno().trim());
			clntpfDAO.updateClntRoleflag(37, "", " ", clntpf);
		}
		
		agsdpf= new Agsdpf();
		
		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass("1");
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString().trim());
		agsdpf.setUserid(sv.userid.toString().trim());
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("2");
		agsdpf.setRegisclass("3");
		agsdpf.setEffectivedt(sv.deldate.toInt());
		agsdpf.setReasoncd(sv.reasondel.toString());
		agsdpf.setReasondtl(sv.resndesc.toString());
		agsdpf.setGenerationno(wsaaGennum);

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in AGSDPF :", ex);
		}
	}


	private void updateClntpf() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(sv.clntsel.toString().trim());
		clntpfDAO.updateClntRoleflag(37, "Y", " ", clntpf);
	}
		
	private void insertClrrpf() {
		cltrelnDTO=new CltrelnDTO();
		cltrelnDTO.setClntcoy("9");
		cltrelnDTO.setClntpfx("CN");
		cltrelnDTO.setClntnum(sv.clntsel.toString().trim());
		cltrelnDTO.setClrrrole("SR");
		cltrelnDTO.setForepfx("SR");
		cltrelnDTO.setForecoy("2");
		cltrelnDTO.setForenum(sv.levelno.trim());
		clrrpfDAO.intsertClrrpf(cltrelnDTO);
		}
	
	protected void updateDatabase3040() {
		
		agsdpfList = agsdpfDAO.getRecbyLevelNo(sv.levelno.toString().trim());
		if(!agsdpfList.isEmpty()) {
			for (Agsdpf agsdpf : agsdpfList) {
				if( "1".equals(agsdpf.getValidflag())) {
					wsaahierCode.set(sv.levelno.toString().trim());
					wsaaroleCode.set("SR");
					agsdpf.setValidflag("2");
					try {
						agsdpfDAO.updateAgsdpfRec(agsdpf);
					} catch (Exception ex) {
						LOGGER.error("Failed to update AGSDPF :", ex);
					}
				}
			}

			/* Update Clrrpf */
			Clrrpf clrr = clrrpfDAO.getClrrForenum(sv.clntsel.toString(), sv.levelno.toString());
			Optional<Clrrpf> flag = Optional.ofNullable(clrr);
			if (flag.isPresent() && !"Y".equals(clrr.getUsed2b())) {
				clrr.setUsed2b("Y");
				try {
					clrrpfDAO.updateClrrUsed2b(clrr.getUsed2b(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getForenum());
				} catch (Exception ex) {
					LOGGER.error("Failed to update CLRRPF :", ex);
				}
			}
			
		}
		/* Update lower hierarchy inactive records */
		updateDatabase3050(wsaaKey.toString());
	}
	
	protected void updateDatabase3050(String levelno) {
		
		hierpfList = hierpfDAO.getAllAgntRec(levelno, levelno, "1", "2");
		if(!hierpfList.isEmpty()) {
			for(Hierpf hier : hierpfList) {
				hierpf = hierpfDAO.getHierpfData(hier.getAgentkey(), "2", "1");
				Optional<Hierpf> flag = Optional.ofNullable(hierpf);
				if (flag.isPresent()) {
					hierpf.setValidflag("2");
					try {
						hierpfDAO.updateHierpfDetails(hierpf);
					} catch (Exception ex) {
						LOGGER.error("Failed to update HIERPF :", ex);
					}
				}
			}
		}
	}
	
	@Override
	protected void whereNext4000() {
		wsspcomn.confirmationKey.set(sv.levelno);
		wsspcomn.programPtr.add(1);
	}

}
