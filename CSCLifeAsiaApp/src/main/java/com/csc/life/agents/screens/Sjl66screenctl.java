package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl66screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sjl66screensfl";
		lrec.subfileClass = Sjl66screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 18;
		lrec.pageSubfile = 20;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 5, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl66ScreenVars sv = (Sjl66ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl66screenctlWritten, sv.Sjl66screensflWritten, av, sv.sjl66screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl66ScreenVars screenVars = (Sjl66ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.slt.setClassString("");
		screenVars.aclntsel.setClassString("");
		screenVars.acltname.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.gender.setClassString("");				
		screenVars.dobDisp.setClassString("");
		screenVars.agnttyp.setClassString("");
		screenVars.liscno.setClassString("");
		screenVars.appdateDisp.setClassString("");
		screenVars.termdateDisp.setClassString("");
		screenVars.status.setClassString("");				
		screenVars.regclass.setClassString("");
		screenVars.regdateDisp.setClassString("");
		screenVars.agntdesc.setClassString("");
	}

/**
 * Clear all the variables in Sjl66screenctl
 */
	public static void clear(VarModel pv) {
		Sjl66ScreenVars screenVars = (Sjl66ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.slt.clear();
		screenVars.aclntsel.clear();
		screenVars.acltname.clear();
		screenVars.agntnum.clear();		
		screenVars.gender.clear();				
		screenVars.dobDisp.clear();
		screenVars.agnttyp.clear();
		screenVars.liscno.clear();
		screenVars.appdateDisp.clear();
		screenVars.termdateDisp.clear();		
		screenVars.status.clear();				
		screenVars.regclass.clear();
		screenVars.regdateDisp.clear();
		screenVars.agntdesc.clear();
	}
}
