package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:13
 * Description:
 * Copybook name: CHDRAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdragtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdragtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdragtKey = new FixedLengthStringData(64).isAPartOf(chdragtFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdragtAgntpfx = new FixedLengthStringData(2).isAPartOf(chdragtKey, 0);
  	public FixedLengthStringData chdragtAgntcoy = new FixedLengthStringData(1).isAPartOf(chdragtKey, 2);
  	public FixedLengthStringData chdragtAgntnum = new FixedLengthStringData(8).isAPartOf(chdragtKey, 3);
  	public FixedLengthStringData chdragtValidflag = new FixedLengthStringData(1).isAPartOf(chdragtKey, 11);
  	public FixedLengthStringData chdragtChdrcoy = new FixedLengthStringData(1).isAPartOf(chdragtKey, 12);
  	public FixedLengthStringData chdragtChdrnum = new FixedLengthStringData(8).isAPartOf(chdragtKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(chdragtKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdragtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdragtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}