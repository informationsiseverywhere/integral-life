/*
 * File: Pm508.java
 * Date: 30 August 2009 1:13:34
 * Author: Quipoz Limited
 * 
 * Class transformed from PM508.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.MacfinqTableDAM;
import com.csc.life.agents.screens.Sm508ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
*****************************************************************
* </pre>
*/
public class Pm508 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM508");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaSwitch = new FixedLengthStringData(1);
	private Validator wsaaFound = new Validator(wsaaSwitch, "1");
	private Validator wsaaNotFound = new Validator(wsaaSwitch, "0");
	private static final String t3692 = "T3692";
	private static final String clntrec = "CLNTREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private MacfinqTableDAM macfinqIO = new MacfinqTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Sm508ScreenVars sv = ScreenProgram.getScreenVars( Sm508ScreenVars.class);

	public Pm508() {
		super();
		screenVars = sv;
		new ScreenModel("Sm508", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		/* Obtain Agent Number.*/
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		sv.agntnum.set(aglfIO.getAgntnum());
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(aglfIO.getAgntcoy());
		agntIO.setAgntnum(aglfIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(wsspcomn.fsuco);
		clntIO.setClntnum(agntIO.getClntnum());
		clntIO.setFormat(clntrec);
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		/* MOVE 'E'                    TO NMAD-LANGUAGE.                */
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.desc.set(namadrsrec.name);
		sv.agtype.set(agntIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		/* MOVE TM602                  TO DESC-DESCTABL.        <LA4631>*/
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntIO.getAgtype());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.acdes.set(descIO.getLongdesc());
		subfileLoad1100();
		scrnparams.subfileRrn.set(1);
	}

protected void subfileLoad1100()
	{
		subfile1100();
	}

protected void subfile1100()
	{
		/* We first need to initialise the subfile - note, seeing as*/
		/* the IO module gets used all over the place now, it may have*/
		/* its own section (20000-SCREEN-IO)*/
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		screenIo20000();
		/* Just read the MACFINQ file sequentially until no more records   */
		/* OR the AGENT changes*/
		macfinqIO.setDataArea(SPACES);
		macfinqIO.setAgntcoy(aglfIO.getAgntcoy());
		macfinqIO.setAgntnum(aglfIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);
		macfinqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");
		macfinqIO.setFormat(SPACES);
		macfinqIO.setStatuz(varcom.oK);
		while ( !(isEQ(macfinqIO.getStatuz(), varcom.endp))) {
			getTheMacfinq1200();
		}
		
	}

protected void getTheMacfinq1200()
	{
		getNext1200();
	}

protected void getNext1200()
	{
		/* Read the file, if there's a record, load it into the next*/
		/* subfile.*/
		macfinqio8000();
		macfinqIO.setFunction(varcom.nextr);
		if (isNE(macfinqIO.getStatuz(), varcom.oK)
		&& isNE(macfinqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(macfinqIO.getParams());
			fatalError600();
		}
		if (isEQ(macfinqIO.getStatuz(), varcom.endp)
		|| isNE(macfinqIO.getAgntcoy(), aglfIO.getAgntcoy())
		|| isNE(macfinqIO.getAgntnum(), aglfIO.getAgntnum())) {
			macfinqIO.setStatuz(varcom.endp);
			return ;
		}
		sv.agmvty.set(macfinqIO.getAgmvty());
		/* MOVE MACFINQ-MLAGTTYP          TO SM508-MLAGTTYP.    <LA3386>*/
		sv.accountType.set(macfinqIO.getMlagttyp());
		/*    MOVE MACFINQ-REPORTAG01        TO SM508-REPORTAG-01. <LA1174>*/
		/*    MOVE MACFINQ-REPORTAG02        TO SM508-REPORTAG-02. <LA1174>*/
		/*    MOVE MACFINQ-REPORTAG03        TO SM508-REPORTAG-03. <LA1174>*/
		sv.reportag01.set(macfinqIO.getZrptga());
		sv.reportag02.set(macfinqIO.getZrptgb());
		sv.reportag03.set(macfinqIO.getZrptgc());
		if (isEQ(macfinqIO.getEffdate(), ZERO)) {
			sv.benCessDate.set(varcom.vrcmMaxDate);
		}
		else {
			sv.benCessDate.set(macfinqIO.getEffdate());
		}
		scrnparams.function.set(varcom.sadd);
		screenIo20000();
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			/** The following GO TO has been highlighted as it may ca use th    */
			/** program to logically error. Therefore, revise the log ic.       */
			/** If you want the program to skip the 2000- section inc lude t    */
			/** following line:                                                 */
			/**    MOVE 3000                TO WSSP-SECTIONNO.                  */
			/** You must also always include the following line, when           */
			/** performing an external section:                                 */
			/**    GO TO PRE-EXIT.                                              */
			/****      GO TO 2300-VALIDATE                              <S19FIX>*/
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/* CALL 'SM508IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                   SM508-DATA-AREA            */
		/*                                   SM508-SUBFILE-AREA.        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		scrnparams.function.set(varcom.srnch);
		sv.subfileArea.set(SPACES);
		processScreen("SM508", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			return ;
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED
	* </pre>
	*/
protected void update3000()
	{
		/*CONT*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
				wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			}
			else {
				wsspcomn.programPtr.add(1);
				wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			}
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}

protected void macfinqio8000()
	{
		/*IO*/
		macfinqIO.setFormat(SPACES);
		SmartFileCode.execute(appVars, macfinqIO);
		/*EXIT*/
	}

protected void screenIo20000()
	{
		/*SCREEN*/
		/*    CALL 'SM508IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      SM508-DATA-AREA            */
		/*                                      SM508-SUBFILE-AREA.        */
		processScreen("SM508", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}
}
