/*
 * File: Sr58pscreen.java
 * Date: {{DATETIME}}
 * Author: Automated screen Code template
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.COBOLAppVars;

/**
 * Screen record for SCREEN Sr58p
 * @version 1.0 generated on {{TIMESTAMP}}
 * @author CSC
 */
public class Sr58pscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {0, 0, 0, 0}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr58pScreenVars sv = (Sr58pScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr58pscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr58pScreenVars screenVars = ( Sr58pScreenVars )pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.prcent.setClassString("");
	}
	
	/**
	 * Clear all the variables in S2500screen
	 */
	public static void clear(VarModel pv) {
		Sr58pScreenVars screenVars = (Sr58pScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.prcent.clear();
	}
}	
