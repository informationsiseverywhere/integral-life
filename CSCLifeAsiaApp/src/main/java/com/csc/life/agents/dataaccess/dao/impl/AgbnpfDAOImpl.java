package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.agents.dataaccess.dao.AgbnpfDAO;
import com.csc.life.agents.dataaccess.model.Agbnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgbnpfDAOImpl extends BaseDAOImpl<Agbnpf> implements AgbnpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgbnpfDAOImpl.class);

	public List<Agbnpf> searchAgbnpfRecord(String agntcoy,String agntnum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT AGNTCOY,AGNTNUM FROM AGBNPF WHERE AGNTCOY= ? AND AGNTNUM = ?");
		PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		ResultSet rs = null;
		List<Agbnpf> outputList = new ArrayList<Agbnpf>();
		Agbnpf agbnpf = null;
		try {
			/*ps.setString(1, agntcoy);
			ps.setString(2, agntnum);*/
			ps.setString(1, StringUtil.fillSpace(agntcoy.trim(), DD.agntcoy.length)); 
			ps.setString(2, StringUtil.fillSpace(agntnum.trim(), DD.agntnum.length)); 
			rs = executeQuery(ps);
			while (rs.next()) {
				agbnpf = new Agbnpf();
				agbnpf.setAgntcoy(agntcoy);
				agbnpf.setAgntnum(agntnum);
				outputList.add(agbnpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgbnpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return outputList;
	}

	
}