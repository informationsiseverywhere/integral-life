/*
 * File: Cr673.java
 * Date: 30 August 2009 2:59:23
 * Author: $Id$
 * 
 * Class transformed from CR673.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.cls;

import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.FileCode;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cr673 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData frlibrary = new FixedLengthStringData(10);
	private FixedLengthStringData frfilnm = new FixedLengthStringData(10);
	private FixedLengthStringData tolibrary = new FixedLengthStringData(10);
	private FixedLengthStringData tofilnm = new FixedLengthStringData(10);

	public Cr673() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		tofilnm = convertAndSetParam(tofilnm, parmArray, 4);
		tolibrary = convertAndSetParam(tolibrary, parmArray, 3);
		frfilnm = convertAndSetParam(frfilnm, parmArray, 2);
		frlibrary = convertAndSetParam(frlibrary, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int copy = 2;
		final int delt = 3;
		final int returnVar = 4;
		while (qState != QS_END) {
			switch (qState) {
			case QS_START: {
				try {
					statuz.set("****");
				}
				catch (ExtMsgException ex){
					if (ex.messageMatches("CPF0000")
					|| ex.messageMatches("LBE0000")) {
						qState = error;
						break;
					}
					else {
						throw ex;
					}
				}
			}
			case copy: {
				try {
					FileCode.copyTable(frlibrary + "." + frfilnm, tolibrary + "." + tofilnm, null, null, COBOLAppVars.REPLACE, true, null, null, null, null, null);
				}
				catch (ExtMsgException ex){
					if (ex.messageMatches("CPF2869")
					|| ex.messageMatches("CPF2817")) {
						qState = delt;
						break;
					}
					else {
						if (ex.messageMatches("CPF0000")
						|| ex.messageMatches("LBE0000")) {
							qState = error;
							break;
						}
						else {
							throw ex;
						}
					}
				}
				qState = returnVar;
				break;
			}
			case delt: {
				FileCode.deleteFile(tolibrary + "." + tofilnm);
				qState = copy;
				break;
			}
			case returnVar: {
				return ;
			}
			case error: {
				appVars.sendMessageToQueue("Unexpected errors occurred", "*");
				statuz.set("BOMB");
				qState = returnVar;
				break;
			}
			default:{
				qState = QS_END;
			}
			}
		}
		
	}
}
