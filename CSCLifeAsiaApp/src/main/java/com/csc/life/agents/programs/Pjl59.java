package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl59ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pjl59 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl59.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl59");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl59ScreenVars sv = ScreenProgram.getScreenVars(Sjl59ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private Batckey wsaaBatckey = new Batckey();	
	private FixedLengthStringData wsaaRecordFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoRecordFound = new Validator(wsaaRecordFoundFlag, "N");
	private Validator wsaaRecordFound = new Validator(wsaaRecordFoundFlag, "Y");
	private FixedLengthStringData wsaaFirsttime = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaStatus= new FixedLengthStringData(1);
	private boolean isFilterSame;
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private List<Agsdpf> agsdpfList = new ArrayList<>();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private FixedLengthStringData action = new FixedLengthStringData(1);

	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String T3692 = "T3692";
	private static final String TJL68 = "TJL68";
	private static final String TJL76 = "TJL76";
	private static final String TJL69 = "TJL69";
	private List<Descpf> tjl76List;
	private List<Descpf> tjl69List;
	private List<Descpf> t3692List;
	private String sjl59 = "Sjl59";
	private Boolean flag = true;

	public Pjl59() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl59", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		wsaaBatckey.set(wsspcomn.batchkey);	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		 if(isNE(action,SPACE)){
			    wsspcomn.sbmaction.set(action);
			    action.set(SPACE);
			   }
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		initializeWsaaVariables();
		initSubfile1200();
		initialise1010();
		protect1020();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(sjl59, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	private void initializeWsaaVariables() {
		initialize(wsaaStatus);
	}
	
	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyDept(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			if (null != agsdpf.getSalesdiv()){
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
				initialise1020();
			}
		}
	}
	
	protected void initialise1020() {
		
		agsdpfList = agsdpfDAO.getRecordsbyDept(wsspcomn.chdrCownnum.toString().trim());
		if(!agsdpfList.isEmpty()) {
			for(Agsdpf agsd : agsdpfList) {
				sv.clntsel.set(agsd.getClientno());
				clntpf = new Clntpf();
				clntpf = cltsioCall2700(agsd.getClientno());
				Optional<Clntpf> op = Optional.ofNullable(clntpf);
				if (op.isPresent()) {
					sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
				}
				if(null != agsd.getLevelno()) {
					sv.levelno.set(agsd.getLevelno());
				}
				if (null != agsd.getLevelclass())
				{
					sv.leveltype.set(agsd.getLevelclass());
					loadDesc();	
				}
				if (null != agsd.getAgentclass()) {
					t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
							agsd.getAgentclass().trim(),wsspcomn.language.toString());
					if(!t3692List.isEmpty()) {
						for (Descpf descItem : t3692List) {
							sv.agtdesc.set(descItem.getLongdesc());		
						}
					}
				}
				if(null != agsd.getUserid()) {
					sv.userid.set(agsd.getUserid());
				}
				if(null != agsd.getActivestatus()) {
					tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
							agsd.getActivestatus().trim(),wsspcomn.language.toString());
					if(!tjl69List.isEmpty()) {
						for (Descpf descItem : tjl69List) {
							sv.status.set(descItem.getLongdesc());		
						}
					}
				}
				tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
						agsd.getRegisclass().trim(),wsspcomn.language.toString());
					if(!tjl76List.isEmpty()) {
						for (Descpf descItem : tjl76List) {
						sv.regclass.set(descItem.getLongdesc());		
					}
				}
				if(null != agsd.getReasoncd()) {
					sv.reasonreg.set(agsd.getReasoncd());
				}
				if(null != agsd.getReasondtl()) {
					sv.resndetl.set(agsd.getReasondtl());
				}
				if(null != agsd.getEffectivedt()) {
					sv.regdate.set(agsd.getEffectivedt());
				}
				scrnparams.function.set(Varcom.sadd);
				processScreen(sjl59, sv);
				if (isNE(scrnparams.statuz, Varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
		wsaaFirsttime.set("N");
	}
		
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			if("2".equals(sv.leveltype.toString())){
				sv.leveldes.set(tjl70Desc.getLongdesc().substring(0, 12));
			}else {
				sv.leveldes.set(tjl70Desc.getLongdesc());
			}
		}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	

	protected void protect1020() {
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(Varcom.oK);
		screenIo2010();
	}
	
	protected void screenIo2010()
	{	
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		screenEditSrnch();
		checkForRecordSelect2030();
		if(!wsaaRecordFound.isTrue()) {
			wsspwindow.value.set(SPACES);
		}
		if (wsaaRecordFound.isTrue() || isFilterSame) {
			isFilterSame = false;
			return;
		}
		filterSearch();
		saveNewFilters2060();
		scrnparams.subfileRrn.set(1);
	}
	
	protected void filterSearch() {
		
		scrnparams.function.set(Varcom.sclr);
		screenIo9000();
		if("1".equals(sv.stattype.toString())) {
			searchActRec();
			wsaaFirsttime.set("Y");
		}
		else if("2".equals(sv.stattype.toString())) {
			searchHisRec();
			wsaaFirsttime.set("Y");
		}
		else if(isEQ(wsaaFirsttime, "Y"))  {
			initialise1020();
			flag = false;
		}
		else {
			initialise1020();
			return;
		}
		wsspcomn.edterror.set("Y");
	}
	
	protected void saveNewFilters2060()
	{
		/* Move the new START and SCAN filters to WS save areas.*/
		wsaaStatus.set(sv.stattype);
		if(isEQ(wsaaFirsttime, "Y")) {
			wsspcomn.edterror.set("Y");
		}
		if(isEQ(wsaaFirsttime, "N") && flag) {
			return;
		}
		if(isEQ(wsaaFirsttime, "N") && !flag) {
			wsspcomn.edterror.set("Y");
			flag = true;
		}
	}
	
	protected void screenEditSrnch() {
		
		scrnparams.function.set(Varcom.srnch);
		processScreen(sjl59, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void checkForRecordSelect2030() {
		
		wsaaNoRecordFound.setTrue();
		while ( !(wsaaRecordFound.isTrue()
		|| isEQ(scrnparams.statuz, Varcom.endp))) {
			checkForSelection2100();
		}
		
		if (wsaaRecordFound.isTrue()) {
			loadWsspData2200();                                        
			return;
		}
		checkFilterIfSame();
	}
	
	protected void checkForSelection2100()
	{
		if (isNE(sv.slt, SPACES)) {
			wsaaRecordFound.setTrue();
		}
	}
	
	protected void loadWsspData2200()
	{
		check2200();
	}

	protected void check2200()
	{
		wsspwindow.value.set(sv.stattype);
		wsspwindow.confirmation.set(sv.stattype);
	}
	
	protected void checkFilterIfSame() {
		
		boolean isFilterSame1 = false;
		if (isEQ(sv.stattype, wsaaStatus)) {
			isFilterSame1 = true;
		}
		if(isFilterSame1 && isEQ(sv.stattype, wsaaStatus)) {
			isFilterSame1 = true;
		}else {
			isFilterSame1 = false;
		}
	}
	
	protected void searchActRec() {
		
		agsdpfList = agsdpfDAO.getDeptRec(wsspcomn.chdrCownnum.toString().trim(), "1");
		if(!agsdpfList.isEmpty()) {
			for(Agsdpf agsd : agsdpfList) {
				sv.clntsel.set(agsd.getClientno());
				clntpf = new Clntpf();
				clntpf = cltsioCall2700(agsd.getClientno());
				Optional<Clntpf> op = Optional.ofNullable(clntpf);
				if (op.isPresent()) {
					sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
				}
				if(null != agsd.getLevelno()) {
					sv.levelno.set(agsd.getLevelno());
				}
				if (null != agsd.getLevelclass())
				{
					sv.leveltype.set(agsd.getLevelclass());
					loadDesc();	
				}
				if (null != agsd.getAgentclass()) {
					t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
							agsd.getAgentclass().trim(),wsspcomn.language.toString());
					if(!t3692List.isEmpty()) {
						for (Descpf descItem : t3692List) {
							sv.agtdesc.set(descItem.getLongdesc());		
						}
					}
				}
				if(null != agsd.getUserid()) {
					sv.userid.set(agsd.getUserid());
				}
				if(null != agsd.getActivestatus()) {
					tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
							agsd.getActivestatus().trim(),wsspcomn.language.toString());
					if(!tjl69List.isEmpty()) {
						for (Descpf descItem : tjl69List) {
							sv.status.set(descItem.getLongdesc());		
						}
					}
				}
				tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
						agsd.getRegisclass().trim(),wsspcomn.language.toString());
					if(!tjl76List.isEmpty()) {
						for (Descpf descItem : tjl76List) {
						sv.regclass.set(descItem.getLongdesc());		
					}
				}
				if(null != agsd.getReasoncd()) {
					sv.reasonreg.set(agsd.getReasoncd());
				}
				if(null != agsd.getReasondtl()) {
					sv.resndetl.set(agsd.getReasondtl());
				}
				if(null != agsd.getEffectivedt()) {
					sv.regdate.set(agsd.getEffectivedt());
				}
				scrnparams.function.set(Varcom.sadd);
				processScreen(sjl59, sv);
				if (isNE(scrnparams.statuz, Varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}
	
	protected void searchHisRec() {
	
		agsdpfList = agsdpfDAO.getDeptRec(wsspcomn.chdrCownnum.toString().trim(), "2");
		if(!agsdpfList.isEmpty()) {
			for(Agsdpf agsd : agsdpfList) {
				sv.clntsel.set(agsd.getClientno());
				clntpf = new Clntpf();
				clntpf = cltsioCall2700(agsd.getClientno());
				Optional<Clntpf> op = Optional.ofNullable(clntpf);
				if (op.isPresent()) {
					sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
				}
				if(null != agsd.getLevelno()) {
					sv.levelno.set(agsd.getLevelno());
				}
				if (null != agsd.getLevelclass())
				{
					sv.leveltype.set(agsd.getLevelclass());
					loadDesc();	
				}
				if (null != agsd.getAgentclass()) {
					t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
							agsd.getAgentclass().trim(),wsspcomn.language.toString());
					if(!t3692List.isEmpty()) {
						for (Descpf descItem : t3692List) {
							sv.agtdesc.set(descItem.getLongdesc());		
						}
					}
				}
				if(null != agsd.getUserid()) {
					sv.userid.set(agsd.getUserid());
				}
				if(null != agsd.getActivestatus()) {
					tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
							agsd.getActivestatus().trim(),wsspcomn.language.toString());
					if(!tjl69List.isEmpty()) {
						for (Descpf descItem : tjl69List) {
							sv.status.set(descItem.getLongdesc());		
						}
					}
				}
				tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
						agsd.getRegisclass().trim(),wsspcomn.language.toString());
					if(!tjl76List.isEmpty()) {
						for (Descpf descItem : tjl76List) {
						sv.regclass.set(descItem.getLongdesc());		
					}
				}
				if(null != agsd.getReasoncd()) {
					sv.reasonreg.set(agsd.getReasoncd());
				}
				if(null != agsd.getReasondtl()) {
					sv.resndetl.set(agsd.getReasondtl());
				}
				if(null != agsd.getEffectivedt()) {
					sv.regdate.set(agsd.getEffectivedt());
				}
				scrnparams.function.set(Varcom.sadd);
				processScreen(sjl59, sv);
				if (isNE(scrnparams.statuz, Varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}
	
	protected void screenIo9000(){
		processScreen(sjl59, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000() {
		return;
	}
	
	@Override
	@SuppressWarnings("static-access")
	protected void whereNext4000() {
		clearSavedFilter();
		wsspcomn.nextprog.set(wsaaProg); 
		wsspcomn.programPtr.add(2);
	}
	private void clearSavedFilter() {
		wsaaStatus.clear();
	}
}