package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:01
 * Description:
 * Copybook name: RLAGPRDREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rlagprdrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rlagprdRec = new FixedLengthStringData(65);
  	public FixedLengthStringData agntcoy = new FixedLengthStringData(1).isAPartOf(rlagprdRec, 0);
  	public FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(rlagprdRec, 1);
  	public FixedLengthStringData actyear = new FixedLengthStringData(4).isAPartOf(rlagprdRec, 9);
  	public FixedLengthStringData actmnth = new FixedLengthStringData(2).isAPartOf(rlagprdRec, 13);
  	public FixedLengthStringData agtype = new FixedLengthStringData(2).isAPartOf(rlagprdRec, 15);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rlagprdRec, 17);
  	public PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(rlagprdRec, 20);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(rlagprdRec, 25);
  	public ZonedDecimalData origamt = new ZonedDecimalData(17, 2).isAPartOf(rlagprdRec, 30);
  	public FixedLengthStringData prdflg = new FixedLengthStringData(1).isAPartOf(rlagprdRec, 47);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rlagprdRec, 48);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(rlagprdRec, 52);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rlagprdRec, 53);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rlagprdRec, 61);


	public void initialize() {
		COBOLFunctions.initialize(rlagprdRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rlagprdRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}