package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:09
 * Description:
 * Copybook name: ZBNWAGTREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwagtrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwagtRec = new FixedLengthStringData(462);
  	public FixedLengthStringData bnwagtTrcde = new FixedLengthStringData(3).isAPartOf(bnwagtRec, 0);
  	public FixedLengthStringData bnwagtSource = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 3);
  	public FixedLengthStringData bnwagtCompany = new FixedLengthStringData(3).isAPartOf(bnwagtRec, 13);
  	public FixedLengthStringData bnwagtAgency = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 16);
  	public FixedLengthStringData bnwagtAgntnum = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 26);
  	public FixedLengthStringData bnwagtAgntlnm = new FixedLengthStringData(50).isAPartOf(bnwagtRec, 36);
  	public FixedLengthStringData bnwagtAgntfnm = new FixedLengthStringData(50).isAPartOf(bnwagtRec, 86);
  	public FixedLengthStringData bnwagtAgnttyp = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 136);
  	public FixedLengthStringData bnwagtAddr01 = new FixedLengthStringData(60).isAPartOf(bnwagtRec, 137);
  	public FixedLengthStringData bnwagtAddr02 = new FixedLengthStringData(60).isAPartOf(bnwagtRec, 197);
  	public FixedLengthStringData bnwagtAddr03 = new FixedLengthStringData(60).isAPartOf(bnwagtRec, 257);
  	public FixedLengthStringData bnwagtCity = new FixedLengthStringData(50).isAPartOf(bnwagtRec, 317);
  	public FixedLengthStringData bnwagtState = new FixedLengthStringData(2).isAPartOf(bnwagtRec, 367);
  	public FixedLengthStringData bnwagtZip = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 369);
  	public FixedLengthStringData bnwagtCountry = new FixedLengthStringData(2).isAPartOf(bnwagtRec, 379);
  	public FixedLengthStringData bnwagtFiller1 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 381).init(SPACES);
  	public FixedLengthStringData bnwagtFiller2 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 382).init(SPACES);
  	public FixedLengthStringData bnwagtFiller3 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 383).init(SPACES);
  	public FixedLengthStringData bnwagtFiller4 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 384).init(SPACES);
  	public FixedLengthStringData bnwagtFiller5 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 385).init(SPACES);
  	public FixedLengthStringData bnwagtFiller6 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 386).init(SPACES);
  	public FixedLengthStringData bnwagtFiller7 = new FixedLengthStringData(1).isAPartOf(bnwagtRec, 387).init(SPACES);
  	public FixedLengthStringData bnwagtDob = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 388);
  	public FixedLengthStringData bnwagtTaxid = new FixedLengthStringData(34).isAPartOf(bnwagtRec, 398);
  	public FixedLengthStringData bnwagtStrtdate = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 432);
  	public FixedLengthStringData bnwagtTrmdate = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 442);
  	public FixedLengthStringData bnwagtProcdate = new FixedLengthStringData(10).isAPartOf(bnwagtRec, 452);


	public void initialize() {
		COBOLFunctions.initialize(bnwagtRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwagtRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}