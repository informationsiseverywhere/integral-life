package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:01
 * Description:
 * Copybook name: T6657REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6657rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6657Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData bankcode = new FixedLengthStringData(2).isAPartOf(t6657Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(t6657Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6657Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6657Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}