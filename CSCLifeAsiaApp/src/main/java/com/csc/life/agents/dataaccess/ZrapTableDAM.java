package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrapTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:22
 * Class transformed from ZRAP.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrapTableDAM extends ZrappfTableDAM {

	public ZrapTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZRAP");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "REPORTAG, " +
		            "ZRORCODE, " +
		            "PRCNT, " +
		            "EFFDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               reportag,
                               zrorcode,
                               prcnt,
                               effdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(agntcoy.toInternal());
	nonKeyFiller2.setInternal(agntnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(75);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getReportag().toInternal()
					+ getZrorcode().toInternal()
					+ getPrcnt().toInternal()
					+ getEffdate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, reportag);
			what = ExternalData.chop(what, zrorcode);
			what = ExternalData.chop(what, prcnt);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getReportag() {
		return reportag;
	}
	public void setReportag(Object what) {
		reportag.set(what);
	}	
	public FixedLengthStringData getZrorcode() {
		return zrorcode;
	}
	public void setZrorcode(Object what) {
		zrorcode.set(what);
	}	
	public PackedDecimalData getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(Object what) {
		setPrcnt(what, false);
	}
	public void setPrcnt(Object what, boolean rounded) {
		if (rounded)
			prcnt.setRounded(what);
		else
			prcnt.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		reportag.clear();
		zrorcode.clear();
		prcnt.clear();
		effdate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}