/*
 * File: PXXXX.java
 * Date: {{dd-mm-yyyy}}
 * Author: Autoated Code template
 * 
 * Class transformed from {{template name}}
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
//package life.agents;
package com.csc.life.agents.programs;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
//import com.csc.life.productdefinition.tablestructures.Th549rec;
import com.csc.life.agents.tablestructures.Tr58orec;
import com.csc.smart.recordstructures.Wsspsmart;

import com.csc.smart400framework.parent.ScreenProgram;
//import com.csc.life.productdefinition.screens.Sh549ScreenVars;
import com.csc.life.agents.screens.Sr58oScreenVars;

import com.csc.smart400framework.parent.ScreenProgCS;
//import com.csc.life.productdefinition.procedures.Th549pt;
import com.csc.life.agents.procedures.Tr58opt;
import com.quipoz.COBOLFramework.GOTOInterface;

/********************************************
 * includes for IO classes
 *******************************************/

/**
 * <pre>
 *  Generation Parameters - SCRVER(02)            Do Not Delete|
 * 
 * (c) Copyright Continuum Corporation Ltd.  1986....1995.
 *     All rights reserved.  Continuum Confidential.
 * 
 * REMARKS.
 *  REPLACE BY TABLE DESCRIPTION.
 * 
 * 
 ***************************************************************** 
 * </pre>
 */
public class Pr58o extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5)
			.init("PR58O");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2)
			.init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(
			575);
	private FixedLengthStringData errors = new FixedLengthStringData(16);
	private FixedLengthStringData F838 = new FixedLengthStringData(4)
			.isAPartOf(errors, 0).init("F838");

	/* Logical File: Extra data screen */
	private DescTableDAM descIO = new DescTableDAM();
	/* Dated items by from date */
	private ItdmTableDAM itmdIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr58orec tr58orec = new Tr58orec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58oScreenVars sv = ScreenProgram
			.getScreenVars(Sr58oScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, generalArea1045, preExit, exit2090, other3080, exit3090
	}

	public Pr58o() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58o", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams,
				parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray,
				1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea,
				parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

	protected void initialise1010() {
		/* INITIALISE-SCREEN */
		sv.dataArea.set(SPACES);
		/* READ-PRIMARY-RECORD */
		/* READ-RECORD */
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		FixedLengthStringData readmode = (isEQ(wsspcomn.flag, "I")) ? varcom.readr
				: varcom.readh;
		itmdIO.setFunction(readmode);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/* READ-SECONDARY-RECORDS */
	}

	protected void readRecord1031() {
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItempfx());
		descIO.setDesccoy(itmdIO.getItemcoy());
		descIO.setDesctabl(itmdIO.getItemtabl());
		descIO.setDescitem(itmdIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
				&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

	protected void moveToScreen1040() {

		sv.company.set(itmdIO.getItemcoy());
		sv.tabl.set(itmdIO.getItemtabl());
		sv.item.set(itmdIO.getItemitem());
		sv.itmfrm.set(itmdIO.getItmfrm());
		sv.itmto.set(itmdIO.getItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		// *****************************************
		// * sets the data string
		tr58orec.tr58oRec.set(itmdIO.getGenarea());
		if (isNE(itmdIO.getGenarea(), SPACES)) {
			// goTo(GotoLabel.generalArea1045);
		}

	}

	protected void generalArea1045() {
		// ********************************************
		// * Need to check if this is Generic

		if (isEQ(itmdIO.getItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		} else {
			sv.itmfrm.set(itmdIO.getItmfrm());
		}
		if (isEQ(itmdIO.getItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		} else {
			sv.itmto.set(itmdIO.getItmto());
		}
		sv.totamnt01.set(tr58orec.totamnt01);
		sv.totamnt02.set(tr58orec.totamnt02);
		sv.totamnt03.set(tr58orec.totamnt03);
		sv.totamnt04.set(tr58orec.totamnt04);
		sv.totamnt05.set(tr58orec.totamnt05);
		sv.totamnt06.set(tr58orec.totamnt06);

		sv.prcnt01.set(tr58orec.prcnt01);
		sv.prcnt02.set(tr58orec.prcnt02);
		sv.prcnt03.set(tr58orec.prcnt03);
		sv.prcnt04.set(tr58orec.prcnt04);
		sv.prcnt05.set(tr58orec.prcnt05);
		sv.prcnt06.set(tr58orec.prcnt06);

		sv.oppc01.set(tr58orec.oppc01);
		sv.oppc02.set(tr58orec.oppc02);

		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
	}

	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}

	}

	protected void screenEdit2000() {
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		// *********************************
		// Put all your validation code here - same as ORD
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl("T1693");
		itemIO.setItemitem(sv.company);
		itemIO.setFunction(varcom.readr);

		itemIO.execute();

		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			sv.company.setReverse(BaseScreenData.REVERSED);
			sv.company.setColor(BaseScreenData.RED);
			sv.companyErr.set(F838);
			wsspcomn.edterror.set("Y");
		}

		// ******** if any errors found set this
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}

	}

	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			return;
		}
		updatePrimaryRecord3050();
		updateRecord3055();

	}

	protected void updatePrimaryRecord3050() {
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setTranid(varcom.vrcmCompTranid);
	}

	protected void updateRecord3055() {
		itmdIO.setTableprog(wsaaProg);
		itmdIO.setGenarea(tr58orec.tr58oRec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

	protected void checkChanges3100() {
		check3100();
	}

	protected void check3100() {

		if (isNE(sv.totamnt01, tr58orec.totamnt01)) {
			tr58orec.totamnt01.set(sv.totamnt01);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.totamnt02, tr58orec.totamnt02)) {
			tr58orec.totamnt02.set(sv.totamnt02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.totamnt03, tr58orec.totamnt03)) {
			tr58orec.totamnt03.set(sv.totamnt03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.totamnt04, tr58orec.totamnt04)) {
			tr58orec.totamnt04.set(sv.totamnt04);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.totamnt05, tr58orec.totamnt05)) {
			tr58orec.totamnt05.set(sv.totamnt05);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.totamnt06, tr58orec.totamnt06)) {
			tr58orec.totamnt06.set(sv.totamnt06);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt01, tr58orec.prcnt01)) {
			tr58orec.prcnt01.set(sv.prcnt01);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt02, tr58orec.prcnt02)) {
			tr58orec.prcnt02.set(sv.prcnt02);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt03, tr58orec.prcnt03)) {
			tr58orec.prcnt03.set(sv.prcnt03);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt04, tr58orec.prcnt04)) {
			tr58orec.prcnt04.set(sv.prcnt04);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt05, tr58orec.prcnt05)) {
			tr58orec.prcnt05.set(sv.prcnt05);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnt06, tr58orec.prcnt06)) {
			tr58orec.prcnt06.set(sv.prcnt06);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.oppc01, tr58orec.oppc01)) {
			tr58orec.oppc01.set(sv.oppc01);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.oppc02, tr58orec.oppc02)) {
			tr58orec.oppc02.set(sv.oppc02);
			wsaaUpdateFlag = "Y";
		}
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}

	protected void callPrintProgram5000() {
		/* START */
		callProgram(Tr58opt.class, wsaaTablistrec);
		/* EXIT */
	}

}
