/*
 * File: Br524.java
 * Date: 29 August 2009 22:14:55
 * Author: Quipoz Limited
 *
 * Class transformed from BR524.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Getclnt;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Getclntrec;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.financials.dataaccess.PreqTableDAM;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.dataaccess.BwrkTableDAM;
import com.csc.fsu.clients.dataaccess.ClbaTableDAM;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Chkbkcd;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Chkbkcdrec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.life.agents.dataaccess.AgcsTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.agents.tablestructures.T6657rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Conerrrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*REMARKS.
*
*  This program is for consolidation of Agent Payments
*
*
****************************************************************
* </pre>
*/
public class Br524 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10).isAPartOf(wsaaInit, 0);
	private FixedLengthStringData wsaaTime = new FixedLengthStringData(10).isAPartOf(wsaaInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaInit, 20);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private PackedDecimalData wsaaMaxDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private static final String wsaaAuth = "AU";
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();

	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1).init("Y");
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");
	private PackedDecimalData wsaaTotalAmount = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* WSAA-PREV-KEY */
	private FixedLengthStringData wsaaPrevPayclt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPrevSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevSacstyp = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevCurrcode = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGenlpfx = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGenlcoy = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGenlcur = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlcode = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
	private static final String preqrec = "PREQREC";
	private static final String cheqrec = "CHEQREC";
	private static final String agcsrec = "AGCSREC";
	private static final String e141 = "E141";
	private static final String e192 = "E192";
	private static final String e193 = "E193";
	private static final String e289 = "E289";
	private static final String t5645 = "T5645";
	private static final String t6657 = "T6657";
	private static final String tr393 = "TR393";
	private static final String t3688 = "T3688";
	private static final String t3583 = "T3583";
	private static final String t3629 = "T3629";
	private static final String t3672 = "T3672";
	private static final String t5690 = "T5690";
		/* CONTROL-TOTALS */
	private static final int ct07 = 7;
	private static final int ct08 = 8;

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AgcsTableDAM agcsIO = new AgcsTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private BwrkTableDAM bwrkIO = new BwrkTableDAM();
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private ClbaTableDAM clbaIO = new ClbaTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PreqTableDAM preqIO = new PreqTableDAM();
	private Chkbkcdrec chkbkcdrec = new Chkbkcdrec();
	private Glkeyrec glkeyrec = new Glkeyrec();
	private T3672rec t3672rec = new T3672rec();
	private T3688rec t3688rec = new T3688rec();
	private T5645rec t5645rec = new T5645rec();
	private T5690rec t5690rec = new T5690rec();
	private Tr393rec tr393rec = new Tr393rec();
	private T3629rec t3629rec = new T3629rec();
	private Upperrec upperrec = new Upperrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Alocnorec alocnorec = new Alocnorec();
	private T6657rec t6657rec = new T6657rec();
	private Conerrrec conerrrec = new Conerrrec();
	private Getclntrec getclntrec = new Getclntrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		exit3190,
		reRead3310,
		exit3350
	}

	public Br524() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaInit.set(bsscIO.getDatimeInit());
		varcom.vrcmDate.set(getCobolDate());
		varcom.vrcmUser.set(wsaaUser);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		agcsIO.setDataKey(SPACES);
		agcsIO.setAgntcoy(bsprIO.getCompany());
		agcsIO.setFormat(agcsrec);
		agcsIO.setFunction(varcom.begnh);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readAgcs2010();
				case eof2080:
					eof2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readAgcs2010()
	{
		SmartFileCode.execute(appVars, agcsIO);
		if (isNE(agcsIO.getStatuz(),varcom.oK)
		&& isNE(agcsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcsIO.getParams());
			syserrrec.statuz.set(agcsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agcsIO.getStatuz(),varcom.endp)
		|| isNE(agcsIO.getAgntcoy(),bsprIO.getCompany())) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.eof2080);
		}
		agcsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (firstRead.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		createCheq3100();
		createPaymentHeader3200();
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		if (isNE(agcsIO.getFrcdate(),varcom.vrcmMaxDate)) {
			agcsIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, agcsIO);
			wsspEdterror.set(SPACES);
			return ;
		}
		wsspEdterror.set(varcom.oK);
		if (firstRead.isTrue()) {
			wsaaFirstRead.set("N");
			alocnorec.company.set(bsprIO.getCompany());
			alocnorec.prefix.set(fsupfxcpy.reqn);
			alocnorec.genkey.set(SPACES);
			alocnorec.function.set("NEXT");
			callProgram(Alocno.class, alocnorec.alocnoRec);
			if (isNE(alocnorec.statuz,varcom.oK)) {
				syserrrec.params.set(alocnorec.alocnoRec);
				syserrrec.statuz.set(alocnorec.statuz);
				fatalError600();
			}
			wsaaPrevPayclt.set(agcsIO.getPayclt());
			wsaaPrevCurrcode.set(agcsIO.getCurrcode());
		}
	}

protected void update3000()
	{
		upds3010();
	}

protected void upds3010()
	{
		if (isNE(agcsIO.getPayclt(),wsaaPrevPayclt)) {
			createCheq3100();
			createPaymentHeader3200();
			alocnorec.company.set(bsprIO.getCompany());
			alocnorec.prefix.set(fsupfxcpy.reqn);
			alocnorec.genkey.set(SPACES);
			alocnorec.function.set("NEXT");
			callProgram(Alocno.class, alocnorec.alocnoRec);
			if (isNE(alocnorec.statuz,varcom.oK)) {
				syserrrec.params.set(alocnorec.alocnoRec);
				syserrrec.statuz.set(alocnorec.statuz);
				fatalError600();
			}
			wsaaPrevPayclt.set(agcsIO.getPayclt());
			wsaaPrevCurrcode.set(agcsIO.getCurrcode());
			wsaaTotalAmount.set(ZERO);
		}
		createPaymentDissec3400();
		wsaaTotalAmount.add(agcsIO.getPayamt());
		updateAgcs3450();
		agcsIO.setFunction(varcom.nextr);
	}

protected void createCheq3100()
	{
		try {
			cheq3110();
			writeCheq3180();
		}
		catch (GOTOException e){
		}
	}

protected void cheq3110()
	{
		cheqIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t6657);
		itemIO.setItemitem(wsaaPrevCurrcode);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t6657rec.t6657Rec.set(itemIO.getGenarea());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t3688);
		itemIO.setItemitem(t6657rec.bankcode);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3688rec.t3688Rec.set(itemIO.getGenarea());
		cheqIO.setReqnno(alocnorec.alocNo);
		cheqIO.setPaycurr(t3688rec.bcurr);
		cheqIO.setReqnbcde(t6657rec.bankcode);
		cheqIO.setReqncoy(bsprIO.getCompany());
		cheqIO.setClntcoy(bsprIO.getFsuco());
		cheqIO.setClntnum01(wsaaPrevPayclt);
		cheqIO.setUser(varcom.vrcmUser);
		cheqIO.setAuthuser(varcom.vrcmUser);
		cheqIO.setRequser(varcom.vrcmUser);
		cheqIO.setAppruser(varcom.vrcmUser);
		cheqIO.setCheqdupn(SPACES);
		cheqIO.setPaydate(bsscIO.getEffectiveDate());
		cheqIO.setAuthdate(bsscIO.getEffectiveDate());
		cheqIO.setApprdte(bsscIO.getEffectiveDate());
		cheqIO.setReqdate(bsscIO.getEffectiveDate());
		cheqIO.setBranch(batcdorrec.branch);
		cheqIO.setPresdate(ZERO);
		cheqIO.setStmtpage(ZERO);
		cheqIO.setArchdate(ZERO);
		cheqIO.setPresflag("N");
		cheqIO.setTransactionDate(varcom.vrcmDate);
		cheqIO.setTransactionTime(varcom.vrcmTime);
		cheqIO.setApprtime(varcom.vrcmTime);
		cheqIO.setReqtime(varcom.vrcmTime);
		cheqIO.setAuthtime(varcom.vrcmTime);
		cheqIO.setProcind(wsaaAuth);
		cheqIO.setPayamt(wsaaTotalAmount);
		cheqIO.setZprnno(ZERO);
		contotrec.totno.set(ct08);
		contotrec.totval.set(wsaaTotalAmount);
		callContot001();
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntnum(wsaaPrevPayclt);
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			conerrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		cheqIO.setClttype01(cltsIO.getClttype());
		cheqIO.setSurnam01(cltsIO.getSurname());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getFsuco());
		itemIO.setItemtabl(tr393);
		itemIO.setItemitem(bsprIO.getFsuco());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr393rec.tr393Rec.set(itemIO.getGenarea());
		if (isEQ(cltsIO.getClttype(),"C")) {
			cheqIO.setSalNInit01(SPACES);
		}
		else {
			if (isEQ(tr393rec.salutind,"S")) {
				getDesc3181();
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(descIO.getShortdesc(), "  ");
				stringVariable1.addExpression(" ", "  ");
				stringVariable1.addExpression(cltsIO.getInitials(), "  ");
				stringVariable1.setStringInto(cheqIO.getSalNInit01());
			}
			else {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
				stringVariable2.addExpression(" ", "  ");
				stringVariable2.addExpression(cltsIO.getInitials(), "  ");
				stringVariable2.setStringInto(cheqIO.getSalNInit01());
			}
		}
		upperrec.upperRec.set(SPACES);
		upperrec.name.set(cltsIO.getSurname());
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz,varcom.oK)) {
			cheqIO.setCapname(cltsIO.getSurname());
		}
		else {
			cheqIO.setCapname(upperrec.name);
		}
		readAglf3182();
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t5690);
		itemIO.setItemitem(aglfIO.getPaymth());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5690rec.t5690Rec.set(itemIO.getGenarea());
		cheqIO.setReqntype(t5690rec.paymentMethod);
		cheqIO.setReqntype(t5690rec.paymentMethod);
		cheqIO.setFacthous(SPACES);
		cheqIO.setBankkey(SPACES);
		cheqIO.setBankacckey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3672);
		itemIO.setItemitem(t5690rec.paymentMethod);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3672rec.t3672Rec.set(itemIO.getGenarea());
	}

protected void writeCheq3180()
	{
		cheqIO.setFunction(varcom.writr);
		cheqIO.setFormat(cheqrec);
		SmartFileCode.execute(appVars, cheqIO);
		if (isNE(cheqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cheqIO.getParams());
			conerrrec.statuz.set(cheqIO.getStatuz());
			fatalError600();
		}
		contotrec.totno.set(ct07);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit3190);
	}

protected void getDesc3181()
	{
		upperrec.upperRec.set(SPACES);
		upperrec.name.set(cltsIO.getSalutl());
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz,varcom.oK)) {
			wsaaItemitem.set(cltsIO.getSalutl());
		}
		else {
			wsaaItemitem.set(upperrec.name);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getFsuco());
		descIO.setDesctabl(t3583);
		descIO.setDescitem(wsaaItemitem);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(descIO.getParams());
			conerrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

protected void readAglf3182()
	{
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(bsprIO.getCompany());
		aglfIO.setAgntnum(agcsIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(aglfIO.getParams());
			conerrrec.statuz.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void createPaymentHeader3200()
	{
		preq3201();
	}

protected void preq3201()
	{
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(batcdorrec.actmonth);
		preqIO.setPostyear(batcdorrec.actyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(bsprIO.getCompany());
		preqIO.setRldgcoy(bsprIO.getCompany());
		preqIO.setRdocnum(alocnorec.alocNo);
		preqIO.setRldgacct(t6657rec.bankcode);
		preqIO.setOrigccy(wsaaPrevCurrcode);
		preqIO.setEffdate(bsscIO.getEffectiveDate());
		preqIO.setTrandesc(cltsIO.getSurname());
		preqIO.setBranch(batcdorrec.branch);
		preqIO.setTransactionDate(varcom.vrcmDate);
		preqIO.setTransactionTime(varcom.vrcmTime);
		preqIO.setUser(varcom.vrcmUser);
		preqIO.setFrcdate(ZERO);
		preqIO.setRcamt(ZERO);
		preqIO.setTaxcat(SPACES);
		chkbkcdrec.function.set("REQNBANK");
		chkbkcdrec.bankcode.set(t6657rec.bankcode);
		chkbkcdrec.user.set(varcom.vrcmUser);
		chkbkcdrec.company.set(bsprIO.getCompany());
		callProgram(Chkbkcd.class, chkbkcdrec.chkbkcdRec);
		if (isNE(chkbkcdrec.statuz,varcom.oK)) {
			conerrrec.params.set(chkbkcdrec.chkbkcdRec);
			conerrrec.syserror.set(chkbkcdrec.statuz);
			fatalError600();
		}
		wsaaGenlkey.set(chkbkcdrec.genlkey);
		preqIO.setGenlcoy(wsaaGenlcoy);
		preqIO.setGlcode(wsaaGlcode);
		preqIO.setGlsign(chkbkcdrec.sign);
		getGlcodeSignContot3291();
		preqIO.setCnttot(glkeyrec.contot);
		preqIO.setSacscode(t3672rec.sacscode);
		preqIO.setSacstyp(t3672rec.sacstyp);
		preqIO.setRldgacct(t6657rec.bankcode);
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate,wsaaTotalAmount));
		/*    MOVE PREQ-ACCTAMT           TO ZRDP-AMOUNT-IN.               */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO PREQ-ACCTAMT.                 */
		if (isNE(preqIO.getAcctamt(), ZERO)) {
			zrdecplrec.amountIn.set(preqIO.getAcctamt());
			a000CallRounding();
			preqIO.setAcctamt(zrdecplrec.amountOut);
		}
		preqIO.setOrigamt(wsaaTotalAmount);
		wsaaJrnseq.set(ZERO);
		preqIO.setJrnseq(ZERO);
		preqIO.setFormat(preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(preqIO.getParams());
			conerrrec.statuz.set(preqIO.getStatuz());
			fatalError600();
		}
	}

protected void getGlcodeSignContot3291()
	{
		getGlcode3291();
	}

protected void getGlcode3291()
	{
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(bsprIO.getCompany());
		glkeyrec.trancode.set(batcdorrec.trcde);
		glkeyrec.sacscode.set(t3672rec.sacscode);
		glkeyrec.sacstyp.set(t3672rec.sacstyp);
		glkeyrec.ledgkey.set(SPACES);
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isEQ(glkeyrec.statuz,"NFND")) {
			glkeyrec.statuz.set(e141);
		}
		else {
			if (isEQ(glkeyrec.statuz,"MRNF")) {
				glkeyrec.statuz.set(e192);
			}
			else {
				if (isEQ(glkeyrec.statuz,"NCUR")) {
					glkeyrec.statuz.set(e193);
				}
				else {
					if (isEQ(glkeyrec.statuz,"BCTL")) {
						glkeyrec.statuz.set(e289);
					}
				}
			}
		}
		if (isNE(glkeyrec.statuz,varcom.oK)) {
			syserrrec.params.set(glkeyrec.glkeyRec);
			syserrrec.statuz.set(glkeyrec.statuz);
			fatalError600();
		}
	}

protected void getCurrencyRate3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3300();
				case reRead3310:
					reRead3310();
					findRate3320();
				case exit3350:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3300()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(preqIO.getOrigccy());
	}

protected void reRead3310()
	{
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate,ZERO)
		|| isGT(wsaaX,7))) {
			findRate3320();
		}

		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.reRead3310);
			}
			else {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(varcom.mrnf);
				fatalError600();
			}
		}
		else {
			goTo(GotoLabel.exit3350);
		}
	}

protected void findRate3320()
	{
		if (isGTE(bsscIO.getEffectiveDate(),t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(bsscIO.getEffectiveDate(),t3629rec.todate[wsaaX.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void createPaymentDissec3400()
	{
		start3410();
	}

protected void start3410()
	{
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(batcdorrec.actmonth);
		preqIO.setPostyear(batcdorrec.actyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(bsprIO.getCompany());
		preqIO.setRdocnum(alocnorec.alocNo);
		preqIO.setRldgcoy(bsprIO.getCompany());
		preqIO.setRldgacct(agcsIO.getAgntnum());
		preqIO.setOrigccy(wsaaPrevCurrcode);
		getCurrencyRate3300();
		preqIO.setGenlcur(wsaaLedgerCcy);
		preqIO.setSacscode(t5645rec.sacscode02);
		preqIO.setSacstyp(t5645rec.sacstype02);
		preqIO.setGlcode(t5645rec.glmap02);
		preqIO.setGlsign(t5645rec.sign02);
		preqIO.setCnttot(t5645rec.cnttot02);
		preqIO.setOrigamt(agcsIO.getPayamt());
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		preqIO.setUser(varcom.vrcmUser);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate,agcsIO.getPayamt()));
		/*    MOVE PREQ-ACCTAMT           TO ZRDP-AMOUNT-IN.               */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO PREQ-ACCTAMT.                 */
		if (isNE(preqIO.getAcctamt(), ZERO)) {
			zrdecplrec.amountIn.set(preqIO.getAcctamt());
			a000CallRounding();
			preqIO.setAcctamt(zrdecplrec.amountOut);
		}
		preqIO.setGenlcoy(bsprIO.getCompany());
		wsaaJrnseq.add(1);
		preqIO.setJrnseq(wsaaJrnseq);
		preqIO.setEffdate(bsscIO.getEffectiveDate());
		a100GetAgentname();
		preqIO.setTrandesc(namadrsrec.name);
		preqIO.setBranch(batcdorrec.branch);
		preqIO.setRcamt(ZERO);
		preqIO.setFrcdate(ZERO);
		preqIO.setTransactionDate(varcom.vrcmDate);
		preqIO.setTransactionTime(varcom.vrcmTime);
		preqIO.setUser(varcom.vrcmUser);
		preqIO.setFormat(preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(preqIO.getParams());
			conerrrec.statuz.set(preqIO.getStatuz());
			fatalError600();
		}
	}

protected void updateAgcs3450()
	{
		/*START*/
		agcsIO.setFrcdate(bsscIO.getEffectiveDate());
		agcsIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agcsIO);
		if (isNE(agcsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcsIO.getParams());
			syserrrec.statuz.set(agcsIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILE*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A010-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.currency.set(preqIO.getGenlcur());
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A090-EXIT*/
	}

protected void a100GetAgentname()
	{
		a110Post();
	}

protected void a110Post()
	{
		getclntrec.getclntRec.set(SPACES);
		getclntrec.acctPrefix.set(fsupfxcpy.agnt);
		getclntrec.acctCompany.set(agcsIO.getAgntcoy());
		getclntrec.acctNumber.set(agcsIO.getAgntnum());
		getclntrec.function.set("CLNT");
		callProgram(Getclnt.class, getclntrec.getclntRec);
		if (isNE(getclntrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(getclntrec.statuz);
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(getclntrec.clntPrefix);
		namadrsrec.clntCompany.set(getclntrec.clntCompany);
		namadrsrec.clntNumber.set(getclntrec.clntNumber);
		namadrsrec.function.set("LGNMS");
		namadrsrec.pcodeFlag.set("1");
		namadrsrec.language.set(bsscIO.getLanguage());
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}
}
