/*
 * File: C5338.java
 * Date: 30 August 2009 2:58:45
 * Author: $Id$
 * 
 * Class transformed from C5338.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.agents.batchprograms.B5338;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.FileCode;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C5338 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData runprm1 = new FixedLengthStringData(10);

	public C5338() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					runprm1.set(subString(params, 51, 10));
					if (isNE(runprm1,"KEEP")) {
						FileCode.clearMember("AGPYPF");
					}
					/* CALLPROG*/
					callProgram(B5338.class, new Object[] {params});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
