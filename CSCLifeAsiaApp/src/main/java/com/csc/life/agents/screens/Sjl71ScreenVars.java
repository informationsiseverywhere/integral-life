package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.life.annuities.screens.Sjl14protect;
import com.csc.life.annuities.screens.Sjl14screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl71ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData agncysel = DD.agncysel.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(dataFields, 9);
	public FixedLengthStringData salerepno = DD.salerep.copy().isAPartOf(dataFields, 29);
	public FixedLengthStringData salebranch = DD.salebranch.copy().isAPartOf(dataFields, 37);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 45);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
			.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agncyselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData salerepnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData salebranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,
			getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agncyselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] salerepnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] salebranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl71screenWritten = new LongData(0);
	public LongData Sjl71protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl71ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agncyselOut,
				new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(actionOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(useridOut,
				new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(salerepnoOut,
				new String[] { "04", null, "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(salebranchOut,
				new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "06", null, "-06", null, null, null, null, null, null, null, null, null });
		screenFields = new BaseData[] {agncysel ,action, userid , salerepno , salebranch , saledept};
		screenOutFields = new BaseData[][] {agncyselOut,actionOut , useridOut , salerepnoOut , salebranchOut, saledeptOut};
		screenErrFields = new BaseData[] {agncyselErr, actionErr, useridErr , salerepnoErr , salebranchErr , saledeptErr};
		screenDateFields = new BaseData[]{};
		screenDateErrFields = new BaseData[]{};
		screenDateDispFields = new BaseData[]{};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sjl71screen.class;
		protectRecord = Sjl71protect.class;
	}


	public int getDataAreaSize() {
		return 149;
	}

	public int getDataFieldsSize() {
		return 53;
	}

	public int getErrorIndicatorSize() {
		return 24;
	}

	public int getOutputFieldSize() {
		return 72;
	}


}
