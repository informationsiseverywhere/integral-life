package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52Q
 * @version 1.0 generated on 11/22/13 6:36 AM
 * @author CSC
 */
public class Sr52qScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(374);
	public FixedLengthStringData dataFields = new FixedLengthStringData(102).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData daexpys = new FixedLengthStringData(6).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] daexpy = ZDArrayPartOfStructure(2, 3, 0, daexpys, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(daexpys, 0, FILLER_REDEFINE);
	public ZonedDecimalData daexpy01 = DD.daexpy.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData daexpy02 = DD.daexpy.copyToZonedDecimal().isAPartOf(filler,3);
	public FixedLengthStringData dlvrmodes = new FixedLengthStringData(36).isAPartOf(dataFields, 7);
	public FixedLengthStringData[] dlvrmode = FLSArrayPartOfStructure(9, 4, dlvrmodes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(dlvrmodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData dlvrmode01 = DD.dlvrmode.copy().isAPartOf(filler1,0);
	public FixedLengthStringData dlvrmode02 = DD.dlvrmode.copy().isAPartOf(filler1,4);
	public FixedLengthStringData dlvrmode03 = DD.dlvrmode.copy().isAPartOf(filler1,8);
	public FixedLengthStringData dlvrmode04 = DD.dlvrmode.copy().isAPartOf(filler1,12);
	public FixedLengthStringData dlvrmode05 = DD.dlvrmode.copy().isAPartOf(filler1,16);
	public FixedLengthStringData dlvrmode06 = DD.dlvrmode.copy().isAPartOf(filler1,20);
	public FixedLengthStringData dlvrmode07 = DD.dlvrmode.copy().isAPartOf(filler1,24);
	public FixedLengthStringData dlvrmode08 = DD.dlvrmode.copy().isAPartOf(filler1,28);
	public FixedLengthStringData dlvrmode09 = DD.dlvrmode.copy().isAPartOf(filler1,32);
	public FixedLengthStringData letterTypes = new FixedLengthStringData(16).isAPartOf(dataFields, 43);
	public FixedLengthStringData[] letterType = FLSArrayPartOfStructure(2, 8, letterTypes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(letterTypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData letterType01 = DD.hlettype.copy().isAPartOf(filler2,0);
	public FixedLengthStringData letterType02 = DD.hlettype.copy().isAPartOf(filler2,8);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 102);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData daexpysErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] daexpyErr = FLSArrayPartOfStructure(2, 4, daexpysErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(daexpysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData daexpy01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData daexpy02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData dlvrmodesErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] dlvrmodeErr = FLSArrayPartOfStructure(9, 4, dlvrmodesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(dlvrmodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData dlvrmode01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData dlvrmode02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData dlvrmode03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData dlvrmode04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData dlvrmode05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData dlvrmode06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData dlvrmode07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData dlvrmode08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData dlvrmode09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData hlettypesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] hlettypeErr = FLSArrayPartOfStructure(2, 4, hlettypesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(hlettypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hlettype01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData hlettype02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 170);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData daexpysOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] daexpyOut = FLSArrayPartOfStructure(2, 12, daexpysOut, 0);
	public FixedLengthStringData[][] daexpyO = FLSDArrayPartOfArrayStructure(12, 1, daexpyOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(daexpysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] daexpy01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] daexpy02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData dlvrmodesOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] dlvrmodeOut = FLSArrayPartOfStructure(9, 12, dlvrmodesOut, 0);
	public FixedLengthStringData[][] dlvrmodeO = FLSDArrayPartOfArrayStructure(12, 1, dlvrmodeOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(108).isAPartOf(dlvrmodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dlvrmode01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] dlvrmode02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] dlvrmode03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] dlvrmode04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] dlvrmode05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] dlvrmode06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] dlvrmode07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] dlvrmode08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] dlvrmode09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData hlettypesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] hlettypeOut = FLSArrayPartOfStructure(2, 12, hlettypesOut, 0);
	public FixedLengthStringData[][] hlettypeO = FLSDArrayPartOfArrayStructure(12, 1, hlettypeOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(hlettypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hlettype01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] hlettype02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52qscreenWritten = new LongData(0);
	public LongData Sr52qprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52qScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dlvrmode01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daexpy01Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hlettype01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(daexpy02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hlettype02Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode02Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode03Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode04Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode05Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode06Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode07Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode08Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmode09Out,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, dlvrmode01, daexpy01, letterType01, daexpy02, letterType02, dlvrmode02, dlvrmode03, dlvrmode04, dlvrmode05, dlvrmode06, dlvrmode07, dlvrmode08, dlvrmode09};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, dlvrmode01Out, daexpy01Out, hlettype01Out, daexpy02Out, hlettype02Out, dlvrmode02Out, dlvrmode03Out, dlvrmode04Out, dlvrmode05Out, dlvrmode06Out, dlvrmode07Out, dlvrmode08Out, dlvrmode09Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, dlvrmode01Err, daexpy01Err, hlettype01Err, daexpy02Err, hlettype02Err, dlvrmode02Err, dlvrmode03Err, dlvrmode04Err, dlvrmode05Err, dlvrmode06Err, dlvrmode07Err, dlvrmode08Err, dlvrmode09Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52qscreen.class;
		protectRecord = Sr52qprotect.class;
	}

}
