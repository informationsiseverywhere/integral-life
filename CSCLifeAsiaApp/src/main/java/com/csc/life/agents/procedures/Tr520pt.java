/*
 * File: Tr520pt.java
 * Date: 30 August 2009 2:42:39
 * Author: Quipoz Limited
 * 
 * Class transformed from TR520PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Tr520rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR520.
*
*
*****************************************************************
* </pre>
*/
public class Tr520pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("OR RATES DEFINITION FOR REGULAR PREMIUM        SR520");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(13);
	private FixedLengthStringData filler9 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" OR Component");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler10 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine005, 0, FILLER).init("    Code         Year 1   Year 2   Year 3   Year 4   Year 5  Year 6   Year 7");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(77);
	private FixedLengthStringData filler11 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 4);
	private FixedLengthStringData filler12 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler19 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 4);
	private FixedLengthStringData filler20 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler27 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 4);
	private FixedLengthStringData filler28 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 4);
	private FixedLengthStringData filler36 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler43 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 4);
	private FixedLengthStringData filler44 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler51 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 4);
	private FixedLengthStringData filler52 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler59 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 4);
	private FixedLengthStringData filler60 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(77);
	private FixedLengthStringData filler67 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 4);
	private FixedLengthStringData filler68 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(77);
	private FixedLengthStringData filler75 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 4);
	private FixedLengthStringData filler76 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(77);
	private FixedLengthStringData filler83 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 4);
	private FixedLengthStringData filler84 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 17).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 35).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 44).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 53).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 62).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(29);
	private FixedLengthStringData filler91 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Continuation Item:");
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 21);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(28);
	private FixedLengthStringData filler92 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr520rec tr520rec = new Tr520rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr520pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr520rec.tr520Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tr520rec.zcomcode01);
		fieldNo008.set(tr520rec.zryrperc01);
		fieldNo009.set(tr520rec.zryrperc02);
		fieldNo010.set(tr520rec.zryrperc03);
		fieldNo011.set(tr520rec.zryrperc04);
		fieldNo012.set(tr520rec.zryrperc05);
		fieldNo013.set(tr520rec.zryrperc06);
		fieldNo015.set(tr520rec.zcomcode02);
		fieldNo014.set(tr520rec.zryrperc07);
		fieldNo016.set(tr520rec.zryrperc08);
		fieldNo017.set(tr520rec.zryrperc09);
		fieldNo018.set(tr520rec.zryrperc10);
		fieldNo019.set(tr520rec.zryrperc11);
		fieldNo020.set(tr520rec.zryrperc12);
		fieldNo023.set(tr520rec.zcomcode03);
		fieldNo021.set(tr520rec.zryrperc13);
		fieldNo022.set(tr520rec.zryrperc14);
		fieldNo024.set(tr520rec.zryrperc15);
		fieldNo025.set(tr520rec.zryrperc16);
		fieldNo026.set(tr520rec.zryrperc17);
		fieldNo027.set(tr520rec.zryrperc18);
		fieldNo031.set(tr520rec.zcomcode04);
		fieldNo028.set(tr520rec.zryrperc19);
		fieldNo029.set(tr520rec.zryrperc20);
		fieldNo030.set(tr520rec.zryrperc21);
		fieldNo032.set(tr520rec.zryrperc22);
		fieldNo033.set(tr520rec.zryrperc23);
		fieldNo034.set(tr520rec.zryrperc24);
		fieldNo039.set(tr520rec.zcomcode05);
		fieldNo035.set(tr520rec.zryrperc25);
		fieldNo036.set(tr520rec.zryrperc26);
		fieldNo037.set(tr520rec.zryrperc27);
		fieldNo038.set(tr520rec.zryrperc28);
		fieldNo040.set(tr520rec.zryrperc29);
		fieldNo041.set(tr520rec.zryrperc30);
		fieldNo047.set(tr520rec.zcomcode06);
		fieldNo042.set(tr520rec.zryrperc31);
		fieldNo043.set(tr520rec.zryrperc32);
		fieldNo044.set(tr520rec.zryrperc33);
		fieldNo045.set(tr520rec.zryrperc34);
		fieldNo046.set(tr520rec.zryrperc35);
		fieldNo048.set(tr520rec.zryrperc36);
		fieldNo055.set(tr520rec.zcomcode07);
		fieldNo049.set(tr520rec.zryrperc37);
		fieldNo050.set(tr520rec.zryrperc38);
		fieldNo051.set(tr520rec.zryrperc39);
		fieldNo052.set(tr520rec.zryrperc40);
		fieldNo053.set(tr520rec.zryrperc41);
		fieldNo054.set(tr520rec.zryrperc42);
		fieldNo063.set(tr520rec.zcomcode08);
		fieldNo056.set(tr520rec.zryrperc43);
		fieldNo057.set(tr520rec.zryrperc44);
		fieldNo058.set(tr520rec.zryrperc45);
		fieldNo059.set(tr520rec.zryrperc46);
		fieldNo060.set(tr520rec.zryrperc47);
		fieldNo061.set(tr520rec.zryrperc48);
		fieldNo071.set(tr520rec.zcomcode09);
		fieldNo062.set(tr520rec.zryrperc49);
		fieldNo064.set(tr520rec.zryrperc50);
		fieldNo065.set(tr520rec.zryrperc51);
		fieldNo066.set(tr520rec.zryrperc52);
		fieldNo067.set(tr520rec.zryrperc53);
		fieldNo068.set(tr520rec.zryrperc54);
		fieldNo079.set(tr520rec.zcomcode10);
		fieldNo069.set(tr520rec.zryrperc55);
		fieldNo070.set(tr520rec.zryrperc56);
		fieldNo072.set(tr520rec.zryrperc57);
		fieldNo073.set(tr520rec.zryrperc58);
		fieldNo074.set(tr520rec.zryrperc59);
		fieldNo075.set(tr520rec.zryrperc60);
		fieldNo087.set(tr520rec.contitem);
		fieldNo076.set(tr520rec.zryrperc61);
		fieldNo077.set(tr520rec.zryrperc62);
		fieldNo078.set(tr520rec.zryrperc63);
		fieldNo080.set(tr520rec.zryrperc64);
		fieldNo081.set(tr520rec.zryrperc65);
		fieldNo082.set(tr520rec.zryrperc66);
		fieldNo083.set(tr520rec.zryrperc67);
		fieldNo084.set(tr520rec.zryrperc68);
		fieldNo085.set(tr520rec.zryrperc69);
		fieldNo086.set(tr520rec.zryrperc70);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
