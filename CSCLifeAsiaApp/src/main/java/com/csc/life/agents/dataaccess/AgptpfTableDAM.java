package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgptpfTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:41
 * Class transformed from AGPTPF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgptpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 106;
	public FixedLengthStringData agptrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agptpfRecord = agptrec;
	
	public FixedLengthStringData rldgcoy = DD.rldgcoy.copy().isAPartOf(agptrec);
	public FixedLengthStringData rldgacct = DD.rldgacct.copy().isAPartOf(agptrec);
	public PackedDecimalData sacscurbal = DD.sacscurbal.copy().isAPartOf(agptrec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(agptrec);
	public FixedLengthStringData tagsusind = DD.tagsusind.copy().isAPartOf(agptrec);
	public FixedLengthStringData taxcde = DD.taxcde.copy().isAPartOf(agptrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(agptrec);
	public FixedLengthStringData agccqind = DD.agccqind.copy().isAPartOf(agptrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(agptrec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(agptrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(agptrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(agptrec);
	public PackedDecimalData whtax = DD.whtax.copy().isAPartOf(agptrec);
	public PackedDecimalData tcolbal = DD.tcolbal.copy().isAPartOf(agptrec);
	public PackedDecimalData tdeduct = DD.tdeduct.copy().isAPartOf(agptrec);
	public PackedDecimalData tdcchrg = DD.tdcchrg.copy().isAPartOf(agptrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(agptrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgptpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for AgptpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgptpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgptpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgptpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgptpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgptpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGPTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"RLDGCOY, " +
							"RLDGACCT, " +
							"SACSCURBAL, " +
							"PROCFLG, " +
							"TAGSUSIND, " +
							"TAXCDE, " +
							"CURRCODE, " +
							"AGCCQIND, " +
							"PAYCLT, " +
							"FACTHOUS, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"WHTAX, " +
							"TCOLBAL, " +
							"TDEDUCT, " +
							"TDCCHRG, " +
							"PAYMTH, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     rldgcoy,
                                     rldgacct,
                                     sacscurbal,
                                     procflg,
                                     tagsusind,
                                     taxcde,
                                     currcode,
                                     agccqind,
                                     payclt,
                                     facthous,
                                     bankkey,
                                     bankacckey,
                                     whtax,
                                     tcolbal,
                                     tdeduct,
                                     tdcchrg,
                                     paymth,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		rldgcoy.clear();
  		rldgacct.clear();
  		sacscurbal.clear();
  		procflg.clear();
  		tagsusind.clear();
  		taxcde.clear();
  		currcode.clear();
  		agccqind.clear();
  		payclt.clear();
  		facthous.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		whtax.clear();
  		tcolbal.clear();
  		tdeduct.clear();
  		tdcchrg.clear();
  		paymth.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgptrec() {
  		return agptrec;
	}

	public FixedLengthStringData getAgptpfRecord() {
  		return agptpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgptrec(what);
	}

	public void setAgptrec(Object what) {
  		this.agptrec.set(what);
	}

	public void setAgptpfRecord(Object what) {
  		this.agptpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agptrec.getLength());
		result.set(agptrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}