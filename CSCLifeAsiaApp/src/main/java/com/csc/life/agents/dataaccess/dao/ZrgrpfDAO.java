package com.csc.life.agents.dataaccess.dao;
 
import java.util.List;

import com.csc.life.agents.dataaccess.model.Zrgrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZrgrpfDAO extends BaseDAO<Zrgrpf> {
	public int insertZrgrData(Zrgrpf zrgrpf);

   
}
