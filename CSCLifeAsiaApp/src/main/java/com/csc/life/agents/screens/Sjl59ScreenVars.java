package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl59protect;
import com.csc.life.agents.screens.Sjl59screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;


public class Sjl59ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(229);
	public FixedLengthStringData dataFields = new FixedLengthStringData(101).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 66);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 70);
	public FixedLengthStringData stattype = DD.statustype.copy().isAPartOf(dataFields, 100);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 101);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData stattypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 133); 
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] stattypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(526);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(299).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(subfileFields,11); 
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(subfileFields,66);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(subfileFields,67);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(subfileFields, 99);
	public FixedLengthStringData status = DD.salestatus.copy().isAPartOf(subfileFields, 119);
	public FixedLengthStringData regclass = DD.agncynum.copy().isAPartOf(subfileFields,129); 
	public ZonedDecimalData regdate = DD.occdate.copyToZonedDecimal().isAPartOf(subfileFields,137);
	public FixedLengthStringData reasonreg = DD.reasoncd.copy().isAPartOf(subfileFields,145);
	public FixedLengthStringData resndetl = DD.resndetl.copy().isAPartOf(subfileFields,149);
	public FixedLengthStringData agtdesc = DD.agtydesc.copy().isAPartOf(subfileFields,269);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(56).isAPartOf(subfileArea, 299);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile,20);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData statusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData regclassErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData regdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData reasonregErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData resndetlErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData agtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(168).isAPartOf(subfileArea, 355);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] statusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] regclassOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] regdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] reasonregOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] resndetlOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] agtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 523);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	public FixedLengthStringData regdateDisp = new FixedLengthStringData(10);
	
	public LongData Sjl59screensflWritten = new LongData(0);
	public LongData Sjl59screenctlWritten = new LongData(0);
	public LongData Sjl59screenWritten = new LongData(0);
	public LongData Sjl59protectWritten = new LongData(0);
	public GeneralTable sjl59screensfl = new GeneralTable(AppVars.getInstance());
	
	public boolean hasSubfile() {
		return false;
	}

	public Sjl59ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		
		fieldIndMap.put(companyOut,
				new String[] { "05", "06" , "-05", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "07", "08" , "-07", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "09", "10" , "-09", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "11", "12" , "-11", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "13", "14" , "-13", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "21", "22" , "-21", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "23", "24" , "-23", null , null, null, null, null, null, null, null, null });
		
		screenFields = new BaseData[] { company, brnchcd, brnchdesc, aracde, aradesc, saledept, saledptdes, stattype };
		screenOutFields = new BaseData[][] { companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, saledeptOut, saledptdesOut, stattypeOut };
		screenErrFields = new BaseData[] { companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, saledeptErr, saledptdesErr, stattypeErr };
						
		screenSflFields = new BaseData[] {slt, clntsel, cltname, levelno, leveltype , leveldes, agtype, userid, status, regclass, regdate, reasonreg, resndetl, agtdesc };
		screenSflOutFields = new BaseData[][] {sltOut, clntselOut, cltnameOut, levelnoOut, leveltypeOut, leveldesOut, agtypeOut, useridOut,
												statusOut, regclassOut, regdateOut, reasonregOut, resndetlOut, agtdescOut };
		screenSflErrFields = new BaseData[] {sltErr, clntselErr, cltnameErr, levelnoErr, leveltypeErr, leveldesErr, agtypeErr, useridErr,
												statusErr, regclassErr, regdateErr, reasonregErr, resndetlErr, agtdescErr };
		
		screenDateFields = new BaseData[] {};
		screenSflDateFields = new BaseData[] {regdate};
		screenSflDateErrFields = new BaseData[] {regdateErr};
		screenSflDateDispFields = new BaseData[] {regdateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorSflInds = errorSubfile;
		screenSflRecord = Sjl59screensfl.class;
		screenCtlRecord = Sjl59screenctl.class;
		initialiseSubfileArea();
		screenRecord = Sjl59screen.class;
		protectRecord = Sjl59protect.class;
		}
		
		public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl59screenctl.lrec.pageSubfile);
		}
}
