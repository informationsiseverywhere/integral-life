package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:17
 * Description:
 * Copybook name: ZCTNREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zctnrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zctnrevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zctnrevKey = new FixedLengthStringData(256).isAPartOf(zctnrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData zctnrevChdrcoy = new FixedLengthStringData(1).isAPartOf(zctnrevKey, 0);
  	public FixedLengthStringData zctnrevChdrnum = new FixedLengthStringData(8).isAPartOf(zctnrevKey, 1);
  	public PackedDecimalData zctnrevTranno = new PackedDecimalData(5, 0).isAPartOf(zctnrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(zctnrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zctnrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zctnrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}