package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR695
 * @version 1.0 generated on 30/08/09 07:24
 * @author Quipoz
 */
public class Sr695ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(316);
	public FixedLengthStringData dataFields = new FixedLengthStringData(92).isAPartOf(dataArea, 0);
	public FixedLengthStringData basicCommMeth = DD.bascmeth.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bascpy = DD.bascpy.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData basscmth = DD.basscmth.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData basscpy = DD.basscpy.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData bastcmth = DD.bastcmth.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData bastcpy = DD.bastcpy.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,33);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData rnwcpy = DD.rnwcpy.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData srvcpy = DD.srvcpy.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 92);
	public FixedLengthStringData bascmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bascpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData basscmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData basscpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bastcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bastcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData rnwcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData srvcpyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 148);
	public FixedLengthStringData[] bascmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bascpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] basscmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] basscpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bastcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bastcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] rnwcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] srvcpyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr695screenWritten = new LongData(0);
	public LongData Sr695protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr695ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, basicCommMeth, basscmth, bastcmth, bascpy, srvcpy, rnwcpy, basscpy, bastcpy, itmfrm, itmto};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, bascmethOut, basscmthOut, bastcmthOut, bascpyOut, srvcpyOut, rnwcpyOut, basscpyOut, bastcpyOut, itmfrmOut, itmtoOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, bascmethErr, basscmthErr, bastcmthErr, bascpyErr, srvcpyErr, rnwcpyErr, basscpyErr, bastcpyErr, itmfrmErr, itmtoErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr695screen.class;
		protectRecord = Sr695protect.class;
	}

}
