package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.CltrelnDTO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.life.agents.screens.Sjl58ScreenVars;
import com.csc.life.agents.tablestructures.Tjl68rec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl58 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl58.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL58");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl58ScreenVars sv = ScreenProgram.getScreenVars(Sjl58ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaatopLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaatoproleCode = new FixedLengthStringData(2).isAPartOf(wsaatopLevel, 0);
	private FixedLengthStringData wsaatophierCode = new FixedLengthStringData(8).isAPartOf(wsaatopLevel, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	private FixedLengthStringData levelshortdesc = new FixedLengthStringData(30);
	
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private List<Agsdpf> agsdpfList = new ArrayList<Agsdpf>();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = new Clntpf();
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private CltrelnDTO cltrelnDTO;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private Hierpf hierpf = new Hierpf();
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private List<Hierpf> hierpfList = new ArrayList<Hierpf>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private String salesDept = "";
	private String salesDiv = "";
	private String leveltype = "";
	private String uplvlno = "";
	private String wsaaGennum;
	private static final String E186 = "E186";	
	private static final String JL98 = "JL98";
	private static final String JL84 = "JL84";
	private static final String HL01 = "HL01";
	private static final String JL85 = "JL85";
	private static final String JL86 = "JL86";
	private static final String F393 = "F393";
	private static final String F782 = "F782";
	private static final String E058 = "E058";
	private static final String RUBN = "RUBN";
	private static final String RUBO = "RUBO";
	private static final String RUBP = "RUBP";
	private static final String JL89 = "JL89";
	private static final String RUCA = "RUCA";
	private static final String RUCS = "RUCS";
	private static final String RUCU = "RUCU";
	private static final String RUCV = "RUCV";
	
	private Tr386rec tr386rec = new Tr386rec();	
	private Tjl68rec tjl68rec = new Tjl68rec();	
	private List<Descpf> t1692List;
	private List<Descpf> t5696List;
	private List<Descpf> tjl68List;
	private List<Descpf> tjl70List;
	private Map<String, List<Itempf>> tjl68ListMap;
	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String salestatus = "Active";
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";

	public Pjl58() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl58", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	protected void largename(Clntpf clntpf)
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename(Clntpf clntpf)
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		if ("1".equals(clntpf.getEthorig())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
	
	@Override
	protected void initialise1000() {
		
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
	    sv.regdate.set(varcom.vrcmMaxDate);
	    sv.moddate.set(varcom.vrcmMaxDate);
    	sv.deldate.set(varcom.vrcmMaxDate);
		a7050Begin();
		initialise1010();
		protect1020();
	}


	protected void initialise1010() {
		
		if (isEQ(wsspcomn.flag, "I")) {
			sv.clntsel.set(SPACE);
			sv.cltname.set(SPACE);
			sv.levelno.set(wsspcomn.chdrCownnum);
			sv.brnchcd.set(SPACE);
			sv.brnchdesc.set(SPACE);
			sv.aracde.set(SPACE);
			sv.aradesc.set(SPACE);
			sv.agtype.set(SPACE);
			sv.resndetl.set(SPACE);
			sv.saledept.set(SPACE);
			sv.saledptdes.set(SPACE);
			sv.leveltype.set(SPACE);
			sv.leveldes.set(SPACE);
			sv.reasonreg.set(SPACE);
		}
		else if (isEQ(wsspcomn.flag, "J") || isEQ(wsspcomn.flag, "K") || isEQ(wsspcomn.flag, "N")) {
			initialise1020();
		}
	}
		protected void initialise1020() {
		
		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				salesDept = sv.saledept.toString();
				salesDiv = wsaaSalediv.toString();
				loadDesc();		
			}
			if (null != agsdpf.getLevelno())
			{
				sv.levelno.set(agsdpf.getLevelno());
				wsaahierCode.set(agsdpf.getLevelno());
				if (null != agsdpf.getLevelclass())
				{
					sv.leveltype.set(agsdpf.getLevelclass());
					leveltype = sv.leveltype.toString();
					loadDesc();
					if("3".equals(agsdpf.getLevelclass())) {
						wsaaroleCode.set("SS");
					}
					else if ("2".equals(agsdpf.getLevelclass())) {
						wsaaroleCode.set("SB");
					}
				}
				
				hierpf = hierpfDAO.getHierpfData(wsaaKey.toString(), "1", "1");
				Optional<Hierpf> exists = Optional.ofNullable(hierpf);
				if (exists.isPresent()) {
					wsaaupLevel.set(hierpf.getUpperlevel());
					sv.uplevelno.set(wsaauphierCode);
					uplvlno = sv.uplevelno.toString();
					Clntpf clnt = cltsioCall2700(agsdpf.getClientno());
					Optional<Clntpf> flag = Optional.ofNullable(clnt);
					if (flag.isPresent() && isNE(sv.uplevelno,SPACES)) {
						sv.upleveldes.set(clnt.getGivname() + " " + clnt.getSurname());
					}
    				if("1".equals(hierpf.getActivestatus())) {
    					sv.status.set(salestatus);
    				}
				}
			}
			if (null != agsdpf.getAgentclass())
				sv.agtype.set(agsdpf.getAgentclass());
			if ("1".equals(agsdpf.getActivestatus()) && (isEQ(sv.status,SPACES))) {
				sv.status.set(salestatus);
			}
		}
	}
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected void a7050Begin()
	{
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		Itempf itempf = itemDAO.findItemByItem(wsspcomn.company.toString(), "TR386",wsaaTr386Key.toString());
		
		tr386rec.tr386Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		
		if (isEQ(wsspcomn.sbmaction, "I") || isEQ(wsspcomn.sbmaction, "N")) {
			sv.scrndesc.set(tr386rec.progdesc[1].toString());
		} else if (isEQ(wsspcomn.sbmaction, "J")) {
			sv.scrndesc.set(tr386rec.progdesc[2].toString());
		}
		else if (isEQ(wsspcomn.sbmaction, "K")) {
			sv.scrndesc.set(tr386rec.progdesc[3].toString());
		}
	}
	
	@SuppressWarnings("static-access")
	private void protect1020() {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.companyOut[Varcom.pr.toInt()].set("Y");
			sv.levelnoOut[Varcom.pr.toInt()].set("Y");
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			sv.uplevelnoOut[Varcom.nd.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "J")) {
			sv.companyOut[Varcom.pr.toInt()].set("Y");
			sv.levelnoOut[Varcom.pr.toInt()].set("Y");
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			if ("2".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("Y");
			}
			if ("3".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("N");
			}
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
			sv.regdateOut[Varcom.nd.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
			sv.reasonregOut[Varcom.nd.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "K")) {
			sv.clntselOut[Varcom.pr.toInt()].set("Y");
			sv.cltnameOut[Varcom.pr.toInt()].set("Y");
			sv.companyOut[Varcom.pr.toInt()].set("Y");
			sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
			sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
			sv.aracdeOut[Varcom.pr.toInt()].set("Y");
			sv.aradescOut[Varcom.pr.toInt()].set("Y");
			sv.levelnoOut[Varcom.pr.toInt()].set("Y");
			sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
			sv.leveldesOut[Varcom.pr.toInt()].set("Y");
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			sv.saledeptOut[Varcom.pr.toInt()].set("Y");
			sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
			sv.agtypeOut[Varcom.pr.toInt()].set("Y");
			sv.agtydescOut[Varcom.pr.toInt()].set("Y");
			if ("2".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("Y");
				sv.upleveldesOut[Varcom.pr.toInt()].set("Y");
			}
			if ("3".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("N");
				sv.uplevelnoOut[Varcom.pr.toInt()].set("Y");
				sv.upleveldesOut[Varcom.pr.toInt()].set("Y");
			}
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.regdateOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.reasonregOut[Varcom.nd.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "N")) {
			sv.clntselOut[Varcom.pr.toInt()].set("Y");
			sv.cltnameOut[Varcom.pr.toInt()].set("Y");
			sv.companyOut[Varcom.pr.toInt()].set("Y");
			sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
			sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
			sv.aracdeOut[Varcom.pr.toInt()].set("Y");
			sv.aradescOut[Varcom.pr.toInt()].set("Y");
			sv.levelnoOut[Varcom.pr.toInt()].set("Y");
			sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
			sv.leveldesOut[Varcom.pr.toInt()].set("Y");
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			sv.saledeptOut[Varcom.pr.toInt()].set("Y");
			sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
			sv.agtypeOut[Varcom.pr.toInt()].set("Y");
			sv.agtydescOut[Varcom.pr.toInt()].set("Y");
			if ("2".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("Y");
				sv.upleveldesOut[Varcom.pr.toInt()].set("Y");
			}
			if ("3".equals(agsdpf.getLevelclass())) {
				sv.uplevelnoOut[Varcom.nd.toInt()].set("N");
				sv.uplevelnoOut[Varcom.pr.toInt()].set("Y");
				sv.upleveldesOut[Varcom.pr.toInt()].set("Y");
			}
			sv.statusOut[Varcom.pr.toInt()].set("Y");
			sv.regdateOut[Varcom.nd.toInt()].set("Y");
			sv.reasonregOut[Varcom.nd.toInt()].set("Y");
			sv.moddateOut[Varcom.nd.toInt()].set("Y");
			sv.reasonmodOut[Varcom.nd.toInt()].set("Y");
			sv.deldateOut[Varcom.nd.toInt()].set("Y");
			sv.reasondelOut[Varcom.nd.toInt()].set("Y");
		}
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(Varcom.oK);
		validate2010();
		sv.status.set(salestatus);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void validate2010() {
		
		if (isEQ(wsspcomn.flag, "N")) {	
			return;
		}
		valDate();
		readTjl68();
		if(isEQ(sv.clntsel, SPACES)){
			sv.clntselErr.set(E186);
		}
		else {
			chgClient2120();
		}
		if(isEQ(sv.brnchcd, SPACES)) {
			sv.brnchcdErr.set(E186);
		}
		else{
			valBranchCode();
		}
		if(isEQ(sv.leveltype, SPACES)) {
			sv.leveltypeErr.set(E186);
		}
		else{
			valLevelType();
		}
		if(isEQ(sv.saledept, SPACES)) {
			sv.saledeptErr.set(E186);
		}
		else{
			valSalesDept();
		}
		if(isEQ(sv.agtype, SPACES)) {
			sv.agtypeErr.set(E186);
		}
		if (isEQ(wsspcomn.flag, "I")) {	
		if(isEQ(sv.regdate, varcom.vrcmMaxDate) || isEQ(sv.regdate, SPACES)) {
			sv.regdateErr.set(E186);
		}
		if(isEQ(sv.reasonreg, SPACES)) {
			sv.reasonregErr.set(E186);
			}
		}
		if (isEQ(wsspcomn.flag, "J")) {	
			if(isEQ(sv.moddate, varcom.vrcmMaxDate) || isEQ(sv.moddate, SPACES)) {
				sv.moddateErr.set(E186);
			}
			if(isEQ(sv.reasonmod, SPACES)) {
				sv.reasonmodErr.set(E186);
			}
		}
		if (isEQ(wsspcomn.flag, "K")) {	
			if(isEQ(sv.deldate, varcom.vrcmMaxDate) || isEQ(sv.deldate, SPACES)) {
				sv.deldateErr.set(E186);
			}
			if(isEQ(sv.reasondel, SPACES)) {
				sv.reasondelErr.set(E186);
			}
			if(isEQ(sv.leveltype, "3")){
				wsaatoproleCode.set("SS");
				wsaauproleCode.set("SS");
			}
			if(isEQ(sv.leveltype, "2")){
				wsaatoproleCode.set("SB");
				wsaauproleCode.set("SB");
			}
		}
		validateItems();
	}
	
	protected void validateItems() {
		valAreaCode();
		if(isEQ(sv.leveltypeErr, RUCU)) {
			
		}
		else {
			valUpLvlNo();
		}
	}
	
	
	protected void readTjl68()
    {
		if(isEQ(sv.saledept, SPACES)) {
			valSalesDept();
		}
		tjl68ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "TJL68");
		String keyItemitem = sv.saledept.toString();
		if (null != tjl68ListMap && tjl68ListMap.containsKey(keyItemitem.trim())) {
			List<Itempf> itempfList = tjl68ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (itempf == null) {
					syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("TJL68").concat(wsaaProg.toString()));
					fatalError600();
				} else {
					tjl68rec.tjl68Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				}
			}
		}   
    }
	
	protected void valBranchCode() {
		if (isEQ(tjl68rec.agntbr, SPACES)) {
			readTjl68();
		}
		t1692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "T1692", sv.brnchcd.toString().trim(),wsspcomn.language.toString());
		if(!t1692List.isEmpty()) {
			for (Descpf descItem : t1692List) {
				if (descItem.getDescitem().trim().equals(sv.brnchcd.toString().trim())){
					sv.brnchdesc.set(descItem.getLongdesc());
					break;
				}
				else {
					sv.brnchdesc.set(" ");
				}
			}
		}
		if(isNE(sv.brnchcd,tjl68rec.agntbr)) {
			sv.brnchcdErr.set(JL85);
			sv.saledeptErr.set(JL85);
		}
	}
   
    protected void valAreaCode() {
    	
    	if (isEQ(tjl68rec.aracde, SPACES)) {
			readTjl68();
		}
	    t5696List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "T5696", sv.aracde.toString().trim(),wsspcomn.language.toString());
	    if(!t5696List.isEmpty()) {
			for (Descpf descItem : t5696List) {
				if (descItem.getDescitem().trim().equals(sv.aracde.toString().trim())){
					sv.aradesc.set(descItem.getLongdesc());
					break;
				}
				if(isEQ(tjl68rec.aracde, SPACES) && (isNE(sv.aracde, SPACES))) {
					sv.aradesc.set(descItem.getLongdesc());
				}
			}
	    }
		if(isNE(tjl68rec.aracde, SPACES)) {
			if(isNE(sv.aracde,tjl68rec.aracde) && isNE(sv.aracde, SPACES)) {
				sv.aracdeErr.set(JL86);
				sv.saledeptErr.set(JL86);
			}
			if(isEQ(sv.aracde, SPACES)) {
				sv.aradesc.set(SPACES);
			}
		}
		if(isEQ(sv.aracde, SPACES)){
			sv.aradesc.set(SPACES);
		}	
    }
    
    protected void valLevelType() {
    	tjl70List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "TJL70", sv.leveltype.toString().trim(),wsspcomn.language.toString());
    	if (tjl70List.isEmpty()) {
    		sv.leveltypeErr.set(RUBO);
			sv.leveldes.set(" ");
    	} else {
	    	for (Descpf descItem : tjl70List) {
				if (descItem.getDescitem().trim().equals(sv.leveltype.toString().trim())){
					sv.leveldes.set(descItem.getLongdesc());
					levelshortdesc.set(descItem.getShortdesc());   
					break;
				}
			}
    	}
    	
    	if(isEQ(wsspcomn.flag, "J") && isNE(leveltype, sv.leveltype)) {
    		if(isEQ(leveltype, "3")){
				wsaatoproleCode.set("SS");
				wsaauproleCode.set("SS");
			}
			if(isEQ(leveltype, "2")){
				wsaatoproleCode.set("SB");
				wsaauproleCode.set("SB");
			}
			wsaauphierCode.set(sv.levelno.toString());
			wsaatophierCode.set(sv.levelno.toString());
			Hierpf hier = hierpfDAO.getHierpfLvlNo(wsaatopLevel.toString(), wsaaupLevel.toString(), "1","1");
			Optional<Hierpf> isExists = Optional.ofNullable(hier);
			if (isExists.isPresent()) {
				sv.leveltypeErr.set(RUCU);
			}
    	}
    		
    }
  
    protected void valSalesDept() {
    	if (isNE(sv.saledept,SPACES)) {
	    	tjl68List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "TJL68", sv.saledept.toString().trim(),wsspcomn.language.toString());
			if (tjl68List.isEmpty()) {
				sv.saledeptErr.set(RUBN);
				sv.saledptdes.set(" ");
			} else {
			   	for (Descpf descItem : tjl68List) {
					if (descItem.getDescitem().trim().equals(sv.saledept.toString().trim())){
						sv.saledptdes.set(descItem.getLongdesc());
						break;
					}
					else {
						sv.saledptdes.set(" ");
					}
				}
			}
			if(isEQ(wsspcomn.flag, "J") && isNE(salesDept, sv.saledept)) {
				agsdpfList = agsdpfDAO.getAgsdpfbyDept(salesDiv,sv.levelno.toString());
				if(!agsdpfList.isEmpty()) {
					for(Agsdpf agsd : agsdpfList) {
						if("3".equals(agsd.getLevelclass())) {
							wsaatoproleCode.set("SS");
							wsaauproleCode.set("SS");
						}
						if("2".equals(agsd.getLevelclass())) {
							wsaatoproleCode.set("SB");
							wsaauproleCode.set("SB");
						}
						wsaauphierCode.set(agsd.getLevelno());
						wsaatophierCode.set(agsd.getLevelno());
						Hierpf hier = hierpfDAO.getHierpfLvlNo(wsaatopLevel.toString(), wsaaupLevel.toString(), "1","1");
						Optional<Hierpf> exists = Optional.ofNullable(hier);
						if (exists.isPresent()) {
							sv.saledeptErr.set(RUCA);
						}
					}
				}
			}
    	}
    	else {
    		sv.saledptdes.set(" ");
    		return;
    	}
    }
    
    protected void valUpLvlNo() {
    	
    	if (isEQ(sv.leveltype, "3")) {
    		sv.uplevelnoOut[Varcom.nd.toInt()].set("N");
    		Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.uplevelno.toString());
    		if(agsd != null) {
    			if (isEQ(agsd.getActivestatus(), "1") && isEQ(agsd.getValidflag(), "1")){
    				boolean found = false;
    				found = agsd.getSalesdiv().contains(sv.saledept.toString());
    				if(!found) {
    					//sv.saledeptErr.set(JL98);
    					sv.uplevelnoErr.set(JL98);
    				}
    				if(found) {
    					Clntpf clnt = cltsioCall2700(agsd.getClientno());
    					Optional<Clntpf> op = Optional.ofNullable(clnt);
    					if (op.isPresent()) {
    						sv.upleveldes.set(clnt.getGivname() + " " + clnt.getSurname());
    					}
    				}
    			}
    			else {
    				sv.uplevelnoErr.set(RUCS);
    			}
    			/* To validate if uplevelno can be changed */
    			if(isEQ(wsspcomn.flag, "J") && isNE(uplvlno, sv.uplevelno)) {
    				Agsdpf agpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString());
    				if(agpf != null) {
	    				if(isEQ(agpf.getLevelclass(), "2")){
	    					wsaaroleCode.set("SB");
	    					wsaatoproleCode.set("SB");
	    					wsaauproleCode.set("SB");
	    				}
	    				if(isEQ(agpf.getLevelclass(), "3")){
	    					wsaaroleCode.set("SS");
	    					wsaatoproleCode.set("SS");
	    					wsaauproleCode.set("SS");
	    				}
    				}
    				wsaahierCode.set(sv.levelno.toString());
    				String agntkey = wsaaKey.toString();
    				wsaauphierCode.set(sv.levelno.toString());
    				wsaatophierCode.set(sv.levelno.toString());
    				Hierpf hier = hierpfDAO.getHierpfLvlNo(wsaatopLevel.toString(), wsaaupLevel.toString(), "1","1");
    				Optional<Hierpf> isExists = Optional.ofNullable(hier);
    				if (isExists.isPresent()) {
    					Agsdpf agspf = agsdpfDAO.getDatabyLevelno(sv.uplevelno.toString());
    					if(agspf != null) {
	        				if(isEQ(agspf.getLevelclass(), "2")){
	        					wsaaroleCode.set("SB");
	        					wsaatoproleCode.set("SB");
	        					wsaauproleCode.set("SB");
	        				}
	        				if(isEQ(agspf.getLevelclass(), "3")){
	        					wsaaroleCode.set("SS");
	        					wsaatoproleCode.set("SS");
	        					wsaauproleCode.set("SS");
	        				}
    					}
        				wsaahierCode.set(sv.uplevelno.toString());
        				wsaauphierCode.set(sv.uplevelno.toString());
        				wsaatophierCode.set(sv.uplevelno.toString());
        				/* To check if sv.uplevelno is present in toplevel or uplevel*/
        				Hierpf hrpf = hierpfDAO.getHierpfLvlNo(wsaatopLevel.toString(), wsaaupLevel.toString(), "1","1");
        				Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
        				if (isPrsnt.isPresent()) {
        					if(isNE(hier.getToplevel(), hrpf.getToplevel())) {
        						sv.uplevelnoErr.set(RUCV);
        					}
        				}
        				/* To check if sv.uplevelno is present in agentkey */
        				Hierpf hpf = hierpfDAO.getHierpfData(wsaaKey.toString(), "1", "1");
        				Optional<Hierpf> flag = Optional.ofNullable(hpf);
        				if (flag.isPresent()) {
        					/*if toplevel of levelno and sv.uplevel is diff OR upper level of sv.uplevel is same as levelno*/
        					if(isNE(hier.getToplevel(), hpf.getToplevel()) || isEQ(hpf.getUpperlevel(),agntkey)) {
        						sv.uplevelnoErr.set(RUCV);
        					}
        				}
        				/* To check if it is not present on hierpf */
        				if(!(isPrsnt.isPresent()) && !(flag.isPresent())) {
        					Agsdpf aspf = agsdpfDAO.getDatabyLevelno(sv.uplevelno.toString());
        					Optional<Agsdpf> isPr = Optional.ofNullable(aspf);
            				if (isPr.isPresent()) {
            					sv.uplevelnoErr.set(RUCV);
            				}
        				}
    				}
    	    	}
    		}
    		else {
    			if(isNE(sv.uplevelno,SPACES)){
    				sv.uplevelnoErr.set(RUBP);
    			}
    		}
    		if(isEQ(sv.uplevelno, SPACES)) {
    			sv.upleveldes.set(" ");
    		}
    	} else {
    		sv.uplevelnoOut[Varcom.nd.toInt()].set("Y");
    		/*sv.uplevelno.set(" ");
    		sv.upleveldes.set(" ");*/
    	}
    	
    }
    
    protected void valDate() {	
    	datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
    	if (isEQ(wsspcomn.flag, "I")) {
    	if (isGT(sv.regdate, wsaaToday) && isNE(sv.regdate, varcom.vrcmMaxDate) ) {
			sv.regdateErr.set(HL01);
		} 
    	}
    	if (isEQ(wsspcomn.flag, "J")) {
	    	if (isGT(sv.moddate, wsaaToday) && isNE(sv.moddate, varcom.vrcmMaxDate) ) {
				sv.moddateErr.set(HL01);
			} 
    	}
    	if (isEQ(wsspcomn.flag, "K")) {
	    	if (isGT(sv.deldate, wsaaToday) && isNE(sv.deldate, varcom.vrcmMaxDate) ) {
				sv.deldateErr.set(HL01);
			} 
		} 
    }
	
	protected void chgClient2120()
	{
			/*    Validate client number*/
			Agsdpf agsd = agsdpfDAO.getAgsdpfData(sv.clntsel.toString());
			if(agsd != null) {
				if (isEQ(wsspcomn.flag, "I")) {
					if (isEQ(agsd.getClientno(),sv.clntsel.trim())) {
						sv.clntselErr.set(JL84);
					}
				}
				if (isEQ(wsspcomn.flag, "J")) {
					Agsdpf ags = agsdpfDAO.getDatabyLevelno(sv.levelno.toString());
					if(null != ags) {
						if (isEQ(agsd.getClientno(),sv.clntsel.trim()) && isNE(sv.clntsel, SPACE)
									&& isNE(agsd.getClientno(),ags.getClientno())) {
							sv.clntselErr.set(JL84);
						}
					}
				}
			}
			else {
				Clntpf clntpf = cltsioCall2700(sv.clntsel.toString());
				if (clntpf == null) {
					sv.clntselErr.set(E058);
					return ;
				}
				if (!"1".equals(clntpf.getValidflag())) {
					sv.clntselErr.set(F393);
					return ;
				}
				Clntpf clnt = clntpfDAO.selectActiveClient(clntpf);
				if(clnt !=null) {
					if ((clnt.getCltdod() != 0
						&& isNE(clnt.getCltdod(), varcom.vrcmMaxDate)) || isEQ(clnt.getCltstat(),"DC")) {
							sv.clntselErr.set(F782);
							return ;
					}
					
					else
					{
						if(!clnt.getCltstat().equals("AC")) {
							sv.clntselErr.set(JL89);
							return ;
						}
					}
				}
				plainname(clntpf);
				wsspcomn.clntkey.set(sv.clntsel);
				sv.cltname.set(wsspcomn.longconfname);
			}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	
	@Override
	protected void update3000() {
		
		if (isEQ(wsspcomn.flag, "I")){
			insertAgsdpf();
			updateClntpf();
			insertClrrpf();
			if(isEQ(sv.leveltype, "3")) {
				insertHierpf();
			}
		}
		if (isEQ(wsspcomn.flag, "J")){
			agsdpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
			Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
			if (isExists.isPresent()) {
				if((isEQ(sv.leveltype, "3")) && (isEQ(agsdpf.getLevelclass(), "3"))){
					updateHierpf1010();
				}
				else if(isNE(agsdpf.getLevelclass(),sv.leveltype)) {
					updateHierpf1020();
					updateClntpf1020(agsdpf);
					updateClrrpf1020();
				}
			}
			updateAgsdpf1010();
		}
		if (isEQ(wsspcomn.flag, "K")){
			updateDatabase2010();
		}
		if (isEQ(wsspcomn.flag, "N")){
			updateDatabase3010();
		}
	}
	
	protected void updateClntpf1020(Agsdpf agsdpf) {
		
		Clntpf clnt = cltsioCall2700(agsdpf.getClientno());
		if(isEQ(agsdpf.getLevelclass(),"3")) {
			clntpfDAO.updateClntRoleflag(39, " ", " ", clnt);
		}
		if(isEQ(agsdpf.getLevelclass(),"2")) {
			clntpfDAO.updateClntRoleflag(38, " ", " ", clnt);
		}
		updateClntpf();
	}
	
	protected void insertAgsdpf() {
		
		wsaaBranch.set(sv.brnchcd);
		wsaaArea.set(sv.aracde);
		wsaaDeptcode.set(sv.saledept);
		
		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass(sv.leveltype.toString());
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString());
		agsdpf.setUserid(" ");
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("1");
		agsdpf.setRegisclass("1");
		agsdpf.setEffectivedt(sv.regdate.toInt());
		agsdpf.setReasoncd(sv.reasonreg.toString());
		agsdpf.setReasondtl(sv.resndetl.toString());
		agsdpf.setGenerationno("1");

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in AGSDPF :", ex);
		}
	}

	protected void updateClntpf() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(sv.clntsel.toString().trim());
		if(isEQ(sv.leveltype,"3")) {
			clntpfDAO.updateClntRoleflag(39, "Y", " ", clntpf);
		}
		if(isEQ(sv.leveltype,"2")) {
			clntpfDAO.updateClntRoleflag(38, "Y", " ", clntpf);
		}
	}
	
	protected void insertClrrpf() {
		
		cltrelnDTO=new CltrelnDTO();
		cltrelnDTO.setClntcoy("9");
		cltrelnDTO.setClntpfx("CN");
		cltrelnDTO.setClntnum(sv.clntsel.toString().trim());
		if(isEQ(sv.leveltype,"3")) {
			cltrelnDTO.setClrrrole("SS");
			cltrelnDTO.setForepfx("SS");
		}
		if(isEQ(sv.leveltype,"2")) {
			cltrelnDTO.setClrrrole("SB");
			cltrelnDTO.setForepfx("SB");
		}
		
		cltrelnDTO.setForecoy("2");
		cltrelnDTO.setForenum(sv.levelno.toString());
		clrrpfDAO.intsertClrrpf(cltrelnDTO);
	}
	
	protected void insertHierpf() {		
		
		Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.uplevelno.toString());
		if(agsd != null) {
			Clrrpf clrr = clrrpfDAO.getClrrForenum(agsd.getClientno(), agsd.getLevelno());
			if(clrr != null) {
				if (clrr.getClrrrole().equals("SB")) {
					wsaatoproleCode.set("SB");
					wsaauproleCode.set("SB");
				}
				if (clrr.getClrrrole().equals("SS")) {
					wsaauproleCode.set("SS");
					String level = "SS".concat(clrr.getForenum());
					Hierpf hier = hierpfDAO.getHierpfData(level, "1", "1");
					Optional<Hierpf> isExists = Optional.ofNullable(hier);
					if (isExists.isPresent()) {
						wsaatopLevel.set(hier.getToplevel());
					}
				}
			}
		}
		wsaaroleCode.set("SS");
		wsaahierCode.set(sv.levelno);
		if(isEQ(wsaatophierCode, SPACES) && isEQ(wsaatoproleCode, "SB")) {
			wsaatophierCode.set(sv.uplevelno);
		}
		wsaauphierCode.set(sv.uplevelno);
		hierpf = new Hierpf();
		hierpf.setAgentkey(wsaaKey.toString());
		hierpf.setToplevel(wsaatopLevel.toString());
		hierpf.setUpperlevel(wsaaupLevel.toString());
		hierpf.setApprovestatus(" ");
		hierpf.setEffectivedt(sv.regdate.toInt());
		hierpf.setGenerationno("1");
		hierpf.setReasoncd(sv.reasonreg.toString());
		hierpf.setReasondtl(sv.resndetl.toString());
		hierpf.setRegisclass("1");
		hierpf.setActivestatus("1");
		hierpf.setValidflag("1");
		hierpfDAO.insertHierpfDetails(hierpf);
	}
	
	protected void updateAgsdpf1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			int wsaaGen=Integer.parseInt(agsdpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
			agsdpf.setValidflag("1");
			agsdpf.setActivestatus("2");
		}
		
		try {
			agsdpfDAO.updateAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update AGSDPF :", ex);
		}
		
		if(isEQ(sv.aracde,SPACES))
			wsaaArea.set("  ");	
		else	
			wsaaArea.set(sv.aracde.trim());
		
		wsaaBranch.set(sv.brnchcd.trim());
		wsaaDeptcode.set(sv.saledept.trim());
		
		
		if(isNE(agsdpf.getClientno().trim(),sv.clntsel.trim()))
		{
			updateClntpf1020(agsdpf);
			updateClrrpf1030(agsdpf);
			insertClrrpf();			
		}
		
		agsdpf= new Agsdpf();
		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass(sv.leveltype.toString());
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString().trim());
		agsdpf.setUserid(" ");
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("1");
		agsdpf.setRegisclass("2");
		agsdpf.setEffectivedt(sv.moddate.toInt());
		agsdpf.setReasoncd(sv.reasonmod.toString());
		agsdpf.setReasondtl(sv.resndetl.toString());
		agsdpf.setGenerationno(wsaaGennum);

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in AGSDPF :", ex);
		}
	}
	
	protected void updateClrrpf1030(Agsdpf agsdpf) {
		Clrrpf clrr = clrrpfDAO.getClrrForenum(agsdpf.getClientno(), sv.levelno.toString());
		Optional<Clrrpf> isExists = Optional.ofNullable(clrr);
		if (isExists.isPresent()) {
			clrr.setUsed2b("Y");
			try {
				clrrpfDAO.updateClrrUsed2b(clrr.getUsed2b(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getForenum());
			} catch (Exception ex) {
				LOGGER.error("Failed to update CLRRPF :", ex);
			}
		}
	}
	
	protected void updateHierpf1010() {
	
		if("3".equals(agsdpf.getLevelclass())) {
			wsaaroleCode.set("SS");
		}
		else if ("2".equals(agsdpf.getLevelclass())) {
			wsaaroleCode.set("SB");
		}
		wsaahierCode.set(agsdpf.getLevelno());
		hierpf = hierpfDAO.getHierpfData(wsaaKey.toString(), "1", "1");
		Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
		if (isExists.isPresent()) {
			Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.uplevelno.toString());
			if(agsd != null) {
				if ("2".equals(agsd.getLevelclass())) {
					wsaauproleCode.set("SB");
					wsaauphierCode.set(sv.uplevelno);
				}
				if ("3".equals(agsd.getLevelclass())) {
					wsaauproleCode.set("SS");
					wsaauphierCode.set(sv.uplevelno);
				}
				Hierpf hrpf = hierpfDAO.getHierpfData(wsaaupLevel.toString(), "1", "1");
				if(hrpf != null) {
					wsaatopLevel.set(hrpf.getToplevel());
				}
				else {
					wsaatoproleCode.set("SB");
					wsaatophierCode.set(sv.uplevelno);
				}
			}
			else {
				wsaaupLevel.set(SPACES);
				wsaatopLevel.set(SPACES);
			}
			updateHierpf1030(hierpf);
		}
	}
	
	protected void updateHierpf1030(Hierpf hierpf) {
		int wsaaGen=Integer.parseInt(hierpf.getGenerationno().trim());
		int wsaaGenno=wsaaGen+1;
		wsaaGennum= String.valueOf(wsaaGenno);
		hierpf.setValidflag("1");
		hierpf.setActivestatus("2");
		try {
			hierpfDAO.updateHierpf(hierpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update HIERPF :", ex);
		}
		
		wsaaroleCode.set("SS");
		wsaahierCode.set(sv.levelno);
		hierpf.setAgentkey(wsaaKey.toString());
		hierpf.setToplevel(wsaatopLevel.toString());
		hierpf.setUpperlevel(wsaaupLevel.toString());
		hierpf.setApprovestatus(" ");
		hierpf.setEffectivedt(sv.moddate.toInt());
		hierpf.setGenerationno(wsaaGennum);
		hierpf.setReasoncd(sv.reasonmod.toString());
		hierpf.setReasondtl(sv.resndetl.toString());
		hierpf.setRegisclass("2");
		hierpf.setActivestatus("1");
		hierpf.setValidflag("1");
		hierpfDAO.insertHierpfDetails(hierpf);
	}
	
	
	protected void updateHierpf1020() {
		if(isEQ(agsdpf.getLevelclass(), "3") && (isEQ(sv.leveltype,"2"))){
			if("3".equals(agsdpf.getLevelclass())) {
				wsaaroleCode.set("SS");
			}
			else if ("2".equals(agsdpf.getLevelclass())) {
				wsaaroleCode.set("SB");
			}
			wsaahierCode.set(agsdpf.getLevelno());
			hierpf = hierpfDAO.getHierpfData(wsaaKey.toString(), "1", "1");
			Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
			if (isExists.isPresent()) {
				int wsaaGen=Integer.parseInt(hierpf.getGenerationno().trim());
				int wsaaGenno=wsaaGen+1;
				wsaaGennum= String.valueOf(wsaaGenno);
				hierpf.setValidflag("1");
				hierpf.setActivestatus("2");
				try {
					hierpfDAO.updateHierpf(hierpf);
				} catch (Exception ex) {
					LOGGER.error("Failed to update HIERPF :", ex);
				}
			}
		}
		if(isEQ(agsdpf.getLevelclass(), "2") && (isEQ(sv.leveltype,"3"))){
			insertHierpf();
		}
	}
	
	protected void updateClrrpf1020() {
		Clrrpf clrr = clrrpfDAO.getClrrForenum(sv.clntsel.toString(), sv.levelno.toString());
		Optional<Clrrpf> isExists = Optional.ofNullable(clrr);
		if (isExists.isPresent()) {
			if("3".equals(sv.leveltype.toString())) {
				clrr.setClrrrole("SS");
				clrr.setForepfx("SS");
			}
			if("2".equals(sv.leveltype.toString())) {
				clrr.setClrrrole("SB");
				clrr.setForepfx("SB");
			}
			try {
				clrrpfDAO.updateClientRole(clrr.getClntpfx(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getClrrrole(),clrr.getForepfx());
			} catch (Exception ex) {
				LOGGER.error("Failed to update CLRRPF :", ex);
			}
		}
	}
	
	protected void updateDatabase2010() {
		
		/* Update Agsdpf */
		agsdpf = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			int wsaaGen=Integer.parseInt(agsdpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
			agsdpf.setValidflag("1");
			agsdpf.setActivestatus("2");
			try {
				agsdpfDAO.updateAgsdpfDetails(agsdpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update AGSDPF :", ex);
			}
		}
		agsdpf= new Agsdpf();
		agsdpf.setLevelno(sv.levelno.toString());
		agsdpf.setLevelclass(sv.leveltype.toString());
		agsdpf.setAgentclass(sv.agtype.toString());
		agsdpf.setSalesdiv(wsaaSalediv.toString());
		agsdpf.setClientno(sv.clntsel.toString().trim());
		agsdpf.setUserid(" ");
		agsdpf.setValidflag("1");
		agsdpf.setActivestatus("2");
		agsdpf.setRegisclass("3");
		agsdpf.setEffectivedt(sv.deldate.toInt());
		agsdpf.setReasoncd(sv.reasondel.toString());
		agsdpf.setReasondtl(sv.resndetl.toString());
		agsdpf.setGenerationno(wsaaGennum);

		try {
			agsdpfDAO.insertAgsdpfDetails(agsdpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert in AGSDPF :", ex);
		}
		
		
		/* Update Hierpf */
		if("3".equals(agsdpf.getLevelclass())) {
			wsaaroleCode.set("SS");
			wsaahierCode.set(sv.levelno);
			hierpf = hierpfDAO.getAgntkey(wsaaKey.toString(), "1");
			Optional<Hierpf> isPrsnt = Optional.ofNullable(hierpf);
			if (isPrsnt.isPresent()) {
				int wsaaGen=Integer.parseInt(hierpf.getGenerationno().trim());
				int wsaaGenno=wsaaGen+1;
				wsaaGennum= String.valueOf(wsaaGenno);
				hierpf.setValidflag("1");
				hierpf.setActivestatus("2");
				try {
					hierpfDAO.updateHierpf(hierpf);
				} catch (Exception ex) {
					LOGGER.error("Failed to update HIERPF :", ex);
				}
			}
			hierpf.setAgentkey(wsaaKey.toString());
			hierpf.setToplevel(hierpf.getToplevel());
			hierpf.setUpperlevel(hierpf.getUpperlevel());
			hierpf.setApprovestatus(" ");
			hierpf.setEffectivedt(sv.deldate.toInt());
			hierpf.setGenerationno(wsaaGennum);
			hierpf.setReasoncd(sv.reasondel.toString());
			hierpf.setReasondtl(sv.resndetl.toString());
			hierpf.setRegisclass("3");
			hierpf.setActivestatus("2");
			hierpf.setValidflag("1");
			try {
				hierpfDAO.insertHierpfDetails(hierpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update HIERPF :", ex);
			}
		}
		
		/* Update Clrrpf */
		Clrrpf clrr = clrrpfDAO.getClrrForenum(sv.clntsel.toString(), sv.levelno.toString());
		Optional<Clrrpf> flag = Optional.ofNullable(clrr);
		if (flag.isPresent()) {
			clrr.setUsed2b("Y");
			try {
				clrrpfDAO.updateClrrUsed2b(clrr.getUsed2b(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getForenum());
			} catch (Exception ex) {
				LOGGER.error("Failed to update CLRRPF :", ex);
			}
		}
		
	}
	
	protected void updateDatabase3010(){
		
		/* Update Agsdpf */
		agsdpfList = agsdpfDAO.getRecbyLevelNo(sv.levelno.toString().trim());
		if(!agsdpfList.isEmpty()) {
			for (Agsdpf agsdpf : agsdpfList) {
				if( "1".equals(agsdpf.getValidflag())) {
					agsdpf.setValidflag("2");
					try {
						agsdpfDAO.updateAgsdpfRec(agsdpf);
					} catch (Exception ex) {
						LOGGER.error("Failed to update AGSDPF :", ex);
					}
					
					/* Update Hierpf */
					Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.levelno.toString().trim());
					if(null != agsd) {
						if("2".equals(agsd.getLevelclass())) {
							wsaaroleCode.set("SB");
						}
						if("3".equals(agsd.getLevelclass())) {
							wsaaroleCode.set("SS");
						}
					}
					wsaahierCode.set(sv.levelno);
					hierpf = hierpfDAO.getAgntkey(wsaaKey.toString(), "1");
					Optional<Hierpf> isPrsnt = Optional.ofNullable(hierpf);
					if (isPrsnt.isPresent()) {
						hierpf.setValidflag("2");
						try {
							hierpfDAO.updateHierpfDetails(hierpf);
						} catch (Exception ex) {
							LOGGER.error("Failed to update HIERPF :", ex);
						}
					}
				}
			}

			/* Update Clrrpf */
			Clrrpf clrr = clrrpfDAO.getClrrForenum(sv.clntsel.toString(), sv.levelno.toString());
			Optional<Clrrpf> flag = Optional.ofNullable(clrr);
			if (flag.isPresent() && !"Y".equals(clrr.getUsed2b())) {
				clrr.setUsed2b("Y");
				try {
					clrrpfDAO.updateClrrUsed2b(clrr.getUsed2b(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getForenum());
				} catch (Exception ex) {
					LOGGER.error("Failed to update CLRRPF :", ex);
				}
			}			
		}
		/* Update lower hierarchy inactive records */
		updateDatabase3020(wsaaKey.toString());
	}
	
	protected void updateDatabase3020(String levelno) {
		
		hierpfList = hierpfDAO.getAllAgntRec(levelno, levelno, "1", "2");
		if(!hierpfList.isEmpty()) {
			for(Hierpf hier : hierpfList) {
				wsaaKey.set(hier.getAgentkey());
				agsdpf = agsdpfDAO.getAgsdpfData(wsaahierCode.toString(), "2", "1");
				Optional<Agsdpf> isPrsnt = Optional.ofNullable(agsdpf);
				if (isPrsnt.isPresent()) {
					agsdpf.setValidflag("2");
					try {
						agsdpfDAO.updateAgsdpfRec(agsdpf);
					} catch (Exception ex) {
						LOGGER.error("Failed to update AGSDPF :", ex);
					}
				}
				
				/* Update Hierpf */
				hierpf = hierpfDAO.getHierpfData(hier.getAgentkey(), "2", "1");
				Optional<Hierpf> flag = Optional.ofNullable(hierpf);
				if (flag.isPresent()) {
					hierpf.setValidflag("2");
					try {
						hierpfDAO.updateHierpfDetails(hierpf);
					} catch (Exception ex) {
						LOGGER.error("Failed to update HIERPF :", ex);
					}
				}
				
				/* Update Clrrpf */
				Clrrpf clrr = clrrpfDAO.getClrrForenum(agsdpf.getClientno(), wsaahierCode.toString());
				Optional<Clrrpf> prsnt = Optional.ofNullable(clrr);
				if (prsnt.isPresent() && !"Y".equals(clrr.getUsed2b())) {
					clrr.setUsed2b("Y");
					try {
						clrrpfDAO.updateClrrUsed2b(clrr.getUsed2b(), clrr.getClntcoy(), clrr.getClntnum(),clrr.getForenum());
					} catch (Exception ex) {
						LOGGER.error("Failed to update CLRRPF :", ex);
					}
				}	
			}
		}
	}
	
	@Override
	protected void whereNext4000() {
		wsspcomn.confirmationKey.set(levelshortdesc.trim() + " " + sv.levelno.toString());
		if(isEQ(wsspcomn.flag, "N")){
			wsspcomn.programPtr.add(2);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}
}
