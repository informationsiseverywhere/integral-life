package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgorpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:45
 * Class transformed from AGORPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgorpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 127;
	public FixedLengthStringData agorrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agorpfRecord = agorrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agorrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agorrec);
	public FixedLengthStringData reportag = DD.reportag.copy().isAPartOf(agorrec);
	public FixedLengthStringData agtype01 = DD.agtype.copy().isAPartOf(agorrec);
	public FixedLengthStringData agtype02 = DD.agtype.copy().isAPartOf(agorrec);
	public PackedDecimalData effdate01 = DD.effdate.copy().isAPartOf(agorrec);
	public PackedDecimalData effdate02 = DD.effdate.copy().isAPartOf(agorrec);
	public FixedLengthStringData desc = DD.desc.copy().isAPartOf(agorrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agorrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agorrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agorrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgorpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgorpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgorpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgorpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgorpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgorpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgorpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGORPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"REPORTAG, " +
							"AGTYPE01, " +
							"AGTYPE02, " +
							"EFFDATE01, " +
							"EFFDATE02, " +
							"DESC_T, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     reportag,
                                     agtype01,
                                     agtype02,
                                     effdate01,
                                     effdate02,
                                     desc,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		reportag.clear();
  		agtype01.clear();
  		agtype02.clear();
  		effdate01.clear();
  		effdate02.clear();
  		desc.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgorrec() {
  		return agorrec;
	}

	public FixedLengthStringData getAgorpfRecord() {
  		return agorpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgorrec(what);
	}

	public void setAgorrec(Object what) {
  		this.agorrec.set(what);
	}

	public void setAgorpfRecord(Object what) {
  		this.agorpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agorrec.getLength());
		result.set(agorrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}