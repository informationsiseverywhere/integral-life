package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl64ScreenVars;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pjl64  extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl64.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL64");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl64ScreenVars sv = ScreenProgram.getScreenVars(Sjl64ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaatopLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaatoproleCode = new FixedLengthStringData(2).isAPartOf(wsaatopLevel, 0);
	private FixedLengthStringData wsaatophierCode = new FixedLengthStringData(8).isAPartOf(wsaatopLevel, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	
	private Gensswrec gensswrec = new Gensswrec();
	private Batckey wsaaBatckey = new Batckey();	
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private List<Hierpf> hierpfList = new ArrayList<>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Descpf desc = new Descpf();
	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String T3692 = "T3692";
	private static final String TJL68 = "TJL68";
	private List<Descpf> t3692List;
	private String sjl64 = "Sjl64";

	public Pjl64() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl64", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		wsaaBatckey.set(wsspcomn.batchkey);	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			sv.history.set("?");
			return;
		}
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		initSubfile1200();
		initialise1010();
		protect1020();
		cont1030();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(sjl64, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
			}
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltype.set(agsdpf.getLevelclass());
				loadDesc();
			}
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.agtdesc.set(descItem.getLongdesc());		
					}
				}
			}
			sv.levelno.set(agsdpf.getLevelno());
			initialise1020(agsdpf);
		}
	}
	
	protected void initialise1020(Agsdpf agsdpf) {
		
		wsaahierCode.set(agsdpf.getLevelno());
		if (null != agsdpf.getLevelclass())
		{
			if("3".equals(agsdpf.getLevelclass())) {
				wsaaroleCode.set("SS");
			}
			else if ("2".equals(agsdpf.getLevelclass())) {
				wsaaroleCode.set("SB");
			}
		}
		Hierpf hrp  = hierpfDAO.getHierpfData(wsaaKey.toString(), "1","1");
		if (null != hrp){
					wsaaupLevel.set(hrp.getUpperlevel());
					if("SB".equals(wsaauproleCode.toString()) || "SS".equals(wsaauproleCode.toString())){
						sv.ulevelno.set(wsaauphierCode.toString());
						if("SS".equals(wsaauproleCode.toString())) {
							sv.uleveltype.set("3");
							desc = descDAO.getdescData("IT", TJL70, "3", wsspcomn.company.toString(), wsspcomn.language.toString());
							if(null != desc) {
								sv.uleveldes.set(desc.getLongdesc());
							}
						}
						if("SB".equals(wsaauproleCode.toString())) {
							sv.uleveltype.set("2");
							desc = descDAO.getdescData("IT", TJL70, "2", wsspcomn.company.toString(), wsspcomn.language.toString());
							if(null != desc) {
								sv.uleveldes.set(desc.getLongdesc());
							}
						}
						initialise1030(wsaauphierCode.toString());
					}
					
					wsaatopLevel.set(hrp.getToplevel());
					if("SB".equals(wsaatoproleCode.toString()) || "SS".equals(wsaatoproleCode.toString())) {
						sv.tlevelno.set(wsaatophierCode.toString());
						if("SS".equals(wsaatoproleCode.toString())) {
							sv.tleveltype.set("3");
							desc = descDAO.getdescData("IT", TJL70, "3", wsspcomn.company.toString(), wsspcomn.language.toString());
							if(null != desc) {
								sv.tleveldes.set(desc.getLongdesc());
							}
						}
						if("SB".equals(wsaatoproleCode.toString())) {
							sv.tleveltype.set("2");
							desc = descDAO.getdescData("IT", TJL70, "2", wsspcomn.company.toString(), wsspcomn.language.toString());
							if(null != desc) {
								sv.tleveldes.set(desc.getLongdesc());
							}
						}
						initialise1040(wsaatophierCode.toString());
					}				
		}
		/*To display the lower hierarchy*/
		hierpfList = hierpfDAO.getAllAgntRec(wsaaKey.toString(), wsaaKey.toString(), "1","1");
		if(!hierpfList.isEmpty()) {
			for (Hierpf hier : hierpfList) {
				wsaaKey.set(hier.getAgentkey());
				if("SS".equals(wsaaroleCode.toString())) {
					initialise1060(hier.getAgentkey());
				}
			}
		}
	}
	
	protected void initialise1030(String levelno) {
		
		agsdpf = agsdpfDAO.getDatabyLevelno(levelno.trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.uclntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.ucltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.uagtdesc.set(descItem.getLongdesc());		
					}
				}
			}
		}
	}
	
	protected void initialise1040(String levelno) {
		
		agsdpf = agsdpfDAO.getDatabyLevelno(levelno.trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.tclntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.tcltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.tagtdesc.set(descItem.getLongdesc());		
					}
				}
			}
		}
	}
	
	protected void initialise1060(String levelno) {
		wsaaKey.set(levelno);
		sv.llevelno.set(wsaahierCode.toString());
		agsdpf = agsdpfDAO.getDatabyLevelno(sv.llevelno.toString());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.slt.set(SPACES);
			sv.lclntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.lcltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.lagtdesc.set(descItem.getLongdesc());		
					}
				}
			}
			if (null != agsdpf.getLevelclass()) {
				if("3".equals(agsdpf.getLevelclass())) {
					desc = descDAO.getdescData("IT", TJL70, "3", wsspcomn.company.toString(), wsspcomn.language.toString());
					if(null != desc) {
						sv.lleveldes.set(desc.getLongdesc());
					}
				}
				if("2".equals(agsdpf.getLevelclass())) {
					desc = descDAO.getdescData("IT", TJL70, "2", wsspcomn.company.toString(), wsspcomn.language.toString());
					if(null != desc) {
						sv.lleveldes.set(desc.getLongdesc());
					}
				}
			}
			scrnparams.function.set(Varcom.sadd);
			processScreen(sjl64, sv);
			if (isNE(scrnparams.statuz, Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
	}
	
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected void protect1020() {
		sv.clntselOut[Varcom.pr.toInt()].set("Y");
		sv.cltnameOut[Varcom.pr.toInt()].set("Y");
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.levelnoOut[Varcom.pr.toInt()].set("Y");
		sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.leveldesOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
		sv.agtypeOut[Varcom.pr.toInt()].set("Y");
		sv.uclntselOut[Varcom.pr.toInt()].set("Y");
		sv.ucltnameOut[Varcom.pr.toInt()].set("Y");
		sv.ulevelnoOut[Varcom.pr.toInt()].set("Y");
		sv.uleveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.uleveldesOut[Varcom.pr.toInt()].set("Y");
		sv.uagtypeOut[Varcom.pr.toInt()].set("Y");
		sv.tclntselOut[Varcom.pr.toInt()].set("Y");
		sv.tcltnameOut[Varcom.pr.toInt()].set("Y");
		sv.tlevelnoOut[Varcom.pr.toInt()].set("Y");
		sv.tleveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.tleveldesOut[Varcom.pr.toInt()].set("Y");
		sv.tagtypeOut[Varcom.pr.toInt()].set("Y");
		sv.agtdescOut[Varcom.pr.toInt()].set("Y");
		sv.tagtdescOut[Varcom.pr.toInt()].set("Y");
		sv.uagtdescOut[Varcom.pr.toInt()].set("Y");
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	
	protected void cont1030(){
		wsaahierCode.set(sv.levelno.toString());
		if("3".equals(sv.leveltype.toString())) {
			wsaaroleCode.set("SS");
		}
		else if ("2".equals(sv.leveltype.toString())) {
			wsaaroleCode.set("SB");
		}
		Hierpf hier = hierpfDAO.getHierpfTop(wsaaKey.toString(),"1");
		Optional<Hierpf> isExists = Optional.ofNullable(hier);
		if (isExists.isPresent() || ("2".equals(sv.leveltype.toString()))) {
			sv.historyOut[Varcom.pr.toInt()].set("Y");
			sv.history.set(SPACES);
		}else{
			sv.historyOut[Varcom.pr.toInt()].set("N");
			sv.history.set("+");
		}
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return ;
	}
	
	@Override
	protected void screenEdit2000() {
		
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	@Override
	protected void update3000() {
		wsspcomn.chdrCownnum.set(sv.levelno.trim());
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
	}
	
	@Override
	protected void whereNext4000() {
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
				saveProgram4100();
			}
		}
		if (isEQ(sv.history, "X")) {
			sv.history.set("+");
			gensswrec.function.set("A");
			callGenssw4300();
			return ;
		}
		if (isEQ(sv.history, "?")) {
			historyRet4900();
		}					
		nextProgram4010();
	}
	
	protected void callGenssw4300() {
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, Varcom.oK) && isNE(gensswrec.statuz, Varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}

		if (isEQ(gensswrec.statuz, Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set("H093");
			wsspcomn.nextprog.set(scrnparams.scrname);
			return;
		}
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; loopVar3 != 8; loopVar3 += 1) {
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void loadProgram4400() {
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
	}

	protected void endChoices4050() {
		/*    No more selected (or none)*/
		/*       - restore stack form wsaa to wssp*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar2 = 0; loopVar2 != 8; loopVar2 += 1){
				restoreProgram4200();
			}
		}
		/*    If current stack action is * then re-display screen*/
		/*       (in this case, some other option(s) were requested)*/
		/*    Otherwise continue as normal.*/
	

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		} else {
			wsspcomn.programPtr.add(1);
		}
	}
	
	protected void restoreProgram4200()
	{
		wsaaX.add(1);
		wsaaY.add(1);
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
	}

	protected void saveProgram4100() {
		wsaaX.add(1);
		wsaaY.add(1);
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
	}
	
	protected void historyRet4900()
	{
		sv.history.set("+");
		
		/*  - restore the saved programs to the program stack              */
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar10 = 0; loopVar10 != 8; loopVar10 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);

	}
	
	protected void restore4610()
	{
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaY.add(1);
		wsaaX.add(1);
	}
	
	protected void nextProgram4010(){
		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			wsaahierCode.set(agsdpf.getLevelno());
			if (null != agsdpf.getLevelclass())
			{
				if("3".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SS");
				}
				else if ("2".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SB");
				}
			}
			hierpfList = hierpfDAO.getHierpfUp(wsaaKey.toString(), "1");
			if(!hierpfList.isEmpty()) {
				for(Hierpf hrpf : hierpfList) {
					 wsaaKey.set(hrpf.getAgentkey());
					if ("AG".equals(wsaaroleCode.toString())){
						wsspcomn.nextprog.set(wsaaProg);
						wsspcomn.programPtr.add(1); 
						return;
					}
				}
			}
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(2); 
	}
}