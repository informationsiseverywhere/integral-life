/*
 * File: Tcmca01.java
 * Date: 30 August 2009 2:33:21
 * Author: Quipoz Limited
 * 
 * Class transformed from TCMCA01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.CovrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5565rec;
import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*             THAI AGE/TERM BANDED INITIAL COMMISSION
*
* This Subroutine is cloned from CMCA001. The only difference
* is that Commission Calculations for some Riders will always
* be based on the Main Coverage, as opposed to the individual
* Rider.
*
* Thus, when processing for a Rider, to access Table T5565,
* the key is composed of CRTABLE from the Main Cover of the
* Contract, the TERM from the Term of the Rider.  The rest of
* logic remains unchanged.
*
*****************************************************************
* </pre>
*/
public class Tcmca01 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "TCMCA01";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5692Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentClass = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 4).init(SPACES);
	private PackedDecimalData wsaaT5565 = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaInprempc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaIncmrate = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaY = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommAmt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaT5565Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5565Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5565Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5565Srcebus = new FixedLengthStringData(2).isAPartOf(wsaaT5565Key, 4).init(SPACES);
	private static final String h026 = "H026";
	private static final String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private static final String t5692 = "T5692";
	private static final String t5565 = "T5565";
	private static final String t1693 = "T1693";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrcmcTableDAM covrcmcIO = new CovrcmcTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5692rec t5692rec = new T5692rec();
	private T5565rec t5565rec = new T5565rec();
	private T1693rec t1693rec = new T1693rec();
	private Comlinkrec comlinkrec = new Comlinkrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1050, 
		avoidT5692Move3010, 
		continue4520, 
		continue4540, 
		exit4590
	}

	public Tcmca01() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		readChdrlnb500();
		obtainCalcRules1000();
		calcTermBand2000();
		calcInitCommission3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readChdrlnb500()
	{
		/*START*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrlnbIO.setChdrnum(comlinkrec.chdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void obtainCalcRules1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
				case next1050: 
					next1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		/* Coverage and Rider Codes being Hard Coded, as we are*/
		/* always looking for the Main Coverage.*/
		covrlnbIO.setChdrcoy(comlinkrec.chdrcoy);
		covrlnbIO.setChdrnum(comlinkrec.chdrnum);
		covrlnbIO.setLife(comlinkrec.life);
		covrlnbIO.setCoverage("01");
		covrlnbIO.setRider("00");
		covrlnbIO.setPlanSuffix(comlinkrec.planSuffix);
		covrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			dbError8100();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5565);
		/* Change T5565 item key, add soure of business as part of the key.*/
		/* MOVE COVRLNB-CRTABLE        TO ITDM-ITEMITEM.                */
		wsaaT5565Crtable.set(covrlnbIO.getCrtable());
		wsaaT5565Srcebus.set(chdrlnbIO.getSrcebus());
		itdmIO.setItemitem(wsaaT5565Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
	}

protected void next1050()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(), comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5565)
		|| isNE(itdmIO.getItemitem(), wsaaT5565Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			if (isNE(itdmIO.getItemitem(), wsaaT5565Key)
			&& isNE(wsaaT5565Srcebus, "**")) {
				itdmIO.setDataKey(SPACES);
				itdmIO.setItemcoy(comlinkrec.chdrcoy);
				itdmIO.setItemtabl(t5565);
				wsaaT5565Crtable.set(covrlnbIO.getCrtable());
				wsaaT5565Srcebus.set("**");
				itdmIO.setItemitem(wsaaT5565Key);
				itdmIO.setItmfrm(comlinkrec.effdate);
				itdmIO.setFunction(varcom.begn);
				goTo(GotoLabel.next1050);
				/* UNREACHABLE CODE
				syserrrec.params.set(covrlnbIO.getCrtable());
				syserrrec.statuz.set(h026);
				dbError8100();
				 */
			}
			else {
				t5565rec.t5565Rec.set(itdmIO.getGenarea());
			}
		}
	}

protected void calcTermBand2000()
	{
		obtainPremDate2020();
		calculateTerm2040();
	}

protected void obtainPremDate2020()
	{
		covrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		covrcmcIO.setChdrnum(comlinkrec.chdrnum);
		covrcmcIO.setLife(comlinkrec.life);
		covrcmcIO.setCoverage(comlinkrec.coverage);
		covrcmcIO.setRider(comlinkrec.rider);
		covrcmcIO.setPlanSuffix(comlinkrec.planSuffix);
		covrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrcmcIO);
		if (isNE(covrcmcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrcmcIO.getParams());
			syserrrec.statuz.set(covrcmcIO.getStatuz());
			dbError8100();
		}
	}

protected void calculateTerm2040()
	{
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.intDate2.set(covrcmcIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		compute(wsaaTerm, 5).set(add(datcon3rec.freqFactor, 0.999));
		/*EXIT*/
	}

protected void calcInitCommission3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3000();
				case avoidT5692Move3010: 
					avoidT5692Move3010();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		itdmIO.setDataKey(SPACES);
		wsaaCrtable.set(covrlnbIO.getCrtable());
		wsaaAgentClass.set(comlinkrec.agentClass);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5692);
		itdmIO.setItemitem(wsaaT5692Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(), comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5692)
		|| isNE(itdmIO.getItemitem(), wsaaT5692Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemtabl(t5692);
			wsaaCrtable.set("****");
			itdmIO.setItemitem(wsaaT5692Key);
			itdmIO.setItmfrm(comlinkrec.effdate);
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getItemcoy(), comlinkrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(), t5692)
			|| isNE(itdmIO.getItemitem(), wsaaT5692Key)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				t5692rec.age01.set(ZERO);
				t5692rec.age02.set(ZERO);
				t5692rec.age03.set(ZERO);
				t5692rec.age04.set(ZERO);
				goTo(GotoLabel.avoidT5692Move3010);
			}
		}
		t5692rec.t5692Rec.set(itdmIO.getGenarea());
	}

protected void avoidT5692Move3010()
	{
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife, SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifecmcIO.getParams());
			syserrrec.statuz.set(lifecmcIO.getStatuz());
			dbError8100();
		}
		if (isNE(comlinkrec.effdate, covrcmcIO.getCrrcd())) {
			calcAge3100();
		}
		getT5692Value4000();
		getT5565Value4500();
		compute(wsaaT5565, 3).setRounded(div(mult(wsaaT5565, wsaaInprempc), 100));
		compute(wsaaCommAmt, 3).setRounded(div(mult(comlinkrec.annprem, wsaaT5565), 100));
		compute(wsaaCommAmt, 3).setRounded(div(mult(wsaaCommAmt, wsaaIncmrate), 100));
		comlinkrec.icommtot.set(wsaaCommAmt);
	}

protected void calcAge3100()
	{
		init3110();
	}

protected void init3110()
	{
		/* CHDRLNB is already read in 500 section.                         */
		/*    MOVE CHDRLNBREC             TO CHDRLNB-FORMAT.               */
		/*    MOVE CLNK-CHDRCOY           TO CHDRLNB-CHDRCOY.              */
		/*    MOVE CLNK-CHDRNUM           TO CHDRLNB-CHDRNUM.              */
		/*    MOVE READR                  TO CHDRLNB-FUNCTION.             */
		/*    CALL 'CHDRLNBIO'            USING CHDRLNB-PARAMS.            */
		/*    IF CHDRLNB-STATUZ        NOT = O-K                           */
		/*        MOVE CHDRLNB-PARAMS     TO SYSR-PARAMS                   */
		/*        MOVE CHDRLNB-STATUZ     TO SYSR-STATUZ                   */
		/*    END-IF.                                                      */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(comlinkrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifecmcIO.getCltdob());
		agecalcrec.intDate2.set(comlinkrec.effdate);
		agecalcrec.language.set(comlinkrec.language);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError8000();
		}
		lifecmcIO.setAnbAtCcd(agecalcrec.agerating);
	}

protected void getT5692Value4000()
	{
		para4000();
	}

protected void para4000()
	{
		wsaaInprempc.set(100);
		wsaaIncmrate.set(100);
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5692rec.age01)) {
			wsaaIncmrate.set(t5692rec.incmrate01);
			wsaaInprempc.set(t5692rec.inprempc01);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5692rec.age02)) {
			wsaaIncmrate.set(t5692rec.incmrate02);
			wsaaInprempc.set(t5692rec.inprempc02);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5692rec.age03)) {
			wsaaIncmrate.set(t5692rec.incmrate03);
			wsaaInprempc.set(t5692rec.inprempc03);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5692rec.age04)) {
			wsaaIncmrate.set(t5692rec.incmrate04);
			wsaaInprempc.set(t5692rec.inprempc04);
			return ;
		}
	}

protected void getT5565Value4500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4500();
				case continue4520: 
					continue4520();
				case continue4540: 
					continue4540();
				case exit4590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4500()
	{
		wsaaY.set(0);
		wsaaX.set(0);
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd01)) {
			wsaaY.set(1);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd02)) {
			wsaaY.set(2);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd03)) {
			wsaaY.set(3);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd04)) {
			wsaaY.set(4);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd05)) {
			wsaaY.set(5);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd06)) {
			wsaaY.set(6);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd07)) {
			wsaaY.set(7);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd08)) {
			wsaaY.set(8);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd09)) {
			wsaaY.set(9);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(), t5565rec.anbAtCcd10)) {
			wsaaY.set(10);
			goTo(GotoLabel.continue4520);
		}
		goTo(GotoLabel.exit4590);
	}

protected void continue4520()
	{
		/*  WORK OUT THE ARRAY'S X INDEX BASED ON TERM*/
		if (isLTE(wsaaTerm, t5565rec.termMax01)) {
			wsaaX.set(1);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax02)) {
			wsaaX.set(2);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax03)) {
			wsaaX.set(3);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax04)) {
			wsaaX.set(4);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax05)) {
			wsaaX.set(5);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax06)) {
			wsaaX.set(6);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax07)) {
			wsaaX.set(7);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax08)) {
			wsaaX.set(8);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm, t5565rec.termMax09)) {
			wsaaX.set(9);
			goTo(GotoLabel.continue4540);
		}
		goTo(GotoLabel.exit4590);
	}

protected void continue4540()
	{
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY, 9));
		wsaaIndex.add(wsaaX);
		wsaaT5565.set(t5565rec.prcnt[wsaaIndex.toInt()]);
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
