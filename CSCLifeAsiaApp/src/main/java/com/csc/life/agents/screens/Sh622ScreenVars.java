package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH622
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sh622ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(652);
	public FixedLengthStringData dataFields = new FixedLengthStringData(172).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData keyopts = new FixedLengthStringData(2).isAPartOf(dataFields, 25);
	public FixedLengthStringData[] keyopt = FLSArrayPartOfStructure(2, 1, keyopts, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(keyopts, 0, FILLER_REDEFINE);
	public FixedLengthStringData keyopt01 = DD.keyopt.copy().isAPartOf(filler,0);
	public FixedLengthStringData keyopt02 = DD.keyopt.copy().isAPartOf(filler,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData zryrpercs = new FixedLengthStringData(110).isAPartOf(dataFields, 62);
	public ZonedDecimalData[] zryrperc = ZDArrayPartOfStructure(22, 5, 2, zryrpercs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(110).isAPartOf(zryrpercs, 0, FILLER_REDEFINE);
	public ZonedDecimalData zryrperc01 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData zryrperc02 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData zryrperc03 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData zryrperc04 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData zryrperc05 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData zryrperc06 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,25);
	public ZonedDecimalData zryrperc07 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData zryrperc08 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,35);
	public ZonedDecimalData zryrperc09 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,40);
	public ZonedDecimalData zryrperc10 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,45);
	public ZonedDecimalData zryrperc11 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,50);
	public ZonedDecimalData zryrperc12 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,55);
	public ZonedDecimalData zryrperc13 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,60);
	public ZonedDecimalData zryrperc14 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,65);
	public ZonedDecimalData zryrperc15 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,70);
	public ZonedDecimalData zryrperc16 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,75);
	public ZonedDecimalData zryrperc17 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,80);
	public ZonedDecimalData zryrperc18 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData zryrperc19 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,90);
	public ZonedDecimalData zryrperc20 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,95);
	public ZonedDecimalData zryrperc21 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,100);
	public ZonedDecimalData zryrperc22 = DD.zryrperc.copyToZonedDecimal().isAPartOf(filler1,105);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 172);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData keyoptsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] keyoptErr = FLSArrayPartOfStructure(2, 4, keyoptsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(keyoptsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData keyopt01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData keyopt02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData zryrpercsErr = new FixedLengthStringData(88).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] zryrpercErr = FLSArrayPartOfStructure(22, 4, zryrpercsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(88).isAPartOf(zryrpercsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zryrperc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zryrperc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zryrperc03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData zryrperc04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData zryrperc05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData zryrperc06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData zryrperc07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData zryrperc08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData zryrperc09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData zryrperc10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData zryrperc11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData zryrperc12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData zryrperc13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData zryrperc14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData zryrperc15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData zryrperc16Err = new FixedLengthStringData(4).isAPartOf(filler3, 60);
	public FixedLengthStringData zryrperc17Err = new FixedLengthStringData(4).isAPartOf(filler3, 64);
	public FixedLengthStringData zryrperc18Err = new FixedLengthStringData(4).isAPartOf(filler3, 68);
	public FixedLengthStringData zryrperc19Err = new FixedLengthStringData(4).isAPartOf(filler3, 72);
	public FixedLengthStringData zryrperc20Err = new FixedLengthStringData(4).isAPartOf(filler3, 76);
	public FixedLengthStringData zryrperc21Err = new FixedLengthStringData(4).isAPartOf(filler3, 80);
	public FixedLengthStringData zryrperc22Err = new FixedLengthStringData(4).isAPartOf(filler3, 84);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 292);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData keyoptsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] keyoptOut = FLSArrayPartOfStructure(2, 12, keyoptsOut, 0);
	public FixedLengthStringData[][] keyoptO = FLSDArrayPartOfArrayStructure(12, 1, keyoptOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(keyoptsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] keyopt01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] keyopt02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData zryrpercsOut = new FixedLengthStringData(264).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] zryrpercOut = FLSArrayPartOfStructure(22, 12, zryrpercsOut, 0);
	public FixedLengthStringData[][] zryrpercO = FLSDArrayPartOfArrayStructure(12, 1, zryrpercOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(264).isAPartOf(zryrpercsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zryrperc01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] zryrperc02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zryrperc03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] zryrperc04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] zryrperc05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] zryrperc06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] zryrperc07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] zryrperc08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] zryrperc09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] zryrperc10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] zryrperc11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public FixedLengthStringData[] zryrperc12Out = FLSArrayPartOfStructure(12, 1, filler5, 132);
	public FixedLengthStringData[] zryrperc13Out = FLSArrayPartOfStructure(12, 1, filler5, 144);
	public FixedLengthStringData[] zryrperc14Out = FLSArrayPartOfStructure(12, 1, filler5, 156);
	public FixedLengthStringData[] zryrperc15Out = FLSArrayPartOfStructure(12, 1, filler5, 168);
	public FixedLengthStringData[] zryrperc16Out = FLSArrayPartOfStructure(12, 1, filler5, 180);
	public FixedLengthStringData[] zryrperc17Out = FLSArrayPartOfStructure(12, 1, filler5, 192);
	public FixedLengthStringData[] zryrperc18Out = FLSArrayPartOfStructure(12, 1, filler5, 204);
	public FixedLengthStringData[] zryrperc19Out = FLSArrayPartOfStructure(12, 1, filler5, 216);
	public FixedLengthStringData[] zryrperc20Out = FLSArrayPartOfStructure(12, 1, filler5, 228);
	public FixedLengthStringData[] zryrperc21Out = FLSArrayPartOfStructure(12, 1, filler5, 240);
	public FixedLengthStringData[] zryrperc22Out = FLSArrayPartOfStructure(12, 1, filler5, 252);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sh622screenWritten = new LongData(0);
	public LongData Sh622protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh622ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, zryrperc01, zryrperc02, zryrperc03, zryrperc04, zryrperc05, zryrperc06, zryrperc07, zryrperc08, zryrperc09, zryrperc10, zryrperc21, zryrperc22, zryrperc11, zryrperc12, zryrperc13, zryrperc14, zryrperc15, zryrperc16, zryrperc17, zryrperc18, zryrperc19, zryrperc20, keyopt01, keyopt02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, zryrperc01Out, zryrperc02Out, zryrperc03Out, zryrperc04Out, zryrperc05Out, zryrperc06Out, zryrperc07Out, zryrperc08Out, zryrperc09Out, zryrperc10Out, zryrperc21Out, zryrperc22Out, zryrperc11Out, zryrperc12Out, zryrperc13Out, zryrperc14Out, zryrperc15Out, zryrperc16Out, zryrperc17Out, zryrperc18Out, zryrperc19Out, zryrperc20Out, keyopt01Out, keyopt02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, zryrperc01Err, zryrperc02Err, zryrperc03Err, zryrperc04Err, zryrperc05Err, zryrperc06Err, zryrperc07Err, zryrperc08Err, zryrperc09Err, zryrperc10Err, zryrperc21Err, zryrperc22Err, zryrperc11Err, zryrperc12Err, zryrperc13Err, zryrperc14Err, zryrperc15Err, zryrperc16Err, zryrperc17Err, zryrperc18Err, zryrperc19Err, zryrperc20Err, keyopt01Err, keyopt02Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh622screen.class;
		protectRecord = Sh622protect.class;
	}

}
