package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class Sr52zscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52zScreenVars sv = (Sr52zScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.Sr52zscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52zScreenVars screenVars = (Sr52zScreenVars)pv;
	
		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in Sr52zscreen
 */
	public static void clear(VarModel pv) {
		Sr52zScreenVars screenVars = (Sr52zScreenVars) pv;
	
		ScreenRecord.clear(pv);
	}
}
