package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl31ScreenVars extends SmartVarModel { 



	public FixedLengthStringData dataArea = new FixedLengthStringData(367);
	public FixedLengthStringData dataFields = new FixedLengthStringData(175).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agncynum = DD.agncynum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData regnum = DD.agncyRegnum.copy().isAPartOf(dataFields,18);
	public ZonedDecimalData srdate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,31);
	public ZonedDecimalData endate = DD.enddate.copyToZonedDecimal().isAPartOf(dataFields,39);
	public ZonedDecimalData ipdate = DD.ipdate.copyToZonedDecimal().isAPartOf(dataFields,47);
	public ZonedDecimalData ppdate = DD.ppdate.copyToZonedDecimal().isAPartOf(dataFields,55);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,110);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,142);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,145);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 175);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agncynumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData endateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ipdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ppdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 223);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agncynumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] endateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ipdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ppdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	

	public FixedLengthStringData srdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData endateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ipdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ppdateDisp = new FixedLengthStringData(10);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public LongData Sjl31screenWritten = new LongData(0);
	public LongData Sjl31protectWritten = new LongData(0);

	
	
	public boolean hasSubfile() {
		return false;
	}


	public Sjl31ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,new String[] {"01","20","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agncynumOut,new String[] {"02","21","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(regnumOut,new String[] {"04","23","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(srdateOut,new String[] {"05","24","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(endateOut,new String[] {"06","25","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ipdateOut,new String[] {"07","26","-07",null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(ppdateOut,new String[] {"08","27","-08",null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltnameOut,new String[] {"09","28","-09",null,null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntbrOut,new String[] { "10", "37", "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,new String[] { "91", "38", "-91", null, null, null, null, null, null, null, null, null });
		
		screenFields = new BaseData[] {clntsel,agncynum,regnum,srdate,endate,ipdate,ppdate,cltname,agntbr,agbrdesc,aracde,aradesc };
		screenOutFields = new BaseData[][] {clntselOut, agncynumOut, regnumOut, srdateOut,endateOut, ipdateOut, ppdateOut,cltnameOut,agntbrOut,agbrdescOut,aracdeOut,aradescOut };
		screenErrFields = new BaseData[] {clntselErr,agncynumErr, regnumErr, srdateErr, endateErr, ipdateErr, ppdateErr,cltnameErr,agntbrErr,agbrdescErr,aracdeErr,aradescErr  };
		
		screenDateFields = new BaseData[] {srdate, endate, ipdate,ppdate};
		screenDateErrFields = new BaseData[] {srdateErr, endateErr, ipdateErr,ppdateErr};
		screenDateDispFields = new BaseData[] {srdateDisp, endateDisp, ipdateDisp,ppdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl31screen.class;
		protectRecord = Sjl31protect.class;
	}
}