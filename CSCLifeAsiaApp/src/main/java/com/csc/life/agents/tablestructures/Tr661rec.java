package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR661REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr661rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr661Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData startDate = new ZonedDecimalData(8, 0).isAPartOf(tr661Rec, 0);
  	public FixedLengthStringData zbnwcoy = new FixedLengthStringData(3).isAPartOf(tr661Rec, 8);
  	public FixedLengthStringData zstate = new FixedLengthStringData(2).isAPartOf(tr661Rec, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(487).isAPartOf(tr661Rec, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr661Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr661Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}