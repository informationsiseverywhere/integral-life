package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5692
 * @version 1.0 generated on 30/08/09 06:49
 * @author Quipoz
 */
public class S5692ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(568);
	public FixedLengthStringData dataFields = new FixedLengthStringData(152).isAPartOf(dataArea, 0);
	public FixedLengthStringData ages = new FixedLengthStringData(12).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] age = ZDArrayPartOfStructure(4, 3, 0, ages, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(ages, 0, FILLER_REDEFINE);
	public ZonedDecimalData age01 = DD.age.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData age02 = DD.age.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData age03 = DD.age.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData age04 = DD.age.copyToZonedDecimal().isAPartOf(filler,9);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData incmrates = new FixedLengthStringData(20).isAPartOf(dataFields, 13);
	public ZonedDecimalData[] incmrate = ZDArrayPartOfStructure(4, 5, 2, incmrates, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(incmrates, 0, FILLER_REDEFINE);
	public ZonedDecimalData incmrate01 = DD.incmrate.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData incmrate02 = DD.incmrate.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData incmrate03 = DD.incmrate.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData incmrate04 = DD.incmrate.copyToZonedDecimal().isAPartOf(filler1,15);
	public FixedLengthStringData inprempcs = new FixedLengthStringData(20).isAPartOf(dataFields, 33);
	public ZonedDecimalData[] inprempc = ZDArrayPartOfStructure(4, 5, 2, inprempcs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(inprempcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData inprempc01 = DD.inprempc.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData inprempc02 = DD.inprempc.copyToZonedDecimal().isAPartOf(filler2,5);
	public ZonedDecimalData inprempc03 = DD.inprempc.copyToZonedDecimal().isAPartOf(filler2,10);
	public ZonedDecimalData inprempc04 = DD.inprempc.copyToZonedDecimal().isAPartOf(filler2,15);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,53);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,61);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,69);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData rwcmrates = new FixedLengthStringData(20).isAPartOf(dataFields, 107);
	public ZonedDecimalData[] rwcmrate = ZDArrayPartOfStructure(4, 5, 2, rwcmrates, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(rwcmrates, 0, FILLER_REDEFINE);
	public ZonedDecimalData rwcmrate01 = DD.rwcmrate.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData rwcmrate02 = DD.rwcmrate.copyToZonedDecimal().isAPartOf(filler3,5);
	public ZonedDecimalData rwcmrate03 = DD.rwcmrate.copyToZonedDecimal().isAPartOf(filler3,10);
	public ZonedDecimalData rwcmrate04 = DD.rwcmrate.copyToZonedDecimal().isAPartOf(filler3,15);
	public FixedLengthStringData reprempcs = new FixedLengthStringData(20).isAPartOf(dataFields, 127);
	public ZonedDecimalData[] reprempc = ZDArrayPartOfStructure(4, 5, 2, reprempcs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(reprempcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData reprempc01 = DD.rwprempc.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData reprempc02 = DD.rwprempc.copyToZonedDecimal().isAPartOf(filler4,5);
	public ZonedDecimalData reprempc03 = DD.rwprempc.copyToZonedDecimal().isAPartOf(filler4,10);
	public ZonedDecimalData reprempc04 = DD.rwprempc.copyToZonedDecimal().isAPartOf(filler4,15);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,147);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 152);
	public FixedLengthStringData agesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] ageErr = FLSArrayPartOfStructure(4, 4, agesErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(16).isAPartOf(agesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData age01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData age02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData age03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData age04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData incmratesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] incmrateErr = FLSArrayPartOfStructure(4, 4, incmratesErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(16).isAPartOf(incmratesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData incmrate01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData incmrate02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData incmrate03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData incmrate04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData inprempcsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] inprempcErr = FLSArrayPartOfStructure(4, 4, inprempcsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(16).isAPartOf(inprempcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData inprempc01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData inprempc02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData inprempc03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData inprempc04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData rwcmratesErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] rwcmrateErr = FLSArrayPartOfStructure(4, 4, rwcmratesErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(16).isAPartOf(rwcmratesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rwcmrate01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData rwcmrate02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData rwcmrate03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData rwcmrate04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData rwprempcsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData[] rwprempcErr = FLSArrayPartOfStructure(4, 4, rwprempcsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(16).isAPartOf(rwprempcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rwprempc01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData rwprempc02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData rwprempc03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData rwprempc04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 256);
	public FixedLengthStringData agesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] ageOut = FLSArrayPartOfStructure(4, 12, agesOut, 0);
	public FixedLengthStringData[][] ageO = FLSDArrayPartOfArrayStructure(12, 1, ageOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(48).isAPartOf(agesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] age01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] age02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] age03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] age04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData incmratesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] incmrateOut = FLSArrayPartOfStructure(4, 12, incmratesOut, 0);
	public FixedLengthStringData[][] incmrateO = FLSDArrayPartOfArrayStructure(12, 1, incmrateOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(48).isAPartOf(incmratesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] incmrate01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] incmrate02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] incmrate03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] incmrate04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData inprempcsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] inprempcOut = FLSArrayPartOfStructure(4, 12, inprempcsOut, 0);
	public FixedLengthStringData[][] inprempcO = FLSDArrayPartOfArrayStructure(12, 1, inprempcOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(48).isAPartOf(inprempcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] inprempc01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] inprempc02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] inprempc03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] inprempc04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData rwcmratesOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] rwcmrateOut = FLSArrayPartOfStructure(4, 12, rwcmratesOut, 0);
	public FixedLengthStringData[][] rwcmrateO = FLSDArrayPartOfArrayStructure(12, 1, rwcmrateOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(48).isAPartOf(rwcmratesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rwcmrate01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] rwcmrate02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] rwcmrate03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] rwcmrate04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData rwprempcsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 252);
	public FixedLengthStringData[] rwprempcOut = FLSArrayPartOfStructure(4, 12, rwprempcsOut, 0);
	public FixedLengthStringData[][] rwprempcO = FLSDArrayPartOfArrayStructure(12, 1, rwprempcOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(48).isAPartOf(rwprempcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rwprempc01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] rwprempc02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] rwprempc03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] rwprempc04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5692screenWritten = new LongData(0);
	public LongData S5692protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5692ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, age01, age03, age02, age04, incmrate01, inprempc01, incmrate02, inprempc02, incmrate04, inprempc04, rwcmrate02, reprempc02, rwcmrate04, reprempc04, rwcmrate01, reprempc01, incmrate03, inprempc03, rwcmrate03, reprempc03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, age01Out, age03Out, age02Out, age04Out, incmrate01Out, inprempc01Out, incmrate02Out, inprempc02Out, incmrate04Out, inprempc04Out, rwcmrate02Out, rwprempc02Out, rwcmrate04Out, rwprempc04Out, rwcmrate01Out, rwprempc01Out, incmrate03Out, inprempc03Out, rwcmrate03Out, rwprempc03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, age01Err, age03Err, age02Err, age04Err, incmrate01Err, inprempc01Err, incmrate02Err, inprempc02Err, incmrate04Err, inprempc04Err, rwcmrate02Err, rwprempc02Err, rwcmrate04Err, rwprempc04Err, rwcmrate01Err, rwprempc01Err, incmrate03Err, inprempc03Err, rwcmrate03Err, rwprempc03Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5692screen.class;
		protectRecord = S5692protect.class;
	}

}
