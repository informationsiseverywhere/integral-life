package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SJL75
 * @version 1.0 generated on 30/08/09 07:14
 * @author Quipoz
 */
public class Sjl75ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData clntname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltyp = DD.saleveltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldesc = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,196);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
			.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData clntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,
			getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(207);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(92).isAPartOf(subfileArea, 0);
	public ZonedDecimalData select = DD.select.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData agncynum = DD.agncynum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData regclass = DD.brnch.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(subfileFields,13);
	public FixedLengthStringData regnum = DD.regnum.copy().isAPartOf(subfileFields,60);
	public ZonedDecimalData startDate = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,76);
	public ZonedDecimalData dateend = DD.enddate.copyToZonedDecimal().isAPartOf(subfileFields,84);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 92);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData agncynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData regclassErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData enddateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 120);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] agncynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] regclassOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] enddateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 204);
	/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData startDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateendDisp = new FixedLengthStringData(10);

	public LongData Sjl75screensflWritten = new LongData(0);
	public LongData Sjl75screenctlWritten = new LongData(0);
	public LongData Sjl75screenWritten = new LongData(0);
	public LongData Sjl75windowWritten = new LongData(0);
	public LongData Sjl75hideWritten = new LongData(0);
	public LongData Sjl75protectWritten = new LongData(0);
	public GeneralTable Sjl75screensfl = new GeneralTable(AppVars.getInstance());

	@Override
	public boolean hasSubfile() {
	return true;
}

	public Sjl75ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {

		screenSflFields = new BaseData[] {select, agncynum, regclass, cltname, regnum, startDate, dateend};
		screenSflOutFields = new BaseData[][] {selectOut, agncynumOut, regclassOut, cltnameOut, regnumOut,
			srdateOut, srdateOut, enddateOut};
		screenSflErrFields = new BaseData[] {selectErr, agncynumErr, regclassErr, cltnameErr, regnumErr, srdateErr, enddateErr};
		screenSflDateFields = new BaseData[] {startDate, dateend};
		screenSflDateErrFields = new BaseData[] {srdateErr, enddateErr};
		screenSflDateDispFields = new BaseData[] {startDateDisp, dateendDisp};

		
		screenFields = new BaseData[] {clntsel,clntname,company,agntbr,agbrdesc,aracde,aradesc,levelno,leveltyp,leveldesc,saledept,saledptdes,indxflg};
		screenOutFields = new BaseData[][] {clntselOut,clntnameOut,companyOut,agntbrOut,agbrdescOut,aracdeOut,aradescOut,levelnoOut,leveltypOut,leveldescOut,saledeptOut,saledptdesOut,indxflgOut};
		screenErrFields = new BaseData[] {clntselErr,clntnameErr,companyErr,agntbrErr,agbrdescErr,aracdeErr,aradescErr,levelnoErr,leveltypErr,leveldescErr,saledeptErr,saledptdesErr,indxflgErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl75screen.class;
		screenSflRecord = Sjl75screensfl.class;
		screenCtlRecord = Sjl75screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl75protect.class;
	}
	
	public int getDataAreaSize() {
		return 405;
	}

	public int getDataFieldsSize() {
		return 197;
	}

	public int getErrorIndicatorSize() {
		return 52;
	}

	public int getOutputFieldSize() {
		return 156;
	}
	
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl75screenctl.lrec.pageSubfile);
	}
	
	
	
}
