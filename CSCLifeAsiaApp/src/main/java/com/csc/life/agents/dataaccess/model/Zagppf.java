package com.csc.life.agents.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Zagppf implements Serializable {

	private static final long serialVersionUID = 853929391484174378L;
	
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;	
	private int agntcoy;
	private String agntnum;
	private String agntype01;
	private String agntype02;
	private String datime;
	private String jobName;
	private byte mth;
	private Double prcnt01;
	private Double prcnt02;
	private String userProfile;
	private short year;
	
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public int getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(int i) {
		this.agntcoy = i;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getAgntype01() {
		return agntype01;
	}
	public void setAgntype01(String agntype01) {
		this.agntype01 = agntype01;
	}
	public String getAgntype02() {
		return agntype02;
	}
	public void setAgntype02(String agntype02) {
		this.agntype02 = agntype02;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public byte getMth() {
		return mth;
	}
	public void setMth(byte mth) {
		this.mth = mth;
	}
	public Double getPrcnt01() {
		return prcnt01;
	}
	public void setPrcnt01(Double prcnt01) {
		this.prcnt01 = prcnt01;
	}
	public Double getPrcnt02() {
		return prcnt02;
	}
	public void setPrcnt02(Double prcnt02) {
		this.prcnt02 = prcnt02;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public short getYear() {
		return year;
	}
	public void setYear(short year) {
		this.year = year;
	}
}
