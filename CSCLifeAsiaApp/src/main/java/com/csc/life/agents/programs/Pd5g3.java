package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.dao.AgslpfDAO;
import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.life.agents.screens.Sd5g3ScreenVars;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pd5g3 extends ScreenProgCS {

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PD5G3");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private String t3692 = "T3692";
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private Clntpf clntpf = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sd5g3ScreenVars sv = ScreenProgram.getScreenVars(Sd5g3ScreenVars.class);
	private Agslpf agslpf = new Agslpf(); 
	private AgslpfDAO agslpfDAO = getApplicationContext().getBean("agslpfDAO", AgslpfDAO.class);
	private List<Agslpf> agslpfList = new ArrayList<>();;
	private List<String> deleteList;
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private static final String M = "M";
	private static final String I = "I";
	private String e186 = "E186";

	public Pd5g3() {
		super();
		screenVars = sv;
		new ScreenModel("Sd5g3", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void plainname() {
		try {
			plain100();
		} catch (GOTOException e) {
		}
	}

	protected void plain100() {
		wsspcomn.longconfname.set(SPACES);
		if (clntpf.getClttype().equals("C")) {
			corpname();
			return;
		}
		if (!clntpf.getGivname().equals(SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(clntpf.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(clntpf.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		} else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
	}


	protected void corpname() {
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(clntpf.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(clntpf.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
	}

	protected void initialise1000() {
		
		initialise1010();
		retrvAglflnb1020();
		readrAgentFile1030();
		clientDets1040();
		agentType1060();
		subfileProcess1050();
	}

	
	protected void initialise1010() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	}

	protected void retrvAglflnb1020() {
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (!aglfIO.getStatuz().equals(varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (!aglfIO.getEffdate().equals(NUMERIC)) {
			aglfIO.setEffdate(varcom.vrcmMaxDate);
		}
	}

	protected void readrAgentFile1030() {
		sv.agnum.set(aglfIO.getAgntnum());
		agntlagIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agntlagIO);
		if (!agntlagIO.getStatuz().equals(varcom.oK) && !agntlagIO.getStatuz().equals(varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

	protected void clientDets1040() {
		sv.clntsel.set(agntlagIO.getClntnum());
		clntpf.setClntnum(agntlagIO.getClntnum().toString());
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(wsspcomn.fsuco.toString());
		if(agntlagIO.getClntnum().toString()!=null){
			clntpf = clntpfDAO.selectActiveClient(clntpf);

			if(clntpf!=null){
				plainname();
				sv.cltname.set(wsspcomn.longconfname);
			}
		}
	}

	protected void agentType1060() {
		sv.agtype.set(agntlagIO.getAgtype());
		descpf = descDAO.getdescData("IT", t3692, agntlagIO.getAgtype().toString(), wsspcomn.company.toString(),
				wsspcomn.language.toString());/* IJTI-1523 */

		if (descpf==null) {
			fatalError600();
		}
		sv.agtydesc.set(descpf.getLongdesc());
	}

	protected void subfileProcess1050() {
		scrnparams.subfileRrn.set(1);
		createSubfile();
	}

	protected void createSubfile() { // Add subfile on screen
		sv.initialiseSubfileArea();
		clearsubfile();//call sclr
		sv.subfileFields.set(SPACES);
		if(!wsspcomn.flag.equals(M) && !wsspcomn.flag.equals(I)){
			for(int i = 1; i <= 9; i++ ) {
				addsubfile();//subfile add row
				clearAgencySaleLic();
			}
		}
		if (!scrnparams.statuz.equals(varcom.oK) && !scrnparams.statuz.equals(Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
		
		if(wsspcomn.flag.equals(M) || wsspcomn.flag.equals(I)){
			loadSubfileByAgent();	
		}else if(agslpfList.size()>0){
			
			addprvsrecord();
		}
	}

	protected void preScreenEdit() {
		try {
			preStart();
		} catch (GOTOException e) {
		}
	}

	protected void preStart() {
		if (wsspcomn.flag.equals("I") || wsspcomn.flag.equals("T") || wsspcomn.flag.equals("R")) {
			scrnparams.function.set(varcom.prot);
		}
		/* CHECK-FOR-ERRORS */
		if (!sv.errorIndicators.equals(SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/* VALIDATE-SUBFILE */
		return;
	}

	protected void screenEdit2000() {
		wsspcomn.edterror.set(Varcom.oK);
		if (scrnparams.statuz.equals("CALC")) {
			wsspcomn.edterror.set("Y");
		}

		/* VALIDATE-SCREEN */
		scrnparams.function.set(Varcom.srnch);
		processScreen("SD5G3", sv);
		if (!scrnparams.statuz.equals(Varcom.oK) && !scrnparams.statuz.equals(Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		while (!scrnparams.statuz.equals(varcom.endp)) {
			validateSubfile2100();
		}

	}

	protected void validateSubfile2100() {
		
		updateErrorIndicators2170();
		readNextModifiedRecord2180();
	}

	protected void updateErrorIndicators2170() {

		if (!sv.errorSubfile.equals(SPACES)) {
			wsspcomn.edterror.set("Y"); 
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SD5G3", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	protected void readNextModifiedRecord2180(){
		scrnparams.function.set(varcom.srnch);
		processScreen("SD5G3", sv);
		if(!scrnparams.statuz.equals(varcom.oK) &&  !scrnparams.statuz.equals(varcom.endp)){
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}


	protected void update3000() {
		try {
			updateDatabase3010();
		} catch (GOTOException e) {
		}
	}

	/**
	 * Insert Agency Sale Licnese Records
	 */
	protected void updateDatabase3010() {

		startSflUpdate();
		deleteList=new ArrayList<String>();
		
		if(!wsspcomn.flag.equals(I)){ 
			
			if(agslpfList !=null && agslpfList.size() >0){
				for(Agslpf agslpf1 :agslpfList){
					deleteList.add(agslpf1.getAgnum());
				}
				agslpfDAO.deleteAgslpfRecord(deleteList);
			}
			
			if(agslpfList.size() == 0){
				deleteList.add(sv.agnum.toString());
				agslpfDAO.deleteAgslpfRecord(deleteList);
			}
			agslpfDAO.insertAgslpfRecord(agslpfList);
		}
	}

	protected void  startSflUpdate(){

		wsaaCount.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SD5G3", sv);
		agslpfList.clear();
		while (!scrnparams.statuz.equals(varcom.endp)) {
			agslpf = new Agslpf();
			if(!sv.tlaglicno.equals(SPACES)){
				checkSaleType();
				agslpf.setClntsel(sv.clntsel.trim());
				agslpf.setAgnum(sv.agnum.trim());
				agslpf.setAgtype(sv.agtype.trim());
				agslpf.setSalelicensetype(sv.salelicensetype.trim());
				agslpf.setTlaglicno(sv.tlaglicno.trim());
				agslpf.setFrmdate(sv.frmdate.toInt());
				agslpf.setTodate(sv.todate.toInt());
				agslpfList.add(agslpf);	
			}

			readNextModifiedRecord();
		}

	}

	protected void validation3110() {
		
	}

	protected void readNextModifiedRecord() {
		scrnparams.function.set(varcom.srdn);
		processScreen("SD5G3", sv);
		if (!scrnparams.statuz.equals(varcom.oK.toString()) && !scrnparams.statuz.equals(varcom.endp.toString())) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* EXIT */
	}

	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.programPtr.add(1);
		/* EXIT */
	}
	protected void loadSubfileByAgent(){
		List<Agslpf> list = agslpfDAO.getAgslpfByAgentNum(aglfIO.getAgntnum().toString());

		if(list.size() > 0){
			for(Agslpf agslpf : list){
				sv.agtype.set(agslpf.getAgtype());
				sv.salelicensetype.set(agslpf.getSalelicensetype());
				sv.tlaglicno.set(agslpf.getTlaglicno());
				sv.frmdate.set(agslpf.getFrmdate());
				sv.todate.set(agslpf.getTodate());
				addsubfile();//add subfile row
			}
		}
		clearAgencySaleLic();
		scrnparams.function.set(Varcom.supd); 
		processScreen("SD5G3", sv);
		for(int i=1; i<=9-list.size(); i++){
			addsubfile();//add subfile row
		}
	}
	protected void checkSaleType(){
		if(!sv.tlaglicno.equals(SPACES)){
			if(sv.salelicensetype.equals(SPACES)){
				sv.salelicensetypeErr.set(SPACES);
				sv.salelicensetypeErr.set(e186);
				wsspcomn.edterror.set("Y");
			}
			updateErrorIndicators2170();
		}
	}
	protected void clearAgencySaleLic(){
		sv.salelicensetype.clear();
		sv.tlaglicno.clear();
		sv.frmdate.clear();
		sv.frmdateDisp.clear();
		sv.todate.clear();
		sv.todateDisp.clear();
	}
	protected void clearsubfile(){
		scrnparams.function.set(Varcom.sclr);
		processScreen("SD5G3", sv);
		if (!scrnparams.statuz.equals(varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}		
	}
	protected void addsubfile(){
			scrnparams.function.set(Varcom.sadd); 
			processScreen("SD5G3", sv);	
	}
	protected void addprvsrecord(){
		clearsubfile();
		for(Agslpf agslpf : agslpfList){
		sv.agtype.set(agslpf.getAgtype());
		sv.salelicensetype.set(agslpf.getSalelicensetype());
		sv.tlaglicno.set(agslpf.getTlaglicno());
		sv.frmdate.set(agslpf.getFrmdate());
		sv.todate.set(agslpf.getTodate());
		addsubfile();
	}
		clearAgencySaleLic();
		for(int i=1; i<=9-agslpfList.size(); i++){
			addsubfile();//add subfile row
		}
		
	}
}
	
