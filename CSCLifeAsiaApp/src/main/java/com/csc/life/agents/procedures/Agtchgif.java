/*
 * File: Agtchgif.java
 * Date: 29 August 2009 20:13:13
 * Author: Quipoz Limited
 * 
 * Class transformed from AGTCHGIF.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.dataaccess.AgcmchgTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglfchgTableDAM;
import com.csc.life.agents.dataaccess.PcddchgTableDAM;
import com.csc.life.agents.recordstructures.Agtchgrec;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  AGTCHGIF - Agent Change (In Force contracts)
*  --------------------------------------------
*
*  Overview
*  ~~~~~~~~
*
*  This  provides  the  facility  to  change  agent  commission
*  details from the old agent to a new agent.
*  The   function   of   this  subroutine  is  to  support  the
*  commission change for an  individual  contract  called  from
*  either  batch  processing or an online request. T6688  which
*  is keyed on contract status will determine when this
*  program is called, i.e. the IF status contracts will  invoke
*  this processing.
*  The  parameters  passed  to  this  program are set up in the
*  copybook AGTCHGREC.
*
*  A Batch has  been  opened  in  the  calling  program  so  no
*  creating  of  a  new  batch  is  required.  PTRN records are
*  written from the calling program also.
*
*  AGCMCHG
*  ~~~~~~~
*  Create a logical file AGCMCHG based  on  the  physical  file
*  AGCMPF  and  include  all  the fields as the AGCMMJA logical
*  with the same key order:-
*
*      CHDRCOY
*      CHDRNUM
*      LIFE
*      AGNTNUM
*
*  Linkage
*  ~~~~~~~
*  AGTCHGREC
*  ---------
*
*  AGCG-STATUZ
*  AGCG-CHDRCOY
*  AGCG-CHDRNUM
*  AGCG-LIFE
*  AGCG-COVERAGE
*  AGCG-RIDER
*  AGCG-PLAN-SUFFIX
*  AGCG-SEQNO
*  AGCG-TRANNO
*  AGCG-AGNTNUM-NEW
*  AGCG-AGNTNUM-OLD
*  AGCG-SERV-COMM-FLAG
*  AGCG-INIT-COMM-FLAG
*  AGCG-RNWL-COMM-FLAG
*  AGCG-BATCKEY
*  AGCG-TERMID
*  AGCG-USER
*  AGCG-TRANSACTION-DATE
*  AGCG-TRANSACTION-TIME
*
*  Main-Processing Section.
*  ~~~~~~~~~~~~~~~~~~~~~~~~
*
*          Initialise.
*
*          Read CHDR for the AGCG-CHDRNUM.
*
*          The AGCM's are processed twice to avoid file
*          problems with non-unique keys.
*
*          If AGCG-Init-Comm-Flag = 'Y'
*             Perform Clawback-Commission
*                Until AGCMCHG-STATUZ = ENDP.
*
*          Perform Process-AGCM
*                Until AGCMCHG-STATUZ = ENDP.
*
*  Clawback-Commission Section.
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  Use BEGNH to read the AGCM's so that  all  records  are  not
*  required  to be re-read before update. This is necessary due
*  to the AGCM's having non-unique keys.
*
*          Set AGCMCHG-PARAMS   to spaces.
*          Set AGCMCHG-CHDRCOY  to AGCG-CHDRCOY.
*          Set AGCMCHG-CHDRNUM  to AGCG-CHDRNUM.
*          Set AGCMCHG-AGNTNUM  to spaces.
*          Set AGCMCHG-FUNCTION to 'BEGNH '
*
*          Call 'AGCMCHGIO' to read the record.
*
*          Perform normal error handling.
*
*            If AGCMCHG-STATUZ = ENDP
*            Or Next contract found
*               Move ENDP go to exit
*            Else
*              Continue.
*
*            Set WSAA-JRNSEQ to 0.
*
*            If AGCMCHG-COMPAY = AGCMCHG-COMERN
*               Go To Rewrt-Para.
*
*            If AGCMCHG-AGNTNUM NOT = AGCG-AGNTNUM-OLD
*               Move 'N' to WSAA-IF-OVERRIDE-AG
*               Perform Find-Override
*               If WSAA-IF-OVERRIDE-AGE = 'Y'
*                  Perform Post-Comm-Clawback
*               Else
*                  Go To Rewrt-Para.
*
*       Rewrt-Para.
*
*            This paragraph must be performed for each AGCM
*            found (whether a relevant record or not) so as to
*            release the held status.
*
*            Set AGCMCHG-COMPAY to AGCMCHG-COMERN.
*
*            Rewrt this AGCM record.
*
*            Set AGCMCHG-FUNCTION to 'NEXTR'.
*
*            Exit this section.
*
*  Process-AGCM Section
*  ~~~~~~~~~~~~~~~~~~~~~
*  Use BEGNH to read the AGCM's so that  all  records  are  not
*  required  to be re-read before update. This is necessary due
*  to the AGCM's having non-unique keys.
*
*          Set AGCMCHG-PARAMS   to spaces.
*          Set AGCMCHG-CHDRCOY  to AGCG-CHDRCOY.
*          Set AGCMCHG-CHDRNUM  to AGCG-CHDRNUM.
*          Set AGCMCHG-AGNTNUM  to spaces.
*          Set AGCMCHG-FUNCTION to 'BEGNH '
*
*          Call 'AGCMCHGIO' to read the record.
*
*          Perform normal error handling.
*
*            If AGCMCHG-STATUZ = ENDP
*            Or Next contract found
*               Move ENDP go to exit
*            Else
*              Continue.
*
*       At this point any clawback has been done and the
*       commission paid is equal to the commission earned.
*
*            If AGCMCHG-AGNTNUM   NOT = AGCG-AGNTNUM-OLD
*               Move 'N'          to  WSAA-IF-OVERRIDE-AG
*               Perform Find-Override
*               If WSAA-IF-OVERRIDE-AG = 'N'
*                  Go to Rewrt-Para.
*
*       If the transaction numbers are equal, then the NEXTR
*       has picked up an AGCM already written by this program.
*       Go and get the next record in this case.
*
*            If AGCMCHG-TRANNO     = WSAA-TRANNO
*               Go To REWRT-PARA.
*
*       Override agents can NOT be selected directly.
*
*            If AGCMCHG-OVRDCAT    = 'O'
*            AND AGCMCHG-AGNTNUM   = AGCG-OLD-AGNTNUM
*               Go To REWRT-PARA.
*
*            Perform Write-new-AGCM.
*            Set AGCMCHG-FUNCTION to 'NEXTR'.
*            Go To Exit.
*
*       Rewrt-Para.
*
*            This paragraph must be performed for each AGCM
*            found (whether a relevant record or not) so as to
*            release the held status.
*
*            Rewrt this AGCM record.
*
*            Set AGCMCHG-FUNCTION to 'NEXTR'.
*
*            Exit this section.
*
*  Find-Override Section
*  ~~~~~~~~~~~~~~~~~~~~~
*
*  We need to determine whether this AGCM can  be  clawed  back
*  if it is an override agent.
*
*            Set AGLF-PARAMS   to Spaces.
*            Set AGLF-CHDRCOY  to AGCG-CHDRCOY.
*            Set AGLF-AGNTNUM  to AGCG-OLD-AGNTNUM.
*            Set AGLF-FUNCTION to 'READR'.
*
*       FIND-PARA.
*
*            Call 'AGLFIO' Using AGLF-PARAMS.
*
*            Perform normal error handling.
*
*            If AGLF-REPORTAG   = Spaces
*               Move 'N'       to WSAA-IF-OVERRIDE-AG
*               Go To Exit.
*
*            If AGLF-REPORTAG   = AGCMCHG-AGNTNUM
*               Move 'Y'       to WSAA-IF-OVERRIDE-AG
*               Go To Exit.
*
*            Move AGLF-REPORTAG  to  AGLF-AGNTNUM.
*            GO TO FIND-PARA.
*
*            Exit this Section.
*
*  Post-Commission-Clawback Section
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*  This  section  will  post a negative amount to the old agent
*  if  commission   paid   exceeds   commission   earned.   The
*  processing  includes ALL AGCM's, this therefore includes any
*  override agents.
*
*  It will be necessary to read T5645 first for  the  component
*  to get the accounting entries required.
*  Call  'AGCMCHGIO' with function 'READH' to read and hold the
*  record.
*  Perform normal error handling.
*
*  Create  2  ACMV  records  for  each  transaction,  one   for
*  Commission  Clawback  for  agents,  and one for re-crediting
*  Agent Commission in Advance. The details for these  will  be
*  held  on  T5645,  accessed  by program Id. Line #1 will give
*  the details for the first posting for which  the  amount  on
*  the  ACMV will be negative and line #2 will give the details
*  for the second posting for which  the  amount  on  the  ACMV
*  will be positive.
*
*  The  subroutine  LIFACMV  will  be  called  to  add the ACMV
*  records.
*
*  ACMV-RDOCNUM          -   AGCM-CHDRNUM
*  ACMV-RLDGACCT         -   AGCM-AGNTNUM
*  ACMV-BATCCOY          -   from redefined AGCG-BATCKEY
*  ACMV-ORIGCURR         -   CHDR-CNTCURR
*  ACMV-TRANNO           -   CHDR-TRANNO
*  ACMV-JRNSEQ           -   WSAA-JRNSEQ + 1
*  ACMV-ORIGAMT          -   AGCM-COMERN - AGCM-COMPAY
*  ACMV-TRANREF          -   AGCM-CHDRNUM
*  ACMV-SACSTYP          -   From T5645
*  ACMV-SACSCODE         -   From T5645
*  ACMV-GLCODE           -   From T5645
*  ACMV-GLSIGN           -   From T5645
*  ACMV-CONTOT           -   From T5645
*  ACMV-POSTYEAR         -   from redefined AGCG-BATCKEY
*  ACMV-POSTMONTH        -   from redefined AGCG-BATCKEY
*  ACMV-EFFDATE          -   Todays Date
*  ACMV-TRANSACTION-DATE -   AGCG-TRANSACTION-DATE
*  ACMV-TRANSACTION-TIME -   AGCG-TRANSACTION-TIME
*  ACMV-USER             -   AGCG-USER
*  ACMV-TERMID           -   AGCG-TERMID
*
*  Write-new-AGCM Section.
*  ~~~~~~~~~~~~~~~~~~~~~~~
*  Firstly we must update the old agents AGCM record  with  the
*  new transaction information.
*
*  Store the incoming AGCM values for AGCM-VALIDFLAG.
*
*  Change the incoming AGCM record to have a VALIDFLAG of '2'.
*
*  REWRT this AGCM record.
*
*  Move  the  stored VALIDFLAG to the AGCMCHG-VALIDFLAG and set
*  the AGNTNUM to the AGCG-AGNTNUM-NEW.
*
*  It will depend  on  the  value  of  flags  passed  from  the
*  linkage   whether  you  have  to  split  the  AGCM  out.  If
*  AGCG-SERV-COMM-FLAG,         AGCG-RNWL-COMM-FLAG         and
*  AGCG-INIT-COMM-FLAG  are  'Y'  then a REWRT of the record is
*  required with the new agent number.
*
*  If a combination of 'Y' and 'N'  are  found  then  the  AGCM
*  will need to be split out.
*  E.g. Init = 'Y' Serv = 'N' Rnwl = 'N'
*  Before Change
*  AGCM AG No. #12345  Init = 100  Serv = 50  Rnwl = 25
*  After Change to Agent #54321
*  AGCM AG No. #12345 as above with Valid Flag '2'
*  AGCM Ag No. #12345  Init = 100  Serv = 0   Rnwl = 0
*  AGCM AG No. #54321  Init = 0    Serv = 50  Rnwl = 25
*
*            If AGCG-Init-comm-flag = 'Y'
*               Perform Write-new-PCDD.
*
*  The  new agent's details must be that of the new agent. This
*  requires then a read of the AGLF to  ascertain  new  methods
*  and  Agent Classification Category. The new values are to be
*  on  the  new  AGCM.  It  also  means  the  recalculation  of
*  commission  using these new values. This will be the same as
*  at Contract Issue but excluding Single Premium  calculations
*  as these will not be affected.
*
*  All  types  are  to  be  calculated - Renewal, Servicing and
*  Initial.
*
*  COVR's will need to be retrieved to get the  component  type
*  as  this  is  required  to  read  T5687  to  get the default
*  commission calculation and payment methods.  For component
*  rider, read TR695 to get commission calculation and payment
*  methods based on main coverage. If TR695 record is not found,
*  default commission calculation and payment methods from T5687
*  will then be used.
*
*  When  the  Initial  Commission  is  recalculated   any   new
*  override has to be calculated.
*
*            If AGLF-REPORTAG     NOT = Spaces
*               Perform NEW-OVERRIDE
*                 Until AGLF-REPORTAG = Spaces
*                 Or    AGLF-OVCPC    = Zero.
*
*  Write-new-PCDD Section.
*  ~~~~~~~~~~~~~~~~~~~~~~~
*  Call  'PCDDIO'  with  function  'READH' to read and hold the
*  record.
*  Perform normal error handling.
*
*  Change the  Valid  Flag  of  the  PCDD  to  '2'  and  update
*  transaction details.
*
*  REWRT this PCDD record.
*
*  Move  the  new  Agent  Number  to PCDD-AGNTNUM and set Valid
*  Flag to '1'.
*
*  WRITR this new PCDD record if one not found else REWRT.
*
*  New-Override Section.
*  ~~~~~~~~~~~~~~~~~~~~~
*
*  Write a new AGCM for each  override  agent  found.  Use  the
*  values  calculated  in the Initial Commission re-calculation
*  section multiplied by the Override percentage  to  work  out
*  this agent's commission.
*
*  Read the new AGLF-REPORTAG until equal to spaces.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allow only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various AT's
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced, form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTR's
* will be written. Also, if the premium has increased or
* decreased, a number of STTR's will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTR's is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTR's
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
*****************************************************************
* </pre>
*/
public class Agtchgif extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "AGTCHGIF";
	private PackedDecimalData wsaaCommClawback = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCommAdvance = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaValidflag = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaDormflag = new FixedLengthStringData(1);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaPtdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);
	private String wsaaIfOverrideAg = "";
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSplitBcomm = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSplitBpts = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSplitBcommSave = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSplitBptsSave = new PackedDecimalData(5, 2);

	private FixedLengthStringData wsaaCommTots = new FixedLengthStringData(27);
	private PackedDecimalData wsaaIcommtot = new PackedDecimalData(17, 2).isAPartOf(wsaaCommTots, 0);
	private PackedDecimalData wsaaPayamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaCommTots, 9);
	private PackedDecimalData wsaaErndamt = new PackedDecimalData(17, 2).isAPartOf(wsaaCommTots, 18);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private String wsaaPcddRewritten = "";
	private FixedLengthStringData wsaaStoredAgentClass = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(11, 5);
	private PackedDecimalData wsaaBillfreq = new PackedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
		/* WSAA-TRANSFER-FLAGS */
	private String wsaaTranInit = "";
	private String wsaaTranServ = "";
	private String wsaaTranRnwl = "";

	private FixedLengthStringData wsaaCommPatterns = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaBascpyStore = new FixedLengthStringData(4).isAPartOf(wsaaCommPatterns, 0);
	private FixedLengthStringData wsaaSrvcpyStore = new FixedLengthStringData(4).isAPartOf(wsaaCommPatterns, 4);
	private FixedLengthStringData wsaaRnwcpyStore = new FixedLengthStringData(4).isAPartOf(wsaaCommPatterns, 8);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaBasicCommMethod = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);

	private FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4);
	private Validator agentCommChange = new Validator(wsaaTrcde, "T521");
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String f294 = "F294";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5644 = "T5644";
	private static final String t5645 = "T5645";
	private static final String t5647 = "T5647";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String tr695 = "TR695";
	private static final String th605 = "TH605";
		/* FORMATS */
	private static final String agcmchgrec = "AGCMCHGREC";
	private static final String agcmrec = "AGCMREC";
	private static final String aglfrec = "AGLFREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String pcddchgrec = "PCDDCHGREC";
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
	private AgcmchgTableDAM agcmchgIO = new AgcmchgTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AglfchgTableDAM aglfchgIO = new AglfchgTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PcddchgTableDAM pcddchgIO = new PcddchgTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Syserrrec syserrrec = new Syserrrec();
	private T5644rec t5644rec = new T5644rec();
	private T5645rec t5645rec = new T5645rec();
	private T5647rec t5647rec = new T5647rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr695rec tr695rec = new Tr695rec();
	private Th605rec th605rec = new Th605rec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Varcom varcom = new Varcom();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Agtchgrec agtchgrec = new Agtchgrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		rewrt420, 
		exit490, 
		rewrt520, 
		nextr530, 
		exit590, 
		rewrtOldAgcm620, 
		newAgcm640, 
		exit690, 
		exit740, 
		exit4290, 
		writeAgcm5050, 
		checkNext5040, 
		initComm6020, 
		accumAmts6030
	}

	public Agtchgif() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		agtchgrec.agentRec = convertAndSetParam(agtchgrec.agentRec, parmArray, 0);
		try {
			startSubr100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr100()
	{
		/*PARA*/
		initialise150();
		readChdr200();
		readTh605350();
		/* Only want to clawback commission if the Initial Commission is*/
		/* to be transferred.*/
		if (isEQ(agtchgrec.initCommFlag, "Y")) {
			begnhAgcm300();
			while ( !(isEQ(agcmchgIO.getStatuz(), varcom.endp))) {
				clawbackCommission400();
			}
			
		}
		begnhAgcm300();
		while ( !(isEQ(agcmchgIO.getStatuz(), varcom.endp))) {
			processAllAgcms500();
		}
		
		a000Statistics();
		/*EXIT*/
		exitProgram();
	}

protected void initialise150()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaBatckey.set(agtchgrec.batckey);
		wsaaPcddRewritten = "N";
		wsaaSplitBcomm.set(ZERO);
		wsaaSplitBpts.set(ZERO);
		wsaaSplitBcommSave.set(ZERO);
		wsaaCommClawback.set(ZERO);
		wsaaCommAdvance.set(ZERO);
		wsaaTranno.set(ZERO);
		wsaaCommTots.set(ZERO);
		wsaaFreqFactor.set(ZERO);
		wsaaBillfreq.set(ZERO);
		wsaaSeqno.set(ZERO);
		wsaaValidflag.set(SPACES);
		wsaaDormflag.set(SPACES);
		wsaaCntcurr.set(SPACES);
		wsaaBasicCommMeth.set(SPACES);
		wsaaStoredAgentClass.set(SPACES);
		wsaaTranInit = "N";
		wsaaTranServ = "N";
		wsaaTranRnwl = "N";
		wsaaCommPatterns.set(SPACES);
		wsaaTrcde.set(wsaaBatckey.batcBatctrcde);
		/*EXIT*/
	}

protected void readChdr200()
	{
		para210();
	}

protected void para210()
	{
		/* Read the CHDR record and store any values needed*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(agtchgrec.chdrcoy);
		chdrlifIO.setChdrnum(agtchgrec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			fatalError9000();
		}
		wsaaCntcurr.set(chdrlifIO.getCntcurr());
		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
		callDatcon3250();
	}

protected void callDatcon3250()
	{
		go260();
	}

protected void go260()
	{
		if (isEQ(chdrlifIO.getBillfreq(), "00")) {
			wsaaFreqFactor.set(1);
			return ;
		}
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(chdrlifIO.getBtdate());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError9000();
		}
		wsaaFreqFactor.set(datcon3rec.freqFactor);
	}

protected void begnhAgcm300()
	{
		/*BEGNH-AGCM*/
		agcmchgIO.setParams(SPACES);
		agcmchgIO.setChdrcoy(agtchgrec.chdrcoy);
		agcmchgIO.setChdrnum(agtchgrec.chdrnum);
		agcmchgIO.setAgntnum(SPACES);
		agcmchgIO.setFormat(agcmchgrec);
		/* MOVE BEGNH                  TO AGCMCHG-FUNCTION.             */
		agcmchgIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void readTh605350()
	{
		start351();
	}

protected void start351()
	{
		/* Read TH605 to see whether system is "Override based on Agent    */
		/* Details"                                                        */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(agtchgrec.chdrcoy);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(agtchgrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void clawbackCommission400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para410();
				case rewrt420: 
					rewrt420();
				case exit490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para410()
	{
		/* Clawback commission*/
		SmartFileCode.execute(appVars, agcmchgIO);
		if ((isNE(agcmchgIO.getStatuz(), varcom.oK))
		&& (isNE(agcmchgIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(agcmchgIO.getStatuz(), varcom.endp))
		|| (isNE(agcmchgIO.getChdrcoy(), agtchgrec.chdrcoy))
		|| (isNE(agcmchgIO.getChdrnum(), agtchgrec.chdrnum))) {
			agcmchgIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit490);
		}
		wsaaJrnseq.set(ZERO);
		if (isEQ(agcmchgIO.getCompay(), agcmchgIO.getComern())) {
			goTo(GotoLabel.rewrt420);
		}
		/* Check T5688 to see if Component level accounting is*/
		/* required.*/
		readT56882500();
		/* This checks that if an AGCM is an override agent for the*/
		/* incoming changed agent, then clawback is necessary.*/
		/* (More than one AGCM record for an override agent can exist*/
		/*  if there are multiple agents for a contract, so we need to*/
		/*  be sure that we clawback for the correct overriding agent)*/
		if (isNE(agcmchgIO.getAgntnum(), agtchgrec.agntnumOld)) {
			wsaaIfOverrideAg = "N";
			findOverride4100();
			if (isEQ(wsaaIfOverrideAg, "N")) {
				goTo(GotoLabel.rewrt420);
			}
		}
		/*  COMPUTE WSAA-COMM-CLAWBACK  = AGCMCHG-COMERN -               */
		/*                                AGCMCHG-COMPAY.                */
		/* We want the Original Amount field on the ACMV as a positive   */
		/* Amount and the GLSIGN as a '+'. ie Debit.                     */
		compute(wsaaCommClawback, 2).set(sub(agcmchgIO.getCompay(), agcmchgIO.getComern()));
		wsaaCommAdvance.set(wsaaCommClawback);
		readT5645800();
		setupCommonAcmv900();
		postCommClawback1000();
		postAdvanceComm2000();
		agcmchgIO.setCompay(agcmchgIO.getComern());
	}

protected void rewrt420()
	{
		/* MOVE REWRT                  TO AGCMCHG-FUNCTION.             */
		agcmchgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmchgIO);
		if (isNE(agcmchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
		/*NEXTR*/
		agcmchgIO.setFunction(varcom.nextr);
	}

protected void processAllAgcms500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para510();
				case rewrt520: 
					rewrt520();
				case nextr530: 
					nextr530();
				case exit590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para510()
	{
		/* At this point any clawback has been done and the commission*/
		/* paid is now equal to the commission earned*/
		SmartFileCode.execute(appVars, agcmchgIO);
		if ((isNE(agcmchgIO.getStatuz(), varcom.oK))
		&& (isNE(agcmchgIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(agcmchgIO.getStatuz(), varcom.endp))
		|| (isNE(agcmchgIO.getChdrcoy(), agtchgrec.chdrcoy))
		|| (isNE(agcmchgIO.getChdrnum(), agtchgrec.chdrnum))) {
			agcmchgIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit590);
		}
		/* This checks that if an AGCM is an override agent, but not*/
		/* for the incoming changed agent - just perform a REWRT and*/
		/* get the next AGCM record*/
		if (isNE(agcmchgIO.getAgntnum(), agtchgrec.agntnumOld)) {
			wsaaIfOverrideAg = "N";
			findOverride4100();
			if (isEQ(wsaaIfOverrideAg, "N")) {
				goTo(GotoLabel.rewrt520);
			}
		}
		/* If the transaction numbers are equal, then the NEXTR has*/
		/* picked up an AGCM already written by this program - REWRT the*/
		/* record to release the HOLD and get the next record.*/
		if (isEQ(agcmchgIO.getTranno(), wsaaTranno)) {
			goTo(GotoLabel.rewrt520);
		}
		/* For OVERRIDE cases no Agent Commission change is necessary*/
		/* directly, i.e.if you select an agent who is the override agent t*/
		/* apply no change. If however, you select an agent who has an over*/
		/* agent apply changes as necessary.*/
		if ((isEQ(agcmchgIO.getOvrdcat(), "O"))
		&& (isEQ(agcmchgIO.getAgntnum(), agtchgrec.agntnumOld))) {
			goTo(GotoLabel.rewrt520);
		}
		processAgcmRecord600();
		goTo(GotoLabel.nextr530);
	}

protected void rewrt520()
	{
		/* MOVE REWRT                  TO AGCMCHG-FUNCTION.             */
		agcmchgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmchgIO);
		if (isNE(agcmchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
	}

protected void nextr530()
	{
		agcmchgIO.setFunction(varcom.nextr);
	}

protected void processAgcmRecord600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					go605();
					checkCommFlags610();
				case rewrtOldAgcm620: 
					rewrtOldAgcm620();
					writeAgcm630();
				case newAgcm640: 
					newAgcm640();
				case exit690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go605()
	{
		/* Store the payment patterns*/
		wsaaBascpyStore.set(agcmchgIO.getBascpy());
		wsaaSrvcpyStore.set(agcmchgIO.getSrvcpy());
		wsaaRnwcpyStore.set(agcmchgIO.getRnwcpy());
	}

protected void checkCommFlags610()
	{
		/* If we have to transfer initial commission and the AGCM*/
		/* record we have is an overriding agent (for the changed agent),*/
		/* go to REWRT the record*/
		/* If, however, we have an overriding agent but initial commission*/
		/* is not to be transferred - go to exit.*/
		if ((isEQ(agcmchgIO.getOvrdcat(), "O"))) {
			if ((isEQ(agtchgrec.initCommFlag, "Y"))) {
				goTo(GotoLabel.rewrtOldAgcm620);
			}
			else {
				goTo(GotoLabel.exit690);
			}
		}
		/* If any of the commission flags are set to 'Y'*/
		/* (and there is a commission payment method), move*/
		/* spaces to the relevant commission payment method and*/
		/* set a flag to indicate that this commission is to be*/
		/* transferred.*/
		/* The blank payment method is written to the AGCM record*/
		/* for the Old Agent and tells us that this commission*/
		/* has been transferred.*/
		if ((isEQ(agtchgrec.initCommFlag, "Y"))
		&& (isNE(agcmchgIO.getBascpy(), SPACES))) {
			writeNewPcdd700();
			wsaaBascpyStore.set(SPACES);
			wsaaTranInit = "Y";
		}
		else {
			wsaaTranInit = "N";
		}
		if ((isEQ(agtchgrec.servCommFlag, "Y"))
		&& (isNE(agcmchgIO.getSrvcpy(), SPACES))) {
			wsaaSrvcpyStore.set(SPACES);
			wsaaTranServ = "Y";
		}
		else {
			wsaaTranServ = "N";
		}
		if ((isEQ(agtchgrec.rnwlCommFlag, "Y"))
		&& (isNE(agcmchgIO.getRnwcpy(), SPACES))) {
			wsaaRnwcpyStore.set(SPACES);
			wsaaTranRnwl = "Y";
		}
		else {
			wsaaTranRnwl = "N";
		}
		/* Only go ahead if there is at least one type of commission*/
		/* to transfer*/
		if ((isEQ(wsaaTranInit, "Y"))
		|| (isEQ(wsaaTranServ, "Y"))
		|| (isEQ(wsaaTranRnwl, "Y"))) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit690);
		}
		/* We now have the record we want, so store the dormant and*/
		/* valid flags - to ensure that when we write out an AGCM*/
		/* record, it will be in the same state as the original*/
		wsaaValidflag.set(agcmchgIO.getValidflag());
		wsaaDormflag.set(agcmchgIO.getDormantFlag());
		wsaaSeqno.set(agcmchgIO.getSeqno());
		wsaaPtdate.set(agcmchgIO.getPtdate());
		wsaaAgntnum.set(agcmchgIO.getAgntnum());
	}

protected void rewrtOldAgcm620()
	{
		agcmchgIO.setValidflag("2");
		/*MOVE 'Y'                    TO AGCMCHG-DORMANT-FLAG.         */
		agcmchgIO.setCurrto(agtchgrec.transactionDate);
		agcmchgIO.setTransactionDate(agtchgrec.transactionDate);
		/*MOVE VRCM-MAX-DATE          TO AGCMCHG-CURRFROM.             */
		agcmchgIO.setTransactionTime(agtchgrec.transactionTime);
		/* MOVE REWRT                  TO AGCMCHG-FUNCTION.             */
		agcmchgIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmchgIO);
		if (isNE(agcmchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
		/* If the AGCM is an override agent we do not want to do anything*/
		/* else, just go to EXIT*/
		if (isEQ(agcmchgIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.exit690);
		}
		/* If all the commission is to be transferred, do not write a*/
		/* record for the old agent*/
		if ((isEQ(agtchgrec.initCommFlag, "Y"))
		&& (isEQ(agtchgrec.rnwlCommFlag, "Y"))
		&& (isEQ(agtchgrec.servCommFlag, "Y"))) {
			goTo(GotoLabel.newAgcm640);
		}
	}

protected void writeAgcm630()
	{
		/* If all the commission has been transferred, we do not want to*/
		/* write a record for the old agent*/
		if ((isEQ(wsaaBascpyStore, SPACES))
		&& (isEQ(wsaaSrvcpyStore, SPACES))
		&& (isEQ(wsaaRnwcpyStore, SPACES))) {
			goTo(GotoLabel.newAgcm640);
		}
		agcmchgIO.setBascpy(wsaaBascpyStore);
		agcmchgIO.setSrvcpy(wsaaSrvcpyStore);
		agcmchgIO.setRnwcpy(wsaaRnwcpyStore);
		agcmchgIO.setCurrfrom(agtchgrec.transactionDate);
		agcmchgIO.setCurrto(varcom.vrcmMaxDate);
		agcmchgIO.setValidflag(wsaaValidflag);
		agcmchgIO.setDormantFlag(wsaaDormflag);
		agcmchgIO.setSeqno(wsaaSeqno);
		agcmchgIO.setPtdate(wsaaPtdate);
		agcmchgIO.setTranno(wsaaTranno);
		agcmchgIO.setAgntnum(wsaaAgntnum);
		agcmchgIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmchgIO);
		if (isNE(agcmchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
	}

protected void newAgcm640()
	{
		/* Write AGCM record for the new agent*/
		agcmForNewAgent3000();
	}

protected void writeNewPcdd700()
	{
		try {
			check705();
			readhOld710();
			rewrtOld715();
			readhNew720();
			writrNew730();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void check705()
	{
		/* Because we loop around through all the AGCM's for the old agent,*/
		/* the first time through we will rewrite the PCDD record for the*/
		/* old agent - which effectively removes it from the PCDDCHG logica*/
		/* view.*/
		/* Therefore, if the PCDD has already been rewritten - Go To Exit.*/
		if (isEQ(wsaaPcddRewritten, "Y")) {
			goTo(GotoLabel.exit740);
		}
	}

protected void readhOld710()
	{
		pcddchgIO.setParams(SPACES);
		pcddchgIO.setChdrcoy(agtchgrec.chdrcoy);
		pcddchgIO.setChdrnum(agtchgrec.chdrnum);
		pcddchgIO.setAgntnum(agtchgrec.agntnumOld);
		pcddchgIO.setFormat(pcddchgrec);
		pcddchgIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcddchgIO);
		if ((isNE(pcddchgIO.getStatuz(), varcom.oK))
		&& (isNE(pcddchgIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			fatalError9000();
		}
		if ((isEQ(pcddchgIO.getStatuz(), varcom.mrnf))) {
			goTo(GotoLabel.exit740);
		}
		wsaaSplitBcomm.set(pcddchgIO.getSplitBcomm());
		wsaaSplitBpts.set(pcddchgIO.getSplitBpts());
	}

protected void rewrtOld715()
	{
		pcddchgIO.setTransactionDate(agtchgrec.transactionDate);
		pcddchgIO.setCurrto(agtchgrec.transactionDate);
		pcddchgIO.setTransactionTime(agtchgrec.transactionTime);
		pcddchgIO.setCurrfrom(varcom.vrcmMaxDate);
		pcddchgIO.setValidflag("2");
		pcddchgIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, pcddchgIO);
		if (isNE(pcddchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			fatalError9000();
		}
		wsaaPcddRewritten = "Y";
	}

protected void readhNew720()
	{
		/* The New Agent may already have a split for this contract.*/
		/* If this is the case, just rewrite the PCDD record for the*/
		/* new agent - updating the commission split and bonus points*/
		/* values.*/
		/* Otherwise, just write out a new record for the New Agent.*/
		pcddchgIO.setChdrcoy(agtchgrec.chdrcoy);
		pcddchgIO.setChdrnum(agtchgrec.chdrnum);
		pcddchgIO.setAgntnum(agtchgrec.agntnumNew);
		pcddchgIO.setFormat(pcddchgrec);
		pcddchgIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, pcddchgIO);
		if ((isNE(pcddchgIO.getStatuz(), varcom.oK))
		&& (isNE(pcddchgIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			fatalError9000();
		}
		pcddchgIO.setTransactionDate(agtchgrec.transactionDate);
		pcddchgIO.setCurrfrom(agtchgrec.transactionDate);
		pcddchgIO.setTransactionTime(agtchgrec.transactionTime);
		pcddchgIO.setCurrto(varcom.vrcmMaxDate);
		pcddchgIO.setAgntnum(agtchgrec.agntnumNew);
		if (isEQ(pcddchgIO.getStatuz(), varcom.mrnf)) {
			/*NEXT_SENTENCE*/
		}
		else {
			processOldPcdd750();
			goTo(GotoLabel.exit740);
		}
	}

	/**
	* <pre>
	* In order to recalculate the commission for the New Agent,
	* we will need to store the commission and bonus points
	* split - therefore it is done in both sections (WRITR for
	* new Agent or REWRT for New Agent), cos you can only get
	* into one or the other.
	* </pre>
	*/
protected void writrNew730()
	{
		/* Write out a record for the New Agent*/
		pcddchgIO.setValidflag("1");
		pcddchgIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, pcddchgIO);
		if (isNE(pcddchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			fatalError9000();
		}
		wsaaSplitBcommSave.set(pcddchgIO.getSplitBcomm());
	}

protected void processOldPcdd750()
	{
		/*GO*/
		/* Rewrite the record, updating the commission split and*/
		/* bonus points values*/
		setPrecision(pcddchgIO.getSplitBcomm(), 2);
		pcddchgIO.setSplitBcomm(add(pcddchgIO.getSplitBcomm(), wsaaSplitBcomm));
		setPrecision(pcddchgIO.getSplitBpts(), 2);
		pcddchgIO.setSplitBpts(add(pcddchgIO.getSplitBpts(), wsaaSplitBpts));
		pcddchgIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, pcddchgIO);
		if (isNE(pcddchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			fatalError9000();
		}
		wsaaSplitBcommSave.set(pcddchgIO.getSplitBcomm());
		/*EXIT*/
	}

protected void readT5645800()
	{
		para810();
	}

protected void para810()
	{
		/* Read T5645 to get the Sub-Account details for this*/
		/* transaction*/
		itemIO.setItemcoy(agtchgrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void setupCommonAcmv900()
	{
		para910();
	}

protected void para910()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(wsaaBatckey.batcKey);
		lifacmvrec.rdocnum.set(agtchgrec.chdrnum);
		lifacmvrec.rldgcoy.set(agtchgrec.chdrcoy);
		lifacmvrec.genlcoy.set(agtchgrec.chdrcoy);
		lifacmvrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifacmvrec.origcurr.set(wsaaCntcurr);
		lifacmvrec.tranno.set(wsaaTranno);
		compute(lifacmvrec.jrnseq, 0).set(add(wsaaJrnseq, 1));
		lifacmvrec.tranref.set(agtchgrec.chdrnum);
		lifacmvrec.postyear.set(wsaaBatckey.batcBatcactyr);
		lifacmvrec.postmonth.set(wsaaBatckey.batcBatcactmn);
		lifacmvrec.effdate.set(agtchgrec.transactionDate);
		lifacmvrec.transactionDate.set(agtchgrec.transactionDate);
		lifacmvrec.transactionTime.set(agtchgrec.transactionTime);
		lifacmvrec.user.set(agtchgrec.user);
		lifacmvrec.termid.set(agtchgrec.termid);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(agtchgrec.chdrcoy);
		descIO.setDesctabl(t1688);
		descIO.setLanguage("E");
		descIO.setDescitem(wsaaBatckey.batcBatctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError9000();
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
	}

protected void postCommClawback1000()
	{
		para1010();
	}

protected void para1010()
	{
		if (isEQ(wsaaCommClawback, ZERO)) {
			return ;
		}
		lifacmvrec.origamt.set(wsaaCommClawback);
		lifacmvrec.rldgacct.set(agcmchgIO.getAgntnum());
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		readCovrenq1100();
		/* MOVE SPACES                 TO LIFA-SUBSTITUTE-CODE(06).     */
		lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
		if (isEQ(agcmchgIO.getOvrdcat(), "O")) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
		}
		else {
			wsaaRldgChdrnum.set(agcmchgIO.getChdrnum());
			wsaaRldgLife.set(agcmchgIO.getLife());
			wsaaRldgCoverage.set(agcmchgIO.getCoverage());
			wsaaRldgRider.set(agcmchgIO.getRider());
			wsaaPlan.set(agcmchgIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.sacscode.set(t5645rec.sacscode01);
			lifacmvrec.sacstyp.set(t5645rec.sacstype01);
			lifacmvrec.glcode.set(t5645rec.glmap01);
			lifacmvrec.glsign.set(t5645rec.sign01);
			lifacmvrec.contot.set(t5645rec.cnttot01);
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
		/* Override Commission                                             */
		if (isEQ(th605rec.indic, "Y")) {
			zorlnkrec.annprem.set(agcmchgIO.getAnnprem());
			a100CallZorcompy();
		}
	}

protected void readCovrenq1100()
	{
		start1100();
	}

protected void start1100()
	{
		covrenqIO.setChdrcoy(agcmchgIO.getChdrcoy());
		covrenqIO.setChdrnum(agcmchgIO.getChdrnum());
		covrenqIO.setLife(agcmchgIO.getLife());
		covrenqIO.setCoverage(agcmchgIO.getCoverage());
		covrenqIO.setRider(agcmchgIO.getRider());
		covrenqIO.setPlanSuffix(agcmchgIO.getPlanSuffix());
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError9000();
		}
	}

protected void postAdvanceComm2000()
	{
		para2010();
	}

protected void para2010()
	{
		if (isEQ(wsaaCommAdvance, ZERO)) {
			return ;
		}
		covrenqIO.setChdrcoy(agtchgrec.chdrcoy);
		covrenqIO.setChdrnum(agcmchgIO.getChdrnum());
		covrenqIO.setLife(agcmchgIO.getLife());
		covrenqIO.setCoverage(agcmchgIO.getCoverage());
		covrenqIO.setRider(agcmchgIO.getRider());
		covrenqIO.setPlanSuffix(agcmchgIO.getPlanSuffix());
		covrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrenqIO.getParams());
			syserrrec.statuz.set(covrenqIO.getStatuz());
			fatalError9000();
		}
		lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		lifacmvrec.origamt.set(wsaaCommAdvance);
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			wsaaRldgChdrnum.set(agcmchgIO.getChdrnum());
			wsaaRldgLife.set(agcmchgIO.getLife());
			wsaaRldgCoverage.set(agcmchgIO.getCoverage());
			wsaaRldgRider.set(agcmchgIO.getRider());
			wsaaPlan.set(agcmchgIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			if (isEQ(agcmchgIO.getOvrdcat(), "O")) {
				lifacmvrec.sacscode.set(t5645rec.sacscode06);
				lifacmvrec.sacstyp.set(t5645rec.sacstype06);
				lifacmvrec.glcode.set(t5645rec.glmap06);
				lifacmvrec.glsign.set(t5645rec.sign06);
				lifacmvrec.contot.set(t5645rec.cnttot06);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode05);
				lifacmvrec.sacstyp.set(t5645rec.sacstype05);
				lifacmvrec.glcode.set(t5645rec.glmap05);
				lifacmvrec.glsign.set(t5645rec.sign05);
				lifacmvrec.contot.set(t5645rec.cnttot05);
			}
		}
		else {
			/*MOVE AGCG-CHDRNUM           TO LIFA-RLDGACCT              */
			wsaaRldgChdrnum.set(agcmchgIO.getChdrnum());
			wsaaRldgLife.set(agcmchgIO.getLife());
			wsaaRldgCoverage.set(agcmchgIO.getCoverage());
			wsaaRldgRider.set(agcmchgIO.getRider());
			wsaaPlan.set(agcmchgIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			if (isEQ(agcmchgIO.getOvrdcat(), "O")) {
				lifacmvrec.sacscode.set(t5645rec.sacscode04);
				lifacmvrec.sacstyp.set(t5645rec.sacstype04);
				lifacmvrec.glcode.set(t5645rec.glmap04);
				lifacmvrec.glsign.set(t5645rec.sign04);
				lifacmvrec.contot.set(t5645rec.cnttot04);
			}
			else {
				lifacmvrec.sacscode.set(t5645rec.sacscode02);
				lifacmvrec.sacstyp.set(t5645rec.sacstype02);
				lifacmvrec.glcode.set(t5645rec.glmap02);
				lifacmvrec.glsign.set(t5645rec.sign02);
				lifacmvrec.contot.set(t5645rec.cnttot02);
			}
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			fatalError9000();
		}
	}

protected void readT56882500()
	{
		para2500();
	}

protected void para2500()
	{
		/* Read T5688 to determine whether component or contract           */
		/* level accounting is needed.                                     */
		itdmIO.setItemcoy(agtchgrec.chdrcoy);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(), agtchgrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(chdrlifIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e308);
			fatalError9000();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void agcmForNewAgent3000()
	{
		/*GO*/
		getAglfDetails4000();
		processCovr4200();
		writeAgcmRec3500();
		/*EXIT*/
	}

protected void writeAgcmRec3500()
	{
		para3510();
	}

protected void para3510()
	{
		agcmchgIO.setChdrcoy(covrIO.getChdrcoy());
		agcmchgIO.setChdrnum(covrIO.getChdrnum());
		agcmchgIO.setAgntnum(agtchgrec.agntnumNew);
		agcmchgIO.setLife(covrIO.getLife());
		agcmchgIO.setCoverage(covrIO.getCoverage());
		agcmchgIO.setRider(covrIO.getRider());
		agcmchgIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmchgIO.setTranno(wsaaTranno);
		agcmchgIO.setAgentClass(aglfIO.getAgentClass());
		agcmchgIO.setTermid(agtchgrec.termid);
		agcmchgIO.setTransactionDate(agtchgrec.transactionDate);
		agcmchgIO.setTransactionTime(agtchgrec.transactionTime);
		agcmchgIO.setUser(agtchgrec.user);
		agcmchgIO.setValidflag("1");
		agcmchgIO.setCurrfrom(agtchgrec.transactionDate);
		agcmchgIO.setCurrto(varcom.vrcmMaxDate);
		/*    MOVE ZEROES                 TO AGCMCHG-PTDATE.               */
		agcmchgIO.setPtdate(wsaaPtdate);
		agcmchgIO.setCedagent(SPACES);
		agcmchgIO.setOvrdcat("B");
		agcmchgIO.setDormantFlag(wsaaDormflag);
		agcmchgIO.setSeqno(wsaaSeqno);
		agcmchgIO.setFormat(agcmchgrec);
		agcmchgIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmchgIO);
		if (isNE(agcmchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmchgIO.getParams());
			syserrrec.statuz.set(agcmchgIO.getStatuz());
			fatalError9000();
		}
	}

protected void getAglfDetails4000()
	{
		para4010();
	}

protected void para4010()
	{
		/* Get AGLF details for the new agent*/
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(agtchgrec.chdrcoy);
		aglfIO.setAgntnum(agtchgrec.agntnumNew);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if ((isNE(aglfIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError9000();
		}
		wsaaStoredAgentClass.set(aglfIO.getAgentClass());
	}

protected void findOverride4100()
	{
		para4110();
		findPara4120();
	}

protected void para4110()
	{
		/* Look up AGLF record for the old agent, to check for any*/
		/* override details*/
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(agtchgrec.chdrcoy);
		aglfIO.setAgntnum(agtchgrec.agntnumOld);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
	}

protected void findPara4120()
	{
		SmartFileCode.execute(appVars, aglfIO);
		if ((isNE(aglfIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(aglfIO.getReportag(), SPACES)) {
			wsaaIfOverrideAg = "N";
			return ;
		}
		if (isEQ(aglfIO.getReportag(), agcmchgIO.getAgntnum())) {
			wsaaIfOverrideAg = "Y";
			return ;
		}
		aglfIO.setAgntnum(aglfIO.getReportag());
		findPara4120();
		return;
	}

protected void processCovr4200()
	{
		try {
			go4210();
			doComm4220();
			serv4240();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void go4210()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(agcmchgIO.getChdrcoy());
		covrIO.setChdrnum(agcmchgIO.getChdrnum());
		covrIO.setLife(agcmchgIO.getLife());
		covrIO.setCoverage(agcmchgIO.getCoverage());
		covrIO.setRider(agcmchgIO.getRider());
		covrIO.setPlanSuffix(agcmchgIO.getPlanSuffix());
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError9000();
		}
	}

protected void doComm4220()
	{
		readT56874600();
		if (isNE(covrIO.getRider(), "00")) {
			readTr6954700();
		}
		/*INIT*/
		if ((isEQ(agtchgrec.initCommFlag, "Y"))
		&& (isEQ(wsaaTranInit, "Y"))) {
			recalcInitComm6000();
		}
		else {
			agcmchgIO.setBasicCommMeth(SPACES);
			agcmchgIO.setBascpy(SPACES);
			agcmchgIO.setInitcom(ZERO);
			agcmchgIO.setCompay(ZERO);
			agcmchgIO.setComern(ZERO);
		}
		/* If the AGCM record we have is for an overriding agent, we do*/
		/* not want to recalculate the servicing and renewal commission.*/
		if (isEQ(agcmchgIO.getOvrdcat(), "O")) {
			goTo(GotoLabel.exit4290);
		}
	}

protected void serv4240()
	{
		if ((isEQ(agtchgrec.servCommFlag, "Y"))
		&& (isEQ(wsaaTranServ, "Y"))) {
			recalcServComm7000();
		}
		else {
			agcmchgIO.setSrvcpy(SPACES);
			agcmchgIO.setScmdue(ZERO);
			agcmchgIO.setScmearn(ZERO);
		}
		/*RNWL*/
		if ((isEQ(agtchgrec.rnwlCommFlag, "Y"))
		&& (isEQ(wsaaTranRnwl, "Y"))) {
			recalcRnwlComm8000();
		}
		else {
			agcmchgIO.setRnwcpy(SPACES);
			agcmchgIO.setRnlcdue(ZERO);
			agcmchgIO.setRnlcearn(ZERO);
		}
	}

protected void setUpLinkage4500()
	{
		para4510();
	}

protected void para4510()
	{
		comlinkrec.chdrcoy.set(agtchgrec.chdrcoy);
		comlinkrec.chdrnum.set(agtchgrec.chdrnum);
		comlinkrec.agent.set(agtchgrec.agntnumNew);
		comlinkrec.life.set(covrIO.getLife());
		comlinkrec.jlife.set(covrIO.getJlife());
		comlinkrec.coverage.set(covrIO.getCoverage());
		comlinkrec.rider.set(covrIO.getRider());
		comlinkrec.planSuffix.set(covrIO.getPlanSuffix());
		comlinkrec.crtable.set(covrIO.getCrtable());
		/* MOVE CHDRLIF-OCCDATE        TO CLNK-EFFDATE.                 */
		comlinkrec.effdate.set(agcmchgIO.getEfdate());
		wsaaBillfreq.set(chdrlifIO.getBillfreq());
		comlinkrec.billfreq.set(chdrlifIO.getBillfreq());
		comlinkrec.agentClass.set(wsaaStoredAgentClass);
		comlinkrec.seqno.set(wsaaSeqno);
		comlinkrec.icommtot.set(0);
		comlinkrec.icommpd.set(0);
		comlinkrec.icommernd.set(0);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		comlinkrec.instprem.set(0);
		comlinkrec.annprem.set(0);
		comlinkrec.targetPrem.set(0);
		comlinkrec.ptdate.set(0);
		comlinkrec.currto.set(0);
		/* COMPUTE CLNK-ANNPREM ROUNDED                                 */
		/*                      = COVR-INSTPREM * WSAA-BILLFREQ         */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		/* COMPUTE CLNK-INSTPREM ROUNDED                                */
		/*                       = COVR-INSTPREM * WSAA-FREQ-FACTOR     */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		compute(comlinkrec.annprem, 3).setRounded(mult(agcmchgIO.getAnnprem(), (div(wsaaSplitBcommSave, 100))));
		compute(comlinkrec.instprem, 6).setRounded(mult(mult(div(agcmchgIO.getAnnprem(), wsaaBillfreq), wsaaFreqFactor), (div(wsaaSplitBcommSave, 100))));
		if (agentCommChange.isTrue()) {
			comlinkrec.annprem.set(agcmchgIO.getAnnprem());
			compute(comlinkrec.instprem, 3).setRounded(mult(covrIO.getInstprem(), (div(wsaaSplitBcommSave, 100))));
		}
	}

protected void readT56874600()
	{
		go4610();
	}

protected void go4610()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(agtchgrec.chdrcoy);
		itdmIO.setItemtabl(t5687);
		/*  MOVE COVR-CRTABLE           TO ITDM-ITEMITEM.                */
		itdmIO.setItemitem(covrIO.getCrtable());
		wsaaCrtable.set(covrIO.getCrtable());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), agtchgrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), t5687))
		|| (isNE(itdmIO.getItemitem(), covrIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(covrIO.getCrtable());
			syserrrec.statuz.set(f294);
			fatalError9000();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMethod.set(t5687rec.basicCommMeth);
		wsaaBascpy.set(t5687rec.bascpy);
		wsaaRnwcpy.set(t5687rec.rnwcpy);
		wsaaSrvcpy.set(t5687rec.srvcpy);
	}

protected void readTr6954700()
	{
		go4710();
	}

protected void go4710()
	{
		wsaaTr695Coverage.set(wsaaCrtable);
		wsaaTr695Rider.set(covrIO.getCrtable());
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(agtchgrec.chdrcoy);
		itdmIO.setItemtabl(tr695);
		itdmIO.setItemitem(wsaaTr695Key);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if ((isNE(itdmIO.getItemcoy(), agtchgrec.chdrcoy))
		|| (isNE(itdmIO.getItemtabl(), tr695))
		|| (isNE(itdmIO.getItemitem(), wsaaTr695Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaTr695Rider.set("****");
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemcoy(agtchgrec.chdrcoy);
			itdmIO.setItemtabl(tr695);
			itdmIO.setItemitem(wsaaTr695Key);
			itdmIO.setItmfrm(chdrlifIO.getOccdate());
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError9000();
			}
			if (isNE(itdmIO.getItemcoy(), agtchgrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(), tr695)
			|| isNE(itdmIO.getItemitem(), wsaaTr695Key)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				return ;
			}
		}
		tr695rec.tr695Rec.set(itdmIO.getGenarea());
		wsaaBasicCommMethod.set(tr695rec.basicCommMeth);
		wsaaBascpy.set(tr695rec.bascpy);
		wsaaRnwcpy.set(tr695rec.rnwcpy);
		wsaaSrvcpy.set(tr695rec.srvcpy);
	}

protected void newOverride5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					newOvrd5010();
				case writeAgcm5050: 
					writeAgcm5050();
				case checkNext5040: 
					checkNext5040();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void newOvrd5010()
	{
		/* Compute the overrider commissions*/
		setPrecision(agcmIO.getInitcom(), 3);
		agcmIO.setInitcom(mult(wsaaIcommtot, (div(aglfchgIO.getOvcpc(), 100))), true);
		/* Commission payed and earned are calculated                      */
		/* COMPUTE                                                      */
		/*   AGCM-COMPAY ROUNDED                                        */
		/*                = WSAA-PAYAMNT * (AGLFCHG-OVCPC / 100).       */
		/* COMPUTE                                                      */
		/*   AGCM-COMERN ROUNDED                                        */
		/*                = WSAA-ERNDAMT * (AGLFCHG-OVCPC / 100).       */
		/* Set up AGCM record values*/
		agcmIO.setScmdue(0);
		agcmIO.setScmearn(0);
		agcmIO.setRnlcdue(0);
		agcmIO.setComern(0);
		agcmIO.setCompay(0);
		agcmIO.setRnlcearn(0);
		/*                                AGCM-SRVCPY,                  */
		/*                                AGCM-RNWCPY.                  */
		agcmIO.setSrvcpy(SPACES);
		agcmIO.setRnwcpy(SPACES);
		agcmIO.setAnnprem(comlinkrec.annprem);
		agcmIO.setBasicCommMeth(wsaaBasicCommMeth);
		/* Set up the Agent Number*/
		agcmIO.setAgntnum(aglfchgIO.getReportag());
		/* Read the AGLF details for the Reporting To Agent, to ensure*/
		/* that we set up the AGCM record with the correct payment*/
		/* pattern details*/
		aglfchgIO.setAgntcoy(agtchgrec.chdrcoy);
		aglfchgIO.setAgntnum(aglfchgIO.getReportag());
		aglfchgIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfchgIO);
		if (isNE(aglfchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfchgIO.getParams());
			syserrrec.statuz.set(aglfchgIO.getStatuz());
			fatalError9000();
		}
		agcmIO.setBascpy(aglfchgIO.getBcmtab());
		/* If all commission values are zero do not write an AGCM -*/
		/* go to the check for another overriding agent*/
		if (isEQ(agcmIO.getAnnprem(), 0)
		&& isEQ(agcmIO.getInitcom(), 0)
		&& isEQ(agcmIO.getScmdue(), 0)
		&& isEQ(agcmIO.getScmearn(), 0)
		&& isEQ(agcmIO.getRnlcdue(), 0)
		&& isEQ(agcmIO.getRnlcearn(), 0)) {
			goTo(GotoLabel.checkNext5040);
		}
		if (isEQ(agcmIO.getBascpy(), SPACES)) {
			agcmIO.setBascpy(wsaaBascpy);
			goTo(GotoLabel.writeAgcm5050);
		}
		setUpLinkage4500();
		comlinkrec.agent.set(agcmIO.getAgntnum());
		comlinkrec.agentClass.set(wsaaStoredAgentClass);
		comlinkrec.icommtot.set(agcmIO.getInitcom());
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		/*  MOVE CHDRLIF-OCCDATE       TO CLNK-EFFDATE.         <LA4517>*/
		comlinkrec.effdate.set(agcmchgIO.getEfdate());
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5644);
		itemIO.setItemitem(agcmIO.getBascpy());
		comlinkrec.method.set(agcmIO.getBascpy());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr, SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				fatalError9000();
			}
		}
		agcmIO.setCompay(comlinkrec.payamnt);
		agcmIO.setComern(comlinkrec.erndamt);
	}

protected void writeAgcm5050()
	{
		/* Set up the AGCM details*/
		agcmIO.setTermid(agtchgrec.termid);
		agcmIO.setTransactionDate(agtchgrec.transactionDate);
		agcmIO.setTransactionTime(agtchgrec.transactionTime);
		agcmIO.setUser(agtchgrec.user);
		agcmIO.setChdrcoy(agtchgrec.chdrcoy);
		agcmIO.setChdrnum(agtchgrec.chdrnum);
		/*MOVE WSAA-STORED-AGENT-CLASS TO AGCM-AGENT-CLASS.            */
		agcmIO.setAgentClass(aglfchgIO.getAgentClass());
		agcmIO.setLife(covrIO.getLife());
		agcmIO.setCoverage(covrIO.getCoverage());
		agcmIO.setRider(covrIO.getRider());
		agcmIO.setPlanSuffix(covrIO.getPlanSuffix());
		agcmIO.setTranno(wsaaTranno);
		/* MOVE CHDRLIF-OCCDATE        TO AGCM-EFDATE.                  */
		agcmIO.setEfdate(agcmchgIO.getEfdate());
		agcmIO.setCurrfrom(agtchgrec.transactionDate);
		agcmIO.setCurrto(varcom.vrcmMaxDate);
		agcmIO.setOvrdcat("O");
		agcmIO.setValidflag("1");
		/*    MOVE SPACES                 TO AGCM-CEDAGENT.                */
		/*    MOVE ZEROES                 TO AGCM-PTDATE.                  */
		agcmIO.setCedagent(agtchgrec.agntnumNew);
		agcmIO.setPtdate(wsaaPtdate);
		agcmIO.setSeqno(wsaaSeqno);
		agcmIO.setDormantFlag(wsaaDormflag);
		agcmIO.setFormat(agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			fatalError9000();
		}
	}

protected void checkNext5040()
	{
		/* Check to see if this Reporting To Agent reports to another*/
		/* agent*/
		if (isEQ(aglfchgIO.getReportag(), SPACES)) {
			aglfchgIO.setOvcpc(0);
			return ;
		}
		if (isEQ(aglfchgIO.getOvcpc(), ZERO)) {
			return ;
		}
		/* Check whether the overiders are looping*/
		if (isEQ(aglfchgIO.getReportag(), aglfchgIO.getAgntnum())) {
			aglfchgIO.setReportag(SPACES);
			aglfchgIO.setOvcpc(0);
		}
		/*EXIT*/
	}

protected void recalcInitComm6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para6010();
					basicComm6015();
				case initComm6020: 
					initComm6020();
				case accumAmts6030: 
					accumAmts6030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6010()
	{
		setUpLinkage4500();
		/*  IF T5687-BASIC-COMM-METH    = SPACES                         */
		if (isEQ(wsaaBasicCommMethod, SPACES)) {
			goTo(GotoLabel.initComm6020);
		}
	}

protected void basicComm6015()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5647);
		/*  MOVE T5687-BASIC-COMM-METH  TO ITEM-ITEMITEM,                */
		itemIO.setItemitem(wsaaBasicCommMethod);
		agcmchgIO.setBasicCommMeth(wsaaBasicCommMethod);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		/*  MOVE T5687-BASIC-COMM-METH  TO CLNK-METHOD.                  */
		comlinkrec.method.set(wsaaBasicCommMethod);
		if (isNE(t5647rec.commsubr, SPACES)) {
			comlinkrec.language.set(agtchgrec.language);
			callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				fatalError9000();
			}
		}
	}

protected void initComm6020()
	{
		/*  IF (T5687-BASCPY            = SPACES) AND                    */
		if ((isEQ(wsaaBascpy, SPACES))
		&& (isEQ(aglfIO.getBcmtab(), SPACES))) {
			goTo(GotoLabel.accumAmts6030);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5644);
		/*  IF T5687-BASCPY             = SPACES                         */
		if (isEQ(wsaaBascpy, SPACES)) {
			itemIO.setItemitem(aglfIO.getBcmtab());
			comlinkrec.method.set(aglfIO.getBcmtab());
			agcmchgIO.setBascpy(aglfIO.getBcmtab());
		}
		else {
			/*     MOVE T5687-BASCPY        TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaBascpy);
			agcmchgIO.setBascpy(wsaaBascpy);
			comlinkrec.method.set(wsaaBascpy);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr, SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				fatalError9000();
			}
		}
	}

protected void accumAmts6030()
	{
		zrdecplrec.amountIn.set(comlinkrec.icommtot);
		a200CallRounding();
		comlinkrec.icommtot.set(zrdecplrec.amountOut);
		/* The initial commission is set to whatever has been calculated*/
		agcmchgIO.setInitcom(comlinkrec.icommtot);
		/* If the new amounts calculated are greater than what*/
		/* is on the AGCM at the moment, set the AGCM values*/
		/* to the new amounts.*/
		/* IF CLNK-PAYAMNT             >  AGCMCHG-COMPAY                */
		agcmchgIO.setCompay(comlinkrec.payamnt);
		/* IF CLNK-ERNDAMT             >  AGCMCHG-COMERN                */
		agcmchgIO.setComern(comlinkrec.erndamt);
		/*  MOVE T5687-BASIC-COMM-METH  TO WSAA-BASIC-COMM-METH.         */
		wsaaBasicCommMeth.set(wsaaBasicCommMethod);
		wsaaIcommtot.set(comlinkrec.icommtot);
		if (isNE(aglfIO.getReportag(), SPACES)) {
			aglfchgIO.setReportag(aglfIO.getReportag());
			aglfchgIO.setOvcpc(aglfIO.getOvcpc());
			while ( !(isEQ(aglfchgIO.getReportag(), SPACES)
			|| isEQ(aglfchgIO.getOvcpc(), ZERO))) {
				newOverride5000();
			}
			
		}
	}

protected void recalcServComm7000()
	{
		para7010();
	}

protected void para7010()
	{
		setUpLinkage4500();
		/* COMPUTE CLNK-ANNPREM ROUNDED                                 */
		/*                        = COVR-INSTPREM * WSAA-BILLFREQ       */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		/* COMPUTE CLNK-INSTPREM ROUNDED                                */
		/*                        = COVR-INSTPREM *                     */
		/*                                 WSAA-FREQ-FACTOR             */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5644);
		/*  IF T5687-SRVCPY             = SPACES                         */
		if (isEQ(wsaaSrvcpy, SPACES)) {
			itemIO.setItemitem(aglfIO.getScmtab());
			agcmchgIO.setSrvcpy(aglfIO.getScmtab());
		}
		else {
			/*     MOVE T5687-SRVCPY        TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaSrvcpy);
			agcmchgIO.setSrvcpy(wsaaSrvcpy);
		}
		if (isEQ(itemIO.getItemitem(), SPACES)) {
			return ;
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(aglfIO.getScmtab(), SPACES)) {
			comlinkrec.method.set(aglfIO.getScmtab());
		}
		else {
			/*     MOVE T5687-SRVCPY        TO CLNK-METHOD.                  */
			comlinkrec.method.set(wsaaSrvcpy);
		}
		if (isNE(t5644rec.compysubr, SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				fatalError9000();
			}
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a200CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a200CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		/* If the new amounts calculated are greater than what*/
		/* is on the AGCM at the moment, set the AGCM values*/
		/* to the new amounts.*/
		if (isGT(comlinkrec.payamnt, agcmchgIO.getScmdue())) {
			agcmchgIO.setScmdue(comlinkrec.payamnt);
		}
		if (isGT(comlinkrec.erndamt, agcmchgIO.getScmearn())) {
			agcmchgIO.setScmearn(comlinkrec.erndamt);
		}
	}

protected void recalcRnwlComm8000()
	{
		para8010();
	}

protected void para8010()
	{
		setUpLinkage4500();
		/* COMPUTE CLNK-ANNPREM ROUNDED                                 */
		/*                      = COVR-INSTPREM * WSAA-BILLFREQ         */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		/* COMPUTE CLNK-INSTPREM ROUNDED                                */
		/*                         = COVR-INSTPREM *                    */
		/*                                 WSAA-FREQ-FACTOR             */
		/*                        * (WSAA-SPLIT-BCOMM-SAVE / 100).      */
		agcmchgIO.setRnlcdue(0);
		agcmchgIO.setRnlcearn(0);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5644);
		if (isNE(aglfIO.getRcmtab(), SPACES)) {
			itemIO.setItemitem(aglfIO.getRcmtab());
			comlinkrec.method.set(aglfIO.getRcmtab());
			agcmchgIO.setRnwcpy(aglfIO.getRcmtab());
		}
		else {
			/*     MOVE T5687-RNWCPY        TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaRnwcpy);
			agcmchgIO.setRnwcpy(wsaaRnwcpy);
			comlinkrec.method.set(wsaaRnwcpy);
		}
		if (isEQ(itemIO.getItemitem(), SPACES)) {
			return ;
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError9000();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr, SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			if (isNE(comlinkrec.statuz, varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				syserrrec.statuz.set(comlinkrec.statuz);
				fatalError9000();
			}
		}
		else {
			return ;
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a200CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a200CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		/* If the new amounts calculated are greater than what*/
		/* is on the AGCM at the moment, set the AGCM values*/
		/* to the new amounts.*/
		if (isGT(comlinkrec.payamnt, agcmchgIO.getRnlcdue())) {
			agcmchgIO.setRnlcdue(comlinkrec.payamnt);
		}
		if (isGT(comlinkrec.erndamt, agcmchgIO.getRnlcearn())) {
			agcmchgIO.setRnlcearn(comlinkrec.erndamt);
		}
	}

protected void fatalError9000()
	{
		error9110();
		exit9120();
	}

protected void error9110()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit9120()
	{
		agtchgrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(wsaaTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(agcmchgIO.getAgntnum());
		lifsttrrec.oldAgntnum.set(agtchgrec.agntnumOld);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			fatalError9000();
		}
	}

protected void a100CallZorcompy()
	{
		a110Start();
	}

protected void a110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(agcmchgIO.getEfdate());
		zorlnkrec.ptdate.set(chdrlifIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		/* MOVE CHDRLIF-TRANNO         TO ZORL-TRANNO.          <LA4955>*/
		zorlnkrec.tranno.set(wsaaTranno);
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set("Y");
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			fatalError9000();
		}
	}

protected void a200CallRounding()
	{
		/*A210-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(agtchgrec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(wsaaCntcurr);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError9000();
		}
		/*A290-EXIT*/
	}
}
