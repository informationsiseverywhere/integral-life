package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR663
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr663ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(219);
	public FixedLengthStringData dataFields = new FixedLengthStringData(91).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData zparam = DD.zparam.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData zprofile = DD.zprofile.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData ztaxid = DD.ztaxid.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(32).isAPartOf(dataArea, 91);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData zparamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zprofileErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ztaxidErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 123);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] zparamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] zprofileOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ztaxidOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);

	public LongData Sr663screenWritten = new LongData(0);
	public LongData Sr663protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr663ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zprofileOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zparamOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dteappOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ztaxidOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, zprofile, zparam, dteapp, ztaxid};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, zprofileOut, zparamOut, dteappOut, ztaxidOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, zprofileErr, zparamErr, dteappErr, ztaxidErr};
		screenDateFields = new BaseData[] {dteapp};
		screenDateErrFields = new BaseData[] {dteappErr};
		screenDateDispFields = new BaseData[] {dteappDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr663screen.class;
		protectRecord = Sr663protect.class;
	}

}
