package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl49ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(87);
	public FixedLengthStringData dataFields = new FixedLengthStringData(55).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cname = DD.cname.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 55);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 63);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData subfileArea = new FixedLengthStringData(207);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(92).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData agncynum = DD.agncynum.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData brnch = DD.brnch.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(subfileFields,13);
	public FixedLengthStringData regnum = DD.regnum.copy().isAPartOf(subfileFields,60);
	public ZonedDecimalData startDate = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,76);
	public ZonedDecimalData dateend = DD.enddate.copyToZonedDecimal().isAPartOf(subfileFields,84);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 92);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData agncynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData brnchErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData enddateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 120);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] agncynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] brnchOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] enddateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 204);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData startDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateendDisp = new FixedLengthStringData(10);

	public LongData Sjl49screensflWritten = new LongData(0);
	public LongData Sjl49screenctlWritten = new LongData(0);
	public LongData Sjl49screenWritten = new LongData(0);
	public LongData Sjl49windowWritten = new LongData(0);
	public LongData Sjl49hideWritten = new LongData(0);
	public LongData Sjl49protectWritten = new LongData(0);
	public GeneralTable Sjl49screensfl = new GeneralTable(AppVars.getInstance());

	@Override
	public boolean hasSubfile() {
		return true;
	}

	@Override
	public GeneralTable getScreenSubfileTable() {
		return Sjl49screensfl;
	}

	public Sjl49ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {slt, agncynum, brnch, cltname, regnum, startDate, dateend};
		screenSflOutFields = new BaseData[][] {sltOut, agncynumOut, brnchOut, cltnameOut, regnumOut,
			srdateOut, srdateOut, enddateOut};
		screenSflErrFields = new BaseData[] {sltErr, agncynumErr, brnchErr, cltnameErr, regnumErr, srdateErr, enddateErr};
		screenSflDateFields = new BaseData[] {startDate, dateend};
		screenSflDateErrFields = new BaseData[] {srdateErr, enddateErr};
		screenSflDateDispFields = new BaseData[] {startDateDisp, dateendDisp};

		screenFields = new BaseData[] {clntnum, cname};
		screenOutFields = new BaseData[][] {clntnumOut, cnameOut};
		screenErrFields = new BaseData[] {clntnumErr, cnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl49screen.class;
		screenSflRecord = Sjl49screensfl.class;
		screenCtlRecord = Sjl49screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl49protect.class;
		hideRecord = Sjl49hide.class;
	}

	@Override
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl49screenctl.lrec.pageSubfile);
	}
}
