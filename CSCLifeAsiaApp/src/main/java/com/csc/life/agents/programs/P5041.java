/*
 * File: P5041.java
 * Date: 29 August 2009 23:59:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P5041.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.agents.screens.S5041ScreenVars;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*       P5041 - Agent Commission Change Sub-menu
*       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*       Overview
*       ~~~~~~~~
*
*       This Sub-menu will allow you to select a contract to  chan e  the
*       current  commission  agents  to a new agent. Noting that o ly one
*       agent can be changed at a time.
*
*       1000-INITIALISE.
*
*       Initialise the screen for display.
*
*       2000-SCREEN-EDIT.
*
*       Validation needs to be performed on the contract number.
*
*       Firstly the contract number must be numeric.
*
*       Read the CHDRMJA with a READR to check to see it is  an  e isting
*       contract.
*
*       If no matching record is found report an error.
*
*       If  the  contract  does  exist you must then read T5679 us ng the
*       sub-menu transaction code. If the STATCODE on the CHDR  do s  not
*       appear on this table, again report an error.
*
*       3000-UPDATE.
*
*       Do  a KEEPS on the CHDRMJA here so that in further process ng the
*       contract can be Retrieved.
*
*       Call 'Batcdor' with an AUTO Function to open  a  batch  fo   this
*       transaction (if necessary).
*
*       Call 'Sftlock' to lock the contract.
*
*       4000-WHERE-NEXT.
*
*       Add 1 to WSSP-PROGRAM-PTR.
*
*
*****************************************************************
* </pre>
*/
public class P5041 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5041");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStatus = "";
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e717 = "E717";
	private static final String f259 = "F259";
	private static final String g667 = "G667";
	private static final String f910 = "F910";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmjarec = "CHDRMJAREC";
	private static final String itemrec = "ITEMREC";
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5041ScreenVars sv = ScreenProgram.getScreenVars( S5041ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		validate2200, 
		verifyBatchControl2300, 
		exit2090, 
		exit3900
	}

	public P5041() {
		super();
		screenVars = sv;
		new ScreenModel("S5041", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateAction2100();
				case validate2200: 
					validate2200();
				case verifyBatchControl2300: 
					verifyBatchControl2300();
					batchProgs2310();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5041IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5041-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateAction2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
			goTo(GotoLabel.validate2200);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.verifyBatchControl2300);
		}
	}

protected void validate2200()
	{
		/*    Validate fields*/
		if (isEQ(sv.chdrsel, SPACES)) {
			sv.chdrselErr.set(g667);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/* Read the contract header details.*/
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setChdrcoy(wsspcomn.company);
		chdrmjaIO.setChdrnum(sv.chdrsel);
		chdrmjaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(f259);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.chdr);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.chdrsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.chdrselErr.set(sdasancrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Read the valid statii from table T5679.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaValidStatus = "N";
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 12)
		|| isEQ(wsaaValidStatus, "Y")); wsaaIndex.add(1)){
			headerStatuzCheck2910();
		}
		if (isNE(wsaaValidStatus, "Y")) {
			sv.chdrselErr.set(e717);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*    GO TO 2900-EXIT.                                             */
		goTo(GotoLabel.exit2090);
	}

protected void verifyBatchControl2300()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(e073);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			goTo(GotoLabel.exit2090);
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
	}

protected void batchProgs2310()
	{
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			wsspcomn.edterror.set("Y");
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2900-EXIT.                                         */
			return ;
		}
		wsspcomn.secProg[1].set(bcbprogrec.nxtprog1);
		wsspcomn.secProg[2].set(bcbprogrec.nxtprog2);
		wsspcomn.secProg[3].set(bcbprogrec.nxtprog3);
		wsspcomn.secProg[4].set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     CHECK THE CONTRACT HEADER STATUS AGAINST T5679.
	* </pre>
	*/
protected void headerStatuzCheck2910()
	{
		/*HEADER-STATUZ-CHECK*/
		if (isNE(t5679rec.cnRiskStat[wsaaIndex.toInt()], SPACES)) {
			if ((isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()], chdrmjaIO.getStatcode()))) {
				wsaaValidStatus = "Y";
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*            GO TO 2990-EXIT.                                     */
				return ;
			}
		}
		/*EXIT1*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateWssp3100();
			updateBatchControl3200();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateWssp3100()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		/*  Update WSSP Key details*/
		/*  Keep the contract header record.*/
		chdrmjaIO.setFormat(chdrmjarec);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmjaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		/* Softlock the contract header record.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		/* MOVE S5041-CHDRSEL          TO SFTL-ENTITY.                  */
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3900);
		}
		if (isNE(subprogrec.bchrqd, "Y")) {
			goTo(GotoLabel.exit3900);
		}
	}

protected void updateBatchControl3200()
	{
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
			wsspcomn.edterror.set("Y");
			rollback();
			return ;
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.set(1);
		/*EXIT*/
	}
}
