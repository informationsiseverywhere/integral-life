package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5043
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5043ScreenVars extends SmartVarModel { 


	/*public FixedLengthStringData dataArea = new FixedLengthStringData(43);
	public FixedLengthStringData dataFields = new FixedLengthStringData(11).isAPartOf(dataArea, 0);*/
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,1);
	
	
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 11);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	
	
	/*public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 19);*/
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5043screenWritten = new LongData(0);
	public LongData S5043protectWritten = new LongData(0);

	public static int[] screenSflPfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	
	public boolean hasSubfile() {
		return false;
	}


	public S5043ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agntselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		/*screenFields = new BaseData[] {agntsel, action};
		screenOutFields = new BaseData[][] {agntselOut, actionOut};
		screenErrFields = new BaseData[] {agntselErr, actionErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};*/
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5043screen.class;
		protectRecord = S5043protect.class;
	}
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 43;
	}
	

	public int getDataFieldsSize(){
		return 11;  
	}
	public int getErrorIndicatorSize(){
		return 8; 
	}
	
	public int getOutputFieldSize(){
		return 24; 
	}
	

	public BaseData[] getscreenFields(){
		return new BaseData[] {agntsel, action};
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]  {agntselOut,actionOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {agntselErr,actionErr};
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {};

	}
	
	public FixedLengthStringData isDMSFlag = new FixedLengthStringData(1);	//ILIFE-8880
}
