package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:53
 * Description:
 * Copybook name: TM604REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tm604rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tm604Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData numofmbr = new ZonedDecimalData(7, 0).isAPartOf(tm604Rec, 0);
  	public FixedLengthStringData prems = new FixedLengthStringData(34).isAPartOf(tm604Rec, 7);
  	public ZonedDecimalData[] prem = ZDArrayPartOfStructure(2, 17, 2, prems, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(prems, 0, FILLER_REDEFINE);
  	public ZonedDecimalData prem01 = new ZonedDecimalData(17, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData prem02 = new ZonedDecimalData(17, 2).isAPartOf(filler, 17);
  	public FixedLengthStringData rpthead = new FixedLengthStringData(50).isAPartOf(tm604Rec, 41);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(409).isAPartOf(tm604Rec, 91, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tm604Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tm604Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}