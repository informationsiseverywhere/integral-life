package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.life.agents.programs.Sr5c2screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;
public class Sr5c2ScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(17);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData cnfrm = new FixedLengthStringData(1).isAPartOf(dataFields,0);	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(4).isAPartOf(dataArea, 1);
	public FixedLengthStringData cnfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(12).isAPartOf(dataArea,5);
	public FixedLengthStringData[] cnfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	
	
	public LongData Sr5c2screenWritten = new LongData(0);
	public LongData Sr5c2protectWritten = new LongData(0);
	public boolean hasSubfile() { 
		return false;
	}
	public Sr5c2ScreenVars() {
		super();
		initialiseScreenVars();
	}
	protected void initialiseScreenVars() {
		fieldIndMap.put(cnfrmOut,new String[] {"2","3","-2","4",null, null, null, null, null, null, null, null});
	screenFields = new BaseData[] {cnfrm};
	screenOutFields = new BaseData[][]{cnfrmOut};
	screenErrFields = new BaseData[]{cnfrmErr};
	screenDateFields = new BaseData[] {};
	screenDateErrFields = new BaseData[]{};
	screenDateDispFields = new BaseData[]{};
	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr5c2screen.class;
	protectRecord = Sr5c2protect.class;

	}
}
