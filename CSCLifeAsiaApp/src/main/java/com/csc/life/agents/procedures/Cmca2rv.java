/*
 * File: Cmca2rv.java
 * Date: 29 August 2009 22:41:07
 * Author: Quipoz Limited
 * 
 * Class transformed from CMCA2RV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglfcmcTableDAM;
import com.csc.life.agents.dataaccess.CovrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5576rec;
import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*      REVERSAL OF TERM MULTIPLIER BASED INITIAL COMMISSION
*
*  Obtain the Term Multiplier                 (T5576),  keyed by
* Coverage/rider  table code  and  effective  date  (hence  use
* ITDM).
*  Obtain  the  ANB @ CCD by reading the LIFECMC record with the
* key passed.  The  age is be used to access the table in order
* to get the correct entries.
* Obtain the premium cessation date from the COVRCMC record.
* Calculate the term as follows:-
*      Obtain  the  Date of Birth (DoB) of the (first) life and
*           add  the  Max  age  from T5576 to give a date. (Use
*           DATCON2  with  a frequency of 1, the date in as the
*           DoB  and  the  factor as the Max age.) Compare this
*           date  against the premium cessation date and select
*           the lesser of the two for further calculations.
*      Subtract the  effective  date  passed  (commission/risk)
*           from the above date and determine by the entries on
*           the table how accurate  the  result  will  be.  The
*           entries can indicate one  of five possible results,
*           'high year', 'low year',  'high month', 'low month'
*           and actual day.  The result may be a fraction.  Use
*           DATCON3 to do the  date  calculations  depending on
*           the "nearest" type as follows:
*           Nearest month - call DATCON3 with a factor of 12 to
*                give  the  number  of  months  between the two
*                dates.  Round up/down as applicable.
*           Nearest year/exact day - call DATCON3 with a factor
*                of  01 to give the number of years between the
*                two  dates.  Round  up/down  as  applicable to
*                nearest year, or take as is for exact day..
* Calculate the rate to be used:-
*      Multiply  the  above  result by the percentage per annum
*           rate.  (If  using "nearest month" method, also then
*           divide by 12).
*      Compare  this  new  result  against  the 'Min' and 'Max'
*           percentage  range. If it is greater than the 'Max',
*           then  use  the 'Max' percentage. If it is less than
*           the 'Min', then use the 'Min' percentage.
*      Add the 'Flat' rate to the above rate.
* Calculate   the    Initial   Commission   by   applying   the
* enhancements.
*      Firstly, access the enhancement rules table T5692, Keyed
*           on coverage/rider code  and  agent  class (READR on
*           AGLFCMC).  Based on the  age  of the life (READR on
*           LIFECMC with the key passed) access the appropriate
*           section in  the  commission  enhancement  table and
*           obtain the enhancement % of premium for the Initial
*           Commission.  Apply this  enhancement  to the
*           previously calculated rate.
*      Secondly, apply the  above  adjusted  rate to the annual
*           premium passed in  the linkage area. This gives the
*           commission amount.
*      Thirdly, apply  the  commission  enhancement  percentage
*           for initial commission from T5692 to the commission
*           amount calculated  above.  This  gives the enhanced
*           initial commission.
* The initial commission should  now be returned in the linkage
* area.
*****************************************************************
* </pre>
*/
public class Cmca2rv extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMCA2RV";

	private FixedLengthStringData wsaaT5692Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentClass = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 4).init(SPACES);
	private ZonedDecimalData wsaaT5692 = new ZonedDecimalData(5, 2).init(ZERO);
	private ZonedDecimalData wsaaT5692pc = new ZonedDecimalData(5, 2).init(ZERO);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCommAmt = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaMaxAge = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRate = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaRate10 = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaRateOver10 = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaDuration = new PackedDecimalData(11, 5);
	private ZonedDecimalData wsaaPrevTableAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaQuotient = new ZonedDecimalData(6, 0);

	private FixedLengthStringData wsaaT5576Result = new FixedLengthStringData(2);
	private Validator wsaaHighYear = new Validator(wsaaT5576Result, "HY");
	private Validator wsaaLowYear = new Validator(wsaaT5576Result, "LY");
	private Validator wsaaHighMonth = new Validator(wsaaT5576Result, "HM");
	private Validator wsaaLowMonth = new Validator(wsaaT5576Result, "LM");
	private Validator wsaaActualDay = new Validator(wsaaT5576Result, "AD");

	private FixedLengthStringData wsaaT5576Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5576Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5576Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5576Srcebus = new FixedLengthStringData(2).isAPartOf(wsaaT5576Key, 4).init(SPACES);
		/* ERRORS */
	private String f252 = "F252";
	private String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private String t5692 = "T5692";
	private String t5576 = "T5576";
	private String t1693 = "T1693";
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Life agents - commission calculation sys*/
	private AglfcmcTableDAM aglfcmcIO = new AglfcmcTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
		/*Coverage/rider details - commission calc*/
	private CovrcmcTableDAM covrcmcIO = new CovrcmcTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*LIFE record Commission Calculations*/
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T1693rec t1693rec = new T1693rec();
	private T5576rec t5576rec = new T5576rec();
	private T5692rec t5692rec = new T5692rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1050, 
		continue2020, 
		exit2090, 
		continue2120, 
		exit2190, 
		yearDay3040, 
		exit3090, 
		calc27010, 
		exit7090, 
		exit7590, 
		seExit8090, 
		dbExit8190
	}

	public Cmca2rv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		readChdrlnb500();
		obtains1000();
		calcTermBand2000();
		wsaaRate10.set(wsaaRate);
		calcTermBand2100();
		wsaaRateOver10.set(wsaaRate);
		calcInitCommission7000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readChdrlnb500()
	{
		/*RAEDR*/
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrlnbIO.setChdrnum(comlinkrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void obtains1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case next1050: {
					next1050();
					obtainPremDate1020();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5576);
		wsaaT5576Crtable.set(comlinkrec.crtable);
		wsaaT5576Srcebus.set(chdrlnbIO.getSrcebus());
		itdmIO.setItemitem(wsaaT5576Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	}

protected void next1050()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5576)
		|| isNE(itdmIO.getItemitem(),wsaaT5576Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			if (isNE(itdmIO.getItemitem(),wsaaT5576Key)
			&& isNE(wsaaT5576Srcebus,"**")) {
				itdmIO.setDataKey(SPACES);
				itdmIO.setItemcoy(comlinkrec.chdrcoy);
				itdmIO.setItemtabl(t5576);
				wsaaT5576Crtable.set(comlinkrec.crtable);
				wsaaT5576Srcebus.set("**");
				itdmIO.setItemitem(wsaaT5576Key);
				itdmIO.setItmfrm(comlinkrec.effdate);
				itdmIO.setFunction(varcom.begn);
				goTo(GotoLabel.next1050);
			}
			syserrrec.params.set(comlinkrec.crtable);
			syserrrec.statuz.set(f252);
			dbError8100();
		}
		else {
			t5576rec.t5576Rec.set(itdmIO.getGenarea());
		}
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife,SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(),varcom.oK)) {
			dbError8100();
		}
	}

protected void obtainPremDate1020()
	{
		covrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		covrcmcIO.setChdrnum(comlinkrec.chdrnum);
		covrcmcIO.setLife(comlinkrec.life);
		covrcmcIO.setCoverage(comlinkrec.coverage);
		covrcmcIO.setRider(comlinkrec.rider);
		covrcmcIO.setPlanSuffix(comlinkrec.planSuffix);
		covrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrcmcIO);
		if (isNE(covrcmcIO.getStatuz(),varcom.oK)) {
			covrcmcIO.setParams(covrcmcIO.getParams());
			dbError8100();
		}
		if (isNE(comlinkrec.effdate,covrcmcIO.getCrrcd())) {
			calcAge6000();
		}
	}

protected void calcTermBand2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					calculateTerm2000();
				}
				case continue2020: {
					continue2020();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calculateTerm2000()
	{
		wsaaRate.set(0);
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age01)) {
			wsaaIndex.set(1);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age02)) {
			wsaaIndex.set(2);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age03)) {
			wsaaIndex.set(3);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age04)) {
			wsaaIndex.set(4);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age05)) {
			wsaaIndex.set(5);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age06)) {
			wsaaIndex.set(6);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age07)) {
			wsaaIndex.set(7);
			goTo(GotoLabel.continue2020);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5576rec.age08)) {
			wsaaIndex.set(8);
			goTo(GotoLabel.continue2020);
		}
		wsaaRate.set(0);
		goTo(GotoLabel.exit2090);
	}

protected void continue2020()
	{
		wsaaPrevTableAge.set(t5576rec.age[wsaaIndex.toInt()]);
		t5576rec.age[wsaaIndex.toInt()].set(0);
		wsaaMaxAge.set(t5576rec.agemax[wsaaIndex.toInt()]);
		datcon2rec.freqFactor.set(wsaaMaxAge);
		datcon2rec.intDate1.set(lifecmcIO.getCltdob());
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			systemError8000();
		}
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			systemError8000();
		}
		wsaaDate.set(datcon2rec.intDate2);
		if (isLT(covrcmcIO.getPremCessDate(),wsaaDate)) {
			wsaaDate.set(covrcmcIO.getPremCessDate());
		}
		dateCalc3000();
		compute(wsaaRate, 6).setRounded(mult(wsaaDuration,t5576rec.annumpc[wsaaIndex.toInt()]));
		if (wsaaHighMonth.isTrue()
		|| wsaaLowMonth.isTrue()) {
			compute(wsaaRate, 6).setRounded(div(wsaaRate,12));
		}
		if (isLT(wsaaRate,t5576rec.minpcnt[wsaaIndex.toInt()])) {
			wsaaRate.set(t5576rec.minpcnt[wsaaIndex.toInt()]);
		}
		if (isGT(wsaaRate,t5576rec.maxpcnt[wsaaIndex.toInt()])) {
			wsaaRate.set(t5576rec.maxpcnt[wsaaIndex.toInt()]);
		}
		wsaaRate.add(t5576rec.flatpcnt[wsaaIndex.toInt()]);
	}

protected void calcTermBand2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					calculateTerm2100();
				}
				case continue2120: {
					continue2120();
				}
				case exit2190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void calculateTerm2100()
	{
		wsaaRate.set(0);
		if (isEQ(wsaaPrevTableAge,t5576rec.age01)) {
			wsaaIndex.set(1);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age02)) {
			wsaaIndex.set(2);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age03)) {
			wsaaIndex.set(3);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age04)) {
			wsaaIndex.set(4);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age05)) {
			wsaaIndex.set(5);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age06)) {
			wsaaIndex.set(6);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age07)) {
			wsaaIndex.set(7);
			goTo(GotoLabel.continue2120);
		}
		if (isEQ(wsaaPrevTableAge,t5576rec.age08)) {
			wsaaIndex.set(8);
			goTo(GotoLabel.continue2120);
		}
		wsaaRate.set(0);
		goTo(GotoLabel.exit2190);
	}

protected void continue2120()
	{
		t5576rec.age[wsaaIndex.toInt()].set(0);
		wsaaMaxAge.set(t5576rec.agemax[wsaaIndex.toInt()]);
		datcon2rec.freqFactor.set(wsaaMaxAge);
		datcon2rec.intDate1.set(lifecmcIO.getCltdob());
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz,varcom.bomb)) {
			systemError8000();
		}
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			systemError8000();
		}
		wsaaDate.set(datcon2rec.intDate2);
		if (isLT(covrcmcIO.getPremCessDate(),wsaaDate)) {
			wsaaDate.set(covrcmcIO.getPremCessDate());
		}
		dateCalc3000();
		compute(wsaaRate, 6).setRounded(mult(wsaaDuration,t5576rec.annumpc[wsaaIndex.toInt()]));
		if (wsaaHighMonth.isTrue()
		|| wsaaLowMonth.isTrue()) {
			compute(wsaaRate, 6).setRounded(div(wsaaRate,12));
		}
		if (isLT(wsaaRate,t5576rec.minpcnt[wsaaIndex.toInt()])) {
			wsaaRate.set(t5576rec.minpcnt[wsaaIndex.toInt()]);
		}
		if (isGT(wsaaRate,t5576rec.maxpcnt[wsaaIndex.toInt()])) {
			wsaaRate.set(t5576rec.maxpcnt[wsaaIndex.toInt()]);
		}
		wsaaRate.add(t5576rec.flatpcnt[wsaaIndex.toInt()]);
	}

protected void dateCalc3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3000();
					month3020();
				}
				case yearDay3040: {
					yearDay3040();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		wsaaDuration.set(0);
		if (isNE(t5576rec.highmth,SPACES)) {
			wsaaT5576Result.set("HM");
		}
		if (isNE(t5576rec.lowmth,SPACES)) {
			wsaaT5576Result.set("LM");
		}
		if (isNE(t5576rec.highyr,SPACES)) {
			wsaaT5576Result.set("HY");
		}
		if (isNE(t5576rec.lowyr,SPACES)) {
			wsaaT5576Result.set("LY");
		}
		if (isNE(t5576rec.actualday,SPACES)) {
			wsaaT5576Result.set("AD");
		}
	}

protected void month3020()
	{
		if (wsaaHighMonth.isTrue()
		|| wsaaLowMonth.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.yearDay3040);
		}
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.intDate2.set(wsaaDate);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			systemError8000();
		}
		if (wsaaLowMonth.isTrue()) {
			wsaaQuotient.set(datcon3rec.freqFactor);
			wsaaDuration.set(wsaaQuotient);
		}
		else {
			compute(wsaaDuration, 5).set(add(datcon3rec.freqFactor,0.99999));
		}
		goTo(GotoLabel.exit3090);
	}

protected void yearDay3040()
	{
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.intDate2.set(wsaaDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			systemError8000();
		}
		if (wsaaActualDay.isTrue()) {
			wsaaDuration.set(datcon3rec.freqFactor);
			goTo(GotoLabel.exit3090);
		}
		if (wsaaLowYear.isTrue()) {
			wsaaQuotient.set(datcon3rec.freqFactor);
			wsaaDuration.set(wsaaQuotient);
		}
		else {
			compute(wsaaDuration, 5).set(add(datcon3rec.freqFactor,0.99999));
		}
		goTo(GotoLabel.exit3090);
	}

protected void calcAge6000()
	{
		init6010();
	}

protected void init6010()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(comlinkrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifecmcIO.getCltdob());
		agecalcrec.intDate2.set(comlinkrec.effdate);
		agecalcrec.language.set(comlinkrec.language);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError8000();
		}
		lifecmcIO.setAnbAtCcd(agecalcrec.agerating);
	}

protected void calcInitCommission7000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para7000();
				}
				case calc27010: {
					calc27010();
				}
				case exit7090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para7000()
	{
		comlinkrec.icommtot.set(0);
		aglfcmcIO.setAgntcoy(comlinkrec.chdrcoy);
		aglfcmcIO.setAgntnum(comlinkrec.agent);
		aglfcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfcmcIO);
		if (isNE(aglfcmcIO.getStatuz(),varcom.oK)) {
			dbError8100();
		}
		itdmIO.setDataKey(SPACES);
		wsaaCrtable.set(comlinkrec.crtable);
		wsaaAgentClass.set(aglfcmcIO.getAgentClass());
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5692);
		itdmIO.setItemitem(wsaaT5692Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5692)
		|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemtabl(t5692);
			wsaaCrtable.set("****");
			itdmIO.setItemitem(wsaaT5692Key);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				syserrrec.params.set(itdmIO.getParams());
				dbError8100();
			}
			if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5692)
			|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(wsaaT5692Key);
				syserrrec.statuz.set(f252);
				dbError8100();
			}
			else {
				t5692rec.t5692Rec.set(itdmIO.getGenarea());
			}
		}
		else {
			t5692rec.t5692Rec.set(itdmIO.getGenarea());
		}
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife,SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(),varcom.oK)) {
			dbError8100();
		}
		getT5692Value7500();
		if (isEQ(wsaaRate10,0)) {
			goTo(GotoLabel.calc27010);
		}
		wsaaRate.set(wsaaRate10);
		compute(wsaaRate, 6).setRounded(div(mult(wsaaRate,wsaaT5692),100));
		compute(wsaaCommAmt, 6).setRounded(div(mult(comlinkrec.annprem,wsaaRate),100));
		compute(wsaaCommAmt, 3).setRounded(div(mult(wsaaCommAmt,wsaaT5692pc),100));
		comlinkrec.icommtot.add(wsaaCommAmt);
	}

protected void calc27010()
	{
		if (isEQ(wsaaRateOver10,0)) {
			goTo(GotoLabel.exit7090);
		}
		wsaaRate.set(wsaaRateOver10);
		compute(wsaaRate, 6).setRounded(div(mult(wsaaRate,wsaaT5692),100));
		compute(wsaaCommAmt, 6).setRounded(div(mult(comlinkrec.annprem,wsaaRate),100));
		compute(wsaaCommAmt, 3).setRounded(div(mult(wsaaCommAmt,wsaaT5692pc),100));
		comlinkrec.icommtot.add(wsaaCommAmt);
	}

protected void getT5692Value7500()
	{
		try {
			para7500();
		}
		catch (GOTOException e){
		}
	}

protected void para7500()
	{
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age01)) {
			wsaaT5692.set(t5692rec.incmrate01);
			wsaaT5692pc.set(t5692rec.inprempc01);
			goTo(GotoLabel.exit7590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age02)) {
			wsaaT5692.set(t5692rec.incmrate02);
			wsaaT5692pc.set(t5692rec.inprempc02);
			goTo(GotoLabel.exit7590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age03)) {
			wsaaT5692.set(t5692rec.incmrate03);
			wsaaT5692pc.set(t5692rec.inprempc03);
			goTo(GotoLabel.exit7590);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age04)) {
			wsaaT5692.set(t5692rec.incmrate04);
			wsaaT5692pc.set(t5692rec.inprempc04);
			goTo(GotoLabel.exit7590);
		}
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
