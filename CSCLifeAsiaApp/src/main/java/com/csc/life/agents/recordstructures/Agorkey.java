package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:09
 * Description:
 * Copybook name: AGORKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agorkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agorFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agorKey = new FixedLengthStringData(64).isAPartOf(agorFileKey, 0, REDEFINE);
  	public FixedLengthStringData agorAgntcoy = new FixedLengthStringData(1).isAPartOf(agorKey, 0);
  	public FixedLengthStringData agorAgntnum = new FixedLengthStringData(8).isAPartOf(agorKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(agorKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agorFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agorFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}