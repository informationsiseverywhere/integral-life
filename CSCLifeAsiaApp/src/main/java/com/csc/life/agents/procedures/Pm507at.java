/*
 * File: Pm507at.java
 * Date: 30 August 2009 1:13:23
 * Author: Quipoz Limited
 *
 * Class transformed from PM507AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.ChdragtTableDAM;
import com.csc.life.agents.dataaccess.MacfTableDAM;
import com.csc.life.agents.dataaccess.MacfflvTableDAM;
import com.csc.life.agents.dataaccess.MacfinqTableDAM;
import com.csc.life.agents.dataaccess.MacfslvTableDAM;
import com.csc.life.agents.recordstructures.Agtchgrec;
import com.csc.life.agents.tablestructures.T6688rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Agent Commission Change AT module.
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*  This AT module is called  by  the  Agent  Commission  Change
*  Program  PM507.  The  contract  number  is  passed in the AT
*  parameters area as the "primary key".
*
*  For the agent and contract selected    we  must  update  the
*  relevant records.
*
*  In  Force  contracts  require  AGCM's to be updated together
*  with PCDD's.
*
*  Proposal status contracts require either the CHDR  Servicing
*  Agent  to  be updated or the PCDD dependent on whether there
*  is more than one agent or not. Basically if only  one  agent
*  is  entered  at  New Business time no PCDD record is created
*  until Issue. It is  only  when  a  split  of  commission  is
*  entered at proposal that a PCDD record is written.
*
*  A  new  table  has been set up that is keyed by status code.
*  T6688's extra data screen will simply have one  field  which
*  determines  which  subroutine  will  be  called.  There  are
*  currently 2 major subroutines that will be called:-
*
*  AGTCHGIF - In Force contract processing
*  AGTCHGPR - Proposal contract processing.
*
*  Mainline Processing.
*  ~~~~~~~~~~~~~~~~~~~~
*  Initialise.
*
*  Get Todays date by calling DATCON1.
*
*  Readr Contract Header (CHDRLIF) and use the STATCODE of  the
*  CHDR to read T6688.
*
*  Processing.
*
*  Set up the AGTCHGREC fields and call T6688-AGTCHGSUB.
*
*  Only update the CHDR record with the new agent number if
*  the WSAA-COMIND flag is 'Y' - otherwise go to Release the
*  Softlock.
*
*  Add  1  to the Tranno and Write a new CHDR record. Noting to
*  keep a history. Ie. Valid Flag '2' the originating CHDR.
*
*  Housekeeping.
*
*  Write a PTRN record.
*
*  Release the Softlock.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pm507at extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PM507AT");
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaValidStatcode = new FixedLengthStringData(1);
	private Validator validStatcode = new Validator(wsaaValidStatcode, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1);
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");
	private FixedLengthStringData wsaaAgnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZrptgb = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZrptgc = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZrptgd = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbReportag = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStore1Agnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStore1Zrptga = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStore1Effdate = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaStore1Agmvty = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStore1Tranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaStorexAgnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStorexZrptga = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStorexEffdate = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaStorexAgmvty = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStorexTranno = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFlvlagt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSlvlagt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(216);
	private FixedLengthStringData wsaaAgent = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 20);
	private FixedLengthStringData wsaaAgentnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 24).init(SPACES);
	private FixedLengthStringData wsaaNewagt = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 32).init(SPACES);
	private FixedLengthStringData wsaaServcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 40).init(SPACES);
	private FixedLengthStringData wsaaRnwlcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 41).init(SPACES);
	private FixedLengthStringData wsaaInalcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 42).init(SPACES);
	private FixedLengthStringData wsaaComind = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 43).init(SPACES);
	private FixedLengthStringData wsaaZrorcomm = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 44).init(SPACES);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 45).init(ZERO);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 47).init(ZERO);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 49).init(ZERO).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(159).isAPartOf(wsaaTransactionRec, 57, FILLER).init(SPACES);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(5, 8, wsaaReportags, 0);
		/* TABLES */
	private String t6688 = "T6688";
	private String t5679 = "T5679";
	private String th605 = "TH605";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String ptrnrec = "PTRNREC";
	private String macfrec = "MACFREC";
	private String macfflvrec = "MACFFLVREC";
	private String chdrlifrec = "CHDRLIFREC";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
	private Agtchgrec agtchgrec = new Agtchgrec();
	private Atmodrec atmodrec = new Atmodrec();
		/*Contract Header View by Agent*/
	private ChdragtTableDAM chdragtIO = new ChdragtTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agent Career History*/
	private MacfTableDAM macfIO = new MacfTableDAM();
		/*Reporting to (First Level)*/
	private MacfflvTableDAM macfflvIO = new MacfflvTableDAM();
		/*Agent Career History Inquiry*/
	private MacfinqTableDAM macfinqIO = new MacfinqTableDAM();
		/*Reporting to (2nd Level)*/
	private MacfslvTableDAM macfslvIO = new MacfslvTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private T5679rec t5679rec = new T5679rec();
	private T6688rec t6688rec = new T6688rec();
	private Th605rec th605rec = new Th605rec();
	private Varcom varcom = new Varcom();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		xxxxErrorProg,
		exit1009,
		nextr3810,
		exit3810,
		nextr3820,
		exit3820,
		nextr3100,
		exit3100,
		z110Nextr,
		z110Exit,
		z120Nextr,
		z120Exit,
		m001LevelCall,
		m001Next,
		m001Exit,
		m00xLevelCall,
		m00xNext,
		m00xExit,
		m00yLevelCall,
		m00yNext,
		m00yExit
	}

	public Pm507at() {
		super();
	}

public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaFlvlagt.set(SPACES);
		wsaaSlvlagt.set(SPACES);
		wsaaTransactionRec.set(atmodrec.transArea);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaBatckey.batcBatcactyr.set(wsaaBatcactyr);
		wsaaBatckey.batcBatcactmn.set(wsaaBatcactmn);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		m001UpdateLevel1();
	}

protected void exit0009()
	{
		exitProgram();
	}

protected void readT56791200()
	{
		start1200();
	}

protected void start1200()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void xxxxFatalError()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					xxxxFatalErrors();
				}
				case xxxxErrorProg: {
					xxxxErrorProg();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.xxxxErrorProg);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		try {
			initialise1001();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1001()
	{
		chdrlifIO.setChdrcoy(chdragtIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdragtIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		wsaaStatcode.set(chdrlifIO.getStatcode());
		wsaaAgentnum.set(chdrlifIO.getAgntnum());
		validateStatus1100();
		if (!validStatcode.isTrue()
		|| !validPstatcode.isTrue()) {
			goTo(GotoLabel.exit1009);
		}
		softLock6000();
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t6688);
		itemIO.setItemitem(wsaaStatcode);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(),varcom.oK))
		&& (isNE(itemIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t6688rec.t6688Rec.set(itemIO.getGenarea());
	}

protected void validateStatus1100()
	{
		/*START*/
		wsaaValidStatcode.set("N");
		wsaaValidPstatcode.set("N");
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
		|| validStatcode.isTrue()); wsaaIndex.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIndex.toInt()],chdrlifIO.getStatcode())) {
				wsaaValidStatcode.set("Y");
			}
		}
		if (validStatcode.isTrue()) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| validPstatcode.isTrue()); wsaaIndex.add(1)){
				if (isEQ(t5679rec.cnPremStat[wsaaIndex.toInt()],chdrlifIO.getPstatcode())) {
					wsaaValidPstatcode.set("Y");
				}
			}
		}
		/*EXIT*/
	}

protected void readTh6051200()
	{
		read1210();
	}

protected void read1210()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdragtIO.getChdrcoy());
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(chdragtIO.getChdrcoy());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		agtchgrec.statuz.set("****");
		agtchgrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		agtchgrec.chdrnum.set(chdrlifIO.getChdrnum());
		agtchgrec.life.set(SPACES);
		agtchgrec.coverage.set(SPACES);
		agtchgrec.rider.set(SPACES);
		agtchgrec.updateFlag.set(SPACES);
		agtchgrec.seqno.set(ZERO);
		agtchgrec.planSuffix.set(ZERO);
		agtchgrec.tranno.set(chdrlifIO.getTranno());
		agtchgrec.agntnumOld.set(wsaaAgentnum);
		wsaaNewagt.set(wsaaAgentnum);
		agtchgrec.agntnumNew.set(wsaaAgentnum);
		agtchgrec.initCommFlag.set(wsaaInalcom);
		agtchgrec.rnwlCommFlag.set(wsaaRnwlcom);
		agtchgrec.servCommFlag.set(wsaaServcom);
		agtchgrec.orCommFlag.set(wsaaZrorcomm);
		agtchgrec.language.set(atmodrec.language);
		agtchgrec.batckey.set(wsaaBatckey);
		agtchgrec.termid.set(wsaaTermid);
		agtchgrec.user.set(wsaaUser);
		agtchgrec.transactionDate.set(wsaaEffdate);
		agtchgrec.transactionTime.set(wsaaTransactionTime);
		callProgram(t6688rec.agtchgsub, agtchgrec.agentRec);
		if (isNE(agtchgrec.statuz,varcom.oK)) {
			syserrrec.params.set(agtchgrec.agentRec);
			xxxxFatalError();
		}
	}

protected void affectedAgent3800()
	{
		start3800();
	}

protected void start3800()
	{
		macfflvIO.setParams(SPACES);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaAgent);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setTranno(99999);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		macfflvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfflvIO.setFitKeysSearch("AGNTCOY", "ZRPTGA");

		while ( !(isEQ(macfflvIO.getStatuz(),varcom.endp))) {
			n1stLevel3810();
		}

		macfslvIO.setParams(SPACES);
		macfslvIO.setAgntcoy(atmodrec.company);
		macfslvIO.setZrptgb(wsaaAgent);
		macfslvIO.setAgntnum(SPACES);
		macfslvIO.setEffdate(varcom.vrcmMaxDate);
		macfslvIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		macfslvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfslvIO.setFitKeysSearch("AGNTCOY");
		while ( !(isEQ(macfslvIO.getStatuz(),varcom.endp))) {
			n2ndLevel3820();
		}

	}

protected void n1stLevel3810()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3810();
				}
				case nextr3810: {
					nextr3810();
				}
				case exit3810: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3810()
	{
		SmartFileCode.execute(appVars, macfflvIO);

		if (isNE(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfflvIO.getZrptga(),wsaaAgent)
		|| isNE(macfflvIO.getAgntcoy(),atmodrec.company)) {
			macfflvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3810);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);
		macfinqIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		//macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(),varcom.oK)
		&& isNE(macfinqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfinqIO.getAgntnum(),macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3810);
		}
		if (isNE(macfinqIO.getZrptga(),macfflvIO.getZrptga())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3810);
		}
		if (isNE(macfflvIO.getAgntnum(),wsaaFlvlagt)) {
			wsaaFlvlagt.set(macfflvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.nextr3810);
		}
		wsaaChdrnum.set(SPACES);
		chdragtIO.setParams(SPACES);
		chdragtIO.setAgntpfx("AG");
		chdragtIO.setAgntcoy(atmodrec.company);
		chdragtIO.setAgntnum(macfflvIO.getAgntnum());
		wsaaAgntnum.set(macfflvIO.getAgntnum());
		chdragtIO.setValidflag("1");
		chdragtIO.setChdrcoy(atmodrec.company);
		chdragtIO.setChdrnum(SPACES);
		chdragtIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		chdragtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdragtIO.setFitKeysSearch("AGNTPFX", "AGNTCOY","AGNTNUM","VALIDFLAG");

		while ( !(isEQ(chdragtIO.getStatuz(),varcom.endp))) {
			readChdragtio3100();
		}

	}

protected void nextr3810()
	{
		macfflvIO.setFunction(varcom.nextr);
	}

protected void n2ndLevel3820()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3820();
				}
				case nextr3820: {
					nextr3820();
				}
				case exit3820: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3820()
	{
		SmartFileCode.execute(appVars, macfslvIO);
		if (isNE(macfslvIO.getStatuz(),varcom.oK)
		&& isNE(macfslvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfslvIO.getStatuz());
			syserrrec.params.set(macfslvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfslvIO.getZrptgb(),wsaaAgent)
		|| isNE(macfslvIO.getAgntcoy(),atmodrec.company)) {
			macfslvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfslvIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3820);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);

		//performance improvement -- Anjali
		//macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		macfinqIO.setFunction(varcom.begn);

		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(),varcom.oK)
		&& isNE(macfinqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfinqIO.getAgntnum(),macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3820);
		}
		if (isNE(macfinqIO.getZrptgb(),macfflvIO.getZrptgb())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3820);
		}
		if (isNE(macfslvIO.getAgntnum(),wsaaSlvlagt)) {
			wsaaSlvlagt.set(macfslvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.nextr3820);
		}
		wsaaChdrnum.set(SPACES);
		chdragtIO.setParams(SPACES);
		chdragtIO.setAgntpfx("AG");
		chdragtIO.setAgntcoy(atmodrec.company);
		chdragtIO.setAgntnum(macfslvIO.getAgntnum());
		wsaaAgntnum.set(macfslvIO.getAgntnum());
		chdragtIO.setValidflag("1");
		chdragtIO.setChdrcoy(atmodrec.company);
		chdragtIO.setChdrnum(SPACES);
		chdragtIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		chdragtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdragtIO.setFitKeysSearch("AGNTPFX", "AGNTCOY","AGNTNUM","VALIDFLAG");

		while ( !(isEQ(chdragtIO.getStatuz(),varcom.endp))) {
			readChdragtio3100();
		}

	}

protected void nextr3820()
	{
		macfslvIO.setFunction(varcom.nextr);
	}

protected void readChdragtio3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3100();
				}
				case nextr3100: {
					nextr3100();
				}
				case exit3100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, chdragtIO);
		if (isNE(chdragtIO.getStatuz(),varcom.oK)
		&& isNE(chdragtIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdragtIO.getStatuz());
			syserrrec.params.set(chdragtIO.getParams());
			xxxxFatalError();
		}
		if (isNE(chdragtIO.getAgntpfx(),"AG")
		|| isNE(chdragtIO.getAgntcoy(),atmodrec.company)
		|| isNE(chdragtIO.getAgntnum(),wsaaAgntnum)
		|| isNE(chdragtIO.getValidflag(),"1")) {
			chdragtIO.setStatuz(varcom.endp);
		}
		if (isEQ(chdragtIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit3100);
		}
		if (isEQ(chdragtIO.getChdrnum(),wsaaChdrnum)) {
			goTo(GotoLabel.nextr3100);
		}
		wsaaChdrnum.set(chdragtIO.getChdrnum());
		initialise1000();
		if (!validStatcode.isTrue()
		|| !validPstatcode.isTrue()) {
			goTo(GotoLabel.nextr3100);
		}
		process2000();
		updateChdr4000();
		writePtrn5000();
		housekeepingTerminate3000();
	}

protected void nextr3100()
	{
		chdragtIO.setFunction(varcom.nextr);
	}

protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		releaseSoftlock3300();
		/*EXIT*/
	}

protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void z100UpdateAffectedAgent()
	{
		z100Start();
	}

protected void z100Start()
	{
		macfflvIO.setParams(SPACES);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaAgent);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		macfflvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfflvIO.setFitKeysSearch("AGNTCOY", "ZRPTGA");
		while ( !(isEQ(macfflvIO.getStatuz(),varcom.endp))) {
			z1101stLevel();
		}

		macfslvIO.setParams(SPACES);
		macfslvIO.setAgntcoy(atmodrec.company);
		macfslvIO.setZrptgb(wsaaAgent);
		macfslvIO.setAgntnum(SPACES);
		macfslvIO.setEffdate(varcom.vrcmMaxDate);

		//performance improvement -- Anjali
		macfslvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfslvIO.setFitKeysSearch("AGNTCOY", "ZRPTGB");

		macfslvIO.setFunction(varcom.begn);
		while ( !(isEQ(macfslvIO.getStatuz(),varcom.endp))) {
			z1202ndLevel();
		}

	}

protected void z1101stLevel()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					z110Start();
				}
				case z110Nextr: {
					z110Nextr();
				}
				case z110Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z110Start()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfflvIO.getZrptga(),wsaaAgent)
		|| isNE(macfflvIO.getAgntcoy(),atmodrec.company)) {
			macfflvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.z110Exit);
		}
		if (isNE(macfflvIO.getAgntnum(),wsaaFlvlagt)) {
			wsaaFlvlagt.set(macfflvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.z110Nextr);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);

		//performance improvement -- Anjali
		//macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		macfinqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(),varcom.oK)
		&& isNE(macfinqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfinqIO.getAgntnum(),macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.z110Nextr);
		}
		if (isNE(macfinqIO.getZrptga(),macfflvIO.getZrptga())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.z110Nextr);
		}
		aglfIO.setAgntcoy(atmodrec.company);
		aglfIO.setAgntnum(macfflvIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			xxxxFatalError();
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(atmodrec.company);
		agntIO.setAgntnum(macfflvIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			xxxxFatalError();
		}
		macfIO.setDataArea(SPACES);
		macfIO.setAgmvty("F");
		macfIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfIO.setAgntnum(macfflvIO.getAgntnum());
		macfIO.setMlagttyp(agntIO.getAgtype());
		macfIO.setEffdate(wsaaToday);
		macfIO.setCurrfrom(wsaaToday);
		macfIO.setCurrto(varcom.vrcmMaxDate);
		initialize(wsaaReportags);
		wsbbReportag.set(aglfIO.getReportag());
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag,SPACES))) {
			getReportag3100();
		}

		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setFunction(varcom.readh);
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfIO.getStatuz(),varcom.oK)) {
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				xxxxFatalError();
			}
			else {
				goTo(GotoLabel.z110Nextr);
			}
		}
		macfIO.setFunction(varcom.writr);
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			xxxxFatalError();
		}
	}

protected void z110Nextr()
	{
		macfflvIO.setFunction(varcom.nextr);
	}

protected void z1202ndLevel()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					z120Start();
				}
				case z120Nextr: {
					z120Nextr();
				}
				case z120Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z120Start()
	{
		SmartFileCode.execute(appVars, macfslvIO);
		if (isNE(macfslvIO.getStatuz(),varcom.oK)
		&& isNE(macfslvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfslvIO.getStatuz());
			syserrrec.params.set(macfslvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfslvIO.getZrptgb(),wsaaAgent)
		|| isNE(macfslvIO.getAgntcoy(),atmodrec.company)) {
			macfslvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfslvIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.z120Exit);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);
		macfinqIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		//macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(),varcom.oK)
		&& isNE(macfinqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			xxxxFatalError();
		}
		if (isNE(macfinqIO.getAgntnum(),macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.z120Nextr);
		}
		if (isNE(macfinqIO.getZrptgb(),macfflvIO.getZrptgb())
		|| isNE(macfinqIO.getAgntcoy(),macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.z120Nextr);
		}
		if (isNE(macfslvIO.getAgntnum(),wsaaSlvlagt)) {
			wsaaSlvlagt.set(macfslvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.z120Nextr);
		}
		aglfIO.setAgntcoy(atmodrec.company);
		aglfIO.setAgntnum(macfslvIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			xxxxFatalError();
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(atmodrec.company);
		agntIO.setAgntnum(macfslvIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			xxxxFatalError();
		}
		macfIO.setDataArea(SPACES);
		macfIO.setAgmvty("F");
		macfIO.setAgntcoy(macfslvIO.getAgntcoy());
		macfIO.setAgntnum(macfslvIO.getAgntnum());
		macfIO.setMlagttyp(agntIO.getAgtype());
		macfIO.setEffdate(wsaaToday);
		macfIO.setCurrfrom(wsaaToday);
		macfIO.setCurrto(varcom.vrcmMaxDate);
		initialize(wsaaReportags);
		wsaaIndex.set(1);
		wsbbReportag.set(aglfIO.getReportag());
		while ( !(isEQ(wsbbReportag,SPACES))) {
			getReportag3100();
		}

		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setFunction(varcom.readh);
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfIO.getStatuz(),varcom.oK)) {
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(),varcom.oK)
			&& isNE(macfIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				xxxxFatalError();
			}
			else {
				goTo(GotoLabel.z120Nextr);
			}
		}
		macfIO.setFunction(varcom.writr);
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			xxxxFatalError();
		}
	}

protected void z120Nextr()
	{
		macfslvIO.setFunction(varcom.nextr);
	}

protected void getReportag3100()
	{
		reportag3100();
	}

protected void reportag3100()
	{
		aglflnbIO.setAgntnum(wsbbReportag);
		aglflnbIO.setAgntcoy(atmodrec.company);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.oK)) {
			wsaaReportag[wsaaIndex.toInt()].set(aglflnbIO.getAgntnum());
			wsaaIndex.add(1);
			wsbbReportag.set(aglflnbIO.getReportag());
		}
	}

protected void updateChdr4000()
	{
		go4100();
	}

protected void go4100()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(chdragtIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdragtIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		compute(wsaaTranno, 0).set(add(1,chdrlifIO.getTranno()));
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(wsaaEffdate);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(wsaaComind,"Y")) {
			chdrlifIO.setAgntnum(wsaaNewagt);
		}
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(wsaaEffdate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writePtrn5000()
	{
		writePtrn5100();
	}

protected void writePtrn5100()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setTransactionDate(wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionTime);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);  
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void z200PersonalContracts()
	{
		/*Z200-START*/
		chdragtIO.setParams(SPACES);
		chdragtIO.setAgntpfx("AG");
		chdragtIO.setAgntcoy(atmodrec.company);
		chdragtIO.setAgntnum(wsaaAgent);
		wsaaAgntnum.set(wsaaAgent);
		chdragtIO.setValidflag("1");
		chdragtIO.setChdrcoy(atmodrec.company);
		chdragtIO.setChdrnum(SPACES);
		chdragtIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		chdragtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdragtIO.setFitKeysSearch("AGNTPFX", "AGNTCOY","VALIDFLAG");

		while ( !(isEQ(chdragtIO.getStatuz(),varcom.endp))) {
			readChdragtio3100();
		}

		/*Z200-EXIT*/
	}

protected void softLock6000()
	{
		start6000();
	}

protected void start6000()
	{
		sftlockrec.function.set("LOCK");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void m001UpdateLevel1()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					m001Reportag01();
				}
				case m001LevelCall: {
					m001LevelCall();
				}
				case m001Next: {
					m001Next();
				}
				case m001Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m001Reportag01()
	{
		macfflvIO.setDataKey(SPACES);
		wsaaAgnum.set(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaAgent);
		wsaaAgnum.set(wsaaAgent);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(macfflvrec);
	}

protected void m001LevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.endp)
		|| isNE(macfflvIO.getZrptga(),wsaaAgnum)
		|| isNE(macfflvIO.getAgntcoy(),atmodrec.company)) {
			goTo(GotoLabel.m001Exit);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getCurrto(),varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m001LevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)) {
			wsaaStore1Agnum.set(SPACES);
			wsaaStore1Zrptga.set(SPACES);
			wsaaStore1Effdate.set(varcom.vrcmMaxDate);
			wsaaStore1Zrptga.set(macfflvIO.getZrptga());
			wsaaStore1Agnum.set(macfflvIO.getAgntnum());
			wsaaStore1Effdate.set(macfflvIO.getEffdate());
			wsaaStore1Tranno.set(macfflvIO.getTranno());
			wsaaStore1Agmvty.set(macfflvIO.getAgmvty());
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(),atmodrec.company)
		&& isEQ(macfflvIO.getZrptga(),wsaaAgnum)
		&& isNE(macfflvIO.getZrptgb(),wsaaAgentnum)) {
			macfflvIO.setCurrto(wsaaToday);
			wsaaTranno.set(macfflvIO.getTranno());
			macfflvIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfflvIO);
			if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(macfflvIO.getStatuz());
				syserrrec.params.set(macfflvIO.getParams());
				xxxxFatalError();
			}
			macfflvIO.setAgmvty("F");
			wsaaTranno.add(1);
			macfflvIO.setTranno(wsaaTranno);
			macfflvIO.setFunction(varcom.writr);
			macfflvIO.setEffdate(wsaaToday);
			macfflvIO.setCurrfrom(wsaaToday);
			macfflvIO.setCurrto(varcom.vrcmMaxDate);
		}
		else {
			goTo(GotoLabel.m001Next);
		}
		wsbbReportag.set(wsaaAgent);
		if (isNE(wsaaNewagt,SPACES)) {
			wsbbReportag.set(wsaaNewagt);
		}
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag,SPACES))) {
			getReportag3100();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isNE(wsaaNewagt,SPACES)) {
			m0xyDownlineUpdate();
		}
		if (isEQ(macfflvIO.getMlagttyp(),"AG")) {
			goTo(GotoLabel.m001Next);
		}
		else {
			wsaaAgnum.set(macfflvIO.getAgntnum());
			wsaaZrptgb.set(macfflvIO.getZrptgb());
			wsaaZrptgc.set(macfflvIO.getZrptgc());
			wsaaZrptgd.set(macfflvIO.getZrptgd());
			m00xUpdateLevelX();
		}
	}

protected void m001Next()
	{
		wsaaTranno.set(ZERO);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m001LevelCall);
	}

protected void m00xUpdateLevelX()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					m00xReportag0x();
				}
				case m00xLevelCall: {
					m00xLevelCall();
				}
				case m00xNext: {
					m00xNext();
				}
				case m00xExit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m00xReportag0x()
	{
		macfflvIO.setDataKey(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaAgnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(macfflvrec);
	}

protected void m00xLevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.endp)
		|| isNE(macfflvIO.getZrptga(),wsaaAgnum)
		|| isNE(macfflvIO.getAgntcoy(),atmodrec.company)) {
			macfflvIO.setDataKey(SPACES);
			wsaaAgnum.set(SPACES);
			macfflvIO.setAgntcoy(atmodrec.company);
			macfflvIO.setZrptga(wsaaStore1Zrptga);
			wsaaAgnum.set(wsaaStore1Zrptga);
			macfflvIO.setAgntnum(wsaaStore1Agnum);
			macfflvIO.setEffdate(wsaaStore1Effdate);
			macfflvIO.setTranno(wsaaStore1Tranno);
			macfflvIO.setAgmvty(wsaaStore1Agmvty);
			macfflvIO.setFunction(varcom.begnh);
			macfflvIO.setFormat(macfflvrec);
			SmartFileCode.execute(appVars, macfflvIO);
			if (isNE(macfflvIO.getStatuz(),varcom.oK)
			&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(macfflvIO.getStatuz());
				syserrrec.params.set(macfflvIO.getParams());
				xxxxFatalError();
			}
			goTo(GotoLabel.m00xExit);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getCurrto(),varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m00xLevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)) {
			wsaaStorexAgnum.set(SPACES);
			wsaaStorexZrptga.set(SPACES);
			wsaaStorexEffdate.set(varcom.vrcmMaxDate);
			wsaaStorexZrptga.set(macfflvIO.getZrptga());
			wsaaStorexAgnum.set(macfflvIO.getAgntnum());
			wsaaStorexEffdate.set(macfflvIO.getEffdate());
			wsaaStorexTranno.set(macfflvIO.getTranno());
			wsaaStorexAgmvty.set(macfflvIO.getAgmvty());
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(),atmodrec.company)
		&& isEQ(macfflvIO.getZrptga(),wsaaAgnum)
		&& (isNE(macfflvIO.getZrptgb(),wsaaZrptgb)
		|| isNE(macfflvIO.getZrptgc(),wsaaZrptgc)
		|| isNE(macfflvIO.getZrptgd(),wsaaZrptgd))) {
			wsaaTranno.set(macfflvIO.getTranno());
			macfflvIO.setCurrto(wsaaToday);
			macfflvIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfflvIO);
			if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(macfflvIO.getStatuz());
				syserrrec.params.set(macfflvIO.getParams());
				xxxxFatalError();
			}
			macfflvIO.setAgmvty("F");
			wsaaTranno.add(1);
			macfflvIO.setTranno(wsaaTranno);
			macfflvIO.setFunction(varcom.writr);
			macfflvIO.setEffdate(wsaaToday);
			macfflvIO.setCurrfrom(wsaaToday);
			macfflvIO.setCurrto(varcom.vrcmMaxDate);
		}
		else {
			goTo(GotoLabel.m00xNext);
		}
		wsbbReportag.set(wsaaAgnum);
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag,SPACES))) {
			getReportag3100();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfflvIO.getMlagttyp(),"AG")) {
			goTo(GotoLabel.m00xNext);
		}
		else {
			wsaaAgnum.set(macfflvIO.getAgntnum());
			wsaaZrptgb.set(macfflvIO.getZrptgb());
			wsaaZrptgc.set(macfflvIO.getZrptgc());
			wsaaZrptgd.set(macfflvIO.getZrptgd());
			m00yUpdateLevelY();
		}
	}

protected void m00xNext()
	{
		macfflvIO.setDataArea(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaStorexZrptga);
		macfflvIO.setAgntnum(wsaaStorexAgnum);
		wsaaAgnum.set(wsaaStorexZrptga);
		macfflvIO.setEffdate(wsaaStorexEffdate);
		macfflvIO.setAgmvty(wsaaStorexAgmvty);
		macfflvIO.setTranno(wsaaStorexTranno);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m00xLevelCall);
	}

protected void m00yUpdateLevelY()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					m00yReportag0y();
				}
				case m00yLevelCall: {
					m00yLevelCall();
				}
				case m00yNext: {
					m00yNext();
				}
				case m00yExit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m00yReportag0y()
	{
		macfflvIO.setDataKey(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(atmodrec.company);
		macfflvIO.setZrptga(wsaaAgnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(macfflvrec);
	}

protected void m00yLevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.endp)
		|| isNE(macfflvIO.getAgntcoy(),atmodrec.company)
		|| isNE(macfflvIO.getZrptga(),wsaaAgnum)) {
			goTo(GotoLabel.m00yExit);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isNE(macfflvIO.getCurrto(),varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m00yLevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(),varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(),atmodrec.company)
		&& isEQ(macfflvIO.getZrptga(),wsaaAgnum)
		&& (isNE(macfflvIO.getZrptgb(),wsaaZrptgb)
		|| isNE(macfflvIO.getZrptgc(),wsaaZrptgc)
		|| isNE(macfflvIO.getZrptgd(),wsaaZrptgd))) {
			wsaaTranno.set(macfflvIO.getTranno());
			macfflvIO.setCurrto(wsaaToday);
			macfflvIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfflvIO);
			if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(macfflvIO.getStatuz());
				syserrrec.params.set(macfflvIO.getParams());
				xxxxFatalError();
			}
			macfflvIO.setAgmvty("F");
			wsaaTranno.add(1);
			macfflvIO.setTranno(wsaaTranno);
			macfflvIO.setFunction(varcom.writr);
			macfflvIO.setEffdate(wsaaToday);
			macfflvIO.setCurrfrom(wsaaToday);
			macfflvIO.setCurrto(varcom.vrcmMaxDate);
		}
		else {
			goTo(GotoLabel.m00yNext);
		}
		wsbbReportag.set(wsaaAgnum);
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag,SPACES))) {
			getReportag3100();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
	}

protected void m00yNext()
	{
		wsaaTranno.set(ZERO);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m00yLevelCall);
	}

protected void m0xyDownlineUpdate()
	{
		m0xyUpd();
	}

protected void m0xyUpd()
	{
		macfIO.setDataKey(SPACES);
		macfIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfIO.setAgntnum(macfflvIO.getAgntnum());
		macfIO.setAgmvty(macfflvIO.getAgmvty());
		macfIO.setTranno(macfflvIO.getTranno());
		macfIO.setEffdate(macfflvIO.getEffdate());
		macfIO.setFunction(varcom.readh);
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
		macfIO.setZrptga(wsaaNewagt);
		macfIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfflvIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			xxxxFatalError();
		}
	}
}
