/*
 * File: Bm505.java
 * Date: 29 August 2009 21:47:42
 * Author: Quipoz Limited
 * 
 * Class transformed from BM505.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.MacfeffTableDAM;
import com.csc.life.agents.procedures.Rlagpro;
import com.csc.life.agents.recordstructures.Pm509par;
import com.csc.life.agents.recordstructures.Rlagprorec;
import com.csc.life.agents.reports.Rm505Report;
import com.csc.life.agents.tablestructures.Tm604rec;
import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrncpy;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Headings:
*    Sequence No.
*    Agent Code
*    Agent Name
*    Branch Code
*    AM Code
*    AM Name
*    Cumulative No. of Policies
*    Cumulative FYP for Direct
*                   for Group
*    Cumulative direct agent
*    Persistency percentage
*    Agent status remarks
*
* Business Requirement and Analysis
* ---------------------------------
* The business requirement here is to produce repoprts for awards/
* contests purposes which are based on 5 Key Result Areas (5KRA):
*    1. FYP (measured by individual & group, month-to-date &
*       year-to-date.
*    2. Cases (number of policies sold by individual & group).
*    3. Manpower (new recruits).
*    4. Activity Ratio (Active Agents/In-force Agents MTD & average
*       YTD).
*
*
* Report Description
* ------------------
*    Quality Business Award.
*
* Criteria ( from table TM604 )
* -----------------------------
*    Min Group FYP and direct FYP
*    Min cases
*    Min 12-month persistency percentage
*    Min number of direct recruits with min FYP and min pols per
*      recruit
*
* Format/Issues
* -------------
*    .  Select all in-force agents.
*    .  Sort by Group Cumulative FYP.
*    .  FYP to include all in-force contracts within date range.
*    .  Report requied on monthly basis, with cumulative figures for
*       the year between 26th Dec to 25th Dec.
*    .  See RM505 *PRTF for layout.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Bm505 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rm505Report printerFile = new Rm505Report();
	private SortFileDAM regionSortFile = new SortFileDAM("LU00");
	private FixedLengthStringData printerRec = new FixedLengthStringData(150);

	private FixedLengthStringData regionSortRec = new FixedLengthStringData(135);
	private FixedLengthStringData sortAgntnum01 = new FixedLengthStringData(8).isAPartOf(regionSortRec, 0);
	private FixedLengthStringData sortAgntname01 = new FixedLengthStringData(30).isAPartOf(regionSortRec, 8);
	private FixedLengthStringData sortAgntbr = new FixedLengthStringData(2).isAPartOf(regionSortRec, 38);
	private FixedLengthStringData sortAgntnum02 = new FixedLengthStringData(8).isAPartOf(regionSortRec, 40);
	private FixedLengthStringData sortAgntname02 = new FixedLengthStringData(30).isAPartOf(regionSortRec, 48);
	private ZonedDecimalData sortNumofmbr = new ZonedDecimalData(7, 0).isAPartOf(regionSortRec, 78).setUnsigned();
	private ZonedDecimalData sortPrem01 = new ZonedDecimalData(15, 2).isAPartOf(regionSortRec, 85);
	private ZonedDecimalData sortPrem02 = new ZonedDecimalData(15, 2).isAPartOf(regionSortRec, 100);
	private FixedLengthStringData sortStatuz = new FixedLengthStringData(4).isAPartOf(regionSortRec, 115);
	private ZonedDecimalData sortDatefrm = new ZonedDecimalData(8, 0).isAPartOf(regionSortRec, 119).setUnsigned();
	private ZonedDecimalData sortDateto = new ZonedDecimalData(8, 0).isAPartOf(regionSortRec, 127).setUnsigned();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BM505");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private int wsaaTextfieldLen = 30;
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(37);
	private String wsaaEofSort = "";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgntname = new FixedLengthStringData(30);
	private String wsaaAmMore = "Y";
	private FixedLengthStringData wsaaTm604Item = new FixedLengthStringData(7);
	private ZonedDecimalData wsaaAgIndex = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaAgAgntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAppRec = new FixedLengthStringData(37);
	private FixedLengthStringData wsaaAppAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAppRec, 0);
	private FixedLengthStringData wsaaAppProcess = new FixedLengthStringData(1).isAPartOf(wsaaAppRec, 20);
	private ZonedDecimalData wsaaAppDatefrm = new ZonedDecimalData(8, 0).isAPartOf(wsaaAppRec, 21).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaAppDatefrm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaAppDateto = new ZonedDecimalData(8, 0).isAPartOf(wsaaAppRec, 29).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaAppDateto, 0, FILLER_REDEFINE);

	private FixedLengthStringData wsaaMacfRec = new FixedLengthStringData(40);
	private FixedLengthStringData wsaaMacfProcess = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 0);
	private FixedLengthStringData wsaaMacfPromoted = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 1);
	private FixedLengthStringData wsaaMacfDemoted = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 2);
	private FixedLengthStringData wsaaMacfInforce = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 3);
	private ZonedDecimalData wsaaMacfDateto = new ZonedDecimalData(8, 0).isAPartOf(wsaaMacfRec, 4).setUnsigned();
	private ZonedDecimalData wsaaMacfDatefrm = new ZonedDecimalData(8, 0).isAPartOf(wsaaMacfRec, 12).setUnsigned();
	private ZonedDecimalData wsaaMacfEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaMacfRec, 20).setUnsigned();
	private FixedLengthStringData wsaaMacfMlagttyp = new FixedLengthStringData(2).isAPartOf(wsaaMacfRec, 28);
	private FixedLengthStringData wsaaMacfAgmvty = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 30);
	private FixedLengthStringData wsaaMacfAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaMacfRec, 31);
	private FixedLengthStringData wsaaMacfAgntcoy = new FixedLengthStringData(1).isAPartOf(wsaaMacfRec, 39);
		/* WSAA-AGNT-STATUZ */
	private String inForce = "INF";
	private String terminated = "TRM";
	private String wsaaTotalDesc = "TOTAL";
		/* WSAA-TOTAL-REC */
	private ZonedDecimalData wsaaTotalNumofmbr = new ZonedDecimalData(7, 0).init(ZERO);
	private ZonedDecimalData wsaaTotalPrem01 = new ZonedDecimalData(15, 2).init(ZERO);
	private ZonedDecimalData wsaaTotalPrem02 = new ZonedDecimalData(15, 2).init(ZERO);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLen = new PackedDecimalData(15, 5).init(100).setUnsigned();
	private String descrec = "DESCREC";
	private String aglfrec = "AGLFREC";
	private String agntlagrec = "AGNTLAGREC";
	private String cltsrec = "CLTSREC";
	private String macfeffrec = "MACFEFFREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
	private String tm604 = "TM604";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rm505H01 = new FixedLengthStringData(143);
	private FixedLengthStringData rm505h01O = new FixedLengthStringData(143).isAPartOf(rm505H01, 0);
	private FixedLengthStringData rh01Rpthead = new FixedLengthStringData(50).isAPartOf(rm505h01O, 0);
	private FixedLengthStringData rh01Datefrm = new FixedLengthStringData(10).isAPartOf(rm505h01O, 50);
	private FixedLengthStringData rh01Dateto = new FixedLengthStringData(10).isAPartOf(rm505h01O, 60);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rm505h01O, 70);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rm505h01O, 71);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rm505h01O, 101);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rm505h01O, 111);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rm505h01O, 113);

	private FixedLengthStringData rm505D01 = new FixedLengthStringData(119);
	private FixedLengthStringData rm505d01O = new FixedLengthStringData(119).isAPartOf(rm505D01, 0);
	private ZonedDecimalData numpols = new ZonedDecimalData(4, 0).isAPartOf(rm505d01O, 0);
	private FixedLengthStringData agntnum01 = new FixedLengthStringData(8).isAPartOf(rm505d01O, 4);
	private FixedLengthStringData agntname01 = new FixedLengthStringData(30).isAPartOf(rm505d01O, 12);
	private FixedLengthStringData agntbr = new FixedLengthStringData(2).isAPartOf(rm505d01O, 42);
	private FixedLengthStringData agntnum02 = new FixedLengthStringData(8).isAPartOf(rm505d01O, 44);
	private FixedLengthStringData agntname02 = new FixedLengthStringData(30).isAPartOf(rm505d01O, 52);
	private ZonedDecimalData numofmbr = new ZonedDecimalData(7, 0).isAPartOf(rm505d01O, 82);
	private ZonedDecimalData premium01 = new ZonedDecimalData(13, 2).isAPartOf(rm505d01O, 89);
	private ZonedDecimalData premium02 = new ZonedDecimalData(13, 2).isAPartOf(rm505d01O, 102);
	private FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rm505d01O, 115);

		/* RM505-D02 */
	private FixedLengthStringData rm505d02O = new FixedLengthStringData(115);
	private FixedLengthStringData agntnum011 = new FixedLengthStringData(8).isAPartOf(rm505d02O, 0);
	private FixedLengthStringData agntname011 = new FixedLengthStringData(30).isAPartOf(rm505d02O, 8);
	private FixedLengthStringData agntbr1 = new FixedLengthStringData(2).isAPartOf(rm505d02O, 38);
	private FixedLengthStringData agntnum021 = new FixedLengthStringData(8).isAPartOf(rm505d02O, 40);
	private FixedLengthStringData agntname021 = new FixedLengthStringData(30).isAPartOf(rm505d02O, 48);
	private ZonedDecimalData numofmbr1 = new ZonedDecimalData(7, 0).isAPartOf(rm505d02O, 78);
	private ZonedDecimalData premium011 = new ZonedDecimalData(13, 2).isAPartOf(rm505d02O, 85);
	private ZonedDecimalData premium021 = new ZonedDecimalData(13, 2).isAPartOf(rm505d02O, 98);
	private FixedLengthStringData statuz1 = new FixedLengthStringData(4).isAPartOf(rm505d02O, 111);

	private FixedLengthStringData rm505S01 = new FixedLengthStringData(57);
	private FixedLengthStringData rm505s01O = new FixedLengthStringData(57).isAPartOf(rm505S01, 0);
	private FixedLengthStringData statdesc = new FixedLengthStringData(24).isAPartOf(rm505s01O, 0);
	private ZonedDecimalData numofmbr2 = new ZonedDecimalData(7, 0).isAPartOf(rm505s01O, 24);
	private ZonedDecimalData premium012 = new ZonedDecimalData(13, 2).isAPartOf(rm505s01O, 31);
	private ZonedDecimalData premium022 = new ZonedDecimalData(13, 2).isAPartOf(rm505s01O, 44);

		/* RM505-S02 */
	private FixedLengthStringData rm505s02O = new FixedLengthStringData(33);
	private ZonedDecimalData numofmbr3 = new ZonedDecimalData(7, 0).isAPartOf(rm505s02O, 0);
	private ZonedDecimalData premium013 = new ZonedDecimalData(13, 2).isAPartOf(rm505s02O, 7);
	private ZonedDecimalData premium023 = new ZonedDecimalData(13, 2).isAPartOf(rm505s02O, 20);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Dbcstrncpy dbcstrncpy = new Dbcstrncpy();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agent Career History by Effective Date*/
	private MacfeffTableDAM macfeffIO = new MacfeffTableDAM();
	private Pm509par pm509par = new Pm509par();
	private Rlagprorec rlagprorec = new Rlagprorec();
	private Tm604rec tm604rec = new Tm604rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		nextr2200, 
		exit2200, 
		performMacfeff2300, 
		exit2300, 
		exit2400, 
		a2500Exit, 
		exit3100
	}

	public Bm505() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
		setUpHeadingFromTable1050();
	}

protected void initialise1010()
	{
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("CHGPRTF FILE(RM505) PAGESIZE(66 198) LPI(6) CPI(15)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLen);
		printerFile.openOutput();
		pm509par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaCount.set(ZERO);
		wsaaEofSort = "N";
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bsprIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bsprIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(pm509par.datefrm);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Datefrm.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(pm509par.dateto);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Dateto.set(datcon1rec.extDate);
	}

protected void setUpHeadingFromTable1050()
	{
		initialize(tm604rec.tm604Rec);
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(tm604);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(bprdIO.getAuthCode().toString());
		stringVariable1.append(pm509par.mlagttyp.toString());
		wsaaTm604Item.setLeft(stringVariable1.toString());
		itdmIO.setItemitem(wsaaTm604Item);
		itdmIO.setItmfrm(pm509par.datefrm);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),tm604)
		|| isNE(itdmIO.getItemitem(),wsaaTm604Item)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(tm604);
			itdmIO.setItemitem(wsaaTm604Item);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.params.set(varcom.mrnf);
			fatalError600();
		}
		tm604rec.tm604Rec.set(itdmIO.getGenarea());
		rh01Rpthead.set(tm604rec.rpthead);
	}

protected void callAglf1200()
	{
		/*CALL*/
		aglfIO.setAgntcoy(bsprIO.getCompany());
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)
		&& isNE(aglfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callAgntlag1300()
	{
		/*READR-AGENT-FILE*/
		agntlagIO.setAgntcoy(bsprIO.getCompany());
		agntlagIO.setFormat(agntlagrec);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClts1400()
	{
		readClientFile1410();
	}

protected void readClientFile1410()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
	}

protected void callMacfeff1600()
	{
		/*READ-MACFEFF*/
		macfeffIO.setFormat(macfeffrec);
		SmartFileCode.execute(appVars, macfeffIO);
		if (isNE(macfeffIO.getStatuz(),varcom.oK)
		&& isNE(macfeffIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfeffIO.getStatuz());
			syserrrec.params.set(macfeffIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		wsspEdterror.set(varcom.oK);
		regionSortFile.openOutput();
		readAglf2100();
		regionSortFile.close();
		FileSort fs1 = new FileSort(regionSortFile);
		fs1.addSortKey(sortPrem02, false);
		fs1.addSortKey(sortPrem01, false);
		fs1.sort();
		regionSortFile.openInput();
		printReport3100();
		regionSortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void readAglf2100()
	{
		/*START*/
		initialize(wsaaAppRec);
		wsaaAgIndex.set(ZERO);
		aglfIO.setAgntcoy(bsprIO.getCompany());
		aglfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		aglfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		aglfIO.setFitKeysSearch("AGNTCOY");
		while ( !(isEQ(aglfIO.getStatuz(),varcom.endp))) {
			callAglfRoutine2200();
		}
		
		/*EXIT*/
	}

protected void callAglfRoutine2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
					validate2200();
				}
				case nextr2200: {
					nextr2200();
				}
				case exit2200: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		callAglf1200();
		if (isEQ(aglfIO.getStatuz(),varcom.endp)
		|| isNE(aglfIO.getAgntcoy(),bsprIO.getCompany())) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2200);
		}
		initialize(regionSortRec);
		wsspEdterror.set(varcom.oK);
		wsaaAgIndex.add(1);
		if (isEQ(wsaaAgIndex,1)) {
			wsaaAgAgntnum.set(aglfIO.getAgntnum());
		}
		else {
			if (isNE(aglfIO.getAgntnum(),wsaaAgAgntnum)) {
				wsaaAgIndex.set(1);
				wsaaAgAgntnum.set(aglfIO.getAgntnum());
			}
		}
	}

protected void validate2200()
	{
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		callAgntlag1300();
		if (isEQ(agntlagIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.nextr2200);
		}
		initialize(wsaaMacfRec);
		validateMacfeff2300();
		wsaaAppAgntnum.set(aglfIO.getAgntnum());
		wsaaAppProcess.set(wsaaMacfProcess);
		wsaaAppDatefrm.set(wsaaMacfDatefrm);
		wsaaAppDateto.set(wsaaMacfDateto);
		if (isEQ(wsaaAppProcess,"Y")
		&& isLTE(wsaaAppDatefrm,wsaaAppDateto)) {
			processAgnt2400();
		}
	}

protected void nextr2200()
	{
		aglfIO.setAgntnum(wsaaAgAgntnum);
		aglfIO.setFunction(varcom.begn);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,wsaaAgIndex)); wsaaIndex.add(1)){
			callAglf1200();
			aglfIO.setFunction(varcom.nextr);
		}
		aglfIO.setFunction(varcom.nextr);
	}

protected void validateMacfeff2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readMacfeff2300();
				}
				case performMacfeff2300: {
					performMacfeff2300();
					processDateRange2300();
					validateAgmvty2300();
					assignToWsaa2300();
				}
				case exit2300: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readMacfeff2300()
	{
		macfeffIO.setDataKey(SPACES);
		macfeffIO.setAgntcoy(aglfIO.getAgntcoy());
		macfeffIO.setAgntnum(aglfIO.getAgntnum());
		macfeffIO.setEffdate(varcom.maxdate);
		macfeffIO.setFunction(varcom.begn);
		wsaaMacfDatefrm.set(pm509par.datefrm);
		wsaaMacfDateto.set(pm509par.dateto);
		wsaaMacfProcess.set(SPACES);
		wsaaMacfInforce.set(SPACES);
	}

protected void performMacfeff2300()
	{
		callMacfeff1600();
	}

protected void processDateRange2300()
	{
		if (isEQ(wsaaMacfProcess,"N")) {
			goTo(GotoLabel.exit2300);
		}
		if (isEQ(macfeffIO.getStatuz(),varcom.endp)
		|| isNE(macfeffIO.getAgntnum(),aglfIO.getAgntnum())
		|| isNE(macfeffIO.getAgntcoy(),aglfIO.getAgntcoy())) {
			if (isEQ(wsaaMacfAgntnum,aglfIO.getAgntnum())
			&& isEQ(wsaaMacfAgntcoy,aglfIO.getAgntcoy())
			&& isEQ(wsaaMacfInforce,"Y")) {
				if (isEQ(aglfIO.getDtetrm(),varcom.vrcmMaxDate)
				|| isGT(aglfIO.getDtetrm(),pm509par.dateto)) {
					wsaaMacfDateto.set(pm509par.dateto);
				}
				else {
					if (isGTE(aglfIO.getDtetrm(),pm509par.datefrm)) {
						wsaaMacfDateto.set(aglfIO.getDtetrm());
					}
					else {
						wsaaMacfProcess.set("N");
					}
				}
				if (isEQ(wsaaMacfPromoted,"Y")
				&& isEQ(agntlagIO.getAgtype(),pm509par.mlagttyp)) {
					wsaaMacfProcess.set("Y");
					if (isLT(wsaaMacfEffdate,pm509par.datefrm)) {
						wsaaMacfDatefrm.set(pm509par.datefrm);
					}
					else {
						wsaaMacfDateto.set(wsaaMacfEffdate);
					}
				}
				if (isEQ(wsaaMacfDemoted,"Y")
				&& isEQ(agntlagIO.getAgtype(),pm509par.mlagttyp)) {
					wsaaMacfProcess.set("Y");
					if (isLT(wsaaMacfEffdate,pm509par.datefrm)) {
						wsaaMacfDatefrm.set(pm509par.datefrm);
					}
					else {
						wsaaMacfDateto.set(wsaaMacfEffdate);
					}
				}
			}
			goTo(GotoLabel.exit2300);
		}
		if (isEQ(wsaaMacfPromoted,"Y")
		&& isEQ(macfeffIO.getMlagttyp(),pm509par.mlagttyp)) {
			wsaaMacfProcess.set("Y");
			wsaaMacfPromoted.set(SPACES);
			if (isLT(wsaaMacfEffdate,pm509par.datefrm)) {
				wsaaMacfDatefrm.set(pm509par.datefrm);
			}
			else {
				wsaaMacfDateto.set(wsaaMacfEffdate);
			}
		}
		if (isEQ(wsaaMacfDemoted,"Y")
		&& isEQ(macfeffIO.getMlagttyp(),pm509par.mlagttyp)) {
			wsaaMacfProcess.set("Y");
			wsaaMacfDemoted.set(SPACES);
			if (isLT(wsaaMacfEffdate,pm509par.datefrm)) {
				wsaaMacfDatefrm.set(pm509par.datefrm);
			}
			else {
				wsaaMacfDateto.set(wsaaMacfEffdate);
			}
			if (isGT(macfeffIO.getEffdate(),pm509par.dateto)) {
				macfeffIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit2300);
			}
		}
	}

protected void validateAgmvty2300()
	{
		wsaaMacfPromoted.set(SPACES);
		if (isEQ(macfeffIO.getAgmvty(),"P")){
			wsaaMacfInforce.set("Y");
			if (isLT(macfeffIO.getEffdate(),pm509par.dateto)) {
				wsaaMacfPromoted.set("Y");
			}
		}
		else if (isEQ(macfeffIO.getAgmvty(),"D")){
			wsaaMacfInforce.set("Y");
			wsaaMacfDemoted.set("Y");
			if (isGTE(macfeffIO.getEffdate(),pm509par.datefrm)
			&& isLTE(macfeffIO.getEffdate(),pm509par.dateto)) {
				wsaaMacfProcess.set("N");
			}
		}
		else if (isEQ(macfeffIO.getAgmvty(),"T")){
			wsaaMacfInforce.set("N");
			if (isGTE(macfeffIO.getEffdate(),pm509par.dateto)) {
				wsaaMacfDateto.set(pm509par.dateto);
			}
			else {
				if (isGTE(macfeffIO.getEffdate(),pm509par.datefrm)) {
					wsaaMacfDateto.set(macfeffIO.getEffdate());
				}
				else {
					wsaaMacfDateto.set(pm509par.datefrm);
				}
			}
		}
		else if (isEQ(macfeffIO.getAgmvty(),"A")
		|| isEQ(macfeffIO.getAgmvty(),"R")){
			wsaaMacfInforce.set("Y");
			if (isEQ(wsaaMacfAgmvty,"T")) {
				wsaaMacfDateto.set(pm509par.dateto);
			}
			if (isEQ(macfeffIO.getMlagttyp(),pm509par.mlagttyp)) {
				if (isGTE(macfeffIO.getEffdate(),pm509par.datefrm)
				&& isLTE(macfeffIO.getEffdate(),pm509par.dateto)) {
					wsaaMacfDatefrm.set(macfeffIO.getEffdate());
					wsaaMacfProcess.set("Y");
				}
				else {
					if (isLT(macfeffIO.getEffdate(),pm509par.datefrm)) {
						wsaaMacfDatefrm.set(pm509par.datefrm);
						wsaaMacfProcess.set("Y");
					}
				}
			}
		}
	}

protected void assignToWsaa2300()
	{
		wsaaMacfEffdate.set(macfeffIO.getEffdate());
		wsaaMacfAgmvty.set(macfeffIO.getAgmvty());
		wsaaMacfAgntnum.set(macfeffIO.getAgntnum());
		wsaaMacfAgntcoy.set(macfeffIO.getAgntcoy());
		/*NEXTR*/
		macfeffIO.setFunction(varcom.nextr);
		goTo(GotoLabel.performMacfeff2300);
	}

protected void processAgnt2400()
	{
		try {
			start2400();
			release2400();
		}
		catch (GOTOException e){
		}
	}

protected void start2400()
	{
		sortAgntnum01.set(wsaaAppAgntnum);
		callClts1400();
		sortAgntname01.set(wsspLongconfname);
		sortAgntbr.set(agntlagIO.getAgntbr());
		if (isNE(aglfIO.getDtetrm(),varcom.vrcmMaxDate)) {
			sortStatuz.set(terminated);
		}
		else {
			sortStatuz.set(inForce);
		}
		sortDatefrm.set(wsaaAppDatefrm);
		sortDateto.set(wsaaAppDateto);
		wsaaAmMore = "Y";
		wsaaAgntnum.set(SPACES);
		wsaaAgntname.set(SPACES);
		while ( !(isEQ(wsaaAmMore,"N"))) {
			a2500LoadAm();
		}
		
		sortAgntnum02.set(wsaaAgntnum);
		sortAgntname02.set(wsaaAgntname);
		initialize(rlagprorec.rec);
		rlagprorec.agntcoy.set(aglfIO.getAgntcoy());
		rlagprorec.agntnum.set(sortAgntnum01);
		rlagprorec.mlagttyp.set(pm509par.mlagttyp);
		rlagprorec.datefrm.set(wsaaAppDatefrm);
		rlagprorec.dateto.set(wsaaAppDateto);
		rlagprorec.acctMnthyer.set(SPACES);
		rlagprorec.fillerOut.fill("Y");
		callProgram(Rlagpro.class, rlagprorec.rec);
		if (isNE(rlagprorec.statuz,varcom.oK)) {
			syserrrec.statuz.set(rlagprorec.statuz);
			syserrrec.params.set(rlagprorec.rec);
			fatalError600();
		}
		sortNumofmbr.add(rlagprorec.numofmbr);
		sortPrem01.add(rlagprorec.mldirpp01);
		if (isEQ(pm509par.mlagttyp,"AG")) {
			sortPrem02.add(rlagprorec.mlperpp01);
		}
		else {
			sortPrem02.add(rlagprorec.mlgrppp01);
		}
		if (isLT(sortNumofmbr,tm604rec.numofmbr)
		|| isLT(sortPrem01,tm604rec.prem02)
		|| isLT(sortPrem02,tm604rec.prem01)) {
			goTo(GotoLabel.exit2400);
		}
	}

protected void release2400()
	{
		regionSortFile.write(regionSortRec);
	}

protected void a2500LoadAm()
	{
		try {
			a2500Read();
			a2500ReportToAgentDetails();
		}
		catch (GOTOException e){
		}
	}

protected void a2500Read()
	{
		if (isEQ(aglfIO.getReportag(),SPACES)) {
			wsaaAmMore = "N";
			goTo(GotoLabel.a2500Exit);
		}
		wsaaAgntnum.set(aglfIO.getReportag());
		agntlagIO.setAgntnum(aglfIO.getReportag());
		callAgntlag1300();
		if (isEQ(agntlagIO.getStatuz(),varcom.mrnf)) {
			wsaaAmMore = "N";
			goTo(GotoLabel.a2500Exit);
		}
		callClts1400();
		wsaaAgntname.set(wsspLongconfname);
	}

protected void a2500ReportToAgentDetails()
	{
		aglfIO.setAgntnum(aglfIO.getReportag());
		aglfIO.setFunction(varcom.readr);
		callAglf1200();
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void printReport3100()
	{
		try {
			start3100();
		}
		catch (GOTOException e){
		}
	}

protected void start3100()
	{
		wsaaEofSort = "N";
		wsaaCount.set(ZERO);
		regionSortFile.read(regionSortRec);
		if (regionSortFile.isAtEnd()) {
			wsaaEofSort = "Y";
			goTo(GotoLabel.exit3100);
		}
		while ( !(isEQ(wsaaEofSort,"Y"))) {
			processReport3200();
		}
		
		printSummary3500();
	}

protected void processReport3200()
	{
		/*START*/
		printHeader3300();
		printDetail3400();
		regionSortFile.read(regionSortRec);
		if (regionSortFile.isAtEnd()) {
			wsaaEofSort = "Y";
		}
		/*EXIT*/
	}

protected void printHeader3300()
	{
		/*START*/
		if (newPageReq.isTrue()) {
			printerFile.printRm505h01(rm505H01, indicArea);
			wsaaOverflow.set("N");
		}
		/*EXIT*/
	}

protected void printDetail3400()
	{
		start3400();
	}

protected void start3400()
	{
		wsaaCount.add(1);
		numpols.set(wsaaCount);
		agntnum01.set(sortAgntnum01);
		dbcstrncpy.dbcsInputString.set(sortAgntname01);
		dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
		dbcstrncpy.dbcsStatuz.set(SPACES);
		dbcsTrnc(dbcstrncpy.rec);
		if (isNE(dbcstrncpy.dbcsStatuz,"****")) {
			dbcstrncpy.dbcsOutputString.set(SPACES);
		}
		agntname01.set(dbcstrncpy.dbcsOutputString);
		agntbr.set(sortAgntbr);
		agntnum02.set(sortAgntnum02);
		dbcstrncpy.dbcsInputString.set(sortAgntname02);
		dbcstrncpy.dbcsOutputLength.set(wsaaTextfieldLen);
		dbcstrncpy.dbcsStatuz.set(SPACES);
		dbcsTrnc(dbcstrncpy.rec);
		if (isNE(dbcstrncpy.dbcsStatuz,"****")) {
			dbcstrncpy.dbcsOutputString.set(SPACES);
		}
		agntname02.set(dbcstrncpy.dbcsOutputString);
		numofmbr.set(sortNumofmbr);
		premium01.set(sortPrem01);
		premium02.set(sortPrem02);
		statuz.set(sortStatuz);
		calTotal3420();
		printerFile.printRm505d01(rm505D01, indicArea);
	}

protected void calTotal3420()
	{
		/*START*/
		wsaaTotalNumofmbr.add(sortNumofmbr);
		wsaaTotalPrem01.add(sortPrem01);
		wsaaTotalPrem02.add(sortPrem02);
		/*EXIT*/
	}

protected void printSummary3500()
	{
		/*START*/
		statdesc.set(wsaaTotalDesc);
		numofmbr2.set(wsaaTotalNumofmbr);
		premium012.set(wsaaTotalPrem01);
		premium022.set(wsaaTotalPrem02);
		printerFile.printRm505s01(rm505S01, indicArea);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
