package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.AgslpfDAO;
import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgslpfDAOImpl extends BaseDAOImpl<Agslpf>implements AgslpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgslpfDAOImpl.class);
	
	@Override
	public void insertAgslpfRecord(List<Agslpf> agslist) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO AGSLPF (CLNTNUM,AGNTNUM,AGTYPE,TLAGLICNO,AGNCYSALICDE,FROMDATE,TODATE,USRPRF,JOBNM,DATIME)");
		sql.append(" VALUES(?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		try {
			for (Agslpf agslpf : agslist) {
				int i = 1;
				
				ps.setString(i++, agslpf.getClntsel());
				ps.setString(i++, agslpf.getAgnum());
				ps.setString(i++, agslpf.getAgtype());
				ps.setString(i++, agslpf.getTlaglicno());
				ps.setString(i++, agslpf.getSalelicensetype());
				ps.setInt(i++, agslpf.getFrmdate());
				ps.setInt(i++, agslpf.getTodate());
				ps.setString(i++, getUsrprf());
				ps.setString(i++, getJobnm());
				ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertAgslpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public List<Agslpf> getAgslpfByAgentNum(String agntNumber) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM AGSLPF WHERE AGNTNUM=?");
        PreparedStatement ps = getPrepareStatement(stringBuilder.toString());
        ResultSet rs = null;
        List<Agslpf> agslpfList = new ArrayList<>();
        try {
        	ps.setString(1, agntNumber);
            rs = executeQuery(ps);
            while (rs.next()) {
            	Agslpf agslpf = new Agslpf();
            	agslpf.setClntsel(rs.getString(2));
            	agslpf.setAgnum(rs.getString(3));
            	agslpf.setAgtype(rs.getString(4));
            	agslpf.setTlaglicno(rs.getString(5));
            	agslpf.setSalelicensetype(rs.getString(6));
            	agslpf.setFrmdate(rs.getInt(7));
            	agslpf.setTodate(rs.getInt(8));
            	agslpfList.add(agslpf);
                }
            } catch (SQLException e) {
            LOGGER.error("getAgslRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return agslpfList;
	}

	@Override
	public void deleteAgslpfRecord(List<String> delist) {
		StringBuilder sqlDelete = new StringBuilder();
		sqlDelete.append("DELETE FROM AGSLPF ");
		sqlDelete.append(" WHERE AGNTNUM in ('");
		String delete =  String.join("','", delist);
		sqlDelete.append(delete);
		sqlDelete.append("')");
		PreparedStatement ps = getPrepareStatement(sqlDelete.toString());
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("Error occured while deleting AGSLPF: ", e);//IJTI-1561
			throw new SQLRuntimeException (e);
		} finally {
			close(ps, null);
		}
	}
	
	@Override
	public List<Agslpf> getAgslpfByAgentAndLic(String agntNumber,String licType) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM AGSLPF WHERE AGNTNUM=? AND AGNCYSALICDE=?");
        PreparedStatement ps = getPrepareStatement(stringBuilder.toString());
        ResultSet rs = null;
        List<Agslpf> agslpfList = new ArrayList<>();
        try {
        	ps.setString(1, agntNumber);
        	ps.setString(2, licType);
            rs = executeQuery(ps);
            while (rs.next()) {
            	Agslpf agslpf = new Agslpf();
            	agslpf.setClntsel(rs.getString(2));
            	agslpf.setAgnum(rs.getString(3));
            	agslpf.setAgtype(rs.getString(4));
            	agslpf.setTlaglicno(rs.getString(5));
            	agslpf.setSalelicensetype(rs.getString(6));
            	agslpf.setFrmdate(rs.getInt(7));
            	agslpf.setTodate(rs.getInt(8));
            	agslpfList.add(agslpf);
                }
            } catch (SQLException e) {
            LOGGER.error("getAgslRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return agslpfList;
	}
}
