package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class Sr52oscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52oScreenVars sv = (Sr52oScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52oscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52oScreenVars screenVars = (Sr52oScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.descn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.pstate.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.dlvrmode.setClassString("");
		screenVars.shortds.setClassString("");
		screenVars.despdateDisp.setClassString("");
		screenVars.packdateDisp.setClassString("");
		screenVars.remdteDisp.setClassString("");
		screenVars.deemdateDisp.setClassString("");
		screenVars.nextActDateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr52oscreen
 */
	public static void clear(VarModel pv) {
		Sr52oScreenVars screenVars = (Sr52oScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.descn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypedes.clear();
		screenVars.cntcurr.clear();
		screenVars.rstate.clear();
		screenVars.pstate.clear();
		screenVars.register.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.dlvrmode.clear();
		screenVars.shortds.clear();
		screenVars.despdateDisp.clear();
		screenVars.despdate.clear();
		screenVars.packdateDisp.clear();
		screenVars.packdate.clear();
		screenVars.remdteDisp.clear();
		screenVars.remdte.clear();
		screenVars.deemdateDisp.clear();
		screenVars.deemdate.clear();
		screenVars.nextActDateDisp.clear();
		screenVars.nextActDate.clear();
	}
}
