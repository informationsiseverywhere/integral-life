package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm603screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm603ScreenVars sv = (Sm603ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm603screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm603ScreenVars screenVars = (Sm603ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.active01.setClassString("");
		screenVars.agtype01.setClassString("");
		screenVars.agtype02.setClassString("");
		screenVars.agtype03.setClassString("");
		screenVars.agtype04.setClassString("");
		screenVars.active02.setClassString("");
		screenVars.mlagttyp01.setClassString("");
		screenVars.mlagttyp02.setClassString("");
		screenVars.mlagttyp03.setClassString("");
		screenVars.mlagttyp04.setClassString("");
		screenVars.agtype05.setClassString("");
		screenVars.agtype06.setClassString("");
		screenVars.agtype07.setClassString("");
		screenVars.agtype08.setClassString("");
		screenVars.agtype09.setClassString("");
		screenVars.agtype10.setClassString("");
		screenVars.mlagttyp05.setClassString("");
		screenVars.mlagttyp06.setClassString("");
		screenVars.mlagttyp07.setClassString("");
		screenVars.mlagttyp08.setClassString("");
		screenVars.mlagttyp09.setClassString("");
		screenVars.mlagttyp10.setClassString("");
		screenVars.active03.setClassString("");
	}

/**
 * Clear all the variables in Sm603screen
 */
	public static void clear(VarModel pv) {
		Sm603ScreenVars screenVars = (Sm603ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.active01.clear();
		screenVars.agtype01.clear();
		screenVars.agtype02.clear();
		screenVars.agtype03.clear();
		screenVars.agtype04.clear();
		screenVars.active02.clear();
		screenVars.mlagttyp01.clear();
		screenVars.mlagttyp02.clear();
		screenVars.mlagttyp03.clear();
		screenVars.mlagttyp04.clear();
		screenVars.agtype05.clear();
		screenVars.agtype06.clear();
		screenVars.agtype07.clear();
		screenVars.agtype08.clear();
		screenVars.agtype09.clear();
		screenVars.agtype10.clear();
		screenVars.mlagttyp05.clear();
		screenVars.mlagttyp06.clear();
		screenVars.mlagttyp07.clear();
		screenVars.mlagttyp08.clear();
		screenVars.mlagttyp09.clear();
		screenVars.mlagttyp10.clear();
		screenVars.active03.clear();
	}
}
