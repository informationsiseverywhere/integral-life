/*
 * File: Pr52r.java
 * Date: December 3, 2013 3:27:41 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR52R.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.screens.Sr52rScreenVars;
import com.csc.life.productdefinition.procedures.Tr52rpt;
import com.csc.life.productdefinition.tablestructures.Tr52rrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
****************************************************************** ****
* </pre>
*/
public class Pr52r extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52R");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr52rrec tr52rrec = new Tr52rrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr52rScreenVars sv = ScreenProgram.getScreenVars( Sr52rScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public Pr52r() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52r", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr52rrec.tr52rRec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr52rrec.agelimit.set(ZERO);
		tr52rrec.dsifact.set(ZERO);
		tr52rrec.factorsa01.set(ZERO);
		tr52rrec.factorsa02.set(ZERO);
		tr52rrec.factorsa03.set(ZERO);
		tr52rrec.factorsa04.set(ZERO);
		tr52rrec.factorsa05.set(ZERO);
		tr52rrec.factorsa06.set(ZERO);
		tr52rrec.factorsa07.set(ZERO);
		tr52rrec.factorsa08.set(ZERO);
		tr52rrec.factorsa09.set(ZERO);
		tr52rrec.factorsa10.set(ZERO);
		tr52rrec.factsamn01.set(ZERO);
		tr52rrec.factsamn02.set(ZERO);
		tr52rrec.factsamn03.set(ZERO);
		tr52rrec.factsamn04.set(ZERO);
		tr52rrec.factsamn05.set(ZERO);
		tr52rrec.factsamn06.set(ZERO);
		tr52rrec.factsamn07.set(ZERO);
		tr52rrec.factsamn08.set(ZERO);
		tr52rrec.factsamn09.set(ZERO);
		tr52rrec.factsamn10.set(ZERO);
		tr52rrec.factsamx01.set(ZERO);
		tr52rrec.factsamx02.set(ZERO);
		tr52rrec.factsamx03.set(ZERO);
		tr52rrec.factsamx04.set(ZERO);
		tr52rrec.factsamx05.set(ZERO);
		tr52rrec.factsamx06.set(ZERO);
		tr52rrec.factsamx07.set(ZERO);
		tr52rrec.factsamx08.set(ZERO);
		tr52rrec.factsamx09.set(ZERO);
		tr52rrec.factsamx10.set(ZERO);
		tr52rrec.toterm01.set(ZERO);
		tr52rrec.toterm02.set(ZERO);
		tr52rrec.toterm03.set(ZERO);
		tr52rrec.toterm04.set(ZERO);
		tr52rrec.toterm05.set(ZERO);
		tr52rrec.toterm06.set(ZERO);
		tr52rrec.toterm07.set(ZERO);
		tr52rrec.toterm08.set(ZERO);
		tr52rrec.toterm09.set(ZERO);
		tr52rrec.toterm10.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.agelimit.set(tr52rrec.agelimit);
		sv.dsifact.set(tr52rrec.dsifact);
		sv.factorsas.set(tr52rrec.factorsas);
		sv.factsamns.set(tr52rrec.factsamns);
		sv.factsamxs.set(tr52rrec.factsamxs);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.toterms.set(tr52rrec.toterms);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr52rrec.tr52rRec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.agelimit, tr52rrec.agelimit)) {
			tr52rrec.agelimit.set(sv.agelimit);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.dsifact, tr52rrec.dsifact)) {
			tr52rrec.dsifact.set(sv.dsifact);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.factorsas, tr52rrec.factorsas)) {
			tr52rrec.factorsas.set(sv.factorsas);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.factsamns, tr52rrec.factsamns)) {
			tr52rrec.factsamns.set(sv.factsamns);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.factsamxs, tr52rrec.factsamxs)) {
			tr52rrec.factsamxs.set(sv.factsamxs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.toterms, tr52rrec.toterms)) {
			tr52rrec.toterms.set(sv.toterms);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr52rpt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
