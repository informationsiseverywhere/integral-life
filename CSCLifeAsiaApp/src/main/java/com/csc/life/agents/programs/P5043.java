/*
 * File: P5043.java
 * Date: 29 August 2009 23:59:54
 * Author: Quipoz Limited
 *
 * Class transformed from P5043.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.MagupfDAO;
import com.csc.fsu.general.dataaccess.model.Magupf;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.screens.S5043ScreenVars;
import com.csc.life.contractservicing.dataaccess.dao.AglflnbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                   AGENT MAINTENANCE SUBMENU.
*                   ==========================
*
*Validation
*----------
*
* Key 1 - Agent agreement number (AGLF)
*
*      Y = mandatory, must exist on file.
*         - also read AGNTLAG and check that the agent branch is
*                the same as the sign-on branch
*
*      N = optional, if entered must not exist on file, must be
*           within correct number range (call ALOCNO to check).
*
*
*
*Updating
*--------
*
* If creating an agent  and  the  number  is  blank,  call  the
* automatic number allocation routine (ALOCNO) to get a number.
*
* During validation, read  and  store the agent header. For new
* agency agreements, initialise  all the details, and keep them
* in the I/O module.
*           Valid flag to 1,
*           All numeric fields and dates to zero,
*           Company to sign-on company,
*           Agent number as returned from ALOCNO,
*
* Set up the WSSP-FLAG  ("I"  for actions C, D and E, otherwise
* "M") in WSSP.
*
* Soft lock the agent agreement (call SOFTLCK). If the agent is
* already locked, display an error message.
*
*****************************************************************
* </pre>
*/
public class P5043 extends ScreenProgCS {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(P5043.class);

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5043");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private String wsaaAllowFlag = "";
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);

		/* FORMATS */
	private static final String aglfrec = "AGLFREC";

	private FixedLengthStringData wsaaAlocnoThereFlag = new FixedLengthStringData(1).init("N");
	private Validator alocnoAlreadyThere = new Validator(wsaaAlocnoThereFlag, "Y");
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Alocnorec alocnorec = new Alocnorec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
//	private S5043ScreenVars sv = ScreenProgram.getScreenVars( S5043ScreenVars.class);
	private S5043ScreenVars sv =   getLScreenVars();
	//ILIFE-6896
	private MagupfDAO magupfDAO = getApplicationContext().getBean("magupfDAO", MagupfDAO.class); 
	private AgntpfDAO agntpfDAO =  getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private AglflnbpfDAO aglflnbpfDAO = getApplicationContext().getBean("aglflnbpfDAOP5079",AglflnbpfDAO.class);
	//ILIFE-8880 Starts
	private boolean isDMSFlgOn = false;

	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//IBPLIFE-7757
	private static final String SYS_COY = "0";
	private static final String DMS_FEATURE = "SAFCF001";
	//ILIFE-8880 End
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit2190,
		exit2490,
		passAgentDetails3020,
		batching3080,
		exit3090,
		callAlocno3210
	}

	public P5043() {
		super();
		screenVars = sv;
		new ScreenModel("S5043", AppVars.getInstance(), sv);
	}
	protected S5043ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5043ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspcomn.clntkey.set(SPACES);
		wsaaAgntnum.set(SPACES);
		sv.dataArea.set(SPACES);
		aglfIO.setDataArea(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		isDMSFlgOn = FeaConfg.isFeatureExist(SYS_COY, DMS_FEATURE, appVars, "IT");	//ILIFE-8880
		if(isDMSFlgOn) {
			sv.isDMSFlag.set("Y");
		} else {
			sv.isDMSFlag.set("N");
		}
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set("E070");
		}
		aglfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		//IBPLIFE-7757 start
		List<Itempf> items = itempfDAO.getItemsbyItemcodeLikeOperator("IT", wsspcomn.company.toString(), "T3642", "AG");
		for(Itempf itempf : items) {
			if(itempf.getItemitem().trim().length()==4) {
				alocnorec.genkey.set(wsspcomn.branch.toString());
				break;
			} else if(itempf.getItemitem().substring(4,6).equals(wsspcomn.branch.toString())) {
				alocnorec.genkey.set(itempf.getItemitem().substring(2,4)+ wsspcomn.branch.toString());
				break;
			}
		}
		//IBPLIFE-7757 end
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5043IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5043-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2400();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, SPACES)) {
			return ;
		}
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agntsel);
		aglfIO.setFunction(varcom.readr);
		if (isNE(sv.agntsel, SPACES)) {
			SmartFileCode.execute(appVars, aglfIO);
			if (isNE(aglfIO.getStatuz(), varcom.oK)
			&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(aglfIO.getParams());
				fatalError600();
			}
		}
		else {
			aglfIO.setStatuz(varcom.mrnf);
		}
		if (isEQ(aglfIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.agntselErr.set("E305");
			return ;
		}
		if (isEQ(subprogrec.key1, "Y")) {
			checkRuser2500();
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.agntselErr.set(sdasancrec.statuz);
				return ;
			}
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.action, "C")
		&& isNE(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
			sv.agntselErr.set("H912");
			return ;
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.action, "H")
		&& isEQ(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
			sv.agntselErr.set("H912");
			return ;
		}
		if (isNE(aglfIO.getAgntnum(), sv.agntsel)) {
			sv.agntselErr.set("E305");
			return ;
		}
		if (isEQ(aglfIO.getStatuz(), varcom.oK)
		&& isEQ(subprogrec.key1, "N")) {
			sv.agntselErr.set("E330");
		}
		validateKey12211CustomerSpecific();
		/*    For transactions on existing agents,*/
		/*       check the agent selected is form correct branch*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.agntselErr, SPACES)) {
			//ILIFE-6896
			Agntpf agntlag = agntpfDAO.getAgntlagData(wsspcomn.company.toString(), sv.agntsel.toString());
			if (agntlag == null) {
				fatalError600();
			}else {
				if (isNE(agntlag.getAgntbr(), wsspcomn.branch)) {
					sv.agntselErr.set("E455");
				}
			}
		}
		wsaaAllowFlag = "N";
		if (isEQ(sv.action, "G")) {
			m500CheckAllowFlag();
			if (isNE(wsaaAllowFlag, "Y")) {
				sv.agntselErr.set("ML01");
			}
		}
	}


	protected void validateKey12211CustomerSpecific() {
		if(isDMSFlgOn && isEQ(sv.agntsel, SPACES)){
			sv.agntselErr.set("F692");	//ILIFE-8880
		}

		/* For new agents, if a number is entered, */
		/* check it against the automatic number allocation range. */
		if (isEQ(subprogrec.key1, "N") && isNE(sv.agntsel, SPACES) && isEQ(sv.agntselErr, SPACES) && !isDMSFlgOn) {		//ILIFE-8880
			checkAlocno2300();
		}
	}
protected void checkAlocno2300()
	{
		check2310();
	}

protected void check2310()
	{
		alocnorec.function.set("CHECK");
		alocnorec.prefix.set("AG");
		alocnorec.company.set(wsspcomn.company);
		//alocnorec.genkey.set(wsspcomn.branch);
		alocnorec.alocNo.set(sv.agntsel);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		/*  IF ALNO-STATUZ              = BOMB                           */
		/*      MOVE ALNO-STATUZ        TO SYSR-STATUZ                   */
		/*      PERFORM 600-FATAL-ERROR.                                 */
		/*  IF ALNO-STATUZ              NOT = O-K                        */
		/*      MOVE ALNO-STATUZ        TO S5043-AGNTSEL-ERR.            */
		if (isNE(alocnorec.statuz, varcom.oK)) {
			alocnorec.genkey.set(SPACES);
			callProgram(Alocno.class, alocnorec.alocnoRec);
			if (isEQ(alocnorec.statuz, varcom.bomb)) {
				syserrrec.statuz.set(alocnorec.statuz);
				fatalError600();
			}
			else {
				if (isNE(alocnorec.statuz, varcom.oK)) {
					sv.agntselErr.set(alocnorec.statuz);
				}
			}
		}
	}

protected void verifyBatchControl2400()
	{
		try {
			validateRequest2410();
			retrieveBatchProgs2420();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2410()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			/*      MOVE E073               TO S5043-ACTION-ERR.             */
			sv.actionErr.set("E073");
			goTo(GotoLabel.exit2490);
		}
	}

protected void retrieveBatchProgs2420()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void checkRuser2500()
	{
		begin2510();
	}

protected void begin2510()
	{
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.agnt);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.agntsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
	}

protected void m500CheckAllowFlag(){
	//ILIFE-6896
	Magupf magupf = magupfDAO.getMaguRecord(wsspcomn.company.toString(), wsspcomn.userid.toString());

	if (magupf == null) {
		wsaaAllowFlag = "Y";
		return ;
	}
	if (isNE(magupf.getAgntnum01(), SPACES)) {
		wsaaAgntnum.set(magupf.getAgntnum01());
		if (isEQ(magupf.getAgntnum01(), sv.agntsel)) {
			wsaaAllowFlag = "Y";
		}else {
			m600CheckAgent();
		}
	}
	if (isNE(magupf.getAgntnum02(), SPACES)&& isNE(wsaaAllowFlag, "Y")) {
		wsaaAgntnum.set(magupf.getAgntnum02());
		if (isEQ(magupf.getAgntnum02(), sv.agntsel)) {
			wsaaAllowFlag = "Y";
		}else {
			m600CheckAgent();
		}
	}
	if (isNE(magupf.getAgntnum03(), SPACES)	&& isNE(wsaaAllowFlag, "Y")) {
		wsaaAgntnum.set(magupf.getAgntnum03());
		if (isEQ(magupf.getAgntnum03(), sv.agntsel)) {
			wsaaAllowFlag = "Y";
		}else {
			m600CheckAgent();
		}
	}
}

protected void m600CheckAgent(){
	//ILIFE-6896
	Aglflnbpf aglflnbpf = aglflnbpfDAO.getAglflnb(sv.agntsel.toString().trim(), wsspcomn.company.toString().trim());
	while ( aglflnbpf != null && !(isEQ(aglflnbpf.getReportag(), SPACES)|| isEQ(wsaaAllowFlag, "Y"))) {
		aglflnbpf = aglflnbpfDAO.getAglflnb(aglflnbpf.getReportag().trim(), wsspcomn.company.toString().trim());
		if (isEQ(aglflnbpf.getAgntnum(), wsaaAgntnum)) {
			wsaaAllowFlag = "Y";
		}
	}

		/*M600-EXIT*/
}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateWssp3010();
				case passAgentDetails3020:
					passAgentDetails3020();
				case batching3080:
					batching3080();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		/*    IF SCRN-ACTION = 'C' OR 'D' OR 'E'                           */
		/*       MOVE 'I'                 TO WSSP-FLAG                     */
		/*    ELSE                                                         */
		/*       MOVE 'M'                 TO WSSP-FLAG.                    */
		/*  Set the WSSP-FLAG to 'A' if the SCRN-ACTION is A               */
		/*  IF SCRN-ACTION = 'C' OR 'D' OR 'E'                   <V4L017>*/
		if (isEQ(scrnparams.action, "D")
		|| isEQ(scrnparams.action, "E")
		|| isEQ(scrnparams.action, "F")
		|| isEQ(scrnparams.action, "G")) {
			wsspcomn.flag.set("I");
		}
		else {
			if (isEQ(scrnparams.action, "A")) {
				wsspcomn.flag.set("A");
			}
			else {
				if (isEQ(scrnparams.action, "C")) {
					wsspcomn.flag.set("T");
				}
				else {
					if (isEQ(scrnparams.action, "H")) {
						wsspcomn.flag.set("R");
					}
					else {
						wsspcomn.flag.set("M");
					}
				}
			}
		}
		/*    Allocate new contract number if not entered.*/
		if (isEQ(sv.agntsel, SPACES)
		&& isEQ(subprogrec.key1, "N")) {
			allocateNumber3200();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/*    Soft lock contract.*/
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.passAgentDetails3020);
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("AG");
		if (isNE(sv.agntsel, SPACES)) {
			/*    MOVE S5043-AGNTSEL       TO SFTL-ENTITY                   */
			sftlockrec.entity.set(aglfIO.getAgntnum());
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.agntselErr.set("G354");
		}
	}

protected void passAgentDetails3020()
	{
		/*    For new Agents, initialise the header*/
		if (isEQ(subprogrec.key1, "N")) {
			initialiseHeader3300();
		}
		/*    Store the Agent header for use by the transaction programs*/
		aglfIO.setFunction("KEEPS");
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void allocateNumber3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case callAlocno3210:
					callAlocno3210();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callAlocno3210()
	{
		wsaaAlocnoThereFlag.set("N");
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set("AG");
		alocnorec.company.set(wsspcomn.company);
//		alocnorec.genkey.set(wsspcomn.branch);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, varcom.oK)) {
			alocnorec.genkey.set(SPACES);
			callProgram(Alocno.class, alocnorec.alocnoRec);
		}
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.agntselErr.set(alocnorec.statuz);
		}
		else {
			checkAlocno3230();
			if (alocnoAlreadyThere.isTrue()) {
				goTo(GotoLabel.callAlocno3210);
			}
			else {
				aglfIO.setAgntnum(alocnorec.alocNo);
				sv.agntsel.set(alocnorec.alocNo);
			}
		}
	}

	protected void checkAlocno3230() {
		// ILIFE-6896
		Agntpf agntlag = agntpfDAO.getAgntlagData(wsspcomn.company.toString(),alocnorec.alocNo.toString());
		if (agntlag != null) {
			wsaaAlocnoThereFlag.set("Y");
		}
	}

protected void initialiseHeader3300()
	{
		/*SETUP*/
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		if (isNE(sv.agntsel, SPACES)) {
			aglfIO.setAgntnum(sv.agntsel);
		}
		else {
			aglfIO.setAgntnum(alocnorec.alocNo);
		}
		aglfIO.setDtepay(ZERO);
		aglfIO.setFixprc(ZERO);
		aglfIO.setSprprc(ZERO);
		aglfIO.setIntcrd(ZERO);
		aglfIO.setMinsta(ZERO);
		aglfIO.setOvcpc(ZERO);
		aglfIO.setTcolprct(ZERO);
		aglfIO.setTcolmax(ZERO);
		aglfIO.setTaxalw(ZERO);
		aglfIO.setDteapp(varcom.vrcmMaxDate);
		aglfIO.setDtetrm(varcom.vrcmMaxDate);
		aglfIO.setEffdate(varcom.vrcmMaxDate);
		aglfIO.setDteexp(varcom.vrcmMaxDate);
		aglfIO.setTransactionDate(varcom.vrcmDate);
		aglfIO.setTransactionTime(varcom.vrcmTime);
		aglfIO.setUser(varcom.vrcmUser);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}