/*
 * Date: 29 August 2009 21:03:47
 * Author: Quipoz Limited
 *
 * Class transformed from B5338.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.common.DD;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.tablestructures.Tr392rec;
import com.csc.life.agents.cls.C5338cpyf;
import com.csc.life.agents.dataaccess.AcmvagpTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglfagtTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpypfTableDAM;
import com.csc.life.agents.reports.R5338Report;
import com.csc.life.agents.tablestructures.T5622rec;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* B5338 - Agent statement print.
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*  Initialise
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*    The first  thing  this  program  does  is  to  extract  all
* outstanding  transactions  from the account movements (ACMVPF)
* to the agent payments file (AGPYPF) using the CPYF command. In
* normal circumstances  this  would  be  done  in the calling CL
* program but as the parameters depend on information from T5645
* which is  accessed from the COBOL program another call to a CL
* is performed to perform the copy file, C5338CPYF.
*
*    Access T5645  with  a  key of program number (B5338).  From
* line  01  obtain  the  subaccount  code  and  type  for  agent
* transactions.  Call C5338CPYF  to  execute the CPYF command to
* select all outstanding agent transactions from ACMVPF with the
* following parameters:
*
*         - replace records
*         - include records by field test:
*           If SACSCODE matches T5645 line 01
*           and FRCDATE equal to 9999999
*           and INTEXTIND equal to 'E'
*           and TRDT (transaction date) less than or equal
*               to PARM-DATE
*         - format options *MAP and *DROP
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*  Check for statement production.
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*
*    Sequentially  read  the Life Agents file (AGLFPF) using the
* logical view  AGLFAGT. For each current valid agent we need to
* check  whether  the  agent  receives  payments  in a specified
* currency  or  not  and  if  the  outstanding  transactions are
* greater than the minimum payment amount.
*
*
*    If the currency field on the agent file is spaces:
*
*         Read each of  the  records on AGPYPF using the logical
*      view AGPYAGT for this agent. For each set of transactions
*      within a currency for the agent accumulate the sum of the
*      original amount fields.
*
*         If  the  sum  accumulated  for  a  currency  > minimum
*      statement  amount  on the agent file move the currency to
*      the header line field and perform Print Details ELSE read
*      transactions for next currency.
*
*
*    If the currency field on the agent file is not spaces:
*
*         Read all  of  the  records on AGPYPF using the logical
*      view AGPYAGT  for  this  agent/currency.  Accumulate  the
*      amount of  the  transactions  in  working storage. If the
*      transaction currency is different from the agent currency
*      a conversion will  need to be done calling 'XCVRT' with a
*      function of 'SELL'. The accumulated amount will be in the
*      agent's currency.
*
*         If  the  sum  accumulated  for  the currency > minimum
*      statement  amount  on  the  agent  file  move  the  agent
*      currency  to  the  header  line  field  and perform Print
*      Details  ELSE  read  next  set  of  transactions for next
*      agent.
*
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
* Print Details.
*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*
*    Set up the header lines as follows:
*
*         Header  line  1  with  the  statement date as parm run
*      date.
*
*         Header line 2 with date = system date.
*
*         Header  line  3  with  time as system time and company
*      from  parameters.  The  company  description can be taken
*      from  T1693  in company 0 and the company from parameters
*      as the key.
*
*         Header line 4 with page number.
*
*         Header  line 5 with agent number from AGLFPF. Read the
*      FSU  agent  file AGNTPF using logical view AGNT with a
*      key  of parm company / agent. With the client company and
*      number  from  this  record  access the client file CLNTPF
*      using   logical   view   CLTS  with  a  key  of company /
*      client.  This file  contains  all  details of the agent's
*      contact, name and address for the remaining header lines.
*
*         Header  line  6 should already have the currency field
*      set  up  and the description from T3629 using currency as
*      the key.
*
*
*    In  order  to print the detail lines we need to re-read the
* AGPYPF   file   to  access  the  agents  unreconciled  account
* movements.  Dependent on  whether  or not the agent's currency
* field is set up in AGLFPF there will be different processing:
*
*-- Process agent with a specified currency
*
*    If the agent currency field is not spaces:
*
*    Read  AGPYPF  using  logical  view  AGPYAGT  with  a key of
* company  /  agent  as  used  to  accumulate the amounts in the
* previous section. For each record found set up a detail line:
*
*         - effective date as set up in AGPYPF
*         - description from transaction description in AGPYPF
*         - reference from transaction reference in AGPYPF
*         - original amount from AGPYPF
*         - original currency from AGPYPF
*         - amount as original amount unless currency differs
*           from agent currency in which case:
*           Call 'XCVRT' with a function of 'SELL' to convert
*           the amount to agent currency.
*         - accumulate the amount field in working storage.
*
*    After  processing  all  the records for this agent/currency
* print the total line as the accumulated amount.
*
*-- Process agent with an unspecified currency
*
*    If the agent currency field is spaces:
*
*    Read  AGPYPF  using  logical  view  AGPYAGT  with  a key of
* company  /  agent / currency as used to accumulate the amounts
* in the previous section. For each record found set up a detail
* line:
*
*         - effective date as set up in AGPYPF
*         - description from transaction description in AGPYPF
*         - reference from transaction reference in AGPYPF
*         - original amount = spaces
*         - original currency = spaces
*         - amount as original amount
*         - accumulate the amount field in working storage.
*
*    After  processing  all  the records for this agent/currency
* print the total line as the accumulated amount.
*
*****************************************************************
* </pre>
*/
public class B5338 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5338Report printerFile = new R5338Report();
	private AgpypfTableDAM agpypf = new AgpypfTableDAM();
	private FixedLengthStringData printerRec = new FixedLengthStringData(450);
	private AgpypfTableDAM agpypfRec = new AgpypfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5338");
	private int wsaaPageSize = 64;
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).init(66).setUnsigned();
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaEnough = new FixedLengthStringData(1).init("N");
	private Validator enoughMoneyEarned = new Validator(wsaaEnough, "Y");

	private FixedLengthStringData wsaaStoredAgnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaStoredAgent = new FixedLengthStringData(8).isAPartOf(wsaaStoredAgnt, 0);
	private FixedLengthStringData wsaaStoredAglfcurr = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaStoredAgpycurr = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaTotOrigAmt = new PackedDecimalData(18, 2).init(ZERO);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2).init(ZERO);

	private FixedLengthStringData wsaaChdrCurrArray = new FixedLengthStringData(1080);
	private FixedLengthStringData[] wsaaChdrCurr = FLSArrayPartOfStructure(90, 12, wsaaChdrCurrArray, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(9, wsaaChdrCurr, 0);
	private FixedLengthStringData[] wsaaCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaChdrCurr, 9);
	private PackedDecimalData wsaaAmountB4 = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDeductn = new PackedDecimalData(15, 2).init(ZERO);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private PackedDecimalData wsaaTotWhtax = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotPrmtlr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotCrcard = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotAgadvn = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaCurColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaNewColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotNetam = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTdcchrg = new PackedDecimalData(14, 2).init(ZERO);
	private FixedLengthStringData wsaaZchrgcde = new FixedLengthStringData(6).init(SPACES);

	private FixedLengthStringData wsaaNewAgnt = new FixedLengthStringData(1).init("Y");
	private Validator newAgent = new Validator(wsaaNewAgnt, "Y");

	private FixedLengthStringData wsaaAgntPrinted = new FixedLengthStringData(1).init("Y");
	private Validator agntPrinted = new Validator(wsaaAgntPrinted, "Y");

	private FixedLengthStringData wsaaNewCurr = new FixedLengthStringData(1).init("Y");
	private Validator newCurrency = new Validator(wsaaNewCurr, "Y");

	private FixedLengthStringData wsaaC5338cpyfParms = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaT564501 = new FixedLengthStringData(2).isAPartOf(wsaaC5338cpyfParms, 0);
	private FixedLengthStringData wsaaC5338Company = new FixedLengthStringData(1).isAPartOf(wsaaC5338cpyfParms, 2);
	private ZonedDecimalData wsaaC5338Effdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaC5338cpyfParms, 3).setUnsigned();

	private ZonedDecimalData wsaaProcessLevel = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private Validator endOfAllAgents = new Validator(wsaaProcessLevel, "3");
	private Validator endOfAgent = new Validator(wsaaProcessLevel, new ValueRange("2", "3"));
	private Validator endOfCurrency = new Validator(wsaaProcessLevel, new ValueRange("1", "3"));

	private FixedLengthStringData wsaaBlackList = new FixedLengthStringData(1);
	private Validator commissionSusp = new Validator(wsaaBlackList, "C", "X");
		/* TABLES */
	private String t5645 = "T5645";
	private String t3629 = "T3629";
	private String t1693 = "T1693";
	private String t5622 = "T5622";
	private String tr392 = "TR392";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String acmvrec = "ACMVREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;

	private FixedLengthStringData r5338h01Record = new FixedLengthStringData(153+DD.cltaddr.length*4);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData r5338h01O = new FixedLengthStringData(153+DD.cltaddr.length*4).isAPartOf(r5338h01Record, 0);
	private FixedLengthStringData stdat = new FixedLengthStringData(10).isAPartOf(r5338h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5338h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5338h01O, 11);
	private FixedLengthStringData agnt = new FixedLengthStringData(8).isAPartOf(r5338h01O, 41);
	private FixedLengthStringData forattn = new FixedLengthStringData(30).isAPartOf(r5338h01O, 49);
	private FixedLengthStringData statcurr = new FixedLengthStringData(3).isAPartOf(r5338h01O, 79);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(r5338h01O, 82);
	private FixedLengthStringData agntname = new FixedLengthStringData(30).isAPartOf(r5338h01O, 112);
	private FixedLengthStringData tagsusind = new FixedLengthStringData(1).isAPartOf(r5338h01O, 142);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length*3);
	private FixedLengthStringData pcode = new FixedLengthStringData(10).isAPartOf(r5338h01O, 143+DD.cltaddr.length*4);

	private FixedLengthStringData r5338d01Record = new FixedLengthStringData(109);
	private FixedLengthStringData r5338d01O = new FixedLengthStringData(109).isAPartOf(r5338d01Record, 0);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5338d01O, 0);
	private FixedLengthStringData dtldesc = new FixedLengthStringData(30).isAPartOf(r5338d01O, 10);
	private FixedLengthStringData dtlref = new FixedLengthStringData(30).isAPartOf(r5338d01O, 40);
	private FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(r5338d01O, 70);
	private ZonedDecimalData orgamnt = new ZonedDecimalData(17, 2).isAPartOf(r5338d01O, 72);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(r5338d01O, 89);
	private ZonedDecimalData newamnt = new ZonedDecimalData(17, 2).isAPartOf(r5338d01O, 92);

	private FixedLengthStringData r5338t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t01O = new FixedLengthStringData(18).isAPartOf(r5338t01Record, 0);
	private ZonedDecimalData totcbal = new ZonedDecimalData(18, 2).isAPartOf(r5338t01O, 0);

	private FixedLengthStringData r5338t02Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t02O = new FixedLengthStringData(17).isAPartOf(r5338t02Record, 0);
	private ZonedDecimalData taxamt = new ZonedDecimalData(17, 2).isAPartOf(r5338t02O, 0);

	private FixedLengthStringData r5338t03Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t03O = new FixedLengthStringData(17).isAPartOf(r5338t03Record, 0);
	private ZonedDecimalData taxamt1 = new ZonedDecimalData(17, 2).isAPartOf(r5338t03O, 0);

	private FixedLengthStringData r5338t04Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t04O = new FixedLengthStringData(17).isAPartOf(r5338t04Record, 0);
	private ZonedDecimalData taxamt2 = new ZonedDecimalData(17, 2).isAPartOf(r5338t04O, 0);

	private FixedLengthStringData r5338t05Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t05O = new FixedLengthStringData(18).isAPartOf(r5338t05Record, 0);
	private ZonedDecimalData accbal = new ZonedDecimalData(18, 2).isAPartOf(r5338t05O, 0);

	private FixedLengthStringData r5338t06Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t06O = new FixedLengthStringData(18).isAPartOf(r5338t06Record, 0);
	private ZonedDecimalData accbal1 = new ZonedDecimalData(18, 2).isAPartOf(r5338t06O, 0);

	private FixedLengthStringData r5338t07Record = new FixedLengthStringData(14);
	private FixedLengthStringData r5338t07O = new FixedLengthStringData(14).isAPartOf(r5338t07Record, 0);
	private ZonedDecimalData tdcchrg = new ZonedDecimalData(14, 2).isAPartOf(r5338t07O, 0);
	private IntegerData chdrIndex = new IntegerData();
		/*Life Agent Payment Fast Extract Logical*/
	private AcmvagpTableDAM acmvagpIO = new AcmvagpTableDAM();
		/*SUB ACCOUNT MOVEMENT*/
	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Life Agents file logical view for state*/
	private AglfagtTableDAM aglfagtIO = new AglfagtTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Account movements copy file for agent pa*/
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5622rec t5622rec = new T5622rec();
	private T5645rec t5645rec = new T5645rec();
	private Tr392rec tr392rec = new Tr392rec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1190,
		exit1290,
		exit1390,
		exit2290,
		exit2390,
		para2450,
		exit3250,
		para3455,
		setAddr3500,
		exit3500,
		read4010,
		exit4000,
		read4a10,
		exit4a00,
		read6010,
		exit6090,
		lgnmExit,
		plainExit,
		payeeExit,
		exit7090,
		exit8090
	}

	public B5338() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		printerFile.openOutput();
		initialise200();
		while ( !(endOfAllAgents.isTrue())) {
			mainProcessing1000();
		}

		/*OUT*/
		printerFile.close();
		agpypf.close();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("B5338");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT564501.set(t5645rec.sacscode01);
		wsaaC5338Company.set(runparmrec.company);
		wsaaC5338Effdate.set(runparmrec.effdate);
		callProgram(C5338cpyf.class, wsaaC5338cpyfParms);
		agpypf.openInput();
		aglfagtIO.setParams(SPACES);
		aglfagtIO.setAgntcoy(runparmrec.company);
		aglfagtIO.setFunction(varcom.begn);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set("0");
		wsaaItemkey.itemItemtabl.set(t1693);
		wsaaItemkey.itemItemitem.set(runparmrec.company);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(runparmrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			wsaaCompanynm.set(SPACES);
		}
		else {
			wsaaCompanynm.set(getdescrec.longdesc);
		}
	}

protected void mainProcessing1000()
	{
		try {
			process1100();
		}
		catch (GOTOException e){
		}
	}

protected void process1100()
	{
		wsaaProcessLevel.set(ZERO);
		wsaaTotOrigAmt.set(ZERO);
		wsaaTotWhtax.set(ZERO);
		wsaaTotPrmtlr.set(ZERO);
		wsaaChdrCurrArray.set(SPACES);
		wsaaTotColltr.set(ZERO);
		wsaaAgntPrinted.set("N");
		SmartFileCode.execute(appVars, aglfagtIO);
		if (isNE(aglfagtIO.getStatuz(),varcom.oK)
		&& isNE(aglfagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(aglfagtIO.getParams());
			databaseError006();
		}
		if (isEQ(aglfagtIO.getStatuz(),varcom.endp)) {
			wsaaProcessLevel.set(3);
			goTo(GotoLabel.exit1190);
		}
		if (isNE(aglfagtIO.getAgntcoy(),runparmrec.company)) {
			wsaaProcessLevel.set(3);
			goTo(GotoLabel.exit1190);
		}
		wsaaStoredAgnt.set(aglfagtIO.getAgntnum());
		wsaaBlackList.set(aglfagtIO.getTagsusind());
		wsaaNewAgnt.set("Y");
		wsaaStoredAgpycurr.set(SPACES);
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5622);
		itdmIO.setItemitem(aglfagtIO.getTaxcde());
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(aglfagtIO.getTaxcde(),itdmIO.getItemitem())
		|| isNE(runparmrec.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5622)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			t5622rec.taxrate.set(ZERO);
		}
		else {
			t5622rec.t5622Rec.set(itdmIO.getGenarea());
		}
		getBalColltr6000();
		if (isEQ(aglfagtIO.getCurrcode(),SPACES)) {
			while ( !(endOfAgent.isTrue())) {
				agntWithoutCurr1200();
			}

		}
		else {
			agntWithCurr1300();
		}
		if (agntPrinted.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
		}
		aglfagtIO.setFunction(varcom.nextr);
		wsaaLineCount.set(99);
	}

protected void agntWithoutCurr1200()
	{
		try {
			process1200();
		}
		catch (GOTOException e){
		}
	}

protected void process1200()
	{
		if (newAgent.isTrue()) {
			bgnNewAgnt2200();
			if (endOfAgent.isTrue()) {
				goTo(GotoLabel.exit1290);
			}
		}
		wsaaNewCurr.set("Y");
		storeCurrency2700();
		while ( !(endOfCurrency.isTrue()
		|| endOfAgent.isTrue())) {
			printDetails2300();
		}

		pageOverflow3500();
		printTotals3700();
		wsaaAgntPrinted.set("Y");
	}

protected void agntWithCurr1300()
	{
		try {
			process1300();
		}
		catch (GOTOException e){
		}
	}

protected void process1300()
	{
		if (newAgent.isTrue()) {
			bgnNewAgnt2200();
			if (endOfAgent.isTrue()) {
				goTo(GotoLabel.exit1390);
			}
		}
		wsaaEnough.set("N");
		while ( !(endOfAgent.isTrue()
		|| enoughMoneyEarned.isTrue())) {
			checkIfEnoughMoney2400();
		}

		wsaaStoredAgpycurr.set(SPACES);
		if (enoughMoneyEarned.isTrue()) {
			wsaaAgntPrinted.set("Y");
			printStatement3000();
		}
	}

protected void bgnNewAgnt2200()
	{
		try {
			process2200();
		}
		catch (GOTOException e){
		}
	}

protected void process2200()
	{
		wsaaStoredAgent.set(aglfagtIO.getAgntnum());
		agpyagtIO.setRldgcoy(runparmrec.company);
		agpyagtIO.setRldgacct(wsaaStoredAgnt);
		//performance improvement -- Anjali
		agpyagtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agpyagtIO.setFitKeysSearch("RLDGCOY" , "RLDGACCT");

		agpyagtIO.setOrigcurr(SPACES);
		agpyagtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			goTo(GotoLabel.exit2290);
		}
		wsaaNewAgnt.set("N");
		wsaaNewCurr.set("Y");
	}

protected void printDetails2300()
	{
		try {
			para2300();
		}
		catch (GOTOException e){
		}
	}

protected void para2300()
	{
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			goTo(GotoLabel.exit2390);
		}
		if (isNE(agpyagtIO.getOrigcurr(),wsaaStoredAgpycurr)) {
			wsaaProcessLevel.set(1);
			goTo(GotoLabel.exit2390);
		}
		if (isEQ(agpyagtIO.getGlsign(),"+")) {
			compute(wsaaAmount, 2).set(mult(agpyagtIO.getOrigamt(),-1));
		}
		else {
			wsaaAmount.set(agpyagtIO.getOrigamt());
		}
		wsaaTotOrigAmt.add(wsaaAmount);
		keepChdrCurr4100();
		origcurr.set(agpyagtIO.getOrigcurr());
		orgamnt.set(wsaaAmount);
		newamnt.set(wsaaAmount);
		sacstyp.set(agpyagtIO.getSacstyp());
		pageOverflow3500();
		printDetails3600();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			databaseError006();
		}
	}

protected void checkIfEnoughMoney2400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para2400();
				}
				case para2450: {
					para2450();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2400()
	{
		if (isEQ(agpyagtIO.getGlsign(),"+")) {
			compute(wsaaAmount, 2).set(mult(agpyagtIO.getOrigamt(),-1));
		}
		else {
			wsaaAmount.set(agpyagtIO.getOrigamt());
		}
		if (isNE(agpyagtIO.getOrigcurr(),aglfagtIO.getCurrcode())
		&& isNE(agpyagtIO.getOrigcurr(),SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotOrigAmt.add(wsaaAmount);
			goTo(GotoLabel.para2450);
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		conlinkrec.currIn.set(agpyagtIO.getOrigcurr());
		conlinkrec.currOut.set(aglfagtIO.getCurrcode());
		conlinkrec.amountIn.set(wsaaAmount);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz,"BOMB")) {
			conerrrec.statuz.set(conlinkrec.statuz);
			systemError005();
		}
		wsaaTotOrigAmt.add(conlinkrec.amountOut);
	}

protected void para2450()
	{
		checkMinAmnt2500();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
		}
		/*EXIT*/
	}

protected void checkMinAmnt2500()
	{
		/*PARA*/
		if (isGT(aglfagtIO.getMinsta(),wsaaTotOrigAmt)) {
			wsaaEnough.set("N");
		}
		else {
			wsaaEnough.set("Y");
		}
		/*EXIT*/
	}

protected void storeCurrency2700()
	{
		/*PARA*/
		wsaaStoredAgpycurr.set(agpyagtIO.getOrigcurr());
		wsaaNewCurr.set("N");
		wsaaTotOrigAmt.set(ZERO);
		wsaaTotWhtax.set(ZERO);
		wsaaTotColltr.set(ZERO);
		wsaaProcessLevel.set(ZERO);
		/*EXIT*/
	}

protected void printStatement3000()
	{
		/*PARA*/
		wsaaTotOrigAmt.set(0);
		wsaaTotWhtax.set(0);
		wsaaTotColltr.set(0);
	//added by zhaohui for 4483
		agpyagtIO.setParams(SPACES);
	//end
		agpyagtIO.setRldgcoy(runparmrec.company);
		agpyagtIO.setRldgacct(wsaaStoredAgnt);
		agpyagtIO.setOrigcurr(SPACES);
		wsaaProcessLevel.set(0);
		wsaaNewAgnt.set("Y");
		reProcess3100();
		/*EXIT*/
	}

protected void reProcess3100()
	{
		/*PARA*/
		agntWithCurr3300();
		printTotals3700();
		/*EXIT*/
	}

protected void bgnNewAgnt3250()
	{
		try {
			process3250();
		}
		catch (GOTOException e){
		}
	}

protected void process3250()
	{
		agpyagtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			goTo(GotoLabel.exit3250);
		}
		wsaaProcessLevel.set(0);
		wsaaNewAgnt.set("N");
		wsaaNewCurr.set("Y");
	}

protected void agntWithCurr3300()
	{
		/*PROCESS*/
		if (newAgent.isTrue()) {
			bgnNewAgnt3250();
		}
		while ( !(endOfAgent.isTrue())) {
			tranPerAgnt3450();
		}

		/*EXIT*/
	}

protected void tranPerAgnt3450()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3450();
				}
				case para3455: {
					para3455();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3450()
	{
		if (isEQ(agpyagtIO.getGlsign(),"+")) {
			compute(wsaaAmount, 2).set(mult(agpyagtIO.getOrigamt(),-1));
		}
		else {
			wsaaAmount.set(agpyagtIO.getOrigamt());
		}
		origcurr.set(agpyagtIO.getOrigcurr());
		if (isNE(agpyagtIO.getOrigcurr(),aglfagtIO.getCurrcode())
		&& isNE(agpyagtIO.getOrigcurr(),SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			orgamnt.set(wsaaAmount);
			newamnt.set(wsaaAmount);
			wsaaTotOrigAmt.add(wsaaAmount);
			goTo(GotoLabel.para3455);
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		conlinkrec.currIn.set(agpyagtIO.getOrigcurr());
		conlinkrec.currOut.set(aglfagtIO.getCurrcode());
		conlinkrec.amountIn.set(wsaaAmount);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz,"BOMB")) {
			conerrrec.statuz.set(conlinkrec.statuz);
			systemError005();
		}
		orgamnt.set(wsaaAmount);
		wsaaTotOrigAmt.add(conlinkrec.amountOut);
		newamnt.set(conlinkrec.amountOut);
	}

protected void para3455()
	{
		keepChdrCurr4100();
		sacstyp.set(agpyagtIO.getSacstyp());
		pageOverflow3500();
		printDetails3600();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
		}
	}

protected void pageOverflow3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3500();
				}
				case setAddr3500: {
					setAddr3500();
					printR5338d013500();
				}
				case exit3500: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3500()
	{
		if (isLT(wsaaLineCount,wsaaPageSize)) {
			goTo(GotoLabel.exit3500);
		}
		printerRec.set(SPACES);
		agntIO.setParams(SPACES);
		cltsIO.setParams(SPACES);
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(runparmrec.effdate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		stdat.set(datcon1rec.extDate);
		agnt.set(aglfagtIO.getAgntnum());
		agntIO.setAgntnum(aglfagtIO.getAgntnum());
		tagsusind.set(aglfagtIO.getTagsusind());
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		if (isEQ(aglfagtIO.getTagsusind(),"I")){
			indOn.setTrue(1);
		}
		else if (isEQ(aglfagtIO.getTagsusind(),"C")){
			indOn.setTrue(2);
		}
		else if (isEQ(aglfagtIO.getTagsusind(),"X")){
			indOn.setTrue(3);
		}
		company.set(runparmrec.company);
		agntIO.setAgntcoy(runparmrec.company);
		companynm.set(wsaaCompanynm);
		agntIO.setAgntpfx("AG");
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agntIO.getParams());
			databaseError006();
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(agntIO.getClntcoy());
		cltsIO.setClntnum(agntIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		forattn.set(cltsIO.getForAttOf());
		if (isEQ(aglfagtIO.getCurrcode(),SPACES)) {
			currdesc.set(SPACES);
			goTo(GotoLabel.setAddr3500);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setLanguage(runparmrec.language);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(aglfagtIO.getCurrcode());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			currdesc.set(SPACES);
		}
		else {
			currdesc.set(descIO.getLongdesc());
		}
	}

protected void setAddr3500()
	{
		statcurr.set(aglfagtIO.getCurrcode());
		agntname.set(wsspLongconfname);
		addr1.set(cltsIO.getCltaddr01());
		addr2.set(cltsIO.getCltaddr02());
		addr3.set(cltsIO.getCltaddr03());
		addr4.set(cltsIO.getCltaddr04());
		pcode.set(cltsIO.getCltpcode());
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
	}

protected void printR5338d013500()
	{
		printerFile.printR5338h01(r5338h01Record, indicArea);
		wsaaLineCount.set(16);
	}

protected void printDetails3600()
	{
		/*PARA*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		printerRec.set(SPACES);
		setPrintDets3650();
		printerFile.printR5338d01(r5338d01Record);
		wsaaLineCount.add(1);
		/*EXIT*/
	}

protected void setPrintDets3650()
	{
		para3650();
	}

protected void para3650()
	{
		dtldesc.set(agpyagtIO.getTrandesc());
		dtlref.set(agpyagtIO.getRdocnum());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(agpyagtIO.getEffdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			appVars.addDiagnostic("DOCNO = "+agpyagtIO.getRdocnum(), 0);
			appVars.addDiagnostic("AGTNO = "+agpyagtIO.getRldgacct(), 0);
			appVars.addDiagnostic("EFFDATE = "+agpyagtIO.getEffdate(), 0);
			appVars.addDiagnostic("ORGAMT  = "+agpyagtIO.getOrigamt(), 0);
			conerrrec.statuz.set(datcon1rec.statuz);
			systemError005();
		}
		effdate.set(datcon1rec.extDate);
	}

protected void printTotals3700()
	{
		para3700();
	}

protected void para3700()
	{
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaTotOrigAmt);
		callContot001();
		if (commissionSusp.isTrue()) {
			contotrec.totno.set(ct06);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
		}
		else {
			contotrec.totno.set(ct05);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
		}
		if (isNE(wsaaTotOrigAmt,ZERO)) {
			wsaaLineCount.add(3);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(3);
			}
			printerRec.set(SPACES);
			totcbal.set(wsaaTotOrigAmt);
			printerFile.printR5338t01(r5338t01Record);
		}
		compute(wsaaTotWhtax, 3).setRounded(div(mult(wsaaTotOrigAmt,t5622rec.taxrate),100));
		if (isNE(wsaaTotWhtax,ZERO)) {
			wsaaLineCount.add(1);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(1);
			}
			printerRec.set(SPACES);
			compute(taxamt, 2).set(mult(wsaaTotWhtax,(-1)));
			printerFile.printR5338t02(r5338t02Record);
		}
		compute(wsaaTotNetam, 2).set(sub(wsaaTotOrigAmt,wsaaTotWhtax));
		getCollateral5000();
		if (isNE(wsaaTotColltr,ZERO)) {
			wsaaLineCount.add(1);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(1);
			}
			printerRec.set(SPACES);
			compute(taxamt2, 2).set(mult(wsaaTotColltr,(-1)));
			printerFile.printR5338t04(r5338t04Record);
		}
		compute(wsaaTotNetam, 2).set(sub(wsaaTotNetam,wsaaTotColltr));
		wsaaLineCount.add(3);
		if (isGT(wsaaLineCount,wsaaPageSize)) {
			pageOverflow3500();
			wsaaLineCount.add(3);
		}
		printerRec.set(SPACES);
		accbal1.set(wsaaTotNetam);
		printerFile.printR5338t06(r5338t06Record);
		wsaaAmountB4.set(wsaaTotNetam);
		for (chdrIndex.set(1); !(isGT(chdrIndex,90)
		|| isEQ(wsaaChdrnum[chdrIndex.toInt()],SPACES)); chdrIndex.add(1)){
			getPremiumTolerance4000();
		}
		wsaaSacscode.set(t5645rec.sacscode07);
		wsaaSacstyp.set(t5645rec.sacstype07);
		wsaaTotNetam.set(wsaaAmountB4);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction4a00();
		wsaaTotCrcard.set(wsaaDeductn);
		wsaaSacscode.set(t5645rec.sacscode08);
		wsaaSacstyp.set(t5645rec.sacstype08);
		wsaaTotNetam.set(wsaaAmountB4);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction4a00();
		wsaaTotAgadvn.set(wsaaDeductn);
		wsaaTotNetam.set(wsaaAmountB4);
		if (isNE(wsaaTotPrmtlr,ZERO)
		|| isNE(wsaaTotCrcard,ZERO)
		|| isNE(wsaaTotAgadvn,ZERO)) {
			wsaaLineCount.add(2);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(2);
			}
			printerRec.set(SPACES);
			compute(taxamt1, 2).set(mult((add(add(wsaaTotPrmtlr,wsaaTotCrcard),wsaaTotAgadvn)),(-1)));
			printerFile.printR5338t03(r5338t03Record);
		}
		getBankCharge7000();
		if (isNE(wsaaTdcchrg,ZERO)) {
			wsaaLineCount.add(2);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(2);
			}
			printerRec.set(SPACES);
			tdcchrg.set(wsaaTdcchrg);
			printerFile.printR5338t07(r5338t07Record);
		}
		compute(wsaaTotNetam, 2).set(sub(wsaaTotNetam,wsaaTdcchrg));
		if (isNE(wsaaTotNetam,ZERO)) {
			wsaaLineCount.add(2);
			if (isGT(wsaaLineCount,wsaaPageSize)) {
				pageOverflow3500();
				wsaaLineCount.add(2);
			}
			printerRec.set(SPACES);
			accbal.set(wsaaTotNetam);
			printerFile.printR5338t05(r5338t05Record);
		}
		wsaaLineCount.set(99);
	}

protected void getPremiumTolerance4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4000();
				}
				case read4010: {
					read4010();
				}
				case exit4000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4000()
	{
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(t5645rec.sacscode09);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(t5645rec.sacstype09);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvsacIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvsacIO.setFormat(acmvrec);
	}

protected void read4010()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),t5645rec.sacscode09)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),t5645rec.sacstype09)) {
			goTo(GotoLabel.exit4000);
		}
		if (isEQ(wsaaChdrnum[chdrIndex.toInt()],acmvsacIO.getRdocnum())
		&& isEQ(wsaaCntcurr[chdrIndex.toInt()],acmvsacIO.getOrigcurr())) {
			/*CONTINUE_STMT*/
		}
		else {
			acmvsacIO.setFunction(varcom.nextr);
			goTo(GotoLabel.read4010);
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			if (isLT(wsaaAmountB4,acmvsacIO.getOrigamt())) {
				goTo(GotoLabel.exit4000);
			}
			if (isEQ(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())) {
				wsaaTotPrmtlr.add(acmvsacIO.getOrigamt());
				wsaaAmountB4.subtract(acmvsacIO.getOrigamt());
			}
		}
		acmvsacIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read4010);
	}

protected void getPremiumDeduction4a00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para4a00();
				}
				case read4a10: {
					read4a10();
				}
				case exit4a00: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4a00()
	{
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(wsaaSacscode);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(wsaaSacstyp);
		acmvsacIO.setOrigcurr(SPACES);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvsacIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvsacIO.setFormat(acmvrec);
	}

protected void read4a10()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),wsaaSacscode)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),wsaaSacstyp)
		|| isLTE(wsaaAmountB4,0)) {
			goTo(GotoLabel.exit4a00);
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			if (isGTE(wsaaAmountB4,acmvsacIO.getOrigamt())) {
				wsaaDeductn.add(acmvsacIO.getOrigamt());
				wsaaAmountB4.subtract(acmvsacIO.getOrigamt());
			}
		}
		acmvsacIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read4a10);
	}

protected void keepChdrCurr4100()
	{
		/*PARA*/
		chdrIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(chdrIndex,wsaaChdrCurr.length); chdrIndex.add(1)){
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()],SPACES)) {
					wsaaChdrnum[chdrIndex.toInt()].set(agpyagtIO.getRdocnum());
					wsaaCntcurr[chdrIndex.toInt()].set(agpyagtIO.getOrigcurr());
					break searchlabel1;
				}
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()],agpyagtIO.getRdocnum())
				&& isEQ(wsaaCntcurr[chdrIndex.toInt()],agpyagtIO.getOrigcurr())) {
					/*NEXT_SENTENCE*/
					break searchlabel1;
				}
			}
		}
		/*EXIT*/
	}

protected void getCollateral5000()
	{
		/*PARA*/
		compute(wsaaTotColltr, 3).setRounded(div(mult(wsaaTotOrigAmt,aglfagtIO.getTcolprct()),100));
		if (isNE(aglfagtIO.getTcolmax(),ZERO)) {
			if (isGT(aglfagtIO.getTcolmax(),wsaaCurColltr)) {
				compute(wsaaNewColltr, 2).set(add(wsaaCurColltr,wsaaTotColltr));
				if (isGT(wsaaNewColltr,aglfagtIO.getTcolmax())) {
					compute(wsaaTotColltr, 2).set(sub(aglfagtIO.getTcolmax(),wsaaCurColltr));
				}
			}
			else {
				wsaaTotColltr.set(ZERO);
			}
		}
		/*EXIT*/
	}

protected void getBalColltr6000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para6000();
				}
				case read6010: {
					read6010();
				}
				case exit6090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para6000()
	{
		wsaaCurColltr.set(ZERO);
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(t5645rec.sacscode03);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(t5645rec.sacstype03);
		acmvsacIO.setOrigcurr(SPACES);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvsacIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvsacIO.setFormat(acmvrec);
	}

protected void read6010()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),t5645rec.sacscode03)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),t5645rec.sacstype03)) {
			goTo(GotoLabel.exit6090);
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			wsaaCurColltr.add(acmvsacIO.getAcctamt());
		}
		acmvsacIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read6010);
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void getBankCharge7000()
	{
		try {
			start7010();
		}
		catch (GOTOException e){
		}
	}

protected void start7010()
	{
		wsaaTdcchrg.set(ZERO);
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(runparmrec.company);
		aglfIO.setAgntnum(aglfagtIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(aglfIO.getParams());
			databaseError006();
		}
		if (isNE(aglfIO.getPaymth(),"DC")) {
			wsaaTdcchrg.set(ZERO);
			goTo(GotoLabel.exit7090);
		}
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(aglfIO.getBankkey());
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(babrIO.getParams());
			databaseError006();
		}
		if (isEQ(babrIO.getFacthous01(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous02(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous03(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous04(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous05(),aglfIO.getFacthous())) {
			readTr3928000();
		}
		else {
			wsaaTdcchrg.set(ZERO);
		}
	}

protected void readTr3928000()
	{
		try {
			start8010();
			read8020();
		}
		catch (GOTOException e){
		}
	}

protected void start8010()
	{
		if (isEQ(babrIO.getFacthous01(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde01());
		}
		if (isEQ(babrIO.getFacthous02(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde02());
		}
		if (isEQ(babrIO.getFacthous03(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde03());
		}
		if (isEQ(babrIO.getFacthous04(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde04());
		}
		if (isEQ(babrIO.getFacthous05(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde05());
		}
		if (isEQ(wsaaZchrgcde,SPACES)) {
			goTo(GotoLabel.exit8090);
		}
	}

protected void read8020()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tr392);
		itemIO.setItemitem(wsaaZchrgcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		tr392rec.tr392Rec.set(itemIO.getGenarea());
		wsaaTdcchrg.set(tr392rec.tdcchrg);
	}
}
