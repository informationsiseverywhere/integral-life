package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:00
 * Description:
 * Copybook name: AGCMDMYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmdmykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmdmyFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmdmyKey = new FixedLengthStringData(64).isAPartOf(agcmdmyFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmdmyChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmdmyKey, 0);
  	public FixedLengthStringData agcmdmyChdrnum = new FixedLengthStringData(8).isAPartOf(agcmdmyKey, 1);
  	public FixedLengthStringData agcmdmyLife = new FixedLengthStringData(2).isAPartOf(agcmdmyKey, 9);
  	public FixedLengthStringData agcmdmyCoverage = new FixedLengthStringData(2).isAPartOf(agcmdmyKey, 11);
  	public FixedLengthStringData agcmdmyRider = new FixedLengthStringData(2).isAPartOf(agcmdmyKey, 13);
  	public PackedDecimalData agcmdmyPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmdmyKey, 15);
  	public PackedDecimalData agcmdmySeqno = new PackedDecimalData(2, 0).isAPartOf(agcmdmyKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(agcmdmyKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmdmyFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmdmyFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}