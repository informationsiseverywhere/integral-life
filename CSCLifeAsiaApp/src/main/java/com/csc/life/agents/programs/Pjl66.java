package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.screens.Sjl66ScreenVars;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pjl66 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl66.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL66");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl66ScreenVars sv = ScreenProgram.getScreenVars(Sjl66ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	private Batckey wsaaBatckey = new Batckey();	
	private FixedLengthStringData action = new FixedLengthStringData(1);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaRecordFoundFlag = new FixedLengthStringData(1).init("N");
	private Validator wsaaNoRecordFound = new Validator(wsaaRecordFoundFlag, "N");
	private Validator wsaaRecordFound = new Validator(wsaaRecordFoundFlag, "Y");
	private FixedLengthStringData wsaaFirsttime = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaStatus= new FixedLengthStringData(1);
	private boolean isFilterSame;
	private Gensswrec gensswrec = new Gensswrec();
	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, wsaaSecProgs, 0);
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private List<Hierpf> hierpfList = new ArrayList<>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Aglfpf aglfpf = new Aglfpf(); 
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
	private Agntpf agntpf = new Agntpf();
	List<Agntpf> agntpfList = new ArrayList<>();
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private Boolean flag = true;
	private Boolean firstTime = false;
	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String T3692 = "T3692";
	private static final String TJL68 = "TJL68";
	private static final String TJL76 = "TJL76";
	private static final String TJL69 = "TJL69";
	private static final String H093 = "h093";
	private List<Descpf> tjl76List;
	private List<Descpf> tjl69List;
	private List<Descpf> t3692List;
	private String sjl66 = "Sjl66";
	private String levelno = "";

	public Pjl66() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl66", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		wsaaBatckey.set(wsspcomn.batchkey);	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		 if(isNE(action,SPACE)){
			    wsspcomn.sbmaction.set(action);
			    action.set(SPACE);
			   }
		sv.action1.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		initSubfile1200();
		initializeWsaaVariables();
		initialise1010();
		protect1020();
		/*To display all records of agentkey*/
		initialise1040();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	private void initializeWsaaVariables() {
		initialize(wsaaStatus);
	}
	
	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
			}
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltype.set(agsdpf.getLevelclass());
				if("3".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SS");
				}
				else if ("2".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SB");
				}
				loadDesc();
			}
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.agtdesc.set(descItem.getLongdesc());		
					}
				}
			}
			sv.levelno.set(agsdpf.getLevelno());
			wsaahierCode.set(agsdpf.getLevelno());
			levelno = wsaaKey.toString();
		}
	}
		
	protected void initialise1040() {
		
		hierpfList = hierpfDAO.getHierpfUp(levelno,"1");
		if (!hierpfList.isEmpty()) {
			for(Hierpf hrpf : hierpfList) {
				wsaaKey.set(hrpf.getAgentkey());
				if("AG".equals(wsaaroleCode.toString())){
					Hierpf hier = hierpfDAO.getHierpfAgRec(wsaaKey.toString(),levelno,"1",hrpf.getRegisclass());
					if(null != hier) {
							tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
									hier.getActivestatus().trim(),wsspcomn.language.toString());
							if(!tjl69List.isEmpty()) {
								for (Descpf descItem : tjl69List) {
									sv.status.set(descItem.getLongdesc());		
								}
							}
							sv.regdate.set(hier.getEffectivedt());
							tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
									hier.getRegisclass().trim(),wsspcomn.language.toString());
							if(!tjl76List.isEmpty()) {
								for (Descpf descItem : tjl76List) {
									sv.regclass.set(descItem.getLongdesc());		
								}
							}
							agntpf = agntpfDAO.getAgentData(wsaaroleCode.toString(), "2", wsaahierCode.toString());
							Optional<Agntpf> isExists = Optional.ofNullable(agntpf);
							if (isExists.isPresent()) {
								sv.aclntsel.set(agntpf.getClntnum());
								sv.agntnum.set(agntpf.getAgntnum());
								t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
										agntpf.getAgtype().trim(),wsspcomn.language.toString());
								if(!t3692List.isEmpty()) {
									for (Descpf descItem : t3692List) {
										sv.agntdesc.set(descItem.getLongdesc());		
									}
								}
								clntpf = new Clntpf();
								clntpf = cltsioCall2700(agntpf.getClntnum());
								Optional<Clntpf> op = Optional.ofNullable(clntpf);
								if (op.isPresent()) {
									sv.acltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
									sv.gender.set(clntpf.getCltsex());
									sv.dob.set(clntpf.getCltdob());
								}
							}
							aglfpf = aglfpfDAO.searchAglfRecord(agntpf.getAgntcoy(),agntpf.getAgntnum());
							Optional<Aglfpf> isPrsnt = Optional.ofNullable(aglfpf);
							if (isPrsnt.isPresent()) {
								sv.liscno.set(aglfpf.getTlaglicno());
								sv.appdate.set(aglfpf.getDteapp());
								sv.termdate.set(aglfpf.getDtetrm());
							}
							scrnparams.function.set(Varcom.sadd);
							processScreen(sjl66, sv);
							if (isNE(scrnparams.statuz, Varcom.oK)) {
								syserrrec.statuz.set(scrnparams.statuz);
								fatalError600();
							}
					}
				}
			}
		}
		wsaaFirsttime.set("N");
	}

	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	

	protected void protect1020() {
		sv.clntselOut[Varcom.pr.toInt()].set("Y");
		sv.cltnameOut[Varcom.pr.toInt()].set("Y");
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.levelnoOut[Varcom.pr.toInt()].set("Y");
		sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.leveldesOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
		sv.agtypeOut[Varcom.pr.toInt()].set("Y");
		sv.agtdescOut[Varcom.pr.toInt()].set("Y");
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		
		if(isEQ(sv.action1,"1")) {
			return;
		}
		wsspcomn.edterror.set(Varcom.oK);
		screenIo2010();
		validateSubfile2060();
	}
	
	protected void screenIo2010(){	

		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		screenEditSrnch();
		checkForRecordSelect2030();
		if(!wsaaRecordFound.isTrue()) {
			wsspwindow.value.set(SPACES);
		}
		if (wsaaRecordFound.isTrue() || isFilterSame) {
			isFilterSame = false;
			return;
		}
		filterSearch();
		saveNewFilters2060();
		scrnparams.subfileRrn.set(1);
	}
	
	protected void screenEditSrnch() {
		
		scrnparams.function.set(Varcom.srnch);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void checkForRecordSelect2030() {
		

		/* Check to see if a record has been 'Selected' from the screen,
		 and if so load the information from that record into the WSSP
		 fields.*/
		wsaaNoRecordFound.setTrue();
		while ( !(wsaaRecordFound.isTrue()
		|| isEQ(scrnparams.statuz, Varcom.endp))) {
			checkForSelection2100();
		}
		
		if (wsaaRecordFound.isTrue()) {
			loadWsspData2200();                                      
			return;
		}
		checkFilterIfSame();
	}
	
	protected void checkForSelection2100()
	{
		if (isNE(sv.action1, SPACES)) {
			wsaaRecordFound.setTrue();
		}
	}
	
	protected void loadWsspData2200()
	{
		check2200();
	}

	protected void check2200()
	{
		 /*When a record has been selected on the screen, the database
		 record is read for the selection to obtain additional fields
		 required for passing back via the WSSP fields.*/
		wsspwindow.value.set(sv.stattype);
		wsspwindow.confirmation.set(sv.stattype);
	}
	
	protected void checkFilterIfSame() {
		
		boolean isFilterSame1 = false;
		if (isEQ(sv.stattype, wsaaStatus)) {
			isFilterSame1 = true;
		}
		if(isFilterSame1 && isEQ(sv.stattype, wsaaStatus)) {
			isFilterSame1 = true;
		}else {
			isFilterSame1 = false;
		}
	}
	
	protected void filterSearch() {
		
		scrnparams.function.set(Varcom.sclr);
		screenIo9000();
		if("1".equals(sv.stattype.toString())) {
			searchActRec();
			wsaaFirsttime.set("Y");
		}
		else if("2".equals(sv.stattype.toString())) {
			searchHisRec();
			wsaaFirsttime.set("Y");
		}
		else if(isEQ(wsaaFirsttime, "Y"))  {
			initialise1040();
			flag = false;
		}
		else {
			initialise1040();
			return;
		}
	}
	
	protected void searchActRec() {
		
		hierpfList = hierpfDAO.getAgentKeyup(levelno, "1", "1");
		if(!hierpfList.isEmpty()) {
			for(Hierpf hrp : hierpfList) {
				wsaaKey.set(hrp.getAgentkey());
				if("AG".equals(wsaaroleCode.toString())){
					Hierpf hier= hierpfDAO.getHierpfList(wsaaKey.toString(),levelno,"1","1",hrp.getRegisclass());
					if(null != hier) {
							tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
									hier.getActivestatus().trim(),wsspcomn.language.toString());
							if(!tjl69List.isEmpty()) {
								for (Descpf descItem : tjl69List) {
									sv.status.set(descItem.getLongdesc());		
								}
							}
							sv.regdate.set(hier.getEffectivedt());
							tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
									hier.getRegisclass().trim(),wsspcomn.language.toString());
							if(!tjl76List.isEmpty()) {
								for (Descpf descItem : tjl76List) {
									sv.regclass.set(descItem.getLongdesc());		
								}
							}
							agntpf = agntpfDAO.getAgentData(wsaaroleCode.toString(), "2", wsaahierCode.toString());
							Optional<Agntpf> isExists = Optional.ofNullable(agntpf);
							if (isExists.isPresent()) {
								sv.aclntsel.set(agntpf.getClntnum());
								sv.agntnum.set(agntpf.getAgntnum());
								t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
										agntpf.getAgtype().trim(),wsspcomn.language.toString());
								if(!t3692List.isEmpty()) {
									for (Descpf descItem : t3692List) {
										sv.agntdesc.set(descItem.getLongdesc());		
									}
								}
								clntpf = new Clntpf();
								clntpf = cltsioCall2700(agntpf.getClntnum());
								Optional<Clntpf> op = Optional.ofNullable(clntpf);
								if (op.isPresent()) {
									sv.acltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
									sv.gender.set(clntpf.getCltsex());
									sv.dob.set(clntpf.getCltdob());
								}
							}
							aglfpf = aglfpfDAO.searchAglfRecord(agntpf.getAgntcoy(),agntpf.getAgntnum());
							Optional<Aglfpf> isPrsnt = Optional.ofNullable(aglfpf);
							if (isPrsnt.isPresent()) {
								sv.liscno.set(aglfpf.getTlaglicno());
								sv.appdate.set(aglfpf.getDteapp());
								sv.termdate.set(aglfpf.getDtetrm());
							}
							scrnparams.function.set(Varcom.sadd);
							processScreen(sjl66, sv);
							if (isNE(scrnparams.statuz, Varcom.oK)) {
								syserrrec.statuz.set(scrnparams.statuz);
								fatalError600();
							}
						}
					}
				}
			}
		}
	
	protected void searchHisRec() {
		
		hierpfList = hierpfDAO.getAgentKeyup(levelno, "2", "1");
		if(!hierpfList.isEmpty()) {
			for(Hierpf hrp : hierpfList) {
				wsaaKey.set(hrp.getAgentkey());
					if("AG".equals(wsaaroleCode.toString())){
						Hierpf hier = hierpfDAO.getHierpfList(wsaaKey.toString(),levelno,"2","1",hrp.getRegisclass());
						if(null != hier)  {
								tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
										hier.getActivestatus().trim(),wsspcomn.language.toString());
								if(!tjl69List.isEmpty()) {
									for (Descpf descItem : tjl69List) {
										sv.status.set(descItem.getLongdesc());		
									}
								}
								sv.regdate.set(hier.getEffectivedt());
								tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
										hier.getRegisclass().trim(),wsspcomn.language.toString());
								if(!tjl76List.isEmpty()) {
									for (Descpf descItem : tjl76List) {
										sv.regclass.set(descItem.getLongdesc());		
									}
								}
								agntpf = agntpfDAO.getAgentData(wsaaroleCode.toString(), "2", wsaahierCode.toString());
								Optional<Agntpf> isExists = Optional.ofNullable(agntpf);
								if (isExists.isPresent()) {
									sv.aclntsel.set(agntpf.getClntnum());
									sv.agntnum.set(agntpf.getAgntnum());
									t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
											agntpf.getAgtype().trim(),wsspcomn.language.toString());
									if(!t3692List.isEmpty()) {
										for (Descpf descItem : t3692List) {
											sv.agntdesc.set(descItem.getLongdesc());		
										}
									}
									clntpf = new Clntpf();
									clntpf = cltsioCall2700(agntpf.getClntnum());
									Optional<Clntpf> op = Optional.ofNullable(clntpf);
									if (op.isPresent()) {
										sv.acltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
										sv.gender.set(clntpf.getCltsex());
										sv.dob.set(clntpf.getCltdob());
									}
								}
								aglfpf = aglfpfDAO.searchAglfRecord(agntpf.getAgntcoy(),agntpf.getAgntnum());
								Optional<Aglfpf> isPrsnt = Optional.ofNullable(aglfpf);
								if (isPrsnt.isPresent()) {
									sv.liscno.set(aglfpf.getTlaglicno());
									sv.appdate.set(aglfpf.getDteapp());
									sv.termdate.set(aglfpf.getDtetrm());
								}
								scrnparams.function.set(Varcom.sadd);
								processScreen(sjl66, sv);
								if (isNE(scrnparams.statuz, Varcom.oK)) {
									syserrrec.statuz.set(scrnparams.statuz);
									fatalError600();
								}
							}
						}
					}
				}
			}
	
	protected void screenIo9000()
	{
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void saveNewFilters2060()
	{
		wsaaStatus.set(sv.stattype);
		if(isEQ(wsaaFirsttime, "Y")) {
			wsspcomn.edterror.set("Y");
		}
		if(isEQ(wsaaFirsttime, "N") && flag) {
			return;
		}
		if(isEQ(wsaaFirsttime, "N") && !flag) {
			wsspcomn.edterror.set("Y");
			flag = true;
		}
	}
	
	protected void validateSubfile2060()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,Varcom.endp))) {
			validateSubfile2600();
		}	
	}
	
	protected void validateSubfile2600(){
		validation2610();
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}

	protected void validation2610()
	{
		if (isEQ(sv.action1,SPACES)) {
			updateErrorIndicators2670();
		}
		if (isEQ(sv.action1,"1")) {
			
		}
		else {
			updateErrorIndicators2670();
		}
	}

	protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(Varcom.oK);
			readNextModifiedRecord2680();
		}
		scrnparams.function.set(Varcom.supd);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	@Override
	protected void update3000() {
		
		wsspcomn.chdrCownnum.set(sv.levelno.trim());
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		return;
	}
	
	@Override
	protected void whereNext4000()
	{
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			nextProgram4080();
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*") && firstTime) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return;
		}
		para4000();
		bypassStart4010();
		nextProgram4080();
	}

	protected void para4000(){
		
		agntpfDAO.deleteCacheObject(agntpf);
		aglfIO.setFunction(Varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			sv.action1.set(SPACES);
			compute(sub1, 0).set(add(wsspcomn.programPtr,1));
			sub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
			return;
		}
		scrnparams.statuz.set(Varcom.oK);
		scrnparams.function.set(Varcom.sstrt);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void bypassStart4010(){
		
		if (isNE(sv.action1, "1")) {
			while ( !(isNE(sv.action1,SPACES)
			|| isEQ(scrnparams.statuz,Varcom.endp))) {
				readSubfile4400();
			}
			
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)) {
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			sub1.set(wsspcomn.programPtr);
			sub2.set(0);
			for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
				saveProgramStack4200();
			}
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		selectIs24030();
	}
	
	protected void selectIs24030(){
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.action2);
		aglfIO.setFunction(Varcom.reads);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntpf.setAgntcoy(wsspcomn.company.toString());
		agntpf.setAgntnum(sv.action2.toString());
		agntpfDAO.setCacheObject(agntpf);
		if (isEQ(sv.action1, "1")) { 
			callGenssw4070();
			firstTime = true;
		}
	}

	protected void callGenssw4070(){
		wsspcomn.flag.set("I");
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("1");
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,Varcom.oK)
		&& isNE(gensswrec.statuz,Varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(H093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
	}

	protected void nextProgram4080(){
		wsspcomn.nextprog.set(wsaaProg);
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            wsspcomn.programPtr.subtract(1);
            return;
		}
		if(isEQ(sv.action1, "1")) {
			wsspcomn.programPtr.add(1);
		}else {
			wsspcomn.programPtr.add(2);
		}
	}

	protected void restoreProgram4100(){

		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
	}

	protected void saveProgramStack4200(){
		sub1.add(1);
		sub2.add(1);
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
	}

	protected void loadProgramStack4300(){
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
	}

	protected void readSubfile4400(){
		scrnparams.function.set(Varcom.srdn);
		processScreen(sjl66, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
}
