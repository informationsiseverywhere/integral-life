package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:04
 * Description:
 * Copybook name: AGCSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcsKey = new FixedLengthStringData(64).isAPartOf(agcsFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcsAgntcoy = new FixedLengthStringData(1).isAPartOf(agcsKey, 0);
  	public FixedLengthStringData agcsPayclt = new FixedLengthStringData(8).isAPartOf(agcsKey, 1);
  	public FixedLengthStringData agcsSacscode = new FixedLengthStringData(2).isAPartOf(agcsKey, 9);
  	public FixedLengthStringData agcsSacstyp = new FixedLengthStringData(2).isAPartOf(agcsKey, 11);
  	public FixedLengthStringData agcsAgntnum = new FixedLengthStringData(8).isAPartOf(agcsKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(agcsKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}