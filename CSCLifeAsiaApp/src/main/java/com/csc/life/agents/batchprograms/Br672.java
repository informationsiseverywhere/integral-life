/*
 * File: Br672.java
 * Date: 29 August 2009 22:36:09
 * Author: Quipoz Limited
 * 
 * Class transformed from BR672.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwcvgrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.agents.tablestructures.Tr663rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.dataaccess.PcddlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart.tablestructures.T1697rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Coverage Details Extraction for Bonus Workbench
*  *********************************************************
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of records skipped
*     03  -  Number of records written
*
*   In INPUT-OUTPUT SECTION, we specify the output file ZBNWPF in which
*   the premium payment recores are written.
*   Define some variables for DB override
*
*   In 2000-READ-FILE SECTION, we fetch every record from the cursor
*   and then process those required records
*
*   We would process those transactions that set up on the table T1697
*   only. Bypass those transactions which are not on the table.
*   We would check if the contract has been populated. If the contract
*   has been populated, skip the record and the increment the control
*   control total CT02. Otherwise store contract number
*
*   In 3000-UPDATE SECTION, we would retrieve the agent split
*   information (In order to reduce the IO, we limit the number
*   of agents to maximum 500) and loop the coverage file COVRPF/COVTPF
*   via the logical view COVRMJA/COVTLNB to write the coverage details
*    records
*   Check if the contract is in proposal state (after AFI), we would
*   read the coverages from COVTPF. Otherwise we would read the
*   details from COVRPF.
*
*   Create the section 3100-AGENT-SPLIT to retrieve which agents are
*    eligible to obtain commission
*
*   Create the section 3500-WRITE to write the coverage details record
*
*   In 4000-UPDATE SECTION, we close the output file ZBNWPF and cursor
*   as well as copy the file to the FTP library which is given in
*   the process definition
*
*   Create the section A100-DATE-CONVERSION to convert the date in the
*   format MM/DD/YYYY.
*
*   Create the section A200-PACK-STRING to pack the data string such
*   that semi colon will be added to separate the data.
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br672 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlptrnpf1rs = null;
	private java.sql.PreparedStatement sqlptrnpf1ps = null;
	private java.sql.Connection sqlptrnpf1conn = null;
	private String sqlptrnpf1 = "";
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR672");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(6, 0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIz = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaMaxItem = new ZonedDecimalData(3, 0).init(500).setUnsigned();
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCrrcd = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaLength = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaConvAmount = new FixedLengthStringData(19);
	private ZonedDecimalData wsaaInptAmount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaIntmAmount = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData wsaaIntmAmountX = new FixedLengthStringData(17).isAPartOf(wsaaIntmAmount, 0, REDEFINE);
	private FixedLengthStringData wsaaIntmInteger = new FixedLengthStringData(15).isAPartOf(wsaaIntmAmountX, 0);
	private FixedLengthStringData wsaaIntmDecimal = new FixedLengthStringData(2).isAPartOf(wsaaIntmAmountX, 15);

	private FixedLengthStringData wsaaNegativeSign = new FixedLengthStringData(1);
	private Validator negativeSign = new Validator(wsaaNegativeSign, "Y");
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);

		/* WSAA-AGENTINFOR */
	private FixedLengthStringData[] wsaaAgent = FLSInittedArray (500, 16);
	private FixedLengthStringData[] wsaaAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaAgent, 0);
	private FixedLengthStringData[] wsaaProfile = FLSDArrayPartOfArrayStructure(3, wsaaAgent, 8);
	private ZonedDecimalData[] wsaaSplitcomm = ZDArrayPartOfArrayStructure(5, 2, wsaaAgent, 11, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaTrcdeFound = new FixedLengthStringData(1);
	private Validator trcdeFound = new Validator(wsaaTrcdeFound, "N");

	private FixedLengthStringData wsaaPcddFound = new FixedLengthStringData(1);
	private Validator pcddFound = new Validator(wsaaPcddFound, "Y");

	private FixedLengthStringData wsaaFirstIssue = new FixedLengthStringData(1);
	private Validator firstIssue = new Validator(wsaaFirstIssue, "Y");

	private FixedLengthStringData wsaaTransaction = new FixedLengthStringData(4);
	private Validator contractIssue = new Validator(wsaaTransaction, "T642");

	private FixedLengthStringData wsaaTerminated = new FixedLengthStringData(1);
	private Validator terminated = new Validator(wsaaTerminated, "Y");
	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2);

	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqx, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqn = new ZonedDecimalData(2, 0).isAPartOf(filler5, 0).setUnsigned();
	private FixedLengthStringData wsaaStoredChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private static final int wsaaT1697MaxTranscd = 72;
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private static final String t1697 = "T1697";
	private static final String tr663 = "TR663";
	private static final String tr658 = "TR658";
	private static final String t5687 = "T5687";
	private static final String t5679 = "T5679";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler7 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler7, 5);

		/* SQL-PTRNPF */
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(15);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(ptrnrec, 0);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(ptrnrec, 8);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(ptrnrec, 12);

		/*  Control indicators*/
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private Zbnwcvgrec zbnwcvgrec = new Zbnwcvgrec();
	private T1697rec t1697rec = new T1697rec();
	private Tr663rec tr663rec = new Tr663rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private T5679rec t5679rec = new T5679rec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090, 
		call3120, 
		eof3180, 
		next3220, 
		exit3290, 
		next3320, 
		exit3390, 
		call3820, 
		next3880, 
		exit3890, 
		a430Loop, 
		a440Set, 
		a490Exit
	}

	public Br672() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspcomn.edterror;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* We override the Database file ZBNWPF for record written*/
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bprdIO.getCompany());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaZbnwFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		zbnwpf.openOutput();
		wsspcomn.edterror.set(varcom.oK);
		/* Declare and open the sql cursor*/
		sqlptrnpf1 = " SELECT  CHDRNUM, BATCTRCDE" +
" FROM   " + getAppVars().getTableNameOverriden("PTRNPF") + " " +
" WHERE CHDRCOY = ?" +
" AND VALIDFLAG <> '2'" +
" AND TRDT = ?" +
" ORDER BY CHDRNUM";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlptrnpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PtrnpfTableDAM());
			sqlptrnpf1ps = getAppVars().prepareStatementEmbeded(sqlptrnpf1conn, sqlptrnpf1, "PTRNPF");
			getAppVars().setDBString(sqlptrnpf1ps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlptrnpf1ps, 2, wsaaEffectiveDate);
			sqlptrnpf1rs = getAppVars().executeQuery(sqlptrnpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* READ TR663, T1697 AND T5679*/
		readTables1100();
	}

protected void readTables1100()
	{
		tr6631110();
		t16971120();
		t56791130();
	}

protected void tr6631110()
	{
		/*READ TR663 TO GET TR663-APROFILE*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(tr663);
		itemIO.setItemitem(bprdIO.getCompany());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr663rec.tr663Rec.set(itemIO.getGenarea());
	}

protected void t16971120()
	{
		/* READ T1697*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t1697);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1697rec.t1697Rec.set(itemIO.getGenarea());
	}

protected void t56791130()
	{
		/*READ T5679*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlptrnpf1rs)) {
				getAppVars().getDBObject(sqlptrnpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlptrnpf1rs, 2, sqlBatctrcde);
				getAppVars().getDBObject(sqlptrnpf1rs, 3, sqlTranno);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Increment the control total for number of records read*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspcomn.edterror.set(varcom.endp);
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		/* Check record is to process those transactions that set*/
		/* up on the table T1697 only.Bypass those transactions which*/
		/* are not on the table*/
		wsspcomn.edterror.set(varcom.oK);
		wsaaTrcdeFound.set("Y");
		for (wsaaIx.set(1); !(isGT(wsaaIx,wsaaT1697MaxTranscd)
		|| trcdeFound.isTrue()); wsaaIx.add(1)){
			if (isEQ(sqlBatctrcde,t1697rec.transcd[wsaaIx.toInt()])) {
				trcdeFound.setTrue();
			}
		}
		if (!trcdeFound.isTrue()) {
			wsspcomn.edterror.set(SPACES);
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			return ;
		}
		/* Check if the contract has been populated.If the contract*/
		/* has been populated,skip the record and the increatement*/
		/* the control total CT02.Otherwise store contract number.*/
		if (isEQ(sqlChdrnum,wsaaStoredChdrnum)) {
			wsspcomn.edterror.set(SPACES);
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			return ;
		}
		else {
			wsaaStoredChdrnum.set(sqlChdrnum);
		}
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/*  Update database records.*/
		agentSplit3100();
		/*Read CHDRENQ and check if the contract is in proposal state.*/
		/*Then deceide read the coverage from COVTPF or COVRPF*/
		a300ReadChdrenq();
		if (isEQ(chdrenqIO.getValidflag(),"1")) {
			covrmjaIO.setParams(SPACES);
			covrmjaIO.setChdrcoy(bprdIO.getCompany());
			covrmjaIO.setChdrnum(sqlChdrnum);
			covrmjaIO.setLife(SPACES);
			covrmjaIO.setCoverage(SPACES);
			covrmjaIO.setRider(SPACES);
			covrmjaIO.setPlanSuffix(ZERO);
			covrmjaIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrmjaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrmjaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

			covrmjaIO.setFormat(formatsInner.covrmjarec);
			while ( !(isEQ(covrmjaIO.getStatuz(),varcom.endp))) {
				covr3200();
			}
			
		}
		else {
			covtlnbIO.setParams(SPACES);
			covtlnbIO.setChdrcoy(bprdIO.getCompany());
			covtlnbIO.setChdrnum(sqlChdrnum);
			covtlnbIO.setLife(SPACES);
			covtlnbIO.setCoverage(SPACES);
			covtlnbIO.setRider(SPACES);
			covtlnbIO.setSeqnbr(ZERO);
			covtlnbIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
				covt3300();
			}
			
		}
	}

protected void agentSplit3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					init3110();
				case call3120: 
					call3120();
					write3130();
				case eof3180: 
					eof3180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init3110()
	{
		/* Initialize the IO with the key of contract numbers with function*/
		/* BEGN and store the information in the working storage*/
		wsaaPcddFound.set("N");
		pcddlnbIO.setParams(SPACES);
		pcddlnbIO.setChdrcoy(bprdIO.getCompany());
		pcddlnbIO.setChdrnum(sqlChdrnum);
		pcddlnbIO.setFormat(formatsInner.pcddlnbrec);
		pcddlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcddlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		wsaaIx.set(0);
	}

protected void call3120()
	{
		/* CALL THE IO*/
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			syserrrec.params.set(pcddlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(pcddlnbIO.getStatuz(),varcom.endp)
		|| isNE(pcddlnbIO.getChdrcoy(),bprdIO.getCompany())
		|| isNE(pcddlnbIO.getChdrnum(),sqlChdrnum)) {
			pcddlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.eof3180);
		}
	}

protected void write3130()
	{
		pcddFound.setTrue();
		wsaaIx.add(1);
		if (isGT(wsaaIx,wsaaMaxItem)) {
			pcddlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.eof3180);
		}
		wsaaAgntnum[wsaaIx.toInt()].set(pcddlnbIO.getAgntnum());
		wsaaProfile[wsaaIx.toInt()].set(tr663rec.zprofile);
		wsaaSplitcomm[wsaaIx.toInt()].set(pcddlnbIO.getSplitBcomm());
		/*NEXT*/
		pcddlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3120);
	}

protected void eof3180()
	{
		if (!pcddFound.isTrue()) {
			/* Retrieve the agent information form the contract header*/
			a300ReadChdrenq();
			wsaaAgntnum[1].set(chdrenqIO.getAgntnum());
			wsaaProfile[1].set(tr663rec.zprofile);
			wsaaSplitcomm[1].set(100);
		}
		/*EXIT*/
	}

protected void covr3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call3210();
				case next3220: 
					next3220();
				case exit3290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call3210()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)
		&& isNE(covrmjaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
		if (isEQ(covrmjaIO.getStatuz(),varcom.endp)
		|| isNE(covrmjaIO.getChdrnum(),sqlChdrnum)
		|| isNE(covrmjaIO.getChdrcoy(),bprdIO.getCompany())) {
			covrmjaIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		/*Skip Joint Life*/
		if (isNE(covrmjaIO.getJlife(),"00")
		&& isNE(covrmjaIO.getJlife(),SPACES)) {
			goTo(GotoLabel.next3220);
		}
		/*Write the coverage detail record by coverage & agent.*/
		wsaaCrtable.set(covrmjaIO.getCrtable());
		wsaaStatcode.set(covrmjaIO.getStatcode());
		wsaaCrrcd.set(covrmjaIO.getCrrcd());
		wsaaInstprem.set(covrmjaIO.getInstprem());
		wsaaRider.set(covrmjaIO.getRider());
		wsaaSumins.set(covrmjaIO.getSumins());
		wsaaSingp.set(covrmjaIO.getSingp());
		setup3400();
		for (wsaaIz.set(1); !(isGT(wsaaIz,wsaaMaxItem)); wsaaIz.add(1)){
			write3700();
		}
	}

protected void next3220()
	{
		covrmjaIO.setFunction(varcom.nextr);
	}

protected void covt3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call3310();
				case next3320: 
					next3320();
				case exit3390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call3310()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
		|| isNE(covtlnbIO.getChdrnum(),sqlChdrnum)
		|| isNE(covtlnbIO.getChdrcoy(),bprdIO.getCompany())) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3390);
		}
		/*Skip Joint Life*/
		if (isNE(covtlnbIO.getJlife(),"00")
		&& isNE(covtlnbIO.getJlife(),SPACES)) {
			goTo(GotoLabel.next3320);
		}
		/*Write the coverage detail record by coverage & agent.*/
		wsaaCrtable.set(covtlnbIO.getCrtable());
		wsaaStatcode.set(chdrenqIO.getStatcode());
		wsaaCrrcd.set(chdrenqIO.getOccdate());
		wsaaInstprem.set(covtlnbIO.getInstprem());
		wsaaRider.set(covtlnbIO.getRider());
		wsaaSumins.set(covtlnbIO.getSumins());
		wsaaSingp.set(covtlnbIO.getSingp());
		setup3400();
		for (wsaaIz.set(1); !(isGT(wsaaIz,wsaaMaxItem)); wsaaIz.add(1)){
			write3700();
		}
	}

protected void next3320()
	{
		covtlnbIO.setFunction(varcom.nextr);
	}

protected void setup3400()
	{
		zbnwcvgrec.bnwcvgRec.set(SPACES);
		/*Determine the contract is just issued*/
		wsaaTransaction.set(sqlBatctrcde);
		if (contractIssue.isTrue()) {
			checkFirstTimeIssue3800();
		}
		if (contractIssue.isTrue()
		&& firstIssue.isTrue()) {
			zbnwcvgrec.bnwcvgTrcde.set("CR");
		}
		else {
			zbnwcvgrec.bnwcvgTrcde.set("UPD");
		}
		zbnwcvgrec.bnwcvgSource.set("LIFE/ASIA");
		zbnwcvgrec.bnwcvgCrtable.set(wsaaCrtable);
		/*Retrive the coverage status description from the table TR658*/
		/*with the key COVRMJA-STATCODE.*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(tr658);
		descIO.setDesccoy(bprdIO.getCompany());
		descIO.setDescitem(wsaaStatcode);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setFormat(formatsInner.descrec);
		descIO.setLanguage(bsscIO.getLanguage());
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc(SPACES);
		}
		zbnwcvgrec.bnwcvgStatdesc.set(descIO.getLongdesc());
		wsaaInptDate.set(wsaaCrrcd);
		a100DateConversion();
		zbnwcvgrec.bnwcvgStrtdate.set(wsaaConvDate);
		/*Check whether the contract has been terminated,which is defined*/
		/*in the table T5679.*/
		terminated.setTrue();
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)
		|| !terminated.isTrue()); wsaaIx.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaIx.toInt()],wsaaStatcode)) {
				wsaaTerminated.set("N");
			}
		}
		if (terminated.isTrue()) {
			wsaaInptDate.set(bsscIO.getEffectiveDate());
			a100DateConversion();
			zbnwcvgrec.bnwcvgTrmdate.set(wsaaConvDate);
		}
		else {
			zbnwcvgrec.bnwcvgTrmdate.set("99/99/9999");
		}
		zbnwcvgrec.bnwcvgPartner.set(SPACES);
		zbnwcvgrec.bnwcvgClrhouse.set(SPACES);
		zbnwcvgrec.bnwcvgSpclcls.set(SPACES);
		zbnwcvgrec.bnwcvgPremcls.set(SPACES);
		/*left shift the modal premium COVRMJA-INSTPREM/COVTLNB-INSTPREM*/
		wsaaInptAmount.set(wsaaInstprem);
		a400LeftShift();
		zbnwcvgrec.bnwcvgInstprem.set(wsaaConvAmount);
		/*Derive the annual premium from the instalment premium & billing*/
		/*frequency and then left shit the derived amount*/
		a500GetBillfreq();
		if (isEQ(payrIO.getStatuz(),varcom.mrnf)) {
			wsaaBillfreqx.set(ZERO);
		}
		else {
			wsaaBillfreqx.set(payrIO.getBillfreq());
		}
		compute(wsaaAnnprem, 2).set(mult(wsaaInstprem,wsaaBillfreqn));
		wsaaInptAmount.set(wsaaAnnprem);
		a400LeftShift();
		zbnwcvgrec.bnwcvgAnnprem.set(wsaaConvAmount);
		zbnwcvgrec.bnwcvgTarcmsn.set("0");
		/*Check whether this is base component*/
		if (isEQ(wsaaRider,"00")) {
			zbnwcvgrec.bnwcvgLevel.set("base");
		}
		else {
			zbnwcvgrec.bnwcvgLevel.set("rider");
		}
		/*Left shift the SUMINS*/
		wsaaInptAmount.set(wsaaSumins);
		a400LeftShift();
		zbnwcvgrec.bnwcvgFaceamt.set(wsaaConvAmount);
		zbnwcvgrec.bnwcvgCashval.set("0");
		/*Left shift the modal premium*/
		wsaaInptAmount.set(wsaaSingp);
		a400LeftShift();
		zbnwcvgrec.bnwcvgGuideamt.set(wsaaConvAmount);
		/*Populate the plan description from the table T5687 via DESCIO*/
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5687);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bprdIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFormat(formatsInner.descrec);
		descIO.setDescitem(wsaaCrtable);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc(SPACES);
		}
		zbnwcvgrec.bnwcvgPlan.set(descIO.getLongdesc());
		zbnwcvgrec.bnwcvgMarkt.set(SPACES);
		/*Populate the company ID and state by calling the subroutin ZBWCMP*/
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("POL");
		zbwcmpyrec.input.set(sqlChdrnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwcvgrec.bnwcvgCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwcvgrec.bnwcvgCompany.set(SPACES);
		}
		zbnwcvgrec.bnwcvgChdrnum.set(sqlChdrnum);
		zbnwcvgrec.bnwcvgNewfield.set(SPACES);
		wsaaInptDate.set(bsscIO.getEffectiveDate());
		a100DateConversion();
		zbnwcvgrec.bnwcvgTrandate.set(wsaaConvDate);
		zbnwcvgrec.bnwcvgProcdate.set(wsaaConvDate);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void write3700()
	{
			write3710();
		}

protected void write3710()
	{
		if (isNE(wsaaAgntnum[wsaaIz.toInt()],SPACES)) {
			zbnwcvgrec.bnwcvgAgntnum[1].set(wsaaAgntnum[wsaaIz.toInt()]);
			zbnwcvgrec.bnwcvgProfile[1].set(wsaaProfile[wsaaIz.toInt()]);
			wsaaInptAmount.set(wsaaSplitcomm[wsaaIz.toInt()]);
			a400LeftShift();
			zbnwcvgrec.bnwcvgCmsnrtst[1].set(wsaaConvAmount);
			zbnwcvgrec.bnwcvgCmsnrtrnl[1].set(wsaaConvAmount);
			zbnwcvgrec.bnwcvgAgntrole[1].set(SPACES);
			zbnwcvgrec.bnwcvgProdrtst[1].set(SPACES);
			zbnwcvgrec.bnwcvgProdrtrnl[1].set(SPACES);
			zbnwcvgrec.bnwcvgProdrtst[1].set(100);
			zbnwcvgrec.bnwcvgProdrtrnl[1].set(100);
		}
		else {
			compute(wsaaIz, 0).set(add(wsaaMaxItem,1));
			return ;
		}
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct03);
		callContot001();
	}

protected void checkFirstTimeIssue3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3810();
				case call3820: 
					call3820();
					check3830();
				case next3880: 
					next3880();
				case exit3890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3810()
	{
		ptrnenqIO.setParams(SPACES);
		ptrnenqIO.setChdrcoy(bprdIO.getCompany());
		ptrnenqIO.setChdrnum(sqlChdrnum);
		ptrnenqIO.setBatctrcde(sqlBatctrcde);
		ptrnenqIO.setTranno(sqlTranno);
		ptrnenqIO.setFormat(ptrnrec);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		firstIssue.setTrue();
	}

protected void call3820()
	{
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		&& isNE(ptrnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		if (isEQ(ptrnenqIO.getStatuz(),varcom.endp)
		|| isNE(ptrnenqIO.getChdrcoy(),bprdIO.getCompany())
		|| isNE(ptrnenqIO.getChdrnum(),sqlChdrnum)) {
			ptrnenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3890);
		}
	}

protected void check3830()
	{
		if (isEQ(ptrnenqIO.getTranno(),sqlTranno)) {
			goTo(GotoLabel.next3880);
		}
		wsaaTransaction.set(ptrnenqIO.getBatctrcde());
		if (contractIssue.isTrue()) {
			wsaaFirstIssue.set("N");
			goTo(GotoLabel.exit3890);
		}
	}

protected void next3880()
	{
		ptrnenqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3820);
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlptrnpf1conn, sqlptrnpf1ps, sqlptrnpf1rs);
		/*  Close any open files.*/
		zbnwpf.close();
		/*  Put the file to FTP library for download*/
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaStatuz);
			stringVariable1.addExpression(bprdIO.getRunLibrary());
			stringVariable1.addExpression(wsaaZbnwFn);
			stringVariable1.addExpression(wsaaFtpLibrary);
			stringVariable1.addExpression(wsaaFtpFile);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void a100DateConversion()
	{
		/*A110-INIT*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A190-EXIT*/
	}

protected void a200PackString()
	{
		/*A210-INIT*/
		wsaaString.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgTrcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgSource, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCrtable, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgStatdesc, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgStrtdate, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgTrmdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgPartner, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgClrhouse, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgSpclcls, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgPremcls, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgInstprem, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAnnprem, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgTarcmsn, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgLevel, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgFaceamt, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCashval, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgGuideamt, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgPlan, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgMarkt, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCompany, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgChdrnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgNewfield, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[1], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[2], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[3], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[4], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[5], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[6], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[7], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[8], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[9], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntnum[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgAgntrole[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProfile[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtst[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgCmsnrtrnl[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtst[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProdrtrnl[10], " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgTrandate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcvgrec.bnwcvgProcdate, " ");
		stringVariable1.setStringInto(wsaaString);
		/*A290-EXIT*/
	}

protected void a300ReadChdrenq()
	{
		/*A310-INIT*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(bprdIO.getCompany());
		chdrenqIO.setChdrnum(sqlChdrnum);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(formatsInner.chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*A390-EXIT*/
	}

protected void a400LeftShift()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a410Init();
					a420Loop();
				case a430Loop: 
					a430Loop();
				case a440Set: 
					a440Set();
				case a490Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410Init()
	{
		wsaaConvAmount.set(SPACES);
		wsaaNegativeSign.set("N");
		wsaaIx.set(0);
		compute(wsaaIy, 0).set(length(wsaaIntmDecimal) + 1);
		if (isEQ(wsaaInptAmount,ZERO)) {
			goTo(GotoLabel.a490Exit);
		}
		else {
			if (isGT(wsaaInptAmount,ZERO)) {
				wsaaIntmAmount.set(wsaaInptAmount);
			}
			else {
				compute(wsaaIntmAmount, 2).set(sub(0,wsaaInptAmount));
				negativeSign.setTrue();
			}
		}
	}

protected void a420Loop()
	{
		/* Adjust the integer part*/
		wsaaIx.add(1);
		if (isGT(wsaaIx,length(wsaaIntmInteger))) {
			goTo(GotoLabel.a490Exit);
		}
		if (isNE(subString(wsaaIntmInteger, wsaaIx, 1),"0")) {
			if (isNE(wsaaIntmDecimal,ZERO)) {
				goTo(GotoLabel.a430Loop);
			}
			else {
				wsaaIy.set(0);
				goTo(GotoLabel.a440Set);
			}
		}
		a420Loop();
		return ;
	}

protected void a430Loop()
	{
		/* Adjust the decimal part*/
		wsaaIy.subtract(1);
		if (isLT(wsaaIx,1)
		|| isNE(subString(wsaaIntmDecimal, wsaaIy, 1),"0")) {
			goTo(GotoLabel.a440Set);
		}
		a430Loop();
		return ;
	}

protected void a440Set()
	{
		compute(wsaaLength, 0).set(sub(length(wsaaIntmInteger) + 1,wsaaIx));
		if (negativeSign.isTrue()) {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("-");
				stringVariable1.addExpression(wsaaIntmInteger);
				stringVariable1.addExpression(".");
				stringVariable1.addExpression(wsaaIntmDecimal);
				stringVariable1.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression("-");
				stringVariable2.addExpression(wsaaIntmInteger);
				stringVariable2.setStringInto(wsaaConvAmount);
			}
		}
		else {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(wsaaIntmInteger);
				stringVariable3.addExpression(".");
				stringVariable3.addExpression(wsaaIntmDecimal);
				stringVariable3.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression(wsaaIntmInteger);
				stringVariable4.setStringInto(wsaaConvAmount);
			}
		}
	}

protected void a500GetBillfreq()
	{
		a500GetBillfreqPara();
		a519Call();
	}

protected void a500GetBillfreqPara()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrenqIO.getChdrcoy());
		payrIO.setChdrnum(chdrenqIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
	}

protected void a519Call()
	{
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*A590-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData pcddlnbrec = new FixedLengthStringData(10).init("PCDDLNBREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).init("COVRMJAREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
}
}
