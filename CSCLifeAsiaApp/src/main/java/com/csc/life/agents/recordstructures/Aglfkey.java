package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:07
 * Description:
 * Copybook name: AGLFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglfKey = new FixedLengthStringData(256).isAPartOf(aglfFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfAgntcoy = new FixedLengthStringData(1).isAPartOf(aglfKey, 0);
  	public FixedLengthStringData aglfAgntnum = new FixedLengthStringData(8).isAPartOf(aglfKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(aglfKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}