package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:36
 * Description:
 * Copybook name: TATWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tatwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData tatwFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData tatwKey = new FixedLengthStringData(64).isAPartOf(tatwFileKey, 0, REDEFINE);
  	public FixedLengthStringData tatwAgntcoy = new FixedLengthStringData(1).isAPartOf(tatwKey, 0);
  	public FixedLengthStringData tatwAgntnum = new FixedLengthStringData(8).isAPartOf(tatwKey, 1);
  	public PackedDecimalData tatwEffdate = new PackedDecimalData(8, 0).isAPartOf(tatwKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(tatwKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tatwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tatwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}