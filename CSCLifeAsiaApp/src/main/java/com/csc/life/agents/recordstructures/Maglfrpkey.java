package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:34
 * Description:
 * Copybook name: MAGLFRPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Maglfrpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData maglfrpFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData maglfrpKey = new FixedLengthStringData(256).isAPartOf(maglfrpFileKey, 0, REDEFINE);
  	public FixedLengthStringData maglfrpReportag = new FixedLengthStringData(8).isAPartOf(maglfrpKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(248).isAPartOf(maglfrpKey, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(maglfrpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		maglfrpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}