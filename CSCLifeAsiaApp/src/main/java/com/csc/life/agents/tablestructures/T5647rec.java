package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:32
 * Description:
 * Copybook name: T5647REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5647rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5647Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData commsubr = new FixedLengthStringData(7).isAPartOf(t5647Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(493).isAPartOf(t5647Rec, 7, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5647Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5647Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}