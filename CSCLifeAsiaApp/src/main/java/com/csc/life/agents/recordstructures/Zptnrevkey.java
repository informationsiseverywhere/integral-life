package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:19
 * Description:
 * Copybook name: ZPTNREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zptnrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zptnrevFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zptnrevKey = new FixedLengthStringData(256).isAPartOf(zptnrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData zptnrevChdrcoy = new FixedLengthStringData(1).isAPartOf(zptnrevKey, 0);
  	public FixedLengthStringData zptnrevChdrnum = new FixedLengthStringData(8).isAPartOf(zptnrevKey, 1);
  	public PackedDecimalData zptnrevTranno = new PackedDecimalData(5, 0).isAPartOf(zptnrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(zptnrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zptnrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zptnrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}