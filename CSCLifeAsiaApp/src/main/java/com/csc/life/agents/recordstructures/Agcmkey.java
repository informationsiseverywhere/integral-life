package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:00
 * Description:
 * Copybook name: AGCMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmKey = new FixedLengthStringData(64).isAPartOf(agcmFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmKey, 0);
  	public FixedLengthStringData agcmChdrnum = new FixedLengthStringData(8).isAPartOf(agcmKey, 1);
  	public FixedLengthStringData agcmAgntnum = new FixedLengthStringData(8).isAPartOf(agcmKey, 9);
  	public FixedLengthStringData agcmLife = new FixedLengthStringData(2).isAPartOf(agcmKey, 17);
  	public FixedLengthStringData agcmCoverage = new FixedLengthStringData(2).isAPartOf(agcmKey, 19);
  	public FixedLengthStringData agcmRider = new FixedLengthStringData(2).isAPartOf(agcmKey, 21);
  	public PackedDecimalData agcmPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}