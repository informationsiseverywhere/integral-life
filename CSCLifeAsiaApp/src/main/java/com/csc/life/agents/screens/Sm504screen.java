package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm504screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 1, 64}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm504ScreenVars sv = (Sm504ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm504screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm504ScreenVars screenVars = (Sm504ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.agntsel01.setClassString("");
		screenVars.agtname01.setClassString("");
		screenVars.agntsel02.setClassString("");
		screenVars.agtname02.setClassString("");
		screenVars.agntsel03.setClassString("");
		screenVars.agtname03.setClassString("");
	}

/**
 * Clear all the variables in Sm504screen
 */
	public static void clear(VarModel pv) {
		Sm504ScreenVars screenVars = (Sm504ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.userid.clear();
		screenVars.agntsel01.clear();
		screenVars.agtname01.clear();
		screenVars.agntsel02.clear();
		screenVars.agtname02.clear();
		screenVars.agntsel03.clear();
		screenVars.agtname03.clear();
	}
}
