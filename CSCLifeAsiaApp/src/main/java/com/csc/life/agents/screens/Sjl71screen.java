package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 10/02/20 05:41
 * @author Quipoz
 */
public class Sjl71screen extends ScreenRecord  { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl71ScreenVars sv = (Sjl71ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl71screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl71ScreenVars screenVars = (Sjl71ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.agncysel.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.salerepno.setClassString("");
		screenVars.action.setClassString("");
		screenVars.salebranch.setClassString("");
		screenVars.saledept.setClassString("");
	}

/**
 * Clear all the variables in Sjl71screen
 */
	public static void clear(VarModel pv) {
		Sjl71ScreenVars screenVars = (Sjl71ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.agncysel.clear();
		screenVars.userid.clear();
		screenVars.salerepno.clear();
		screenVars.action.clear();
		screenVars.salebranch.clear();
		screenVars.saledept.clear();
	}

}
