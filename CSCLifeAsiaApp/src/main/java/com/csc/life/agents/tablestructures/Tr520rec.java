package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:53
 * Description:
 * Copybook name: TR520REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr520rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr520Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tr520Rec, 0);
  	public FixedLengthStringData zcomcodes = new FixedLengthStringData(40).isAPartOf(tr520Rec, 8);
  	public FixedLengthStringData[] zcomcode = FLSArrayPartOfStructure(10, 4, zcomcodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(zcomcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zcomcode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData zcomcode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData zcomcode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData zcomcode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData zcomcode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData zcomcode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData zcomcode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData zcomcode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData zcomcode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData zcomcode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData zryrpercs = new FixedLengthStringData(350).isAPartOf(tr520Rec, 48);
  	public ZonedDecimalData[] zryrperc = ZDArrayPartOfStructure(70, 5, 2, zryrpercs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(350).isAPartOf(zryrpercs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zryrperc01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData zryrperc02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData zryrperc03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData zryrperc04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData zryrperc05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData zryrperc06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData zryrperc07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData zryrperc08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData zryrperc09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData zryrperc10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData zryrperc11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public ZonedDecimalData zryrperc12 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 55);
  	public ZonedDecimalData zryrperc13 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 60);
  	public ZonedDecimalData zryrperc14 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 65);
  	public ZonedDecimalData zryrperc15 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 70);
  	public ZonedDecimalData zryrperc16 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 75);
  	public ZonedDecimalData zryrperc17 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 80);
  	public ZonedDecimalData zryrperc18 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData zryrperc19 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 90);
  	public ZonedDecimalData zryrperc20 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 95);
  	public ZonedDecimalData zryrperc21 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 100);
  	public ZonedDecimalData zryrperc22 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 105);
  	public ZonedDecimalData zryrperc23 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 110);
  	public ZonedDecimalData zryrperc24 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 115);
  	public ZonedDecimalData zryrperc25 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 120);
  	public ZonedDecimalData zryrperc26 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 125);
  	public ZonedDecimalData zryrperc27 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 130);
  	public ZonedDecimalData zryrperc28 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 135);
  	public ZonedDecimalData zryrperc29 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 140);
  	public ZonedDecimalData zryrperc30 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 145);
  	public ZonedDecimalData zryrperc31 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 150);
  	public ZonedDecimalData zryrperc32 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 155);
  	public ZonedDecimalData zryrperc33 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 160);
  	public ZonedDecimalData zryrperc34 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 165);
  	public ZonedDecimalData zryrperc35 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 170);
  	public ZonedDecimalData zryrperc36 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 175);
  	public ZonedDecimalData zryrperc37 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 180);
  	public ZonedDecimalData zryrperc38 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 185);
  	public ZonedDecimalData zryrperc39 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 190);
  	public ZonedDecimalData zryrperc40 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 195);
  	public ZonedDecimalData zryrperc41 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 200);
  	public ZonedDecimalData zryrperc42 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 205);
  	public ZonedDecimalData zryrperc43 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 210);
  	public ZonedDecimalData zryrperc44 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 215);
  	public ZonedDecimalData zryrperc45 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 220);
  	public ZonedDecimalData zryrperc46 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 225);
  	public ZonedDecimalData zryrperc47 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 230);
  	public ZonedDecimalData zryrperc48 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 235);
  	public ZonedDecimalData zryrperc49 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 240);
  	public ZonedDecimalData zryrperc50 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 245);
  	public ZonedDecimalData zryrperc51 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 250);
  	public ZonedDecimalData zryrperc52 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 255);
  	public ZonedDecimalData zryrperc53 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 260);
  	public ZonedDecimalData zryrperc54 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 265);
  	public ZonedDecimalData zryrperc55 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 270);
  	public ZonedDecimalData zryrperc56 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 275);
  	public ZonedDecimalData zryrperc57 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 280);
  	public ZonedDecimalData zryrperc58 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 285);
  	public ZonedDecimalData zryrperc59 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 290);
  	public ZonedDecimalData zryrperc60 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 295);
  	public ZonedDecimalData zryrperc61 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 300);
  	public ZonedDecimalData zryrperc62 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 305);
  	public ZonedDecimalData zryrperc63 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 310);
  	public ZonedDecimalData zryrperc64 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 315);
  	public ZonedDecimalData zryrperc65 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 320);
  	public ZonedDecimalData zryrperc66 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 325);
  	public ZonedDecimalData zryrperc67 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 330);
  	public ZonedDecimalData zryrperc68 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 335);
  	public ZonedDecimalData zryrperc69 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 340);
  	public ZonedDecimalData zryrperc70 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 345);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(102).isAPartOf(tr520Rec, 398, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr520Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr520Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}