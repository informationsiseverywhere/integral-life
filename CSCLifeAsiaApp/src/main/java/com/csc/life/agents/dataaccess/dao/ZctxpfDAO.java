package com.csc.life.agents.dataaccess.dao;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZctxpfDAO extends BaseDAO<Zctxpf>{
	public Zctxpf readRecord(String chdrcoy,String chdrnum);
	public boolean updateRecord(Zctxpf zctxpf);
	public void insertZctxpfRecord(Zctxpf zctxpf);
	public boolean updateClient(Zctxpf zctxpf);
	public List<Zctxpf> getZctxpfRecord(String chdrnum);
	public Zctxpf readRecordByDate(Zctxpf zctxpf);
	public int getZctxpfCnt(String chdrcoy,String chdrnum);
	public List<Zctxpf> readContributionTaxList(String chdrcoy,String chdrnum) ;
	public boolean updateAmount(Zctxpf zctxpf);
	public Map<String, Zctxpf> readContributionDetails(String chdrcoy, List<String> chdrnum, int date, String procflag);
	public boolean insertRecord(List<Zctxpf> zctxpf);
	public boolean updateRecords(List<Zctxpf> zctxpf);
	public boolean updateCocolisc(String chdrcoy,String chdrnum, BigDecimal cocoamnt, BigDecimal liscamnt); //ILIFE-7899
	public List<Zctxpf> getRecord(String chdrcoy, String chdrnum,String startDate); //ILIFE-7916
	public List<Zctxpf> getzctxpfRecord(String chdrcoy, String chdrnum);  //ILIFE-7916	
}
