/*
 * File: Cmpy2rv.java
 * Date: 29 August 2009 22:41:37
 * Author: Quipoz Limited
 * 
 * Class transformed from CMPY2RV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ChdrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.life.agents.tablestructures.T5694rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*                  INSTALMENT RATE COMMISSION PAYMENT
*
* Obtain the  commission  payment  calculation  rules  (T5694),
* keyed by Coverage/rider  table code and effective date (hence
* use ITDM).
* Calculate 'To year'
*      The entries in this  table  are referenced by 'To year'.
*           To obtain the  year, calculate the year as follows,
*           obtain the Paid-to  Date  by  reading  the contract
*           header (CHDRCMC).
*      Use DATCON3  with a  frequency  of  1  and  passing  the
*           effective date  (commission/risk)  and  the Paid-to
*           date (PTD).  This  will  return the number of years
*           in existence and this value will be used to look up
*           the table entries in T5694, i.e.  the 'To year'.
* If the resulting rate  is  not zero, calculate the commission
* payable by applying the enhancements.
*      Firstly, access the enhancement rules table T5692, Keyed
*           on coverage/rider code  and  agent  class (READR on
*           AGLFCMC).  Based on the  age  of the life (READR on
*           LIFECMC with the key passed) access the appropriate
*           section in  the  commission  enhancement  table and
*           obtain the enhancement % of premium for the Renewal
*           Commission.  Apply this  enhancement  to the % rate
*           from T5694, also accessed by age band.
*      Secondly,  apply  the   above   adjusted   rate  to  the
*           instalment  premium  passed  in  the  linkage area.
*           This gives the commission amount.
*      Thirdly, apply  the  commission  enhancement  percentage
*           for renewal commission from T5692 to the commission
*           amount calculated  above.  This  gives the enhanced
*           renewal commission.
* The renewal commission should  now be returned in the linkage
* area as both the commission paid and earned.
*****************************************************************
* </pre>
*/
public class Cmpy2rv extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMPY2RV";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(6, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5692Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentClass = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 4).init(SPACES);
	private PackedDecimalData wsaaReprempc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaRwcmrate = new PackedDecimalData(5, 2);
	private ZonedDecimalData wsaaT5694 = new ZonedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaRenComm = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaMethod = new FixedLengthStringData(4).isAPartOf(wsaaItemitem, 0).init(SPACES);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaItemitem, 4).init(SPACES);
		/* ERRORS */
	private String h028 = "H028";
	private String lifecmcrec = "LIFECMCREC";
		/* TABLES */
	private String t5692 = "T5692";
	private String t5694 = "T5694";
		/*Contract Header Commission Calcs.*/
	private ChdrcmcTableDAM chdrcmcIO = new ChdrcmcTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*LIFE record Commission Calculations*/
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5692rec t5692rec = new T5692rec();
	private T5694rec t5694rec = new T5694rec();
	private Varcom varcom = new Varcom();
	private ExternalisedRules er = new ExternalisedRules();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		next1050, 
		bypassDatcon31080, 
		cont2010, 
		exit3090, 
		seExit8090, 
		dbExit8190
	}

	public Cmpy2rv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		obtainPaymntRules1000();
		if (isEQ(wsaaT5694,ZERO)) {
			goTo(GotoLabel.exit090);
		}
		calcRnwlCommission2000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainPaymntRules1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case next1050: {
					next1050();
				}
				case bypassDatcon31080: {
					bypassDatcon31080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		chdrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrcmcIO.setChdrnum(comlinkrec.chdrnum);
		chdrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrcmcIO);
		if (isNE(chdrcmcIO.getStatuz(),varcom.oK)
		&& isNE(chdrcmcIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrcmcIO.getParams());
			syserrrec.statuz.set(chdrcmcIO.getStatuz());
			dbError8100();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5694);
		wsaaMethod.set(comlinkrec.method);
		wsaaSrcebus.set(chdrcmcIO.getSrcebus());
		itdmIO.setItemitem(wsaaItemitem);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
	}

protected void next1050()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5694)
		|| isNE(itdmIO.getItemitem(),wsaaItemitem)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			if (isNE(itdmIO.getItemitem(),wsaaItemitem)
			&& isNE(wsaaSrcebus,"**")) {
				itdmIO.setDataKey(SPACES);
				itdmIO.setItemcoy(comlinkrec.chdrcoy);
				itdmIO.setItemtabl(t5694);
				wsaaMethod.set(comlinkrec.method);
				wsaaSrcebus.set("**");
				itdmIO.setItemitem(wsaaItemitem);
				itdmIO.setItmfrm(comlinkrec.effdate);
				itdmIO.setFunction(varcom.begn);
				goTo(GotoLabel.next1050);
			}
			itdmIO.setItemcoy(comlinkrec.chdrcoy);
			itdmIO.setItemtabl(t5694);
			itdmIO.setItemitem(wsaaItemitem);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h028);
			systemError8000();
		}
		else {
			t5694rec.t5694Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(comlinkrec.billfreq,"00")) {
			wsaaTerm.set(0);
			goTo(GotoLabel.bypassDatcon31080);
		}
		datcon3rec.intDate2.set(chdrcmcIO.getPtdate());
		if (isNE(comlinkrec.ptdate,ZERO)) {
			datcon3rec.intDate2.set(comlinkrec.ptdate);
		}
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		compute(wsaaTerm, 5).set(add(datcon3rec.freqFactor,1.0));
	}

protected void bypassDatcon31080()
	{
		wsaaT5694.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,11)); wsaaIndex.add(1)){
			getT56944000();
		}
		/*EXIT*/
	}

protected void calcRnwlCommission2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para2000();
				}
				case cont2010: {
					cont2010();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2000()
	{
		itdmIO.setDataKey(SPACES);
		wsaaCrtable.set(comlinkrec.crtable);
		wsaaAgentClass.set(comlinkrec.agentClass);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5692);
		itdmIO.setItemitem(wsaaT5692Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		&& isEQ(itdmIO.getItemtabl(),t5692)
		&& isEQ(itdmIO.getItemitem(),wsaaT5692Key)) {
			t5692rec.t5692Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont2010);
		}
		itdmIO.setItemtabl(t5692);
		wsaaCrtable.set("****");
		itdmIO.setItemitem(wsaaT5692Key);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)
		&& isEQ(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		&& isEQ(itdmIO.getItemtabl(),t5692)
		&& isEQ(itdmIO.getItemitem(),wsaaT5692Key)) {
			t5692rec.t5692Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont2010);
		}
		t5692rec.age01.set(ZERO);
		t5692rec.age02.set(ZERO);
		t5692rec.age03.set(ZERO);
		t5692rec.age04.set(ZERO);
	}

protected void cont2010()
	{
		lifecmcIO.setDataArea(SPACES);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife,SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		lifecmcIO.setFormat(lifecmcrec);
		lifecmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifecmcIO.getParams());
			dbError8100();
		}
		getT5692Value3000();	
		// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization start
		/*ILIFE-7965 : Starts*/
		boolean gstOnCommFlag = FeaConfg.isFeatureExist(chdrcmcIO.getChdrcoy().toString().trim(), "CTISS007", appVars, "IT");
		if(gstOnCommFlag && AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMCMCPMT")
				&& er.isExternalized(chdrcmcIO.cnttype.toString(), comlinkrec.crtable.toString())){	
			comlinkrec.ptdate.set(chdrcmcIO.getPtdate());
			comlinkrec.srcebus.set("");
			comlinkrec.cnttype.set(chdrcmcIO.cnttype);
			comlinkrec.cltdob.set(lifecmcIO.cltdob);
			comlinkrec.anbccd.set(lifecmcIO.anbAtCcd);
			if (isEQ(comlinkrec.agentype, SPACES))
				comlinkrec.agentype.set("**");
			callProgram("VPMCMCPMT", comlinkrec.clnkallRec);
		}else{
			if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMCMCPMT")
					&& er.isExternalized(chdrcmcIO.cnttype.toString(), comlinkrec.crtable.toString())
					&& isEQ(comlinkrec.coverage, "01") && isEQ(comlinkrec.rider, "00")
					&& isNE(comlinkrec.method, "RCP7"))) {
				compute(wsaaT5694, 3).setRounded(div(mult(wsaaT5694, wsaaReprempc), 100));
				compute(wsaaRenComm, 3).setRounded(div(mult(comlinkrec.instprem, wsaaT5694), 100));
				compute(wsaaRenComm, 3).setRounded(mult(wsaaRenComm, (div(wsaaRwcmrate, 100))));
				comlinkrec.payamnt.set(wsaaRenComm);
				comlinkrec.erndamt.set(wsaaRenComm);
			} else {
				comlinkrec.ptdate.set(chdrcmcIO.getPtdate());
				comlinkrec.srcebus.set("");
				comlinkrec.cnttype.set(chdrcmcIO.cnttype);
				comlinkrec.cltdob.set(lifecmcIO.cltdob);
				comlinkrec.anbccd.set(lifecmcIO.anbAtCcd);
				if (isEQ(comlinkrec.agentype, SPACES))
					comlinkrec.agentype.set("**");
				callProgram("VPMCMCPMT", comlinkrec.clnkallRec);
			}
		}
		/*ILIFE-7965 : Ends*/
			}
	// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization end
	 

protected void getT5692Value3000()
	{
		try {
			para3000();
		}
		catch (GOTOException e){
		}
	}

protected void para3000()
	{
		wsaaReprempc.set(100);
		wsaaRwcmrate.set(100);
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age01)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate01);
			wsaaReprempc.set(t5692rec.reprempc01);
			goTo(GotoLabel.exit3090);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age02)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate02);
			wsaaReprempc.set(t5692rec.reprempc02);
			goTo(GotoLabel.exit3090);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age03)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate03);
			wsaaReprempc.set(t5692rec.reprempc03);
			goTo(GotoLabel.exit3090);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age04)) {
			wsaaRwcmrate.set(t5692rec.rwcmrate04);
			wsaaReprempc.set(t5692rec.reprempc04);
			goTo(GotoLabel.exit3090);
		}
	}

protected void getT56944000()
	{
		/*PARA*/
		if (isLTE(wsaaTerm,t5694rec.toYear[wsaaIndex.toInt()])) {
			wsaaT5694.set(t5694rec.instalpc[wsaaIndex.toInt()]);
			wsaaIndex.set(13);
		}
		/*EXIT*/
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
