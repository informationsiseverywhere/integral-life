package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5695screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5695ScreenVars sv = (S5695ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5695screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5695ScreenVars screenVars = (S5695ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.freqFrom01.setClassString("");
		screenVars.freqTo01.setClassString("");
		screenVars.cmrate01.setClassString("");
		screenVars.premaxpc01.setClassString("");
		screenVars.commenpc01.setClassString("");
		screenVars.freqFrom02.setClassString("");
		screenVars.freqTo02.setClassString("");
		screenVars.cmrate02.setClassString("");
		screenVars.premaxpc02.setClassString("");
		screenVars.commenpc02.setClassString("");
		screenVars.freqFrom03.setClassString("");
		screenVars.freqTo03.setClassString("");
		screenVars.cmrate03.setClassString("");
		screenVars.premaxpc03.setClassString("");
		screenVars.commenpc03.setClassString("");
		screenVars.freqFrom04.setClassString("");
		screenVars.freqTo04.setClassString("");
		screenVars.cmrate04.setClassString("");
		screenVars.premaxpc04.setClassString("");
		screenVars.commenpc04.setClassString("");
		screenVars.freqFrom05.setClassString("");
		screenVars.freqTo05.setClassString("");
		screenVars.cmrate05.setClassString("");
		screenVars.premaxpc05.setClassString("");
		screenVars.commenpc05.setClassString("");
		screenVars.freqFrom06.setClassString("");
		screenVars.freqTo06.setClassString("");
		screenVars.cmrate06.setClassString("");
		screenVars.premaxpc06.setClassString("");
		screenVars.commenpc06.setClassString("");
		screenVars.freqFrom07.setClassString("");
		screenVars.freqTo07.setClassString("");
		screenVars.cmrate07.setClassString("");
		screenVars.premaxpc07.setClassString("");
		screenVars.commenpc07.setClassString("");
		screenVars.freqFrom08.setClassString("");
		screenVars.freqTo08.setClassString("");
		screenVars.cmrate08.setClassString("");
		screenVars.premaxpc08.setClassString("");
		screenVars.commenpc08.setClassString("");
		screenVars.freqFrom09.setClassString("");
		screenVars.freqTo09.setClassString("");
		screenVars.cmrate09.setClassString("");
		screenVars.premaxpc09.setClassString("");
		screenVars.commenpc09.setClassString("");
		screenVars.freqFrom10.setClassString("");
		screenVars.freqTo10.setClassString("");
		screenVars.cmrate10.setClassString("");
		screenVars.premaxpc10.setClassString("");
		screenVars.commenpc10.setClassString("");
		screenVars.freqFrom11.setClassString("");
		screenVars.freqTo11.setClassString("");
		screenVars.cmrate11.setClassString("");
		screenVars.premaxpc11.setClassString("");
		screenVars.commenpc11.setClassString("");
	}

/**
 * Clear all the variables in S5695screen
 */
	public static void clear(VarModel pv) {
		S5695ScreenVars screenVars = (S5695ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.freqFrom01.clear();
		screenVars.freqTo01.clear();
		screenVars.cmrate01.clear();
		screenVars.premaxpc01.clear();
		screenVars.commenpc01.clear();
		screenVars.freqFrom02.clear();
		screenVars.freqTo02.clear();
		screenVars.cmrate02.clear();
		screenVars.premaxpc02.clear();
		screenVars.commenpc02.clear();
		screenVars.freqFrom03.clear();
		screenVars.freqTo03.clear();
		screenVars.cmrate03.clear();
		screenVars.premaxpc03.clear();
		screenVars.commenpc03.clear();
		screenVars.freqFrom04.clear();
		screenVars.freqTo04.clear();
		screenVars.cmrate04.clear();
		screenVars.premaxpc04.clear();
		screenVars.commenpc04.clear();
		screenVars.freqFrom05.clear();
		screenVars.freqTo05.clear();
		screenVars.cmrate05.clear();
		screenVars.premaxpc05.clear();
		screenVars.commenpc05.clear();
		screenVars.freqFrom06.clear();
		screenVars.freqTo06.clear();
		screenVars.cmrate06.clear();
		screenVars.premaxpc06.clear();
		screenVars.commenpc06.clear();
		screenVars.freqFrom07.clear();
		screenVars.freqTo07.clear();
		screenVars.cmrate07.clear();
		screenVars.premaxpc07.clear();
		screenVars.commenpc07.clear();
		screenVars.freqFrom08.clear();
		screenVars.freqTo08.clear();
		screenVars.cmrate08.clear();
		screenVars.premaxpc08.clear();
		screenVars.commenpc08.clear();
		screenVars.freqFrom09.clear();
		screenVars.freqTo09.clear();
		screenVars.cmrate09.clear();
		screenVars.premaxpc09.clear();
		screenVars.commenpc09.clear();
		screenVars.freqFrom10.clear();
		screenVars.freqTo10.clear();
		screenVars.cmrate10.clear();
		screenVars.premaxpc10.clear();
		screenVars.commenpc10.clear();
		screenVars.freqFrom11.clear();
		screenVars.freqTo11.clear();
		screenVars.cmrate11.clear();
		screenVars.premaxpc11.clear();
		screenVars.commenpc11.clear();
	}
}
