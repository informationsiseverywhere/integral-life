package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52Y
 * @version 1.0 generated on 11/22/13 6:36 AM
 * @author CSC
 */
public class Sr52yScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(142);
	public FixedLengthStringData dataFields = new FixedLengthStringData(46).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData multind = DD.multind.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData relto = DD.relto.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 46);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData multindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData reltoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 70);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] multindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] reltoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52yscreenWritten = new LongData(0);
	public LongData Sr52yprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52yScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(multindOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reltoOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, multind, relto};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, multindOut, reltoOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, multindErr, reltoErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52yscreen.class;
		protectRecord = Sr52yprotect.class;
	}

}
