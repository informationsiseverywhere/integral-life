/*
 * File: P5040.java
 * Date: 29 August 2009 23:59:24
 * Author: Quipoz Limited
 * 
 * Class transformed from P5040.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AgbnTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglfrptTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.S5040ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
*  Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*  information.
*
*  An agent may have  one,  none or many agents reporting to it.
*  Agent records have  a  reporting  agent  number  which may be
*  optionally entered.  Using the agent number from AGLF passed,
*  read the  AGLFRPT logical file sequentially, loading one page
*  worth of reporting  agents  (or  less  if  the  agent  number
*  changes). If not the  end,  read  the next record and set the
*  subfile more flag  if  it reports to the required agent (i.e.
*  the one off AGLF).
*
*
* Validation, Updating
* --------------------
*
*  Standard processing.
*
*
* Next program
* ------------
*
*  If roll up  was  pressed,  load  the  next  page of reporting
*  agents   and   re-present   the   screen   (SCRN-SCRNAME   to
*  WSSP-NEXTPROG).
*
*  Otherwise, add  one  and  exit  (with  WSSP-NEXTPROG  set  to
*  WSAA-PROG).
*
*****************************************************************
* </pre>
*/
public class P5040 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5040");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDataKey = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaAgntpfx = new FixedLengthStringData(2).isAPartOf(wsaaDataKey, 0);
	private FixedLengthStringData wsaaAgntcoy = new FixedLengthStringData(1).isAPartOf(wsaaDataKey, 2);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaDataKey, 3);
	private PackedDecimalData wsaaLineCount = new PackedDecimalData(3, 0).setUnsigned();
		/* TABLES */
	private String t3692 = "T3692";
		/*LIFE AGENT / BROKER CONTACT NAMES*/
	private AgbnTableDAM agbnIO = new AgbnTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Logical File Layout*/
	private AglfrptTableDAM aglfrptIO = new AglfrptTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	
	private S5040ScreenVars sv = getLScreenVars();

	protected S5040ScreenVars getLScreenVars(){
		return ScreenProgram.getScreenVars( S5040ScreenVars.class);
	}
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1190, 
		preExit, 
		exit2090
	}

	public P5040() {
		super();
		screenVars = sv;
		new ScreenModel("S5040", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		screenHeader1020();
		headerAgentType1030();
		subfileProcess1040();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5040", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void screenHeader1020()
	{
		aglfIO.setFunction("RETRV");
		callAglf1200();
		agntlagIO.setDataKey(SPACES);
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		sv.agnum.set(aglfIO.getAgntnum());
		callAgntlag1400();
		sv.clntsel.set(agntlagIO.getClntnum());
		callClts1500();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void headerAgentType1030()
	{
		sv.agtype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntlagIO.getAgtype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.agtydesc.set(descIO.getLongdesc());
	}

protected void subfileProcess1040()
	{
		scrnparams.subfileMore.set("Y");
		aglfrptIO.setFunction(varcom.begn);
		wsaaLineCount.set(1);
		while ( !(isEQ(scrnparams.subfileMore,"N")
		|| isGT(wsaaLineCount,14))) {
			loadSubfile1100();
		}
		
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

protected void loadSubfile1100()
	{
		try {
			readReportagLf1110();
			hasReportingAgents1120();
			addToSubfile1130();
		}
		catch (GOTOException e){
		}
	}

protected void readReportagLf1110()
	{
		aglfrptIO.setDataKey(SPACES);
		wsaaAgntpfx.set("AG");
		wsaaAgntcoy.set(aglfIO.getAgntcoy());
		wsaaAgntnum.set(aglfIO.getAgntnum());
		aglfrptIO.setRepagent01(wsaaDataKey);
		callAglfrpt1300();
		if (isEQ(aglfrptIO.getStatuz(),varcom.endp)) {
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit1190);
		}
		if (isNE(aglfrptIO.getLifagnt(),"Y")) {
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(aglfrptIO.getAgntnum(),aglfIO.getAgntnum())) {
			goTo(GotoLabel.exit1190);
		}
		sv.reportag.set(aglfrptIO.getAgntnum());
	}

protected void hasReportingAgents1120()
	{
		readAglfOvcpc1600();
		agntlagIO.setDataKey(SPACES);
		agntlagIO.setAgntnum(aglfrptIO.getAgntnum());
		callAgntlag1400();
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		sv.clntnum.set(agntlagIO.getClntnum());
		callClts1500();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void addToSubfile1130()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("S5040", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaLineCount.add(1);
	}

protected void callAglf1200()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callAglfrpt1300()
	{
		/*CALL*/
		aglfrptIO.setAgntcoy(wsspcomn.company);
		SmartFileCode.execute(appVars, aglfrptIO);
		if (isNE(aglfrptIO.getStatuz(),varcom.oK)
		&& isNE(aglfrptIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(aglfrptIO.getParams());
			fatalError600();
		}
		if (isNE(wsaaDataKey,aglfrptIO.getRepagent01())) {
			aglfrptIO.setStatuz(varcom.endp);
		}
		aglfrptIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void callAgntlag1400()
	{
		/*READR-AGENT-FILE*/
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClts1500()
	{
		/*READ-CLIENT-FILE*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		/*EXIT*/
	}

protected void readAglfOvcpc1600()
	{
		/*READ-AGLF*/
		aglfIO.setAgntnum(aglfrptIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		callAglf1200();
		sv.ovcpc.set(aglfIO.getOvcpc());
		aglfIO.setFunction("RETRV");
		callAglf1200();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			scrnparams.subfileMore.set("Y");
			wsaaLineCount.set(1);
			while ( !(isEQ(scrnparams.subfileMore,"N")
			|| isGT(wsaaLineCount,14))) {
				loadSubfile1100();
			}
			
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
