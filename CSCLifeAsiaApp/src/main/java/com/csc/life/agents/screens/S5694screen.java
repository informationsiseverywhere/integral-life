package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5694screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5694ScreenVars sv = (S5694ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5694screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5694ScreenVars screenVars = (S5694ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.toYear01.setClassString("");
		screenVars.instalpc01.setClassString("");
		screenVars.toYear02.setClassString("");
		screenVars.instalpc02.setClassString("");
		screenVars.toYear03.setClassString("");
		screenVars.instalpc03.setClassString("");
		screenVars.toYear04.setClassString("");
		screenVars.instalpc04.setClassString("");
		screenVars.toYear05.setClassString("");
		screenVars.instalpc05.setClassString("");
		screenVars.toYear06.setClassString("");
		screenVars.instalpc06.setClassString("");
		screenVars.toYear07.setClassString("");
		screenVars.instalpc07.setClassString("");
		screenVars.toYear08.setClassString("");
		screenVars.instalpc08.setClassString("");
		screenVars.toYear09.setClassString("");
		screenVars.instalpc09.setClassString("");
		screenVars.toYear10.setClassString("");
		screenVars.instalpc10.setClassString("");
		screenVars.toYear11.setClassString("");
		screenVars.instalpc11.setClassString("");
	}

/**
 * Clear all the variables in S5694screen
 */
	public static void clear(VarModel pv) {
		S5694ScreenVars screenVars = (S5694ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.toYear01.clear();
		screenVars.instalpc01.clear();
		screenVars.toYear02.clear();
		screenVars.instalpc02.clear();
		screenVars.toYear03.clear();
		screenVars.instalpc03.clear();
		screenVars.toYear04.clear();
		screenVars.instalpc04.clear();
		screenVars.toYear05.clear();
		screenVars.instalpc05.clear();
		screenVars.toYear06.clear();
		screenVars.instalpc06.clear();
		screenVars.toYear07.clear();
		screenVars.instalpc07.clear();
		screenVars.toYear08.clear();
		screenVars.instalpc08.clear();
		screenVars.toYear09.clear();
		screenVars.instalpc09.clear();
		screenVars.toYear10.clear();
		screenVars.instalpc10.clear();
		screenVars.toYear11.clear();
		screenVars.instalpc11.clear();
	}
}
