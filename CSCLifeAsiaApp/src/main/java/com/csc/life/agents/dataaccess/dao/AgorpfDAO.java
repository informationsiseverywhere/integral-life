/******************************************************************************
 * File Name 		: AgorpfDAO.java
 * Author			: smalchi2
 * Creation Date	: 12 January 2017
 * Project			: Integral Life
 * Description		: The DAO Interface for AGORPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.life.agents.dataaccess.model.Agorpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AgorpfDAO extends BaseDAO<Agorpf> {

	public List<Agorpf> searchAgorpfRecord(Agorpf agorpf) throws SQLRuntimeException;

	public List<Agorpf> searchAllAgorpfRecurRecord(Agorpf agorpf);
}
