/*
 * File: Tm604pt.java
 * Date: 30 August 2009 2:38:30
 * Author: Quipoz Limited
 * 
 * Class transformed from TM604PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Tm604rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TM604.
*
*
*****************************************************************
* </pre>
*/
public class Tm604pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine001, 71, FILLER).init("SM535");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(71);
	private FixedLengthStringData filler9 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 7, FILLER).init("Report Title:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine004, 21);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(24);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine005, 7, FILLER).init("Key Result Areas:");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(50);
	private FixedLengthStringData filler13 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 12, FILLER).init("- Min");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine006, 18).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 36, FILLER).init(" FYP for Group");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(57);
	private FixedLengthStringData filler16 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 14, FILLER).init("and");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 18).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine007, 36, FILLER).init(" FYP for Direct Agent");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(31);
	private FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 12, FILLER).init("- Min");
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(7, 0).isAPartOf(wsaaPrtLine008, 18).setPattern("ZZZZZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 25, FILLER).init(" Cases");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(17);
	private FixedLengthStringData filler22 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" F1=Help  F3=Exit");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tm604rec tm604rec = new Tm604rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tm604pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tm604rec.tm604Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo008.set(tm604rec.prem01);
		fieldNo007.set(tm604rec.rpthead);
		fieldNo010.set(tm604rec.numofmbr);
		fieldNo009.set(tm604rec.prem02);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
