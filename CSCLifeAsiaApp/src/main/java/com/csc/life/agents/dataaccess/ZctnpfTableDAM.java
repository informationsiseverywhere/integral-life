package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZctnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:59
 * Class transformed from ZCTNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZctnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 107;
	public FixedLengthStringData zctnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zctnpfRecord = zctnrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zctnrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zctnrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zctnrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zctnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zctnrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zctnrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zctnrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zctnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zctnrec);
	public FixedLengthStringData transCode = DD.trcde.copy().isAPartOf(zctnrec);
	public PackedDecimalData commAmt = DD.cmamt.copy().isAPartOf(zctnrec);
	public PackedDecimalData premium = DD.premium.copy().isAPartOf(zctnrec);
	public PackedDecimalData splitBcomm = DD.splitc.copy().isAPartOf(zctnrec);
	public FixedLengthStringData zprflg = DD.zprflg.copy().isAPartOf(zctnrec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(zctnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zctnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zctnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zctnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZctnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZctnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZctnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZctnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZctnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZctnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZctnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZCTNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"EFFDATE, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TRANNO, " +
							"TRCDE, " +
							"CMAMT, " +
							"PREMIUM, " +
							"SPLITC, " +
							"ZPRFLG, " +
							"TRANDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     effdate,
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     tranno,
                                     transCode,
                                     commAmt,
                                     premium,
                                     splitBcomm,
                                     zprflg,
                                     trandate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		effdate.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		tranno.clear();
  		transCode.clear();
  		commAmt.clear();
  		premium.clear();
  		splitBcomm.clear();
  		zprflg.clear();
  		trandate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZctnrec() {
  		return zctnrec;
	}

	public FixedLengthStringData getZctnpfRecord() {
  		return zctnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZctnrec(what);
	}

	public void setZctnrec(Object what) {
  		this.zctnrec.set(what);
	}

	public void setZctnpfRecord(Object what) {
  		this.zctnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zctnrec.getLength());
		result.set(zctnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}