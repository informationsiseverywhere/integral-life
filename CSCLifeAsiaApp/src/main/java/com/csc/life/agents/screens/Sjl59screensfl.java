package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl59screensfl extends Subfile {


	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {13, 1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl59ScreenVars sv = (Sjl59ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl59screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl59screensfl, 
			sv.Sjl59screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl59ScreenVars sv = (Sjl59ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl59screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl59ScreenVars sv = (Sjl59ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl59screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl59screensflWritten.gt(0))
		{
			sv.sjl59screensfl.setCurrentIndex(0);
			sv.Sjl59screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl59ScreenVars sv = (Sjl59ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl59screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}
	
	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl59ScreenVars screenVars = (Sjl59ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.clntsel.setFieldName("clntsel");
				screenVars.cltname.setFieldName("cltname");
				screenVars.levelno.setFieldName("levelno");				
				screenVars.leveltype.setFieldName("leveltype");				
				screenVars.leveldes.setFieldName("leveldes");
				screenVars.agtype.setFieldName("agtype");
				screenVars.userid.setFieldName("userid");
				screenVars.status.setFieldName("status");				
				screenVars.regclass.setFieldName("regclass");
				screenVars.regdateDisp.setFieldName("regdateDisp");
				screenVars.reasonreg.setFieldName("reasonreg");
				screenVars.resndetl.setFieldName("resndetl");			
				screenVars.agtdesc.setFieldName("agtdesc");	
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.slt.set(dm.getField("slt"));
			screenVars.clntsel.set(dm.getField("clntsel"));
			screenVars.cltname.set(dm.getField("cltname"));
			screenVars.levelno.set(dm.getField("levelno"));
			screenVars.leveltype.set(dm.getField("leveltype"));				
			screenVars.leveldes.set(dm.getField("leveldes"));
			screenVars.agtype.set(dm.getField("agtype"));
			screenVars.userid.set(dm.getField("userid"));	
			screenVars.status.set(dm.getField("status"));	
			screenVars.regclass.set(dm.getField("regclass"));
			screenVars.regdateDisp.set(dm.getField("regdateDisp"));
			screenVars.reasonreg.set(dm.getField("reasonreg"));
			screenVars.resndetl.set(dm.getField("resndetl"));
			screenVars.agtdesc.set(dm.getField("agtdesc"));
		}
	}
	
	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl59ScreenVars screenVars = (Sjl59ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.clntsel.setFieldName("clntsel");
				screenVars.cltname.setFieldName("cltname");
				screenVars.levelno.setFieldName("levelno");				
				screenVars.leveltype.setFieldName("leveltype");				
				screenVars.leveldes.setFieldName("leveldes");
				screenVars.agtype.setFieldName("agtype");
				screenVars.userid.setFieldName("userid");
				screenVars.status.setFieldName("status");				
				screenVars.regclass.setFieldName("regclass");
				screenVars.regdateDisp.setFieldName("regdateDisp");
				screenVars.reasonreg.setFieldName("reasonreg");
				screenVars.resndetl.setFieldName("resndetl");				
				screenVars.agtdesc.setFieldName("agtdesc");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("slt").set(screenVars.slt);
			dm.getField("clntsel").set(screenVars.clntsel);
			dm.getField("cltname").set(screenVars.cltname);
			dm.getField("levelno").set(screenVars.levelno);
			dm.getField("leveltype").set(screenVars.leveltype);
			dm.getField("leveldes").set(screenVars.leveldes);
			dm.getField("agtype").set(screenVars.agtype);
			dm.getField("userid").set(screenVars.userid);
			dm.getField("status").set(screenVars.status);
			dm.getField("regclass").set(screenVars.regclass);
			dm.getField("regdateDisp").set(screenVars.regdateDisp);
			dm.getField("reasonreg").set(screenVars.reasonreg);
			dm.getField("resndetl").set(screenVars.resndetl);
			dm.getField("agtdesc").set(screenVars.agtdesc);
		}

	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl59screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl59ScreenVars screenVars = (Sjl59ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.slt.clearFormatting();
		screenVars.clntsel.clearFormatting();
		screenVars.cltname.clearFormatting();
		screenVars.levelno.clearFormatting();
		screenVars.leveltype.clearFormatting();				
		screenVars.leveldes.clearFormatting();
		screenVars.agtype.clearFormatting();
		screenVars.userid.clearFormatting();
		screenVars.status.clearFormatting();				
		screenVars.regclass.clearFormatting();
		screenVars.regdateDisp.clearFormatting();
		screenVars.reasonreg.clearFormatting();
		screenVars.resndetl.clearFormatting();
		screenVars.agtdesc.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl59ScreenVars screenVars = (Sjl59ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.slt.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.levelno.setClassString("");
		screenVars.leveltype.setClassString("");				
		screenVars.leveldes.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.status.setClassString("");
		screenVars.regclass.setClassString("");
		screenVars.regdateDisp.setClassString("");				
		screenVars.reasonreg.setClassString("");
		screenVars.resndetl.setClassString("");
		screenVars.agtdesc.setClassString("");
	}

/**
 * Clear all the variables in Sjl59screensfl
 */
	public static void clear(VarModel pv) {
		Sjl59ScreenVars screenVars = (Sjl59ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.slt.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.levelno.clear();		
		screenVars.leveltype.clear();				
		screenVars.leveldes.clear();
		screenVars.agtype.clear();
		screenVars.userid.clear();
		screenVars.status.clear();
		screenVars.regclass.clear();		
		screenVars.regdateDisp.clear();				
		screenVars.reasonreg.clear();
		screenVars.resndetl.clear();
		screenVars.agtdesc.clear();
	}
}