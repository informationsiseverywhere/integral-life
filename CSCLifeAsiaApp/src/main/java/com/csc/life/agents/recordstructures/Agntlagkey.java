package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:08
 * Description:
 * Copybook name: AGNTLAGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agntlagkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agntlagFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agntlagKey = new FixedLengthStringData(256).isAPartOf(agntlagFileKey, 0, REDEFINE);
  	public FixedLengthStringData agntlagAgntcoy = new FixedLengthStringData(1).isAPartOf(agntlagKey, 0);
  	public FixedLengthStringData agntlagAgntnum = new FixedLengthStringData(8).isAPartOf(agntlagKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(agntlagKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agntlagFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agntlagFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}