/*
 * File: P5043bo.java
 * Date: 30 August 2009 0:00:02
 * Author: Quipoz Limited
 * 
 * Class transformed from P5043BO.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Sdartnrec;
import com.csc.life.agents.programs.P5043;
import com.csc.life.agents.screens.S5043ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Boverrrec;
import com.csc.smart.recordstructures.Ldrhdr;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Sessiono;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Mainx;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.ExitSection;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*              P5043 Business Object
*
*  This object processes only one method
*
*         INQ - Inquiry
*
***********************************************************************
* </pre>
*/
public class P5043bo  extends Mainx{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(12).init("P5043BO     ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
//	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Ldrhdr ldrhdr = new Ldrhdr();
	private Msgdta requestData = new Msgdta();
	private Msgdta responseData = new Msgdta();
//	private Scrnparams scrnparams = new Scrnparams();
	private Sdartnrec sdartnrec = new Sdartnrec();
	private Sessiono sessiono = new Sessiono();
	private Varcom varcom = new Varcom();
	private Boverrrec validationError = new Boverrrec();
	private Ldrhdr wsaaLdrhdr = new Ldrhdr();
	private Msgdta wsaaRequest = new Msgdta();
	private Msgdta wsaaResponse = new Msgdta();
//	private Wsspcomn wsspcomn = new Wsspcomn();
	private S5043ScreenVars sv1 = ScreenProgram.getScreenVars( S5043ScreenVars.class);
    //add by Tom Chi
	private P5043 p5043;
	//end
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		aExit, 
		exit8190
	}
	public P5043bo() {
		super();
		wsaaProg = new FixedLengthStringData(12).init("P5043BO     ");
		wsaaVersion = new FixedLengthStringData(2).init("01");		
	}

public void mainline(Object... parmArray)
	{
		sdartnrec.rec = convertAndSetParam(sdartnrec.rec, parmArray, 2);
		responseData.data = convertAndSetParam(responseData.data, parmArray, 1);
		ldrhdr.header = convertAndSetParam(ldrhdr.header, parmArray, 0);
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
		}
	}

private void process()
	{
//		initialise1000();
//		callPrograms2000();
//		aInquiry();
//		updateWsspTemp8000();
//		updateWssp9000();
		super.mainline();
	}

protected void initialise1000()
	{
		/*
		 * add by Tom chi
		 * initialize the scrnparams, it will used by validating input(such as P2465.processBo)
		 */
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.company.set(wsspcomn.company);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		scrnparams.language.set(wsspcomn.language);
	    //end	
		/*START*/
		scrnparams.statuz.set(varcom.oK);
		ldrhdr.errlvl.set("0");
		/*EXIT*/
	}

protected void callPrograms2000()
	{
		/*CALL*/
		wsspcomn.edterror.set(varcom.oK);
		aInquiry();
		/*EXIT*/
	}

protected void aInquiry()
	{
		aP5043Start();
	}

protected void aP5043Start()
	{
		wsspcomn.sectionno.set("1000");
		p5043 = new P5043();
		p5043.processBo(new Object[] {wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea,null});		
//		callProgram(P5043.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsspcomn.sectionno.set("PRE");
		p5043.processBo(new Object[] {wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea,null});
		//callProgram(P5043.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsspcomn.sbmaction.set("G");
		scrnparams.action.set("G");
		sv1.action.set("G");
		wsspcomn.company.set(sdartnrec.sdarCompany);
		sv1.agntsel.set(sdartnrec.sdarInKey);
		wsspcomn.sectionno.set("2000");
		p5043.processBo(new Object[] {wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea,"5043",new S5043ScreenVars()});	
		setErrorCode(sv1);  		
//		callProgram(P5043.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		if (isNE(sv1.agntselErr,SPACES)) {
			sdartnrec.sdarStatuz.set(sv1.agntselErr);
		}
		if (isNE(sv1.actionErr,SPACES)) {
			sdartnrec.sdarStatuz.set(sv1.actionErr);
		}
		 /*
		 * tom chi
		 */
		try {
			setupValidatedError(ldrhdr, validationError, responseData);
		} catch (ExitSection e) {
			goTo(GotoLabel.aExit); 
		}
		wsspcomn.sectionno.set("3000");
		p5043.processBo(new Object[] {wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea,null});
//		callProgram(P5043.class, wsspcomn.commonArea, wsspUserArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		sdartnrec.sdarBatchkey.set(wsspcomn.batchkey);
	}

protected void updateWsspTemp8000()
	{
		/*PARA*/
		sessiono.wssp.set(wsspcomn.commonArea);
		wsaaResponse.set(sessiono.rec);
		wsaaLdrhdr.set(ldrhdr.header);
		wsaaLdrhdr.objid.set("SESSION");
		wsaaLdrhdr.vrbid.set("UPD  ");
		callProgram("SESSION", wsaaLdrhdr, wsaaRequest, wsaaResponse);
		/*EXIT*/
	}

protected void updateWssp9000()
	{
	    //add by tom chi
		updateWsspTemp8000();
		//end
		/*PASS*/
		/*EXIT*/
	}

    @Override
    protected FixedLengthStringData getWsaaProg() {
        return wsaaProg;
    }

    @Override
    protected FixedLengthStringData getWsaaVersion() {
        return wsaaVersion;
    }

}
