/*
 * File: Cr561cpyf.java
 * Date: 30 August 2009 2:59:23
 * Author: $Id$
 * 
 * Class transformed from CR561CPYF.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.dataaccess.dao.AgpypfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cr561cpyf extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData cpyfprms = new FixedLengthStringData(11);

	public Cr561cpyf() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		cpyfprms = convertAndSetParam(cpyfprms, parmArray, 0);
		
		String parmSacscode = subString(cpyfprms, 1, 2).toString();
		Character parmCompany = subString(cpyfprms, 3, 1).toString().charAt(0);
		Integer parmEffdate = subString(cpyfprms, 4, 8).toInt();
		
		AgpypfDAO agpypfDAO = DAOFactory.getAgpypfDAO();
		agpypfDAO.deleteAll();
		agpypfDAO.copyFromAcmvpf(parmCompany, parmSacscode, parmEffdate);
	}
}
