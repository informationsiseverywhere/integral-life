package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: T1673rec Date: 03 February 2020 Author: vdivisala
 */
public class T1673rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData t1673Rec = new FixedLengthStringData(3);
	public FixedLengthStringData cmgwmop = DD.cmgwmop.copy().isAPartOf(t1673Rec, 0);
	public FixedLengthStringData cmgwspltyp = DD.cmgwspltyp.copy().isAPartOf(t1673Rec, 1);

	public void initialize() {
		COBOLFunctions.initialize(t1673Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			t1673Rec.isAPartOf(baseString, Boolean.TRUE);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}