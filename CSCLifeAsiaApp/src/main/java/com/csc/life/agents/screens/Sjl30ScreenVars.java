package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl30ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData agncysel = DD.agncysel.copy().isAPartOf(dataFields, 1);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
			.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agncyselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,
			getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agncyselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl30screenWritten = new LongData(0);
	public LongData Sjl30protectWritten = new LongData(0);

	public static int[] screenSflPfInds = new int[] { 8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21 };

	public boolean hasSubfile() {
		return false;
	}

	public Sjl30ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agncyselOut,
				new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(actionOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sjl30screen.class;
		protectRecord = Sjl30protect.class;
	}

	public static int[] getScreenSflPfInds() {
		return screenSflPfInds;
	}

	public int getDataAreaSize() {
		return 41;
	}

	public int getDataFieldsSize() {
		return 9;
	}

	public int getErrorIndicatorSize() {
		return 8;
	}

	public int getOutputFieldSize() {
		return 24;
	}

	public BaseData[] getscreenFields() {
		return new BaseData[] { agncysel, action };
	}

	public BaseData[][] getscreenOutFields() {
		return new BaseData[][] { agncyselOut, actionOut };
	}

	public BaseData[] getscreenErrFields() {
		return new BaseData[] { agncyselErr, actionErr };

	}

	public BaseData[] getscreenDateFields() {
		return new BaseData[] {};
	}

	public BaseData[] getscreenDateDispFields() {
		return new BaseData[] {};
	}

	public BaseData[] getscreenDateErrFields() {
		return new BaseData[] {};

	}
}
