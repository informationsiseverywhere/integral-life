package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:58
 * Description:
 * Copybook name: AGCMBRKKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmbrkkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmbrkFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmbrkKey = new FixedLengthStringData(64).isAPartOf(agcmbrkFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmbrkChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmbrkKey, 0);
  	public FixedLengthStringData agcmbrkChdrnum = new FixedLengthStringData(8).isAPartOf(agcmbrkKey, 1);
  	public FixedLengthStringData agcmbrkAgntnum = new FixedLengthStringData(8).isAPartOf(agcmbrkKey, 9);
  	public FixedLengthStringData agcmbrkLife = new FixedLengthStringData(2).isAPartOf(agcmbrkKey, 17);
  	public FixedLengthStringData agcmbrkCoverage = new FixedLengthStringData(2).isAPartOf(agcmbrkKey, 19);
  	public FixedLengthStringData agcmbrkRider = new FixedLengthStringData(2).isAPartOf(agcmbrkKey, 21);
  	public PackedDecimalData agcmbrkPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmbrkKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmbrkKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmbrkFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmbrkFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}