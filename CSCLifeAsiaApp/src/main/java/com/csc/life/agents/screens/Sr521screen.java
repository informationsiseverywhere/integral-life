package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr521screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr521ScreenVars sv = (Sr521ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr521screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr521ScreenVars screenVars = (Sr521ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.zcomcode01.setClassString("");
		screenVars.zrspperc01.setClassString("");
		screenVars.zrtuperc01.setClassString("");
		screenVars.zcomcode02.setClassString("");
		screenVars.zcomcode03.setClassString("");
		screenVars.zrspperc03.setClassString("");
		screenVars.zrtuperc03.setClassString("");
		screenVars.zrspperc02.setClassString("");
		screenVars.zrtuperc02.setClassString("");
		screenVars.contitem.setClassString("");
		screenVars.zcomcode04.setClassString("");
		screenVars.zrspperc04.setClassString("");
		screenVars.zrtuperc04.setClassString("");
		screenVars.zcomcode05.setClassString("");
		screenVars.zrspperc05.setClassString("");
		screenVars.zrtuperc05.setClassString("");
		screenVars.zcomcode06.setClassString("");
		screenVars.zrspperc06.setClassString("");
		screenVars.zrtuperc06.setClassString("");
		screenVars.zcomcode07.setClassString("");
		screenVars.zrspperc07.setClassString("");
		screenVars.zrtuperc07.setClassString("");
		screenVars.zcomcode08.setClassString("");
		screenVars.zrspperc08.setClassString("");
		screenVars.zrtuperc08.setClassString("");
		screenVars.zcomcode09.setClassString("");
		screenVars.zrspperc09.setClassString("");
		screenVars.zrtuperc09.setClassString("");
		screenVars.zcomcode10.setClassString("");
		screenVars.zrspperc10.setClassString("");
		screenVars.zrtuperc10.setClassString("");
	}

/**
 * Clear all the variables in Sr521screen
 */
	public static void clear(VarModel pv) {
		Sr521ScreenVars screenVars = (Sr521ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.zcomcode01.clear();
		screenVars.zrspperc01.clear();
		screenVars.zrtuperc01.clear();
		screenVars.zcomcode02.clear();
		screenVars.zcomcode03.clear();
		screenVars.zrspperc03.clear();
		screenVars.zrtuperc03.clear();
		screenVars.zrspperc02.clear();
		screenVars.zrtuperc02.clear();
		screenVars.contitem.clear();
		screenVars.zcomcode04.clear();
		screenVars.zrspperc04.clear();
		screenVars.zrtuperc04.clear();
		screenVars.zcomcode05.clear();
		screenVars.zrspperc05.clear();
		screenVars.zrtuperc05.clear();
		screenVars.zcomcode06.clear();
		screenVars.zrspperc06.clear();
		screenVars.zrtuperc06.clear();
		screenVars.zcomcode07.clear();
		screenVars.zrspperc07.clear();
		screenVars.zrtuperc07.clear();
		screenVars.zcomcode08.clear();
		screenVars.zrspperc08.clear();
		screenVars.zrtuperc08.clear();
		screenVars.zcomcode09.clear();
		screenVars.zrspperc09.clear();
		screenVars.zrtuperc09.clear();
		screenVars.zcomcode10.clear();
		screenVars.zrspperc10.clear();
		screenVars.zrtuperc10.clear();
	}
}
