package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.model.Zagppf;
import com.csc.life.agents.dataaccess.dao.ZagppfDAO;
import com.csc.life.agents.dataaccess.dao.impl.ZagppfDAOImpl;
import com.csc.life.agents.dataaccess.dao.AgentDAOFactory;
import com.csc.life.agents.dataaccess.dao.ZqbdpfDAO;
import com.csc.life.agents.dataaccess.dao.ZqbhpfDAO;
import com.csc.life.agents.tablestructures.Tr58orec;
import com.csc.life.agents.tablestructures.Tr58rrec;
import com.csc.life.agents.tablestructures.Tr58trec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;

import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;

import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Zqbdpf;
import com.csc.smart400framework.dataaccess.model.Zqbhpf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.ConversionUtil;
import com.quipoz.COBOLFramework.util.StringUtil;

import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Br59d extends Mainb {
	
	private String ivrm = "IVRM";
	private FixedLengthStringData     wsaaProg = new FixedLengthStringData(5).init("BR59D");
	private PackedDecimalData    wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData     wsaaCycleCnt = new PackedDecimalData(8, 0);
	

	private FixedLengthStringData   lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData  lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData  lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData  lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData  lsaaBuparec = new FixedLengthStringData(1024);
	private FixedLengthStringData   wsaaStatuz = new FixedLengthStringData(4);
	
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private T5645rec t5645rec = new T5645rec();
	private Tr58rrec tr58rrec = new Tr58rrec(); 
	private Tr58orec tr58orec = new Tr58orec();
	private Tr58trec tr58trec = new Tr58trec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	
	private Itempf itempf;
	private Zqbdpf zqbdpf;
	private Zqbhpf zqbhpf;
	
	private DescTableDAM descpf = new DescTableDAM();
	private ItempfDAO itempfDAO = DAOFactory.getItempfDAO();
	private Zagppf zagppf = new Zagppf();
	private ZagppfDAO zagppfDAO = getApplicationContext().getBean("zagppfDAO", ZagppfDAO.class);
	private CovrenqTableDAM covrenqDao = new CovrenqTableDAM(); 
	private CovtlnbTableDAM covtlnbDao = new CovtlnbTableDAM();
	private ZqbdpfDAO zqbdpfDao = AgentDAOFactory.getZqbdpfDAO();
	private ZqbhpfDAO zqbhpfDao = AgentDAOFactory.getZqbhpfDAO();
	private AglfTableDAM aglfDao = new AglfTableDAM();
	
	private Map<String,String>      wsaaaTr58o = new LinkedHashMap<String,String>();
	private Map<String,String>      wsaaaTr58r = new LinkedHashMap<String,String>();
	private List<Itempf> itemAl;
	private List<Zqbdpf> zqbdpfList;
	private List<Zqbhpf> zqbhpfList;
	
	private String tableName;
	private String itemitem;
	private String longdesc;
	private String lastagntnum;
	private String covrRiderType;
	private String aglfrec = "AGLFREC";
	private String covtlnbrec = "COVTLNBREC";
	private char lastagntcoy;
	private String ok = Varcom.oK.trim();
	private String mrnf = Varcom.mrnf.trim();
	private String endp = Varcom.endp.trim();
	private String statuz;
	private String params;
	
	private ZonedDecimalData plnsfx = new ZonedDecimalData(2); 

	private double interim;
	private double aqb;
	private double totAmnt;
	private double bonusprcent;
	private double persprcnt;
	private Iterator<Zqbdpf> zqbdIt;
	private int leaeseq;
	private int laaqseq;
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	
	
	
protected void restart0900()
	{
		//restart0910();
	}
	
protected FixedLengthStringData getWsaaProg() {
return wsaaProg;
}

protected PackedDecimalData getWsaaCommitCnt() {
return wsaaCommitCnt;
}

protected FixedLengthStringData getWsspEdterror() {
return wsspEdterror;
}

protected PackedDecimalData getWsaaCycleCnt() {
return wsaaCycleCnt;
}

protected FixedLengthStringData getLsaaStatuz() {
return lsaaStatuz;
}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
this.lsaaStatuz = lsaaStatuz;
}

protected FixedLengthStringData getLsaaBsscrec() {
return lsaaBsscrec;
}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
this.lsaaBsscrec = lsaaBsscrec;
}

protected FixedLengthStringData getLsaaBsprrec() {
return lsaaBsprrec;
}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
this.lsaaBsprrec = lsaaBsprrec;
}

protected FixedLengthStringData getLsaaBprdrec() {
return lsaaBprdrec;
}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
this.lsaaBprdrec = lsaaBprdrec;
}

protected FixedLengthStringData getLsaaBuparec() {
return lsaaBuparec;
}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
this.lsaaBuparec = lsaaBuparec;
}

public void mainline(Object... parmArray)
{
	lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
	lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
	lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
	lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
	lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
	try {
		process();
	}
	catch (COBOLExitProgramException e) {
	}
}

private void process()
{
	super.mainline();
}

@Override
protected void initialise1000() {
	
	tableName = "T5645";
	itemitem = wsaaProg.toString().trim();
	readItempf();
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	tableName = "T1688";
	readT1688Descpf();
	prepLifAcmv1100();
	tableName = "TR58O";
	readItmdm();
	readTr58rItempf();
	readZqbdpf();
	tableName="TR58T";
	itemitem= bsprIO.getCompany().toString().trim();
	readItempf();
	tr58trec.tr58tRec.set(StringUtil.rawToString(itempf.getGenarea()));
}


protected void readItempf()
{		
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(tableName);
	itempf.setItemitem(itemitem);
	itempf.setValidflag("1");
	itemAl = itempfDAO.findByExample(itempf);
	if(!itemAl.isEmpty())
	{
		itempf = itemAl.get(0);
	}
	else
	{
		syserrrec.params.set(itempf.getItemitem());
		fatalError600();
	}		
}

protected void readItmdm()
{		
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(tableName);
	itempf.setItmto(bsscIO.getEffectiveDate().getbigdata());
	itempf.setValidflag("1");
	itemAl = itempfDAO.findByCriteria(itempf);
	if(!itemAl.isEmpty())
	{
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			wsaaaTr58o.put(itempf.getItemitem().trim(),StringUtil.rawToString(itempf.getGenarea()));
		}		
	}
	else
	{
		syserrrec.params.set(itempf.getItemitem());
		fatalError600();
	}
	
}


protected void readTr58rItempf()
{		
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl("TR58R");
	itempf.setValidflag("1");
	itemAl = itempfDAO.findByExample(itempf);
	if(!itemAl.isEmpty())
	{
		Iterator<Itempf> it = itemAl.iterator();
		while(it.hasNext())
		{
			itempf = it.next();
			wsaaaTr58r.put(itempf.getItemitem().trim(),StringUtil.rawToString(itempf.getGenarea()));
		}		
	}
	else
	{
		syserrrec.statuz.set(mrnf);
		syserrrec.params.set(itempf.getItemitem());
		fatalError600();
	}
}


public void readT1688Descpf()
	{
		descpf.setDataArea(SPACE);
		descpf.setDesccoy(bsprIO.getCompany());
		descpf.setDescitem(bprdIO.getAuthCode());
		descpf.setDesctabl(tableName);
		descpf.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars,descpf);
		if(isEQ(descpf.getStatuz(),Varcom.oK) 
				&& isEQ(descpf.getDescitem(),bprdIO.getAuthCode())
				&& isEQ(descpf.getDesctabl(),tableName)
				&& isEQ(descpf.getDesccoy(),bsprIO.getCompany()))
		{
			longdesc = ConversionUtil.toString(descpf.getLongdesc());
	}
		else
		{
			longdesc="";
		}
}

public void prepLifAcmv1100()
{
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batccoy.set(batcdorrec.company);
	lifacmvrec.batcbrn.set(batcdorrec.branch);
	lifacmvrec.batcactmn.set(batcdorrec.actmonth);
	lifacmvrec.batcactyr.set(batcdorrec.actyear);
	lifacmvrec.batctrcde.set(batcdorrec.trcde);
	lifacmvrec.batcbatch.set(batcdorrec.batch);
	lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
	lifacmvrec.tranno.set(ZERO);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.trandesc.set(longdesc);
	lifacmvrec.postyear.set(SPACE);
	lifacmvrec.postmonth.set(SPACE);
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.termid.set(SPACE);
	lifacmvrec.suprflag.set(SPACE);
	lifacmvrec.genlcoy.set(bsprIO.getCompany());
	lifacmvrec.genlcur.set(SPACE);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.threadNumber.set(ZERO);
	lifacmvrec.substituteCode[6].set(SPACE);
}

protected void readZqbdpf()
{
	zqbdpf = new Zqbdpf();
	zqbdpf.setRectype('A');
	zqbdpf.setAgntcoy(ConversionUtil.toCharacter(bsprIO.getCompany()));
	zqbdpf.setBatcactyr(bsscIO.getAcctYear().toShort());
	zqbdpf.setBatcactmn(bsscIO.getAcctMonth().toByte());
	zqbdpf.setPrcdate(0);
	zqbdpfList = zqbdpfDao.findByCriteria(zqbdpf);
	zqbdIt = zqbdpfList.iterator();
}
@Override
protected void readFile2000() {
	// TODO Auto-generated method stub
	if(!zqbdIt.hasNext())
	{
		wsspEdterror.set(Varcom.endp);
		return;
	}
	wsspEdterror.set(Varcom.oK);
}


@Override
protected void edit2500() {
	contotrec.totval.set(1);
	contotrec.totno.set(ct01);
	callContot001();	
}

@Override
protected void update3000() {
	zqbdpf = zqbdIt.next();
	if(!zqbdpf.getAgntnum().equals(lastagntnum)
			|| zqbdpf.getAgntcoy() != lastagntcoy)
	{
		readZqbhpf();
		agentPersistency();
		agentQuaterlyBonusRate();
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		lastagntcoy = zqbdpf.getAgntcoy();
		lastagntnum = zqbdpf.getAgntnum();
		
	}
	updateAgentQuaterlyBonus();
	if(aqb != 0)
	{
		accounting();
	}
}

protected void readZqbhpf()
{
	zqbhpf = new Zqbhpf();
	zqbhpf.setRectype('A');
	zqbhpf.setAgntcoy(zqbdpf.getAgntcoy());
	zqbhpf.setAgntnum(zqbdpf.getAgntnum());
	zqbhpf.setBatcactyr(zqbdpf.getBatcactyr());
	zqbhpf.setBatcactmn(zqbdpf.getBatcactmn());
	zqbhpfList = zqbhpfDao.findByExample(zqbhpf);
	if(!zqbhpfList.isEmpty())
	{
		zqbhpf = zqbhpfList.get(0);
	}
	else
	{
		statuz = mrnf;
		params = zqbhpf.getAgntnum() +" "+ zqbhpf.getAgntcoy();
		fatalError();
	}
	
}

protected void agentPersistency()
{
	readZagppf();
	if(mrnf.equals(statuz))
	{
		readAglf();
		datcon3rec.datcon3Rec.set(SPACES);
		 datcon3rec.datcon3Rec.set(SPACE);
		 datcon3rec.intDate1.set(aglfDao.getDteapp());
		 datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		 datcon3rec.frequency.set("12");
		 callProgram(Datcon3.class, datcon3rec.datcon3Rec);	
		 if(isLT(datcon3rec.freqFactor,tr58trec.months))
		 {
			 zagppf.setPrcnt01(100.00);
		 }
		 else
		 {
			 zagppf.setPrcnt01(0.00);
		 }		 
	}
}

protected void readZagppf()
{
	zagppf.setAgntcoy(zqbdpf.getAgntcoy());
	zagppf.setAgntnum(zqbdpf.getAgntnum());
	zagppf.setYear(zqbdpf.getBatcactyr());
	zagppf.setMth(zqbdpf.getBatcactmn());
	
	zagppf = zagppfDAO.getZagppf(zagppf);
}

protected void readAglf()
{
	aglfDao.setDataArea(SPACES);
	aglfDao.setAgntcoy(zqbdpf.getAgntcoy());
	aglfDao.setAgntnum(zqbdpf.getAgntnum());
	aglfDao.setFunction(Varcom.readr);
	aglfDao.setFormat(aglfrec);
	SmartFileCode.execute(appVars, aglfDao);
	statuz = aglfDao.getStatuz().trim();
	if(!ok.equals(statuz) && !mrnf.equals(statuz))
	{
		params = aglfDao.getParams().toString();
		fatalError();
	}
}
protected void agentQuaterlyBonusRate()
{
	tr58orec.tr58oRec.set(SPACES);
 	if(wsaaaTr58o.containsKey("A"+zqbdpf.getAgtype()))
	{
		tr58orec.tr58oRec.set(wsaaaTr58o.get("A"+zqbdpf.getAgtype()));
	}
	else if(wsaaaTr58o.containsKey("A**"))
	{
		tr58orec.tr58oRec.set(wsaaaTr58o.get("A**"));
	}		
	prcntDeterm();
}

protected void prcntDeterm()
{
	totAmnt = zqbhpf.getTotamnt().doubleValue();
	bonusprcent = 0;
	int size = tr58orec.totamnt.length;
	for(int i =1 ; i < size;i++)
	{
		if(isLTE(totAmnt , tr58orec.totamt[i]))
		{
			if( i == 1 || isGT(zqbhpf.getTotamnt().doubleValue() , tr58orec.totamt[i-1]))
			{
				bonusprcent = tr58orec.prct[i].getData();
				break;
			}		
		}
	}	
	if(wsaaaTr58r.containsKey(zqbdpf.getAgtype().trim()))
	{
		tr58rrec.tr58rRec.set(wsaaaTr58r.get(zqbdpf.getAgtype()));
		if(zagppf.getPrcnt01() >= tr58rrec.minpcnt.getData())
		{
			persprcnt = tr58orec.oppc01.toDouble();			
		}
		else
		{
			persprcnt = tr58orec.oppc02.toDouble();
		}	
	}
	else
	{
		persprcnt = 0;
	}
}

protected void updateAgentQuaterlyBonus()
{
	interim = zqbdpf.getAcctamt().doubleValue()*(bonusprcent/100);
	aqb = interim*(persprcnt/100);
	zqbdpf.setPrcnt(BigDecimal.valueOf(bonusprcent));
	zqbdpf.setTotbon(BigDecimal.valueOf(aqb));
	zqbdpf.setUsrprf(bsscIO.getUserProfile().trim());
	zqbdpf.setJobnm(bsscIO.getJobName().trim());
	zqbdpf.setPrcdate(bsscIO.getEffectiveDate().toInt());
	zqbdpfDao.update(zqbdpf);
	contotrec.totval.set(1);
	contotrec.totno.set(ct03);
	callContot001();	
	contotrec.totval.set(aqb);
	contotrec.totno.set(ct04);
	callContot001();
	
}


public void accounting()
{   
	
	if(!zqbdpf.getChdrcoy().equals(ConversionUtil.toCharacter(covrenqDao.getChdrcoy()))
			||!zqbdpf.getChdrnum().trim().equals(covrenqDao.getChdrnum().trim())
			||!zqbdpf.getLife().trim().equals(covrenqDao.getLife().trim())
			|| !zqbdpf.getCoverage().trim().equals(covrenqDao.getCoverage().trim())
			|| !zqbdpf.getRider().trim().equals(covrenqDao.getRider().trim())
			|| zqbdpf.getPlnsfx() != covrenqDao.getPlanSuffix().getbigdata())
	{
		readCoverage();
	}
	plnsfx.set(zqbdpf.getPlnsfx());
	lifacmvleae();
	lifacmvlaaq();
}

protected void readCoverage()
{
	covrenqDao.setDataArea(SPACES);
	covrenqDao.setChdrcoy(zqbdpf.getChdrcoy());
	covrenqDao.setChdrnum(zqbdpf.getChdrnum());
	covrenqDao.setLife(zqbdpf.getLife());
	covrenqDao.setCoverage(zqbdpf.getCoverage());
	covrenqDao.setRider(zqbdpf.getRider());
	covrenqDao.setPlanSuffix(zqbdpf.getPlnsfx());
	covrenqDao.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars,covrenqDao);
	statuz = covrenqDao.getStatuz().trim();
	if(ok.equals(statuz))
	{
		covrRiderType = covrenqDao.getCrtable().trim();
	}
	else if(mrnf.equals(statuz))
	{
		readCovt();
	}
	else
	{
		params=covrenqDao.getParams().toString();
		fatalError();
	}
		
}

protected void readCovt()
{
	covtlnbDao.setDataArea(SPACES);

	covtlnbDao.setChdrcoy(zqbdpf.getChdrcoy());
	covtlnbDao.setChdrnum(zqbdpf.getChdrnum());
	covtlnbDao.setLife(zqbdpf.getLife());
	covtlnbDao.setRider(zqbdpf.getRider());
	covtlnbDao.setCoverage(zqbdpf.getCoverage());	
	covtlnbDao.setSeqnbr(0);
	covtlnbDao.setFunction(Varcom.begn);
	covtlnbDao.setFormat(covtlnbrec);
	SmartFileCode.execute(appVars, covtlnbDao);
	statuz =  covtlnbDao.getStatuz().trim();
	if(!ok.equals(statuz)
			&& !endp.trim().equals(statuz))
	{
		params = covtlnbDao.getParams().toString();
		fatalError();
	}
	if(!endp.equals(statuz)
			&& covtlnbDao.getChdrcoy().toString().equals(covtlnbDao.getChdrcoy().trim())
			&& covtlnbDao.getChdrnum().toString().equals(covtlnbDao.getChdrnum().trim())
			&& zqbdpf.getLife().equals(covtlnbDao.getLife().trim())
			&& zqbdpf.getCoverage().equals(covtlnbDao.getCoverage())
			&& zqbdpf.getRider().equals(covtlnbDao.getRider()))			
			{
				covrRiderType = covtlnbDao.getCrtable().trim();
			}
	else
	{
		covrRiderType = "";
	}
}

protected void lifacmvleae()
{
	lifacmvrec.rdocnum.set(zqbdpf.getChdrnum());
	lifacmvrec.jrnseq.set(++leaeseq);
	lifacmvrec.sacscode.set(t5645rec.sacscode01);
	lifacmvrec.sacstyp.set(t5645rec.sacstype01);
	lifacmvrec.glcode.set(t5645rec.glmap01);
	lifacmvrec.glsign.set(t5645rec.sign01);
	lifacmvrec.contot.set(t5645rec.cnttot01);
	lifacmvrec.origcurr.set(zqbdpf.getOrigcurr());
	lifacmvrec.origamt.set(zqbdpf.getTotbon());
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.rldgcoy.set(zqbdpf.getChdrcoy());
	lifacmvrec.rldgacct.set(zqbdpf.getChdrnum()+zqbdpf.getLife()+zqbdpf.getCoverage()+zqbdpf.getRider()+plnsfx);
	lifacmvrec.tranref.set(zqbdpf.getAgntnum());
	lifacmvrec.substituteCode[06].set(covrRiderType);
	callLifacmv();
	
}
protected void lifacmvlaaq()
{
	lifacmvrec.jrnseq.set(++laaqseq);
	lifacmvrec.sacscode.set(t5645rec.sacscode02);
	lifacmvrec.sacstyp.set(t5645rec.sacstype02);
	lifacmvrec.glcode.set(t5645rec.glmap02);
	lifacmvrec.glsign.set(t5645rec.sign02);
	lifacmvrec.contot.set(t5645rec.cnttot02);
	lifacmvrec.origamt.set(zqbdpf.getTotbon());
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.rldgcoy.set(zqbdpf.getAgntcoy());
	lifacmvrec.rldgacct.set(zqbdpf.getAgntnum());
	lifacmvrec.tranref.set(zqbdpf.getChdrnum()+zqbdpf.getLife()+zqbdpf.getCoverage()+zqbdpf.getRider()+plnsfx);
	lifacmvrec.substituteCode[06].set(SPACES);
	callLifacmv();
}

protected void callLifacmv() {
	// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
	lifacmvrec.ind.set("D");
	lifacmvrec.prefix.set("CH");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, Varcom.oK)) {
		params = lifacmvrec.lifacmvRec.getData();
		statuz = lifacmvrec.statuz.trim();
		fatalError600();
	}
}
protected void readCovr()
{
	
}
@Override
protected void commit3500() {
	// TODO Auto-generated method stub
	
}

@Override
protected void close4000() {
	// TODO Auto-generated method stub
	
}

@Override
protected void rollback3600() {
	// TODO Auto-generated method stub
	
}

protected void fatalError()
{
	syserrrec.statuz.set(statuz);
	syserrrec.params.set(params);
	fatalError600();
}

}
