/*
 * File: Br667.java
 * Date: 29 August 2009 22:34:22
 * Author: Quipoz Limited
 * 
 * Class transformed from BR667.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwprmrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.agents.tablestructures.Tr664rec;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Premium Payment Details Extraction for Bonus Workbench
*  *********************************************************
*   This batch job will extract the premium records from the premium
*   history  files (ZPTNPF) and then create the interface file as well as
*   populate those records into the file.
*
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of records written
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br667 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlzptnpf1rs = null;
	private java.sql.PreparedStatement sqlzptnpf1ps = null;
	private java.sql.Connection sqlzptnpf1conn = null;
	private String sqlzptnpf1 = "";
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR667");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPlan = new FixedLengthStringData(30);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLength = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private ZonedDecimalData wsaaInptAmount = new ZonedDecimalData(17, 4);
	private ZonedDecimalData wsaaIntmAmount = new ZonedDecimalData(17, 4).setUnsigned();

	private FixedLengthStringData wsaaIntmAmountX = new FixedLengthStringData(17).isAPartOf(wsaaIntmAmount, 0, REDEFINE);
	private FixedLengthStringData wsaaIntmInteger = new FixedLengthStringData(13).isAPartOf(wsaaIntmAmountX, 0);
	private FixedLengthStringData wsaaIntmDecimal = new FixedLengthStringData(4).isAPartOf(wsaaIntmAmountX, 13);

	private FixedLengthStringData wsaaNegativeSign = new FixedLengthStringData(1);
	private Validator negativeSign = new Validator(wsaaNegativeSign, "Y");
	private FixedLengthStringData wsaaConvAmount = new FixedLengthStringData(19);
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);

	private FixedLengthStringData wsaaTr664Item = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaTr664Trcde = new FixedLengthStringData(4).isAPartOf(wsaaTr664Item, 0);
	private FixedLengthStringData wsaaTr664Zprflg = new FixedLengthStringData(1).isAPartOf(wsaaTr664Item, 4);
	private Validator singlePremium = new Validator(wsaaTr664Zprflg, "S");
	private Validator regularPremium = new Validator(wsaaTr664Zprflg, "R");
	private Validator initialPremium = new Validator(wsaaTr664Zprflg, "I");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String covtlnbrec = "COVTLNBREC";
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String tr664 = "TR664";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler5, 5);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Desckey wsaaDesckey = new Desckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zbnwprmrec zbnwprmrec = new Zbnwprmrec();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();
	private Tr664rec tr664rec = new Tr664rec();
	private SqlZptnpfInner sqlZptnpfInner = new SqlZptnpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090, 
		a430Loop, 
		a440Set, 
		a490Exit
	}

	public Br667() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		defineCursor1020();
	}

protected void initialise1010()
	{
		/* We override the Database file ZBNWPF for record written*/
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bprdIO.getCompany());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaZbnwFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/* Define the upper & lower boundary of the effective date*/
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		zbnwpf.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void defineCursor1020()
	{
		/*  Define the query required by declaring a cursor*/
		sqlzptnpf1 = " SELECT  CHDRNUM, LIFE, COVERAGE, RIDER, EFFDATE, BILLCD, INSTFROM, TRANNO, ORIGAMT, TRCDE, ZPRFLG" +
" FROM   " + getAppVars().getTableNameOverriden("ZPTNPF") + " " +
" WHERE CHDRCOY = ?" +
" AND TRANDATE = ?" +
" ORDER BY CHDRNUM, TRANNO, EFFDATE, LIFE, COVERAGE, RIDER";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlzptnpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.agents.dataaccess.ZptnpfTableDAM());
			sqlzptnpf1ps = getAppVars().prepareStatementEmbeded(sqlzptnpf1conn, sqlzptnpf1, "ZPTNPF");
			getAppVars().setDBString(sqlzptnpf1ps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlzptnpf1ps, 2, wsaaEffectiveDate);
			sqlzptnpf1rs = getAppVars().executeQuery(sqlzptnpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzptnpf1rs)) {
				getAppVars().getDBObject(sqlzptnpf1rs, 1, sqlZptnpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlzptnpf1rs, 2, sqlZptnpfInner.sqlLife);
				getAppVars().getDBObject(sqlzptnpf1rs, 3, sqlZptnpfInner.sqlCoverage);
				getAppVars().getDBObject(sqlzptnpf1rs, 4, sqlZptnpfInner.sqlRider);
				getAppVars().getDBObject(sqlzptnpf1rs, 5, sqlZptnpfInner.sqlEffdate);
				getAppVars().getDBObject(sqlzptnpf1rs, 6, sqlZptnpfInner.sqlBillcd);
				getAppVars().getDBObject(sqlzptnpf1rs, 7, sqlZptnpfInner.sqlInstfrom);
				getAppVars().getDBObject(sqlzptnpf1rs, 8, sqlZptnpfInner.sqlTranno);
				getAppVars().getDBObject(sqlzptnpf1rs, 9, sqlZptnpfInner.sqlOrigamt);
				getAppVars().getDBObject(sqlzptnpf1rs, 10, sqlZptnpfInner.sqlTrcde);
				getAppVars().getDBObject(sqlzptnpf1rs, 11, sqlZptnpfInner.sqlZprflg);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Increment the control total for number of records read*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/* Check record is required for processing.*/
		/* Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/* Retrieve the policy header & coverages details*/
		readChdr3100();
		if (isEQ(chdrenqIO.getValidflag(),"1")) {
			readCovr3200();
		}
		else {
			readCovt3300();
		}
		if (isEQ(chdrenqIO.getValidflag(),"1")) {
			wsaaCrtable.set(covrmjaIO.getCrtable());
		}
		else {
			wsaaCrtable.set(covtlnbIO.getCrtable());
		}
		wsaaDesckey.set(SPACES);
		wsaaDesckey.descDesccoy.set(bprdIO.getCompany());
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescpfx.set(smtpfxcpy.item);
		wsaaDesckey.descDescitem.set(chdrenqIO.getCnttype());
		wsaaDesckey.descLanguage.set(bsscIO.getLanguage());
		a300GetDescription();
		wsaaPlan.set(descIO.getLongdesc());
		/* Write the record*/
		write3400();
	}

protected void readChdr3100()
	{
		/*READ*/
		chdrenqIO.setDataArea(SPACES);
		chdrenqIO.setChdrcoy(bprdIO.getCompany());
		chdrenqIO.setChdrnum(sqlZptnpfInner.sqlChdrnum);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readCovr3200()
	{
		read3210();
	}

protected void read3210()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(bprdIO.getCompany());
		covrmjaIO.setChdrnum(sqlZptnpfInner.sqlChdrnum);
		covrmjaIO.setLife(sqlZptnpfInner.sqlLife);
		covrmjaIO.setCoverage(sqlZptnpfInner.sqlCoverage);
		covrmjaIO.setRider(sqlZptnpfInner.sqlRider);
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(covrmjaIO.getStatuz());
			syserrrec.params.set(covrmjaIO.getParams());
			fatalError600();
		}
	}

protected void readCovt3300()
	{
		read3310();
	}

protected void read3310()
	{
		covtlnbIO.setDataArea(SPACES);
		covtlnbIO.setChdrcoy(bprdIO.getCompany());
		covtlnbIO.setChdrnum(sqlZptnpfInner.sqlChdrnum);
		covtlnbIO.setLife(sqlZptnpfInner.sqlLife);
		covtlnbIO.setCoverage(sqlZptnpfInner.sqlCoverage);
		covtlnbIO.setRider(sqlZptnpfInner.sqlRider);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");


		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
		|| isNE(covtlnbIO.getChdrcoy(),bprdIO.getCompany())
		|| isNE(covtlnbIO.getChdrnum(), sqlZptnpfInner.sqlChdrnum)
		|| isNE(covtlnbIO.getLife(), sqlZptnpfInner.sqlLife)
		|| isNE(covtlnbIO.getCoverage(), sqlZptnpfInner.sqlCoverage)
		|| isNE(covtlnbIO.getRider(), sqlZptnpfInner.sqlRider)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}

protected void write3400()
	{
		write3410();
	}

protected void write3410()
	{
		initialize(zbnwprmrec.bnwprmRec);
		zbnwprmrec.bnwprmTrcde.set("CR");
		zbnwprmrec.bnwprmSource.set("LIFE/ASIA");
		zbnwprmrec.bnwprmCategory.set(SPACES);
		wsaaInptAmount.set(sqlZptnpfInner.sqlOrigamt);
		a400LeftShift();
		zbnwprmrec.bnwprmAmount.set(wsaaConvAmount);
		wsaaInptDate.set(sqlZptnpfInner.sqlEffdate);
		a100DateConversion();
		zbnwprmrec.bnwprmEffdate.set(wsaaConvDate);
		wsaaInptDate.set(sqlZptnpfInner.sqlBillcd);
		a100DateConversion();
		zbnwprmrec.bnwprmDuedate.set(wsaaConvDate);
		initialize(zbwcmpyrec.bwcmpyRec);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.function.set("POL");
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.input.set(sqlZptnpfInner.sqlChdrnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwprmrec.bnwprmCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwprmrec.bnwprmCompany.set(SPACES);
		}
		zbnwprmrec.bnwprmCrtable.set(wsaaCrtable);
		zbnwprmrec.bnwprmChdrnum.set(sqlZptnpfInner.sqlChdrnum);
		zbnwprmrec.bnwprmPlan.set(wsaaPlan);
		readTr6643700();
		zbnwprmrec.bnwprmPremtype.set(tr664rec.zpremtype);
		wsaaInptDate.set(bsscIO.getEffectiveDate());
		a100DateConversion();
		zbnwprmrec.bnwprmTrandate.set(wsaaConvDate);
		zbnwprmrec.bnwprmProcdate.set(wsaaConvDate);
		zbnwprmrec.bnwprmFiller.set("0");
		zbnwprmrec.bnwprmInd.set(SPACES);
		zbnwprmrec.bnwprmMemo.set(SPACES);
		zbnwprmrec.bnwprmRollover.set(SPACES);
		zbnwprmrec.bnwprmPyind.set(SPACES);
		zbnwprmrec.bnwprmFund.set(SPACES);
		zbnwprmrec.bnwprmOvrprnt.set(SPACES);
		zbnwprmrec.bnwprmOvrind.set(SPACES);
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		/* Increment the control total for number of records written*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void readTr6643700()
	{
					read3710();
					call3720();
				}

protected void read3710()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(tr664);
		wsaaTr664Item.set(SPACES);
		wsaaTr664Trcde.set(sqlZptnpfInner.sqlTrcde);
		wsaaTr664Zprflg.set(sqlZptnpfInner.sqlZprflg);
		itemIO.setItemitem(wsaaTr664Item);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void call3720()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			tr664rec.tr664Rec.set(itemIO.getGenarea());
			return ;
		}
		if (isNE(wsaaTr664Zprflg,"*")) {
			wsaaTr664Zprflg.set("*");
			itemIO.setItemitem(wsaaTr664Item);
			call3720();
			return ;
		}
		else {
			tr664rec.tr664Rec.set(SPACES);
		}
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlzptnpf1conn, sqlzptnpf1ps, sqlzptnpf1rs);
		/*  Close any open files.*/
		zbnwpf.close();
		/*  Put the file to FTP library for download*/
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaStatuz);
			stringVariable1.addExpression(bprdIO.getRunLibrary());
			stringVariable1.addExpression(wsaaZbnwFn);
			stringVariable1.addExpression(wsaaFtpLibrary);
			stringVariable1.addExpression(wsaaFtpFile);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void a100DateConversion()
	{
		/*A110-INIT*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A190-EXIT*/
	}

protected void a200PackString()
	{
		/*A210-INIT*/
		wsaaString.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(zbnwprmrec.bnwprmTrcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmSource, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmCategory, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmAmount, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmEffdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmDuedate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmCrtable, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmCompany, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmChdrnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmPlan, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmPremtype, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmInd, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmMemo, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmRollover, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmPyind, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmFund, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmTrandate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmProcdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmFiller, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmOvrprnt, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwprmrec.bnwprmOvrind, " ");
		stringVariable1.setStringInto(wsaaString);
		/*A290-EXIT*/
	}

protected void a300GetDescription()
	{
		a310Init();
	}

protected void a310Init()
	{
		descIO.setDataArea(SPACES);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void a400LeftShift()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					a410Init();
					a420Loop();
				case a430Loop: 
					a430Loop();
				case a440Set: 
					a440Set();
				case a490Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410Init()
	{
		wsaaConvAmount.set(SPACES);
		wsaaNegativeSign.set("N");
		wsaaIx.set(0);
		compute(wsaaIy, 0).set(length(wsaaIntmDecimal) + 1);
		if (isEQ(wsaaInptAmount,ZERO)) {
			goTo(GotoLabel.a490Exit);
		}
		else {
			if (isGT(wsaaInptAmount,ZERO)) {
				wsaaIntmAmount.set(wsaaInptAmount);
			}
			else {
				compute(wsaaIntmAmount, 4).set(sub(0,wsaaInptAmount));
				negativeSign.setTrue();
			}
		}
	}

protected void a420Loop()
	{
		/* Adjust the integer part*/
		wsaaIx.add(1);
		if (isGT(wsaaIx,length(wsaaIntmInteger))) {
			goTo(GotoLabel.a490Exit);
		}
		if (isNE(subString(wsaaIntmInteger, wsaaIx, 1),"0")) {
			if (isNE(wsaaIntmDecimal,ZERO)) {
				goTo(GotoLabel.a430Loop);
			}
			else {
				wsaaIy.set(0);
				goTo(GotoLabel.a440Set);
			}
		}
		a420Loop();
		return ;
	}

protected void a430Loop()
	{
		/* Adjust the decimal part*/
		wsaaIy.subtract(1);
		if (isLT(wsaaIx,1)
		|| isNE(subString(wsaaIntmDecimal, wsaaIy, 1),"0")) {
			goTo(GotoLabel.a440Set);
		}
		a430Loop();
		return ;
	}

protected void a440Set()
	{
		compute(wsaaLength, 0).set(sub(length(wsaaIntmInteger) + 1,wsaaIx));
		if (negativeSign.isTrue()) {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("-");
				stringVariable1.addExpression(wsaaIntmInteger);
				stringVariable1.addExpression(".");
				stringVariable1.addExpression(wsaaIntmDecimal);
				stringVariable1.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression("-");
				stringVariable2.addExpression(wsaaIntmInteger);
				stringVariable2.setStringInto(wsaaConvAmount);
			}
		}
		else {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(wsaaIntmInteger);
				stringVariable3.addExpression(".");
				stringVariable3.addExpression(wsaaIntmDecimal);
				stringVariable3.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression(wsaaIntmInteger);
				stringVariable4.setStringInto(wsaaConvAmount);
			}
			}
		}
/*
 * Class transformed  from Data Structure SQL-ZPTNPF--INNER
 */
private static final class SqlZptnpfInner { 

		/* SQL-ZPTNPF */
	private FixedLengthStringData zptnrec = new FixedLengthStringData(46);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(zptnrec, 0);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(zptnrec, 8);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(zptnrec, 10);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(zptnrec, 12);
	private PackedDecimalData sqlEffdate = new PackedDecimalData(8, 0).isAPartOf(zptnrec, 14);
	private PackedDecimalData sqlBillcd = new PackedDecimalData(8, 0).isAPartOf(zptnrec, 19);
	private PackedDecimalData sqlInstfrom = new PackedDecimalData(8, 0).isAPartOf(zptnrec, 24);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(zptnrec, 29);
	private PackedDecimalData sqlOrigamt = new PackedDecimalData(17, 2).isAPartOf(zptnrec, 32);
	private FixedLengthStringData sqlTrcde = new FixedLengthStringData(4).isAPartOf(zptnrec, 41);
	private FixedLengthStringData sqlZprflg = new FixedLengthStringData(1).isAPartOf(zptnrec, 45);
	}
}
