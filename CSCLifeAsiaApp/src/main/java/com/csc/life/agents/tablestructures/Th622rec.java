package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:43
 * Description:
 * Copybook name: TH622REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th622rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th622Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData keyopts = new FixedLengthStringData(2).isAPartOf(th622Rec, 0);
  	public FixedLengthStringData[] keyopt = FLSArrayPartOfStructure(2, 1, keyopts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(keyopts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData keyopt01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData keyopt02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData zryrpercs = new FixedLengthStringData(110).isAPartOf(th622Rec, 2);
  	public ZonedDecimalData[] zryrperc = ZDArrayPartOfStructure(22, 5, 2, zryrpercs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(110).isAPartOf(zryrpercs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zryrperc01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData zryrperc02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData zryrperc03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData zryrperc04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData zryrperc05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData zryrperc06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData zryrperc07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData zryrperc08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData zryrperc09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData zryrperc10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData zryrperc11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public ZonedDecimalData zryrperc12 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 55);
  	public ZonedDecimalData zryrperc13 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 60);
  	public ZonedDecimalData zryrperc14 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 65);
  	public ZonedDecimalData zryrperc15 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 70);
  	public ZonedDecimalData zryrperc16 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 75);
  	public ZonedDecimalData zryrperc17 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 80);
  	public ZonedDecimalData zryrperc18 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 85);
  	public ZonedDecimalData zryrperc19 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 90);
  	public ZonedDecimalData zryrperc20 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 95);
  	public ZonedDecimalData zryrperc21 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 100);
  	public ZonedDecimalData zryrperc22 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 105);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(388).isAPartOf(th622Rec, 112, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th622Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th622Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}