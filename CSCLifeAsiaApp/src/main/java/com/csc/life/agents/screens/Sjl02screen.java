package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN Date: 11 November 2019 Author: vdivisala
 */
@SuppressWarnings("unchecked")
public class Sjl02screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 22, 3, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sjl02ScreenVars sv = (Sjl02ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl02screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sjl02ScreenVars screenVars = (Sjl02ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.qlfy01.setClassString("");
		screenVars.qlfydesc01.setClassString("");
		screenVars.qlfy02.setClassString("");
		screenVars.qlfydesc02.setClassString("");
		screenVars.qlfy03.setClassString("");
		screenVars.qlfydesc03.setClassString("");
		screenVars.qlfy04.setClassString("");
		screenVars.qlfydesc04.setClassString("");
		screenVars.qlfy05.setClassString("");
		screenVars.qlfydesc05.setClassString("");
		screenVars.qlfy06.setClassString("");
		screenVars.qlfydesc06.setClassString("");
		screenVars.qlfy07.setClassString("");
		screenVars.qlfydesc07.setClassString("");
		screenVars.qlfy08.setClassString("");
		screenVars.qlfydesc08.setClassString("");
		screenVars.qlfy09.setClassString("");
		screenVars.qlfydesc09.setClassString("");
		screenVars.qlfy10.setClassString("");
		screenVars.qlfydesc10.setClassString("");
	}

	/**
	 * Clear all the variables in Sjl02screen
	 */
	public static void clear(VarModel pv) {
		Sjl02ScreenVars screenVars = (Sjl02ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.qlfy01.clear();
		screenVars.qlfydesc01.clear();
		screenVars.qlfy02.clear();
		screenVars.qlfydesc02.clear();
		screenVars.qlfy03.clear();
		screenVars.qlfydesc03.clear();
		screenVars.qlfy04.clear();
		screenVars.qlfydesc04.clear();
		screenVars.qlfy05.clear();
		screenVars.qlfydesc05.clear();
		screenVars.qlfy06.clear();
		screenVars.qlfydesc06.clear();
		screenVars.qlfy07.clear();
		screenVars.qlfydesc07.clear();
		screenVars.qlfy08.clear();
		screenVars.qlfydesc08.clear();
		screenVars.qlfy09.clear();
		screenVars.qlfydesc09.clear();
		screenVars.qlfy10.clear();
		screenVars.qlfydesc10.clear();
	}
}