package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:06
 * Description:
 * Copybook name: T5695REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5695rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5695Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cmrates = new FixedLengthStringData(55).isAPartOf(t5695Rec, 0);
  	public ZonedDecimalData[] cmrate = ZDArrayPartOfStructure(11, 5, 2, cmrates, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(cmrates, 0, FILLER_REDEFINE);
  	public ZonedDecimalData cmrate01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData cmrate02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData cmrate03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData cmrate04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData cmrate05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData cmrate06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData cmrate07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData cmrate08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData cmrate09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData cmrate10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public ZonedDecimalData cmrate11 = new ZonedDecimalData(5, 2).isAPartOf(filler, 50);
  	public FixedLengthStringData commenpcs = new FixedLengthStringData(55).isAPartOf(t5695Rec, 55);
  	public ZonedDecimalData[] commenpc = ZDArrayPartOfStructure(11, 5, 2, commenpcs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(55).isAPartOf(commenpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData commenpc01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData commenpc02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData commenpc03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData commenpc04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData commenpc05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData commenpc06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData commenpc07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData commenpc08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData commenpc09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData commenpc10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public ZonedDecimalData commenpc11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
  	public FixedLengthStringData freqFroms = new FixedLengthStringData(22).isAPartOf(t5695Rec, 110);
  	public FixedLengthStringData[] freqFrom = FLSArrayPartOfStructure(11, 2, freqFroms, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(22).isAPartOf(freqFroms, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqFrom01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData freqFrom02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData freqFrom03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData freqFrom04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData freqFrom05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData freqFrom06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData freqFrom07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData freqFrom08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData freqFrom09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData freqFrom10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData freqFrom11 = new FixedLengthStringData(2).isAPartOf(filler2, 20);
  	public FixedLengthStringData freqTos = new FixedLengthStringData(22).isAPartOf(t5695Rec, 132);
  	public FixedLengthStringData[] freqTo = FLSArrayPartOfStructure(11, 2, freqTos, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(22).isAPartOf(freqTos, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freqTo01 = new FixedLengthStringData(2).isAPartOf(filler3, 0);
  	public FixedLengthStringData freqTo02 = new FixedLengthStringData(2).isAPartOf(filler3, 2);
  	public FixedLengthStringData freqTo03 = new FixedLengthStringData(2).isAPartOf(filler3, 4);
  	public FixedLengthStringData freqTo04 = new FixedLengthStringData(2).isAPartOf(filler3, 6);
  	public FixedLengthStringData freqTo05 = new FixedLengthStringData(2).isAPartOf(filler3, 8);
  	public FixedLengthStringData freqTo06 = new FixedLengthStringData(2).isAPartOf(filler3, 10);
  	public FixedLengthStringData freqTo07 = new FixedLengthStringData(2).isAPartOf(filler3, 12);
  	public FixedLengthStringData freqTo08 = new FixedLengthStringData(2).isAPartOf(filler3, 14);
  	public FixedLengthStringData freqTo09 = new FixedLengthStringData(2).isAPartOf(filler3, 16);
  	public FixedLengthStringData freqTo10 = new FixedLengthStringData(2).isAPartOf(filler3, 18);
  	public FixedLengthStringData freqTo11 = new FixedLengthStringData(2).isAPartOf(filler3, 20);
  	public FixedLengthStringData premaxpcs = new FixedLengthStringData(55).isAPartOf(t5695Rec, 154);
  	public ZonedDecimalData[] premaxpc = ZDArrayPartOfStructure(11, 5, 2, premaxpcs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(55).isAPartOf(premaxpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData premaxpc01 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData premaxpc02 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 5);
  	public ZonedDecimalData premaxpc03 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 10);
  	public ZonedDecimalData premaxpc04 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 15);
  	public ZonedDecimalData premaxpc05 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 20);
  	public ZonedDecimalData premaxpc06 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 25);
  	public ZonedDecimalData premaxpc07 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 30);
  	public ZonedDecimalData premaxpc08 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 35);
  	public ZonedDecimalData premaxpc09 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 40);
  	public ZonedDecimalData premaxpc10 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 45);
  	public ZonedDecimalData premaxpc11 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 50);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(291).isAPartOf(t5695Rec, 209, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5695Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5695Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}