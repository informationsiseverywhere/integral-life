package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:23
 * Description:
 * Copybook name: TR695REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr695rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr695Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData basicCommMeth = new FixedLengthStringData(4).isAPartOf(tr695Rec, 0);
  	public FixedLengthStringData bascpy = new FixedLengthStringData(4).isAPartOf(tr695Rec, 4);
  	public FixedLengthStringData basscmth = new FixedLengthStringData(4).isAPartOf(tr695Rec, 8);
  	public FixedLengthStringData basscpy = new FixedLengthStringData(4).isAPartOf(tr695Rec, 12);
  	public FixedLengthStringData bastcmth = new FixedLengthStringData(4).isAPartOf(tr695Rec, 16);
  	public FixedLengthStringData bastcpy = new FixedLengthStringData(4).isAPartOf(tr695Rec, 20);
  	public FixedLengthStringData rnwcpy = new FixedLengthStringData(4).isAPartOf(tr695Rec, 24);
  	public FixedLengthStringData srvcpy = new FixedLengthStringData(4).isAPartOf(tr695Rec, 28);
  	public FixedLengthStringData filler = new FixedLengthStringData(468).isAPartOf(tr695Rec, 32, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr695Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr695Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}