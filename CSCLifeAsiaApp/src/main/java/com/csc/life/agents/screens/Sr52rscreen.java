package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class Sr52rscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52rScreenVars sv = (Sr52rScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52rscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52rScreenVars screenVars = (Sr52rScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.agelimit.setClassString("");
		screenVars.toterm01.setClassString("");
		screenVars.factorsa01.setClassString("");
		screenVars.factsamn01.setClassString("");
		screenVars.factsamx01.setClassString("");
		screenVars.toterm02.setClassString("");
		screenVars.factorsa02.setClassString("");
		screenVars.factsamn02.setClassString("");
		screenVars.factsamx02.setClassString("");
		screenVars.toterm03.setClassString("");
		screenVars.factorsa03.setClassString("");
		screenVars.factsamn03.setClassString("");
		screenVars.factsamx03.setClassString("");
		screenVars.toterm04.setClassString("");
		screenVars.factorsa04.setClassString("");
		screenVars.factsamn04.setClassString("");
		screenVars.factsamx04.setClassString("");
		screenVars.toterm05.setClassString("");
		screenVars.factorsa05.setClassString("");
		screenVars.factsamn05.setClassString("");
		screenVars.factsamx05.setClassString("");
		screenVars.toterm06.setClassString("");
		screenVars.factorsa06.setClassString("");
		screenVars.factsamn06.setClassString("");
		screenVars.factsamx06.setClassString("");
		screenVars.toterm07.setClassString("");
		screenVars.factorsa07.setClassString("");
		screenVars.factsamn07.setClassString("");
		screenVars.factsamx07.setClassString("");
		screenVars.toterm08.setClassString("");
		screenVars.factorsa08.setClassString("");
		screenVars.factsamn08.setClassString("");
		screenVars.factsamx08.setClassString("");
		screenVars.toterm09.setClassString("");
		screenVars.factorsa09.setClassString("");
		screenVars.factsamn09.setClassString("");
		screenVars.factsamx09.setClassString("");
		screenVars.toterm10.setClassString("");
		screenVars.factorsa10.setClassString("");
		screenVars.factsamn10.setClassString("");
		screenVars.factsamx10.setClassString("");
		screenVars.dsifact.setClassString("");
	}

/**
 * Clear all the variables in Sr52rscreen
 */
	public static void clear(VarModel pv) {
		Sr52rScreenVars screenVars = (Sr52rScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.agelimit.clear();
		screenVars.toterm01.clear();
		screenVars.factorsa01.clear();
		screenVars.factsamn01.clear();
		screenVars.factsamx01.clear();
		screenVars.toterm02.clear();
		screenVars.factorsa02.clear();
		screenVars.factsamn02.clear();
		screenVars.factsamx02.clear();
		screenVars.toterm03.clear();
		screenVars.factorsa03.clear();
		screenVars.factsamn03.clear();
		screenVars.factsamx03.clear();
		screenVars.toterm04.clear();
		screenVars.factorsa04.clear();
		screenVars.factsamn04.clear();
		screenVars.factsamx04.clear();
		screenVars.toterm05.clear();
		screenVars.factorsa05.clear();
		screenVars.factsamn05.clear();
		screenVars.factsamx05.clear();
		screenVars.toterm06.clear();
		screenVars.factorsa06.clear();
		screenVars.factsamn06.clear();
		screenVars.factsamx06.clear();
		screenVars.toterm07.clear();
		screenVars.factorsa07.clear();
		screenVars.factsamn07.clear();
		screenVars.factsamx07.clear();
		screenVars.toterm08.clear();
		screenVars.factorsa08.clear();
		screenVars.factsamn08.clear();
		screenVars.factsamx08.clear();
		screenVars.toterm09.clear();
		screenVars.factorsa09.clear();
		screenVars.factsamn09.clear();
		screenVars.factsamx09.clear();
		screenVars.toterm10.clear();
		screenVars.factorsa10.clear();
		screenVars.factsamn10.clear();
		screenVars.factsamx10.clear();
		screenVars.dsifact.clear();
	}
}
