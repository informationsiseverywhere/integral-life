package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:27
 * Description:
 * Copybook name: T5644REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5644rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5644Rec = new FixedLengthStringData(507);
  	public FixedLengthStringData compysubr = new FixedLengthStringData(7).isAPartOf(t5644Rec, 0);
  	public FixedLengthStringData subrev = new FixedLengthStringData(7).isAPartOf(t5644Rec, 7);
  	public FixedLengthStringData rfdsbrtine = new FixedLengthStringData(7).isAPartOf(t5644Rec, 14);//IBPLIFE-5252
  	public FixedLengthStringData filler = new FixedLengthStringData(486).isAPartOf(t5644Rec, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5644Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5644Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}