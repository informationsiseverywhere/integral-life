package com.csc.life.agents.dataaccess.dao;
 

import com.csc.life.agents.dataaccess.model.Zrcapf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZrcapfDAO extends BaseDAO<Zrcapf> {
	public Zrcapf getZrcapfRecord(Character Agntcoy, String Agntnum);

   
}
