package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:17:12
 * Description:
 * Copybook name: T6688REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6688rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6688Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData agtchgsub = new FixedLengthStringData(8).isAPartOf(t6688Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(492).isAPartOf(t6688Rec, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6688Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6688Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}