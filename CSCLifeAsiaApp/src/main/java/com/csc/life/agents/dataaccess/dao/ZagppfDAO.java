package com.csc.life.agents.dataaccess.dao;

import java.math.BigDecimal;

import com.csc.life.agents.dataaccess.model.Zagppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZagppfDAO extends BaseDAO<Zagppf> {
	
	public Zagppf getZagppf(Zagppf zagppf);

}
