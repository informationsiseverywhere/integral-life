/*
 * File: BAG01.java
 * Date: 18 July 2013 21:03:47
 * Author: Quipoz Limited
 * Ticket: TMLII-AG-06
 * 
 * CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.common.DD;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.model.Zagppf;
import com.csc.life.agents.dataaccess.model.Zmbwpf;
import com.csc.life.agents.dataaccess.dao.ZagppfDAO;
import com.csc.life.agents.dataaccess.dao.ZmbwpfDAO;
import com.csc.life.agents.dataaccess.dao.impl.ZagppfDAOImpl;
import com.csc.life.agents.dataaccess.ZmbwTableDAM;
import com.csc.life.agents.dataaccess.dao.ZmpbpfDAO;
import com.csc.life.agents.dataaccess.dao.impl.ZmbwpfDAOImpl;
import com.csc.life.agents.dataaccess.model.Zmpbpf;
import com.csc.life.agents.tablestructures.Tr58rrec;
import com.csc.life.agents.tablestructures.Tr58srec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* REMARKS
*
* Batch program to read from ZMPBPF for current month record to calculate
* the Monthly Production Bonus. Then, post to Subsidiary Ledger Account 
* Movement (ACMVPF).
*
*****************************************************************
* </pre>
*/
public class B5808 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5808");
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPsprcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData wsaaPymtpc = new ZonedDecimalData(5, 2);
	private PackedDecimalData wsaampbamt = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaminpcnt = new ZonedDecimalData(5, 2);
	
	private FixedLengthStringData wsaaTr58rArray = new FixedLengthStringData(45630);
	private FixedLengthStringData[] wsaaTr58rRec = FLSArrayPartOfStructure(90, 507, wsaaTr58rArray, 0);
	private FixedLengthStringData[] wsaaTr58rKey = FLSDArrayPartOfArrayStructure(2, wsaaTr58rRec, 0);
	private FixedLengthStringData[] wsaaTr58rAgtype = FLSDArrayPartOfArrayStructure(2, wsaaTr58rKey, 0, SPACES);
	private FixedLengthStringData[] wsaaTr58rData = FLSDArrayPartOfArrayStructure(505, wsaaTr58rRec, 2);
	private PackedDecimalData[] wsaaTr58rItmfrm = PDArrayPartOfArrayStructure(8, 0 , wsaaTr58rData, 0);
	private FixedLengthStringData[] wsaaTr58rGenarea = FLSDArrayPartOfArrayStructure(500, wsaaTr58rData, 5);
	private int wsaaTr58rSize = 90;
	private FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	private PackedDecimalData wsaaTr58rix = new PackedDecimalData(3, 0);
	private Zmpbpf zmpbpf ;
	private Zmpbpf zmpbpfData;
	private ZmpbpfDAO zmpbpfDao = getApplicationContext().getBean("zmpbpfDao", ZmpbpfDAO.class);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private ZmbwpfDAO zmbwDaO = new ZmbwpfDAOImpl();
	private Zmbwpf zmbwpf = new Zmbwpf();
	private Zagppf zagppf = new Zagppf();
	private ZagppfDAO zagppfDAO = new ZagppfDAOImpl();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovtTableDAM covtIO = new CovtTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Tr58rrec tr58rrec = new Tr58rrec();
	private Tr58srec ta002rec = new Tr58srec();
	private T5645rec t5645rec = new T5645rec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	
	private String itdmrec = "ITEMREC";
	private String tr58r = "Tr58r";
	private String ta002 = "TA002";
	private String t5645 = "T5645";
	private String h791 = "H791";
	private String covrrec = "COVRREC";
	private String covtrec = "COVTREC";
	
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlzmpbpf1rs = null;
	private java.sql.PreparedStatement sqlzmpbpf1ps = null;
	private java.sql.Connection sqlzmpbpf1conn = null;
	private String sqlzmpbpf1 = "";
	
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler5, 5);

	public int pfRecLen = 184;
	public FixedLengthStringData zmpbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmpbpfRecord = zmpbrec;
	
	public PackedDecimalData sqlAcctamt = DD.acctamt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlAgntcoy = DD.agntcoy.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlAgntnum = DD.agntnum.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlBatcactmn = DD.batcactmn.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlBatcactyr = DD.batcactyr.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlBatctrcde = DD.batctrcde.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlBonusamt = DD.bonusamt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlChdrnum = DD.chdrnum.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlComtot = DD.comtot.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlCoverage = DD.coverage.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlDatime = DD.datime.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlEffdate = DD.effdate.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlJobName = DD.jobnm.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlJobnme = DD.jobnme.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlJobno = DD.jobno.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlLife = DD.life.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlOrigcurr = DD.origcurr.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlPrcdate = DD.prcdate.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlPrcent = DD.prcent.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlPrcnt = DD.prcnt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlRider = DD.rider.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData sqlTranno = DD.tranno.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData sqlUserProfile = DD.usrprf.copy().isAPartOf(zmpbrec); 
	
	protected boolean abort=true;
	
		
	
	
	public B5808() {
		super();
	}


public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readT1688Descpf();
		prepAcmv1100();
		defineCursor1060();
	}
protected void defineCursor1060()
{
	sqlzmpbpf1 = " SELECT  AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,ACCTAMT,CHDRNUM,LIFE,COVERAGE,RIDER,EFFDATE,TRANNO,BATCTRCDE" +
" FROM   " + appVars.getTableNameOverriden("ZMPBPF") + " " +
" WHERE BATCACTYR = ?" +
" AND BATCACTMN = ?" +
" ORDER BY AGNTCOY, AGNTNUM";
	sqlerrorflag = false;
	try {
		sqlzmpbpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.agents.dataaccess.ZmpbpfTableDAM());
		sqlzmpbpf1ps = appVars.prepareStatementEmbeded(sqlzmpbpf1conn, sqlzmpbpf1, "ZMPBPF");
		appVars.setDBInt(sqlzmpbpf1ps, 1, bsscIO.getAcctYear().toInt());
		appVars.setDBInt(sqlzmpbpf1ps, 2, bsscIO.getAcctMonth().toInt());
		sqlzmpbpf1rs = appVars.executeQuery(sqlzmpbpf1ps);
	}
	catch (SQLException ex){
		sqlca = ex;
		sqlerrorflag = true;
	}
	if (sqlerrorflag) {
		sqlError500();
	}
	/*EXIT*/
}
protected void sqlError500()
{
	/*CALL-SYSTEM-ERROR*/
	sqlErrorCode.set(sqlca.getErrorCode());
	syserrrec.syserrStatuz.set(sqlStatuz);
	fatalError600();
}
protected void initialise1010()
	{
	initialize(wsaaTr58rArray);
	itdmIO.setItemcoy(bsprIO.getCompany());
	itdmIO.setItemtabl(tr58r);
	itdmIO.setItemitem(SPACES);
	itdmIO.setItmfrm(varcom.vrcmMaxDate);
	itdmIO.setFunction(varcom.begn);
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
	itdmIO.setStatuz(varcom.oK);
	wsaaTr58rix.set(1);
	while ( !(isEQ(itdmIO.getStatuz(),varcom.endp))) {
		loadTr58r();
	}
	
	itdmIO.setParams(SPACES);
	itdmIO.setItemcoy(bsprIO.getCompany());
	itdmIO.setItemtabl(ta002);
	itdmIO.setItemitem(bsprIO.getCompany());
	itdmIO.setFunction(varcom.begn);
	itdmIO.setFormat(itdmrec);
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(), varcom.oK)
	&& isNE(itdmIO.getStatuz(), varcom.endp)) {
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	ta002rec.tr58sRec.set(itdmIO.getGenarea());
	
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(bsprIO.getCompany());
	itemIO.setItemtabl(t5645);
	itemIO.setItemitem(wsaaProg);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	t5645rec.t5645Rec.set(itemIO.getGenarea());
	

	}
protected void loadTr58r()
{
	SmartFileCode.execute(appVars, itdmIO);
	if (isNE(itdmIO.getStatuz(),varcom.oK)
	&& isNE(itdmIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	if (isEQ(itdmIO.getStatuz(),varcom.endp)
	|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
	|| isNE(itdmIO.getItemtabl(),tr58r)) {
		itdmIO.setStatuz(varcom.endp);
	}
	tr58rrec.tr58rRec.set(itdmIO.getGenarea());
	if (isGT(wsaaTr58rix,wsaaTr58rSize)) {
		syserrrec.statuz.set(h791);
		syserrrec.params.set(tr58r);
		fatalError600();
	}
	wsaaTr58rAgtype[wsaaTr58rix.toInt()].set(itdmIO.getItemitem());
	wsaaTr58rItmfrm[wsaaTr58rix.toInt()].set(itdmIO.getItmfrm());
	wsaaTr58rGenarea[wsaaTr58rix.toInt()].set(itdmIO.getGenarea());
	wsaaTr58rix.add(1);
	itdmIO.setFunction(varcom.nextr);
}

public void readT1688Descpf()
{
	descIO.setDataArea(SPACE);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(bsprIO.getCompany());
	descIO.setDescitem(bprdIO.getAuthCode());
	descIO.setDesctabl("T1688");
	descIO.setLanguage(appVars.getLanguageCode());
	descIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars,descIO);
	if(isEQ(descIO.getStatuz(),Varcom.oK))
	{
		wsaaTrandesc.set(descIO.getLongdesc());
	}
	else
	{
		wsaaTrandesc.set("");
	}
}
	protected void prepAcmv1100(){
		initialize(lifacmvrec);
		lifacmvrec.function.set("PSTW");	
		lifacmvrec.batccoy.set(batcdorrec.company);
		lifacmvrec.batcbrn.set(batcdorrec.branch);
		lifacmvrec.batcactyr.set(batcdorrec.actyear);
		lifacmvrec.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec.batctrcde.set(batcdorrec.trcde);
		lifacmvrec.batcbatch.set(batcdorrec.batch);
		lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec.tranno.set(ZERO);
		lifacmvrec.jrnseq.set(ZERO);	
		lifacmvrec.rldgcoy.set(bsprIO.getCompany());
		lifacmvrec.trandesc.set(wsaaTrandesc);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.suprflag.set(SPACES);
		lifacmvrec.genlcoy.set(bsprIO.getCompany());
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.threadNumber.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
	
	}
	protected void readFile2000()
		{
					readFile2010();
					endOfFile2080();
			}

	protected void readFile2010()
	{
		sqlerrorflag = false;
		sqlerrorflag = false;
		try {
			if (sqlzmpbpf1rs.next()) {
				appVars.getDBObject(sqlzmpbpf1rs, 1, sqlAgntcoy);
				appVars.getDBObject(sqlzmpbpf1rs, 2, sqlAgntnum);
				appVars.getDBObject(sqlzmpbpf1rs, 3, sqlBatcactyr);
				appVars.getDBObject(sqlzmpbpf1rs, 4, sqlBatcactmn);
				appVars.getDBObject(sqlzmpbpf1rs, 5, sqlAcctamt);
				appVars.getDBObject(sqlzmpbpf1rs, 6, sqlChdrnum);
				appVars.getDBObject(sqlzmpbpf1rs, 7, sqlLife);
				appVars.getDBObject(sqlzmpbpf1rs, 8, sqlCoverage);
				appVars.getDBObject(sqlzmpbpf1rs, 9, sqlRider);
				appVars.getDBObject(sqlzmpbpf1rs, 10, sqlEffdate);
				appVars.getDBObject(sqlzmpbpf1rs, 11, sqlTranno);
				appVars.getDBObject(sqlzmpbpf1rs, 12, sqlBatctrcde);
			}
			else {
				endOfFile2080();
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		return;
	}

	

	protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}


	protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		
	}
	
	protected void update3000()
	{
		if(isNE(wsaaAgntnum,sqlAgntnum))
		{
			callAgntlag();
			callZmbw();
			callZagp();
			
			wsaaTr58rix.set(1);
			while(isLTE(wsaaTr58rix,wsaaTr58rSize)){
							
				if(isEQ(wsaaTr58rAgtype[wsaaTr58rix.toInt()],agntlagIO.getAgtype()) &&
						isGTE(bsscIO.getEffectiveDate(),wsaaTr58rItmfrm[wsaaTr58rix.toInt()])){
					tr58rrec.tr58rRec.set(wsaaTr58rGenarea[wsaaTr58rix.toInt()]);
					wsaaminpcnt.set(tr58rrec.minpcnt);
				abort=false;
				}
				
				wsaaTr58rix.add(1);
			}
			if(abort){
				syserrrec.params.set("Tr58r Item not found");
				syserrrec.statuz.set("Tr58r Item not found");
				fatalError600();
			}
			
			if(isGTE(wsaaPsprcnt,wsaaminpcnt)){
				wsaaPymtpc.set(ta002rec.znadjperc01);
			}else{
				wsaaPymtpc.set(ta002rec.znadjperc02);
			}
		wsaaAgntnum.set(sqlAgntnum);
		}
		
		wsaampbamt.setRounded(mult(sqlAcctamt,mult(div(zmbwpf.getPrcent(), 100), div(wsaaPymtpc, 100))));
		callZmpb();
		if(isNE(wsaampbamt,ZERO)){
			postAcmv3100();
		}
	}
	
	protected void postAcmv3100(){
	
		lifacmvrec.rdocnum.set(sqlChdrnum);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(zmbwpf.getGenlcur());
		lifacmvrec.origamt.set(wsaampbamt);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		
		callCovr();
		if(isNE(covrIO.getStatuz(),varcom.endp)){
			lifacmvrec.substituteCode[06].set(covrIO.getCrtable());
		}else{
			callCovt();
			if(isNE(covtIO.getStatuz(),varcom.endp)){
			lifacmvrec.substituteCode[06].set(covtIO.getCrtable());
			}
		}		
		lifacmvrec.rldgacct.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlChdrnum);
		stringVariable1.append(sqlLife);
		stringVariable1.append(sqlCoverage);
		stringVariable1.append(sqlRider);
		stringVariable1.append("00");
		lifacmvrec.rldgacct.setLeft(stringVariable1.toString());
		lifacmvrec.tranref.set(sqlAgntnum);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		lifacmvrec.ind.set("D");
		lifacmvrec.prefix.set("CH");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.sacscode.set(t5645rec.sacscode02);
		lifacmvrec.sacstyp.set(t5645rec.sacstype02);
		lifacmvrec.glcode.set(t5645rec.glmap02);
		lifacmvrec.glsign.set(t5645rec.sign02);
		lifacmvrec.contot.set(t5645rec.cnttot02);
		lifacmvrec.origcurr.set(zmbwpf.getGenlcur());
		lifacmvrec.origamt.set(wsaampbamt);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		//Modified to Fix TMLII-1181
		lifacmvrec.rldgacct.set(SPACE);
		lifacmvrec.rldgacct.setLeft(sqlAgntnum);
		lifacmvrec.tranref.set(SPACES);
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(sqlChdrnum);
		stringVariable2.append(sqlLife);
		stringVariable2.append(sqlCoverage);
		stringVariable2.append(sqlRider);
		stringVariable2.append("00");	
		lifacmvrec.tranref.set(stringVariable2.toString());
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		lifacmvrec.ind.set("D");
		lifacmvrec.prefix.set("CH");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		
	}
	
	protected void callCovr(){
		
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(bsprIO.getCompany());
		covrIO.setChdrnum(sqlChdrnum);
		covrIO.setLife(sqlLife);
		covrIO.setCoverage(sqlCoverage);
		covrIO.setRider(sqlRider);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		
	}
	
	protected void callCovt(){
		covtIO.setDataArea(SPACES);
		covtIO.setChdrcoy(bsprIO.getCompany());
		covtIO.setChdrnum(sqlChdrnum);
		covtIO.setLife(sqlLife);
		covtIO.setCoverage(sqlCoverage);
		covtIO.setRider(sqlRider);
		covtIO.setPlanSuffix(ZERO);
		covtIO.setFormat(covtrec);
		covtIO.setFunction(varcom.begn);
		covtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covtIO);
		if (isNE(covtIO.getStatuz(),varcom.oK)
		&& isNE(covtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtIO.getParams());
			fatalError600();
		}
	}


	protected void callAgntlag()
	{
		/*READR-AGENT-FILE*/
		agntlagIO.setAgntcoy(sqlAgntcoy);
		agntlagIO.setAgntnum(sqlAgntnum);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntlagIO.getStatuz());
			syserrrec.params.set(agntlagIO.getParams());
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
		/*EXIT*/
	}

	
	protected void callZmpb(){
		zmpbpf.setBatcactyr(sqlBatcactyr.getbigdata());
		zmpbpf.setBatcactmn(sqlBatcactmn.getbigdata());
		zmpbpf.setAgntcoy(sqlAgntcoy.toString());
		zmpbpf.setAgntnum(sqlAgntnum.toString());
		zmpbpf.setChdrnum(sqlChdrnum.toString());
		zmpbpf.setLife(sqlLife.toString());
		zmpbpf.setCoverage(sqlCoverage.toString());
		zmpbpf.setRider(sqlRider.toString());
		zmpbpf.setEffdate(sqlEffdate.getbigdata());
		zmpbpf.setTranno(sqlTranno.getbigdata());
		zmpbpf.setBatctrcde(sqlBatctrcde.toString());
		/*zmpbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, zmpbIO);
		if (isNE(zmpbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpbIO.getParams());
			syserrrec.statuz.set(zmpbIO.getStatuz());
			fatalError600();
		}*/
		zmpbpfData=zmpbpfDao.getZmpbpfRecord(zmpbpf);
		if(zmpbpfData!=null)
			  zmpbpfDao.updateZmpbRecord(zmpbpfData);
		else {
		zmpbpf.setComtot(zmbwpf.getComtot());
		zmpbpf.setPrcent(zmbwpf.getPrcent());
		zmpbpf.setPrcnt(wsaaPsprcnt.getbigdata());
		zmpbpf.setBonusamt(wsaampbamt.getbigdata());
		zmpbpfDao.insertZmpbData(zmpbpf);
		}
		/*zmpbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, zmpbIO);
		if (isNE(zmpbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(zmpbIO.getParams());
			syserrrec.statuz.set(zmpbIO.getStatuz());
			callProgram(Syserr.class, syserrrec.syserrRec);
		}*/
		
	}
	
	protected void callZmbw(){
		//zmbwIO.agntcoy.set(sqlAgntcoy);
		//zmbwIO.agntnum.set(sqlAgntnum);
		
		zmbwpf.setAgntcoy(sqlAgntcoy.toString());
		zmbwpf.setAgntnum(sqlAgntnum.toString());
		
		Zmbwpf zmbwpfData = zmbwDaO.getZmbwpf(zmbwpf.getAgntcoy(), zmbwpf.getAgntnum());
	}
	
	protected void callZagp()
	{
		/*READR-AGENT-FILE*/
		
		zagppf.setAgntcoy(sqlAgntcoy.charat(0));
		zagppf.setAgntnum(sqlAgntnum.toString());
		zagppf.setYear(sqlBatcactyr.toShort());
		zagppf.setMth(sqlBatcactmn.toByte());
		
		Zagppf zagppfData = zagppfDAO.getZagppf(zagppf);
		if(zagppfData == null){
			wsaaPsprcnt.set(ZERO);
		}else{
			wsaaPsprcnt.set(zagppf.getPrcnt01());
		}
	}

	
protected void commit3500()
{
	/*COMMIT*/
	/*EXIT*/
}

protected void rollback3600()
{
	/*ROLLBACK*/
	/*EXIT*/
}

protected void close4000()
{
	/*CLOSE-FILES*/
	
	/*EXIT*/
}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

}
