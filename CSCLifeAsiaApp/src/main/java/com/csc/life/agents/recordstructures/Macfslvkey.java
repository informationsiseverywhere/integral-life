package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:34
 * Description:
 * Copybook name: MACFSLVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfslvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfslvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfslvKey = new FixedLengthStringData(64).isAPartOf(macfslvFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfslvAgntcoy = new FixedLengthStringData(1).isAPartOf(macfslvKey, 0);
  	public FixedLengthStringData macfslvZrptgb = new FixedLengthStringData(8).isAPartOf(macfslvKey, 1);
  	public FixedLengthStringData macfslvAgntnum = new FixedLengthStringData(8).isAPartOf(macfslvKey, 9);
  	public PackedDecimalData macfslvEffdate = new PackedDecimalData(8, 0).isAPartOf(macfslvKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(macfslvKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfslvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfslvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}