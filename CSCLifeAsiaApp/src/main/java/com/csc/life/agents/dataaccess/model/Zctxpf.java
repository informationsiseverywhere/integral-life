package com.csc.life.agents.dataaccess.model;

import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.Serializable;
import java.math.BigDecimal;

public class Zctxpf implements Cloneable{
	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String cnttype;
	private String clntnum;
	private BigDecimal amount;
	private BigDecimal zsgtamt;
	private BigDecimal zoeramt;
	private BigDecimal zdedamt;
	private BigDecimal zundamt;
	private BigDecimal zspsamt;
	private int datefrm;
	private int dateto;
	private BigDecimal zsgtpct;
	private BigDecimal zoerpct;
	private BigDecimal zdedpct;
	private BigDecimal zundpct;
	private BigDecimal zspspct;
	private BigDecimal zslryspct;//ILIFE-7345
	private BigDecimal zslrysamt;//ILIFE-7345
	private BigDecimal totprcnt;
	private String procflag;
	private int effdate;
	private BigDecimal cocontamnt;
	private BigDecimal liscamnt;
	private String usrprf;	
	private String jobnm;	
	private Date datime;

	private BigDecimal riskPrem ;  //ILIFE-7916
	private BigDecimal tottax;  //ILIFE-7916
	private String	Crtable;  //ILIFE-7916
	private BigDecimal	instprem; //ILIFE-7916
	private String rollflag;
	private static final Logger LOGGER = LoggerFactory.getLogger(Zctxpf.class);
	
	public Zctxpf() {
		chdrcoy = "";
		chdrnum = "";
		cnttype = "";
		clntnum = "";
		amount = BigDecimal.ZERO;
		zsgtamt = BigDecimal.ZERO;
		zoeramt = BigDecimal.ZERO;
		zdedamt = BigDecimal.ZERO;
		zundamt = BigDecimal.ZERO;
		zspsamt = BigDecimal.ZERO;
		datefrm = 0;
		dateto = 0;
		zsgtpct = BigDecimal.ZERO;
		zoerpct = BigDecimal.ZERO;
		zdedpct = BigDecimal.ZERO;
		zundpct = BigDecimal.ZERO;
		zspspct = BigDecimal.ZERO;
		zslryspct = BigDecimal.ZERO;
		zslrysamt = BigDecimal.ZERO;
		totprcnt = BigDecimal.ZERO;
		effdate = 0;
		usrprf = "";
		jobnm = "";
	}
	
	public String getRollflag() {
		return rollflag;
	}

	public void setRollflag(String rollflag) {
		this.rollflag = rollflag;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public BigDecimal getZsgtamt() {
		return zsgtamt;
	}
	public void setZsgtamt(BigDecimal zsgtamt) {
		this.zsgtamt = zsgtamt;
	}
	public BigDecimal getZoeramt() {
		return zoeramt;
	}
	public void setZoeramt(BigDecimal zoeramt) {
		this.zoeramt = zoeramt;
	}
	public BigDecimal getZdedamt() {
		return zdedamt;
	}
	public void setZdedamt(BigDecimal zdedamt) {
		this.zdedamt = zdedamt;
	}
	public BigDecimal getZundamt() {
		return zundamt;
	}
	public void setZundamt(BigDecimal zundamt) {
		this.zundamt = zundamt;
	}
	public BigDecimal getZspsamt() {
		return zspsamt;
	}
	public void setZspsamt(BigDecimal zspsamt) {
		this.zspsamt = zspsamt;
	}
	
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public String getClntnum() {
		return clntnum;
	}
	public void setClntnum(String clntnum) {
		this.clntnum = clntnum;
	}
	public int getDatefrm() {
		return datefrm;
	}
	public void setDatefrm(int datefrm) {
		this.datefrm = datefrm;
	}
	public int getDateto() {
		return dateto;
	}
	public void setDateto(int dateto) {
		this.dateto = dateto;
	}
	public BigDecimal getZsgtpct() {
		return zsgtpct;
	}
	public void setZsgtpct(BigDecimal zsgtpct) {
		this.zsgtpct = zsgtpct;
	}
	public BigDecimal getZoerpct() {
		return zoerpct;
	}
	public void setZoerpct(BigDecimal zoerpct) {
		this.zoerpct = zoerpct;
	}
	public BigDecimal getZdedpct() {
		return zdedpct;
	}
	public void setZdedpct(BigDecimal zdedpct) {
		this.zdedpct = zdedpct;
	}
	public BigDecimal getZundpct() {
		return zundpct;
	}
	public void setZundpct(BigDecimal zundpct) {
		this.zundpct = zundpct;
	}
	public BigDecimal getZspspct() {
		return zspspct;
	}
	public void setZspspct(BigDecimal zspspct) {
		this.zspspct = zspspct;
	}
	public BigDecimal getTotprcnt() {
		return totprcnt;
	}
	public void setTotprcnt(BigDecimal totprcnt) {
		this.totprcnt = totprcnt;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getCocontamnt() {
		return cocontamnt;
	}
	public void setCocontamnt(BigDecimal cocontamnt) {
		this.cocontamnt = cocontamnt;
	}
	public BigDecimal getLiscamnt() {
		return liscamnt;
	}
	public void setLiscamnt(BigDecimal liscamnt) {
		this.liscamnt = liscamnt;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	//ILIFE-7345-start
	public BigDecimal getZslrysamt() {
		return zslrysamt;
	}
	public void setZslrysamt(BigDecimal zslrysamt) {
		this.zslrysamt = zslrysamt;
	}
	public BigDecimal getZslryspct() {
		return zslryspct;
	}
	public void setZslryspct(BigDecimal zslryspct) {
		this.zslryspct = zslryspct;
	}
	//ILIFE-7345-end
	
	public String getProcflag() {
		return procflag;
	}
	public void setProcflag(String procflag) {
		this.procflag = procflag;
	}
	//ILIFE-7916 Start	
	/**
	 * @return the riskPrem
	 */
	public BigDecimal getRiskPrem() {
		return riskPrem;
	}
	/**
	 * @param riskPrem the riskPrem to set
	 */
	public void setRiskPrem(BigDecimal riskPrem) {
		this.riskPrem = riskPrem;
	}
	/**
	 * @return the tottax
	 */
	public BigDecimal getTottax() {
		return tottax;
	}
	/**
	 * @param tottax the tottax to set
	 */
	public void setTottax(BigDecimal tottax) {
		this.tottax = tottax;
	}
	
	/**
	 * @return the crtable
	 */
	public String getCrtable() {
		return Crtable;
	}
	/**
	 * @param crtable the crtable to set
	 */
	public void setCrtable(String crtable) {
		Crtable = crtable;
	}
	/**
	 * @return the instprem
	 */
	public BigDecimal getInstprem() {
		return instprem;
	}
	/**
	 * @param instprem the instprem to set
	 */
	public void setInstprem(BigDecimal instprem) {
		this.instprem = instprem;
	}

	

	//ILIFE-7916 END
	public Zctxpf copyObject() {
		try {
			return (Zctxpf) super.clone();
		}
		catch(CloneNotSupportedException e) {
			LOGGER.error("Unable to copy object", e);
		}
		return new Zctxpf();
	}
}
