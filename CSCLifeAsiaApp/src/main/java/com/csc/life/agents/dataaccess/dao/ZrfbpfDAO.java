package com.csc.life.agents.dataaccess.dao;
 
import java.util.List;

import com.csc.life.agents.dataaccess.model.Zrfbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZrfbpfDAO extends BaseDAO<Zrfbpf> {
	public void insertZrfbData(Zrfbpf zrfbpf);

   
}
