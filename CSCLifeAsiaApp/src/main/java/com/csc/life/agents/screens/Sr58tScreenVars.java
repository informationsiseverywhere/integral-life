/*
 * File: Screen Variables for Sr58t
 * Date: 18 July 2013 2:36:10
 * Author: CSC
 * 
 * 
 * Copyright (2013) CSC Asia, all rights reserved.
 */

package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

public class Sr58tScreenVars extends SmartVarModel { 

//Ticket #TMLII-1918
	public FixedLengthStringData dataArea = new FixedLengthStringData(1718);
	public FixedLengthStringData dataFields = new FixedLengthStringData(342).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData chdrtypes = new FixedLengthStringData(90).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] chdrtype = FLSArrayPartOfStructure(30, 3, chdrtypes, 0);
	  public FixedLengthStringData chdrtype01 = DD.chdrtype.copy().isAPartOf(chdrtypes,0); 
	  public FixedLengthStringData chdrtype02 = DD.chdrtype.copy().isAPartOf(chdrtypes,3); 
	  public FixedLengthStringData chdrtype03 = DD.chdrtype.copy().isAPartOf(chdrtypes,6); 
	  public FixedLengthStringData chdrtype04 = DD.chdrtype.copy().isAPartOf(chdrtypes,9); 
	  public FixedLengthStringData chdrtype05 = DD.chdrtype.copy().isAPartOf(chdrtypes,12); 
	  public FixedLengthStringData chdrtype06 = DD.chdrtype.copy().isAPartOf(chdrtypes,15); 
	  public FixedLengthStringData chdrtype07 = DD.chdrtype.copy().isAPartOf(chdrtypes,18); 
	  public FixedLengthStringData chdrtype08 = DD.chdrtype.copy().isAPartOf(chdrtypes,21); 
	  public FixedLengthStringData chdrtype09 = DD.chdrtype.copy().isAPartOf(chdrtypes,24); 
	  public FixedLengthStringData chdrtype10 = DD.chdrtype.copy().isAPartOf(chdrtypes,27); 
	  public FixedLengthStringData chdrtype11 = DD.chdrtype.copy().isAPartOf(chdrtypes,30); 
	  public FixedLengthStringData chdrtype12 = DD.chdrtype.copy().isAPartOf(chdrtypes,33); 
	  public FixedLengthStringData chdrtype13 = DD.chdrtype.copy().isAPartOf(chdrtypes,36); 
	  public FixedLengthStringData chdrtype14 = DD.chdrtype.copy().isAPartOf(chdrtypes,39); 
	  public FixedLengthStringData chdrtype15 = DD.chdrtype.copy().isAPartOf(chdrtypes,42); 
	  public FixedLengthStringData chdrtype16 = DD.chdrtype.copy().isAPartOf(chdrtypes,45); 
	  public FixedLengthStringData chdrtype17 = DD.chdrtype.copy().isAPartOf(chdrtypes,48); 
	  public FixedLengthStringData chdrtype18 = DD.chdrtype.copy().isAPartOf(chdrtypes,51); 
	  public FixedLengthStringData chdrtype19 = DD.chdrtype.copy().isAPartOf(chdrtypes,54); 
	  public FixedLengthStringData chdrtype20 = DD.chdrtype.copy().isAPartOf(chdrtypes,57); 
	  public FixedLengthStringData chdrtype21 = DD.chdrtype.copy().isAPartOf(chdrtypes,60); 
	  public FixedLengthStringData chdrtype22 = DD.chdrtype.copy().isAPartOf(chdrtypes,63); 
	  public FixedLengthStringData chdrtype23 = DD.chdrtype.copy().isAPartOf(chdrtypes,66); 
	  public FixedLengthStringData chdrtype24 = DD.chdrtype.copy().isAPartOf(chdrtypes,69); 
	  public FixedLengthStringData chdrtype25 = DD.chdrtype.copy().isAPartOf(chdrtypes,72); 
	  public FixedLengthStringData chdrtype26 = DD.chdrtype.copy().isAPartOf(chdrtypes,75); 
	  public FixedLengthStringData chdrtype27 = DD.chdrtype.copy().isAPartOf(chdrtypes,78); 
	  public FixedLengthStringData chdrtype28 = DD.chdrtype.copy().isAPartOf(chdrtypes,81); 
	  public FixedLengthStringData chdrtype29 = DD.chdrtype.copy().isAPartOf(chdrtypes,84); 
	  public FixedLengthStringData chdrtype30 = DD.chdrtype.copy().isAPartOf(chdrtypes,87); 
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,90); 
	public ZonedDecimalData effperd = DD.effperd.copyToZonedDecimal().isAPartOf(dataFields,91); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,97); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,105); 
	public ZonedDecimalData months = DD.months.copyToZonedDecimal().isAPartOf(dataFields,135); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,137); 
	//Ticket #TMLII-1918
	public FixedLengthStringData crtables = new FixedLengthStringData(200).isAPartOf(dataFields, 142);
	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(50, 4, crtables, 0);
	  public FixedLengthStringData crtable01 = DD.crtable.copy().isAPartOf(crtables,0); 
	  public FixedLengthStringData crtable02 = DD.crtable.copy().isAPartOf(crtables,4); 
	  public FixedLengthStringData crtable03 = DD.crtable.copy().isAPartOf(crtables,8); 
	  public FixedLengthStringData crtable04 = DD.crtable.copy().isAPartOf(crtables,12); 
	  public FixedLengthStringData crtable05 = DD.crtable.copy().isAPartOf(crtables,16); 
	  public FixedLengthStringData crtable06 = DD.crtable.copy().isAPartOf(crtables,20); 
	  public FixedLengthStringData crtable07 = DD.crtable.copy().isAPartOf(crtables,24); 
	  public FixedLengthStringData crtable08 = DD.crtable.copy().isAPartOf(crtables,28); 
	  public FixedLengthStringData crtable09 = DD.crtable.copy().isAPartOf(crtables,32); 
	  public FixedLengthStringData crtable10 = DD.crtable.copy().isAPartOf(crtables,36); 
	  public FixedLengthStringData crtable11 = DD.crtable.copy().isAPartOf(crtables,40); 
	  public FixedLengthStringData crtable12 = DD.crtable.copy().isAPartOf(crtables,44); 
	  public FixedLengthStringData crtable13 = DD.crtable.copy().isAPartOf(crtables,48); 
	  public FixedLengthStringData crtable14 = DD.crtable.copy().isAPartOf(crtables,52); 
	  public FixedLengthStringData crtable15 = DD.crtable.copy().isAPartOf(crtables,56); 
	  public FixedLengthStringData crtable16 = DD.crtable.copy().isAPartOf(crtables,60); 
	  public FixedLengthStringData crtable17 = DD.crtable.copy().isAPartOf(crtables,64); 
	  public FixedLengthStringData crtable18 = DD.crtable.copy().isAPartOf(crtables,68); 
	  public FixedLengthStringData crtable19 = DD.crtable.copy().isAPartOf(crtables,72); 
	  public FixedLengthStringData crtable20 = DD.crtable.copy().isAPartOf(crtables,76); 
	  public FixedLengthStringData crtable21 = DD.crtable.copy().isAPartOf(crtables,80); 
	  public FixedLengthStringData crtable22 = DD.crtable.copy().isAPartOf(crtables,84); 
	  public FixedLengthStringData crtable23 = DD.crtable.copy().isAPartOf(crtables,88); 
	  public FixedLengthStringData crtable24 = DD.crtable.copy().isAPartOf(crtables,92); 
	  public FixedLengthStringData crtable25 = DD.crtable.copy().isAPartOf(crtables,96); 
	  public FixedLengthStringData crtable26 = DD.crtable.copy().isAPartOf(crtables,100); 
	  public FixedLengthStringData crtable27 = DD.crtable.copy().isAPartOf(crtables,104); 
	  public FixedLengthStringData crtable28 = DD.crtable.copy().isAPartOf(crtables,108); 
	  public FixedLengthStringData crtable29 = DD.crtable.copy().isAPartOf(crtables,112); 
	  public FixedLengthStringData crtable30 = DD.crtable.copy().isAPartOf(crtables,116); 
	  public FixedLengthStringData crtable31 = DD.crtable.copy().isAPartOf(crtables,120); 
	  public FixedLengthStringData crtable32 = DD.crtable.copy().isAPartOf(crtables,124); 
	  public FixedLengthStringData crtable33 = DD.crtable.copy().isAPartOf(crtables,128); 
	  public FixedLengthStringData crtable34 = DD.crtable.copy().isAPartOf(crtables,132); 
	  public FixedLengthStringData crtable35 = DD.crtable.copy().isAPartOf(crtables,136); 
	  public FixedLengthStringData crtable36 = DD.crtable.copy().isAPartOf(crtables,140); 
	  public FixedLengthStringData crtable37 = DD.crtable.copy().isAPartOf(crtables,144); 
	  public FixedLengthStringData crtable38 = DD.crtable.copy().isAPartOf(crtables,148); 
	  public FixedLengthStringData crtable39 = DD.crtable.copy().isAPartOf(crtables,152); 
	  public FixedLengthStringData crtable40 = DD.crtable.copy().isAPartOf(crtables,156); 
	  public FixedLengthStringData crtable41 = DD.crtable.copy().isAPartOf(crtables,160); 
	  public FixedLengthStringData crtable42 = DD.crtable.copy().isAPartOf(crtables,164); 
	  public FixedLengthStringData crtable43 = DD.crtable.copy().isAPartOf(crtables,168); 
	  public FixedLengthStringData crtable44 = DD.crtable.copy().isAPartOf(crtables,172); 
	  public FixedLengthStringData crtable45 = DD.crtable.copy().isAPartOf(crtables,176); 
	  public FixedLengthStringData crtable46 = DD.crtable.copy().isAPartOf(crtables,180); 
	  public FixedLengthStringData crtable47 = DD.crtable.copy().isAPartOf(crtables,184); 
	  public FixedLengthStringData crtable48 = DD.crtable.copy().isAPartOf(crtables,188); 
	  public FixedLengthStringData crtable49 = DD.crtable.copy().isAPartOf(crtables,192); 
	  public FixedLengthStringData crtable50 = DD.crtable.copy().isAPartOf(crtables,196); 
	//Ticket #TMLII-1918
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(344).isAPartOf(dataArea, 342);
//Ticket #TMLII-1918
	public FixedLengthStringData chdrtypesErr = new FixedLengthStringData(120).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] chdrtypeErr = FLSArrayPartOfStructure(30, 4, chdrtypesErr, 0);
	  public FixedLengthStringData chdrtype01Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,0); 
	  public FixedLengthStringData chdrtype02Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,4); 
	  public FixedLengthStringData chdrtype03Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,8); 
	  public FixedLengthStringData chdrtype04Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,12); 
	  public FixedLengthStringData chdrtype05Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,16); 
	  public FixedLengthStringData chdrtype06Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,20); 
	  public FixedLengthStringData chdrtype07Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,24); 
	  public FixedLengthStringData chdrtype08Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,28); 
	  public FixedLengthStringData chdrtype09Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,32); 
	  public FixedLengthStringData chdrtype10Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,36); 
	  public FixedLengthStringData chdrtype11Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,40); 
	  public FixedLengthStringData chdrtype12Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,44); 
	  public FixedLengthStringData chdrtype13Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,48); 
	  public FixedLengthStringData chdrtype14Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,52); 
	  public FixedLengthStringData chdrtype15Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,56); 
	  public FixedLengthStringData chdrtype16Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,60); 
	  public FixedLengthStringData chdrtype17Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,64); 
	  public FixedLengthStringData chdrtype18Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,68); 
	  public FixedLengthStringData chdrtype19Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,72); 
	  public FixedLengthStringData chdrtype20Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,76); 
	  public FixedLengthStringData chdrtype21Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,80); 
	  public FixedLengthStringData chdrtype22Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,84); 
	  public FixedLengthStringData chdrtype23Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,88); 
	  public FixedLengthStringData chdrtype24Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,92); 
	  public FixedLengthStringData chdrtype25Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,96); 
	  public FixedLengthStringData chdrtype26Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,100); 
	  public FixedLengthStringData chdrtype27Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,104); 
	  public FixedLengthStringData chdrtype28Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,108); 
	  public FixedLengthStringData chdrtype29Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,112); 
	  public FixedLengthStringData chdrtype30Err =new FixedLengthStringData(4).isAPartOf(chdrtypesErr,116); 
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData effperdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData monthsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	//Ticket #TMLII-1918
	public FixedLengthStringData crtablesErr = new FixedLengthStringData(200).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData[] crtableErr = FLSArrayPartOfStructure(50, 4, crtablesErr, 0);
	  public FixedLengthStringData crtable01Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,0); 
	  public FixedLengthStringData crtable02Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,4); 
	  public FixedLengthStringData crtable03Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,8); 
	  public FixedLengthStringData crtable04Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,12); 
	  public FixedLengthStringData crtable05Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,16); 
	  public FixedLengthStringData crtable06Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,20); 
	  public FixedLengthStringData crtable07Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,24); 
	  public FixedLengthStringData crtable08Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,28); 
	  public FixedLengthStringData crtable09Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,32); 
	  public FixedLengthStringData crtable10Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,36); 
	  public FixedLengthStringData crtable11Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,40); 
	  public FixedLengthStringData crtable12Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,44); 
	  public FixedLengthStringData crtable13Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,48); 
	  public FixedLengthStringData crtable14Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,52); 
	  public FixedLengthStringData crtable15Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,56); 
	  public FixedLengthStringData crtable16Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,60); 
	  public FixedLengthStringData crtable17Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,64); 
	  public FixedLengthStringData crtable18Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,68); 
	  public FixedLengthStringData crtable19Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,72); 
	  public FixedLengthStringData crtable20Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,76); 
	  public FixedLengthStringData crtable21Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,80); 
	  public FixedLengthStringData crtable22Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,84); 
	  public FixedLengthStringData crtable23Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,88); 
	  public FixedLengthStringData crtable24Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,92); 
	  public FixedLengthStringData crtable25Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,96); 
	  public FixedLengthStringData crtable26Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,100); 
	  public FixedLengthStringData crtable27Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,104); 
	  public FixedLengthStringData crtable28Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,108); 
	  public FixedLengthStringData crtable29Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,112); 
	  public FixedLengthStringData crtable30Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,116); 
	  public FixedLengthStringData crtable31Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,120); 
	  public FixedLengthStringData crtable32Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,124); 
	  public FixedLengthStringData crtable33Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,128); 
	  public FixedLengthStringData crtable34Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,132); 
	  public FixedLengthStringData crtable35Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,136); 
	  public FixedLengthStringData crtable36Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,140); 
	  public FixedLengthStringData crtable37Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,144); 
	  public FixedLengthStringData crtable38Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,148); 
	  public FixedLengthStringData crtable39Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,152); 
	  public FixedLengthStringData crtable40Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,156); 
	  public FixedLengthStringData crtable41Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,160); 
	  public FixedLengthStringData crtable42Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,164); 
	  public FixedLengthStringData crtable43Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,168); 
	  public FixedLengthStringData crtable44Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,172); 
	  public FixedLengthStringData crtable45Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,176); 
	  public FixedLengthStringData crtable46Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,180); 
	  public FixedLengthStringData crtable47Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,184); 
	  public FixedLengthStringData crtable48Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,188); 
	  public FixedLengthStringData crtable49Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,192); 
	  public FixedLengthStringData crtable50Err = new FixedLengthStringData(4).isAPartOf(crtablesErr,196); 
//Ticket #TMLII-1918
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1032).isAPartOf(dataArea,686);
//Ticket #TMLII-1918
	public FixedLengthStringData chdrtypesOut = new FixedLengthStringData(360).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(30, 12, chdrtypesOut, 0);
	public FixedLengthStringData[][] chdrtypeO = FLSDArrayPartOfArrayStructure(12, 1, chdrtypeOut, 0);
	  public FixedLengthStringData[] chdrtype01Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 0);
	  public FixedLengthStringData[] chdrtype02Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 12);
	  public FixedLengthStringData[] chdrtype03Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 24);
	  public FixedLengthStringData[] chdrtype04Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 36);
	  public FixedLengthStringData[] chdrtype05Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 48);
	  public FixedLengthStringData[] chdrtype06Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 60);
	  public FixedLengthStringData[] chdrtype07Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 72);
	  public FixedLengthStringData[] chdrtype08Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 84);
	  public FixedLengthStringData[] chdrtype09Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 96);
	  public FixedLengthStringData[] chdrtype10Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 108);
	  public FixedLengthStringData[] chdrtype11Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 120);
	  public FixedLengthStringData[] chdrtype12Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 132);
	  public FixedLengthStringData[] chdrtype13Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 144);
	  public FixedLengthStringData[] chdrtype14Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 156);
	  public FixedLengthStringData[] chdrtype15Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 168);
	  public FixedLengthStringData[] chdrtype16Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 180);
	  public FixedLengthStringData[] chdrtype17Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 192);
	  public FixedLengthStringData[] chdrtype18Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 204);
	  public FixedLengthStringData[] chdrtype19Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 216);
	  public FixedLengthStringData[] chdrtype20Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 228);
	  public FixedLengthStringData[] chdrtype21Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 240);
	  public FixedLengthStringData[] chdrtype22Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 252);
	  public FixedLengthStringData[] chdrtype23Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 264);
	  public FixedLengthStringData[] chdrtype24Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 276);
	  public FixedLengthStringData[] chdrtype25Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 288);
	  public FixedLengthStringData[] chdrtype26Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 300);
	  public FixedLengthStringData[] chdrtype27Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 312);
	  public FixedLengthStringData[] chdrtype28Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 324);
	  public FixedLengthStringData[] chdrtype29Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 336);
	  public FixedLengthStringData[] chdrtype30Out = FLSArrayPartOfStructure(12, 1, chdrtypesOut, 348);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] effperdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] monthsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	//Ticket #TMLII-1918
	public FixedLengthStringData crtablesOut = new FixedLengthStringData(600).isAPartOf(outputIndicators, 432);
	public FixedLengthStringData[][] crtableO = FLSDArrayPartOfArrayStructure(12, 1, chdrtypeOut, 0);
	  public FixedLengthStringData[] crtable01Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 0);
	  public FixedLengthStringData[] crtable02Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 12);
	  public FixedLengthStringData[] crtable03Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 24);
	  public FixedLengthStringData[] crtable04Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 36);
	  public FixedLengthStringData[] crtable05Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 48);
	  public FixedLengthStringData[] crtable06Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 60);
	  public FixedLengthStringData[] crtable07Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 72);
	  public FixedLengthStringData[] crtable08Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 84);
	  public FixedLengthStringData[] crtable09Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 96);
	  public FixedLengthStringData[] crtable10Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 108);
	  public FixedLengthStringData[] crtable11Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 120);
	  public FixedLengthStringData[] crtable12Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 132);
	  public FixedLengthStringData[] crtable13Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 144);
	  public FixedLengthStringData[] crtable14Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 156);
	  public FixedLengthStringData[] crtable15Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 168);
	  public FixedLengthStringData[] crtable16Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 180);
	  public FixedLengthStringData[] crtable17Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 192);
	  public FixedLengthStringData[] crtable18Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 204);
	  public FixedLengthStringData[] crtable19Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 216);
	  public FixedLengthStringData[] crtable20Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 228);
	  public FixedLengthStringData[] crtable21Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 240);
	  public FixedLengthStringData[] crtable22Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 252);
	  public FixedLengthStringData[] crtable23Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 264);
	  public FixedLengthStringData[] crtable24Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 276);
	  public FixedLengthStringData[] crtable25Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 288);
	  public FixedLengthStringData[] crtable26Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 300);
	  public FixedLengthStringData[] crtable27Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 312);
	  public FixedLengthStringData[] crtable28Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 324);
	  public FixedLengthStringData[] crtable29Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 336);
	  public FixedLengthStringData[] crtable30Out = FLSArrayPartOfStructure(12, 1, crtablesOut, 348);
      public FixedLengthStringData[] crtable31Out = FLSArrayPartOfStructure(12, 1, crtablesOut,360); 
	 public FixedLengthStringData[] crtable32Out = FLSArrayPartOfStructure(12, 1, crtablesOut,372); 
	 public FixedLengthStringData[] crtable33Out = FLSArrayPartOfStructure(12, 1, crtablesOut,384); 
	 public FixedLengthStringData[] crtable34Out = FLSArrayPartOfStructure(12, 1, crtablesOut,396); 
	 public FixedLengthStringData[] crtable35Out = FLSArrayPartOfStructure(12, 1, crtablesOut,408); 
	 public FixedLengthStringData[] crtable36Out = FLSArrayPartOfStructure(12, 1, crtablesOut,420); 
	 public FixedLengthStringData[] crtable37Out = FLSArrayPartOfStructure(12, 1, crtablesOut,432); 
	 public FixedLengthStringData[] crtable38Out = FLSArrayPartOfStructure(12, 1, crtablesOut,444); 
	 public FixedLengthStringData[] crtable39Out = FLSArrayPartOfStructure(12, 1, crtablesOut,456); 
	 public FixedLengthStringData[] crtable40Out = FLSArrayPartOfStructure(12, 1, crtablesOut,468); 
	 public FixedLengthStringData[] crtable41Out = FLSArrayPartOfStructure(12, 1, crtablesOut,480); 
	 public FixedLengthStringData[] crtable42Out = FLSArrayPartOfStructure(12, 1, crtablesOut,492); 
	 public FixedLengthStringData[] crtable43Out = FLSArrayPartOfStructure(12, 1, crtablesOut,504); 
	 public FixedLengthStringData[] crtable44Out = FLSArrayPartOfStructure(12, 1, crtablesOut,516); 
	 public FixedLengthStringData[] crtable45Out = FLSArrayPartOfStructure(12, 1, crtablesOut,528); 
	 public FixedLengthStringData[] crtable46Out = FLSArrayPartOfStructure(12, 1, crtablesOut,540); 
	 public FixedLengthStringData[] crtable47Out = FLSArrayPartOfStructure(12, 1, crtablesOut,552); 
	 public FixedLengthStringData[] crtable48Out = FLSArrayPartOfStructure(12, 1, crtablesOut,564); 
	 public FixedLengthStringData[] crtable49Out = FLSArrayPartOfStructure(12, 1, crtablesOut,576); 
	 public FixedLengthStringData[] crtable50Out = FLSArrayPartOfStructure(12, 1, crtablesOut,588); 
	//Ticket #TMLII-1918
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable sr58tscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr58tscreenctlWritten = new LongData(0);
	public LongData Sr58tscreensflWritten = new LongData(0);
	public LongData Sr58tscreenWritten = new LongData(0);
	public LongData Sr58twindowWritten = new LongData(0);
	public LongData Sr58thideWritten = new LongData(0);
	public LongData Sr58tprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr58tScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	//Ticket #TMLII-1918
	fieldIndMap.put(chdrtype01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype09Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype10Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype11Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype12Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype13Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype14Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype15Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype16Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype17Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype18Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype19Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype20Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype21Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype22Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype23Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype24Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype25Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype26Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype27Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype28Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype29Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(chdrtype30Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(effperdOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(monthsOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable09Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable10Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable11Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable12Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable13Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable14Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable15Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable16Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable17Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable18Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable19Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable20Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable21Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable22Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable23Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable24Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable25Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable26Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable27Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable28Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable29Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable30Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable31Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable32Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable33Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable34Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable35Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable36Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable37Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable38Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable39Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable40Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable41Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable42Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable43Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable44Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable45Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable46Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable47Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable48Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable49Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(crtable50Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	

	screenFields = new BaseData[] { chdrtype01,chdrtype02,chdrtype03,chdrtype04,chdrtype05,chdrtype06,chdrtype07,chdrtype08,chdrtype09,chdrtype10,chdrtype11,chdrtype12,chdrtype13,chdrtype14,chdrtype15,chdrtype16,chdrtype17,chdrtype18,chdrtype19,chdrtype20,chdrtype21,chdrtype22,chdrtype23,chdrtype24,chdrtype25,chdrtype26,chdrtype27,chdrtype28,chdrtype29,chdrtype30,company,effperd,item,longdesc,months,tabl,crtable01,crtable02,crtable03,crtable04,crtable05,crtable06,crtable07,crtable08,crtable09,crtable10,crtable11,crtable12,crtable13,crtable14,crtable15,crtable16,crtable17,crtable18,crtable19,crtable20,crtable21,crtable22,crtable23,crtable24,crtable25,crtable26,crtable27,crtable28,crtable29,crtable30,crtable31,crtable32,crtable33,crtable34,crtable35,crtable36,crtable37,crtable38,crtable39,crtable40,crtable41,crtable42,crtable43,crtable44,crtable45,crtable46,crtable47,crtable48,crtable49,crtable50 };
	screenOutFields = new BaseData[][] { chdrtype01Out,chdrtype02Out,chdrtype03Out,chdrtype04Out,chdrtype05Out,chdrtype06Out,chdrtype07Out,chdrtype08Out,chdrtype09Out,chdrtype10Out,chdrtype11Out,chdrtype12Out,chdrtype13Out,chdrtype14Out,chdrtype15Out,chdrtype16Out,chdrtype17Out,chdrtype18Out,chdrtype19Out,chdrtype20Out,chdrtype21Out,chdrtype22Out,chdrtype23Out,chdrtype24Out,chdrtype25Out,chdrtype26Out,chdrtype27Out,chdrtype28Out,chdrtype29Out,chdrtype30Out,companyOut,effperdOut,itemOut,longdescOut,monthsOut,tablOut,crtable01Out,crtable02Out,crtable03Out,crtable04Out,crtable05Out,crtable06Out,crtable07Out,crtable08Out,crtable09Out,crtable10Out,crtable11Out,crtable12Out,crtable13Out,crtable14Out,crtable15Out,crtable16Out,crtable17Out,crtable18Out,crtable19Out,crtable20Out,crtable21Out,crtable22Out,crtable23Out,crtable24Out,crtable25Out,crtable26Out,crtable27Out,crtable28Out,crtable29Out,crtable30Out,crtable31Out,crtable32Out,crtable33Out,crtable34Out,crtable35Out,crtable36Out,crtable37Out,crtable38Out,crtable39Out,crtable40Out,crtable41Out,crtable42Out,crtable43Out,crtable44Out,crtable45Out,crtable46Out,crtable47Out,crtable48Out,crtable49Out,crtable50Out };
	screenErrFields = new BaseData[] {  chdrtype01Err,chdrtype02Err,chdrtype03Err,chdrtype04Err,chdrtype05Err,chdrtype06Err,chdrtype07Err,chdrtype08Err,chdrtype09Err,chdrtype10Err,chdrtype11Err,chdrtype12Err,chdrtype13Err,chdrtype14Err,chdrtype15Err,chdrtype16Err,chdrtype17Err,chdrtype18Err,chdrtype19Err,chdrtype20Err,chdrtype21Err,chdrtype22Err,chdrtype23Err,chdrtype24Err,chdrtype25Err,chdrtype26Err,chdrtype27Err,chdrtype28Err,chdrtype29Err,chdrtype30Err,companyErr,effperdErr,itemErr,longdescErr,monthsErr,tablErr,crtable01Err,crtable02Err,crtable03Err,crtable04Err,crtable05Err,crtable06Err,crtable07Err,crtable08Err,crtable09Err,crtable10Err,crtable11Err,crtable12Err,crtable13Err,crtable14Err,crtable15Err,crtable16Err,crtable17Err,crtable18Err,crtable19Err,crtable20Err,crtable21Err,crtable22Err,crtable23Err,crtable24Err,crtable25Err,crtable26Err,crtable27Err,crtable28Err,crtable29Err,crtable30Err,crtable31Err,crtable32Err,crtable33Err,crtable34Err,crtable35Err,crtable36Err,crtable37Err,crtable38Err,crtable39Err,crtable40Err,crtable41Err,crtable42Err,crtable43Err,crtable44Err,crtable45Err,crtable46Err,crtable47Err,crtable48Err,crtable49Err,crtable50Err  };
	//Ticket #TMLII-1918
	screenDateFields = new BaseData[] {   };
	screenDateErrFields = new BaseData[] {   };
	screenDateDispFields = new BaseData[] {    };

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr58tscreen.class;
	protectRecord = Sr58tprotect.class;

	}

 

}
