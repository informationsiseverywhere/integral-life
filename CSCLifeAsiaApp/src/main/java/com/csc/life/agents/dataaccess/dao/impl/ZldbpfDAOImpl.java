package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.financials.dataaccess.dao.model.Preqpf;
import com.csc.life.agents.dataaccess.dao.ZldbpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.agents.dataaccess.model.Zldbpf;
import com.csc.life.productdefinition.dataaccess.model.Medipf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZldbpfDAOImpl extends BaseDAOImpl<Zldbpf>implements ZldbpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZldbpfDAOImpl.class);
	//private static final String getZldbQuery = "select AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,BLPREM,BPAYTY,NETPRE,PROCFLG from ZLDBPF where BATCACTYR=? and BATCACTMN=? and AGNTCOY=? and AGNTNUM=?";
	Zldbpf zldbpfRec = new Zldbpf();
	String getZldbQry="select AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,BLPREM,BPAYTY,AGTYPE,PRCNT,PRMAMT,PREMST,NETPRE,PROCFLG from ZLDBPF where BATCACTYR=? and BATCACTMN=? and AGNTCOY=? and AGNTNUM=?";
	 int[] execreturn;
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.fsu.general.dataaccess.dao.ChdrpfDAO#getDate(java.lang.String,
	 * java.lang.String)
	 */
	
	// add by yy for IFSU-757
    public boolean insertZldbPFRecord(List<Zldbpf> zldbpfList) {
    	boolean isInsertSuccessful = true;
        if (zldbpfList != null && zldbpfList.toString().length()> 0) {
           
            String SQL_ZLDB_INSERT = "INSERT INTO ZLDBPF(AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,AGTYPE,PRCNT,GENLCUR,BLPREM,PREMST,PRMAMT,BPAYTY,BONUSAMT,BNPAID,NETPRE,PROCFLG,PRCDATE,USRPRF,JOBNM,DATIME FROM ZLDBPF WHERE UNIQUE_NUMBER=?";
           
            PreparedStatement psZldbInsert = getPrepareStatement(SQL_ZLDB_INSERT);
            try {
            	  for (Zldbpf zldbpf : zldbpfList) {
                    psZldbInsert.setString(1, zldbpf.getAgntcoy());
                    psZldbInsert.setString(2, zldbpf.getAgntnum());
                    psZldbInsert.setString(3, zldbpf.getBatcactyr().toString());
                    psZldbInsert.setString(4, zldbpf.getBatcactmn().toString());
                    psZldbInsert.setString(5, zldbpf.getAgtype());//IJTI-1485
                    psZldbInsert.setInt(6, zldbpf.getPrcnt());
                    psZldbInsert.setString(7, zldbpf.getGenlcur());//IJTI-1485
                    psZldbInsert.setString(8, zldbpf.getGenlcur());//IJTI-1485
                    psZldbInsert.setString(9, zldbpf.getBlprem().toString());
                    psZldbInsert.setString(10, zldbpf.getPremst().toString());
                    psZldbInsert.setInt(11, zldbpf.getBpayty());
                    psZldbInsert.setString(12, zldbpf.getBonusamt().toString());
                    psZldbInsert.setString(13, zldbpf.getBnpaid().toString());
                    psZldbInsert.setString(14, zldbpf.getNetpre().toString());
                    psZldbInsert.setString(15, zldbpf.getProcflg());
                    psZldbInsert.setInt(16, zldbpf.getPrcdate());
                    // JOBNM,USRPRF,DATIME
                    
                    psZldbInsert.setString(17, getUsrprf());
                    psZldbInsert.setString(18, getJobnm());
                    psZldbInsert.setTimestamp(19, new Timestamp(System.currentTimeMillis()));
                    psZldbInsert.setLong(20, zldbpf.getUniqueNumber());
                    psZldbInsert.addBatch();
            	  }
               
                    execreturn =   psZldbInsert.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("insertZldbPFRecord()", e);//IJTI-1561
                isInsertSuccessful = false;
                throw new SQLRuntimeException(e);
            } finally {
                close(psZldbInsert, null);
            }
        }        
        
        return isInsertSuccessful;
        
    }
	 
	
    
	public boolean updateZldbPF(List<Zldbpf> zldbpfList){
		boolean isUpdateSuccessful = false;
		StringBuilder sb = new StringBuilder("");
		sb.append("UPDATE ZLDBPF SET PROCFLG=?");
		sb.append("WHERE  AND AGNTCOY=? AND AGNTNUM=? AND BATCACTYR=? AND BATCACTMN=? ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = getPrepareStatement(sb.toString());
			for (Zldbpf zldbpf : zldbpfList) {
			ps.setString(1, zldbpf.getAgntcoy());
            ps.setString(2, zldbpf.getAgntnum());
            ps.setString(3, zldbpf.getBatcactyr().toString());
            ps.setString(4, zldbpf.getBatcactmn().toString());
            ps.setString(5, zldbpf.getAgtype());//IJTI-1485
            ps.setInt(6, zldbpf.getPrcnt());
            ps.setString(7, zldbpf.getGenlcur());//IJTI-1485
            ps.setString(8, zldbpf.getGenlcur());//IJTI-1485
            ps.setString(9, zldbpf.getBlprem().toString());
            ps.setString(10, zldbpf.getPremst().toString());
            ps.setInt(11, zldbpf.getBpayty());
            ps.setString(12, zldbpf.getBonusamt().toString());
            ps.setString(13, zldbpf.getBnpaid().toString());
            ps.setString(14, zldbpf.getNetpre().toString());
            ps.setString(15, zldbpf.getProcflg());
            ps.setInt(16, zldbpf.getPrcdate());
            // JOBNM,USRPRF,DATIME
            
            ps.setString(17, getUsrprf());
            ps.setString(18, getJobnm());
            ps.setTimestamp(19, new Timestamp(System.currentTimeMillis()));
            ps.setLong(20, zldbpf.getUniqueNumber());
            ps.addBatch();
			}
            ps.executeBatch();				
			   
			    ps.addBatch();	
						
			ps.executeBatch();
			isUpdateSuccessful = true;
		}catch (SQLException e) {
			LOGGER.error("updateZldbPF()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}		
		//if()
		return isUpdateSuccessful;
		
	}   
	
	public List<Zldbpf> getZldbpfList(Integer batcactyr,Integer batcactmn,String agntcoy,String agntnum,int flag)
	{
		ResultSet rs = null;
		List<Zldbpf> zldbpfList = new ArrayList<Zldbpf>();
		
		
		try {
			
			PreparedStatement stmt = getPrepareStatement(getZldbQry);
			
			if(flag==1)
			{
				 getZldbQry = "select AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,BLPREM,BPAYTY,AGTYPE,PRCNT,PRMAMT,PREMST,NETPRE,PROCFLG from ZLDBPF where BATCACTYR=? and BATCACTMN=?";
				 stmt.setInt(1, batcactyr);
				 stmt.setInt(2, batcactmn);
				 
			}
			else
			{
				stmt.setInt(1, batcactyr);
				stmt.setInt(2, batcactmn);
				stmt.setString(3, agntcoy);
				stmt.setString(4, agntnum);
				
			}
			
			
			rs = stmt.executeQuery();

			while (rs.next()) {				
				
				while (rs.next()) {						
					zldbpfRec.setAgntcoy(rs.getString("AGNTCOY"));
					zldbpfRec.setAgntnum(rs.getString("AGNTNUM"));
					zldbpfRec.setBatcactyr(rs.getInt("BATCACTYR"));
					zldbpfRec.setBatcactyr(rs.getInt("BATCACTMN"));
					zldbpfRec.setBlprem(rs.getBigDecimal("BLPREM"));
					zldbpfRec.setBpayty(rs.getInt("BPAYTY"));
					zldbpfRec.setNetpre(rs.getBigDecimal("NETPRE"));
					zldbpfRec.setProcflg(rs.getString("PROCFLG"));
					zldbpfRec.setPrmamt(rs.getBigDecimal("PRMAMT"));
					zldbpfRec.setPremst(rs.getInt("PREMST"));
					zldbpfRec.setAgtype(rs.getString("AGTYPE"));
					zldbpfRec.setPrcnt(rs.getInt("PRCNT"));
				}
				zldbpfList.add(zldbpfRec);
				 

			}
			//IJTI-851-Overly Broad Catch
		} catch (SQLException e) {

			LOGGER.error("error has occured in ZldbpfDAOImpl.getZldbpfList", e);
		}
		return zldbpfList;
		
	} 
	
public Zldbpf readZldbpfData(Integer batcactyr,Integer batcactmn,String agntcoy,String agntnum,int flag) {
		
	ResultSet rs = null;
	
	Zldbpf zldb;

	try {
		PreparedStatement stmt = getPrepareStatement(getZldbQry);
		if(flag==1)
		{
			 getZldbQry = "select AGNTCOY,AGNTNUM,BATCACTYR,BATCACTMN,BLPREM,BPAYTY,AGTYPE,PRCNT,PRMAMT,PREMST,NETPRE,PROCFLG from ZLDBPF where BATCACTYR=? and BATCACTMN=?";
			 stmt.setInt(1, batcactyr);
			 stmt.setInt(2, batcactmn);
		}
		else
		{
			stmt.setInt(1, batcactyr);
			stmt.setInt(2, batcactmn);
			stmt.setString(3, agntcoy);
			stmt.setString(4, agntnum);
		}
		
		rs = stmt.executeQuery();

		while (rs.next()) {						
			zldbpfRec.setAgntcoy(rs.getString("AGNTCOY"));
			zldbpfRec.setAgntnum(rs.getString("AGNTNUM"));
			zldbpfRec.setBatcactyr(rs.getInt("BATCACTYR"));
			zldbpfRec.setBatcactyr(rs.getInt("BATCACTMN"));
			zldbpfRec.setBlprem(rs.getBigDecimal("BLPREM"));
			zldbpfRec.setBpayty(rs.getInt("BPAYTY"));
			zldbpfRec.setNetpre(rs.getBigDecimal("NETPRE"));
			zldbpfRec.setProcflg(rs.getString("PROCFLG"));
		}
	}
		catch(SQLException e)
		{
			LOGGER.error(" ZldbpfDAOImpl.readZldbpfData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);	
		}
		
		return zldbpfRec;
	}
	
	
}
