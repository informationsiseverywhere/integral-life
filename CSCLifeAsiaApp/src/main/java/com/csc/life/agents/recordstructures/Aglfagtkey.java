package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:06
 * Description:
 * Copybook name: AGLFAGTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfagtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfagtFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData aglfagtKey = new FixedLengthStringData(64).isAPartOf(aglfagtFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfagtAgntcoy = new FixedLengthStringData(1).isAPartOf(aglfagtKey, 0);
  	public FixedLengthStringData aglfagtAgntnum = new FixedLengthStringData(8).isAPartOf(aglfagtKey, 1);
  	public FixedLengthStringData aglfagtCurrcode = new FixedLengthStringData(3).isAPartOf(aglfagtKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(aglfagtKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfagtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfagtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}