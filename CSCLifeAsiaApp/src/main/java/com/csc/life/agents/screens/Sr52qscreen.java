package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class Sr52qscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52qScreenVars sv = (Sr52qScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52qscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52qScreenVars screenVars = (Sr52qScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.dlvrmode01.setClassString("");
		screenVars.daexpy01.setClassString("");
		screenVars.letterType01.setClassString("");
		screenVars.daexpy02.setClassString("");
		screenVars.letterType02.setClassString("");
		screenVars.dlvrmode02.setClassString("");
		screenVars.dlvrmode03.setClassString("");
		screenVars.dlvrmode04.setClassString("");
		screenVars.dlvrmode05.setClassString("");
		screenVars.dlvrmode06.setClassString("");
		screenVars.dlvrmode07.setClassString("");
		screenVars.dlvrmode08.setClassString("");
		screenVars.dlvrmode09.setClassString("");
	}

/**
 * Clear all the variables in Sr52qscreen
 */
	public static void clear(VarModel pv) {
		Sr52qScreenVars screenVars = (Sr52qScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.dlvrmode01.clear();
		screenVars.daexpy01.clear();
		screenVars.letterType01.clear();
		screenVars.daexpy02.clear();
		screenVars.letterType02.clear();
		screenVars.dlvrmode02.clear();
		screenVars.dlvrmode03.clear();
		screenVars.dlvrmode04.clear();
		screenVars.dlvrmode05.clear();
		screenVars.dlvrmode06.clear();
		screenVars.dlvrmode07.clear();
		screenVars.dlvrmode08.clear();
		screenVars.dlvrmode09.clear();
	}
}
