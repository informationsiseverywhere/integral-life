package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.life.agents.dataaccess.model.Agbnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AgbnpfDAO extends BaseDAO<Agbnpf> {

	public List<Agbnpf> searchAgbnpfRecord(String agntcoy,String agntnum) throws SQLRuntimeException;


}