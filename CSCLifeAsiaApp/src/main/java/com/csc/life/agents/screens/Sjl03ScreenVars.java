package com.csc.life.agents.screens;


import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl03ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData agntype = DD.agntype.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
		
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
		
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
		
	public FixedLengthStringData subfileArea = new FixedLengthStringData(173);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(77).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agntqualification = DD.qlfy.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData liceno = DD.liceno.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData quadesc = DD.qlfydesc.copy().isAPartOf(subfileFields,23);
	public ZonedDecimalData startdte = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,53);
	public ZonedDecimalData enddte = DD.enddate.copyToZonedDecimal().isAPartOf(subfileFields,61);
	public FixedLengthStringData uprflag = DD.uprflag.copy().isAPartOf(subfileFields,69);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 77);
	public FixedLengthStringData agntqualificationErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData licenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData quadescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData startdteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData enddteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData uprflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea,101);
	public FixedLengthStringData[] agntqualificationOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] licenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] quadescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] startdteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] enddteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] uprflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea,149);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData startdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData enddteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public LongData Sjl03screensflWritten = new LongData(0);
	public LongData Sjl03screenctlWritten = new LongData(0);
	public LongData Sjl03screenWritten = new LongData(0);
	public LongData Sjl03protectWritten = new LongData(0);
	public GeneralTable sjl03screensfl = new GeneralTable(AppVars.getInstance());
	
	private long maxRow;
		
	public long getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(long maxRow) {
		this.maxRow = maxRow;
	}
	
	/*public static final int[] pfInds = new int[] {1, 2, 3, 4, 6, 10, 13, 12, 15, 16, 17, 18, 21, 22, 23, 24};
	public static int[] affectedInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};*/
	
	public boolean hasSubfile() {
		return true;
	}
	
	public Sjl03ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	/*public static int[] getScreenSflPfInds()
	{
		return pfInds;
	}*/
	
	/*public static int[] getScreenSflAffectedInds()
	{
		return affectedInds;
	}*/
	protected void initialiseScreenVars() {
		fieldIndMap.put(agntypeOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agnumOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agtydescOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clntselOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cltnameOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntqualificationOut,new String[] {"06","14", "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(licenoOut,new String[] {"07","13", "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(quadescOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(startdteOut,new String[] {"09","11", "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(enddteOut,new String[] {"10","12", "-10",null, null, null, null, null, null, null, null, null});
		

		screenFields = new BaseData[] {agntype, agnum, agtydesc, clntsel, cltname};
		screenOutFields = new BaseData[][] {agntypeOut, agnumOut, agtydescOut, clntselOut, cltnameOut};
		screenErrFields = new BaseData[] {agntypeErr, agnumErr, agtydescErr, clntselErr, cltnameErr};
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		
		screenSflFields = new BaseData[] { agntqualification, liceno, quadesc, startdteDisp, enddte, uprflag};
		screenSflOutFields = new BaseData[][] {agntqualificationOut, licenoOut, quadescOut, startdteOut, enddteOut, uprflagOut};
		screenSflErrFields = new BaseData[] {agntqualificationErr, licenoErr, quadescErr, startdteErr, enddteErr, uprflagErr};
		
		screenSflDateFields = new BaseData[] {startdte, enddte};
		screenSflDateErrFields = new BaseData[] {startdteErr, enddteErr};
		screenSflDateDispFields = new BaseData[] {startdteDisp, enddteDisp};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sjl03screen.class;
		screenSflRecord = Sjl03screensfl.class;
		screenCtlRecord = Sjl03screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sjl03protect.class;
		
	}
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl03screenctl.lrec.pageSubfile);
	}
}
