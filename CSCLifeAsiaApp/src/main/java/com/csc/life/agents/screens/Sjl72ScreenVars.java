package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl72ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(dataFields, 11);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData leveltyp = DD.saleveltyp.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData leveldesc = DD.saleveldes.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 167);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 171);
	public FixedLengthStringData salestatus = DD.salestatus.copy().isAPartOf(dataFields, 201);
	public FixedLengthStringData reasonmod = DD.reasoncd.copy().isAPartOf(dataFields,211);
	public FixedLengthStringData resndesc = DD.resndetl.copy().isAPartOf(dataFields,215);
	public FixedLengthStringData reasondel = DD.reasoncd.copy().isAPartOf(dataFields,335);
	public ZonedDecimalData moddate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,339);
	public ZonedDecimalData deldate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,347);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,355);
	

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
			.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData leveltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData leveldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData salestatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData reasonmodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData reasondelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData moddateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData deldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,
			getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] leveltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] leveldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] salestatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] reasonmodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] reasondelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] moddateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] deldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);

	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData moddateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData deldateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData scrndesc = new FixedLengthStringData(30);

	public LongData Sjl72screenWritten = new LongData(0);
	public LongData Sjl72protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl72ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,
				new String[] { "01", "35", "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(useridOut,
				new String[] { "03", "36", "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agntbrOut,
				new String[] { "04", "37", "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agbrdescOut,
				new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "71", "40", "-71", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "91", "38", "-91", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "07", null, "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "08", null, "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypOut,
				new String[] { "10", null, "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldescOut,
				new String[] { "11", null, "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "30", "39", "-30", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "13", null, "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(salestatusOut,
				new String[] { "14", null, "-14", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasonmodOut,
				new String[] { "09", "20", "-09", "21", null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndescOut,
				new String[] { "16", "22", "-16", "23", null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasondelOut,
				new String[] { "17", "24", "-17", "25", null, null, null, null, null, null, null, null });
		fieldIndMap.put(moddateOut,
				new String[] { "18", "26", "-18", "27", null, null, null, null, null, null, null, null });
		fieldIndMap.put(deldateOut,
				new String[] { "55", "28", "-55", "29", null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "51", null, "-51", null, null, null, null, null, null, null, null, null });
		
		
		screenFields = new BaseData[] {clntsel,company,userid,agntbr,agbrdesc,agtydesc,agtype,aracde,aradesc,levelno,leveltyp,leveldesc,saledept,saledptdes,salestatus,reasonmod,resndesc,reasondel,moddate,deldate,cltname};
		screenOutFields = new BaseData[][] {clntselOut,companyOut,useridOut,agntbrOut,agbrdescOut,agtydescOut,agtypeOut,aracdeOut,aradescOut,levelnoOut,leveltypOut,leveldescOut,saledeptOut,saledptdesOut,salestatusOut,reasonmodOut,resndescOut,reasondelOut,moddateOut,deldateOut,cltnameOut};
		screenErrFields = new BaseData[] {clntselErr,companyErr,useridErr,agntbrErr,agbrdescErr,agtydescErr,agtypeErr,aracdeErr,aradescErr,levelnoErr,leveltypErr,leveldescErr,saledeptErr,saledptdesErr,salestatusErr,reasonmodErr,resndescErr,reasondelErr,moddateErr,deldateErr,cltnameErr};
		screenDateFields = new BaseData[] {moddate,deldate};
		screenDateErrFields = new BaseData[] {moddateErr,deldateErr};
		screenDateDispFields = new BaseData[] {moddateDisp,deldateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl72screen.class;
		protectRecord = Sjl72protect.class;
	}


	public int getDataAreaSize() {
		return 738;
	}

	public int getDataFieldsSize() {
		return 402;
	}

	public int getErrorIndicatorSize() {
		return 84;
	}

	public int getOutputFieldSize() {
		return 252;
	}


}
