package com.csc.life.agents.dataaccess.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.csc.life.agents.dataaccess.dao.ZqbdpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.GenericDAOImpl;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Zqbdpf;


public  class ZqbdpfDAOImpl extends GenericDAOImpl<Zqbdpf,Long> implements ZqbdpfDAO
{
	
	public List<Zqbdpf> findByCriteria(Zqbdpf zqbdpf)
	{
		Criteria crit = getSession().createCriteria(Zqbdpf.class);
		crit.add(Restrictions.eq("rectype", zqbdpf.getRectype()));
		crit.add(Restrictions.eq("agntcoy", zqbdpf.getAgntcoy()));
		crit.add(Restrictions.eq("batcactyr", zqbdpf.getBatcactyr()));
		crit.add(Restrictions.eq("batcactmn", zqbdpf.getBatcactmn()));
		crit.add(Restrictions.eq("prcdate", zqbdpf.getPrcdate()));
		crit.addOrder(Order.asc("agntcoy"));
		crit.addOrder(Order.asc("agntnum"));
		crit.addOrder(Order.asc("chdrcoy"));
		crit.addOrder(Order.asc("chdrnum"));
		List<Zqbdpf> list = crit.list();
		
		return list;
	}
	
	public List<Zqbdpf> findByAgentCriteria(Zqbdpf zqbdpf)
	{
		Criteria crit = getSession().createCriteria(Zqbdpf.class);
		crit.add(Restrictions.eq("rectype", zqbdpf.getRectype()));
		crit.add(Restrictions.eq("agntcoy", zqbdpf.getAgntcoy()));
		crit.add(Restrictions.eq("batcactyr", zqbdpf.getBatcactyr()));
		crit.add(Restrictions.eq("batcactmn", zqbdpf.getBatcactmn()));
		crit.add(Restrictions.eq("prcdate", zqbdpf.getPrcdate()));
		crit.addOrder(Order.asc("agntcoy"));
		crit.addOrder(Order.asc("agntnum"));
		List<Zqbdpf> list = crit.list();
		
		return list;
	}
	
	public void save(Zqbdpf zqbd)
	{
		getSession().save(zqbd);
	}
	
	public void update(Zqbdpf zqbd)
	{
		getSession().update(zqbd);
	}
}
