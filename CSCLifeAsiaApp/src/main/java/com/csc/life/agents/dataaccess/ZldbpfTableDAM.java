package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class ZldbpfTableDAM extends PFAdapterDAM {
	
	public int pfRecLen = 201;
	public FixedLengthStringData zldbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zldbpfRecord = zldbrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zldbrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zldbrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(zldbrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(zldbrec);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(zldbrec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(zldbrec);
	public FixedLengthStringData genlcur = DD.genlcur.copy().isAPartOf(zldbrec);
	public PackedDecimalData blprem = DD.blprem.copy().isAPartOf(zldbrec);
	public PackedDecimalData premst = DD.premst.copy().isAPartOf(zldbrec);
	public PackedDecimalData bpayty = DD.bpayty.copy().isAPartOf(zldbrec);
	public PackedDecimalData prmamt = DD.prmamt.copy().isAPartOf(zldbrec);
	public PackedDecimalData bonusamt = DD.bonusamt.copy().isAPartOf(zldbrec);
	public PackedDecimalData bnpaid = DD.bnpaid.copy().isAPartOf(zldbrec);
	public PackedDecimalData netpre = DD.netpre.copy().isAPartOf(zldbrec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(zldbrec);
	public PackedDecimalData prcdate = DD.prcdate.copy().isAPartOf(zldbrec);
	public FixedLengthStringData usrprf = DD.usrprf.copy().isAPartOf(zldbrec);
	public FixedLengthStringData jobnm = DD.jobnm.copy().isAPartOf(zldbrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zldbrec);
	
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZldbpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZldbpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZldbpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZldbpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZldbpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZldbpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZldbpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}
	
	public void setTable() {
		TABLEPF = getTableName("ZLDBPF");
	}
	
	public void setJournalled(boolean journalled) {
		this.journalled = journalled;
	}

	public String getPFTable() {
		return TABLEPF;
	}
	
	public void setColumnConstants() {
		
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"AGTYPE, " +
							"PRCNT, " +
							"GENLCUR, " +
							"BLPREM, " +
							"PREMST, " +
							"PRMAMT, " +
							"BPAYTY, " +
							"BONUSAMT, " +
							"BNPAID, " +
							"NETPRE, " +
							"PROCFLG, " +
							"PRCDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}
	public void setColumns() {
		
		qualifiedColumns = new BaseData[] {
									agntcoy,
									agntnum,
									batcactyr,
									batcactmn,
									agtype,
									prcnt,
									genlcur,
									blprem,
									premst,
									prmamt,
									bpayty,
									bonusamt,
									bnpaid,
									netpre,
									procflg,
									prcdate,
									usrprf,
									jobnm,
									datime,
                                    unique_number
        };
	}
	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
		agntcoy.clear();
		agntnum.clear();
		batcactyr.clear();
		batcactmn.clear();
		agtype.clear();
		prcnt.clear();
		genlcur.clear();
		blprem.clear();
		premst.clear();
		prmamt.clear();
		bpayty.clear();
		bonusamt.clear();
		bnpaid.clear();
		netpre.clear();
		procflg.clear();
		prcdate.clear();
		usrprf.clear();
		jobnm.clear();
		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZldbrec() {
  		return zldbrec;
	}

	public FixedLengthStringData getZldbpfRecord() {
  		return zldbpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZldbrec(what);
	}

	public void setZldbrec(Object what) {
  		this.zldbrec.set(what);
	}

	public void setZldbpfRecord(Object what) {
  		this.zldbpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zldbrec.getLength());
		result.set(zldbrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	
}