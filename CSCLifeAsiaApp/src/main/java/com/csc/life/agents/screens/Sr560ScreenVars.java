package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR560
 * @version 1.0 generated on 30/08/09 07:19
 * @author Quipoz
 */
public class Sr560ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	public FixedLengthStringData agntype = DD.agntype.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(153);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(55).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agtypes = new FixedLengthStringData(4).isAPartOf(subfileFields, 0);
	public FixedLengthStringData[] agtype = FLSArrayPartOfStructure(2, 2, agtypes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(agtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtype01 = DD.agtype.copy().isAPartOf(filler,0);
	public FixedLengthStringData agtype02 = DD.agtype.copy().isAPartOf(filler,2);
	public FixedLengthStringData desc = DD.desca.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData dteapps = new FixedLengthStringData(16).isAPartOf(subfileFields, 29);
	public ZonedDecimalData[] dteapp = ZDArrayPartOfStructure(2, 8, 0, dteapps, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(dteapps, 0, FILLER_REDEFINE);
	public ZonedDecimalData dteapp01 = DD.dteapp.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData dteapp02 = DD.dteapp.copyToZonedDecimal().isAPartOf(filler1,8);
	public FixedLengthStringData zrreptp = DD.zrreptp.copy().isAPartOf(subfileFields,45);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 55);
	public FixedLengthStringData agtypesErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData[] agtypeErr = FLSArrayPartOfStructure(2, 4, agtypesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(agtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtype01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData agtype02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData descaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData dteappsErr = new FixedLengthStringData(8).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData[] dteappErr = FLSArrayPartOfStructure(2, 4, dteappsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(dteappsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData dteapp01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData dteapp02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zrreptpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 79);
	public FixedLengthStringData agtypesOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 0);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(2, 12, agtypesOut, 0);
	public FixedLengthStringData[][] agtypeO = FLSDArrayPartOfArrayStructure(12, 1, agtypeOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(agtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agtype01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] agtype02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] descaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData dteappsOut = new FixedLengthStringData(24).isAPartOf(outputSubfile, 36);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(2, 12, dteappsOut, 0);
	public FixedLengthStringData[][] dteappO = FLSDArrayPartOfArrayStructure(12, 1, dteappOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(dteappsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] dteapp01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] dteapp02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] zrreptpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 151);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dteapp01Disp = new FixedLengthStringData(10);
	public FixedLengthStringData dteapp02Disp = new FixedLengthStringData(10);

	public LongData Sr560screensflWritten = new LongData(0);
	public LongData Sr560screenctlWritten = new LongData(0);
	public LongData Sr560screenWritten = new LongData(0);
	public LongData Sr560protectWritten = new LongData(0);
	public GeneralTable sr560screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr560screensfl;
	}

	public Sr560ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dteapp01Out,new String[] {"21","30","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrreptpOut,new String[] {"22","30","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agtype01Out,new String[] {"23","30","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dteapp02Out,new String[] {"24","30","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agtype02Out,new String[] {"25","30","-25",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {dteapp01, zrreptp, desc, agtype01, dteapp02, agtype02};
		screenSflOutFields = new BaseData[][] {dteapp01Out, zrreptpOut, descaOut, agtype01Out, dteapp02Out, agtype02Out};
		screenSflErrFields = new BaseData[] {dteapp01Err, zrreptpErr, descaErr, agtype01Err, dteapp02Err, agtype02Err};
		screenSflDateFields = new BaseData[] {dteapp01, dteapp02};
		screenSflDateErrFields = new BaseData[] {dteapp01Err, dteapp02Err};
		screenSflDateDispFields = new BaseData[] {dteapp01Disp, dteapp02Disp};

		screenFields = new BaseData[] {clntsel, agnum, agntype, cltname, agtydesc};
		screenOutFields = new BaseData[][] {clntselOut, agnumOut, agntypeOut, cltnameOut, agtydescOut};
		screenErrFields = new BaseData[] {clntselErr, agnumErr, agntypeErr, cltnameErr, agtydescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr560screen.class;
		screenSflRecord = Sr560screensfl.class;
		screenCtlRecord = Sr560screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr560protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr560screenctl.lrec.pageSubfile);
	}
}
