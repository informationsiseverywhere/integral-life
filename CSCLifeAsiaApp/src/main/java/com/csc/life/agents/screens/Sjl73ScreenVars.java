package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl73ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltyp = DD.saleveltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldesc = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agncysel = DD.agncysel.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData agncydesc = DD.cltname.copy().isAPartOf(dataFields,204);
	public FixedLengthStringData regnum = DD.agncyRegnum.copy().isAPartOf(dataFields,251);
	public ZonedDecimalData srdate = DD.srdate.copyToZonedDecimal().isAPartOf(dataFields,264);
	public ZonedDecimalData endate = DD.enddate.copyToZonedDecimal().isAPartOf(dataFields,272);
	public ZonedDecimalData regdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,280);
	public FixedLengthStringData reasonreg = DD.reasoncd.copy().isAPartOf(dataFields,288);
	public FixedLengthStringData resndesc = DD.resndetl.copy().isAPartOf(dataFields,292);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize())
			.isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agncyselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData agncydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData srdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData endateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData regdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData reasonregErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea,
			getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agncyselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] agncydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] srdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] endateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] regdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] reasonregOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);

	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData regdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData endateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData srdateDisp = new FixedLengthStringData(10);

	public LongData Sjl73screenWritten = new LongData(0);
	public LongData Sjl73protectWritten = new LongData(0);

	public FixedLengthStringData tranflag = new FixedLengthStringData(1); 
	
	public boolean hasSubfile() {
		return false;
	}

	public Sjl73ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,
				new String[] { "01", "35", "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agntbrOut,
				new String[] { "04", "37", "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agbrdescOut,
				new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "91", "38", "-91", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "07", null, "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "08", null, "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypOut,
				new String[] { "10", null, "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldescOut,
				new String[] { "11", null, "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "30", "39", "-30", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "13", null, "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasonregOut,
				new String[] { "09", "20", "-09", "21", null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndescOut,
				new String[] { "16", "22", "-16", "23", null, null, null, null, null, null, null, null });
		fieldIndMap.put(regdateOut,
				new String[] { "18", "26", "-18", "27", null, null, null, null, null, null, null, null });
		fieldIndMap.put(agncyselOut,
				new String[] { "03", "28", "-03", "29", null, null, null, null, null, null, null, null });
		
		
		screenFields = new BaseData[] {clntsel,cltname,company,agntbr,agbrdesc,aracde,aradesc,levelno,leveltyp,leveldesc,saledept,saledptdes,agncysel,agncydesc,regnum,srdate,endate,regdate,reasonreg,resndesc};
		screenOutFields = new BaseData[][] {clntselOut,cltnameOut,companyOut,agntbrOut,agbrdescOut,aracdeOut,aradescOut,levelnoOut,leveltypOut,leveldescOut,saledeptOut,saledptdesOut,agncyselOut,agncydescOut,regnumOut,srdateOut,endateOut,regdateOut,reasonregOut,resndescOut};
		screenErrFields = new BaseData[] {clntselErr,cltnameErr,companyErr,agntbrErr,agbrdescErr,aracdeErr,aradescErr,levelnoErr,leveltypErr,leveldescErr,saledeptErr,saledptdesErr,agncyselErr,agncydescErr,regnumErr,srdateErr,endateErr,regdateErr,reasonregErr,resndescErr};
		screenDateFields = new BaseData[] {regdate,srdate,endate};
		screenDateErrFields = new BaseData[] {regdateErr,srdateErr,endateErr};
		screenDateDispFields = new BaseData[] {regdateDisp,srdateDisp,endateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl73screen.class;
		protectRecord = Sjl73protect.class;
	}


	public int getDataAreaSize() {
		return 732;
	}

	public int getDataFieldsSize() {
		return 412;
	}

	public int getErrorIndicatorSize() {
		return 80;
	}

	public int getOutputFieldSize() {
		return 240;
	}


}
