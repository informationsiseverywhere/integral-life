package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl29 Date: 03 February 2020 Author: vdivisala
 */
public class Sjl29ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(125);
	public FixedLengthStringData dataFields = new FixedLengthStringData(45).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 14);
	public FixedLengthStringData cmgwgndr = DD.cmgwgndr.copy().isAPartOf(dataFields, 44);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 45);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cmgwgndrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 65);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cmgwgndrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl29screenWritten = new LongData(0);
	public LongData Sjl29protectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sjl29ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected final void initialiseScreenVars() {
		fieldIndMap.put(cmgwgndrOut, new String[] { "05", "25", "-05", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, cmgwgndr };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, cmgwgndrOut };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, cmgwgndrErr };
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl29screen.class;
		protectRecord = Sjl29protect.class;
	}
}