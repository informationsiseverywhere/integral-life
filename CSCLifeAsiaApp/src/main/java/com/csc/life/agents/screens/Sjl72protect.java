package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl72protect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sjl72ScreenVars sv = (Sjl72ScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sjl72protectWritten, null, av, null, ind2, ind3);
		}


	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl72ScreenVars screenVars = (Sjl72ScreenVars)pv;
		ScreenRecord.setClassStringFormatting(pv);
	}

	public static void clear(VarModel pv) {
		Sjl72ScreenVars screenVars = (Sjl72ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
}
