/*
 * File: P5565.java
 * Date: 30 August 2009 0:31:03
 * Author: Quipoz Limited
 * 
 * Class transformed from P5565.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.procedures.T5565pt;
import com.csc.life.agents.screens.S5565ScreenVars;
import com.csc.life.agents.tablestructures.T5565rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
****************************************************************** ****
* </pre>
*/
public class P5565 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5565");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5565rec t5565rec = new T5565rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5565ScreenVars sv = ScreenProgram.getScreenVars( S5565ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		other3080, 
		exit3090
	}

	public P5565() {
		super();
		screenVars = sv;
		new ScreenModel("S5565", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5565rec.t5565Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			return ;
		}
		t5565rec.anbAtCcd01.set(ZERO);
		t5565rec.anbAtCcd02.set(ZERO);
		t5565rec.anbAtCcd03.set(ZERO);
		t5565rec.anbAtCcd04.set(ZERO);
		t5565rec.anbAtCcd05.set(ZERO);
		t5565rec.anbAtCcd06.set(ZERO);
		t5565rec.anbAtCcd07.set(ZERO);
		t5565rec.anbAtCcd08.set(ZERO);
		t5565rec.anbAtCcd09.set(ZERO);
		t5565rec.anbAtCcd10.set(ZERO);
		t5565rec.prcnt01.set(ZERO);
		t5565rec.prcnt02.set(ZERO);
		t5565rec.prcnt03.set(ZERO);
		t5565rec.prcnt04.set(ZERO);
		t5565rec.prcnt05.set(ZERO);
		t5565rec.prcnt06.set(ZERO);
		t5565rec.prcnt07.set(ZERO);
		t5565rec.prcnt08.set(ZERO);
		t5565rec.prcnt09.set(ZERO);
		t5565rec.prcnt10.set(ZERO);
		t5565rec.prcnt11.set(ZERO);
		t5565rec.prcnt12.set(ZERO);
		t5565rec.prcnt13.set(ZERO);
		t5565rec.prcnt14.set(ZERO);
		t5565rec.prcnt15.set(ZERO);
		t5565rec.prcnt16.set(ZERO);
		t5565rec.prcnt17.set(ZERO);
		t5565rec.prcnt18.set(ZERO);
		t5565rec.prcnt19.set(ZERO);
		t5565rec.prcnt20.set(ZERO);
		t5565rec.prcnt21.set(ZERO);
		t5565rec.prcnt22.set(ZERO);
		t5565rec.prcnt23.set(ZERO);
		t5565rec.prcnt24.set(ZERO);
		t5565rec.prcnt25.set(ZERO);
		t5565rec.prcnt26.set(ZERO);
		t5565rec.prcnt27.set(ZERO);
		t5565rec.prcnt28.set(ZERO);
		t5565rec.prcnt29.set(ZERO);
		t5565rec.prcnt30.set(ZERO);
		t5565rec.prcnt31.set(ZERO);
		t5565rec.prcnt32.set(ZERO);
		t5565rec.prcnt33.set(ZERO);
		t5565rec.prcnt34.set(ZERO);
		t5565rec.prcnt35.set(ZERO);
		t5565rec.prcnt36.set(ZERO);
		t5565rec.prcnt37.set(ZERO);
		t5565rec.prcnt38.set(ZERO);
		t5565rec.prcnt39.set(ZERO);
		t5565rec.prcnt40.set(ZERO);
		t5565rec.prcnt41.set(ZERO);
		t5565rec.prcnt42.set(ZERO);
		t5565rec.prcnt43.set(ZERO);
		t5565rec.prcnt44.set(ZERO);
		t5565rec.prcnt45.set(ZERO);
		t5565rec.prcnt46.set(ZERO);
		t5565rec.prcnt47.set(ZERO);
		t5565rec.prcnt48.set(ZERO);
		t5565rec.prcnt49.set(ZERO);
		t5565rec.prcnt50.set(ZERO);
		t5565rec.prcnt51.set(ZERO);
		t5565rec.prcnt52.set(ZERO);
		t5565rec.prcnt53.set(ZERO);
		t5565rec.prcnt54.set(ZERO);
		t5565rec.prcnt55.set(ZERO);
		t5565rec.prcnt56.set(ZERO);
		t5565rec.prcnt57.set(ZERO);
		t5565rec.prcnt58.set(ZERO);
		t5565rec.prcnt59.set(ZERO);
		t5565rec.prcnt60.set(ZERO);
		t5565rec.prcnt61.set(ZERO);
		t5565rec.prcnt62.set(ZERO);
		t5565rec.prcnt63.set(ZERO);
		t5565rec.prcnt64.set(ZERO);
		t5565rec.prcnt65.set(ZERO);
		t5565rec.prcnt66.set(ZERO);
		t5565rec.prcnt67.set(ZERO);
		t5565rec.prcnt68.set(ZERO);
		t5565rec.prcnt69.set(ZERO);
		t5565rec.prcnt70.set(ZERO);
		t5565rec.prcnt71.set(ZERO);
		t5565rec.prcnt72.set(ZERO);
		t5565rec.prcnt73.set(ZERO);
		t5565rec.prcnt74.set(ZERO);
		t5565rec.prcnt75.set(ZERO);
		t5565rec.prcnt76.set(ZERO);
		t5565rec.prcnt77.set(ZERO);
		t5565rec.prcnt78.set(ZERO);
		t5565rec.prcnt79.set(ZERO);
		t5565rec.prcnt80.set(ZERO);
		t5565rec.prcnt81.set(ZERO);
		t5565rec.prcnt82.set(ZERO);
		t5565rec.prcnt83.set(ZERO);
		t5565rec.prcnt84.set(ZERO);
		t5565rec.prcnt85.set(ZERO);
		t5565rec.prcnt86.set(ZERO);
		t5565rec.prcnt87.set(ZERO);
		t5565rec.prcnt88.set(ZERO);
		t5565rec.prcnt89.set(ZERO);
		t5565rec.prcnt90.set(ZERO);
		t5565rec.termMax01.set(ZERO);
		t5565rec.termMax02.set(ZERO);
		t5565rec.termMax03.set(ZERO);
		t5565rec.termMax04.set(ZERO);
		t5565rec.termMax05.set(ZERO);
		t5565rec.termMax06.set(ZERO);
		t5565rec.termMax07.set(ZERO);
		t5565rec.termMax08.set(ZERO);
		t5565rec.termMax09.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.anbAtCcds.set(t5565rec.anbAtCcds);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.prcnts.set(t5565rec.prcnts);
		/*    MOVE T5565-TERM-MAXS                                         */
		/*      TO S5565-TERM-MAXS               .                         */
		for (ix.set(1); !(isGT(ix, 9)); ix.add(1)){
			sv.termMax[ix.toInt()].set(t5565rec.termMax[ix.toInt()]);
		}
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5565rec.t5565Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.anbAtCcds, t5565rec.anbAtCcds)) {
			t5565rec.anbAtCcds.set(sv.anbAtCcds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.prcnts, t5565rec.prcnts)) {
			t5565rec.prcnts.set(sv.prcnts);
			wsaaUpdateFlag = "Y";
		}
		/*    IF S5565-TERM-MAXS               NOT =                       */
		/*       T5565-TERM-MAXS                                           */
		/*        MOVE S5565-TERM-MAXS                                     */
		/*          TO T5565-TERM-MAXS                                     */
		/*        MOVE 'Y' TO WSAA-UPDATE-FLAG.                            */
		for (ix.set(1); !(isGT(ix, 9)); ix.add(1)){
			if (isNE(t5565rec.termMax[ix.toInt()], NUMERIC)) {
				t5565rec.termMax[ix.toInt()].set(sv.termMax[ix.toInt()]);
				wsaaUpdateFlag = "Y";
			}
			if (isNE(t5565rec.termMax[ix.toInt()], sv.termMax[ix.toInt()])) {
				t5565rec.termMax[ix.toInt()].set(sv.termMax[ix.toInt()]);
				wsaaUpdateFlag = "Y";
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5565pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
