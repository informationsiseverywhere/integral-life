package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpbTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:50
 * Class transformed from AGPB.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpbTableDAM extends AgpbpfTableDAM {

	public AgpbTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGPB");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "RLDGCOY"
		             + ", RLDGACCT"
		             + ", CURRCODE";
		
		QUALIFIEDCOLUMNS = 
		            "RLDGCOY, " +
		            "RLDGACCT, " +
		            "SACSCURBAL, " +
		            "PROCFLG, " +
		            "TAGSUSIND, " +
		            "TAXCDE, " +
		            "CURRCODE, " +
		            "AGCCQIND, " +
		            "PAYCLT, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "WHTAX, " +
		            "TCOLBAL, " +
		            "TDEDUCT, " +
		            "TDCCHRG, " +
		            "PAYMTH, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "RLDGCOY ASC, " +
		            "RLDGACCT ASC, " +
		            "CURRCODE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "RLDGCOY DESC, " +
		            "RLDGACCT DESC, " +
		            "CURRCODE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rldgcoy,
                               rldgacct,
                               sacscurbal,
                               procflg,
                               tagsusind,
                               taxcde,
                               currcode,
                               agccqind,
                               payclt,
                               facthous,
                               bankkey,
                               bankacckey,
                               whtax,
                               tcolbal,
                               tdeduct,
                               tdcchrg,
                               paymth,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(236);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getRldgcoy().toInternal()
					+ getRldgacct().toInternal()
					+ getCurrcode().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(16);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(rldgcoy.toInternal());
	nonKeyFiller20.setInternal(rldgacct.toInternal());
	nonKeyFiller70.setInternal(currcode.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(152);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getSacscurbal().toInternal()
					+ getProcflg().toInternal()
					+ getTagsusind().toInternal()
					+ getTaxcde().toInternal()
					+ nonKeyFiller70.toInternal()
					+ getAgccqind().toInternal()
					+ getPayclt().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getWhtax().toInternal()
					+ getTcolbal().toInternal()
					+ getTdeduct().toInternal()
					+ getTdcchrg().toInternal()
					+ getPaymth().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, sacscurbal);
			what = ExternalData.chop(what, procflg);
			what = ExternalData.chop(what, tagsusind);
			what = ExternalData.chop(what, taxcde);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, agccqind);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, whtax);
			what = ExternalData.chop(what, tcolbal);
			what = ExternalData.chop(what, tdeduct);
			what = ExternalData.chop(what, tdcchrg);
			what = ExternalData.chop(what, paymth);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getSacscurbal() {
		return sacscurbal;
	}
	public void setSacscurbal(Object what) {
		setSacscurbal(what, false);
	}
	public void setSacscurbal(Object what, boolean rounded) {
		if (rounded)
			sacscurbal.setRounded(what);
		else
			sacscurbal.set(what);
	}	
	public FixedLengthStringData getProcflg() {
		return procflg;
	}
	public void setProcflg(Object what) {
		procflg.set(what);
	}	
	public FixedLengthStringData getTagsusind() {
		return tagsusind;
	}
	public void setTagsusind(Object what) {
		tagsusind.set(what);
	}	
	public FixedLengthStringData getTaxcde() {
		return taxcde;
	}
	public void setTaxcde(Object what) {
		taxcde.set(what);
	}	
	public FixedLengthStringData getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(Object what) {
		agccqind.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public PackedDecimalData getWhtax() {
		return whtax;
	}
	public void setWhtax(Object what) {
		setWhtax(what, false);
	}
	public void setWhtax(Object what, boolean rounded) {
		if (rounded)
			whtax.setRounded(what);
		else
			whtax.set(what);
	}	
	public PackedDecimalData getTcolbal() {
		return tcolbal;
	}
	public void setTcolbal(Object what) {
		setTcolbal(what, false);
	}
	public void setTcolbal(Object what, boolean rounded) {
		if (rounded)
			tcolbal.setRounded(what);
		else
			tcolbal.set(what);
	}	
	public PackedDecimalData getTdeduct() {
		return tdeduct;
	}
	public void setTdeduct(Object what) {
		setTdeduct(what, false);
	}
	public void setTdeduct(Object what, boolean rounded) {
		if (rounded)
			tdeduct.setRounded(what);
		else
			tdeduct.set(what);
	}	
	public PackedDecimalData getTdcchrg() {
		return tdcchrg;
	}
	public void setTdcchrg(Object what) {
		setTdcchrg(what, false);
	}
	public void setTdcchrg(Object what, boolean rounded) {
		if (rounded)
			tdcchrg.setRounded(what);
		else
			tdcchrg.set(what);
	}	
	public FixedLengthStringData getPaymth() {
		return paymth;
	}
	public void setPaymth(Object what) {
		paymth.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		rldgcoy.clear();
		rldgacct.clear();
		currcode.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		sacscurbal.clear();
		procflg.clear();
		tagsusind.clear();
		taxcde.clear();
		nonKeyFiller70.clear();
		agccqind.clear();
		payclt.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		whtax.clear();
		tcolbal.clear();
		tdeduct.clear();
		tdcchrg.clear();
		paymth.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}