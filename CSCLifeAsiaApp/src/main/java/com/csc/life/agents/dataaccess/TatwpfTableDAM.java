package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: TatwpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:33
 * Class transformed from TATWPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class TatwpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 82;
	public FixedLengthStringData tatwrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData tatwpfRecord = tatwrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(tatwrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(tatwrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(tatwrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(tatwrec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(tatwrec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(tatwrec);
	public PackedDecimalData taxamt = DD.taxamt.copy().isAPartOf(tatwrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(tatwrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(tatwrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(tatwrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public TatwpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for TatwpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public TatwpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for TatwpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public TatwpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for TatwpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public TatwpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("TATWPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"VALIDFLAG, " +
							"EFFDATE, " +
							"ORIGAMT, " +
							"ORIGCURR, " +
							"TAXAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     validflag,
                                     effdate,
                                     origamt,
                                     origcurr,
                                     taxamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		validflag.clear();
  		effdate.clear();
  		origamt.clear();
  		origcurr.clear();
  		taxamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getTatwrec() {
  		return tatwrec;
	}

	public FixedLengthStringData getTatwpfRecord() {
  		return tatwpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setTatwrec(what);
	}

	public void setTatwrec(Object what) {
  		this.tatwrec.set(what);
	}

	public void setTatwpfRecord(Object what) {
  		this.tatwpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(tatwrec.getLength());
		result.set(tatwrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}