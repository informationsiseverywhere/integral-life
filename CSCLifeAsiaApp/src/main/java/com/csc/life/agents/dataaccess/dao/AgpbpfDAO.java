/*

 * File: AgpbpfDAOImpl.java
 * Date: August 16, 2019
 * Author: DXC
 * Created by: sbatra9
 * 
 * Copyright DXC, all rights reserved.
 */

package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.life.agents.dataaccess.model.Agbnpf;
import com.csc.life.agents.dataaccess.model.Agpbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AgpbpfDAO extends BaseDAO<Agpbpf> {

	public void insertAgpbpfRecord(Agpbpf agpbpf);


}