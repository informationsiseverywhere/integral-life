package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr520screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr520ScreenVars sv = (Sr520ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr520screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr520ScreenVars screenVars = (Sr520ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.zcomcode01.setClassString("");
		screenVars.zryrperc01.setClassString("");
		screenVars.zryrperc02.setClassString("");
		screenVars.zryrperc03.setClassString("");
		screenVars.zryrperc04.setClassString("");
		screenVars.zryrperc05.setClassString("");
		screenVars.zryrperc06.setClassString("");
		screenVars.zcomcode02.setClassString("");
		screenVars.zryrperc07.setClassString("");
		screenVars.zryrperc08.setClassString("");
		screenVars.zryrperc09.setClassString("");
		screenVars.zryrperc10.setClassString("");
		screenVars.zryrperc11.setClassString("");
		screenVars.zryrperc12.setClassString("");
		screenVars.zcomcode03.setClassString("");
		screenVars.zryrperc13.setClassString("");
		screenVars.zryrperc14.setClassString("");
		screenVars.zryrperc15.setClassString("");
		screenVars.zryrperc16.setClassString("");
		screenVars.zryrperc17.setClassString("");
		screenVars.zryrperc18.setClassString("");
		screenVars.zcomcode04.setClassString("");
		screenVars.zryrperc19.setClassString("");
		screenVars.zryrperc20.setClassString("");
		screenVars.zryrperc21.setClassString("");
		screenVars.zryrperc22.setClassString("");
		screenVars.zryrperc23.setClassString("");
		screenVars.zryrperc24.setClassString("");
		screenVars.zcomcode05.setClassString("");
		screenVars.zryrperc25.setClassString("");
		screenVars.zryrperc26.setClassString("");
		screenVars.zryrperc27.setClassString("");
		screenVars.zryrperc28.setClassString("");
		screenVars.zryrperc29.setClassString("");
		screenVars.zryrperc30.setClassString("");
		screenVars.zcomcode06.setClassString("");
		screenVars.zryrperc31.setClassString("");
		screenVars.zryrperc32.setClassString("");
		screenVars.zryrperc33.setClassString("");
		screenVars.zryrperc34.setClassString("");
		screenVars.zryrperc35.setClassString("");
		screenVars.zryrperc36.setClassString("");
		screenVars.zcomcode07.setClassString("");
		screenVars.zryrperc37.setClassString("");
		screenVars.zryrperc38.setClassString("");
		screenVars.zryrperc39.setClassString("");
		screenVars.zryrperc40.setClassString("");
		screenVars.zryrperc41.setClassString("");
		screenVars.zryrperc42.setClassString("");
		screenVars.zcomcode08.setClassString("");
		screenVars.zryrperc43.setClassString("");
		screenVars.zryrperc44.setClassString("");
		screenVars.zryrperc45.setClassString("");
		screenVars.zryrperc46.setClassString("");
		screenVars.zryrperc47.setClassString("");
		screenVars.zryrperc48.setClassString("");
		screenVars.zcomcode09.setClassString("");
		screenVars.zryrperc49.setClassString("");
		screenVars.zryrperc50.setClassString("");
		screenVars.zryrperc51.setClassString("");
		screenVars.zryrperc52.setClassString("");
		screenVars.zryrperc53.setClassString("");
		screenVars.zryrperc54.setClassString("");
		screenVars.zcomcode10.setClassString("");
		screenVars.zryrperc55.setClassString("");
		screenVars.zryrperc56.setClassString("");
		screenVars.zryrperc57.setClassString("");
		screenVars.zryrperc58.setClassString("");
		screenVars.zryrperc59.setClassString("");
		screenVars.zryrperc60.setClassString("");
		screenVars.contitem.setClassString("");
		screenVars.zryrperc61.setClassString("");
		screenVars.zryrperc62.setClassString("");
		screenVars.zryrperc63.setClassString("");
		screenVars.zryrperc64.setClassString("");
		screenVars.zryrperc65.setClassString("");
		screenVars.zryrperc66.setClassString("");
		screenVars.zryrperc67.setClassString("");
		screenVars.zryrperc68.setClassString("");
		screenVars.zryrperc69.setClassString("");
		screenVars.zryrperc70.setClassString("");
	}

/**
 * Clear all the variables in Sr520screen
 */
	public static void clear(VarModel pv) {
		Sr520ScreenVars screenVars = (Sr520ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.zcomcode01.clear();
		screenVars.zryrperc01.clear();
		screenVars.zryrperc02.clear();
		screenVars.zryrperc03.clear();
		screenVars.zryrperc04.clear();
		screenVars.zryrperc05.clear();
		screenVars.zryrperc06.clear();
		screenVars.zcomcode02.clear();
		screenVars.zryrperc07.clear();
		screenVars.zryrperc08.clear();
		screenVars.zryrperc09.clear();
		screenVars.zryrperc10.clear();
		screenVars.zryrperc11.clear();
		screenVars.zryrperc12.clear();
		screenVars.zcomcode03.clear();
		screenVars.zryrperc13.clear();
		screenVars.zryrperc14.clear();
		screenVars.zryrperc15.clear();
		screenVars.zryrperc16.clear();
		screenVars.zryrperc17.clear();
		screenVars.zryrperc18.clear();
		screenVars.zcomcode04.clear();
		screenVars.zryrperc19.clear();
		screenVars.zryrperc20.clear();
		screenVars.zryrperc21.clear();
		screenVars.zryrperc22.clear();
		screenVars.zryrperc23.clear();
		screenVars.zryrperc24.clear();
		screenVars.zcomcode05.clear();
		screenVars.zryrperc25.clear();
		screenVars.zryrperc26.clear();
		screenVars.zryrperc27.clear();
		screenVars.zryrperc28.clear();
		screenVars.zryrperc29.clear();
		screenVars.zryrperc30.clear();
		screenVars.zcomcode06.clear();
		screenVars.zryrperc31.clear();
		screenVars.zryrperc32.clear();
		screenVars.zryrperc33.clear();
		screenVars.zryrperc34.clear();
		screenVars.zryrperc35.clear();
		screenVars.zryrperc36.clear();
		screenVars.zcomcode07.clear();
		screenVars.zryrperc37.clear();
		screenVars.zryrperc38.clear();
		screenVars.zryrperc39.clear();
		screenVars.zryrperc40.clear();
		screenVars.zryrperc41.clear();
		screenVars.zryrperc42.clear();
		screenVars.zcomcode08.clear();
		screenVars.zryrperc43.clear();
		screenVars.zryrperc44.clear();
		screenVars.zryrperc45.clear();
		screenVars.zryrperc46.clear();
		screenVars.zryrperc47.clear();
		screenVars.zryrperc48.clear();
		screenVars.zcomcode09.clear();
		screenVars.zryrperc49.clear();
		screenVars.zryrperc50.clear();
		screenVars.zryrperc51.clear();
		screenVars.zryrperc52.clear();
		screenVars.zryrperc53.clear();
		screenVars.zryrperc54.clear();
		screenVars.zcomcode10.clear();
		screenVars.zryrperc55.clear();
		screenVars.zryrperc56.clear();
		screenVars.zryrperc57.clear();
		screenVars.zryrperc58.clear();
		screenVars.zryrperc59.clear();
		screenVars.zryrperc60.clear();
		screenVars.contitem.clear();
		screenVars.zryrperc61.clear();
		screenVars.zryrperc62.clear();
		screenVars.zryrperc63.clear();
		screenVars.zryrperc64.clear();
		screenVars.zryrperc65.clear();
		screenVars.zryrperc66.clear();
		screenVars.zryrperc67.clear();
		screenVars.zryrperc68.clear();
		screenVars.zryrperc69.clear();
		screenVars.zryrperc70.clear();
	}
}
