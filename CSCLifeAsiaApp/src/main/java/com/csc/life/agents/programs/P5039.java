/*
 * File: P5039.java
 * Date: 29 August 2009 23:59:20
 * Author: Quipoz Limited
 * 
 * Class transformed from P5039.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AgbnTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.S5039ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                     REPORTING TO SUBFILE
*                     ====================
* Initialise
* ----------
*
*  Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*  information.
*
*  Each agent optionally  has another agent to which it reports.
*  The "chain of  command"  can  be  found  by reading the agent
*  record for the  "reports  to"  agent.  This  may  also have a
*  "reports to" agent, and so on.
*
*  Starting with the agent first reported to, follow the "chain"
*  loading one record into the subfile for each agent read. Load
*  all agents into  the subfile without considering subfile page
*  size.
*
*
* Validation, Updating, Next Program
* ----------------------------------
*
*  Standard processing.
*
*****************************************************************
* </pre>
*/
public class P5039 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5039");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* TABLES */
	private String t3692 = "T3692";
		/*LIFE AGENT / BROKER CONTACT NAMES*/
	private AgbnTableDAM agbnIO = new AgbnTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5039ScreenVars sv = getLScreenVars();

	protected S5039ScreenVars getLScreenVars(){
		return ScreenProgram.getScreenVars( S5039ScreenVars.class);
	}
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1190, 
		preExit
	}

	public P5039() {
		super();
		screenVars = sv;
		new ScreenModel("S5039", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		screenHeader1020();
		headerAgentType1030();
		subfileProcess1040();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5039", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void screenHeader1020()
	{
		aglfIO.setFunction("RETRV");
		callAglf1200();
		agntlagIO.setDataKey(SPACES);
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		sv.agnum.set(aglfIO.getAgntnum());
		callAgntlag1300();
		sv.clntsel.set(agntlagIO.getClntnum());
		callClts1400();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void headerAgentType1030()
	{
		sv.agtype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntlagIO.getAgtype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.agtydesc.set(descIO.getLongdesc());
	}

protected void subfileProcess1040()
	{
		scrnparams.subfileMore.set("Y");
		while ( !(isEQ(scrnparams.subfileMore,"N"))) {
			loadSubfile1100();
		}
		
		/*EXIT*/
	}

protected void loadSubfile1100()
	{
		try {
			readAgent1110();
			reportToAgentDetails1120();
		}
		catch (GOTOException e){
		}
	}

protected void readAgent1110()
	{
		if (isEQ(aglfIO.getReportag(),SPACES)) {
			scrnparams.subfileMore.set("N");
			goTo(GotoLabel.exit1190);
		}
		sv.reportag.set(aglfIO.getReportag());
		sv.ovcpc.set(aglfIO.getOvcpc());
		agntlagIO.setDataKey(SPACES);
		agntlagIO.setAgntnum(aglfIO.getReportag());
		callAgntlag1300();
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		sv.clntnum.set(agntlagIO.getClntnum());
		callClts1400();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void reportToAgentDetails1120()
	{
		aglfIO.setAgntnum(aglfIO.getReportag());
		aglfIO.setFunction(varcom.readr);
		callAglf1200();
		/*ADD-TO-SUBFILE*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5039", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void callAglf1200()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callAgntlag1300()
	{
		/*READR-AGENT-FILE*/
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClts1400()
	{
		/*READ-CLIENT-FILE*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S5039", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		updateErrorIndicators2670();
		readNextModifiedRecord2680();
	}

protected void updateErrorIndicators2670()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("S5039", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5039", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
