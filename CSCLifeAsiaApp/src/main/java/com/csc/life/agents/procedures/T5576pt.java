/*
 * File: T5576pt.java
 * Date: 30 August 2009 2:23:15
 * Author: Quipoz Limited
 * 
 * Class transformed from T5576PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.T5576rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5576.
*
*
*****************************************************************
* </pre>
*/
public class T5576pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 27, FILLER).init("Term Multiplier Commission Rates            S5576");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(77);
	private FixedLengthStringData filler9 = new FixedLengthStringData(77).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" To age       % / Annum       Max Age        Min  %      Max  %        Flat %");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler10 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 3).setPattern("ZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 31).setPattern("ZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine005, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine005, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(77);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 3).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine006, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 31).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 3).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 31).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 3).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 31).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine008, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler34 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 3).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 31).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 3).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine010, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 31).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 3).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 31).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(77);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 3).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine012, 6, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 31).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 63, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(25);
	private FixedLengthStringData filler58 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler59 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine013, 9, FILLER).init("Year  Month  Day");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(19);
	private FixedLengthStringData filler60 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Higher:");
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 11);
	private FixedLengthStringData filler61 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 18);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(19);
	private FixedLengthStringData filler62 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 0, FILLER).init("  Lower:");
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 11);
	private FixedLengthStringData filler63 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 12, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 18);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(24);
	private FixedLengthStringData filler64 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Actual:");
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 23);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5576rec t5576rec = new T5576rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5576pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5576rec.t5576Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5576rec.age01);
		fieldNo008.set(t5576rec.annumpc01);
		fieldNo009.set(t5576rec.agemax01);
		fieldNo010.set(t5576rec.minpcnt01);
		fieldNo011.set(t5576rec.maxpcnt01);
		fieldNo012.set(t5576rec.flatpcnt01);
		fieldNo013.set(t5576rec.age02);
		fieldNo014.set(t5576rec.annumpc02);
		fieldNo015.set(t5576rec.agemax02);
		fieldNo016.set(t5576rec.minpcnt02);
		fieldNo017.set(t5576rec.maxpcnt02);
		fieldNo018.set(t5576rec.flatpcnt02);
		fieldNo019.set(t5576rec.age03);
		fieldNo020.set(t5576rec.annumpc03);
		fieldNo021.set(t5576rec.agemax03);
		fieldNo022.set(t5576rec.minpcnt03);
		fieldNo023.set(t5576rec.maxpcnt03);
		fieldNo024.set(t5576rec.flatpcnt03);
		fieldNo025.set(t5576rec.age04);
		fieldNo026.set(t5576rec.annumpc04);
		fieldNo027.set(t5576rec.agemax04);
		fieldNo028.set(t5576rec.minpcnt04);
		fieldNo029.set(t5576rec.maxpcnt04);
		fieldNo030.set(t5576rec.flatpcnt04);
		fieldNo031.set(t5576rec.age05);
		fieldNo032.set(t5576rec.annumpc05);
		fieldNo033.set(t5576rec.agemax05);
		fieldNo034.set(t5576rec.minpcnt05);
		fieldNo035.set(t5576rec.maxpcnt05);
		fieldNo036.set(t5576rec.flatpcnt05);
		fieldNo037.set(t5576rec.age06);
		fieldNo038.set(t5576rec.annumpc06);
		fieldNo039.set(t5576rec.agemax06);
		fieldNo040.set(t5576rec.minpcnt06);
		fieldNo041.set(t5576rec.maxpcnt06);
		fieldNo042.set(t5576rec.flatpcnt06);
		fieldNo043.set(t5576rec.age07);
		fieldNo044.set(t5576rec.annumpc07);
		fieldNo045.set(t5576rec.agemax07);
		fieldNo046.set(t5576rec.minpcnt07);
		fieldNo047.set(t5576rec.maxpcnt07);
		fieldNo048.set(t5576rec.flatpcnt07);
		fieldNo049.set(t5576rec.age08);
		fieldNo050.set(t5576rec.annumpc08);
		fieldNo051.set(t5576rec.agemax08);
		fieldNo052.set(t5576rec.minpcnt08);
		fieldNo053.set(t5576rec.maxpcnt08);
		fieldNo054.set(t5576rec.flatpcnt08);
		fieldNo055.set(t5576rec.highyr);
		fieldNo056.set(t5576rec.highmth);
		fieldNo057.set(t5576rec.lowyr);
		fieldNo058.set(t5576rec.lowmth);
		fieldNo059.set(t5576rec.actualday);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
