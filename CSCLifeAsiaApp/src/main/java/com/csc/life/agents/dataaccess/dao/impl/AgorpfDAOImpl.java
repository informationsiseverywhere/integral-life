/******************************************************************************
 * File Name 		: AgorpfDAOImpl.java
 * Author			: smalchi2
 * Creation Date	: 12 January 2017
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for AGORPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.life.agents.dataaccess.dao.AgorpfDAO;
import com.csc.life.agents.dataaccess.model.Agorpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgorpfDAOImpl extends BaseDAOImpl<Agorpf> implements AgorpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AgorpfDAOImpl.class);

	public List<Agorpf> searchAgorpfRecord(Agorpf agorpf) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("AGNTCOY, AGNTNUM, REPORTAG, AGTYPE01, AGTYPE02, ");
		sqlSelect.append("EFFDATE01, EFFDATE02, DESC_T, USRPRF, JOBNM, ");
		sqlSelect.append("DATIME ");
		sqlSelect.append("FROM AGORPF WHERE AGNTCOY = ? AND AGNTNUM = ? ");
		sqlSelect.append("ORDER BY AGNTCOY ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psAgorpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlAgorpfRs = null;
		List<Agorpf> outputList = new ArrayList<Agorpf>();

		try {

			psAgorpfSelect.setString(1, agorpf.getAgntcoy());
			psAgorpfSelect.setString(2, agorpf.getAgntnum());
			

			sqlAgorpfRs = executeQuery(psAgorpfSelect);

			while (sqlAgorpfRs.next()) {

				agorpf = new Agorpf();

				agorpf.setUniqueNumber(sqlAgorpfRs.getInt("UNIQUE_NUMBER"));
				agorpf.setAgntcoy(sqlAgorpfRs.getString("AGNTCOY"));
				agorpf.setAgntnum(sqlAgorpfRs.getString("AGNTNUM"));
				agorpf.setReportag(sqlAgorpfRs.getString("REPORTAG"));
				agorpf.setAgtype01(sqlAgorpfRs.getString("AGTYPE01"));
				agorpf.setAgtype02(sqlAgorpfRs.getString("AGTYPE02"));
				agorpf.setEffdate01(sqlAgorpfRs.getInt("EFFDATE01"));
				agorpf.setEffdate02(sqlAgorpfRs.getInt("EFFDATE02"));
				agorpf.setDescT(sqlAgorpfRs.getString("DESC_T"));
				agorpf.setUsrprf(sqlAgorpfRs.getString("USRPRF"));
				agorpf.setJobnm(sqlAgorpfRs.getString("JOBNM"));
				agorpf.setDatime(sqlAgorpfRs.getDate("DATIME"));

				outputList.add(agorpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgorpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAgorpfSelect, sqlAgorpfRs);
		}

		return outputList;
	}
	
	public List<Agorpf> searchAllAgorpfRecurRecord(Agorpf agorpf){
		List<Agorpf> list = searchAgorpfRecord(agorpf);
		if(list!=null && !list.isEmpty()) {
			return searchAgorpfRecurRecord(list);
		}
		return list;
	}
	
	public List<Agorpf> searchAgorpfRecurRecord(List<Agorpf> list){
		Agorpf agorpfCondition;
			for(Agorpf agor : list) {
				if(agor.getReportag()!=null && !agor.getReportag().trim().equals("")) {
					agorpfCondition = new Agorpf(agor);
					agorpfCondition.setAgntnum(agor.getReportag());
					List<Agorpf> templist = searchAgorpfRecord(agorpfCondition);
					if(templist != null && !templist.isEmpty()) {
						 list.addAll(searchAgorpfRecurRecord(templist));
						 return list;
					}else {
						return list;
					}
				}
			}
		return list;
	}
	

	
}
