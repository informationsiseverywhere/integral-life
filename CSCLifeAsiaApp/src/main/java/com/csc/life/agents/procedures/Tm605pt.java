/*
 * File: Tm605pt.java
 * Date: 30 August 2009 2:38:33
 * Author: Quipoz Limited
 * 
 * Class transformed from TM605PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Tm605rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TM605.
*
*
*****************************************************************
* </pre>
*/
public class Tm605pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Agent Movement Rules                     SM605");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(55);
	private FixedLengthStringData filler9 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Valid Promotion Codes");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 28);
	private FixedLengthStringData filler10 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 33);
	private FixedLengthStringData filler11 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 38);
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 43);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 48);
	private FixedLengthStringData filler14 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 53);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler15 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Parallel ORC applicable");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler16 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 29, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler17 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 38);
	private FixedLengthStringData filler18 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 43);
	private FixedLengthStringData filler19 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 48);
	private FixedLengthStringData filler20 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 53);
	private FixedLengthStringData filler21 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine005, 54, FILLER).init("    (Yes, No, Optional)");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(55);
	private FixedLengthStringData filler22 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Valid Demotion codes");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 28);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 38);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 43);
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 48);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 53);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(55);
	private FixedLengthStringData filler28 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Valid Transfer Codes");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 28);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 38);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 40, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 48);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 53);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(50);
	private FixedLengthStringData filler34 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine008, 0, FILLER).init("    Year                      FYP Production Level");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(76);
	private FixedLengthStringData filler35 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(60).isAPartOf(wsaaPrtLine009, 16, FILLER).init("For Current Level        For Promotion      For Parallel ORC");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(76);
	private FixedLengthStringData filler37 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 5).setPattern("ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 58).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(76);
	private FixedLengthStringData filler41 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 5).setPattern("ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 58).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(76);
	private FixedLengthStringData filler45 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 5).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 58).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(76);
	private FixedLengthStringData filler49 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 5).setPattern("ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 16).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 58).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(76);
	private FixedLengthStringData filler53 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 5).setPattern("ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 7, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 16).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 34, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZZZZZZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 58).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(28);
	private FixedLengthStringData filler57 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tm605rec tm605rec = new Tm605rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tm605pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tm605rec.tm605Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tm605rec.mlagttyp01);
		fieldNo008.set(tm605rec.mlagttyp02);
		fieldNo009.set(tm605rec.mlagttyp03);
		fieldNo010.set(tm605rec.mlagttyp04);
		fieldNo011.set(tm605rec.mlagttyp05);
		fieldNo012.set(tm605rec.mlagttyp06);
		fieldNo013.set(tm605rec.mlprcind01);
		fieldNo014.set(tm605rec.mlprcind02);
		fieldNo015.set(tm605rec.mlprcind03);
		fieldNo016.set(tm605rec.mlprcind04);
		fieldNo017.set(tm605rec.mlprcind05);
		fieldNo018.set(tm605rec.mlprcind06);
		fieldNo019.set(tm605rec.mlagttyp07);
		fieldNo020.set(tm605rec.mlagttyp08);
		fieldNo021.set(tm605rec.mlagttyp09);
		fieldNo022.set(tm605rec.mlagttyp10);
		fieldNo023.set(tm605rec.mlagttyp11);
		fieldNo024.set(tm605rec.mlagttyp12);
		fieldNo025.set(tm605rec.mlagttyp13);
		fieldNo026.set(tm605rec.mlagttyp14);
		fieldNo027.set(tm605rec.mlagttyp15);
		fieldNo028.set(tm605rec.mlagttyp16);
		fieldNo029.set(tm605rec.mlagttyp17);
		fieldNo030.set(tm605rec.mlagttyp18);
		fieldNo032.set(tm605rec.mlperpp01);
		fieldNo031.set(tm605rec.toYear01);
		fieldNo033.set(tm605rec.mlagtprd01);
		fieldNo034.set(tm605rec.mlgrppp01);
		fieldNo035.set(tm605rec.toYear02);
		fieldNo036.set(tm605rec.mlperpp02);
		fieldNo037.set(tm605rec.mlagtprd02);
		fieldNo038.set(tm605rec.mlgrppp02);
		fieldNo039.set(tm605rec.toYear03);
		fieldNo040.set(tm605rec.mlperpp03);
		fieldNo041.set(tm605rec.mlagtprd03);
		fieldNo042.set(tm605rec.mlgrppp03);
		fieldNo043.set(tm605rec.toYear04);
		fieldNo044.set(tm605rec.mlperpp04);
		fieldNo045.set(tm605rec.mlagtprd04);
		fieldNo046.set(tm605rec.mlgrppp04);
		fieldNo047.set(tm605rec.toYear05);
		fieldNo048.set(tm605rec.mlperpp05);
		fieldNo049.set(tm605rec.mlagtprd05);
		fieldNo050.set(tm605rec.mlgrppp05);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
