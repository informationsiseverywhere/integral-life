/*
 * File: Pm504.java
 * Date: 30 August 2009 1:12:53
 * Author: Quipoz Limited
 *
 * Class transformed from PM504.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.printing.dataaccess.MaguTableDAM;
import com.csc.life.agents.screens.Sm504ScreenVars;
import com.csc.smart.dataaccess.UsrdTableDAM;
import com.csc.smart.procedures.Wsspio;
import com.csc.smart.recordstructures.Wssprec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
*****************************************************************
* </pre>
*/
public class Pm504 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM504");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaFsuco = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgntname = new FixedLengthStringData(47);
	private String f188 = "F188";
		/* FORMATS */
	private String usrdrec = "USRDREC";
	private String magurec = "MAGUREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Agent Production User Access*/
	private MaguTableDAM maguIO = new MaguTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
		/*User Details Logical - Validflag '1's on*/
	private UsrdTableDAM usrdIO = new UsrdTableDAM();
	private Wssprec wssprec = new Wssprec();
	private Sm504ScreenVars sv = ScreenProgram.getScreenVars( Sm504ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		preExit,
		exit3090
	}

	public Pm504() {
		super();
		screenVars = sv;
		new ScreenModel("Sm504", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaCompany.set(wsspcomn.company);
		wsaaFsuco.set(wsspcomn.fsuco);
		wsaaAgntnum.set(SPACES);
		usrdIO.setDataKey(SPACES);
		usrdIO.setFunction(varcom.retrv);
		usrdIO.setFormat(usrdrec);
		SmartFileCode.execute(appVars, usrdIO);
		if (isNE(usrdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(usrdIO.getParams());
			fatalError600();
		}
		sv.userid.set(usrdIO.getUserid());
		setCompany1100();
		maguIO.setCompany(usrdIO.getCompany());
		maguIO.setUserid(usrdIO.getUserid());
		maguIO.setFormat(magurec);
		maguIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, maguIO);
		if (isNE(maguIO.getStatuz(),varcom.oK)
		&& isNE(maguIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(maguIO.getStatuz());
			syserrrec.params.set(maguIO.getParams());
			fatalError600();
		}
		if (isEQ(maguIO.getStatuz(),varcom.oK)) {
			sv.agntsel01.set(maguIO.getAgntnum01());
			sv.agntsel02.set(maguIO.getAgntnum02());
			sv.agntsel03.set(maguIO.getAgntnum03());
		}
		else {
			sv.agntsel01.set(SPACES);
			sv.agntsel02.set(SPACES);
			sv.agntsel03.set(SPACES);
		}
		if (isNE(sv.agntsel01,SPACES)) {
			wsaaAgntnum.set(sv.agntsel01);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname01.set(wsaaAgntname);
			}
			else {
				sv.agtname01.fill("?");
			}
		}
		if (isNE(sv.agntsel02,SPACES)) {
			wsaaAgntnum.set(sv.agntsel02);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname02.set(wsaaAgntname);
			}
			else {
				sv.agtname02.fill("?");
			}
		}
		if (isNE(sv.agntsel03,SPACES)) {
			wsaaAgntnum.set(sv.agntsel03);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname03.set(wsaaAgntname);
			}
			else {
				sv.agtname03.fill("?");
			}
		}
	}

protected void setCompany1100()
	{
		start1100();
	}

protected void start1100()
	{
		wssprec.function.set(varcom.readr);
		callProgram(Wsspio.class, wssprec.params);
		if (isNE(wssprec.statuz,varcom.oK)) {
			syserrrec.params.set(wssprec.params);
			fatalError600();
		}
		wsspcomn.company.set(usrdIO.getCompany());
		scrnparams.company.set(usrdIO.getCompany());
		wssprec.wsspcomn.setSub1String(27, 1, usrdIO.getCompany());
		wssprec.wsspcomn.setSub1String(237, 1, usrdIO.getCompany());
		wssprec.function.set(varcom.rewrt);
		callProgram(Wsspio.class, wssprec.params);
		if (isNE(wssprec.statuz,varcom.oK)) {
			syserrrec.params.set(wssprec.params);
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set(SPACES);
			sv.agtname01.set(SPACES);
			sv.agtname02.set(SPACES);
			sv.agtname03.set(SPACES);
		}
	}

protected void validate2020()
	{
		if (isNE(sv.agntsel01,SPACES)) {
			wsaaAgntnum.set(sv.agntsel01);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname01.set(wsaaAgntname);
			}
			else {
				sv.agntsel01Err.set(f188);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (isNE(sv.agntsel02,SPACES)) {
			wsaaAgntnum.set(sv.agntsel02);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname02.set(wsaaAgntname);
			}
			else {
				sv.agntsel02Err.set(f188);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (isNE(sv.agntsel03,SPACES)) {
			wsaaAgntnum.set(sv.agntsel03);
			validateAgent2100();
			if (isEQ(agntIO.getStatuz(),varcom.oK)) {
				getAgentName2200();
				sv.agtname03.set(wsaaAgntname);
			}
			else {
				sv.agntsel03Err.set(f188);
				wsspcomn.edterror.set(SPACES);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAgent2100()
	{
		/*START*/
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(wsaaAgntnum);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getAgentName2200()
	{
		start2200();
	}

protected void start2200()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaAgntname.set(namadrsrec.name);
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		maguIO.setCompany(usrdIO.getCompany());
		maguIO.setUserid(usrdIO.getUserid());
		maguIO.setFormat(magurec);
		maguIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, maguIO);
		if (isNE(maguIO.getStatuz(),varcom.oK)
		&& isNE(maguIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(maguIO.getStatuz());
			syserrrec.params.set(maguIO.getParams());
			fatalError600();
		}
		if (isEQ(maguIO.getStatuz(),varcom.oK)) {
			maguIO.setFunction(varcom.rewrt);
			if (isEQ(sv.agntsel01,SPACES)
			&& isEQ(sv.agntsel02,SPACES)
			&& isEQ(sv.agntsel03,SPACES)) {
				maguIO.setFunction(varcom.delet);
			}
		}
		else {
			maguIO.setFunction(varcom.updat);
			if (isEQ(sv.agntsel01,SPACES)
			&& isEQ(sv.agntsel02,SPACES)
			&& isEQ(sv.agntsel03,SPACES)) {
				goTo(GotoLabel.exit3090);
			}
		}
		maguIO.setAgntnum01(sv.agntsel01);
		maguIO.setAgntnum02(sv.agntsel02);
		maguIO.setAgntnum03(sv.agntsel03);
		SmartFileCode.execute(appVars, maguIO);
		if (isNE(maguIO.getStatuz(),varcom.oK)
		&& isNE(maguIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(maguIO.getStatuz());
			syserrrec.params.set(maguIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		scrnparams.function.set("HIDEW");
		wsspcomn.programPtr.add(1);
		resetCompany5000();
		/*EXIT*/
	}

protected void resetCompany5000()
	{
		start5000();
	}

protected void start5000()
	{
		wssprec.function.set(varcom.readr);
		callProgram(Wsspio.class, wssprec.params);
		if (isNE(wssprec.statuz,varcom.oK)) {
			syserrrec.params.set(wssprec.params);
			fatalError600();
		}
		wssprec.wsspcomn.setSub1String(27, 1, wsaaCompany);
		wsspcomn.company.set(wsaaCompany);
		wssprec.wsspcomn.setSub1String(237, 1, wsaaFsuco);
		wsspcomn.fsuco.set(wsaaFsuco);
		wssprec.function.set(varcom.rewrt);
		callProgram(Wsspio.class, wssprec.params);
		if (isNE(wssprec.statuz,varcom.oK)) {
			syserrrec.params.set(wssprec.params);
			fatalError600();
		}
	}
}
