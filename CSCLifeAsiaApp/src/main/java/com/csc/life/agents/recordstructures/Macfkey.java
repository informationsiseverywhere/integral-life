package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:33
 * Description:
 * Copybook name: MACFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfKey = new FixedLengthStringData(64).isAPartOf(macfFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfAgntcoy = new FixedLengthStringData(1).isAPartOf(macfKey, 0);
  	public FixedLengthStringData macfAgntnum = new FixedLengthStringData(8).isAPartOf(macfKey, 1);
  	public PackedDecimalData macfTranno = new PackedDecimalData(5, 0).isAPartOf(macfKey, 9);
  	public FixedLengthStringData macfAgmvty = new FixedLengthStringData(2).isAPartOf(macfKey, 12);
  	public PackedDecimalData macfEffdate = new PackedDecimalData(8, 0).isAPartOf(macfKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(macfKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}