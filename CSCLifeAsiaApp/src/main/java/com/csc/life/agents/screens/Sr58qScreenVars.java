package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sr58qScreenVars extends SmartVarModel{

	public FixedLengthStringData dataArea = new FixedLengthStringData(326);
	public FixedLengthStringData dataFields = new FixedLengthStringData(134).isAPartOf(dataArea, 0);

	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9); 
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55); 
	public ZonedDecimalData anntarprm = DD.anntarprm.copyToZonedDecimal().isAPartOf(dataFields,60); 
	public ZonedDecimalData blprem = DD.blprem.copyToZonedDecimal().isAPartOf(dataFields,77); 
	public ZonedDecimalData premst = DD.premst.copyToZonedDecimal().isAPartOf(dataFields,94); 
	public ZonedDecimalData zlocamt = DD.zlocamt.copyToZonedDecimal().isAPartOf(dataFields,109); 
	public PackedDecimalData znadjperc01 = DD.znadjperc.copy().isAPartOf(dataFields,124); 
	public PackedDecimalData znadjperc02 = DD.znadjperc.copy().isAPartOf(dataFields,129); 


	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 134);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData anntarprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData blpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData premstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData zlocamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData znadjperc01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData znadjperc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);


	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea,182);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] anntarprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] blpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] premstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] zlocamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] znadjperc01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] znadjperc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);


	//SFL PAGE
	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);


	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable Sr58qscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr58qscreenctlWritten = new LongData(0);
	public LongData Sr58qscreensflWritten = new LongData(0);
	public LongData Sr58qscreenWritten = new LongData(0);
	public LongData Sr58qwindowWritten = new LongData(0);
	public LongData Sr58qhideWritten = new LongData(0);
	public LongData Sr58qprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr58qScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(anntarprmOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(blpremOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(premstOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmfrmOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmtoOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(zlocamtOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(znadjperc01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(znadjperc02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});

	screenFields = new BaseData[] { company,item,itmfrm,itmto,longdesc,tabl,anntarprm,blprem,premst,zlocamt,znadjperc01,znadjperc02};
	screenOutFields = new BaseData[][] { companyOut,anntarprmOut,blpremOut,itemOut,longdescOut,premstOut,tablOut,itmfrmOut,itmtoOut,zlocamtOut,znadjperc01Out,znadjperc02Out };
	screenErrFields = new BaseData[] {  itemErr,companyErr,longdescErr,tablErr,itmfrmErr,itmtoErr,anntarprmErr,blpremErr,premstErr,zlocamtErr,znadjperc01Err,znadjperc02Err  };
	screenDateFields = new BaseData[] { itmfrm,itmto  };
	screenDateErrFields = new BaseData[] { itmfrmErr,itmtoErr  };
	screenDateDispFields = new BaseData[] { itmfrmDisp,itmtoDisp   };

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr58qscreen.class;
	protectRecord = Sr58qprotect.class;

	}
}
