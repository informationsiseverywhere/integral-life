package com.csc.life.agents.dataaccess;

import com.csc.fsu.agents.dataaccess.AgntpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgntlagTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:46
 * Class transformed from AGNTLAG.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgntlagTableDAM extends AgntpfTableDAM {

	public AgntlagTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGNTLAG");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "AGNTPFX, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "CLNTPFX, " +
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "AGNTREL, " +
		            "AGTYPE, " +
		            "AGNTBR, " +
		            "REPLVL, " +
		            "REPAGENT01, " +
		            "LIFAGNT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "AGENTREFCODE, "+      //ICIL-12
					"AGENCYMGR, "+		  //ICIL-12
		            "DISTRCTCDE, "+    	  //ICIL-12
		            "BNKINTERNALROLE, "+  //ICIL-12
		            "BANCEINDICATORSEL, "+  //ICIL-12
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               agntpfx,
                               tranid,
                               validflag,
                               clntpfx,
                               clntcoy,
                               clntnum,
                               agntrel,
                               agtype,
                               agntbr,
                               replvl,
                               repagent01,
                               lifagnt,
                               userProfile,
                               jobName,
                               datime,
                               agentrefcode,
                               agencymgr,
                               distrctcode,
                               bnkinternalrole,
                               banceindicatorsel,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(agntcoy.toInternal());
	nonKeyFiller2.setInternal(agntnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(103);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getAgntpfx().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getClntpfx().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getAgntrel().toInternal()
					+ getAgtype().toInternal()
					+ getAgntbr().toInternal()
					+ getReplvl().toInternal()
					+ getRepagent01().toInternal()
					+ getLifagnt().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getAgentrefcode().toInternal() //ICIL-12
					+ getAgencymgr().toInternal() 	//ICIL-12
					+ getDistrctcode().toInternal() //ICIL-12
					+ getBnkinternalrole().toInternal() //ICIL-12
					+ getBanceindicatorsel().toInternal()); //ICIL-12
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, clntpfx);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, agntrel);
			what = ExternalData.chop(what, agtype);
			what = ExternalData.chop(what, agntbr);
			what = ExternalData.chop(what, replvl);
			what = ExternalData.chop(what, repagent01);
			what = ExternalData.chop(what, lifagnt);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, agentrefcode);//ICIL-12
			what = ExternalData.chop(what, agencymgr);	//ICIL-12
			what = ExternalData.chop(what, distrctcode);	//ICIL-12	
			what = ExternalData.chop(what, bnkinternalrole);//ICIL-12
			what = ExternalData.chop(what, banceindicatorsel);//ICIL-12
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getClntpfx() {
		return clntpfx;
	}
	public void setClntpfx(Object what) {
		clntpfx.set(what);
	}	
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}	
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}	
	public FixedLengthStringData getAgntrel() {
		return agntrel;
	}
	public void setAgntrel(Object what) {
		agntrel.set(what);
	}	
	public FixedLengthStringData getAgtype() {
		return agtype;
	}
	public void setAgtype(Object what) {
		agtype.set(what);
	}	
	public FixedLengthStringData getAgntbr() {
		return agntbr;
	}
	public void setAgntbr(Object what) {
		agntbr.set(what);
	}	
	public PackedDecimalData getReplvl() {
		return replvl;
	}
	public void setReplvl(Object what) {
		setReplvl(what, false);
	}
	public void setReplvl(Object what, boolean rounded) {
		if (rounded)
			replvl.setRounded(what);
		else
			replvl.set(what);
	}	
	public FixedLengthStringData getRepagent01() {
		return repagent01;
	}
	public void setRepagent01(Object what) {
		repagent01.set(what);
	}	
	public FixedLengthStringData getLifagnt() {
		return lifagnt;
	}
	public void setLifagnt(Object what) {
		lifagnt.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ICIL-12 start
	public FixedLengthStringData getAgentrefcode() {
		return agentrefcode;
	}
	public void setAgentrefcode(Object what) {
		agentrefcode.set(what);
	}
	public FixedLengthStringData getAgencymgr() {
		return agencymgr;
	}
	public void setAgencymgr(Object what) {
		agencymgr.set(what);
	}
	public FixedLengthStringData getDistrctcode() {
		return distrctcode;
	}
	public void setDistrctcode(Object what) {
		distrctcode.set(what);
	}
	public FixedLengthStringData getBnkinternalrole() {
		return bnkinternalrole;
	}
	public void setBnkinternalrole(Object what) {
		bnkinternalrole.set(what);
	}
	
	public FixedLengthStringData getBanceindicatorsel() {
		return banceindicatorsel;
	}

	public void setBanceindicatorsel(Object what) {
		banceindicatorsel.set(what);
	}
	//ICIL-12 End

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getRepagents() {
		return new FixedLengthStringData(repagent01.toInternal()
);
	}
	public void setRepagents(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getRepagents().getLength()).init(obj);
	
		what = ExternalData.chop(what, repagent01);
	}
	public FixedLengthStringData getRepagent(BaseData indx) {
		return getRepagent(indx.toInt());
	}
	public FixedLengthStringData getRepagent(int indx) {

		switch (indx) {
			case 1 : return repagent01;
			default: return null; // Throw error instead?
		}
	
	}
	public void setRepagent(BaseData indx, Object what) {
		setRepagent(indx.toInt(), what);
	}
	public void setRepagent(int indx, Object what) {

		switch (indx) {
			case 1 : setRepagent01(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		agntpfx.clear();
		tranid.clear();
		validflag.clear();
		clntpfx.clear();
		clntcoy.clear();
		clntnum.clear();
		agntrel.clear();
		agtype.clear();
		agntbr.clear();
		replvl.clear();
		repagent01.clear();
		lifagnt.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		agentrefcode.clear();	//ICIL-12
		agencymgr.clear();		//ICIL-12
		distrctcode.clear();	//ICIL-12
		bnkinternalrole.clear();	//ICIL-12
        banceindicatorsel.clear();	//ICIL-12
	}


}