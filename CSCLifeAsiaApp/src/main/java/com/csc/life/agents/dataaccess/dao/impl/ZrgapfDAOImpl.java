/*
 * File: ZrgapfDAOImpl.java
 * Date: July 22, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZrgapfDAO;
import com.csc.life.agents.dataaccess.model.Zrgapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class ZrgapfDAOImpl  extends BaseDAOImpl<Zrgapf> implements ZrgapfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ZrgapfDAOImpl.class);	
	
		
	public Zrgapf getZrgapfRecord(String Agntcoy, String Agntnum) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM ZRGAPF WHERE AGNTCOY = ? AND AGNTNUM = ?");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Zrgapf zrgapf = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Agntcoy);/* IJTI-1523 */
			ps.setString(2, Agntnum);
			rs = ps.executeQuery();
			while(rs.next()){
				zrgapf = new Zrgapf();
				zrgapf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zrgapf.setAgntcoy(Agntcoy);
				zrgapf.setAgntnum(rs.getString("AGNTNUM").trim());
				zrgapf.setAgntype(rs.getString("AGNTYPE").trim());
				zrgapf.setZrecruit(rs.getString("ZRECRUIT").trim());
				zrgapf.setDatime(rs.getTimestamp("DATIME"));
				zrgapf.setEffdate(rs.getBigDecimal("EFFDATE"));
				zrgapf.setJobName(rs.getString("JOBNM").trim());
				zrgapf.setMlagttyp(rs.getString("MLAGTTYP").trim());
				zrgapf.setPrcent(rs.getBigDecimal("PRCENT"));
				zrgapf.setUserProfile(rs.getString("USRPRF").trim());
				
				
				
			}
		}catch (SQLException e) {
			LOGGER.error("getZrgapfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zrgapf;
	}


	}
