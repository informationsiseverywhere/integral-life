/*
 * File: B5403.java
 * Date: 29 August 2009 21:16:31
 * Author: Quipoz Limited
 *
 * Class transformed from B5403.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.accounting.procedures.Glsubst;
import com.csc.fsu.accounting.recordstructures.Glsubstrec;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.tablestructures.Tr393rec;
import com.csc.fsu.financials.dataaccess.CheqTableDAM;
import com.csc.fsu.financials.dataaccess.PreqTableDAM;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Chkbkcd;
import com.csc.fsu.general.procedures.Glkey;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Chkbkcdrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Glkeyrec;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsu.general.tablestructures.T3688rec;
import com.csc.fsu.general.tablestructures.Tr392rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AcmvagtTableDAM;
import com.csc.life.agents.dataaccess.AgcsTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglfagtTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.TatwTableDAM;
import com.csc.life.agents.tablestructures.T5622rec;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.agents.tablestructures.T6657rec;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS
*
* B5403 - Agent Batch payments.
*
*     The  following  control  totals are maintained within this
*  program:
*
*            01 - Agents - Manual Cheque
*            02 - Pay Amount - Manual Cheque
*            03 - Agents - Computer Cheque
*            04 - Pay Amount - Computer Cheque
*            05 - Agents - Bank Transfer
*            06 - Pay Amount - Bank Transfer
*            07 - Requistions created
*            08 - Requistions amount
*            09 - Extract records written
*            10 - Extract amount written
*            15 - Total withholding tax amount.
*
*     Also note that control totals 01 and 02 are now not used
*  due to the direct credit processing. Take this into account
*  when reading the rest of the remark section.
*
*
*     The  following  accounts  are set up in T5645 to cater for
*  General Ledger postings:
*
*            Line           Account                 Currency
*            ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*            01   LA**  +   Commission pay   out    agent
*
*1.) Initialise
*
*     As the  program  can  produce  financial transactions it is
*  'batched'.  The  transaction key used in the overall batch key
*  is taken  from  the  jobstream  parameters.  Set  up  batching
*  parameters and  call  subroutine  'BATCDOR'  to open the batch
*  file.
*
*     Check for restart recovery in the usual way.
*
*2.) Check for agent payment.
*
*     Sequentially  read  the life agents file (AGLFPF) using the
*  logical view  AGLFAGT. For each current valid agent we need to
*  check  whether  the  agent  receives  payments  in a specified
*  currency  or  not  and  if  the  outstanding  transactions are
*  greater than the minimum payment amount.
*
*     Once all  records  have  been processed perform section 5.)
*  Update batch header record and exit program.
*
*
*     If the currency field on the agent file is spaces:
*
*          Read each of  the  records on AGPYPF using the logical
*       view AGPYAGT for  this  agent  and add 1 to control total
*       no.1 for each record found.  For each set of transactions
*       within a currency for the agent accumulate the sum of the
*       original amount fields.
*
*          If  the  sum  accumulated  for  a  currency  > minimum
*       statement amount on the agent file perform Pay Agent ELSE
*       read transactions for next currency.
*
*
*     If the currency field on the agent file is not spaces:
*
*          Read all  of  the  records on AGPYPF using the logical
*       view  AGPYAGT  for  this  agent/currency.  Accumulate the
*       amount of  the  transactions in working storage and add 1
*       to control  total  no.1  for  each  record  found. If the
*       transaction currency is different from the agent currency
*       a conversion will  need to be done calling 'XCVRT' with a
*       function of 'SELL'. The accumulated amount will be in the
*       agents currency.
*
*          If  the  sum  accumulated  for  the currency > minimum
*       statement amount on the agent file perform Pay Agent ELSE
*       read next set of transactions for next agent.
*
*3.) Pay Agent.
*
*     We  know  the  accumulated  sum  from  the previous section
*  exceeds the minimum statement amount if we are here. Therefore
*  we  need  to create a requisition record for FSU processing to
*  produce  output Media for the statement payment. The media may
*  be in different forms e.g.:
*
*          Bank transfer
*          Manual cheque
*          Automatic cheque
*
*     Create a requisition record (CHEQPF) using the logical view
*  REQN setting up the fields as follows:
*
*          - initialise record
*          - if factoring house on the AGLFPF record is not
*            spaces move PRFX-BANK-TAPE to cheque prefix and add
*            1 to control total no.4. Else move PRFX-SEMI-CHEQ to
*            the field and add 1 to control total no.6. (These
*            fields are from the FSU copybook FSUPFXCPY).
*          - set up the requisition key in working storage and
*            move to the requisition data key. The key is made up
*            as follows:
*             Prefix - (PRFX-REQN from FSUPFXCPY)
*             Company - from batch parameters
*             Bankcode - from T6657 using currency as the key
*              (currency from AGLFPF if present else use
*               currency set up in AGPYPF record).
*             Requisition number - automatically allocated by
*             calling 'ALOCNO' with the following params:
*               - function 'NEXT '
*               - prefix = PRFX-REQN
*               - genkey = bankcode
*               - company = batch company
*          - cheque company as batch company
*          - bank code as in T6657 for this currency
*          - cheq number as returned from 'ALOCNO' previously
*          - payee prefix as PRFX-CLNT
*          - payee company and client from AGLFPF
*          - user and auth user as VRCM-USER
*          - factor house, bank key and bank account from AGLFPF
*          - payment date as effective date from parameters
*          - tran date and auth date as today
*          - tran time and auth time as system time
*          - process company as batch company
*          - process bankcode as from T6657 key = currency
*          - process branch from AGLFPF
*          - process indicator as 'AU'
*          - if factoring house on AGLFPF is spaces move
*            RQNN-MCH-CHQ to requisition type else move
*            RQNN-BANK-TAPE to this field. (These fields come
*            from FSU copybook REQNTYPCPY).
*          - pres flag as 'N'
*          - remit type as 7
*          - pay amount as the amount accumulated in this
*            currency in the previous section. Add amount to
*            control total no.5 if this is a bank tape
*            requisition else add amt to control total no.7.
*
*     We  now  need to create a receipt transaction header record
*  for  FSU processing which outputs the Agent payment media. For
*  this  we  will  need  to  call  'LIFRTRN'  with  the following
*  parameters.
*
*          - Function - PSTW
*          - Batch key - as set up in this program
*          - Document number - requisition number
*          - Transaction Sequence number - zero (for header)
*          - Journal Sequence number - 0
*          - Sub-account code and type, GL map, GL sign and
*            control total number - from T3688 (Bank Account
*            details).
*          - Company codes (sub-ledger and GL) - batch company
*          - Subsidiary account - requisition number
*          - Original currency code - currency from AGLFPF if
*            present else the AGPYPF transaction currency.
*          - Original currency amount - accumulated amount from
*            requisition record
*          - Accounting currency code - blank (it will be looked
*            up by the subroutine)
*          - Accounting currency amount - zero (it will be
*            calculated by the subroutine)
*          - Exchange rate - zero (it will be calculated by the
*            subroutine)
*          - Transaction reference - contract transaction number
*          - Transaction description - parameter 1 from jobstream
*            details
*          - Posting month and year - blank (they will be
*            defaulted)
*          - Effective date - parameter effective date
*          - Reconciliation amount - zero
*          - Reconciled date - Max-Date
*          - Transaction ID - from parameters
*          - Substitution code 1 - Requisition type
*
*     After  creating  the  requisition record and receipt header
*  record  we  now  need  to  do  the  relevant  postings for the
*  individual transactions (dissections).
*
*4.) Create dissection postings.
*
*     Re-read  the  agent  payment extract file (AGPYPF) with the
*  logical  view  AGPYAGT  for  this  agent.  Processing  will be
*  dependent on whether the agent has a specific currency held on
*  AGLFPF:
*
*     If  the AGLFPF currency is spaces we need to re-read all of
*  the  AGPYPF  records  for  this company/agent. Else we need to
*  re-read    all    of    the    AGPYPF    records    for   this
*  company/agent/currency   in   which   the   transactions  were
*  previously accumulated. In  both cases we will use the logical
*  view AGPYAGT and process as follows:
*
* -- Update existing ACMVPF record.
*
*     For  each  AGPYPF record read access the accounts movements
*  file  (ACMVPF)  with  the logical view ACMVAGT. Set up the key
*  details from AGPYPF and update the record as follows:
*
*          - move parm run date to reconciliation date
*          - move ACMVPF original amount field to reconciliation
*            amount
*
*     Re write the  record back to the database. Add 1 to control
*  total no.2 and add amount to control total no.3.
*
*--- Post the amount to the agent commission account.
*
*     A  posting  needs  to be made against the agents commission
*  account  using the subroutine 'LIFACMV'. All parameters can be
*  taken from the AGPYPF record together with the following:
*
*          Function - PSTW
*          Batch key - as set up in this program
*          Sub-account code and type, GL map, GL sign and control
*            total number - same as AGPYPF.
*          Reconciliation amount = original currency amount * -1
*          Original amount       = original currency amount * -1
*          Accounting amount     = accounting amount *-1.
*          Reconciled date - parm run date
*          Posting year/month - from PARM-POSTYEAR/MONTH
*          Transaction ID - from parameters
*
*--- Currency conversion.
*
*     Now  we  need  to create a receipt transaction line for FSU
*  processing.  Firstly  a  check has to be made for any currency
*  conversion  which may have to take place if the agent currency
*  is not the same as the transaction currency.
*
*     If the  currency  on  the  AGPYPF record is the same as the
*  currency  on  the  agent  record (AGLFPF) go to Create receipt
*  dissection line.
*
*     Call  the  subroutine 'XCVRT' with a function of 'SELL' and
*  and  a  conversion  date  of today. The in currency and amount
*  will be taken from the AGPYPF record, the out currency will be
*  from AGLFPF.
*
*     The fund conversion amount must be updated in the ledger if
*  currency  conversion  took  place, therefore update the ledger
*  with the appropriate details.
*
*     Call 'LIFACMV' using the following parameters:
*
*            Function - PSTW
*            Batch Key - as set up in this program
*            Document Number - Contract number
*            Transaction seq number - AGPYPF transaction number
*            Journal Sequence Number - 0
*            Company Codes (Sub ledger and GL) - batch company
*            Subsidiary Account - agent key
*            Sub Account Code and Type - from T5645 line 03
*            GL Map,  GL  Sign  and  control total no - also from
*                 T5645 (line 03)
*            Original Currency code - AGLFPF currency
*            Original Currency amount = converted amount
*            Transaction  description  -  from T1688 (batch trans
*                 code)
*            Transaction reference - requisition number
*            Accounting currency - leave blank
*            Exchange rate - leave blank
*            Posting month/year - leave blank
*            Effective date - parm run date
*            Reconciliation amount - zero
*            Reconciliation date - VRCM max date
*            Substitution code 1 - requisition type
*
*     A  second  exchange  posting  will also be done through the
*  subroutine   'LIFACMV'   the  parameters  as  above  with  the
*  following changes:
*
*            Sub Account Code and Type - from T5645 line 04
*            GL Map,  GL  Sign  and  control total no - also from
*                 T5645 (line 04)
*            Original Currency code - AGPYPF currency
*            Original Currency amount = amount converted from
*
*--- Create receipt dissection line.
*
*     A  receipt dissection line has to be written to the receipt
*  file  (RTRNPF)  for the account transaction amounts. This will
*  be done  by  a call to the subroutine 'LIFRTRN' using the same
*  parameters  as  the receipt header previously created with the
*  following changes:
*
*          - Transaction Sequence number - increment by one
*          - Sub-account code and type, GL map, GL sign and
*            control total number - from T5645 line 01
*          - Original currency amount - from AGPYPF (use
*            converted amount if applicable).
*
*     Add 1 to control total no.8 and add amount to control total
*  no.9.
*
*5.) Update batch header record.
*
*     Update  the  batch  header  by  calling  'BATCUP'  with  a
*  function of WRITS and the following parameters:
*
*            - transaction count = 1
*            - all other amounts zero
*             - batch key as set up earlier in the program.
*****************************************************************
*              F AMENDMT  HISTORY                               *
*****************************************************************
* DATE.....   BY..   AMENDMENT...............  NUMBER
*
* 28/09/89    T.S    Fixed few program errors.                NNN
* 14/12/89    T.S    ADDED EXTRA KEYS ON ACMVAGR FILE.        001
* 22/01/90    T.S    Pass over the function 'CVRT' to the     002
*                    subroutine XCVRT instead of the function
*                    'SELL'.
* 20/04/90    LI     Check for change of company on AGLF ie.  003
*                    only process agents for the FSU company  003
* 23/04/90    B.C    Get the agent client number onto the     004
*                    cheque requisition file. (If the pay
*                    client equals spaces.)  (NM CHANGE)
* 23/04/90    B.C    Correct new version from base to write   005
*                    oppposite ACMV. Correct updating of
*                    original ACMV's RCAMT and FRCDATE.
*                    (NM CHANGE).
* 23/04/90    B.C    Get TRANDESC for B618 ACMV going to be   006
*                    written. (NM CHANGE)
* 24/04/90    B.C    Correct error calling.                   007
*
* 03.08.90    F.M.   Life Agents reside in the sign-on company008<008>
*                    not the FSU company. SDF 751
*
* 25/04/1991  G.N.   Use the prefix 'AG' when setting up the  009
*                    key for AGNTAGT as this still uses the
*                    field in the key.
*
* 13/06/91    J.K.   (SDF - 1484)                             010
*                    - Use Run Parameter 1 instead of the
*                      Batch Job Access Code.
*
* 12/12/91    JOS    - Program changed to use CHEQ instead of 011
*                      REQN.
*                    - Program now writes PREQ records instead
*                      of RTRNs. ADDRTRNREC has been commented
*                      out.
*                    - CLNTAGT was not being used, so it has
*                      been commented out.
*
* 09/03/92    M.P.   Addition of user and terminal id fields. 012
*                    Change required for ACMV reports.
*                    sdf 3056
* 19/02/92    G.D.   When calling ALOCNO there is no need   . 013
*                    now to set genkey as the table is read . 013
*                    bp the prefix only (t3642)             . 013
*                    Also (see sdf 1484)                    . 013
*                    Commissiom movements should post a credit
*                    for the cheque. Another occourence has
*                    has been set up in t5645 (fourth ) to cater
*                    for this.
* 18/06/92    P.H.   AQR 4276                                 014
*                    Due to number of changes and fixes to
*                    this program, the contot totals are now
*                    a bit mixed up. Fix to tidy them up.
* 14/05/93    P.B.   AQR 4276 (Cont)                          014
*                    Changes to both table T1671 and to this
*                    program to finalise the Control Totals
*                    tidy-up! (AQR 3685). Also set up sensible
*                    Reconciliation Totals in T1672 (AQR 2605).
*                    Also, instead of checking for factoring
*                    house being spaces to see if the agent
*                    has bank details, check agent's pay method
*                    and type to be more accurate about how to
*                    pay him. i.e. apply AGLF-PAYMTH to T5690
*                    and payment-type to T3672 and check inds.
*                    Extra data screen on T5690(AQR 3230 & 4394).
*                    Also tidy up CHEQ and PREQ records: some
*                    details were missing.
* 21/05/93    P.B.   AQR 4357                                 015
*                    Zeroise new field PREQ-CNTTOT.
* 06/01/94    DHP.   AQR 4743                                 016
*                    Problem : Agents payments job is
*                    requesting a payment to Agent/Client, it
*                    should pay the Payee/Client if his/her
*                    details are entered.
* 13/01/94    J.B.   AQR 4741                                 017
*                    The posting for commission payable (line
*                    4 from T5645) should include a check not
*                    to post if the amount is less than 1,
*                    in line with the other postings.
*                    The total of amounts not posted due to
*                    their value will be maintained as control
*                    total no.13, with the total value of these
*                    amounts stored in total no.14.
*                    Remove EXIT PROGRAM statement; this was
*                    preventing the control totals from being
*                    updated properly.
*                    Update source and object computer.
*
* 05/08/94    L.P.   AQR 5360.                                018
*                    On a restart, the AGLFAGT-FUNCTION was not
*                    set to NEXTR after the first record was read
*
* 03/11/94    W.O.   AQR 5517.                                019
*                    Agent number increased to 8 characters.
*
* 21/02/95    S.V.   Remarks updated.
*
* DD/MM/YY    X.X.   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  NNN
*
*****************************************************************
*
* </pre>
*/
public class B5403 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5403");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCurr = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaLastOrigCcy = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaLedgerCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaTransDesc680 = new FixedLengthStringData(30).init(SPACES);

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRunparm1TranCode = new FixedLengthStringData(4).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaRunparm1Rest = new FixedLengthStringData(6).isAPartOf(wsaaRunparm1, 4);
	private String wsaaLongconfname = "";

	private FixedLengthStringData wsaaGenlkey = new FixedLengthStringData(20);
	private FixedLengthStringData wsaaGenlpfx = new FixedLengthStringData(2).isAPartOf(wsaaGenlkey, 0);
	private FixedLengthStringData wsaaGenlcoy = new FixedLengthStringData(1).isAPartOf(wsaaGenlkey, 2);
	private FixedLengthStringData wsaaGenlcur = new FixedLengthStringData(3).isAPartOf(wsaaGenlkey, 3);
	private FixedLengthStringData wsaaGlcode = new FixedLengthStringData(14).isAPartOf(wsaaGenlkey, 6);
		/* TABLES */
	private String t6657 = "T6657";
	private String t5690 = "T5690";
	private String t5645 = "T5645";
	private String t3688 = "T3688";
	private String t3672 = "T3672";
	private String t3629 = "T3629";
	private String t1688 = "T1688";
	private String t5622 = "T5622";
	private String t3583 = "T3583";
	private String tr393 = "TR393";
	private String tr392 = "TR392";

	private FixedLengthStringData wsaaEnough = new FixedLengthStringData(1).init("N");
	private Validator enoughMoneyEarned = new Validator(wsaaEnough, "Y");

	private FixedLengthStringData wsaaStoredAgnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaStoredAgent = new FixedLengthStringData(8).isAPartOf(wsaaStoredAgnt, 0);

	private FixedLengthStringData wsaaBankTran = new FixedLengthStringData(1).init("N");
	private Validator bankTran = new Validator(wsaaBankTran, "Y");

	private FixedLengthStringData wsaaAutoCheq = new FixedLengthStringData(1).init("N");
	private Validator autoCheq = new Validator(wsaaAutoCheq, "Y");

	private FixedLengthStringData wsaaManCheq = new FixedLengthStringData(1).init("N");
	private Validator manCheq = new Validator(wsaaManCheq, "Y");

	private FixedLengthStringData wsaaOnAcct = new FixedLengthStringData(1).init("N");
	private Validator onAcct = new Validator(wsaaOnAcct, "Y");

	private FixedLengthStringData wsaaPaymthMissing = new FixedLengthStringData(1).init("N");
	private Validator paymthMissing = new Validator(wsaaPaymthMissing, "Y");
	private String itemrec = "ITEMREC   ";
	private String cheqrec = "CHEQREC   ";
	private String preqrec = "PREQREC   ";
	private String acmvrec = "ACMVREC";
	private String tatwrec = "TATWREC";
	private String agcsrec = "AGCSREC";
	private FixedLengthStringData wsaaStoredAglfcurr = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaStoredAgpycurr = new FixedLengthStringData(3).init(SPACES);
	private PackedDecimalData wsaaTotalAgntComm = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotOrigAmt = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaOrigamt = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotalWhtax = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalPrmtlr = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaDeductn = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalCrcard = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalAgadvn = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaDissAmount = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalAdvance = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTotalDeduct = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaLifaTotOrig = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaLifaTotAcct = new PackedDecimalData(15, 2).init(ZERO);

	private FixedLengthStringData wsaaChdrCurrArray = new FixedLengthStringData(1080);
	private FixedLengthStringData[] wsaaChdrCurr = FLSArrayPartOfStructure(90, 12, wsaaChdrCurrArray, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(9, wsaaChdrCurr, 0);
	private FixedLengthStringData[] wsaaCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaChdrCurr, 9);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private PackedDecimalData wsaaTotalColltr = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaCurColltr = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaNewColltr = new PackedDecimalData(15, 2).init(ZERO);
	private PackedDecimalData wsaaTdcchrg = new PackedDecimalData(14, 2).init(ZERO);
	private FixedLengthStringData wsaaZchrgcde = new FixedLengthStringData(6).init(SPACES);

	private FixedLengthStringData wsaaCommTotalArray = new FixedLengthStringData(540);
	private FixedLengthStringData[] wsaaCommTotal = FLSArrayPartOfStructure(20, 27, wsaaCommTotalArray, 0);
	private FixedLengthStringData[] wsaaCommSacscode = FLSDArrayPartOfArrayStructure(2, wsaaCommTotal, 0);
	private FixedLengthStringData[] wsaaCommSacstyp = FLSDArrayPartOfArrayStructure(2, wsaaCommTotal, 2);
	private FixedLengthStringData[] wsaaCommGlcode = FLSDArrayPartOfArrayStructure(14, wsaaCommTotal, 4);
	private PackedDecimalData[] wsaaCommOrigamt = PDArrayPartOfArrayStructure(17, 2, wsaaCommTotal, 18);

	private FixedLengthStringData wsaaNewAgnt = new FixedLengthStringData(1).init("Y");
	private Validator newAgent = new Validator(wsaaNewAgnt, "Y");

	private FixedLengthStringData wsaaBlackList = new FixedLengthStringData(1);
	private Validator noCommission = new Validator(wsaaBlackList, "C", "X");
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaBatchkeyB618 = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaBatchkeyB680 = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaRdockey = new FixedLengthStringData(9);
	
	private FixedLengthStringData wsaaNewCurr = new FixedLengthStringData(1).init("Y");
	private Validator newCurrency = new Validator(wsaaNewCurr, "Y");

	private ZonedDecimalData wsaaProcessLevel = new ZonedDecimalData(1, 0).init(0).setUnsigned();
	private Validator endOfAllAgents = new Validator(wsaaProcessLevel, "3");
	private Validator endOfAgent = new Validator(wsaaProcessLevel, new ValueRange("2", "3"));
	private Validator endOfCurrency = new Validator(wsaaProcessLevel, new ValueRange("1", "3"));
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
	private int ct10 = 10;
	private int ct11 = 11;
	private int ct12 = 12;
	private int ct13 = 13;
	private int ct14 = 14;
	private int ct15 = 15;
	private int ct16 = 16;
	private int ct17 = 17;
	private int ct18 = 17;
	private String e141 = "E141";
	private String e192 = "E192";
	private String e193 = "E193";
	private String e289 = "E289";
	private String g447 = "G447";
	private boolean btchpr01Permission = false; //BTCHPR01
	private IntegerData chdrIndex = new IntegerData();
	private IntegerData commIndex = new IntegerData();
		/*ACCOUNT MOVEMENT FOR AGENTS TRANSACTIONS*/
	private AcmvagtTableDAM acmvagtIO = new AcmvagtTableDAM();
		/*SUB ACCOUNT MOVEMENT*/
	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
		/*Agent Consolidated Payment File*/
	private AgcsTableDAM agcsIO = new AgcsTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Life Agents file logical view for state*/
	private AglfagtTableDAM aglfagtIO = new AglfagtTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Account movements copy file for agent pa*/
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private Alocnorec alocnorec = new Alocnorec();
		/*Bank/Branch Name File*/
	private BabrTableDAM babrIO = new BabrTableDAM();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
		/*Logical File: Cheque details file*/
	private CheqTableDAM cheqIO = new CheqTableDAM();
	private Chkbkcdrec chkbkcdrec = new Chkbkcdrec();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Getdescrec getdescrec = new Getdescrec();
	private Glkeyrec glkeyrec = new Glkeyrec();
	private Glsubstrec glsubstrec = new Glsubstrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
		/*Payment Requisition Logical File*/
	private PreqTableDAM preqIO = new PreqTableDAM();
	private T3629rec t3629rec = new T3629rec();
	private T3672rec t3672rec = new T3672rec();
	private T3688rec t3688rec = new T3688rec();
	private T5622rec t5622rec = new T5622rec();
	private T5645rec t5645rec = new T5645rec();
	private T5690rec t5690rec = new T5690rec();
	private T6657rec t6657rec = new T6657rec();
		/*Agent Tax Withholding File*/
	private TatwTableDAM tatwIO = new TatwTableDAM();
	private Tr392rec tr392rec = new Tr392rec();
	private Tr393rec tr393rec = new Tr393rec();
	private Upperrec upperrec = new Upperrec();
	private Varcom varcom = new Varcom();
	private Itemkey wsaaItemkey = new Itemkey();


	public B5403() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*PROCESS*/
		initialise200();
		while ( !(endOfAllAgents.isTrue())) {
			mainProcessing1000();
		}

		/*OUT*/
		closeBatch700();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		varcom.vrcmTranid.set(runparmrec.tranid);
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("OPEN");
		batcdorrec.prefix.set(commonpar.parmBatcpfx);
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.transcode);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			databaseError006();
		}
		wsaaBatchkeyB618.set(batcdorrec.batchkey);
		batcdorrec.batcdorRec.set(SPACES);
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			databaseError006();
		}
		wsaaBatchkeyB680.set(batcdorrec.batchkey);
		wsaaDate.set(getCobolDate());
		wsaaTime.set(getCobolTime());
		aglfagtIO.setParams(SPACES);
		aglfagtIO.setAgntcoy(runparmrec.company);
		aglfagtIO.setFunction(varcom.begn);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("B5403");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(runparmrec.transcode);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(runparmrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		wsaaItemkey.set(SPACES);
		wsaaItemkey.itemItempfx.set("IT");
		wsaaItemkey.itemItemcoy.set(runparmrec.company);
		wsaaItemkey.itemItemtabl.set(t1688);
		wsaaItemkey.itemItemitem.set(runparmrec.runparm1);
		getdescrec.itemkey.set(wsaaItemkey);
		getdescrec.language.set(runparmrec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.set(SPACES);
		}
		wsaaTransDesc680.set(getdescrec.longdesc);
	}

protected void closeBatch700()
	{
		para701();
	}

protected void para701()
	{
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(wsaaBatchkeyB618);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
		batcuprec.function.set(varcom.writs);
		batcuprec.batchkey.set(wsaaBatchkeyB680);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			conerrrec.params.set(batcuprec.batcupRec);
			conerrrec.statuz.set(batcuprec.statuz);
			systemError005();
		}
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.batchkey.set(wsaaBatchkeyB618);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.batchkey.set(wsaaBatchkeyB680);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.params.set(batcdorrec.batcdorRec);
			conerrrec.statuz.set(batcdorrec.statuz);
			systemError005();
		}
	}

protected void mainProcessing1000()
	{
		try {
			process1100();
		}
		catch (GOTOException e){
		}
	}

protected void process1100()
	{
		wsaaProcessLevel.set(ZERO);
		SmartFileCode.execute(appVars, aglfagtIO);
		if (isNE(aglfagtIO.getStatuz(),varcom.oK)
		&& isNE(aglfagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(aglfagtIO.getParams());
			conerrrec.statuz.set(aglfagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(aglfagtIO.getStatuz(),varcom.endp)) {
			wsaaProcessLevel.set(3);
			return;
		}
		if (isNE(aglfagtIO.getAgntcoy(),runparmrec.company)) {
			wsaaProcessLevel.set(3);
			return;
		}
		getAgentPayMethod1193();
		if (paymthMissing.isTrue()) {
			aglfagtIO.setFunction(varcom.nextr);
			return;
		}
		wsaaBlackList.set(aglfagtIO.getTagsusind());
		if (noCommission.isTrue()) {
			updBlacklistAgent1400();
			aglfagtIO.setFunction(varcom.nextr);
			return;
		}
		itdmIO.setItemcoy(runparmrec.company);
		itdmIO.setItemtabl(t5622);
		itdmIO.setItemitem(aglfagtIO.getTaxcde());
		itdmIO.setItmfrm(runparmrec.effdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(itdmIO.getParams());
			databaseError006();
		}
		if (isNE(aglfagtIO.getTaxcde(),itdmIO.getItemitem())
		|| isNE(runparmrec.company,itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),t5622)
		|| isNE(itdmIO.getStatuz(),varcom.oK)) {
			t5622rec.taxrate.set(ZERO);
		}
		else {
			t5622rec.t5622Rec.set(itdmIO.getGenarea());
		}
		agntIO.setAgntnum(aglfagtIO.getAgntnum());
		agntIO.setAgntcoy(runparmrec.company);
		agntIO.setAgntpfx("AG");
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agntIO.getParams());
			conerrrec.statuz.set(agntIO.getStatuz());
			databaseError006();
		}
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(agntIO.getClntcoy());
		cltsIO.setClntnum(agntIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cltsIO.getParams());
			conerrrec.statuz.set(cltsIO.getStatuz());
			databaseError006();
		}
		updateReqd004();
		if (isEQ(controlrec.flag,"N")) {
			aglfagtIO.setFunction(varcom.nextr);
			return;
		}
		wsaaStoredAglfcurr.set(aglfagtIO.getCurrcode());
		wsaaStoredAgnt.set(aglfagtIO.getAgntnum());
		initCommTotalArray5000();
		wsaaTotOrigAmt.set(0);
		wsaaNewAgnt.set("Y");
		getBalColltr9600();
		if (isEQ(aglfagtIO.getCurrcode(),SPACES)) {
			while ( !(endOfAgent.isTrue())) {
				agntWithoutCurr1200();
			}

		}
		else {
			agntWithCurr1300();
		}
		aglfagtIO.setFunction(varcom.nextr);
	}


protected void getAgentPayMethod1193()
	{
		try {
			start1193();
			checkPayDetls1195();
		}
		catch (GOTOException e){
		}
	}

protected void start1193()
	{
		wsaaBankTran.set("N");
		wsaaAutoCheq.set("N");
		wsaaManCheq.set("N");
		wsaaOnAcct.set("N");
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(runparmrec.company);
		aglfIO.setAgntnum(aglfagtIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(aglfIO.getParams());
			conerrrec.statuz.set(aglfIO.getParams());
			databaseError006();
		}
	}

protected void checkPayDetls1195()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t5690);
		itemIO.setItemitem(aglfIO.getPaymth());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			systemError005();
		}
		t5690rec.t5690Rec.set(itemIO.getGenarea());
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t3672);
		itemIO.setItemitem(t5690rec.paymentMethod);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			conlogrec.error.set(g447);
			conlogrec.params.set(aglfagtIO.getAgntnum());
			callConlog003();
			wsaaPaymthMissing.set("Y");
			return;
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t3672rec.t3672Rec.set(itemIO.getGenarea());
		if (isEQ(t3672rec.onactflg,"Y")) {
			wsaaOnAcct.set("Y");
		}
		else {
			if (isEQ(t3672rec.bankaccreq,"Y")) {
				wsaaBankTran.set("Y");
			}
			else {
				if (isEQ(t3672rec.chqNumReq,"Y")
				&& isEQ(t3672rec.immPostInd,"N")) {
					wsaaManCheq.set("Y");
				}
				else {
					wsaaAutoCheq.set("Y");
				}
			}
		}
	}

protected void agntWithoutCurr1200()
	{
		try {
			process1200();
		}
		catch (GOTOException e){
		}
	}

protected void process1200()
	{
		if (newAgent.isTrue()) {
			bgnNewAgnt2200();
			if (endOfAgent.isTrue()) {
				return;
			}
		}
		wsaaNewCurr.set("Y");
		initCommTotalArray5000();
		storeCurrency2700();
		while ( !(endOfCurrency.isTrue()
		|| endOfAgent.isTrue())) {
			processAgpy2300();
		}

		createCheqRec4000();
		createPaymentHeader4100();
		createPaymentDissec4500();
	}

protected void agntWithCurr1300()
	{
		try {
			process1300();
		}
		catch (GOTOException e){
		}
	}

protected void process1300()
	{
		if (newAgent.isTrue()) {
			bgnNewAgnt2200();
			if (endOfAgent.isTrue()) {
				return;
			}
		}
		storeCurrency2700();
		initCommTotalArray5000();
		wsaaEnough.set("N");
		while ( !(endOfAgent.isTrue()
		|| enoughMoneyEarned.isTrue())) {
			checkIfEnoughMoney2400();
		}

		if (enoughMoneyEarned.isTrue()) {
			processAgpyWithCurr3000();
		}
	}

protected void updBlacklistAgent1400()
	{
					start1410();
					callAgpyagtio1430();
				
	}

protected void start1410()
	{
		agpyagtIO.setRldgcoy(runparmrec.company);
		agpyagtIO.setRldgacct(aglfagtIO.getAgntnum());
		agpyagtIO.setOrigcurr(SPACES);
		agpyagtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agpyagtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agpyagtIO.setFitKeysSearch("RLDGCOY", "RLDGACCT");
	}

protected void callAgpyagtio1430()
	{
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),aglfagtIO.getAgntnum())) {
			return;
		}
		updateAcmv4200();
		agpyagtIO.setFunction(varcom.nextr);
		callAgpyagtio1430();
	}

protected void bgnNewAgnt2200()
	{
		try {
			process2200();
		}
		catch (GOTOException e){
		}
	}

protected void process2200()
	{
		wsaaStoredAgent.set(aglfagtIO.getAgntnum());
		agpyagtIO.setRldgcoy(runparmrec.company);
		agpyagtIO.setRldgacct(wsaaStoredAgnt);
		agpyagtIO.setOrigcurr(SPACES);
		agpyagtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agpyagtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agpyagtIO.setFitKeysSearch("RLDGCOY", "RLDGACCT");
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			return;
		}
		wsaaNewAgnt.set("N");
		wsaaNewCurr.set("Y");
	}

protected void processAgpy2300()
	{
		try {
			para2300();
		}
		catch (GOTOException e){
		}
	}

protected void para2300()
	{
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			return;
		}
		if (isNE(agpyagtIO.getOrigcurr(),wsaaStoredAgpycurr)) {
			wsaaProcessLevel.set(1);
			return;
		}
		wsaaOrigamt.set(agpyagtIO.getOrigamt());
		currConvLdgrUpdate4400();
		updateAcmv4200();
		acumCommPayable6000();
		contotrec.totno.set(ct09);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct10);
		contotrec.totval.set(agpyagtIO.getOrigamt());
		callContot001();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
	}

protected void checkIfEnoughMoney2400()
	{
		
					para2400();
					para2450();
		
	}

protected void para2400()
	{
		if (isNE(agpyagtIO.getOrigcurr(),aglfagtIO.getCurrcode())
		&& isNE(aglfagtIO.getCurrcode(),SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaTotOrigAmt.add(agpyagtIO.getOrigamt());
			para2450();
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		conlinkrec.currIn.set(agpyagtIO.getOrigcurr());
		conlinkrec.currOut.set(aglfagtIO.getCurrcode());
		conlinkrec.amountIn.set(agpyagtIO.getOrigamt());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			conerrrec.params.set(conlinkrec.clnk002Rec);
			conerrrec.statuz.set(conlinkrec.statuz);
			systemError005();
		}
		wsaaTotOrigAmt.add(conlinkrec.amountOut);
	}

protected void para2450()
	{
		checkMinAmnt2500();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
		}
		/*EXIT*/
	}

protected void checkMinAmnt2500()
	{
		/*PARA*/
		if (isGT(aglfagtIO.getMinsta(),wsaaTotOrigAmt)) {
			wsaaEnough.set("N");
		}
		else {
			wsaaEnough.set("Y");
		}
		/*EXIT*/
	}

protected void storeCurrency2700()
	{
		/*PARA*/
		wsaaStoredAgpycurr.set(agpyagtIO.getOrigcurr());
		wsaaNewCurr.set("N");
		wsaaTotOrigAmt.set(ZERO);
		wsaaProcessLevel.set(ZERO);
		/*EXIT*/
	}

protected void processAgpyWithCurr3000()
	{
		/*PARA*/
//added by zhaohui for bug 4483
		agpyagtIO.setParams(SPACES);
//added end
		agpyagtIO.setRldgcoy(runparmrec.company);
		agpyagtIO.setRldgacct(wsaaStoredAgnt);
		agpyagtIO.setOrigcurr(SPACES);
		wsaaProcessLevel.set(0);
		wsaaNewAgnt.set("Y");
		agntWithCurr3300();
		createCheqRec4000();
		createPaymentHeader4100();
		createPaymentDissec4500();
		/*EXIT*/
	}

protected void bgnNewAgnt3250()
	{
		try {
			process3250();
		}
		catch (GOTOException e){
		}
	}

protected void process3250()
	{
		agpyagtIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agpyagtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agpyagtIO.setFitKeysSearch("RLDGCOY", "RLDGACCT");
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
			return;
		}
		wsaaProcessLevel.set(0);
		wsaaNewAgnt.set("N");
		wsaaNewCurr.set("Y");
	}


protected void agntWithCurr3300()
	{
		/*PROCESS*/
		if (newAgent.isTrue()) {
			bgnNewAgnt3250();
		}
		while ( !(endOfAgent.isTrue())) {
			tranPerAgnt3450();
		}

		/*EXIT*/
	}

protected void tranPerAgnt3450()
	{
					para3450();
					para3455();
		
	}

protected void para3450()
	{
		if (isNE(agpyagtIO.getOrigcurr(),aglfagtIO.getCurrcode())
		&& isNE(aglfagtIO.getCurrcode(),SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaOrigamt.set(agpyagtIO.getOrigamt());
			para3455();
		}
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(runparmrec.company);
		conlinkrec.cashdate.set(runparmrec.effdate);
		conlinkrec.currIn.set(agpyagtIO.getOrigcurr());
		conlinkrec.currOut.set(aglfagtIO.getCurrcode());
		conlinkrec.amountIn.set(agpyagtIO.getOrigamt());
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			conerrrec.params.set(conlinkrec.clnk002Rec);
			conerrrec.statuz.set(conlinkrec.statuz);
			systemError005();
		}
		wsaaOrigamt.set(conlinkrec.amountOut);
	}

protected void para3455()
	{
		currConvLdgrUpdate4400();
		updateAcmv4200();
		acumCommPayable6000();
		contotrec.totno.set(ct09);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct10);
		contotrec.totval.set(agpyagtIO.getOrigamt());
		callContot001();
		agpyagtIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(agpyagtIO.getParams());
			conerrrec.statuz.set(agpyagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)
		|| isNE(agpyagtIO.getRldgcoy(),runparmrec.company)
		|| isNE(agpyagtIO.getRldgacct(),wsaaStoredAgnt)) {
			wsaaProcessLevel.set(2);
		}
	}

protected void createCheqRec4000()
	{
		try {
			para4000();
		}
		catch (GOTOException e){
		}
	}

protected void para4000()
	{
		wsaaTotalAgntComm.set(0);
		for (commIndex.set(1); !(isEQ(wsaaCommSacscode[commIndex.toInt()],SPACES)
		|| isGT(commIndex,20)); commIndex.add(1)){
			calcTotalAmount5100();
		}
		compute(wsaaTotalAgntComm, 2).set(mult(wsaaTotalAgntComm,-1));
		compute(wsaaTotalWhtax, 3).setRounded(div(mult(wsaaTotalAgntComm,t5622rec.taxrate),100));
		getBankCharge10100();
		getCollateral9700();
		tatwIO.setParams(SPACES);
		tatwIO.setOrigamt(wsaaTotalAgntComm);
		wsaaTotalAgntComm.subtract(wsaaTotalWhtax);
		wsaaTotalAgntComm.subtract(wsaaTotalColltr);
		wsaaSacscode.set(t5645rec.sacscode12);
		wsaaSacstyp.set(t5645rec.sacstype12);
		wsaaDeductn.set(ZERO);
		for (chdrIndex.set(1); !(isGT(chdrIndex,90)
		|| isEQ(wsaaChdrnum[chdrIndex.toInt()],SPACES)); chdrIndex.add(1)){
			getPremiumTolerance9000();
		}
		wsaaTotalAdvance.set(wsaaDeductn);
		wsaaSacscode.set(t5645rec.sacscode09);
		wsaaSacstyp.set(t5645rec.sacstype09);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction9a00();
		wsaaTotalAdvance.add(wsaaDeductn);
		wsaaSacscode.set(t5645rec.sacscode10);
		wsaaSacstyp.set(t5645rec.sacstype10);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction9a00();
		wsaaTotalAdvance.add(wsaaDeductn);
		wsaaTotalDeduct.set(wsaaTotalWhtax);
		wsaaTotalDeduct.add(wsaaTotalColltr);
		wsaaTotalDeduct.add(wsaaTotalAdvance);
		wsaaLifaTotOrig.subtract(wsaaTotalWhtax);
		wsaaLifaTotAcct.subtract(wsaaTotalWhtax);
		wsaaLifaTotOrig.subtract(wsaaTotalColltr);
		wsaaLifaTotAcct.subtract(wsaaTotalColltr);
		wsaaLifaTotOrig.subtract(wsaaTotalAdvance);
		wsaaLifaTotAcct.subtract(wsaaTotalAdvance);
		if (isNE(wsaaTotalWhtax,ZERO)) {
			moveToLifa9800();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
			tatwIO.setAgntcoy(aglfagtIO.getAgntcoy());
			tatwIO.setAgntnum(aglfagtIO.getAgntnum());
			tatwIO.setEffdate(runparmrec.effdate);
			tatwIO.setValidflag("1");
			tatwIO.setOrigcurr(lifacmvrec.origcurr);
			tatwIO.setTaxamt(wsaaTotalWhtax);
			tatwIO.setFormat(tatwrec);
			tatwIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, tatwIO);
			if (isNE(tatwIO.getStatuz(),varcom.oK)) {
				conerrrec.params.set(tatwIO.getParams());
				conerrrec.statuz.set(tatwIO.getStatuz());
				databaseError006();
			}
			contotrec.totno.set(ct15);
			contotrec.totval.set(wsaaTotalWhtax);
			callContot001();
		}
		if (isNE(wsaaTotalColltr,ZERO)) {
			moveToLifa9900();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
			contotrec.totno.set(ct17);
			contotrec.totval.set(wsaaTotalColltr);
			callContot001();
		}
		if (isNE(wsaaLifaTotOrig,ZERO)) {
			moveToLifa10000();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
		}
		if (isNE(wsaaTotalAdvance,0)) {
			contotrec.totno.set(ct16);
			contotrec.totval.set(wsaaTotalAdvance);
			callContot001();
		}
		if (isNE(wsaaTdcchrg,ZERO)) {
			moveToLifa10100();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
			contotrec.totno.set(ct18);
			contotrec.totval.set(wsaaTdcchrg);
			callContot001();
			wsaaTotalAgntComm.subtract(wsaaTdcchrg);
		}
		if (isLT(wsaaTotalAgntComm,1)
		&& isNE(wsaaTotalAgntComm,0)) {
			contotrec.totno.set(ct13);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(ct14);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
			return;
		}
		if (isEQ(wsaaTotalAgntComm,0)) {
			return;
		}
		if (isEQ(aglfagtIO.getAgccqind(),"Y")
		&& isNE(aglfagtIO.getPayclt(),SPACES)) {
			return;
		}
		cheqIO.setParams(SPACES);
		if (manCheq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(ct02);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		if (autoCheq.isTrue()) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(ct04);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		if (bankTran.isTrue()) {
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			contotrec.totno.set(ct06);
			contotrec.totval.set(wsaaTotalAgntComm);
			callContot001();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t6657);
		itemIO.setItempfx("IT");
		if (isNE(aglfagtIO.getCurrcode(),SPACES)) {
			itemIO.setItemitem(aglfagtIO.getCurrcode());
		}
		else {
			itemIO.setItemitem(wsaaStoredAgpycurr);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t6657rec.t6657Rec.set(itemIO.getGenarea());
		cheqIO.setReqnbcde(t6657rec.bankcode);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t3688);
		itemIO.setItemitem(t6657rec.bankcode);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			databaseError006();
		}
		t3688rec.t3688Rec.set(itemIO.getGenarea());
		cheqIO.setPaycurr(t3688rec.bcurr);
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set(fsupfxcpy.reqn);
		alocnorec.company.set(runparmrec.company);
		alocnorec.genkey.set(SPACES);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz,varcom.oK)) {
			conerrrec.statuz.set(alocnorec.statuz);
			systemError005();
		}
		cheqIO.setReqnno(alocnorec.alocNo);
		cheqIO.setReqncoy(runparmrec.company);
		cheqIO.setClntcoy(agntIO.getClntcoy());
		if (isNE(aglfagtIO.getPayclt(),SPACES)) {
			cheqIO.setClntnum01(aglfagtIO.getPayclt());
		}
		else {
			cheqIO.setClntnum01(agntIO.getClntnum());
		}
		if (isEQ(aglfagtIO.getPayclt(),SPACES)) {
			agntlagIO.setAgntcoy(aglfagtIO.getAgntcoy());
			agntlagIO.setAgntnum(aglfagtIO.getAgntnum());
			agntlagIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, agntlagIO);
			if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
				conerrrec.params.set(agntlagIO.getParams());
				conerrrec.statuz.set(agntlagIO.getStatuz());
				databaseError006();
			}
			else {
				cheqIO.setClntnum01(agntlagIO.getClntnum());
			}
		}
		wsaaUser.set(runparmrec.user);
		if ((isEQ(wsaaUser,ZERO))
		|| (isEQ(wsaaUser,SPACES))) {
			cheqIO.setUser(ZERO);
			cheqIO.setAuthuser(ZERO);
		}
		else {
			cheqIO.setUser(runparmrec.user);
			cheqIO.setAuthuser(runparmrec.user);
		}
		cheqIO.setClttype01(cltsIO.getClttype());
		readTr3934050();
		if (isEQ(cltsIO.getClttype(),"C")) {
			cheqIO.setSurnam01(wsaaLongconfname);
			cheqIO.setSalNInit01(SPACES);
		}
		else {
			cheqIO.setSurnam01(cltsIO.getSurname());
			if (isEQ(tr393rec.salutind,"S")) {
				descT35834070();
				StringBuilder stringVariable1 = new StringBuilder();
				stringVariable1.append(delimitedExp(descIO.getShortdesc(), "  "));
				stringVariable1.append(delimitedExp(" ", "  "));
				stringVariable1.append(delimitedExp(cltsIO.getInitials(), "  "));
				cheqIO.getSalNInit01().setLeft(stringVariable1.toString());
			}
			else {
				StringBuilder stringVariable2 = new StringBuilder();
				stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
				stringVariable2.append(delimitedExp(" ", "  "));
				stringVariable2.append(delimitedExp(cltsIO.getInitials(), "  "));
				cheqIO.getSalNInit01().setLeft(stringVariable2.toString());
			}
		}
		cheqIO.setSurnam01(cltsIO.getSurname());
		upperrec.name.set(cltsIO.getSurname());
		upperrec.statuz.set(SPACES);
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz,varcom.oK)) {
			cheqIO.setCapname(cltsIO.getSurname());
		}
		else {
			cheqIO.setCapname(upperrec.name);
		}
		cheqIO.setCheqdupn(SPACES);
		cheqIO.setRequser(runparmrec.user);
		cheqIO.setFacthous(aglfagtIO.getFacthous());
		cheqIO.setBankkey(aglfagtIO.getBankkey());
		cheqIO.setBankacckey(aglfagtIO.getBankacckey());
		cheqIO.setAppruser(varcom.vrcmUser);
		cheqIO.setPaydate(runparmrec.effdate);
		cheqIO.setBranch(runparmrec.branch);
		cheqIO.setPresdate(ZERO);
		cheqIO.setStmtpage(ZERO);
		cheqIO.setArchdate(ZERO);
		cheqIO.setTransactionDate(wsaaDate);
		cheqIO.setAuthdate(runparmrec.effdate);
		cheqIO.setApprdte(runparmrec.effdate);
		cheqIO.setReqdate(runparmrec.effdate);
		cheqIO.setTransactionTime(wsaaTime);
		cheqIO.setApprtime(wsaaTime);
		cheqIO.setReqtime(wsaaTime);
		cheqIO.setAuthtime(wsaaTime);
		cheqIO.setProcind("AU");
		cheqIO.setReqntype(t5690rec.paymentMethod);
		cheqIO.setPresflag("N");
		cheqIO.setPayamt(wsaaTotalAgntComm);
		cheqIO.setZprnno(ZERO);
		cheqIO.setFormat(cheqrec);
		cheqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, cheqIO);
		if (isNE(cheqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(cheqIO.getParams());
			conerrrec.statuz.set(cheqIO.getStatuz());
			databaseError006();
		}
	}



protected void readTr3934050()
	{
		start4051();
	}

protected void start4051()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.fsuco);
		itemIO.setItemtabl(tr393);
		itemIO.setItemitem(runparmrec.fsuco);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		tr393rec.tr393Rec.set(itemIO.getGenarea());
	}

protected void descT35834070()
	{
		start4071();
	}

protected void start4071()
	{
		upperrec.upperRec.set(SPACES);
		upperrec.name.set(cltsIO.getSalutl());
		callProgram(Upper.class, upperrec.upperRec);
		if (isNE(upperrec.statuz,varcom.oK)) {
			wsaaItemitem.set(cltsIO.getSalutl());
		}
		else {
			wsaaItemitem.set(upperrec.name);
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.fsuco);
		descIO.setDesctabl(t3583);
		descIO.setDescitem(wsaaItemitem);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction("READR");
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.params.set(descIO.getParams());
			conerrrec.statuz.set(descIO.getStatuz());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

protected void createPaymentHeader4100()
	{
		try {
			para4100();
		}
		catch (GOTOException e){
		}
	}

protected void para4100()
	{
		if (isLT(wsaaTotalAgntComm,1)) {
			return;
		}
		if (isEQ(aglfagtIO.getAgccqind(),"Y")
		&& isNE(aglfagtIO.getPayclt(),SPACES)) {
			return;
		}
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(runparmrec.acctmonth);
		preqIO.setPostyear(runparmrec.acctyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(runparmrec.company);
		preqIO.setRdocnum(cheqIO.getReqnno());
		preqIO.setRldgcoy(runparmrec.company);
		chkbkcdrec.function.set("REQNBANK");
		chkbkcdrec.bankcode.set(t6657rec.bankcode);
		chkbkcdrec.user.set(runparmrec.user);
		chkbkcdrec.company.set(runparmrec.company);
		callProgram(Chkbkcd.class, chkbkcdrec.chkbkcdRec);
		if (isNE(chkbkcdrec.statuz,varcom.oK)) {
			conerrrec.params.set(chkbkcdrec.chkbkcdRec);
			conerrrec.syserror.set(chkbkcdrec.statuz);
			systemError005();
		}
		wsaaGenlkey.set(chkbkcdrec.genlkey);
		preqIO.setGenlcoy(wsaaGenlcoy);
		preqIO.setGlcode(wsaaGlcode);
		preqIO.setGlsign(chkbkcdrec.sign);
		getGlcodeSignContot4700();
		preqIO.setCnttot(glkeyrec.contot);
		preqIO.setSacscode(t3672rec.sacscode);
		preqIO.setSacstyp(t3672rec.sacstyp);
		preqIO.setRldgacct(t6657rec.bankcode);
		if (isEQ(wsaaStoredAglfcurr,SPACES)) {
			preqIO.setOrigccy(wsaaStoredAgpycurr);
		}
		else {
			preqIO.setOrigccy(wsaaStoredAglfcurr);
		}
		if (isNE(wsaaLastOrigCcy,preqIO.getOrigccy())) {
			getCurrencyRate7000();
		}
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate,wsaaTotalAgntComm));
		preqIO.setOrigamt(wsaaTotalAgntComm);
		wsaaJrnseq.set(ZERO);
		preqIO.setJrnseq(ZERO);
		preqIO.setEffdate(runparmrec.effdate);
		preqIO.setTrandesc(cltsIO.getSurname());
		preqIO.setBranch(runparmrec.branch);
		preqIO.setTaxcat(SPACES);
		preqIO.setFrcdate(ZERO);
		preqIO.setRcamt(ZERO);
		preqIO.setTransactionDate(wsaaDate);
		preqIO.setTransactionTime(wsaaTime);
		preqIO.setUser(runparmrec.user);
		preqIO.setFormat(preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(preqIO.getParams());
			conerrrec.statuz.set(preqIO.getStatuz());
			databaseError006();
		}
	}



protected void updateAcmv4200()
	{
		
					para4200();
					acmvLoop4205();
		
	}

protected void para4200()
	{
		acmvagtIO.setParams(SPACES);
		acmvagtIO.setFunction(varcom.begnh);
		acmvagtIO.setBatccoy(agpyagtIO.getBatccoy());
		acmvagtIO.setBatcbrn(agpyagtIO.getBatcbrn());
		acmvagtIO.setBatcactyr(agpyagtIO.getBatcactyr());
		acmvagtIO.setBatcactmn(agpyagtIO.getBatcactmn());
		acmvagtIO.setBatctrcde(agpyagtIO.getBatctrcde());
		acmvagtIO.setBatcbatch(agpyagtIO.getBatcbatch());
		acmvagtIO.setRldgacct(agpyagtIO.getRldgacct());
		acmvagtIO.setOrigcurr(agpyagtIO.getOrigcurr());
		acmvagtIO.setSacscode(agpyagtIO.getSacscode());
		acmvagtIO.setSacstyp(agpyagtIO.getSacstyp());
		acmvagtIO.setTranno(agpyagtIO.getTranno());
		acmvagtIO.setRdocnum(agpyagtIO.getRdocnum());
		acmvagtIO.setJrnseq(agpyagtIO.getJrnseq());
	}

protected void acmvLoop4205()
	{
		SmartFileCode.execute(appVars, acmvagtIO);
		if (isNE(acmvagtIO.getStatuz(),varcom.oK)
				&& isNE(acmvagtIO.getStatuz(),varcom.endp)) {
			conerrrec.params.set(acmvagtIO.getParams());
			conerrrec.statuz.set(acmvagtIO.getStatuz());
			databaseError006();
		}
		if (isEQ(acmvagtIO.getStatuz(),varcom.endp)) {
				return;
		}
		if (isNE(acmvagtIO.getBatccoy(),agpyagtIO.getBatccoy())
		|| isNE(acmvagtIO.getRldgacct(),agpyagtIO.getRldgacct())
		|| isNE(acmvagtIO.getSacscode(),agpyagtIO.getSacscode())
		|| isNE(acmvagtIO.getSacstyp(),agpyagtIO.getSacstyp())
		|| isNE(acmvagtIO.getTranno(),agpyagtIO.getTranno())
		|| isNE(acmvagtIO.getRdocnum(),agpyagtIO.getRdocnum())
		|| isNE(acmvagtIO.getJrnseq(),agpyagtIO.getJrnseq())) {
			return;
		}
		if (isNE(agpyagtIO.getOrigamt(),acmvagtIO.getOrigamt())) {
			acmvagtIO.setFunction(varcom.nextr);
			acmvLoop4205();
		}
		if (isNE(acmvagtIO.getFrcdate(),varcom.vrcmMaxDate)) {
			acmvagtIO.setFunction(varcom.nextr);
			acmvLoop4205();
		}
		acmvagtIO.setFrcdate(runparmrec.effdate);
		acmvagtIO.setRcamt(acmvagtIO.getOrigamt());
		acmvagtIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, acmvagtIO);
		if (isNE(acmvagtIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(acmvagtIO.getParams());
			conerrrec.statuz.set(acmvagtIO.getStatuz());
			databaseError006();
		}
		if (noCommission.isTrue()) {
			return;
		}
		contotrec.totno.set(ct11);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct12);
		contotrec.totval.set(agpyagtIO.getOrigamt());
		callContot001();
	}



protected void currConvLdgrUpdate4400()
	{
		try {
			para4400();
		}
		catch (GOTOException e){
		}
	}

protected void para4400()
	{
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.rldgcoy.set(agpyagtIO.getRldgcoy());
		lifacmvrec.genlcoy.set(agpyagtIO.getGenlcoy());
		lifacmvrec.batckey.set(wsaaBatchkeyB680);
		lifacmvrec.rdocnum.set(agpyagtIO.getRdocnum());
		lifacmvrec.tranno.set(0);
		lifacmvrec.jrnseq.set(agpyagtIO.getJrnseq());
		lifacmvrec.sacscode.set(agpyagtIO.getSacscode());
		lifacmvrec.sacstyp.set(agpyagtIO.getSacstyp());
		lifacmvrec.glcode.set(agpyagtIO.getGlcode());
		if (isEQ(agpyagtIO.getGlsign(),"+")) {
			lifacmvrec.glsign.set("-");
		}
		else {
			lifacmvrec.glsign.set("+");
		}
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origcurr.set(agpyagtIO.getOrigcurr());
		lifacmvrec.rldgacct.set(agpyagtIO.getRldgacct());
		lifacmvrec.origamt.set(agpyagtIO.getOrigamt());
		lifacmvrec.acctamt.set(agpyagtIO.getAcctamt());
		lifacmvrec.genlcur.set(agpyagtIO.getGenlcur());
		lifacmvrec.crate.set(agpyagtIO.getCrate());
		lifacmvrec.rcamt.set(lifacmvrec.origamt);
		lifacmvrec.frcdate.set(runparmrec.effdate);
		lifacmvrec.postyear.set(runparmrec.acctyear);
		lifacmvrec.postmonth.set(runparmrec.acctmonth);
		lifacmvrec.trandesc.set(wsaaTransDesc680);
		lifacmvrec.tranref.set(agpyagtIO.getTranref());
		lifacmvrec.effdate.set(runparmrec.effdate);
		lifacmvrec.transactionDate.set(wsaaDate);
		lifacmvrec.transactionTime.set(wsaaTime);
		lifacmvrec.user.set(runparmrec.user);
		varcom.vrcmTranid.set(runparmrec.tranid);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.substituteCode[1].set(cheqIO.getReqntype());
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(runparmrec.company.toString(), "BTCHPR01", appVars, "IT");
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
		
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			databaseError006();
		}
		if (isEQ(agpyagtIO.getOrigcurr(),wsaaStoredAglfcurr)
		|| isEQ(wsaaStoredAglfcurr,SPACES)) {
			wsaaLifaTotOrig.add(lifacmvrec.origamt);
			wsaaLifaTotAcct.add(lifacmvrec.acctamt);
			keepChdrCurr9100();
			return;
		}
		lifacmvrec.sacscode.set(t5645rec.sacscode04);
		lifacmvrec.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec.glcode.set(t5645rec.glmap04);
		lifacmvrec.glsign.set(t5645rec.sign04);
		lifacmvrec.contot.set(t5645rec.cnttot04);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.statuz);
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			databaseError006();
		}
		lifacmvrec.origamt.set(conlinkrec.amountOut);
		lifacmvrec.rcamt.set(conlinkrec.amountOut);
		keepChdrCurr9100();
		lifacmvrec.origcurr.set(wsaaStoredAglfcurr);
		lifacmvrec.sacscode.set(t5645rec.sacscode05);
		lifacmvrec.sacstyp.set(t5645rec.sacstype05);
		lifacmvrec.glcode.set(t5645rec.glmap05);
		lifacmvrec.glsign.set(t5645rec.sign05);
		lifacmvrec.contot.set(t5645rec.cnttot05);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz,varcom.oK)) {
			conerrrec.params.set(lifacmvrec.statuz);
			conerrrec.params.set(lifacmvrec.lifacmvRec);
			conerrrec.statuz.set(lifacmvrec.statuz);
			databaseError006();
		}
		wsaaLifaTotOrig.add(lifacmvrec.origamt);
		wsaaLifaTotAcct.add(lifacmvrec.acctamt);
	}



protected void createPaymentDissec4500()
	{
		try {
			para4500();
		}
		catch (GOTOException e){
		}
	}

protected void para4500()
	{
		if (isLT(wsaaTotalAgntComm,1)) {
			return;
		}
		for (commIndex.set(1); !(isEQ(wsaaCommSacscode[commIndex.toInt()],SPACES)
		|| isGT(commIndex,20)); commIndex.add(1)){
			createDissec4600();
		}
	}



protected void createDissec4600()
	{
		
					para4600();
				
					start4610();
				
	}

protected void para4600()
	{
		if (isEQ(wsaaTotalDeduct,ZERO)) {
			start4610();
		}
		compute(wsaaDissAmount, 2).set(mult(wsaaCommOrigamt[commIndex.toInt()],-1));
		if (isLTE(wsaaDissAmount,wsaaTotalDeduct)) {
			wsaaTotalDeduct.subtract(wsaaDissAmount);
			wsaaCommOrigamt[commIndex.toInt()].set(ZERO);
			return;
		}
		else {
			wsaaCommOrigamt[commIndex.toInt()].add(wsaaTotalDeduct);
			wsaaTotalDeduct.set(ZERO);
		}
	}



protected void start4610()
	{
		if (isEQ(aglfagtIO.getAgccqind(),"Y")
		&& isNE(aglfagtIO.getPayclt(),SPACES)) {
			createAgcs4800();
			return;
		}
		preqIO.setParams(SPACES);
		preqIO.setPostmonth(runparmrec.acctmonth);
		preqIO.setPostyear(runparmrec.acctyear);
		preqIO.setRdocpfx(fsupfxcpy.reqn);
		preqIO.setRdoccoy(runparmrec.company);
		preqIO.setRdocnum(cheqIO.getReqnno());
		preqIO.setRldgcoy(runparmrec.company);
		preqIO.setRldgacct(agntIO.getAgntnum());
		if (isEQ(wsaaStoredAglfcurr,SPACES)) {
			preqIO.setOrigccy(wsaaStoredAgpycurr);
		}
		else {
			preqIO.setOrigccy(wsaaStoredAglfcurr);
		}
		preqIO.setSacscode(t5645rec.sacscode02);
		preqIO.setSacstyp(t5645rec.sacstype02);
		preqIO.setGlcode(t5645rec.glmap02);
		preqIO.setGlsign(t5645rec.sign02);
		preqIO.setCnttot(t5645rec.cnttot02);
		preqIO.setGenlcoy(runparmrec.company);
		setPrecision(preqIO.getOrigamt(), 2);
		preqIO.setOrigamt(mult(wsaaCommOrigamt[commIndex.toInt()],-1));
		preqIO.setCrate(wsaaNominalRate);
		preqIO.setGenlcur(wsaaLedgerCcy);
		setPrecision(preqIO.getAcctamt(), 9);
		preqIO.setAcctamt(mult(wsaaNominalRate,preqIO.getOrigamt()));
		wsaaJrnseq.add(1);
		preqIO.setJrnseq(wsaaJrnseq);
		preqIO.setEffdate(runparmrec.effdate);
		preqIO.setBranch(runparmrec.branch);
		preqIO.setTrandesc(cltsIO.getSurname());
		preqIO.setTaxcat(SPACES);
		preqIO.setRcamt(ZERO);
		preqIO.setFrcdate(ZERO);
		preqIO.setTransactionDate(wsaaDate);
		preqIO.setTransactionTime(wsaaTime);
		preqIO.setUser(runparmrec.user);
		contotrec.totno.set(ct07);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(preqIO.getOrigamt());
		callContot001();
		preqIO.setFormat(preqrec);
		preqIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, preqIO);
		if (isNE(preqIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(preqIO.getParams());
			conerrrec.statuz.set(preqIO.getStatuz());
			databaseError006();
		}
	}

protected void getGlcodeSignContot4700()
	{
		getGlcodeSignContotPara4700();
	}

protected void getGlcodeSignContotPara4700()
	{
		glkeyrec.function.set("RETN");
		glkeyrec.company.set(runparmrec.company);
		glkeyrec.trancode.set(runparmrec.transcode);
		glkeyrec.sacscode.set(t3672rec.sacscode);
		glkeyrec.sacstyp.set(t3672rec.sacstyp);
		glkeyrec.ledgkey.set(SPACES);
		callProgram(Glkey.class, glkeyrec.glkeyRec);
		if (isEQ(glkeyrec.statuz,"NFND")) {
			glkeyrec.statuz.set(e141);
		}
		else {
			if (isEQ(glkeyrec.statuz,"MRNF")) {
				glkeyrec.statuz.set(e192);
			}
			else {
				if (isEQ(glkeyrec.statuz,"NCUR")) {
					glkeyrec.statuz.set(e193);
				}
				else {
					if (isEQ(glkeyrec.statuz,"BCTL")) {
						glkeyrec.statuz.set(e289);
					}
				}
			}
		}
		if (isNE(glkeyrec.statuz,varcom.oK)) {
			conerrrec.params.set(glkeyrec.glkeyRec);
			conerrrec.syserror.set(glkeyrec.statuz);
			systemError005();
		}
		glsubstrec.glsubstRec.set(SPACES);
		glsubstrec.function.set("SBALL");
		glsubstrec.company.set(glkeyrec.company);
		glsubstrec.glacct.set(glkeyrec.genlAccount);
		glsubstrec.branch.set(runparmrec.batcbranch);
		glsubstrec.bankcode.set(t6657rec.bankcode);
		if (isEQ(wsaaStoredAglfcurr,SPACES)) {
			glsubstrec.currency.set(wsaaStoredAgpycurr);
		}
		else {
			glsubstrec.currency.set(wsaaStoredAglfcurr);
		}
		callProgram(Glsubst.class, glsubstrec.glsubstRec);
		if (isNE(glsubstrec.statuz,varcom.oK)) {
			conerrrec.params.set(glsubstrec.glsubstRec);
			conerrrec.syserror.set(glsubstrec.statuz);
			databaseError006();
		}
	}

protected void createAgcs4800()
	{
		create4810();
	}

protected void create4810()
	{
		agcsIO.setDataArea(SPACES);
		agcsIO.setAgntcoy(aglfagtIO.getAgntcoy());
		agcsIO.setAgntnum(aglfagtIO.getAgntnum());
		if (isNE(aglfagtIO.getCurrcode(),SPACES)) {
			agcsIO.setCurrcode(aglfagtIO.getCurrcode());
		}
		else {
			agcsIO.setCurrcode(wsaaStoredAgpycurr);
		}
		agcsIO.setPayclt(aglfagtIO.getPayclt());
		agcsIO.setSacscode(wsaaCommSacscode[commIndex.toInt()]);
		agcsIO.setSacstyp(wsaaCommSacstyp[commIndex.toInt()]);
		compute(wsaaCommOrigamt[commIndex.toInt()], 2).set(mult(wsaaCommOrigamt[commIndex.toInt()],-1));
		agcsIO.setPayamt(wsaaCommOrigamt[commIndex.toInt()]);
		agcsIO.setEffdate(runparmrec.effdate);
		agcsIO.setFrcdate(varcom.vrcmMaxDate);
		agcsIO.setFunction(varcom.writr);
		agcsIO.setFormat(agcsrec);
		SmartFileCode.execute(appVars, agcsIO);
		if (isNE(agcsIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agcsIO.getParams());
			conerrrec.statuz.set(agcsIO.getStatuz());
			databaseError006();
		}
	}

protected void initCommTotalArray5000()
	{
		try {
			start5000();
		}
		catch (GOTOException e){
		}
	}

protected void start5000()
	{
		wsaaTotalWhtax.set(ZERO);
		wsaaTotalColltr.set(ZERO);
		wsaaLifaTotOrig.set(ZERO);
		wsaaTotalDeduct.set(ZERO);
		wsaaLifaTotAcct.set(ZERO);
		wsaaCommTotalArray.set(SPACES);
		wsaaChdrCurrArray.set(SPACES);
		for (commIndex.set(1); !(isGT(commIndex,20)); commIndex.add(1)){
			initCommTotal5010();
		}
		return;
	}



protected void initCommTotal5010()
	{
		wsaaCommOrigamt[commIndex.toInt()].set(0);
	}

protected void calcTotalAmount5100()
	{
		try {
			start5100();
		}
		catch (GOTOException e){
		}
	}

protected void start5100()
	{
		if (isEQ(wsaaCommSacscode[commIndex.toInt()],SPACES)) {
			return;
		}
		wsaaTotalAgntComm.add(wsaaCommOrigamt[commIndex.toInt()]);
	}



protected void acumCommPayable6000()
	{
		/*START*/
		commIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(commIndex,wsaaCommTotal.length); commIndex.add(1)){
				if (isEQ(wsaaCommSacscode[commIndex.toInt()],SPACES)) {
					addCommission6100();
					break searchlabel1;
				}
				if (isEQ(wsaaCommSacscode[commIndex.toInt()],agpyagtIO.getSacscode())
				&& isEQ(wsaaCommSacstyp[commIndex.toInt()],agpyagtIO.getSacstyp())) {
					acumCommission6200();
					break searchlabel1;
				}
			}
		}
		/*EXIT*/
	}

protected void addCommission6100()
	{
		/*START*/
		if (isEQ(agpyagtIO.getGlsign(),"-")) {
			compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt,-1));
		}
		wsaaCommOrigamt[commIndex.toInt()].set(wsaaOrigamt);
		wsaaCommSacscode[commIndex.toInt()].set(agpyagtIO.getSacscode());
		wsaaCommSacstyp[commIndex.toInt()].set(agpyagtIO.getSacstyp());
		wsaaCommGlcode[commIndex.toInt()].set(agpyagtIO.getGlcode());
		/*EXIT*/
	}

protected void acumCommission6200()
	{
		/*START*/
		if (isEQ(agpyagtIO.getGlsign(),"-")) {
			compute(wsaaOrigamt, 2).set(mult(wsaaOrigamt,-1));
		}
		wsaaCommOrigamt[commIndex.toInt()].add(wsaaOrigamt);
		/*EXIT*/
	}

protected void getCurrencyRate7000()
	{
		
					start7000();
					reRead7120();
					findRate7150();
		
	}

protected void start7000()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(preqIO.getOrigccy());
	}

protected void reRead7120()
	{
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),"****")) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaLastOrigCcy.set(preqIO.getOrigccy());
		wsaaLedgerCcy.set(t3629rec.ledgcurr);
		wsaaNominalRate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaNominalRate,ZERO)
		|| isGT(wsaaX,7))) {
			findRate7150();
		}

		if (isEQ(wsaaNominalRate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				reRead7120();
			}
			else {
				conerrrec.params.set(itemIO.getParams());
				conerrrec.statuz.set("MRNF");
				systemError005();
			}
		}
		else {
			return;
		}
	}


protected void findRate7150()
	{
		if (isGTE(runparmrec.effdate,t3629rec.frmdate[wsaaX.toInt()])
		&& isLTE(runparmrec.effdate,t3629rec.todate[wsaaX.toInt()])) {
			wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
		}
		else {
			wsaaX.add(1);
		}
	}

protected void getPremiumTolerance9000()
	{
					para9000();
					read9010();
					nextr9000();
		
	}

protected void para9000()
	{
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(wsaaSacscode);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(wsaaSacstyp);
		acmvsacIO.setFunction(varcom.begnh);
		acmvsacIO.setFormat(acmvrec);
	}

protected void read9010()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),wsaaSacscode)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),wsaaSacstyp)) {
			return;
		}
		if (isEQ(wsaaChdrnum[chdrIndex.toInt()],acmvsacIO.getRdocnum())
		&& isEQ(wsaaCntcurr[chdrIndex.toInt()],acmvsacIO.getOrigcurr())) {
			/*CONTINUE_STMT*/
		}
		else {
			nextr9000();
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			if (isGT(acmvsacIO.getOrigamt(),wsaaTotalAgntComm)) {
				return;
			}
			if (isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())) {
				nextr9000();
			}
			wsaaDeductn.add(acmvsacIO.getOrigamt());
			wsaaTotalAgntComm.subtract(acmvsacIO.getOrigamt());
			acmvsacIO.setFrcdate(runparmrec.effdate);
			acmvsacIO.setRcamt(acmvsacIO.getOrigamt());
			acmvsacIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, acmvsacIO);
			if (isNE(acmvsacIO.getStatuz(),varcom.oK)) {
				conerrrec.params.set(acmvsacIO.getParams());
				conerrrec.statuz.set(acmvsacIO.getStatuz());
				databaseError006();
			}
			moveAcmvToLifa9500();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
		}
	}


protected void nextr9000()
	{
		acmvsacIO.setFunction(varcom.nextr);
		read9010();
	}

protected void getPremiumDeduction9a00()
	{
					para9a00();
					read9a10();
					next9a20();
		
	}

protected void para9a00()
	{
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(wsaaSacscode);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(wsaaSacstyp);
		acmvsacIO.setOrigcurr(SPACES);
		acmvsacIO.setFunction(varcom.begnh);
		acmvsacIO.setFormat(acmvrec);
	}

protected void read9a10()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),wsaaSacscode)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),wsaaSacstyp)
		|| isLTE(wsaaTotalAgntComm,0)) {
			return;
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			if (isGT(acmvsacIO.getOrigamt(),wsaaTotalAgntComm)) {
				next9a20();
			}
			wsaaDeductn.add(acmvsacIO.getOrigamt());
			wsaaTotalAgntComm.subtract(acmvsacIO.getOrigamt());
			acmvsacIO.setFrcdate(runparmrec.effdate);
			acmvsacIO.setRcamt(acmvsacIO.getOrigamt());
			acmvsacIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, acmvsacIO);
			if (isNE(acmvsacIO.getStatuz(),varcom.oK)) {
				conerrrec.params.set(acmvsacIO.getParams());
				conerrrec.statuz.set(acmvsacIO.getStatuz());
				databaseError006();
			}
			moveAcmvToLifa9500();
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				conerrrec.params.set(lifacmvrec.statuz);
				conerrrec.params.set(lifacmvrec.lifacmvRec);
				conerrrec.statuz.set(lifacmvrec.statuz);
				databaseError006();
			}
		}
	}



protected void next9a20()
	{
		acmvsacIO.setFunction(varcom.nextr);
		read9a10();
	}

protected void keepChdrCurr9100()
	{
		/*PARA*/
		chdrIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(chdrIndex,wsaaChdrCurr.length); chdrIndex.add(1)){
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()],SPACES)) {
					wsaaChdrnum[chdrIndex.toInt()].set(agpyagtIO.getRdocnum());
					wsaaCntcurr[chdrIndex.toInt()].set(agpyagtIO.getOrigcurr());
					break searchlabel1;
				}
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()],agpyagtIO.getRdocnum())
				&& isEQ(wsaaCntcurr[chdrIndex.toInt()],agpyagtIO.getOrigcurr())) {
					/*NEXT_SENTENCE*/
					break searchlabel1;
				}
			}
		}
		/*EXIT*/
	}

protected void moveAcmvToLifa9500()
	{
		para9500();
	}

protected void para9500()
	{
		lifacmvrec.frcdate.set(runparmrec.effdate);
		lifacmvrec.rcamt.set(acmvsacIO.getOrigamt());
		compute(lifacmvrec.origamt, 2).set(mult(acmvsacIO.getOrigamt(),(-1)));
		compute(lifacmvrec.acctamt, 2).set(mult(acmvsacIO.getOrigamt(),(-1)));
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rldgcoy.set(acmvsacIO.getRldgcoy());
		lifacmvrec.rldgacct.set(acmvsacIO.getRldgacct());
		lifacmvrec.sacscode.set(acmvsacIO.getSacscode());
		lifacmvrec.sacstyp.set(acmvsacIO.getSacstyp());
		lifacmvrec.genlcoy.set(acmvsacIO.getGenlcoy());
		lifacmvrec.glcode.set(acmvsacIO.getGlcode());
		lifacmvrec.genlcur.set(acmvsacIO.getGenlcur());
		lifacmvrec.glsign.set(acmvsacIO.getGlsign());
		lifacmvrec.origcurr.set(acmvsacIO.getOrigcurr());
		lifacmvrec.crate.set(acmvsacIO.getCrate());
		lifacmvrec.tranref.set(acmvsacIO.getTranref());
		lifacmvrec.rdocnum.set(acmvsacIO.getRdocnum());
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
	}

protected void getBalColltr9600()
	{
					para9600();
					read9610();
		
	}

protected void para9600()
	{
		wsaaCurColltr.set(ZERO);
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(runparmrec.company);
		acmvsacIO.setSacscode(t5645rec.sacscode08);
		acmvsacIO.setRldgacct(aglfagtIO.getAgntnum());
		acmvsacIO.setSacstyp(t5645rec.sacstype08);
		acmvsacIO.setOrigcurr(SPACES);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvsacIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");

		acmvsacIO.setFormat(acmvrec);
	}

protected void read9610()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),runparmrec.company)
		|| isNE(acmvsacIO.getSacscode(),t5645rec.sacscode08)
		|| isNE(acmvsacIO.getRldgacct(),aglfagtIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),t5645rec.sacstype08)) {
			return;
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			wsaaCurColltr.add(acmvsacIO.getAcctamt());
		}
		acmvsacIO.setFunction(varcom.nextr);
		read9610();
	}


protected void getCollateral9700()
	{
		/*PARA*/
		compute(wsaaTotalColltr, 3).setRounded(div(mult(wsaaTotalAgntComm,aglfagtIO.getTcolprct()),100));
		if (isNE(aglfagtIO.getTcolmax(),ZERO)) {
			if (isGT(aglfagtIO.getTcolmax(),wsaaCurColltr)) {
				compute(wsaaNewColltr, 2).set(add(wsaaCurColltr,wsaaTotalColltr));
				if (isGT(wsaaNewColltr,aglfagtIO.getTcolmax())) {
					compute(wsaaTotalColltr, 2).set(sub(aglfagtIO.getTcolmax(),wsaaCurColltr));
				}
			}
			else {
				wsaaTotalColltr.set(ZERO);
			}
		}
		/*EXIT*/
	}

protected void moveToLifa9800()
	{
		para9800();
	}

protected void para9800()
	{
		lifacmvrec.origamt.set(wsaaTotalWhtax);
		lifacmvrec.acctamt.set(wsaaTotalWhtax);
		lifacmvrec.rldgcoy.set(aglfagtIO.getAgntcoy());
		lifacmvrec.rldgacct.set(aglfagtIO.getAgntnum());
		lifacmvrec.rdocnum.set(aglfagtIO.getAgntnum());
		lifacmvrec.tranref.set(SPACES);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}	}

protected void moveToLifa9900()
	{
		para9900();
	}

protected void para9900()
	{
		lifacmvrec.origamt.set(wsaaTotalColltr);
		lifacmvrec.acctamt.set(wsaaTotalColltr);
		lifacmvrec.rldgcoy.set(aglfagtIO.getAgntcoy());
		lifacmvrec.rldgacct.set(aglfagtIO.getAgntnum());
		lifacmvrec.rdocnum.set(aglfagtIO.getAgntnum());
		lifacmvrec.tranref.set(SPACES);
		lifacmvrec.glcode.set(t5645rec.glmap08);
		lifacmvrec.glsign.set(t5645rec.sign08);
		lifacmvrec.sacscode.set(t5645rec.sacscode08);
		lifacmvrec.sacstyp.set(t5645rec.sacstype08);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
	}

protected void moveToLifa10000()
	{
		para10000();
	}

protected void para10000()
	{
		lifacmvrec.glcode.set(t5645rec.glmap01);
		lifacmvrec.glsign.set(t5645rec.sign01);
		lifacmvrec.sacscode.set(t5645rec.sacscode01);
		lifacmvrec.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origamt.set(wsaaLifaTotOrig);
		lifacmvrec.acctamt.set(wsaaLifaTotAcct);
		lifacmvrec.rldgcoy.set(aglfagtIO.getAgntcoy());
		lifacmvrec.rldgacct.set(aglfagtIO.getAgntnum());
		lifacmvrec.rdocnum.set(aglfagtIO.getAgntnum());
		lifacmvrec.rcamt.set(wsaaLifaTotOrig);
		lifacmvrec.frcdate.set(runparmrec.effdate);
		lifacmvrec.tranref.set(SPACES);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}	}

protected void moveToLifa10100()
	{
		param10100();
	}

protected void param10100()
	{
		lifacmvrec.glcode.set(t5645rec.glmap11);
		lifacmvrec.glsign.set(t5645rec.sign11);
		lifacmvrec.sacscode.set(t5645rec.sacscode11);
		lifacmvrec.sacstyp.set(t5645rec.sacstype11);
		lifacmvrec.contot.set(t5645rec.cnttot01);
		lifacmvrec.origamt.set(wsaaTdcchrg);
		lifacmvrec.acctamt.set(wsaaTdcchrg);
		lifacmvrec.rldgcoy.set(aglfagtIO.getAgntcoy());
		lifacmvrec.rldgacct.set(aglfagtIO.getAgntnum());
		lifacmvrec.rdocnum.set(aglfagtIO.getBankkey());
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.frcdate.set(runparmrec.effdate);
		lifacmvrec.tranref.set(SPACES);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		btchpr01Permission = FeaConfg.isFeatureExist(lifacmvrec.batccoy.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
		if(btchpr01Permission)
		{
			lifacmvrec.ind.set("G");
			lifacmvrec.prefix.set("AG");
			}
	}

protected void getBankCharge10100()
	{
		try {
			para10110();
		}
		catch (GOTOException e){
		}
	}

protected void para10110()
	{
		if (isNE(aglfIO.getPaymth(),"DC")) {
			wsaaTdcchrg.set(ZERO);
			return;
		}
		babrIO.setDataKey(SPACES);
		babrIO.setBankkey(aglfIO.getBankkey());
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(babrIO.getParams());
			databaseError006();
		}
		if (isEQ(babrIO.getFacthous01(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous02(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous03(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous04(),aglfIO.getFacthous())
		|| isEQ(babrIO.getFacthous05(),aglfIO.getFacthous())) {
			readTr39210200();
		}
		else {
			wsaaTdcchrg.set(ZERO);
		}
	}


protected void readTr39210200()
	{
		try {
			start10210();
			read10220();
		}
		catch (GOTOException e){
		}
	}

protected void start10210()
	{
		if (isEQ(babrIO.getFacthous01(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde01());
		}
		if (isEQ(babrIO.getFacthous02(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde02());
		}
		if (isEQ(babrIO.getFacthous03(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde03());
		}
		if (isEQ(babrIO.getFacthous04(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde04());
		}
		if (isEQ(babrIO.getFacthous05(),aglfIO.getFacthous())) {
			wsaaZchrgcde.set(babrIO.getZchrgcde05());
		}
		if (isEQ(wsaaZchrgcde,SPACES)) {
			return;
		}
	}


protected void read10220()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(runparmrec.company);
		itemIO.setItemtabl(tr392);
		itemIO.setItemitem(wsaaZchrgcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(itemIO.getParams());
			conerrrec.statuz.set(itemIO.getStatuz());
			databaseError006();
		}
		tr392rec.tr392Rec.set(itemIO.getGenarea());
		wsaaTdcchrg.set(tr392rec.tdcchrg);
	}
}
