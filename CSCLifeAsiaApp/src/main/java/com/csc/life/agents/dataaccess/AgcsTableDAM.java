package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgcsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:30
 * Class transformed from AGCS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgcsTableDAM extends AgcspfTableDAM {

	public AgcsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGCS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", PAYCLT"
		             + ", SACSCODE"
		             + ", SACSTYP"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CURRCODE, " +
		            "PAYCLT, " +
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "PAYAMT, " +
		            "EFFDATE, " +
		            "FRCDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "PAYCLT ASC, " +
		            "SACSCODE ASC, " +
		            "SACSTYP ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "PAYCLT DESC, " +
		            "SACSCODE DESC, " +
		            "SACSTYP DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               currcode,
                               payclt,
                               sacscode,
                               sacstyp,
                               payamt,
                               effdate,
                               frcdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(43);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getPayclt().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getAgntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(agntcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller40.setInternal(payclt.toInternal());
	nonKeyFiller50.setInternal(sacscode.toInternal());
	nonKeyFiller60.setInternal(sacstyp.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(90);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getCurrcode().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getPayamt().toInternal()
					+ getEffdate().toInternal()
					+ getFrcdate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, payamt);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, frcdate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}	
	public PackedDecimalData getPayamt() {
		return payamt;
	}
	public void setPayamt(Object what) {
		setPayamt(what, false);
	}
	public void setPayamt(Object what, boolean rounded) {
		if (rounded)
			payamt.setRounded(what);
		else
			payamt.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(Object what) {
		setFrcdate(what, false);
	}
	public void setFrcdate(Object what, boolean rounded) {
		if (rounded)
			frcdate.setRounded(what);
		else
			frcdate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		payclt.clear();
		sacscode.clear();
		sacstyp.clear();
		agntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		currcode.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		payamt.clear();
		effdate.clear();
		frcdate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}