package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl65ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl65 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl65.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL65");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl65ScreenVars sv = ScreenProgram.getScreenVars(Sjl65ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private Batckey wsaaBatckey = new Batckey();	
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private List<Hierpf> hierpfList = new ArrayList<>();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);

	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String T3692 = "T3692";
	private static final String TJL68 = "TJL68";
	private static final String TJL76 = "TJL76";
	private static final String TJL69 = "TJL69";
	private List<Descpf> tjl76List;
	private List<Descpf> tjl69List;
	private List<Descpf> t3692List;
	private String sjl65 = "Sjl65";

	public Pjl65() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl65", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		wsaaBatckey.set(wsspcomn.batchkey);	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		initSubfile1200();
		initialise1010();
		protect1020();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(sjl65, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
			}
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltype.set(agsdpf.getLevelclass());
				if("3".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SS");
				}
				else if ("2".equals(agsdpf.getLevelclass())) {
					wsaaroleCode.set("SB");
				}
				loadDesc();
			}
			if (null != agsdpf.getAgentclass()) {
				t3692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), T3692,
						agsdpf.getAgentclass().trim(),wsspcomn.language.toString());
				if(!t3692List.isEmpty()) {
					for (Descpf descItem : t3692List) {
						sv.agtdesc.set(descItem.getLongdesc());		
					}
				}
			}
			sv.levelno.set(agsdpf.getLevelno());
			wsaahierCode.set(agsdpf.getLevelno());
			/*To display active and history records of agentkey*/
			initialise1020(wsaaKey.toString());
		}
	}
	
	protected void initialise1020(String levelno) {
		
		hierpfList = hierpfDAO.getHierpfAgHis(levelno,"1");
		if(!hierpfList.isEmpty()) {
			for(Hierpf hier : hierpfList) {
				tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
						hier.getActivestatus().trim(),wsspcomn.language.toString());
				if(!tjl69List.isEmpty()) {
					for (Descpf descItem : tjl69List) {
						sv.status.set(descItem.getLongdesc());		
					}
				}
				sv.reasonreg.set(hier.getReasoncd());
				sv.resndetl.set(hier.getReasondtl());
				sv.regdate.set(hier.getEffectivedt());
				tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
					hier.getRegisclass().trim(),wsspcomn.language.toString());
				if(!tjl76List.isEmpty()) {
					for (Descpf descItem : tjl76List) {
						sv.regclass.set(descItem.getLongdesc());		
					}
				}
				scrnparams.function.set(Varcom.sadd);
				processScreen(sjl65, sv);
				if (isNE(scrnparams.statuz, Varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}	
			}
		}
	}
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	

	protected void protect1020() {
		sv.clntselOut[Varcom.pr.toInt()].set("Y");
		sv.cltnameOut[Varcom.pr.toInt()].set("Y");
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.levelnoOut[Varcom.pr.toInt()].set("Y");
		sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.leveldesOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
		sv.agtypeOut[Varcom.pr.toInt()].set("Y");
		sv.agtdescOut[Varcom.pr.toInt()].set("Y");
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(Varcom.srnch);
		processScreen(sjl65, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000() {
		wsspcomn.chdrCownnum.set(sv.levelno.trim());
		return;	
	}
	
	@Override
	protected void whereNext4000() {
		para4000();
		bypassStart4010();
		nextProgram4010();
	}
	
	protected void para4000()
	{
		if (isEQ(scrnparams.statuz, "KILL")) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			bypassStart4010();
		}
		    
		/*    Re-start the subfile.*/
		scrnparams.statuz.set(Varcom.oK);
		scrnparams.function.set(Varcom.sstrt);
		processScreen(sjl65, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void readSubfile4100(){
		scrnparams.function.set(Varcom.srdn);
		processScreen(sjl65, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void bypassStart4010()
	{
		if (isEQ(sv.slt, SPACES)) {
			while ( !(isNE(sv.slt, SPACES)
			|| isEQ(scrnparams.statuz, Varcom.endp))) {
				readSubfile4100();
			}			
		}
		
		/*  Nothing pressed at all, end working*/
		if ((isEQ(scrnparams.statuz, Varcom.endp)
		&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES))) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
	}
	
		
	protected void nextProgram4010(){
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}
		
}
