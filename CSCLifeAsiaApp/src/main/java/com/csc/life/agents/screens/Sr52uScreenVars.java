package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52U
 * @version 1.0 generated on 11/22/13 6:36 AM
 * @author CSC
 */
public class Sr52uScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(305);
	public FixedLengthStringData dataFields = new FixedLengthStringData(145).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrtypes = new FixedLengthStringData(6).isAPartOf(dataFields, 8);
	public FixedLengthStringData[] chdrtype = FLSArrayPartOfStructure(2, 3, chdrtypes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(chdrtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData chdrtype01 = DD.chdrtype.copy().isAPartOf(filler,0);
	public FixedLengthStringData chdrtype02 = DD.chdrtype.copy().isAPartOf(filler,3);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData descrips = new FixedLengthStringData(60).isAPartOf(dataFields, 25);
	public FixedLengthStringData[] descrip = FLSArrayPartOfStructure(2, 30, descrips, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(descrips, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01 = DD.descrip.copy().isAPartOf(filler1,0);
	public FixedLengthStringData descrip02 = DD.descrip.copy().isAPartOf(filler1,30);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,135);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 145);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrtypesErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] chdrtypeErr = FLSArrayPartOfStructure(2, 4, chdrtypesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(chdrtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData chdrtype01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData chdrtype02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData descripsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] descripErr = FLSArrayPartOfStructure(2, 4, descripsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(descripsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData descrip02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 185);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData chdrtypesOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(2, 12, chdrtypesOut, 0);
	public FixedLengthStringData[][] chdrtypeO = FLSDArrayPartOfArrayStructure(12, 1, chdrtypeOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(24).isAPartOf(chdrtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] chdrtype01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] chdrtype02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData descripsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(2, 12, descripsOut, 0);
	public FixedLengthStringData[][] descripO = FLSDArrayPartOfArrayStructure(12, 1, descripOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(24).isAPartOf(descripsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] descrip01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] descrip02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52uscreenWritten = new LongData(0);
	public LongData Sr52uprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52uScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrtype02Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrnum, cntcurr, rstate, register, cownnum, ownername, chdrtype01, descrip01, chdrtype02, descrip02};
		screenOutFields = new BaseData[][] {chdrnumOut, cntcurrOut, rstateOut, registerOut, cownnumOut, ownernameOut, chdrtype01Out, descrip01Out, chdrtype02Out, descrip02Out};
		screenErrFields = new BaseData[] {chdrnumErr, cntcurrErr, rstateErr, registerErr, cownnumErr, ownernameErr, chdrtype01Err, descrip01Err, chdrtype02Err, descrip02Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52uscreen.class;
		protectRecord = Sr52uprotect.class;
	}

}
