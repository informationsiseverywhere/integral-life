package com.csc.life.agents.batchprograms;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.util.List;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.smart400framework.dataaccess.model.Itempf;

import com.csc.smart400framework.parent.Bachmain;

import com.quipoz.framework.datatype.FixedLengthStringData;
import com.csc.fsu.agents.dataaccess.dao.AgentReportpfDAO;

import com.csc.fsu.agents.dataaccess.model.AgentReportpf;
import com.csc.life.agents.recordstructures.P5001par;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;

/**
 * 
 * @author vrathi4
 * 
 *         This process used to generate the report if in any case new agent
 *         does not full fill the conditions of qualification checked in TJL02.
 *
 */

public class Bjl04 extends Bachmain {

	private static final Logger LOGGER = LoggerFactory.getLogger(Bjl04.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Bjl04");

	protected AgentReportpfDAO agentReportDAO = getApplicationContext().getBean("agentReportpfDAO", AgentReportpfDAO.class);
	protected List<Itempf> itempfList = null; 
	protected List<Itempf> t6579List = null;
	private P5001par p5001par = new P5001par();
	private static final String AGENT_PROP_FILLE = "agentReport.properties";
	FileWriter csvWriter =null;
	public Bjl04() {
		super();
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {

		return wsaaProg;
	}
	@Override
	public void mainline(Object... parmArray) {
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);

		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.error("Bjl04",e);
		}
	}

	@Override
	protected void mainline100() {
		p5001par.parmRecord.set(conjobrec.params);
		writeCSVReport();
		updateExtractedFlag();
	}
	private void writeCSVReport() {		
		 List<AgentReportpf> agentReportpfList=null;
		agentReportpfList=agentReportDAO.fetchReportData();
			try{
				if(csvWriter ==null)
					createFile();
				InputStream input= getClass().getClassLoader().getResourceAsStream(AGENT_PROP_FILLE);
				Properties prop = new Properties(); 
				prop.load(input);			
				csvWriter.append(prop.getProperty("COMPANY"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("OLD_AGENT"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("NEW_AGENT"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("POLICY_NUMBER"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("COMMISSION_TYPE"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("EFFECTIVE_DATE"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("PROCESSING_DATE"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("USER"));
				csvWriter.append(",");
				csvWriter.append(prop.getProperty("REMARKS"));
				csvWriter.append("\n");			
				for (AgentReportpf rowData : agentReportpfList) {
					csvWriter.append(rowData.getCompany().toString());
					csvWriter.append(",");
					csvWriter.append(rowData.getOldAgent().toString());
					csvWriter.append(",");
					csvWriter.append(rowData.getNewAgent().toString());
					csvWriter.append(",");
					csvWriter.append("\"\t"+rowData.getChdrnum()+ "\"");
					csvWriter.append(",");
					if("Y".equals(p5001par.initflg.toString())) {
						csvWriter.append("Intial Commission");
						csvWriter.append("; ");
					} 
					if("Y".equals(p5001par.servflg.toString())){
						csvWriter.append("Servicing Commission");
						csvWriter.append("; ");
					}
					if("Y".equals(p5001par.rnwlflg.toString())) {
						csvWriter.append("Renewal Commission");
					}
					csvWriter.append(",");
					csvWriter.append(rowData.getEffectiveDate().toString());
					csvWriter.append(",");
					csvWriter.append(rowData.getProcessingDate().toString());
					csvWriter.append(",");
					csvWriter.append(rowData.getUsername());
					csvWriter.append(",");
					csvWriter.append(rowData.getRemarks());				    
					csvWriter.append("\n");
				}			
				csvWriter.flush();
				
			} catch (IOException   e) {						
				LOGGER.error("Error occurred while writing CSV file", e);
			}
		}

	private void createFile() throws IOException{
		
		File outputCsvDir = null;
		AppVars appVar = AppVars.getInstance();
		AppConfig appConfig = appVar.getAppConfig();
		outputCsvDir = FileSystems.getDefault().getPath(appConfig.getCSVOutput()).toFile(); 
		if (!outputCsvDir.exists()) {
			outputCsvDir.mkdirs();
		}
		String filePath = outputCsvDir.getPath() + File.separator + "L2AGENTCHG" + runparmrec.jobnum + ".csv";
		File file=FileSystems.getDefault().getPath(filePath).toFile();
		csvWriter = new FileWriter(file);
	}
	
private void updateExtractedFlag() {
	AgentReportpf agentReportpf=new AgentReportpf();
	agentReportpf.setOldAgent(Integer.parseInt(p5001par.agentfrom.toString()));
	agentReportpf.setNewAgent(Integer.parseInt(p5001par.agentto.toString()));
	agentReportDAO.updateExtractedFlag(agentReportpf);
}
}
