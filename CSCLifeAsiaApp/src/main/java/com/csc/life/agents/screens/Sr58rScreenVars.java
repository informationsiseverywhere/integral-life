package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

/**
 * Screen Variables for Sr58r
 * @version 1.0 Generated on Mon Jul 15 20:10:28 SGT 2013
 * @author CSC
 */
public class Sr58rScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(65).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9); 
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25); 
	public ZonedDecimalData minpcnt = DD.minpcnt.copyToZonedDecimal().isAPartOf(dataFields,55); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,60); 


	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 65);

	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData minpcntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);


	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea,93);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] minpcntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);


	//START OF SUBFILE AREA	 


	//START OF SFL FIELDS	 

 
	//SFL TOTAL ERROR DEC


	//SFL ERROR FIELD


	//SFL OUT IND FIELD


	//SFL PAGE

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);


	//public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	//public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable sr58rscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr58rscreenctlWritten = new LongData(0);
	public LongData Sr58rscreensflWritten = new LongData(0);
	public LongData Sr58rscreenWritten = new LongData(0);
	public LongData Sr58rwindowWritten = new LongData(0);
	public LongData Sr58rhideWritten = new LongData(0);
	public LongData Sr58rprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr58rScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmfrmOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmtoOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(minpcntOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});



	screenFields = new BaseData[] { company,item,itmfrm,itmto,longdesc,minpcnt,tabl };
	screenOutFields = new BaseData[][] { companyOut,itemOut,itmfrmOut,itmtoOut,longdescOut,minpcntOut,tablOut };
	screenErrFields = new BaseData[] {  companyErr,itemErr,itmfrmErr,itmtoErr,longdescErr,minpcntErr,tablErr  };
	screenDateFields = new BaseData[] { itmfrm,itmto  };
	screenDateErrFields = new BaseData[] { itmfrmErr,itmtoErr  };
	screenDateDispFields = new BaseData[] { itmfrmDisp,itmtoDisp   };

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr58rscreen.class;
	protectRecord = Sr58rprotect.class;

	}

 

}
