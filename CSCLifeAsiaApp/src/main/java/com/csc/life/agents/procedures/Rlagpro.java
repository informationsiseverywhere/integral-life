/*
 * File: Rlagpro.java
 * Date: 30 August 2009 2:12:54
 * Author: Quipoz Limited
 * 
 * Class transformed from RLAGPRO.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.recordstructures.Rlagprorec;
import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.life.statistics.dataaccess.MaprTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1698rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This subroutine is called to return Agent Production figures
*     for producing the Top Agency Report.
*
*   These files are read for:
*     AGST - Agency Statistical Accumulation
*     MAPR - Production Figures
*     MACF - agent type
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Rlagpro extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("RLAGPRO");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaMlagttyp = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaLeader = new FixedLengthStringData(1);
	private Validator leader = new Validator(wsaaLeader, "Y");
		/* WSAA-APP-REC */
	private FixedLengthStringData wsaaAppAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAppAgntnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAcctMnthyear = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaAcctMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaAcctMnthyear, 0).setUnsigned();
	private ZonedDecimalData wsaaAcctYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaAcctMnthyear, 2).setUnsigned();

	private FixedLengthStringData wsaaCalDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaCalYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaCalDate, 0).setUnsigned();
	private ZonedDecimalData wsaaCalMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalDate, 4).setUnsigned();
	private ZonedDecimalData wsaaCalDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalDate, 6).setUnsigned();
	private ZonedDecimalData wsaaBrkDaten = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaBrkDate = new FixedLengthStringData(8).isAPartOf(wsaaBrkDaten, 0, REDEFINE);
	private ZonedDecimalData wsaaBrkYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaBrkDate, 0).setUnsigned();
	private ZonedDecimalData wsaaBrkMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaBrkDate, 4).setUnsigned();

	private FixedLengthStringData wsaaFigures = new FixedLengthStringData(52);
	private ZonedDecimalData wsaaNumofmbr = new ZonedDecimalData(7, 0).isAPartOf(wsaaFigures, 0).setUnsigned();
	private ZonedDecimalData wsaaMlperpp01 = new ZonedDecimalData(15, 2).isAPartOf(wsaaFigures, 7);
	private ZonedDecimalData wsaaMldirpp01 = new ZonedDecimalData(15, 2).isAPartOf(wsaaFigures, 22);
	private ZonedDecimalData wsaaMlgrppp01 = new ZonedDecimalData(15, 2).isAPartOf(wsaaFigures, 37);
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String agstrec = "AGSTREC";
	private String aglfrec = "AGLFREC";
	private String maprrec = "MAPRREC";
	private String cltsrec = "CLTSREC";
	private String aglflnbrec = "AGLFLNB";
	private String agntlagrec = "AGNTLAGREC";
		/* TABLES */
	private String t1698 = "T1698";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agency Production*/
	private MaprTableDAM maprIO = new MaprTableDAM();
	private Rlagprorec rlagprorec = new Rlagprorec();
	private Syserrrec syserrrec = new Syserrrec();
	private T1698rec t1698rec = new T1698rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readItemFile1100, 
		exit1100, 
		callAgst2510, 
		nextr2510, 
		exit2510, 
		callMapr2520, 
		exit2520, 
		exit600
	}

	public Rlagpro() {
		super();
	}

public void mainline(Object... parmArray)
	{
		rlagprorec.rec = convertAndSetParam(rlagprorec.rec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main000()
	{
		/*MAIN*/
		initialise1000();
		processAgnt2100();
		/*EXIT-PROGRAM*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		rlagprorec.statuz.set(varcom.oK);
		initialize(wsaaFigures);
		initialize(rlagprorec.acctMnthyer);
		rlagprorec.prog.set(wsaaProg);
		wsaaCalDate.set(rlagprorec.datefrm);
		covrtAccMnthyer1100();
		if (isNE(wsaaAcctMnthyear,ZERO)) {
			rlagprorec.acctMnthfrm.set(wsaaAcctMnth);
			rlagprorec.acctYearfrm.set(wsaaAcctYear);
		}
		wsaaCalDate.set(rlagprorec.dateto);
		covrtAccMnthyer1100();
		if (isNE(wsaaAcctMnthyear,ZERO)) {
			rlagprorec.acctMnthto.set(wsaaAcctMnth);
			rlagprorec.acctYearto.set(wsaaAcctYear);
		}
	}

protected void covrtAccMnthyer1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1100();
				}
				case readItemFile1100: {
					readItemFile1100();
					searchForPeriod1100();
				}
				case exit1100: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1100()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(rlagprorec.agntcoy);
		itemIO.setItemtabl(t1698);
		itemIO.setItemitem(rlagprorec.agntcoy);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
	}

protected void readItemFile1100()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		|| isNE(itemIO.getValidflag(),1)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1698rec.t1698Rec.set(itemIO.getGenarea());
		wsaaAcctMnthyear.set(ZERO);
		wsaaIndex.set(1);
	}

protected void searchForPeriod1100()
	{
		while ( !(isGT(wsaaIndex,13)
		|| isNE(wsaaAcctYear,ZERO)
		|| isNE(wsaaAcctMnth,ZERO))) {
			searchTable1100();
		}
		
		if (isEQ(wsaaAcctYear,ZERO)
		&& isEQ(wsaaAcctMnth,ZERO)) {
			if (isNE(t1698rec.contitem,SPACES)) {
				itemIO.setItemitem(t1698rec.contitem);
				goTo(GotoLabel.readItemFile1100);
			}
		}
		goTo(GotoLabel.exit1100);
	}

protected void searchTable1100()
	{
		wsaaBrkDaten.set(t1698rec.frmdate[wsaaIndex.toInt()]);
		if (isEQ(wsaaBrkMnth,wsaaCalMnth)
		&& isEQ(wsaaBrkYr,wsaaCalYear)) {
			wsaaAcctYear.set(t1698rec.acctyr[wsaaIndex.toInt()]);
			wsaaAcctMnth.set(t1698rec.acctmnth[wsaaIndex.toInt()]);
		}
		else {
			wsaaIndex.add(1);
		}
	}

protected void callAglflnb1200()
	{
		/*CALL*/
		aglflnbIO.setAgntcoy(wsaaAppAgntcoy);
		aglflnbIO.setFormat(aglflnbrec);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callAgntlag1300()
	{
		/*READR-AGENT-FILE*/
		agntlagIO.setAgntcoy(wsaaAppAgntcoy);
		agntlagIO.setFormat(agntlagrec);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntlagIO.getStatuz());
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callClts1400()
	{
		readClientFile1410();
	}

protected void readClientFile1410()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsaaAppAgntcoy);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void leadershipStatus1500()
	{
		readAglf1500();
	}

protected void readAglf1500()
	{
		wsaaLeader.set("N");
		aglfIO.setRecKeyData(SPACES);
		aglfIO.setAgntcoy(wsaaAppAgntcoy);
		aglfIO.setAgntnum(wsaaAppAgntnum);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)
		&& isNE(aglfIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getReportag(),SPACES)) {
			wsaaLeader.set("Y");
		}
	}

protected void processAgnt2100()
	{
		control2100();
	}

protected void control2100()
	{
		initialize(wsaaFigures);
		wsaaAppAgntcoy.set(rlagprorec.agntcoy);
		wsaaAppAgntnum.set(rlagprorec.agntnum);
		aglflnbIO.setAgntcoy(wsaaAppAgntcoy);
		agntlagIO.setAgntcoy(wsaaAppAgntcoy);
		aglflnbIO.setAgntnum(wsaaAppAgntnum);
		agntlagIO.setAgntnum(wsaaAppAgntnum);
		callAglflnb1200();
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		callAgntlag1300();
		if (isEQ(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntlagIO.getStatuz());
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		cummFigures2300();
		rlagprorec.numofmbr.set(wsaaNumofmbr);
		rlagprorec.mlperpp01.set(wsaaMlperpp01);
		rlagprorec.mldirpp01.set(wsaaMldirpp01);
		rlagprorec.mlgrppp01.set(wsaaMlgrppp01);
	}

protected void cummFigures2300()
	{
		/*CUMM-CHDR*/
		if (isEQ(rlagprorec.numofmbrOut,"Y")) {
			leadershipStatus1500();
			extractAgst2510();
		}
		/*CUMM-FYP*/
		if (isEQ(rlagprorec.mldirpp01Out,"Y")
		|| isEQ(rlagprorec.mlgrppp01Out,"Y")) {
			extractMapr2520();
		}
		/*EXIT*/
	}

protected void extractAgst2510()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2510();
				}
				case callAgst2510: {
					callAgst2510();
					accumulate2510();
				}
				case nextr2510: {
					nextr2510();
				}
				case exit2510: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2510()
	{
		agstIO.setDataKey(SPACES);
		agstIO.setChdrcoy(wsaaAppAgntcoy);
		agstIO.setAgntnum(wsaaAppAgntnum);
		if (leader.isTrue()) {
			agstIO.setOvrdcat("O");
		}
		else {
			agstIO.setOvrdcat(SPACES);
		}
		agstIO.setAcctyr(rlagprorec.acctYearfrm);
		agstIO.setFunction(varcom.begn);
		agstIO.setFormat(agstrec);
	}

protected void callAgst2510()
	{
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)
		|| isNE(agstIO.getAgntnum(),wsaaAppAgntnum)) {
			goTo(GotoLabel.exit2510);
		}
		if ((leader.isTrue()
		&& isNE(agstIO.getOvrdcat(),"O"))
		|| !(isGTE(agstIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isLTE(agstIO.getAcctyr(),rlagprorec.acctYearto))) {
			goTo(GotoLabel.nextr2510);
		}
	}

protected void accumulate2510()
	{
		if (isEQ(agstIO.getAcctyr(),rlagprorec.acctYearfrm)) {
			if (isEQ(agstIO.getAcctyr(),rlagprorec.acctYearto)) {
				for (wsaaIndex.set(rlagprorec.acctMnthfrm); !(isGT(wsaaIndex,rlagprorec.acctMnthto)); wsaaIndex.add(1)){
					addNumofmbr2515();
				}
			}
			else {
				if (isLT(agstIO.getAcctyr(),rlagprorec.acctYearto)) {
					for (wsaaIndex.set(rlagprorec.acctMnthfrm); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
						addNumofmbr2515();
					}
				}
			}
		}
		else {
			if (isEQ(agstIO.getAcctyr(),rlagprorec.acctYearto)) {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex,rlagprorec.acctMnthto)); wsaaIndex.add(1)){
					addNumofmbr2515();
				}
			}
			else {
				for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
					addNumofmbr2515();
				}
			}
		}
	}

protected void nextr2510()
	{
		agstIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAgst2510);
	}

protected void addNumofmbr2515()
	{
		/*ADD*/
		wsaaNumofmbr.add(agstIO.getStcmth(wsaaIndex));
		/*EXIT*/
	}

protected void extractMapr2520()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2520();
				}
				case callMapr2520: {
					callMapr2520();
					nextMonth2520();
				}
				case exit2520: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2520()
	{
		wsaaMldirpp01.set(ZERO);
		wsaaMlperpp01.set(ZERO);
		wsaaMlgrppp01.set(ZERO);
		maprIO.setRecKeyData(SPACES);
		maprIO.setAgntcoy(wsaaAppAgntcoy);
		maprIO.setAgntnum(wsaaAppAgntnum);
		maprIO.setAcctyr(rlagprorec.acctYearfrm);
		maprIO.setMnth(rlagprorec.acctMnthfrm);
		maprIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		maprIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		maprIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");
		maprIO.setFormat(maprrec);
	}

protected void callMapr2520()
	{
		SmartFileCode.execute(appVars, maprIO);
		if (isNE(maprIO.getStatuz(),varcom.oK)
		&& isNE(maprIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(maprIO.getParams());
			fatalError600();
		}
		if (isEQ(maprIO.getStatuz(),varcom.endp)
		|| isNE(maprIO.getAgntcoy(),wsaaAppAgntcoy)
		|| isNE(maprIO.getAgntnum(),wsaaAppAgntnum)
		|| !(isGTE(maprIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isLTE(maprIO.getAcctyr(),rlagprorec.acctYearto))) {
			goTo(GotoLabel.exit2520);
		}
		/*ACCUMULATE*/
		if ((isEQ(maprIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isEQ(maprIO.getAcctyr(),rlagprorec.acctYearto)
		&& isGTE(maprIO.getMnth(),rlagprorec.acctMnthfrm)
		&& isLTE(maprIO.getMnth(),rlagprorec.acctMnthto))
		|| (isEQ(maprIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isLT(maprIO.getAcctyr(),rlagprorec.acctYearto)
		&& isGTE(maprIO.getMnth(),rlagprorec.acctMnthfrm))
		|| (isEQ(maprIO.getAcctyr(),rlagprorec.acctYearto)
		&& isGT(maprIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isLTE(maprIO.getMnth(),rlagprorec.acctMnthto))
		|| (isGT(maprIO.getAcctyr(),rlagprorec.acctYearfrm)
		&& isLT(maprIO.getAcctyr(),rlagprorec.acctYearto))) {
			wsaaMlperpp01.add(maprIO.getMlperpp(1));
			wsaaMldirpp01.add(maprIO.getMldirpp(1));
			wsaaMlgrppp01.add(maprIO.getMlgrppp(1));
		}
	}

protected void nextMonth2520()
	{
		maprIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callMapr2520);
	}

protected void fatalError600()
	{
		try {
			fatalErr600();
		}
		catch (GOTOException e){
		}
		finally{
			exit600();
		}
	}

protected void fatalErr600()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.exit600);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit600()
	{
		rlagprorec.statuz.set(syserrrec.statuz);
		rlagprorec.dataArea.set(syserrrec.params);
		exitProgram();
	}
}
