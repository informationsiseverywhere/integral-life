package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl61ScreenVars;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pjl61 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl61.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL61");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl61ScreenVars sv = ScreenProgram.getScreenVars(Sjl61ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private Batckey wsaaBatckey = new Batckey();	
	private Gensswrec gensswrec = new Gensswrec();

	private FixedLengthStringData wsaaSecProgs = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(8, 5, wsaaSecProgs, 0);
	
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	List<Map<String, Object>> agncypfList;
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";
	private static final String TJL76 = "TJL76";
	private static final String TJL77 = "TJL77";
	private static final String TJL69 = "TJL69";
	private static final String H093 = "h093";
	private List<Descpf> tjl76List;
	private List<Descpf> tjl69List;
	private List<Descpf> tjl77List;
	
	private String Sjl61 = "Sjl61";
	
	private FixedLengthStringData activestatus = new FixedLengthStringData(7);
	private FixedLengthStringData approvestatus = new FixedLengthStringData(10);
	private FixedLengthStringData regclass = new FixedLengthStringData(1);
	private FixedLengthStringData status = new FixedLengthStringData(30);
	private Boolean appflag = false;
	private Boolean actflag = false;
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0);

	
	public Pjl61() {
		super();
		screenVars = sv;
		new ScreenModel(Sjl61, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		wsaaBatckey.set(wsspcomn.batchkey);	
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		sv.slt.set(ZERO);
		sv.stattypeapp.set(SPACES);
		sv.stattype.set(SPACES);
		initSubfile1200();
		initialise1010();
		protect1020();
		/*To display all records of agentkey*/
		getSubfile1040();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(Sjl61, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
			}
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltype.set(agsdpf.getLevelclass());
				loadDesc();
			}
			if (null != agsdpf.getAgentclass()) {
				sv.agtype.set(agsdpf.getAgentclass());
			}
			if (null != agsdpf.getUserid()) {
				sv.userid.set(agsdpf.getUserid());
			}
			sv.levelno.set(agsdpf.getLevelno());
		}
	}
		
	protected void getSubfile1040() {
		
		wsaaroleCode.set("SR");
		wsaahierCode.set(sv.levelno.trim());
		
		if(isNE(sv.stattypeapp,SPACES))
			appflag = true;
		else
			appflag = false;
		
		if(isNE(sv.stattype,SPACES))
			actflag = true;
		else
			actflag = false;
		
		agncypfList = agncypfDAO.getAgncypfbyClient(wsspcomn.company.toString(), 
				wsaaKey.toString().trim(), appflag, sv.stattypeapp.toString(), actflag, sv.stattype.toString());
		try {
			if(agncypfList.size()>0)
			{	
				for (int iy = 0; iy < agncypfList.size(); iy++) {
					Map<String, Object> agncy = agncypfList.get(iy);
						sv.slt.set(SPACES);
						sv.agncynum.set(agncy.get("AGNCYNUM"));
						sv.regnum.set(agncy.get("REGNUM"));
						sv.startDate.set(agncy.get("SRDATE"));
						sv.dateend.set(agncy.get("ENDATE"));
						
						sv.aclntsel.set(agncy.get("CLNTNUM"));
						Clntpf clntpf1 = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), sv.aclntsel.toString().trim());
						Optional<Clntpf> op = Optional.ofNullable(clntpf1);
						if (op.isPresent()) {
						sv.acltname.set(clntpf1.getGivname() + " " + clntpf1.getSurname());
						}
						
						regclass.set(agncy.get("REGISCLASS"));
						tjl76List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL76,
								regclass.trim(),wsspcomn.language.toString());
				    	
				    	if (!tjl76List.isEmpty()){
				    		for (Descpf descItem : tjl76List) {
								sv.regclass.set(descItem.getLongdesc());		
							}
				    	}
						
						activestatus.set(agncy.get("ACTIVESTATUS"));
						tjl69List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL69,
								activestatus.trim(),wsspcomn.language.toString());
						if(!tjl69List.isEmpty()) {
							for (Descpf descItem : tjl69List) {
								activestatus.set(descItem.getLongdesc().trim());		
							}
						}
						
						approvestatus.set(agncy.get("APPROVESTATUS"));
						tjl77List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), TJL77,
								approvestatus.trim(),wsspcomn.language.toString());
						if(!tjl77List.isEmpty()) {
							for (Descpf descItem : tjl77List) {
								approvestatus.set(descItem.getLongdesc().trim());		
							}
						}
						
						status.set(activestatus + " , " + approvestatus);
						
						sv.status.set(status);
						sv.validflag.set(agncy.get("VALIDFLAG"));
						sv.regdate.set(agncy.get("EFFECTIVEDT"));
						sv.reasonmod.set(agncy.get("REASONCD"));
						sv.resndesc.set(agncy.get("REASONDTL"));
						
						scrnparams.function.set(Varcom.sadd);
						processScreen(Sjl61, sv);
						if (isNE(scrnparams.statuz, Varcom.oK)) {
							syserrrec.statuz.set(scrnparams.statuz);
							fatalError600();
						}
				}
			}
		
		}	catch (Exception ex) {
			LOGGER.error("Failed to get records from agncypf:", ex);
		}

	}

	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	

	protected void protect1020() {
		sv.clntselOut[Varcom.pr.toInt()].set("Y");
		sv.cltnameOut[Varcom.pr.toInt()].set("Y");
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.levelnoOut[Varcom.pr.toInt()].set("Y");
		sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.leveldesOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
		sv.agtypeOut[Varcom.pr.toInt()].set("Y");
	}
	

	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		

		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		wsspcomn.edterror.set(Varcom.oK);
		screenIo2010();
		
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(Varcom.sclr);
		screenIo9000();
		getSubfile1040();
		
		if(isEQ(sv.action1,"1"))
		{
			sv.slt.set("1");
			wsspcomn.chdrChdrnum.set(sv.action2);
		}
		
		else{
		if(isNE(sv.stattypeapp,SPACES))
		{
			appflag = true;
			wsspcomn.edterror.set("Y");
		}
		else
			appflag = false;
		if(isNE(sv.stattype,SPACES))
		{
			actflag = true;
			wsspcomn.edterror.set("Y");
		}
		else
			actflag = false;
		
		}
	}
	
	
	protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen(Sjl61, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
				&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	
	protected void screenIo2010()
	{	
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		screenEditSrnch();
	}
	
	protected void screenEditSrnch() {
		
		scrnparams.function.set(Varcom.srnch);
		processScreen(Sjl61, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000() {
		
		wsspcomn.chdrCownnum.set(sv.levelno.trim());
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		return;
	}
	
	@Override
	protected void whereNext4000()
	{
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
            return;
		}
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.nextprog.set(wsaaProg);
			sv.action1.set(ZERO);
			scrnparams.function.set(Varcom.supd);
			screenIo9000();
			scrnparams.function.set(Varcom.sclr);
			screenIo9000();
			getSubfile1040();
			wsspcomn.nextprog.set(scrnparams.scrname);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		para4000();
		bypassStart4010();
		nextProgram4080();
	}

	protected void para4000(){
		
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			sv.action1.set(SPACES);
			return;
		}
		scrnparams.statuz.set(Varcom.oK);
		scrnparams.function.set(Varcom.sstrt);
		processScreen(Sjl61, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	protected void bypassStart4010(){
		
		if (isNE(sv.action1, "1")) {
			while ( !(isNE(sv.action1,SPACES)
			|| isEQ(scrnparams.statuz,Varcom.endp))) {
				readSubfile4400();
			}
			
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)
		&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return;
		}
		if (isEQ(scrnparams.statuz,Varcom.endp)) {
			return;
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			sub1.set(wsspcomn.programPtr);
			sub2.set(0);
			for (int loopVar1 = 0; loopVar1 != 8; loopVar1 += 1) {
				saveProgramStack4200();
			}
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		if (isEQ(sv.action1, "1")) { 
			callGenssw4070();
		}
	}

	protected void callGenssw4070(){
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		gensswrec.function.set("A");
		wsspcomn.chdrCownnum.set(sv.levelno);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,Varcom.oK)
		&& isNE(gensswrec.statuz,Varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		if (isEQ(gensswrec.statuz,Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.errorCode.set(H093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return;
		}
		compute(sub1, 0).set(add(wsspcomn.programPtr,1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			loadProgramStack4300();
		}
		
		wsspcomn.programPtr.add(1);
	}

	protected void nextProgram4080(){
		wsspcomn.nextprog.set(wsaaProg);
		
		if(isNE(sv.action1, "1")) {
			wsspcomn.programPtr.add(1);
		}
	}

	protected void restoreProgram4100(){

		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
	}

	protected void saveProgramStack4200(){
		sub1.add(1);
		sub2.add(1);
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
	}

	protected void loadProgramStack4300(){
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub1.add(1);
		sub2.add(1);
	}

	protected void readSubfile4400(){
		scrnparams.function.set(Varcom.srdn);
		processScreen(Sjl61, sv);
		if (isNE(scrnparams.statuz,Varcom.oK)
		&& isNE(scrnparams.statuz,Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	

}
