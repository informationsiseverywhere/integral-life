/*
 * File: Tr663pt.java
 * Date: 30 August 2009 2:44:02
 * Author: Quipoz Limited
 * 
 * Class transformed from TR663PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Tr663rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR663.
*
*
*
***********************************************************************
* </pre>
*/
public class Tr663pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Agency Default Attributes                    SR663");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(38);
	private FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 12, FILLER).init("Default Profile Code:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(57);
	private FixedLengthStringData filler9 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 12, FILLER).init("Parameter for Agency:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 35);
	private FixedLengthStringData filler11 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine004, 37, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 43, FILLER).init("AC - Area Code");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(58);
	private FixedLengthStringData filler13 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 43, FILLER).init("SU - Sales Unit");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(54);
	private FixedLengthStringData filler15 = new FixedLengthStringData(43).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 43, FILLER).init("BR - Branch");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(26);
	private FixedLengthStringData filler17 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 12, FILLER).init("Default values");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(18);
	private FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 12, FILLER).init("Agency");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(37);
	private FixedLengthStringData filler21 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 12, FILLER).init("Start Date:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 27);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(61);
	private FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 12, FILLER).init("Tax ID:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine010, 27);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(28);
	private FixedLengthStringData filler25 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tr663rec tr663rec = new Tr663rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr663pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr663rec.tr663Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(tr663rec.zprofile);
		fieldNo006.set(tr663rec.zparam);
		datcon1rec.intDate.set(tr663rec.dteapp);
		callDatcon1100();
		fieldNo007.set(datcon1rec.extDate);
		fieldNo008.set(tr663rec.ztaxid);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
