/*
 * File: Cmpy001.java
 * Date: 29 August 2009 22:41:13
 * Author: Quipoz Limited
 * 
 * Class transformed from CMPY001.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.ChdrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5564rec;
import com.csc.life.agents.tablestructures.T5695rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*                  INITIAL COMMISSION PAYMENT EARNING
*
* Obtain the  commission  payment  rules  table  item  key from
* T5564, keyed  by  Payment  pattern,  Coverage/rider  code and
* effective date (hence use ITDM).
*
*      The entries   in    this   table   are   referenced   by
*           frequency  (read CHDRCMC to obtain frequency). When
*           the  applicable  frequency  is  found,  obtain  the
*           corresponding  item-key  for  this frequency.  This
*           item-key  is  used  to  access  T5695,  this  table
*           contains the method on how to apply this commission
*           payment. It is also a dated table.
*
* Calculate payment.
*
*      Obtain  the  paid-to  date  (PTD)  from   the   contract
*           header  (CHDRCMC).  Call DATCON3 with the effective
*           date  passed  (commission/risk  commencement date),
*           the  paid-to date  and the billing frequency passed
*           to give the number  of  receipts.  This   resulting
*           value is the line entry to table T5695.
*
* (A) Calculate Commission Amount Payable:-
*
*      (1) Calculate  commission amount in the previous  bands.
*           From  the  current line table entry minus 1, obtain
*           the lesser of:-
*
*           (a) the  total  initial commission times the sum of
*                the  %  commission (i.e. sum up to the current
*                line minus 1) or
*
*           (b) the % Max premium times the annual premium.
*
*      (2) Calculate  the  commission amount payable to the end
*           of  the current band,   from the current line table
*           entry, obtain the lesser of:-
*
*           (a) the  sum  of  the  % commission times the total
*                initial commission or
*
*           (b) the % Max premium times the annual premium.
*
*      (3) Commission payable is:-
*
*           - the value  of commission payable in all  previous
*             bands, plus
*           - the value of commission payable to the end of the
*                current frequency in the current band.
*                - ((2) - (1)) * (Curr-freq - from-freq + 1) /
*                                (To-freq   - from-freq + 1)
*               less,
*           - the amount paid to date.
*
*      Note - the frequencies  must be moved to numeric  fields
*             first as the table entries are alphanumeric.
*
*
* (B) Calculate Earned Commission:-
*
*      (1) Calculate  earned  commission in the previous period
*           from  the  current line table entry minus 1, obtain
*           the lesser of:-
*
*           (a) the  total  initial commission times the sum of
*                the  %  earned  commission (i.e. sum up to the
*                current line minus 1) or
*
*           (b) the % Max premium times the annual premium.
*
*      (2) Calculate  the  earned  commission to the end of the
*           current  period  from the current line table entry,
*           obtain the lesser of:-
*
*           (a) the  sum  of  the % earned commission times the
*                total initial commission or
*
*           (b) the % Max premium times the annual premium.
*
*      (3) Commission earned is:-
*
*           - the value  of commission earned  in all  previous
*             bands, plus
*           - the value of commission earned  to the end of the
*                current frequency in the current band.
*                - ((2) - (1)) * (Curr-freq - from-freq + 1) /
*                                (To-freq   - from-freq + 1)
*               less,
*           - the amount earned to date.
*
*
* Return the payment  amount  and  the  earned  amount  in  the
* linkage.
*
*****************************************************************
* </pre>
*/
public class Cmpy001 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "CMPY001";

	private FixedLengthStringData wsaaT5564Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMethod = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5564Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT5695Key = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaY = new PackedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaFreq = new PackedDecimalData(6, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaFreqFrom = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaFreqTo = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCommperc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaErndperc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaCommAmnt1 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommAmnt2 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommPrev = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaComm = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommPybl = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommErnd = new PackedDecimalData(13, 2);
		/* ERRORS */
	private String h027 = "H027";
	private String h029 = "H029";
		/* TABLES */
	private String t5564 = "T5564";
	private String t5695 = "T5695";
		/*Contract Header Commission Calcs.*/
	private ChdrcmcTableDAM chdrcmcIO = new ChdrcmcTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T5564rec t5564rec = new T5564rec();
	private T5695rec t5695rec = new T5695rec();
	private Varcom varcom = new Varcom();
	// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization start
	private ExternalisedRules er = new ExternalisedRules();
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String agntrec = "AGNTREC";
	// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization end
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		bypassDatcon31080, 
		exit1890, 
		sumComm2020, 
		continue2040, 
		exit2090, 
		sumComm3020, 
		continue3040, 
		exit3090, 
		seExit8090, 
		dbExit8190
	}

	public Cmpy001() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		obtainTables1000();
		// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization end
		if (AppVars.getInstance().getAppConfig().isVpmsEnable() && isEQ(comlinkrec.billfreq, "00")) {
			chdrcmc5000();
		}
		/*ILIFE-7965 : Starts*/		
		boolean gstOnCommFlag = FeaConfg.isFeatureExist(comlinkrec.chdrcoy.toString().trim(), "CTISS007", appVars, "IT");		
		if(gstOnCommFlag && AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMCMCPMT")
				&& er.isExternalized(comlinkrec.cnttype.toString(), comlinkrec.crtable.toString())){	
			readLifecmc();
			readChdrlnb();
			readrAgnt();
			if(isEQ(comlinkrec.ptdate,ZERO)){
				comlinkrec.ptdate.set(chdrcmcIO.getPtdate());
			}			
			//ILIFE-6077
			//comlinkrec.ptdate.add(1);
			comlinkrec.srcebus.set("");
			if(isEQ(comlinkrec.cnttype, SPACES)){
				comlinkrec.cnttype.set(chdrcmcIO.cnttype);
			}			
			comlinkrec.cltdob.set(lifecmcIO.cltdob);
			comlinkrec.anbccd.set(lifecmcIO.anbAtCcd);
			if (isEQ(comlinkrec.agentype, SPACES))
				comlinkrec.agentype.set("**");
			callProgram("VPMCMCPMT", comlinkrec.clnkallRec);
		}else{
			if (!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMCMCPMT")
					&& er.isExternalized(chdrcmcIO.cnttype.toString(), comlinkrec.crtable.toString())
					&& isEQ(comlinkrec.coverage, "01") && isEQ(comlinkrec.rider, "00"))) {
				calcCommamntPayable2000();
				calcEarnedComm3000();
			} else {
				readLifecmc();
				readChdrlnb();
				readrAgnt();
				if(isEQ(comlinkrec.ptdate,ZERO)){
					comlinkrec.ptdate.set(chdrcmcIO.getPtdate());
				}	
				//ILIFE-6077
				//comlinkrec.ptdate.add(1);
				comlinkrec.srcebus.set("");
				comlinkrec.cnttype.set(chdrlnbIO.cnttype);
				if(isEQ(comlinkrec.cnttype, SPACES)){
					comlinkrec.cnttype.set(chdrcmcIO.cnttype);
				}
				comlinkrec.cltdob.set(lifecmcIO.cltdob);
				comlinkrec.anbccd.set(lifecmcIO.anbAtCcd);
				if (isEQ(comlinkrec.agentype, SPACES))
					comlinkrec.agentype.set("**");
				callProgram("VPMCMCPMT", comlinkrec.clnkallRec);
			}
		}
		/*ILIFE-7965 : Ends*/				
	}

	protected void readLifecmc() {
		/* READR */
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife, SPACES)) {
			lifecmcIO.setJlife(ZERO);
		} else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifecmcIO.getParams());
			syserrrec.statuz.set(lifecmcIO.getStatuz());
			dbError8100();
		}
		/* EXIT */
	}

	protected void readChdrlnb() {
		/* READR */
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrlnbIO.setChdrnum(comlinkrec.chdrnum);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			dbError8100();
		}
		/* EXIT */
	}

	protected void readrAgnt() {
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			return;
		}
		agntIO.setFormat(agntrec);
		agntIO.setAgntcoy(chdrlnbIO.getAgntcoy());
		agntIO.setAgntpfx(chdrlnbIO.getAgntpfx());
		agntIO.setAgntnum(chdrlnbIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agntIO.getParams());
			syserrrec.statuz.set(agntIO.getStatuz());
			dbError8100();
		}
		comlinkrec.agentype.set(agntIO.agtype);
	}

	// ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization end
protected void exit090()
	{
		exitProgram();
	}

protected void obtainTables1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1000();
				}
				case bypassDatcon31080: {
					bypassDatcon31080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5564);
		wsaaMethod.set(comlinkrec.method);
		wsaaCrtable.set(comlinkrec.crtable);
		itdmIO.setItemitem(wsaaT5564Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5564)
		|| isNE(itdmIO.getItemitem(),wsaaT5564Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5564Key);
			syserrrec.statuz.set(h027);
			dbError8100();
		}
		t5564rec.t5564Rec.set(itdmIO.getGenarea());
		wsaaT5695Key.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)); wsaaIndex.add(1)){
			getItemkey1200();
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5695);
		itdmIO.setItemitem(wsaaT5695Key);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5695)
		|| isNE(itdmIO.getItemitem(),wsaaT5695Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(wsaaT5695Key);
			syserrrec.statuz.set(h029);
			dbError8100();
		}
		else {
			t5695rec.t5695Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(comlinkrec.billfreq,"00")
		|| isEQ(comlinkrec.billfreq,SPACES)) {
			wsaaFreq.set(0);
			goTo(GotoLabel.bypassDatcon31080);
		}
		if (isNE(comlinkrec.ptdate,ZERO)) {
			datcon3rec.intDate2.set(comlinkrec.ptdate);
		}
		else {
			chdrcmc5000();
			datcon3rec.intDate2.set(chdrcmcIO.getPtdate());
		}
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.frequency.set(comlinkrec.billfreq);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			systemError8000();
		}
		wsaaFreq.set(datcon3rec.freqFactor);
		if (isEQ(comlinkrec.function,"ADDC")) {
			compute(wsaaFreq, 5).set(add(datcon3rec.freqFactor,0.99999));
		}
	}

protected void bypassDatcon31080()
	{
		wsaaX.set(0);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,11)); wsaaIndex.add(1)){
			getT56951800();
		}
		/*EXIT*/
	}

protected void getItemkey1200()
	{
		/*PARA*/
		if (isEQ(t5564rec.freq[wsaaIndex.toInt()],comlinkrec.billfreq)) {
			wsaaT5695Key.set(t5564rec.itmkey[wsaaIndex.toInt()]);
		}
		/*EXIT*/
	}

protected void getT56951800()
	{
		try {
			para1800();
		}
		catch (GOTOException e){
		}
	}

protected void para1800()
	{
		if (isNE(t5695rec.freqFrom[wsaaIndex.toInt()],NUMERIC)
		|| isNE(t5695rec.freqTo[wsaaIndex.toInt()],NUMERIC)) {
			goTo(GotoLabel.exit1890);
		}
		wsaaFreqFrom.set(t5695rec.freqFrom[wsaaIndex.toInt()]);
		wsaaFreqTo.set(t5695rec.freqTo[wsaaIndex.toInt()]);
		if (isGTE(wsaaFreq,wsaaFreqFrom)
		&& isLTE(wsaaFreq,wsaaFreqTo)) {
			wsaaX.set(wsaaIndex);
		}
	}

protected void calcCommamntPayable2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para2000();
				}
				case sumComm2020: {
					sumComm2020();
				}
				case continue2040: {
					continue2040();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2000()
	{
		wsaaCommperc.set(0);
		wsaaErndperc.set(0);
		wsaaCommAmnt1.set(0);
		wsaaCommAmnt2.set(0);
		wsaaCommPrev.set(0);
		wsaaComm.set(0);
		wsaaCommPybl.set(0);
		wsaaCommErnd.set(0);
		if (isEQ(wsaaX,0)) {
			goTo(GotoLabel.exit2090);
		}
		wsaaIndex.set(0);
		compute(wsaaY, 0).set(sub(wsaaX,1));
	}

protected void sumComm2020()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex,wsaaY)) {
			goTo(GotoLabel.continue2040);
		}
		wsaaCommperc.add(t5695rec.cmrate[wsaaIndex.toInt()]);
		goTo(GotoLabel.sumComm2020);
	}

protected void continue2040()
	{
		compute(wsaaCommAmnt1, 3).setRounded(div(mult(comlinkrec.icommtot,wsaaCommperc),100));
		if (isNE(wsaaY,0)) {
			compute(wsaaCommAmnt2, 3).setRounded(div((mult(comlinkrec.annprem,t5695rec.premaxpc[wsaaY.toInt()])),100));
		}
		if (isLT(wsaaCommAmnt1,wsaaCommAmnt2)) {
			wsaaCommPrev.set(wsaaCommAmnt1);
		}
		else {
			wsaaCommPrev.set(wsaaCommAmnt2);
		}
		wsaaCommperc.add(t5695rec.cmrate[wsaaX.toInt()]);
		compute(wsaaCommAmnt1, 3).setRounded(div(mult(comlinkrec.icommtot,wsaaCommperc),100));
		compute(wsaaCommAmnt2, 3).setRounded(div((mult(comlinkrec.annprem,t5695rec.premaxpc[wsaaX.toInt()])),100));
		if (isLT(wsaaCommAmnt1,wsaaCommAmnt2)) {
			wsaaComm.set(wsaaCommAmnt1);
		}
		else {
			wsaaComm.set(wsaaCommAmnt2);
		}
		wsaaFreqTo.set(t5695rec.freqTo[wsaaIndex.toInt()]);
		wsaaFreqFrom.set(t5695rec.freqFrom[wsaaIndex.toInt()]);
		compute(wsaaCommPybl, 3).setRounded(sub(add(wsaaCommPrev,(div(mult((sub(wsaaComm,wsaaCommPrev)),(add(sub(wsaaFreq,wsaaFreqFrom),1))),(add(sub(wsaaFreqTo,wsaaFreqFrom),1))))),comlinkrec.icommpd));
		comlinkrec.payamnt.set(wsaaCommPybl);
	}

protected void calcEarnedComm3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para3000();
				}
				case sumComm3020: {
					sumComm3020();
				}
				case continue3040: {
					continue3040();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3000()
	{
		wsaaCommperc.set(0);
		wsaaErndperc.set(0);
		wsaaCommAmnt1.set(0);
		wsaaCommAmnt2.set(0);
		wsaaCommPrev.set(0);
		wsaaComm.set(0);
		wsaaCommPybl.set(0);
		wsaaCommErnd.set(0);
		if (isEQ(wsaaX,0)) {
			goTo(GotoLabel.exit3090);
		}
		wsaaIndex.set(0);
	}

protected void sumComm3020()
	{
		wsaaIndex.add(1);
		if (isGT(wsaaIndex,wsaaY)) {
			goTo(GotoLabel.continue3040);
		}
		wsaaErndperc.add(t5695rec.commenpc[wsaaIndex.toInt()]);
		goTo(GotoLabel.sumComm3020);
	}

protected void continue3040()
	{
		compute(wsaaCommAmnt1, 3).setRounded(div(mult(comlinkrec.icommtot,wsaaErndperc),100));
		if (isNE(wsaaY,0)) {
			compute(wsaaCommAmnt2, 3).setRounded(div((mult(comlinkrec.annprem,t5695rec.premaxpc[wsaaY.toInt()])),100));
		}
		if (isLT(wsaaCommAmnt1,wsaaCommAmnt2)) {
			wsaaCommPrev.set(wsaaCommAmnt1);
		}
		else {
			wsaaCommPrev.set(wsaaCommAmnt2);
		}
		wsaaErndperc.add(t5695rec.commenpc[wsaaX.toInt()]);
		compute(wsaaCommAmnt1, 3).setRounded(div(mult(comlinkrec.icommtot,wsaaErndperc),100));
		compute(wsaaCommAmnt2, 3).setRounded(div((mult(comlinkrec.annprem,t5695rec.premaxpc[wsaaX.toInt()])),100));
		if (isLT(wsaaCommAmnt1,wsaaCommAmnt2)) {
			wsaaComm.set(wsaaCommAmnt1);
		}
		else {
			wsaaComm.set(wsaaCommAmnt2);
		}
		compute(wsaaCommErnd, 3).setRounded(sub(add(wsaaCommPrev,(div(mult((sub(wsaaComm,wsaaCommPrev)),(add(sub(wsaaFreq,wsaaFreqFrom),1))),(add(sub(wsaaFreqTo,wsaaFreqFrom),1))))),comlinkrec.icommernd));
		comlinkrec.erndamt.set(wsaaCommErnd);
	}

protected void systemError8000()
	{
		try {
			se8000();
		}
		catch (GOTOException e){
		}
		finally{
			seExit8090();
		}
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit8090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		try {
			db8100();
		}
		catch (GOTOException e){
		}
		finally{
			dbExit8190();
		}
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit8190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void chdrcmc5000()
	{
		/*READ*/
		chdrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrcmcIO.setChdrnum(comlinkrec.chdrnum);
		chdrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrcmcIO);
		if (isNE(chdrcmcIO.getStatuz(),varcom.oK)
		&& isNE(chdrcmcIO.getStatuz(),varcom.mrnf)) {
			chdrcmcIO.setParams(chdrcmcIO.getParams());
			chdrcmcIO.setStatuz(chdrcmcIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}
}
