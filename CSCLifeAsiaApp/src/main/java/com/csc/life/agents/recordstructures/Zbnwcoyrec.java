package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:12
 * Description:
 * Copybook name: ZBNWCOYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwcoyrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwcoyRec = new FixedLengthStringData(431);
  	public FixedLengthStringData bnwcoyTrcde = new FixedLengthStringData(3).isAPartOf(bnwcoyRec, 0);
  	public FixedLengthStringData bnwcoySource = new FixedLengthStringData(10).isAPartOf(bnwcoyRec, 3);
  	public FixedLengthStringData bnwcoyRectype = new FixedLengthStringData(1).isAPartOf(bnwcoyRec, 13);
  	public FixedLengthStringData bnwcoyCompany = new FixedLengthStringData(3).isAPartOf(bnwcoyRec, 14);
  	public FixedLengthStringData bnwcoyName = new FixedLengthStringData(80).isAPartOf(bnwcoyRec, 17);
  	public FixedLengthStringData bnwcoyAddr01 = new FixedLengthStringData(60).isAPartOf(bnwcoyRec, 97);
  	public FixedLengthStringData bnwcoyAddr02 = new FixedLengthStringData(60).isAPartOf(bnwcoyRec, 157);
  	public FixedLengthStringData bnwcoyAddr03 = new FixedLengthStringData(60).isAPartOf(bnwcoyRec, 217);
  	public FixedLengthStringData bnwcoyAddr04 = new FixedLengthStringData(60).isAPartOf(bnwcoyRec, 277);
  	public FixedLengthStringData bnwcoyCity = new FixedLengthStringData(50).isAPartOf(bnwcoyRec, 337);
  	public FixedLengthStringData bnwcoyState = new FixedLengthStringData(2).isAPartOf(bnwcoyRec, 387);
  	public FixedLengthStringData bnwcoyZip = new FixedLengthStringData(10).isAPartOf(bnwcoyRec, 389);
  	public FixedLengthStringData bnwcoyCountry = new FixedLengthStringData(2).isAPartOf(bnwcoyRec, 399);
  	public FixedLengthStringData bnwcoyStrtdate = new FixedLengthStringData(10).isAPartOf(bnwcoyRec, 401);
  	public FixedLengthStringData bnwcoyChgdate = new FixedLengthStringData(10).isAPartOf(bnwcoyRec, 411);
  	public FixedLengthStringData bnwcoyProcdate = new FixedLengthStringData(10).isAPartOf(bnwcoyRec, 421);


	public void initialize() {
		COBOLFunctions.initialize(bnwcoyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwcoyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}