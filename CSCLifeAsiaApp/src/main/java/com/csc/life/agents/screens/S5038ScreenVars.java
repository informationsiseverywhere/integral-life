package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5038
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5038ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(92);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(58).isAPartOf(subfileArea, 0);
	public FixedLengthStringData contact = DD.contact.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData contactnam = DD.contactnam.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(8).isAPartOf(subfileArea, 58);
	public FixedLengthStringData contactErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData contactnamErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 66);
	public FixedLengthStringData[] contactOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] contactnamOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 90);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5038screensflWritten = new LongData(0);
	public LongData S5038screenctlWritten = new LongData(0);
	public LongData S5038screenWritten = new LongData(0);
	public LongData S5038protectWritten = new LongData(0);
	public GeneralTable s5038screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5038screensfl;
	}

	public S5038ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(contactOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {contact, contactnam};
		screenSflOutFields = new BaseData[][] {contactOut, contactnamOut};
		screenSflErrFields = new BaseData[] {contactErr, contactnamErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntsel, cltname, agnum, agtype, agtydesc};
		screenOutFields = new BaseData[][] {clntselOut, cltnameOut, agnumOut, agtypeOut, agtydescOut};
		screenErrFields = new BaseData[] {clntselErr, cltnameErr, agnumErr, agtypeErr, agtydescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5038screen.class;
		screenSflRecord = S5038screensfl.class;
		screenCtlRecord = S5038screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5038protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5038screenctl.lrec.pageSubfile);
	}
}
