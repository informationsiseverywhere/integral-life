/*
 * File: Br511.java
 * Date: 29 August 2009 22:13:05
 * Author: Quipoz Limited
 * 
 * Class transformed from BR511.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.dataaccess.MacfagtTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   MACFPF  - One time Conversion - Program 1
*
*                                                                     *
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br511 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR511");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevAgntnum = new FixedLengthStringData(8);
	private String macfagtrec = "MACFAGTREC";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*MACF CONVERSION*/
	private MacfagtTableDAM macfagtIO = new MacfagtTableDAM();

	public Br511() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		macfagtIO.setDataKey(SPACES);
		macfagtIO.setEffdate(ZERO);
		wsaaPrevAgntcoy.set(SPACES);
		wsaaPrevAgntnum.set(SPACES);
		macfagtIO.setFunction(varcom.begnh);
		macfagtIO.setFormat(macfagtrec);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, macfagtIO);
		if (isNE(macfagtIO.getStatuz(),varcom.oK)
		&& isNE(macfagtIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfagtIO.getStatuz());
			syserrrec.params.set(macfagtIO.getParams());
			fatalError600();
		}
		if (isEQ(macfagtIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(macfagtIO.getTranno(),ZERO)) {
			if (isNE(macfagtIO.getAgntnum(),wsaaPrevAgntnum)
			|| isNE(macfagtIO.getAgntcoy(),wsaaPrevAgntcoy)) {
				wsaaTranno.set(ZERO);
				wsaaTranno.add(1);
			}
			else {
				wsaaTranno.add(1);
			}
			macfagtIO.setTranno(wsaaTranno);
			wsaaPrevAgntcoy.set(macfagtIO.getAgntcoy());
			wsaaPrevAgntnum.set(macfagtIO.getAgntnum());
			macfagtIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfagtIO);
			if (isNE(macfagtIO.getStatuz(),varcom.oK)
			&& isNE(macfagtIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(macfagtIO.getStatuz());
				syserrrec.params.set(macfagtIO.getParams());
				fatalError600();
			}
		}
		macfagtIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
