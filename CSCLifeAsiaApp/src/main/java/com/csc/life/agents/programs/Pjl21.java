/*
 * File: Pjl21.java
 * Date: 03 February 2020
 * Author: vdivisala
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.nio.charset.StandardCharsets;

import com.csc.life.agents.screens.Sjl21ScreenVars;
import com.csc.life.agents.tablestructures.Tjl21rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * This table is to store the mapping for the common gateway details based on the Transaction.
 * Entries should be valid i.e. existing in the respective tables.
 * </pre>
 */
public class Pjl21 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL21");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private Tjl21rec tjl21rec = new Tjl21rec();
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl21ScreenVars sv = ScreenProgram.getScreenVars(Sjl21ScreenVars.class);
	private boolean isChange = false;

	public Pjl21() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl21", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	@Override
	protected void initialise1000() {
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		String wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		String longdesc = iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, wsaaItemlang);
		sv.longdesc.set(longdesc);

		Itempf itempf = itemDAO.findItemBySeq(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, "1", wsaaItemseq);
		if (itempf == null) {
			return;
		} else {
			tjl21rec.tjl21Rec.set(itempf.getGenareaString());
		}

		sv.cmgwdattyp.set(tjl21rec.cmgwdattyp);
		sv.cmgwreason.set(tjl21rec.cmgwreason);
		sv.cmgwcngcls.set(tjl21rec.cmgwcngcls);
	}
	
	@Override
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(Varcom.prot);
		}
	}

	@Override
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	@Override
	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.cmgwdattyp, tjl21rec.cmgwdattyp)) {
			tjl21rec.cmgwdattyp.set(sv.cmgwdattyp);
			isChange = true;
		}
		if (isNE(sv.cmgwreason, tjl21rec.cmgwreason)) {
			tjl21rec.cmgwreason.set(sv.cmgwreason);
			isChange = true;
		}
		if (isNE(sv.cmgwcngcls, tjl21rec.cmgwcngcls)) {
			tjl21rec.cmgwcngcls.set(sv.cmgwcngcls);
			isChange = true;
		}
		if (isChange) {
			com.csc.smart400framework.dataaccess.model.Itempf item = new com.csc.smart400framework.dataaccess.model.Itempf();
			item.setGenarea(tjl21rec.tjl21Rec.toString().getBytes(StandardCharsets.UTF_8));
			item.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
					.getGENAREAJString(tjl21rec.tjl21Rec.toString().getBytes(StandardCharsets.UTF_8), tjl21rec));
			item.setItempfx(wsaaItempfx);
			item.setItemcoy(wsaaItemcoy);
			item.setItemtabl(wsaaItemtabl);
			item.setItemitem(wsaaItemitem);
			item.setItemseq(wsaaItemseq);
			itemDAO.updateSmartTableItem(item);
		}
	}

	@Override
	protected void whereNext4000() {
		wsspcomn.programPtr.add(1);
	}
}