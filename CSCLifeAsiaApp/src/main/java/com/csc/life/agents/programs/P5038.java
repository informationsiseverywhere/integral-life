/*
 * File: P5038.java
 * Date: 29 August 2009 23:59:13
 * Author: Quipoz Limited
 * 
 * Class transformed from P5038.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AgbnTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.S5038ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                     BROKER CONTACT NAMES.
*                     =====================
* Initialise
* ----------
*
*  Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*  information. Load the screen from the record read.
*
*  Up to nine contacts are allowed per agency, and no more, so a
*  fixed subfile  size  can be used. Initialise the subfile with
*  blank  records  (INIT  function), and then update any records
*  with  existing  details  as required. Details are loaded from
*  the beneficiaries file (AGBN). For each record read:
*
*       - use the  next  available  relative  record  number  to
*            retrieve  a  blank subfile record (i.e. starting at
*            one).
*
*       - look up the  client name (CLTS), formatting the client
*            name for confirmation.
*
*       - hold the client number in a "hidden" field to indicate
*            that the record was  loaded  from the database (and
*            so is not new  -  required  during  validation  and
*            updating).
*
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  =  'I')  protect  the entire
*  screen prior to output.
*
*  If the  'KILL'  function  key was pressed or in enquiry mode,
*  skip all the validation.
*
*  Validate each changed  subfile  record according to the rules
*  defined in the  screen and field help. Leave duplicate client
*  number  validation  till  later.
*
* Updating
* --------
*
*  If the 'KILL' function  key  was  pressed,  or  if in enquiry
*  mode, skip the updating only.
*
*  Add, modify  or  delete  a  contact  record  depending  on if
*  details have  been  keyed in, blanked out or changed.  Ignore
*  any  subfile  records  which were not validated (hidden field
*  flag).  Note,  that  if  a  client  number is changed (can be
*  worked out  from  the hidden field), the original record must
*  be deleted and a new one added - client no is in the key.
*
*  An  attempt  to  enter  the  same  client number twice can be
*  picked  up  here  (DUPR returned from I/O module).  Highlight
*  a duplicate as an error.
*
*****************************************************************
* </pre>
*/
public class P5038 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5038");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaSubCount = new ZonedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(3);
	private Validator wsaaAdd = new Validator(wsaaUpdateFlag, "ADD");
	private Validator wsaaDelete = new Validator(wsaaUpdateFlag, "DEL");
	private Validator wsaaChange = new Validator(wsaaUpdateFlag, "CHG");

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(531);
	private FixedLengthStringData[] wsbbContact = FLSArrayPartOfStructure(9, 8, wsbbStackArray, 0);
	private FixedLengthStringData[] wsbbContactname = FLSArrayPartOfStructure(9, 50, wsbbStackArray, 72);
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(9, 1, wsbbStackArray, 522);
	private ZonedDecimalData wsaaKeySave = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaSaveContact = new FixedLengthStringData(8);
		/* ERRORS */
	private String e655 = "E655";
	private String e048 = "E048";
		/* TABLES */
	private String t3692 = "T3692";
	private String agbnrec = "AGBNREC";
		/*LIFE AGENT / BROKER CONTACT NAMES*/
	private AgbnTableDAM agbnIO = new AgbnTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5038ScreenVars sv = ScreenProgram.getScreenVars( S5038ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1190, 
		exit1290, 
		exit1390, 
		preExit, 
		exit2090, 
		updateErrorIndicators2130, 
		readNextSubfile2185, 
		exit2390, 
		exit3090, 
		exit3190, 
		exit3290, 
		exit3390
	}

	public P5038() {
		super();
		screenVars = sv;
		new ScreenModel("S5038", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retrvAglflnb1020();
		readrAgentFile1030();
		clientDets1040();
		agentType1060();
		subfileProcess1050();
	}

protected void initialise1010()
	{
		wsaaKeySave.set(ZERO);
		wsaaRrn.set(ZERO);
		wsaaSaveContact.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sinit);
		processScreen("S5038", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void retrvAglflnb1020()
	{
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void readrAgentFile1030()
	{
		sv.agnum.set(aglfIO.getAgntnum());
		agntlagIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

protected void clientDets1040()
	{
		sv.clntsel.set(agntlagIO.getClntnum());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void agentType1060()
	{
		sv.agtype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntlagIO.getAgtype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.agtydesc.set(descIO.getLongdesc());
	}

protected void subfileProcess1050()
	{
		initWsSubfile1100();
		loadWsSubfile1200();
		wsSubfileToScreen1300();
		/*EXIT*/
	}

protected void initWsSubfile1100()
	{
		try {
			zeroiseSubfile1110();
		}
		catch (GOTOException e){
		}
	}

protected void zeroiseSubfile1110()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub,9)); wsaaSub.add(1)){
			initToZero1120();
		}
		goTo(GotoLabel.exit1190);
	}

protected void initToZero1120()
	{
		wsbbContact[wsaaSub.toInt()].set(SPACES);
		wsbbContactname[wsaaSub.toInt()].set(SPACES);
		wsbbChange[wsaaSub.toInt()].set("N");
	}

protected void loadWsSubfile1200()
	{
		try {
			setAgbnKey1210();
		}
		catch (GOTOException e){
		}
	}

protected void setAgbnKey1210()
	{
		agbnIO.setDataKey(SPACES);
		agbnIO.setAgntcoy(wsspcomn.company);
		agbnIO.setAgntnum(aglfIO.getAgntnum());
		agbnIO.setContact(SPACES);
		/*READ-AGBN*/
		agbnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agbnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agbnIO.setFitKeysSearch("AGNTNUM");
		wsaaSub.set(ZERO);
		wsaaSubCount.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,9)); wsaaSub.add(1)){
			loadWsSubfile1220();
		}
		goTo(GotoLabel.exit1290);
	}

protected void loadWsSubfile1220()
	{
		callAgbnio1400();
		if (isNE(agbnIO.getStatuz(),varcom.endp)) {
			readClient1230();
		}
		else {
			wsaaSub.set(9);
		}
	}

protected void readClient1230()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agbnIO.getContact());
		cltsio1500();
		plainname();
		wsbbContact[wsaaSub.toInt()].set(agbnIO.getContact());
		wsbbContactname[wsaaSub.toInt()].set(wsspcomn.longconfname);
		wsaaSubCount.add(1);
		agbnIO.setFunction(varcom.nextr);
	}

protected void wsSubfileToScreen1300()
	{
		try {
			moveToScreen1310();
		}
		catch (GOTOException e){
		}
	}

protected void moveToScreen1310()
	{
		scrnparams.function.set(varcom.sclr);
		callScreenIo1600();
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,9)); wsaaSub.add(1)){
			subfileToScreen1320();
		}
		goTo(GotoLabel.exit1390);
	}

protected void subfileToScreen1320()
	{
		sv.contact.set(wsbbContact[wsaaSub.toInt()]);
		sv.contactnam.set(wsbbContactname[wsaaSub.toInt()]);
		scrnparams.subfileRrn.set(wsaaSub);
		scrnparams.function.set(varcom.sadd);
		callScreenIo1600();
	}

protected void callAgbnio1400()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, agbnIO);
		if (isNE(agbnIO.getStatuz(),varcom.oK)
		&& isNE(agbnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agbnIO.getParams());
			fatalError600();
		}
		if ((isNE(agbnIO.getAgntcoy(),aglfIO.getAgntcoy()))
		|| (isNE(agbnIO.getAgntnum(),aglfIO.getAgntnum()))) {
			agbnIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void cltsio1500()
	{
		/*CLTSIO*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callScreenIo1600()
	{
		/*CALL*/
		processScreen("S5038", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			scrnparams.function.set(varcom.prot);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			screenStatuz2020();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void screenStatuz2020()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			goTo(GotoLabel.exit2090);
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5038", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					clientChange2120();
				}
				case updateErrorIndicators2130: {
					updateErrorIndicators2130();
				}
				case readNextSubfile2185: {
					readNextSubfile2185();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void clientChange2120()
	{
		if ((isEQ(sv.contact,SPACES))) {
			sv.contactnam.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2130);
		}
		wsaaKeySave.set(scrnparams.subfileRrn);
		wsaaSaveContact.set(sv.contact);
		for (wsaaRrn.set(1); !(isGT(wsaaRrn,wsaaKeySave)); wsaaRrn.add(1)){
			checkDuplicate2300();
		}
		if (isEQ(wsaaKeySave,ZERO)) {
			sv.contactErr.set(e048);
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(sv.contact);
		cltsio2200();
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			sv.contactErr.set(e655);
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
		}
		sv.contactnam.set(wsspcomn.longconfname);
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void updateErrorIndicators2130()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("S5038", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.readNextSubfile2185);
		}
		wsbbChange[scrnparams.subfileRrn.toInt()].set("Y");
	}

protected void readNextSubfile2185()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5038", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void cltsio2200()
	{
		/*CLTSIO*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkDuplicate2300()
	{
		try {
			go2310();
		}
		catch (GOTOException e){
		}
	}

protected void go2310()
	{
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S5038", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(wsaaRrn,wsaaKeySave)) {
			goTo(GotoLabel.exit2390);
		}
		if (isEQ(sv.contact,wsaaSaveContact)) {
			wsaaKeySave.set(ZERO);
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			goTo(GotoLabel.exit3090);
		}
		agbnIO.setAgntcoy(wsspcomn.company);
		agbnIO.setAgntnum(aglfIO.getAgntnum());
		for (wsaaSub.set(1); !(isGT(wsaaSub,9)); wsaaSub.add(1)){
			update3100();
		}
	}

protected void update3100()
	{
		try {
			update3120();
		}
		catch (GOTOException e){
		}
	}

protected void update3120()
	{
		if (isNE(wsbbChange[wsaaSub.toInt()],"Y")) {
			goTo(GotoLabel.exit3190);
		}
		checkChange3200();
		updateRecord3300();
	}

protected void checkChange3200()
	{
		try {
			checkChange3210();
		}
		catch (GOTOException e){
		}
	}

protected void checkChange3210()
	{
		wsaaUpdateFlag.set(SPACES);
		scrnparams.subfileRrn.set(wsaaSub);
		scrnparams.function.set(varcom.sread);
		callScreenIo1600();
		if (isEQ(sv.contact,SPACES)
		&& isLTE(wsaaSub,wsaaSubCount)) {
			wsaaUpdateFlag.set("DEL");
			goTo(GotoLabel.exit3290);
		}
		if ((isNE(sv.contact,wsbbContact[wsaaSub.toInt()]))
		&& (isGT(wsaaSub,wsaaSubCount))) {
			wsaaUpdateFlag.set("ADD");
			goTo(GotoLabel.exit3290);
		}
		if ((isNE(sv.contact,wsbbContact[wsaaSub.toInt()]))
		&& (isLTE(wsaaSub,wsaaSubCount))) {
			wsaaUpdateFlag.set("CHG");
		}
		if (isEQ(sv.contact,SPACES)
		&& isEQ(wsbbContact[wsaaSub.toInt()],SPACES)) {
			wsaaUpdateFlag.set(SPACES);
			goTo(GotoLabel.exit3290);
		}
	}

protected void updateRecord3300()
	{
		try {
			para3310();
			updateAdd3320();
		}
		catch (GOTOException e){
		}
	}

protected void para3310()
	{
		if (isEQ(wsaaUpdateFlag,SPACES)) {
			goTo(GotoLabel.exit3390);
		}
		if (wsaaDelete.isTrue()) {
			agbnIO.setContact(wsbbContact[wsaaSub.toInt()]);
			sv.contactnam.set(SPACES);
			agbnIO.setFunction(varcom.delet);
			callAgbnio3400();
			goTo(GotoLabel.exit3390);
		}
		if (wsaaAdd.isTrue()) {
			updateAdd3320();
			goTo(GotoLabel.exit3390);
		}
		if (wsaaChange.isTrue()) {
			agbnIO.setContact(wsbbContact[wsaaSub.toInt()]);
			agbnIO.setFunction(varcom.delet);
			callAgbnio3400();
		}
	}

protected void updateAdd3320()
	{
		if (isEQ(sv.contact,SPACES)) {
			goTo(GotoLabel.exit3390);
		}
		agbnIO.setContact(sv.contact);
		agbnIO.setFunction(varcom.readr);
		agbnIO.setFormat(agbnrec);
		SmartFileCode.execute(appVars, agbnIO);
		if ((isNE(agbnIO.getStatuz(),varcom.oK))
		&& (isNE(agbnIO.getStatuz(),varcom.mrnf))) {
			syserrrec.params.set(agbnIO.getParams());
			syserrrec.statuz.set(agbnIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agbnIO.getStatuz(),varcom.mrnf)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit3390);
		}
		agbnIO.setContact(sv.contact);
		agbnIO.setTermid(varcom.vrcmTermid);
		agbnIO.setUser(varcom.vrcmUser);
		agbnIO.setTransactionTime(varcom.vrcmTime);
		agbnIO.setTransactionDate(varcom.vrcmDate);
		agbnIO.setFunction(varcom.writr);
		callAgbnio3400();
	}

protected void callAgbnio3400()
	{
		/*CALL*/
		agbnIO.setFormat(agbnrec);
		SmartFileCode.execute(appVars, agbnIO);
		if (isNE(agbnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agbnIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
