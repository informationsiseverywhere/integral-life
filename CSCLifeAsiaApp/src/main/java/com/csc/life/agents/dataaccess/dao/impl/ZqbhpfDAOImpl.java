package com.csc.life.agents.dataaccess.dao.impl;

import com.csc.businessobjects.programs.Session;
import com.csc.life.agents.dataaccess.dao.ZqbhpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.GenericDAOImpl;
import com.csc.smart400framework.dataaccess.model.Zqbhpf;

public class ZqbhpfDAOImpl extends GenericDAOImpl<Zqbhpf,Long> implements ZqbhpfDAO
{
	public void save(Zqbhpf zqbh)
	{
		getSession().save(zqbh);
	}
}
