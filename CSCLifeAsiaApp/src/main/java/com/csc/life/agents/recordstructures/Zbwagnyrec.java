package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:16
 * Description:
 * Copybook name: ZBWAGNYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbwagnyrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bwagnyRec = new FixedLengthStringData(52);
  	public FixedLengthStringData signonCompany = new FixedLengthStringData(1).isAPartOf(bwagnyRec, 0);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(bwagnyRec, 1);
  	public FixedLengthStringData function = new FixedLengthStringData(3).isAPartOf(bwagnyRec, 2);
  	public FixedLengthStringData input = new FixedLengthStringData(8).isAPartOf(bwagnyRec, 5);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(bwagnyRec, 13);
  	public FixedLengthStringData agency = new FixedLengthStringData(5).isAPartOf(bwagnyRec, 17);
  	public FixedLengthStringData agencyDesc = new FixedLengthStringData(30).isAPartOf(bwagnyRec, 22);


	public void initialize() {
		COBOLFunctions.initialize(bwagnyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bwagnyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}