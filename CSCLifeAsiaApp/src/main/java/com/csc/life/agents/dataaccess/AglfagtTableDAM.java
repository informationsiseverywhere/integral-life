package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AglfagtTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:35
 * Class transformed from AGLFAGT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AglfagtTableDAM extends AglfpfTableDAM {

	public AglfagtTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGLFAGT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM"
		             + ", CURRCODE";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "DTEAPP, " +
		            "DTETRM, " +
		            "CURRCODE, " +
		            "MINSTA, " +
		            "PAYCLT, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "ZRORCODE, " +
		            "EFFDATE, " +
		            "TAXMETH, " +
		            "TAXCDE, " +
		            "IRDNO, " +
		            "TAGSUSIND, " +
		            "TLAGLICNO, " +
		            "TLICEXPDT, " +
		            "TCOLPRCT, " +
		            "TCOLMAX, " +
		            "TSALESUNT, " +
		            "PRDAGENT, " +
		            "AGCCQIND, " +
		          //TMLII-281 AG-01-002
		            "ZRECRUIT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "CURRCODE ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "CURRCODE DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               dteapp,
                               dtetrm,
                               currcode,
                               minsta,
                               payclt,
                               facthous,
                               bankkey,
                               bankacckey,
                               zrorcode,
                               effdate,
                               taxmeth,
                               taxcde,
                               irdno,
                               tagsusind,
                               tlaglicno,
                               tlicexpdt,
                               tcolprct,
                               tcolmax,
                               tsalesunt,
                               prdagent,
                               agccqind,
                               //TMLII-281 AG-01-002
                               zrecruit,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCurrcode().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, currcode);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(agntcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller50.setInternal(currcode.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(186);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ getDteapp().toInternal()
					+ getDtetrm().toInternal()
					+ nonKeyFiller50.toInternal()
					+ getMinsta().toInternal()
					+ getPayclt().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getZrorcode().toInternal()
					+ getEffdate().toInternal()
					+ getTaxmeth().toInternal()
					+ getTaxcde().toInternal()
					+ getIrdno().toInternal()
					+ getTagsusind().toInternal()
					+ getTlaglicno().toInternal()
					+ getTlicexpdt().toInternal()
					+ getTcolprct().toInternal()
					+ getTcolmax().toInternal()
					+ getTsalesunt().toInternal()
					+ getPrdagent().toInternal()
					+ getAgccqind().toInternal()
					//TMLII-281 AG-01-002
					+ getZrecruit().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, dteapp);
			what = ExternalData.chop(what, dtetrm);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, minsta);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, zrorcode);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, taxmeth);
			what = ExternalData.chop(what, taxcde);
			what = ExternalData.chop(what, irdno);
			what = ExternalData.chop(what, tagsusind);
			what = ExternalData.chop(what, tlaglicno);
			what = ExternalData.chop(what, tlicexpdt);
			what = ExternalData.chop(what, tcolprct);
			what = ExternalData.chop(what, tcolmax);
			what = ExternalData.chop(what, tsalesunt);
			what = ExternalData.chop(what, prdagent);
			what = ExternalData.chop(what, agccqind);
			//TMLII-281 AG-01-002
			what = ExternalData.chop(what, zrecruit);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getCurrcode() {
		return currcode;
	}
	public void setCurrcode(Object what) {
		currcode.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getDteapp() {
		return dteapp;
	}
	public void setDteapp(Object what) {
		setDteapp(what, false);
	}
	public void setDteapp(Object what, boolean rounded) {
		if (rounded)
			dteapp.setRounded(what);
		else
			dteapp.set(what);
	}	
	public PackedDecimalData getDtetrm() {
		return dtetrm;
	}
	public void setDtetrm(Object what) {
		setDtetrm(what, false);
	}
	public void setDtetrm(Object what, boolean rounded) {
		if (rounded)
			dtetrm.setRounded(what);
		else
			dtetrm.set(what);
	}	
	public PackedDecimalData getMinsta() {
		return minsta;
	}
	public void setMinsta(Object what) {
		setMinsta(what, false);
	}
	public void setMinsta(Object what, boolean rounded) {
		if (rounded)
			minsta.setRounded(what);
		else
			minsta.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getZrorcode() {
		return zrorcode;
	}
	public void setZrorcode(Object what) {
		zrorcode.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getTaxmeth() {
		return taxmeth;
	}
	public void setTaxmeth(Object what) {
		taxmeth.set(what);
	}	
	public FixedLengthStringData getTaxcde() {
		return taxcde;
	}
	public void setTaxcde(Object what) {
		taxcde.set(what);
	}	
	public FixedLengthStringData getIrdno() {
		return irdno;
	}
	public void setIrdno(Object what) {
		irdno.set(what);
	}	
	public FixedLengthStringData getTagsusind() {
		return tagsusind;
	}
	public void setTagsusind(Object what) {
		tagsusind.set(what);
	}	
	public FixedLengthStringData getTlaglicno() {
		return tlaglicno;
	}
	public void setTlaglicno(Object what) {
		tlaglicno.set(what);
	}	
	public PackedDecimalData getTlicexpdt() {
		return tlicexpdt;
	}
	public void setTlicexpdt(Object what) {
		setTlicexpdt(what, false);
	}
	public void setTlicexpdt(Object what, boolean rounded) {
		if (rounded)
			tlicexpdt.setRounded(what);
		else
			tlicexpdt.set(what);
	}	
	public PackedDecimalData getTcolprct() {
		return tcolprct;
	}
	public void setTcolprct(Object what) {
		setTcolprct(what, false);
	}
	public void setTcolprct(Object what, boolean rounded) {
		if (rounded)
			tcolprct.setRounded(what);
		else
			tcolprct.set(what);
	}	
	public PackedDecimalData getTcolmax() {
		return tcolmax;
	}
	public void setTcolmax(Object what) {
		setTcolmax(what, false);
	}
	public void setTcolmax(Object what, boolean rounded) {
		if (rounded)
			tcolmax.setRounded(what);
		else
			tcolmax.set(what);
	}	
	public FixedLengthStringData getTsalesunt() {
		return tsalesunt;
	}
	public void setTsalesunt(Object what) {
		tsalesunt.set(what);
	}	
	public FixedLengthStringData getPrdagent() {
		return prdagent;
	}
	public void setPrdagent(Object what) {
		prdagent.set(what);
	}	
	public FixedLengthStringData getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(Object what) {
		agccqind.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//TMLII-281 AG-01-002 START
	public void setZrecruit(Object what) {
		zrecruit.set(what);
	}	
	public FixedLengthStringData getZrecruit() {
		return zrecruit;
	}
	//TMLII-281 AG-01-002 END

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		currcode.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		dteapp.clear();
		dtetrm.clear();
		nonKeyFiller50.clear();
		minsta.clear();
		payclt.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		zrorcode.clear();
		effdate.clear();
		taxmeth.clear();
		taxcde.clear();
		irdno.clear();
		tagsusind.clear();
		tlaglicno.clear();
		tlicexpdt.clear();
		tcolprct.clear();
		tcolmax.clear();
		tsalesunt.clear();
		prdagent.clear();
		agccqind.clear();
		//TMLII-281 AG-01-002
		zrecruit.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}