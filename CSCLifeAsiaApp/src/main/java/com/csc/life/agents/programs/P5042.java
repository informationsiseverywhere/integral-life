/*
 * File: P5042.java
 * Date: 29 August 2009 23:59:37
 * Author: Quipoz Limited
 * 
 * Class transformed from P5042.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgqfpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.agents.dataaccess.model.Agqfpf;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.dao.AgcmpfDAO;
import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.life.agents.screens.S5042ScreenVars;
import com.csc.life.agents.tablestructures.Tjl02rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.AgntenqTableDAM;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
*       P5042 - Agent Commission Change  Processing
*       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*       Overview
*       ~~~~~~~~
*
*       The main processing of this  program  is  to  alter a  con ract's
*       Agent  Commission  records  from one old agent to a new ag nt for
*       one agent at a time.
*
*       The program is driven by this screen which will require th  entry
*       of selection criteria:-
*
*            Agent From:    _____
*            Agent To:      _____
*
*            Initial Commission(Y/N):
*            Servicing Commission(Y/N):
*            Renewal Commission(Y/N):
*            Change Servicing Agent(Y/N):
*
*       Initialise Section.
*       ~~~~~~~~~~~~~~~~~~~
*            RETRV the CHDRMJA saved by the Submenu.
*            Set up the S5042 Screen Values from the CHDRMJA toget er
*            with calls to DESCIO to retrieve descriptions require  to
*            show on the screen.
*
*            READR AGNTENQ using the CHDR Agntcoy and Agntnum.
*
*            READR CLTS for AGNTENQ-CLNTNUM to get S5042-SERVAGNAM 
*
*            BEGN on AGCMDMN for contract number only.
*
*            Perform Process-Agcmsvq
*                 Until CHDRMJA-CHDRCOY    NOT = WSSP-COMPANY
*                 Or    CHDRMJA-CHDRNUM    NOT = CHDRMJA-CHDRNUM
*                 Or    AGCMSVQ-STATUZ     = ENDP.
*
*            Load Subfile for each new agent found (i.e. no duplic tes
*            for more than one AGCM found per agent).
*
*       Validation Section.
*       ~~~~~~~~~~~~~~~~~~~
*
*            The New Agent entered on the screen must firstly not  qual
*            spaces and secondly must exist.
*
*            For the 4 flags to be entered all must have a value o 
*            either 'Y' or 'N' ONLY. Note that the 4th flag may on y
*            be 'Y' if at least one other is 'Y'.
*
*            Selection of only one agent is allowed.
*
*       Update Section.
*       ~~~~~~~~~~~~~~~
*            Call the ATREQ subroutine to submit an ATRQ.
*
*       Where-Next Section.
*       ~~~~~~~~~~~~~~~~~~
*
*            Add 1 to WSSP-PROGRAM-PTR.
*
*
*****************************************************************
* </pre>
*/
public class P5042 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5042");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private final int wsaaSubfileSize = 10;
	private ZonedDecimalData wsaaRem = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFunctionKey = new FixedLengthStringData(4);
	private Validator wsaaRolu = new Validator(wsaaFunctionKey, "ROLU");
	private Validator wsaaCalc = new Validator(wsaaFunctionKey, "CALC");
	private Validator wsaaKill = new Validator(wsaaFunctionKey, "KILL");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0).init(SPACES);
	private FixedLengthStringData wsaaStoreAgent = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaSelectedAgent = new FixedLengthStringData(8).init(SPACES);
	private static final String e186 = "E186";
	private static final String g614 = "G614";
	private static final String g615 = "G615";
	private static final String g616 = "G616";
	private static final String g617 = "G617";
	private static final String g638 = "G638";
	private static final String g772 = "G772";
	private static final String t089 = "T089";
	private static final String e475 = "E475";
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	//ILJ-9 start
	private static final String tjl02 = "TJL02";
	private static final String rfbr = "RFBR";
	private static final String jl11 = "JL11";
	private static final String jl12 = "JL12";
	private static final String jl14 = "JL14";
	
	//end
	//ILJ-20
	private static final String JL19 = "JL19";
	private static final String JL20 = "JL20";
	//private static final String agntum = "JL20";
	//end
	private static final String aglfrec = "AGLFREC";
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntenqTableDAM agntenqIO = new AgntenqTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private S5042ScreenVars sv = getLScreenVars();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO2", AgcmpfDAO.class);
	//ILJ-9
	private AgqfpfDAO agqfpfDAO = getApplicationContext().getBean("agqfpfDAO", AgqfpfDAO.class);
	private Tjl02rec tjl02rec = new Tjl02rec(); 
	private static final String VULCERT = "VULCERT ";
	private static final String FXCERT = "FXCERT  ";
	protected Itempf itempf = null;
	protected ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	//end
	
	private List<Agcmpf> agcmList = null;
	private Iterator<Agcmpf> agcmItr = null;	
	private Agcmpf agcmdmn;
	private long wsaaDataKey=0;
	
	//ILJ-9
		private static final String feaconfigID_ILJ = "AGMTN019";
		private boolean iljFlag = false;
	//end
	

	protected S5042ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars( S5042ScreenVars.class);
	}
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		writeToSubfile1300, 
		setMoreSign1400, 
		nextRecord2110, 
		validateScreen2120, 
		checkComm2030, 
		checkForErrors2050, 
		para2200, 
		writeToSubfile2600, 
		setMoreSign2650, 
		softlock3110, 
		exit3090, 
		para5100
	}

	public P5042() {
		super();
		screenVars = sv;
		new ScreenModel("S5042", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		//ILJ-9 start
		iljFlag = FeaConfg.isFeatureExist("9", feaconfigID_ILJ, appVars, "IT");
		
				if(iljFlag) {
					sv.iljScreenflag.set("Y");
				}
				else {
					sv.iljScreenflag.set("N");
				}
		//end
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					loadSubfile1200();
				case writeToSubfile1300: 
					writeToSubfile1300();
					nextRecord1400();
				case setMoreSign1400: 
					setMoreSign1400();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/* Get today's date for use in validating the new agent.        */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		wsaaStoreAgent.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.subfileFields.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		/*PERFORM 5000-CALL-SCREEN-IO.                         <D509CS>*/
		processScreen("S5042", sv);
		/*                                                         <D509CS>*/
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Read CHDRMJA (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrmjaIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmjaIO.getChdrnum());
		sv.cnttype.set(chdrmjaIO.getCnttype());
		sv.cntcurr.set(chdrmjaIO.getCntcurr());
		sv.register.set(chdrmjaIO.getRegister());
		sv.servagnt.set(chdrmjaIO.getAgntnum());
		sv.servbr.set(chdrmjaIO.getCntbranch());
		/*    Obtain the Contract Type description from T5688.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmjaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/*    Obtain the Contract Status description from T3623.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmjaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.chdrstatus.fill("?");
		}
		else {
			sv.chdrstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Premuim Status description from T3588.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmjaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.premstatus.fill("?");
		}
		else {
			sv.premstatus.set(descIO.getShortdesc());
		}
		/*    Obtain the Agent's name.*/
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrmjaIO.getAgntcoy());
		agntenqIO.setAgntnum(chdrmjaIO.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)
		&& isNE(agntenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.servagnam.set(wsspcomn.longconfname);
		/*    Obtain the Branch name from T1692.*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrmjaIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.servbrname.fill("?");
		}
		else {
			sv.servbrname.set(descIO.getLongdesc());
		}
		/*  include the first AGCMDMN rec in loop processing .*/
		agcmList = agcmpfDAO.searchAgcmdmnRecord(wsspcomn.company.toString(), chdrmjaIO.getChdrnum().toString());
		if (agcmList==null || agcmList.size()==0) {
			goTo(GotoLabel.setMoreSign1400);
		}else {
		agcmItr = agcmList.iterator();
		}
//		agcmdmnIO.setDataArea(SPACES);
//		agcmdmnIO.setChdrcoy(wsspcomn.company);
//		agcmdmnIO.setChdrnum(chdrmjaIO.getChdrnum());
//		agcmdmnIO.setAgntnum(SPACES);
//		agcmdmnIO.setLife(SPACES);
//		agcmdmnIO.setCoverage(SPACES);
//		agcmdmnIO.setRider(SPACES);
//		agcmdmnIO.setPlanSuffix(ZERO);
//		agcmdmnIO.setFunction(varcom.begn);
	}

protected void loadSubfile1200()
	{
	
		callAgcmIo5100();
		if(agcmdmn.getStatus().trim().equals(varcom.endp.toString())) {
			goTo(GotoLabel.setMoreSign1400);
		}

	}

protected void writeToSubfile1300()
	{
		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		/*PERFORM 5000-CALL-SCREEN-IO.                         <D509CS>*/
		processScreen("S5042", sv);
		/*                                                         <D509CS>*/
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void nextRecord1400()
	{
		callAgcmIo5100();		
		if (varcom.endp.toString().equals(agcmdmn.getStatus())) {
				goTo(GotoLabel.setMoreSign1400);
			}
			
		if (isEQ(scrnparams.subfileRrn, wsaaSubfileSize)) {
			wsaaRem.set(0);
			goTo(GotoLabel.setMoreSign1400);
		}
		goTo(GotoLabel.writeToSubfile1300);
	}

protected void setMoreSign1400()
	{
	
	if(agcmdmn ==null) {
		scrnparams.subfileMore.set(SPACES);	
	}else if (varcom.endp.toString().equals(agcmdmn.getStatus())){
			scrnparams.subfileMore.set("Y");
			wsaaDataKey=agcmdmn.getUnique_Number();
	}else {
			scrnparams.subfileMore.set(SPACES);
	}
	
	
//		if (isNE(agcmdmnIO.getStatuz(), varcom.endp)) {
//			scrnparams.subfileMore.set("Y");
//			wsaaDataKey.set(agcmdmnIO.getDataKey());
//		}
//		else {
//			scrnparams.subfileMore.set(SPACES);
//		}
		/*EXIT*/
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5042IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5042-DATA-AREA            */
		/*                                      S5042-SUBFILE-AREA.        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		wsaaFunctionKey.set(scrnparams.statuz);
		if (wsaaKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		if (wsaaCalc.isTrue()) {
			wsspcomn.edterror.set("Y");
			validateSubfileScreen2100();
		}
		if (wsaaRolu.isTrue()) {
			rollUp2200();
		}
		if (!wsaaKill.isTrue()
		&& !wsaaCalc.isTrue()
		&& !wsaaRolu.isTrue()) {
			validateSubfileScreen2100();
		}
	}

protected void validateSubfileScreen2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2110();
				case nextRecord2110: 
					nextRecord2110();
				case validateScreen2120: 
					validateScreen2120();
				case checkComm2030: 
					checkComm2030();
				case checkForErrors2050: 
					checkForErrors2050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2110()
	{
		wsaaSelectedAgent.set(SPACES);
		scrnparams.function.set(varcom.sstrt);
	}

protected void nextRecord2110()
	{
		processScreen("S5042", sv);
		if ((isNE(scrnparams.statuz, varcom.oK))
		&& (isNE(scrnparams.statuz, varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.validateScreen2120);
		}
		if (isEQ(sv.select, "1")
		|| isEQ(sv.select, " ")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.selectErr.set(g615);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.select, "1")
		&& isNE(wsaaSelectedAgent, SPACES)) {
			sv.selectErr.set(g617);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.select, "1")
		&& isEQ(wsaaSelectedAgent, SPACES)) {
			wsaaSelectedAgent.set(sv.comagnt);
		}
		if (isEQ(sv.select, " ")) {
			scrnparams.function.set(varcom.srdn);
			goTo(GotoLabel.nextRecord2110);
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5042", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.nextRecord2110);
	}

protected void validateScreen2120()
	{
		/* The 'New Agent' field must be entered.*/
		if (isEQ(sv.newagt, SPACES)) {
			sv.newagtErr.set(e186);
			sv.newagntnm.set(SPACES);
			goTo(GotoLabel.checkComm2030);
		}
		/* Check that the New agent exists.*/
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(chdrmjaIO.getAgntcoy());
		agntenqIO.setAgntnum(sv.newagt);
		agntenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntenqIO);
		if (isNE(agntenqIO.getStatuz(), varcom.oK)
		&& isNE(agntenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		if (isEQ(agntenqIO.getStatuz(), varcom.mrnf)) {
			sv.newagtErr.set(g772);
			sv.newagntnm.set(SPACES);
			goTo(GotoLabel.checkComm2030);
		}
		/* Set Up new agent name on screen.*/
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.newagntnm.set(wsspcomn.longconfname);
		/*  Ensure that the Agent is not dead.                          */
		if (isLTE(cltsIO.getCltdod(), wsaaToday)
		&& isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)) {
			sv.newagtErr.set(t089);
			goTo(GotoLabel.checkComm2030);
		}
		/*  Read the AGLF file to obtain appointed, terminated and      */
		/*  expired dates for the new agent.                            */
		/*  Check the above dates against the effective date of the     */
		/*  transaction to ensure that the agent is active.             */
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrmjaIO.getAgntcoy());
		aglfIO.setAgntnum(sv.newagt);
		aglfIO.setFunction(varcom.readr);
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), varcom.mrnf)) {
			sv.newagtErr.set(g772);
			goTo(GotoLabel.checkComm2030);
		}
		if (isLT(aglfIO.getDtetrm(), wsaaToday)
		|| isLT(aglfIO.getDteexp(), wsaaToday)
		|| isGT(aglfIO.getDteapp(), wsaaToday)) {
			sv.newagtErr.set(e475);
		}
		
		//ILJ-9 start
		//Check if New Agent entered has a valid License Number (i.e. not yet expired as of processing date)
		if (sv.iljScreenflag.compareTo("Y") == 0){
			if (null != aglfIO ) {
			if(null == aglfIO.getTlaglicno() || isEQ(aglfIO.getTlaglicno(), SPACES)) {
				sv.newagtErr.set(jl14);
				wsspcomn.edterror.set("Y");
			}
			else if(isLT(aglfIO.getTlicexpdt(), wsaaToday)){
				sv.newagtErr.set(rfbr);
				wsspcomn.edterror.set("Y");
			}
			else {
			//get item from itempf
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tjl02);
			itempf.setItemitem(chdrmjaIO.getCnttype().toString());
			itempf = itempfDAO.getItempfRecord(itempf);
			if(null == itempf) {
				return;
			}else {
				tjl02rec.tjl02Rec.set(StringUtil.rawToString(itempf.getGenarea())); 
				List<String> qfListByCnttype = new ArrayList<String>();
				for(int i = 0; i <= tjl02rec.qlfy.length-1; i++) {
					if(null != tjl02rec.qlfy[i] && !tjl02rec.qlfy[i].toString().trim().isEmpty()) {
						qfListByCnttype.add(tjl02rec.qlfy[i].toString());
					}
				}
	
				//validate commission agent		
				//ILJ-20
				//List<String> qfList1 = agqfpfDAO.getQualificationListbyAgntNum(sv.newagt.toString());
				List<Agqfpf> qfList1 = agqfpfDAO.getAgqfListbyAgentNumber(sv.newagt.toString());
					
				if(null==qfList1 || qfList1.isEmpty()) {
					for(String s : qfListByCnttype) {
						if(s.equals(FXCERT)){
							sv.newagtErr.set(jl11);
							wsspcomn.edterror.set("Y");
						}
						else if(s.equals(VULCERT)) {
							//scrnparams.errorCode.set(jl12);
							sv.servbrErr.set(jl12);
							wsspcomn.edterror.set("Y");
						}
						
					}
					
				}
				else {
					    int fxFlag=0;//not found
					    int vulFlag=0;
					    int tjl02_fxFlag=0;
					    int tjl02_vulFlag=0;
					    
						for(String s : qfListByCnttype) {
							for(int i=0;i <qfList1.size(); i++) {
								if(s.equals(FXCERT)){
										tjl02_fxFlag=1;		
										if(qfList1.get(i).getQualification().equalsIgnoreCase(s))
										{								
											fxFlag=1;
											if(isLT(qfList1.get(i).getDateend(), wsaaToday)) {
												sv.newagtErr.set(JL19);
												wsspcomn.edterror.set("Y");
											}
										}
								}
								if(s.equals(VULCERT)){
									tjl02_vulFlag=1;
									if(qfList1.get(i).getQualification().equalsIgnoreCase(s))
									{
										vulFlag=1;
										if(isLT(qfList1.get(i).getDateend(), wsaaToday)) {
											sv.servbrErr.set(JL20);
											wsspcomn.edterror.set("Y");
										}
									}
								}
							}
							
						}
						if(tjl02_fxFlag==1 && fxFlag==0) {
							sv.newagtErr.set(jl11);
							wsspcomn.edterror.set("Y");
						}
						if(tjl02_vulFlag==1 && vulFlag==0) {
							sv.servbrErr.set(jl12);
							wsspcomn.edterror.set("Y");
						}
					}
			}
			}
				
		}
		}
		}
		//end
	

protected void checkComm2030()
	{
		if (isEQ(sv.zrorcomm, "Y")
		|| isEQ(sv.zrorcomm, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.zrorcommErr.set(g616);
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(sv.servcom, "Y")
		|| isEQ(sv.servcom, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.servcomErr.set(g616);
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(sv.rnwlcom, "Y")
		|| isEQ(sv.rnwlcom, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.rnwlcomErr.set(g616);
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(sv.inalcom, "Y")
		|| isEQ(sv.inalcom, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.inalcomErr.set(g616);
			goTo(GotoLabel.checkForErrors2050);
		}
		if (isEQ(sv.comind, "Y")
		|| isEQ(sv.comind, "N")) {
			/*NEXT_SENTENCE*/
		}
		else {
			sv.comindErr.set(g616);
			goTo(GotoLabel.checkForErrors2050);
		}
	}

protected void checkForErrors2050()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.inalcom, "N")
		&& isEQ(sv.rnwlcom, "N")
		&& isEQ(sv.servcom, "N")
		&& isEQ(sv.zrorcomm, "N")) {
			sv.inalcomErr.set(g638);
			sv.servcomErr.set("X");
			sv.rnwlcomErr.set("X");
			sv.zrorcommErr.set("X");
			wsspcomn.edterror.set("Y");
		}
		/*  If no agent selected or agent selected = new agent then ERROR*/
		if (isEQ(wsaaSelectedAgent, SPACES)) {
			scrnparams.errorCode.set(g617);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsaaSelectedAgent, sv.newagt)
		&& isNE(wsaaSelectedAgent, SPACES)) {
			sv.newagtErr.set(g614);
			wsspcomn.edterror.set("Y");
		}
	}

protected void rollUp2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para2200: 
					para2200();
				case writeToSubfile2600: 
					writeToSubfile2600();
				case setMoreSign2650: 
					setMoreSign2650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2200()
	{
		sv.selectErr.set(SPACES);
		sv.errorSubfile.set(SPACES);
		wsspcomn.edterror.set("N");
		agcmdmn = agcmpfDAO.searchAgcmdmnRecord(wsaaDataKey);
		agcmdmn.setStatus(varcom.oK.toString());
//		agcmdmnIO.setDataKey(wsaaDataKey);
//		agcmdmnIO.setFunction(varcom.readr);
	}

protected void writeToSubfile2600()
	{

		if (agcmdmn==null) {
			goTo(GotoLabel.setMoreSign2650);
			
		}else if(isGTE(wsaaRem, wsaaSubfileSize)) {
			wsaaDataKey=agcmdmn.getUnique_Number();
			wsaaRem.set(ZERO);
			goTo(GotoLabel.setMoreSign2650);
		}

		moveFieldsToSubfile6000();
		scrnparams.function.set(varcom.sadd);
		callScreenIo5000();
		wsaaRem.add(1);
		goTo(GotoLabel.writeToSubfile2600);
		
	}

protected void setMoreSign2650()
	{
	

		if (isNE(agcmdmn.getStatus(), varcom.endp)) {
			scrnparams.subfileMore.set("Y");
		}
		else {
			scrnparams.subfileMore.set(SPACES);
		}
		scrnparams.function.set(varcom.norml);
		/*    CALL 'S5042IO'              USING SCRN-SCREEN-PARAMS         */
		/*                                      S5042-DATA-AREA            */
		/*                                      S5042-SUBFILE-AREA.        */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set("Y");
		/* This is new code.*/
		wsaaFunctionKey.set(scrnparams.statuz);
		if (wsaaKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			return ;
		}
		/*    GO TO 2090-EXIT.                                          */
		if (wsaaCalc.isTrue()) {
			wsspcomn.edterror.set("Y");
			validateSubfileScreen2100();
		}
		if (wsaaRolu.isTrue()) {
			goTo(GotoLabel.para2200);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
				case softlock3110: 
					softlock3110();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateDatabase3010()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		if (isNE(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.softlock3110);
		}
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		goTo(GotoLabel.exit3090);
	}

protected void softlock3110()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sftlockrec.function.set("TOAT");
		sftlockrec.statuz.set(varcom.oK);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrmjaIO.getChdrnum());
		/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*       the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("P5042AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(chdrmjaIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionRecInner.wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionRecInner.wsaaEffdate.set(wsaaToday);
		wsaaTransactionRecInner.wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaTransactionRecInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransactionRecInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionRecInner.wsaaAgentnum.set(wsaaSelectedAgent);
		wsaaTransactionRecInner.wsaaNewagt.set(sv.newagt);
		wsaaTransactionRecInner.wsaaServcom.set(sv.servcom);
		wsaaTransactionRecInner.wsaaRnwlcom.set(sv.rnwlcom);
		wsaaTransactionRecInner.wsaaInalcom.set(sv.inalcom);
		wsaaTransactionRecInner.wsaaComind.set(sv.comind);
		wsaaTransactionRecInner.wsaaZrorcomm.set(sv.zrorcomm);
		wsaaTransactionRecInner.wsaaBatcactmn.set(wsspcomn.acctmonth);
		wsaaTransactionRecInner.wsaaBatcactyr.set(wsspcomn.acctyear);
		atreqrec.transArea.set(wsaaTransactionRecInner.wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callScreenIo5000()
	{
		/*PARA*/
		/*    CALL 'S5042IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5042-DATA-AREA                         */
		/*                         S5042-SUBFILE-AREA.                     */
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callAgcmIo5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case para5100: 
					para5100();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5100()
	{

	if (agcmItr.hasNext()) {
		
		agcmdmn = agcmItr.next();
		if(agcmdmn.getAgntnum().equals(wsaaStoreAgent.toString())) {
			goTo(GotoLabel.para5100);
		}
		
		wsaaStoreAgent.set(agcmdmn.getAgntnum());

	}else {
		agcmdmn.setStatus(varcom.endp.toString());
		return ;
	}
	

	}

protected void moveFieldsToSubfile6000()
	{

		sv.comagnt.set(agcmdmn.getAgntnum());
		/*    Obtain the Agent's name.*/
		agntenqIO.setDataKey(SPACES);
		agntenqIO.setAgntcoy(agcmdmn.getChdrcoy());
		agntenqIO.setAgntnum(agcmdmn.getAgntnum());
		agntenqIO.setFunction(varcom.readr);
		
		SmartFileCode.execute(appVars, agntenqIO);
		
		if (isNE(agntenqIO.getStatuz(), varcom.oK)
		&& isNE(agntenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(agntenqIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.comagntnm.set(wsspcomn.longconfname);
		sv.sflchg.set("X");
		
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner { 

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(215);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaAgentnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 16).init(SPACES);
	private FixedLengthStringData wsaaNewagt = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 24).init(SPACES);
	private FixedLengthStringData wsaaServcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 32).init(SPACES);
	private FixedLengthStringData wsaaRnwlcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 33).init(SPACES);
	private FixedLengthStringData wsaaInalcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 34).init(SPACES);
	private FixedLengthStringData wsaaComind = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 35).init(SPACES);
	private FixedLengthStringData wsaaZrorcomm = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 36).init(SPACES);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 37).init(ZERO);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 40).init(ZERO);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 42).init(ZERO).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(165).isAPartOf(wsaaTransactionRec, 50, FILLER).init(SPACES);
}
}