/*
 * File: Sr58tprotect.java
 * Date: 18 July 2013 2:36:10
 * Author: CSC
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.COBOLAppVars;

/**
 * Screen record for PROTECT Sr58t
 * @author csc
 */
public class Sr58tprotect extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {-1, -1, -1, -1}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr58tScreenVars sv = ( Sr58tScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr58tprotectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr58tScreenVars screenVars = ( Sr58tScreenVars)pv;
	}
	
	/**
	 * Clear all the variables in S2500screen
	 */
	public static void clear(VarModel pv) {
		Sr58tScreenVars screenVars = (Sr58tScreenVars) pv;
	}
}
