package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52R
 * @version 1.0 generated on 11/22/13 6:36 AM
 * @author CSC
 */
public class Sr52rScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1078);
	public FixedLengthStringData dataFields = new FixedLengthStringData(310).isAPartOf(dataArea, 0);
	public ZonedDecimalData agelimit = DD.agelimit.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData dsifact = DD.dsifact.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData factorsas = new FixedLengthStringData(70).isAPartOf(dataFields, 11);
	public ZonedDecimalData[] factorsa = ZDArrayPartOfStructure(10, 7, 4, factorsas, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(70).isAPartOf(factorsas, 0, FILLER_REDEFINE);
	public ZonedDecimalData factorsa01 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData factorsa02 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,7);
	public ZonedDecimalData factorsa03 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,14);
	public ZonedDecimalData factorsa04 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,21);
	public ZonedDecimalData factorsa05 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,28);
	public ZonedDecimalData factorsa06 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData factorsa07 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,42);
	public ZonedDecimalData factorsa08 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,49);
	public ZonedDecimalData factorsa09 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,56);
	public ZonedDecimalData factorsa10 = DD.factorsa.copyToZonedDecimal().isAPartOf(filler,63);
	public FixedLengthStringData factsamns = new FixedLengthStringData(70).isAPartOf(dataFields, 81);
	public ZonedDecimalData[] factsamn = ZDArrayPartOfStructure(10, 7, 4, factsamns, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(70).isAPartOf(factsamns, 0, FILLER_REDEFINE);
	public ZonedDecimalData factsamn01 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData factsamn02 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,7);
	public ZonedDecimalData factsamn03 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,14);
	public ZonedDecimalData factsamn04 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,21);
	public ZonedDecimalData factsamn05 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,28);
	public ZonedDecimalData factsamn06 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,35);
	public ZonedDecimalData factsamn07 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,42);
	public ZonedDecimalData factsamn08 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,49);
	public ZonedDecimalData factsamn09 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,56);
	public ZonedDecimalData factsamn10 = DD.factsamn.copyToZonedDecimal().isAPartOf(filler1,63);
	public FixedLengthStringData factsamxs = new FixedLengthStringData(70).isAPartOf(dataFields, 151);
	public ZonedDecimalData[] factsamx = ZDArrayPartOfStructure(10, 7, 4, factsamxs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(70).isAPartOf(factsamxs, 0, FILLER_REDEFINE);
	public ZonedDecimalData factsamx01 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData factsamx02 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,7);
	public ZonedDecimalData factsamx03 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,14);
	public ZonedDecimalData factsamx04 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,21);
	public ZonedDecimalData factsamx05 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,28);
	public ZonedDecimalData factsamx06 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,35);
	public ZonedDecimalData factsamx07 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,42);
	public ZonedDecimalData factsamx08 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,49);
	public ZonedDecimalData factsamx09 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,56);
	public ZonedDecimalData factsamx10 = DD.factsamx.copyToZonedDecimal().isAPartOf(filler2,63);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,221);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,229);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,237);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,245);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,275);
	public FixedLengthStringData toterms = new FixedLengthStringData(30).isAPartOf(dataFields, 280);
	public ZonedDecimalData[] toterm = ZDArrayPartOfStructure(10, 3, 0, toterms, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(30).isAPartOf(toterms, 0, FILLER_REDEFINE);
	public ZonedDecimalData toterm01 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData toterm02 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,3);
	public ZonedDecimalData toterm03 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,6);
	public ZonedDecimalData toterm04 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,9);
	public ZonedDecimalData toterm05 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData toterm06 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,15);
	public ZonedDecimalData toterm07 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,18);
	public ZonedDecimalData toterm08 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,21);
	public ZonedDecimalData toterm09 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,24);
	public ZonedDecimalData toterm10 = DD.toterm.copyToZonedDecimal().isAPartOf(filler3,27);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 310);
	public FixedLengthStringData agelimitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData dsifactErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData factorsasErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] factorsaErr = FLSArrayPartOfStructure(10, 4, factorsasErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(factorsasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData factorsa01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData factorsa02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData factorsa03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData factorsa04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData factorsa05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData factorsa06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData factorsa07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData factorsa08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData factorsa09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData factorsa10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData factsamnsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] factsamnErr = FLSArrayPartOfStructure(10, 4, factsamnsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(factsamnsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData factsamn01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData factsamn02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData factsamn03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData factsamn04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData factsamn05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData factsamn06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData factsamn07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData factsamn08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData factsamn09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData factsamn10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData factsamxsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData[] factsamxErr = FLSArrayPartOfStructure(10, 4, factsamxsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(factsamxsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData factsamx01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData factsamx02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData factsamx03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData factsamx04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData factsamx05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData factsamx06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData factsamx07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData factsamx08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData factsamx09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData factsamx10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData totermsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData[] totermErr = FLSArrayPartOfStructure(10, 4, totermsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(totermsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData toterm01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData toterm02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData toterm03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData toterm04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData toterm05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData toterm06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData toterm07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData toterm08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData toterm09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData toterm10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(576).isAPartOf(dataArea, 502);
	public FixedLengthStringData[] agelimitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] dsifactOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData factorsasOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] factorsaOut = FLSArrayPartOfStructure(10, 12, factorsasOut, 0);
	public FixedLengthStringData[][] factorsaO = FLSDArrayPartOfArrayStructure(12, 1, factorsaOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(factorsasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] factorsa01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] factorsa02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] factorsa03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] factorsa04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] factorsa05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] factorsa06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] factorsa07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] factorsa08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] factorsa09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] factorsa10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData factsamnsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] factsamnOut = FLSArrayPartOfStructure(10, 12, factsamnsOut, 0);
	public FixedLengthStringData[][] factsamnO = FLSDArrayPartOfArrayStructure(12, 1, factsamnOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(120).isAPartOf(factsamnsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] factsamn01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] factsamn02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] factsamn03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] factsamn04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] factsamn05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] factsamn06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] factsamn07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] factsamn08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] factsamn09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] factsamn10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	public FixedLengthStringData factsamxsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 276);
	public FixedLengthStringData[] factsamxOut = FLSArrayPartOfStructure(10, 12, factsamxsOut, 0);
	public FixedLengthStringData[][] factsamxO = FLSDArrayPartOfArrayStructure(12, 1, factsamxOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(factsamxsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] factsamx01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] factsamx02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] factsamx03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] factsamx04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] factsamx05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] factsamx06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] factsamx07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] factsamx08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] factsamx09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] factsamx10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData totermsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 456);
	public FixedLengthStringData[] totermOut = FLSArrayPartOfStructure(10, 12, totermsOut, 0);
	public FixedLengthStringData[][] totermO = FLSDArrayPartOfArrayStructure(12, 1, totermOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(totermsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] toterm01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] toterm02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] toterm03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] toterm04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] toterm05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] toterm06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] toterm07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] toterm08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] toterm09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] toterm10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr52rscreenWritten = new LongData(0);
	public LongData Sr52rprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52rScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agelimitOut,new String[] {"01",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm01Out,new String[] {"02",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa01Out,new String[] {"03",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn01Out,new String[] {"03",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx01Out,new String[] {"04",null, "-54",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm02Out,new String[] {"05",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa02Out,new String[] {"06",null, "-56",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn02Out,new String[] {"07",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx02Out,new String[] {"08",null, "-58",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm03Out,new String[] {"09",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa03Out,new String[] {"10",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn03Out,new String[] {"11",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx03Out,new String[] {"12",null, "-62",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm04Out,new String[] {"13",null, "-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa04Out,new String[] {"14",null, "-64",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn04Out,new String[] {"15",null, "-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx04Out,new String[] {"16",null, "-66",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm05Out,new String[] {"17",null, "-67",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa05Out,new String[] {"18",null, "-68",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn05Out,new String[] {"19",null, "-69",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx05Out,new String[] {"20",null, "-70",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm06Out,new String[] {"21",null, "-71",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa06Out,new String[] {"22",null, "-72",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn06Out,new String[] {"23",null, "-73",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx06Out,new String[] {"24",null, "-74",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm07Out,new String[] {"25",null, "-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa07Out,new String[] {"26",null, "-76",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn07Out,new String[] {"27",null, "-77",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx07Out,new String[] {"28",null, "-78",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm08Out,new String[] {"29",null, "-79",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa08Out,new String[] {"30",null, "-80",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn08Out,new String[] {"31",null, "-81",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx08Out,new String[] {"32",null, "-82",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm09Out,new String[] {"33",null, "-83",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa09Out,new String[] {"34",null, "-84",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn09Out,new String[] {"35",null, "-85",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx09Out,new String[] {"36",null, "-86",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(toterm10Out,new String[] {"37",null, "-87",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factorsa10Out,new String[] {"38",null, "-88",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamn10Out,new String[] {"39",null, "-89",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(factsamx10Out,new String[] {"40",null, "-90",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dsifactOut,new String[] {"41",null, "-91",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, agelimit, toterm01, factorsa01, factsamn01, factsamx01, toterm02, factorsa02, factsamn02, factsamx02, toterm03, factorsa03, factsamn03, factsamx03, toterm04, factorsa04, factsamn04, factsamx04, toterm05, factorsa05, factsamn05, factsamx05, toterm06, factorsa06, factsamn06, factsamx06, toterm07, factorsa07, factsamn07, factsamx07, toterm08, factorsa08, factsamn08, factsamx08, toterm09, factorsa09, factsamn09, factsamx09, toterm10, factorsa10, factsamn10, factsamx10, dsifact};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, agelimitOut, toterm01Out, factorsa01Out, factsamn01Out, factsamx01Out, toterm02Out, factorsa02Out, factsamn02Out, factsamx02Out, toterm03Out, factorsa03Out, factsamn03Out, factsamx03Out, toterm04Out, factorsa04Out, factsamn04Out, factsamx04Out, toterm05Out, factorsa05Out, factsamn05Out, factsamx05Out, toterm06Out, factorsa06Out, factsamn06Out, factsamx06Out, toterm07Out, factorsa07Out, factsamn07Out, factsamx07Out, toterm08Out, factorsa08Out, factsamn08Out, factsamx08Out, toterm09Out, factorsa09Out, factsamn09Out, factsamx09Out, toterm10Out, factorsa10Out, factsamn10Out, factsamx10Out, dsifactOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, agelimitErr, toterm01Err, factorsa01Err, factsamn01Err, factsamx01Err, toterm02Err, factorsa02Err, factsamn02Err, factsamx02Err, toterm03Err, factorsa03Err, factsamn03Err, factsamx03Err, toterm04Err, factorsa04Err, factsamn04Err, factsamx04Err, toterm05Err, factorsa05Err, factsamn05Err, factsamx05Err, toterm06Err, factorsa06Err, factsamn06Err, factsamx06Err, toterm07Err, factorsa07Err, factsamn07Err, factsamx07Err, toterm08Err, factorsa08Err, factsamn08Err, factsamx08Err, toterm09Err, factorsa09Err, factsamn09Err, factsamx09Err, toterm10Err, factorsa10Err, factsamn10Err, factsamx10Err, dsifactErr};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52rscreen.class;
		protectRecord = Sr52rprotect.class;
	}

}
