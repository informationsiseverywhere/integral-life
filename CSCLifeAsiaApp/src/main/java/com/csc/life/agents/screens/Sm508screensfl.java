package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm508screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 15;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 21, 4, 67}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm508ScreenVars sv = (Sm508ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sm508screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sm508screensfl, 
			sv.Sm508screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sm508ScreenVars sv = (Sm508ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sm508screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sm508ScreenVars sv = (Sm508ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sm508screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sm508screensflWritten.gt(0))
		{
			sv.sm508screensfl.setCurrentIndex(0);
			sv.Sm508screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sm508ScreenVars sv = (Sm508ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sm508screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sm508ScreenVars screenVars = (Sm508ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.agmvty.setFieldName("agmvty");
				screenVars.benCessDateDisp.setFieldName("benCessDateDisp");
				screenVars.reportag01.setFieldName("reportag01");
				screenVars.reportag02.setFieldName("reportag02");
				screenVars.reportag03.setFieldName("reportag03");
				screenVars.accountType.setFieldName("accountType");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.agmvty.set(dm.getField("agmvty"));
			screenVars.benCessDateDisp.set(dm.getField("benCessDateDisp"));
			screenVars.reportag01.set(dm.getField("reportag01"));
			screenVars.reportag02.set(dm.getField("reportag02"));
			screenVars.reportag03.set(dm.getField("reportag03"));
			screenVars.accountType.set(dm.getField("accountType"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sm508ScreenVars screenVars = (Sm508ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.agmvty.setFieldName("agmvty");
				screenVars.benCessDateDisp.setFieldName("benCessDateDisp");
				screenVars.reportag01.setFieldName("reportag01");
				screenVars.reportag02.setFieldName("reportag02");
				screenVars.reportag03.setFieldName("reportag03");
				screenVars.accountType.setFieldName("accountType");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("agmvty").set(screenVars.agmvty);
			dm.getField("benCessDateDisp").set(screenVars.benCessDateDisp);
			dm.getField("reportag01").set(screenVars.reportag01);
			dm.getField("reportag02").set(screenVars.reportag02);
			dm.getField("reportag03").set(screenVars.reportag03);
			dm.getField("accountType").set(screenVars.accountType);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sm508screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sm508ScreenVars screenVars = (Sm508ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.agmvty.clearFormatting();
		screenVars.benCessDateDisp.clearFormatting();
		screenVars.reportag01.clearFormatting();
		screenVars.reportag02.clearFormatting();
		screenVars.reportag03.clearFormatting();
		screenVars.accountType.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sm508ScreenVars screenVars = (Sm508ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.agmvty.setClassString("");
		screenVars.benCessDateDisp.setClassString("");
		screenVars.reportag01.setClassString("");
		screenVars.reportag02.setClassString("");
		screenVars.reportag03.setClassString("");
		screenVars.accountType.setClassString("");
	}

/**
 * Clear all the variables in Sm508screensfl
 */
	public static void clear(VarModel pv) {
		Sm508ScreenVars screenVars = (Sm508ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.agmvty.clear();
		screenVars.benCessDateDisp.clear();
		screenVars.benCessDate.clear();
		screenVars.reportag01.clear();
		screenVars.reportag02.clear();
		screenVars.reportag03.clear();
		screenVars.accountType.clear();
	}
}
