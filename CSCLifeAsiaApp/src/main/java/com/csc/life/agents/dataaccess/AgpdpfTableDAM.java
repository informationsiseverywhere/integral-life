package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpdpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:45
 * Class transformed from AGPDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpdpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 235;
	public FixedLengthStringData agpdrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agpdpfRecord = agpdrec;
	
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(agpdrec);
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agpdrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agpdrec);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(agpdrec);
	public FixedLengthStringData areadesc = DD.areadesc.copy().isAPartOf(agpdrec);
	public PackedDecimalData dteapp = DD.dteapp.copy().isAPartOf(agpdrec);
	public PackedDecimalData dtetrm = DD.dtetrm.copy().isAPartOf(agpdrec);
	public PackedDecimalData initcom = DD.initcom.copy().isAPartOf(agpdrec);
	public PackedDecimalData rnlcdue = DD.rnlcdue.copy().isAPartOf(agpdrec);
	public PackedDecimalData singlePremium = DD.singprm.copy().isAPartOf(agpdrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(agpdrec);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(agpdrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(agpdrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(agpdrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(agpdrec);
	public PackedDecimalData acctmnth = DD.acctmnth.copy().isAPartOf(agpdrec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(agpdrec);
	public PackedDecimalData xcomd = DD.xcomd.copy().isAPartOf(agpdrec);
	public PackedDecimalData orannl = DD.orannl.copy().isAPartOf(agpdrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agpdrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agpdrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agpdrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgpdpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgpdpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgpdpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgpdpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpdpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgpdpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpdpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGPDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"EFFDATE, " +
							"AGNTCOY, " +
							"AGNTNUM, " +
							"AGENTNAME, " +
							"AREADESC, " +
							"DTEAPP, " +
							"DTETRM, " +
							"INITCOM, " +
							"RNLCDUE, " +
							"SINGPRM, " +
							"CHDRNUM, " +
							"OWNERNAME, " +
							"BILLFREQ, " +
							"OCCDATE, " +
							"ACCTYR, " +
							"ACCTMNTH, " +
							"ORIGCURR, " +
							"XCOMD, " +
							"ORANNL, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     effdate,
                                     agntcoy,
                                     agntnum,
                                     agentname,
                                     areadesc,
                                     dteapp,
                                     dtetrm,
                                     initcom,
                                     rnlcdue,
                                     singlePremium,
                                     chdrnum,
                                     ownername,
                                     billfreq,
                                     occdate,
                                     acctyr,
                                     acctmnth,
                                     origcurr,
                                     xcomd,
                                     orannl,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		effdate.clear();
  		agntcoy.clear();
  		agntnum.clear();
  		agentname.clear();
  		areadesc.clear();
  		dteapp.clear();
  		dtetrm.clear();
  		initcom.clear();
  		rnlcdue.clear();
  		singlePremium.clear();
  		chdrnum.clear();
  		ownername.clear();
  		billfreq.clear();
  		occdate.clear();
  		acctyr.clear();
  		acctmnth.clear();
  		origcurr.clear();
  		xcomd.clear();
  		orannl.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgpdrec() {
  		return agpdrec;
	}

	public FixedLengthStringData getAgpdpfRecord() {
  		return agpdpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgpdrec(what);
	}

	public void setAgpdrec(Object what) {
  		this.agpdrec.set(what);
	}

	public void setAgpdpfRecord(Object what) {
  		this.agpdpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agpdrec.getLength());
		result.set(agpdrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}