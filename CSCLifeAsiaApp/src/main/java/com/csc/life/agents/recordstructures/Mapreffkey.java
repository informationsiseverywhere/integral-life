package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:36
 * Description:
 * Copybook name: MAPREFFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mapreffkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mapreffFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mapreffKey = new FixedLengthStringData(64).isAPartOf(mapreffFileKey, 0, REDEFINE);
  	public FixedLengthStringData mapreffAgntcoy = new FixedLengthStringData(1).isAPartOf(mapreffKey, 0);
  	public FixedLengthStringData mapreffAgntnum = new FixedLengthStringData(8).isAPartOf(mapreffKey, 1);
  	public PackedDecimalData mapreffEffdate = new PackedDecimalData(8, 0).isAPartOf(mapreffKey, 9);
  	public PackedDecimalData mapreffAcctyr = new PackedDecimalData(4, 0).isAPartOf(mapreffKey, 14);
  	public PackedDecimalData mapreffMnth = new PackedDecimalData(2, 0).isAPartOf(mapreffKey, 17);
  	public FixedLengthStringData mapreffCnttype = new FixedLengthStringData(3).isAPartOf(mapreffKey, 19);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(mapreffKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mapreffFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mapreffFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}