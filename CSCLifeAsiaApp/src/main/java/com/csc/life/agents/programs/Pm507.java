/*
 * File: Pm507.java
 * Date: 30 August 2009 1:13:05
 * Author: Quipoz Limited
 *
 * Class transformed from PM507.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.MacfTableDAM;
import com.csc.life.agents.dataaccess.MacfflvTableDAM;
import com.csc.life.agents.dataaccess.MacfinqTableDAM;
import com.csc.life.agents.dataaccess.MacfprdTableDAM;
import com.csc.life.agents.dataaccess.MacfslvTableDAM;
import com.csc.life.agents.dataaccess.MaglfrpTableDAM;
import com.csc.life.agents.screens.Sm507ScreenVars;
import com.csc.life.agents.tablestructures.Tm605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
*****************************************************************
* </pre>
*/
public class Pm507 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM507");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0).init(SPACES);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsbbReportag = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFlvlagt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSlvlagt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAmnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRepnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaRecruited = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAglfrptKey = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaAgntpfx = new FixedLengthStringData(2).isAPartOf(wsaaAglfrptKey, 0);
	private FixedLengthStringData wsaaAgntcoy = new FixedLengthStringData(1).isAPartOf(wsaaAglfrptKey, 2);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAglfrptKey, 3);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(5, 8, wsaaReportags, 0);

	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1);
	private Validator promotion = new Validator(wsaaFlag, "P");
	private Validator demotion = new Validator(wsaaFlag, "D");
		/*    88 TRANSFER                 VALUE 'F'.                       */
	private Validator transfer = new Validator(wsaaFlag, "F","M");
		/* TABLES */
	private static final String tm605 = "TM605";
	private static final String t5696 = "T5696";
	private static final String t1692 = "T1692";
	private static final String t3692 = "T3692";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MacfTableDAM macfIO = new MacfTableDAM();
	private MacfflvTableDAM macfflvIO = new MacfflvTableDAM();
	private MacfinqTableDAM macfinqIO = new MacfinqTableDAM();
	private MacfprdTableDAM macfprdIO = new MacfprdTableDAM();
	private MacfslvTableDAM macfslvIO = new MacfslvTableDAM();
	private MaglfrpTableDAM maglfrpIO = new MaglfrpTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Tm605rec tm605rec = new Tm605rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Sm507ScreenVars sv = ScreenProgram.getScreenVars( Sm507ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit3450, 
		nextr3810, 
		exit3810, 
		nextr3820, 
		exit3820
	}

	public Pm507() {
		super();
		screenVars = sv;
		new ScreenModel("Sm507", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		initialize(sv.dataArea);
		sv.agntselOut[varcom.nd.toInt()].set("Y");
		sv.agntselOut[varcom.pr.toInt()].set("Y");
		sv.mlprcindOut[varcom.pr.toInt()].set("Y");
		sv.mlprcindOut[varcom.nd.toInt()].set("Y");
		sv.mlparorc.set(ZERO);
		wsaaFlag.set(wsspcomn.flag);
		initialize(wsaaReportags);
		initialize(wsaaIndex);
		initialize(wsaaRepnum);
		initialize(wsaaRecruited);
		initialize(wsaaAglfrptKey);
		initialize(wsaaFlvlagt);
		initialize(wsaaSlvlagt);
		/*    MOVE SPACES                 TO WSAA-PCLET-REC.*/
		/*    Set screen fields*/
		aglfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntlagIO.setAgntcoy(aglfIO.getAgntcoy());
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		agntlagIO.setFormat(formatsInner.agntlagrec);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)
		&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsioCall1700();
		/*    MOVE AGNTLAG-AGNTCOY        TO WSAA-PCLET-CHDRCOY.*/
		/*    MOVE AGNTLAG-CLNTNUM        TO WSAA-PCLET-CLNTNUM.*/
		/*    MOVE AGNTLAG-CLNTCOY        TO WSAA-PCLET-CLNTCOY.*/
		/*    MOVE AGNTLAG-CLNTPFX        TO WSAA-PCLET-CLNTPFX.*/
		/*    MOVE AGNTLAG-AGNTPFX        TO WSAA-PCLET-AGNTPFX.*/
		/*    MOVE AGNTLAG-AGNTCOY        TO WSAA-PCLET-AGNTCOY.*/
		/*    MOVE AGNTLAG-AGNTNUM        TO WSAA-PCLET-AGNTNUM.*/
		sv.agnum.set(aglfIO.getAgntnum());
		sv.exclAgmt.set(aglfIO.getExclAgmt());
		wsaaRepnum.set(aglfIO.getReportag());
		sv.aracde.set(aglfIO.getAracde());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5696);
		descIO.setDescitem(aglfIO.getAracde());
		descIO.setFunction(varcom.readr);
		descioCall1600();
		sv.aradesc.set(descIO.getLongdesc());
		sv.reportag.set(aglfIO.getReportag());
		if (isNE(sv.reportag, SPACES)) {
			agntlagIO.setAgntcoy(aglfIO.getAgntcoy());
			agntlagIO.setAgntnum(sv.reportag);
			agntlagIO.setFormat(formatsInner.agntlagrec);
			agntlagIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, agntlagIO);
			if (isNE(agntlagIO.getStatuz(), varcom.oK)
			&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(agntlagIO.getParams());
				fatalError600();
			}
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntnum(agntlagIO.getClntnum());
			cltsioCall1700();
			//#ILIFE-1079
			sv.reportton.set(wsspcomn.longconfname);
			//#ILIFE-1079
		}
		sv.dteapp.set(aglfIO.getDteapp());
		sv.agntbr.set(wsspcomn.branch);
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(sv.agntbr);
		descIO.setFunction(varcom.readr);
		descioCall1600();
		sv.agbrdesc.set(descIO.getLongdesc());
		agntlagIO.setAgntcoy(aglfIO.getAgntcoy());
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		agntlagIO.setFormat(formatsInner.agntlagrec);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)
		&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		sv.clntsel.set(agntlagIO.getClntnum());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(sv.clntsel);
		cltsioCall1700();
		sv.cltname.set(wsspcomn.longconfname);
		sv.agtype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		/* MOVE TM602                  TO DESC-DESCTABL.                */
		descIO.setDesctabl(t3692);
		descIO.setDescitem(sv.agtype);
		descIO.setFunction(varcom.readr);
		descioCall1600();
		sv.agtydesc.set(descIO.getLongdesc());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		if (isNE(wsspcomn.flag, "D")) {
			/*    MOVE 'Y'                 TO SM507-AGENTNO-OUT(PR)         */
			/*    MOVE 'Y'                 TO SM507-AGENTNO-OUT(ND)         */
			sv.agntfmOut[varcom.pr.toInt()].set("Y");
			sv.agntfmOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void descioCall1600()
	{
		description1610();
	}

protected void description1610()
	{
		/*    Call the Description I/O module*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void cltsioCall1700()
	{
		/*DESCRIPTION*/
		/*    Call the Clients Description I/O module*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Call the Subroutine to format the Clients Name.*/
		plainname();
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "Z")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'SM507IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SM507-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "Z")) {
			readMacfprd2300();
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		initialize(sv.tydesc);
		//#ILIFE-1079
		initialize(sv.repname);
		//#ILIFE-1079
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set(SPACES);
		}
		/*    Validate fields*/
		displayText2100();
		/* IF SM507-MLAGTTYP            = SPACES                        */
		if (isEQ(sv.accountType, SPACES)) {
			/*    MOVE E186                TO SM507-MLAGTTYP-ERR            */
			sv.atypeErr.set(errorsInner.e186);
			wsspcomn.edterror.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		if (promotion.isTrue()
		|| demotion.isTrue()
		|| transfer.isTrue()) {
			itemIO.setStatuz(varcom.oK);
			itemIO.setItempfx("IT");
			itemIO.setItemtabl(tm605);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemitem(sv.agtype);
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
				/*       MOVE ML02             TO SM507-MLAGTTYP-ERR            */
				sv.atypeErr.set(errorsInner.ml02);
				wsspcomn.edterror.set(SPACES);
				goTo(GotoLabel.exit2090);
			}
			tm605rec.tm605Rec.set(itemIO.getGenarea());
		}
		if (promotion.isTrue()) {
			/*    IF SM507-MLAGTTYP      = TM605-MLAGTTYP-01 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-02 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-03 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-04 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-05 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-06                */
			if (isEQ(sv.accountType, tm605rec.mlagttyp01)
			|| isEQ(sv.accountType, tm605rec.mlagttyp02)
			|| isEQ(sv.accountType, tm605rec.mlagttyp03)
			|| isEQ(sv.accountType, tm605rec.mlagttyp04)
			|| isEQ(sv.accountType, tm605rec.mlagttyp05)
			|| isEQ(sv.accountType, tm605rec.mlagttyp06)) {
				if (isEQ(sv.mlprcind, SPACES)) {
					/*      IF SM507-MLAGTTYP    = TM605-MLAGTTYP-01                */
					if (isEQ(sv.accountType, tm605rec.mlagttyp01)) {
						sv.mlprcind.set(tm605rec.mlprcind01);
					}
					else {
						/*         IF SM507-MLAGTTYP    = TM605-MLAGTTYP-02             */
						if (isEQ(sv.accountType, tm605rec.mlagttyp02)) {
							sv.mlprcind.set(tm605rec.mlprcind02);
						}
						else {
							/*            IF SM507-MLAGTTYP  = TM605-MLAGTTYP-03            */
							if (isEQ(sv.accountType, tm605rec.mlagttyp03)) {
								sv.mlprcind.set(tm605rec.mlprcind03);
							}
							else {
								/*              IF SM507-MLAGTTYP = TM605-MLAGTTYP-04           */
								if (isEQ(sv.accountType, tm605rec.mlagttyp04)) {
									sv.mlprcind.set(tm605rec.mlprcind04);
								}
								else {
									/*                 IF SM507-MLAGTTYP  = TM605-MLAGTTYP-05       */
									if (isEQ(sv.accountType, tm605rec.mlagttyp05)) {
										sv.mlprcind.set(tm605rec.mlprcind05);
									}
									else {
										/*                  IF SM507-MLAGTTYP = TM605-MLAGTTYP-06       */
										if (isEQ(sv.accountType, tm605rec.mlagttyp06)) {
											sv.mlprcind.set(tm605rec.mlprcind06);
										}
									}
								}
							}
						}
					}
				}
			}
			else {
				/*       MOVE P109             TO SM507-MLAGTTYP-ERR            */
				sv.atypeErr.set(errorsInner.p109);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (demotion.isTrue()) {
			/*    IF SM507-MLAGTTYP      = TM605-MLAGTTYP-07 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-08 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-09 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-10 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-11 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-12                */
			if (isEQ(sv.accountType, tm605rec.mlagttyp07)
			|| isEQ(sv.accountType, tm605rec.mlagttyp08)
			|| isEQ(sv.accountType, tm605rec.mlagttyp09)
			|| isEQ(sv.accountType, tm605rec.mlagttyp10)
			|| isEQ(sv.accountType, tm605rec.mlagttyp11)
			|| isEQ(sv.accountType, tm605rec.mlagttyp12)) {
				/*NEXT_SENTENCE*/
			}
			else {
				/*       MOVE P109             TO SM507-MLAGTTYP-ERR            */
				sv.atypeErr.set(errorsInner.p109);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (transfer.isTrue()) {
			/*    IF SM507-MLAGTTYP      = TM605-MLAGTTYP-13 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-14 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-15 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-16 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-17 OR             */
			/*       SM507-MLAGTTYP      = TM605-MLAGTTYP-18                */
			if (isEQ(sv.accountType, tm605rec.mlagttyp13)
			|| isEQ(sv.accountType, tm605rec.mlagttyp14)
			|| isEQ(sv.accountType, tm605rec.mlagttyp15)
			|| isEQ(sv.accountType, tm605rec.mlagttyp16)
			|| isEQ(sv.accountType, tm605rec.mlagttyp17)
			|| isEQ(sv.accountType, tm605rec.mlagttyp18)) {
				/*NEXT_SENTENCE*/
			}
			else {
				/*       MOVE P109           TO SM507-MLAGTTYP-ERR              */
				sv.atypeErr.set(errorsInner.p109);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (promotion.isTrue()) {
			if (isEQ(sv.mlprcind, "Y")) {
				sv.mlprcindOut[varcom.nd.toInt()].set("N");
				/*          MOVE 'N'            TO SM507-MLPRCIND-OUT(PR)*/
				sv.mlprcindOut[varcom.pr.toInt()].set("Y");
				sv.agntselOut[varcom.nd.toInt()].set("N");
				sv.agntselOut[varcom.pr.toInt()].set("N");
				checkParallel2100();
			}
			if (isEQ(sv.mlprcind, "O")) {
				sv.mlprcind.set("Y");
				sv.mlprcindOut[varcom.nd.toInt()].set("N");
				sv.mlprcindOut[varcom.pr.toInt()].set("N");
				sv.agntselOut[varcom.nd.toInt()].set("N");
				sv.agntselOut[varcom.pr.toInt()].set("N");
				checkParallel2100();
			}
		}
		if (isEQ(wsspcomn.flag, "D")) {
			if (isEQ(wsaaRepnum, SPACES)
			&& isEQ(sv.agntfm, SPACES)) {
				/*           MOVE F692          TO SM507-AGENTNO-ERR            */
				sv.agntfmErr.set(errorsInner.f692);
				wsspcomn.edterror.set(SPACES);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void checkParallel2100()
	{
		start2100();
	}

protected void start2100()
	{
		sv.currfrom.set(wsaaToday);
		if (isEQ(sv.agntsel, SPACES)
		&& isNE(sv.mlprcind, "Y")) {
			sv.currfrom.set(varcom.vrcmMaxDate);
			sv.currto.set(varcom.vrcmMaxDate);
			sv.mlparorc.set(ZERO);
			return ;
		}
		if (isEQ(sv.agntsel, SPACES)
		&& isEQ(sv.mlprcind, "Y")) {
			sv.agntselErr.set(errorsInner.h926);
			wsspcomn.edterror.set(SPACES);
			return ;
		}
		if (isEQ(sv.currfrom, varcom.vrcmMaxDate)) {
			sv.currfromErr.set(errorsInner.h926);
			wsspcomn.edterror.set(SPACES);
			return ;
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(sv.agntsel);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			sv.agntselErr.set(errorsInner.f116);
			wsspcomn.edterror.set(SPACES);
			return ;
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.company);
		/* MOVE 'E'                    TO NMAD-LANGUAGE.                */
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		sv.acdes01.set(namadrsrec.name);
		sv.acctype.set(agntIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		/* MOVE TM602                  TO DESC-DESCTABL.                */
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntIO.getAgtype());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.acdes02.set(descIO.getLongdesc());
	}

protected void displayText2100()
	{
		start12100();
	}

protected void start12100()
	{
		descIO.setDataKey(SPACES);
		/* MOVE TM602                  TO DESC-DESCTABL.                */
		descIO.setDesctabl(t3692);
		/* MOVE SM507-MLAGTTYP         TO DESC-DESCITEM.                */
		descIO.setDescitem(sv.accountType);
		descIO.setFunction(varcom.readr);
		descioCall1600();
		sv.tydesc.set(descIO.getLongdesc());
		if (isNE(sv.repsel, SPACES)) {
			agntlagIO.setAgntcoy(aglfIO.getAgntcoy());
			agntlagIO.setAgntnum(sv.repsel);
			agntlagIO.setFormat(formatsInner.agntlagrec);
			agntlagIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, agntlagIO);
			if (isNE(agntlagIO.getStatuz(), varcom.oK)
			&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(agntlagIO.getParams());
				fatalError600();
			}
			if (isEQ(agntlagIO.getStatuz(), varcom.oK)) {
				cltsIO.setDataKey(SPACES);
				cltsIO.setClntnum(agntlagIO.getClntnum());
				cltsioCall1700();
				//#ILIFE-1079
				sv.repname.set(wsspcomn.longconfname);
				//#ILIFE-1079
			}
			else {
				sv.repselErr.set(errorsInner.g772);
				wsspcomn.edterror.set(SPACES);
			}
		}
		/* IF SM507-AGENTNO       NOT = SPACES                          */
		if (isNE(sv.agntfm, SPACES)) {
			agntlagIO.setAgntcoy(aglfIO.getAgntcoy());
			/*    MOVE SM507-AGENTNO     TO AGNTLAG-AGNTNUM                 */
			agntlagIO.setAgntnum(sv.agntfm);
			agntlagIO.setFormat(formatsInner.agntlagrec);
			agntlagIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, agntlagIO);
			if (isNE(agntlagIO.getStatuz(), varcom.oK)
			&& isNE(agntlagIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(agntlagIO.getParams());
				fatalError600();
			}
			if (isEQ(agntlagIO.getStatuz(), varcom.oK)) {
				cltsIO.setDataKey(SPACES);
				cltsIO.setClntnum(agntlagIO.getClntnum());
				cltsioCall1700();
				sv.descn.set(wsspcomn.longconfname);
			}
			else {
				/*       MOVE G772             TO SM507-AGENTNO-ERR             */
				sv.agntfmErr.set(errorsInner.g772);
				wsspcomn.edterror.set(SPACES);
			}
		}
		if (isNE(sv.repsel, SPACES)) {
			if (isEQ(sv.repsel, sv.agnum)) {
				sv.repselErr.set(errorsInner.e454);
				wsspcomn.edterror.set(SPACES);
			}
		}
	}

protected void readMacfprd2300()
	{
		start2300();
	}

protected void start2300()
	{
		macfprdIO.setAgntcoy(aglfIO.getAgntcoy());
		macfprdIO.setAgntnum(sv.agnum);
		macfprdIO.setEffdate(wsaaToday);
		macfprdIO.setAgmvty(SPACES);
		macfprdIO.setStatuz(varcom.oK);
		macfprdIO.setFormat(formatsInner.macfprdrec);
		macfprdIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		macfprdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfprdIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		SmartFileCode.execute(appVars, macfprdIO);
		if (isNE(macfprdIO.getStatuz(), varcom.oK)
		&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
		if (isNE(sv.agnum, macfprdIO.getAgntnum())
		|| isNE(aglfIO.getAgntcoy(), macfprdIO.getAgntcoy())
		|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
		if (isNE(macfprdIO.getAgmvty(), "T")
		|| isNE(macfprdIO.getAgmvty(), "P")
		|| isNE(macfprdIO.getAgmvty(), "D")) {
			scrnparams.errorCode.set(errorsInner.ml03);
			wsspcomn.edterror.set(SPACES);
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
		aglfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag, "Z")) {
			scrnparams.function.set(varcom.prot);
		}
		if (demotion.isTrue()) {
			demoteAgent3400();
		}
		if (isEQ(wsspcomn.flag, "P")
		|| isEQ(wsspcomn.flag, "D")
		|| isEQ(wsspcomn.flag, "M")) {
			updateAgent3200();
		}
		/*    IF DEMOTION                                                  */
		/*       PERFORM 3400-DEMOTE-AGENT                                 */
		/*    END-IF.                                                      */
		if (isEQ(wsspcomn.flag, "Z")) {
			reversalAgent3300();
		}
		/*    IF PROMOTION OR TRANSFER                                     */
		/*       PERFORM 3800-AFFECTED-AGENT                               */
		/*    END-IF.                                                      */
		/* Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(sv.agnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsspcomn.batchkey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*    IF NOT DEMOTION                                              */
		/*       GO TO 3090-EXIT                                           */
		/*    END-IF.                                                      */
		/*       the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PM507AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.company.set(wsspcomn.company);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryKey.set(SPACES);
		wsaaPrimaryChdrnum.set(sv.agnum);
		wsaaTransactionRecInner.wsaaDemotedAgent.set(sv.agnum);
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaTransactionRecInner.wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionRecInner.wsaaEffdate.set(wsaaToday);
		wsaaTransactionRecInner.wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaTransactionRecInner.wsaaUser.set(varcom.vrcmUser);
		wsaaTransactionRecInner.wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionRecInner.wsaaAgentnum.set(SPACES);
		wsaaTransactionRecInner.wsaaAgentnum.set(sv.repsel);
		wsaaTransactionRecInner.wsaaNewagt.set(SPACES);
		wsaaTransactionRecInner.wsaaNewagt.set(wsaaRecruited);
		wsaaTransactionRecInner.wsaaServcom.set("Y");
		wsaaTransactionRecInner.wsaaRnwlcom.set("Y");
		wsaaTransactionRecInner.wsaaInalcom.set("Y");
		wsaaTransactionRecInner.wsaaComind.set("Y");
		wsaaTransactionRecInner.wsaaZrorcomm.set("Y");
		wsaaTransactionRecInner.wsaaBatcactmn.set(wsspcomn.acctmonth);
		wsaaTransactionRecInner.wsaaBatcactyr.set(wsspcomn.acctyear);
		atreqrec.transArea.set(wsaaTransactionRecInner.wsaaTransactionRec);
		atreqrec.statuz.set("****");
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

protected void getReportag3100()
	{
		reportag3100();
	}

protected void reportag3100()
	{
		/*    MOVE AGLFLNB-REPORTAG       TO AGLFLNB-AGNTNUM.              */
		aglflnbIO.setAgntnum(wsbbReportag);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(), varcom.oK)) {
			wsaaReportag[wsaaIndex.toInt()].set(aglflnbIO.getAgntnum());
			wsaaIndex.add(1);
			wsbbReportag.set(aglflnbIO.getReportag());
		}
	}

protected void updateAgent3200()
	{
		start3200();
	}

protected void start3200()
	{
		macfIO.setDataArea(SPACES);
		wsaaTranno.set(ZERO);
		macfIO.setAgntcoy(aglfIO.getAgntcoy());
		macfIO.setAgntnum(sv.agnum);
		macfIO.setAgmvty(SPACES);
		macfIO.setEffdate(wsaaToday);
		macfIO.setTranno(99999);
		macfIO.setFunction(varcom.begnh);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		if (isEQ(macfIO.getStatuz(), varcom.oK)
		&& isEQ(macfIO.getAgntcoy(), aglfIO.getAgntcoy())
		&& isEQ(macfIO.getAgntnum(), sv.agnum)
		&& isEQ(macfIO.getCurrto(), varcom.vrcmMaxDate)) {
			macfIO.setCurrto(wsaaToday);
			wsaaTranno.set(macfIO.getTranno());
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				fatalError600();
			}
		}
		/*                                                         <LA1174>*/
		macfIO.setDataArea(SPACES);
		macfIO.setAgmvty(wsspcomn.flag);
		wsaaTranno.add(1);
		macfIO.setTranno(wsaaTranno);
		macfIO.setAgntcoy(aglfIO.getAgntcoy());
		macfIO.setAgntnum(sv.agnum);
		/* MOVE SM507-MLAGTTYP         TO MACF-MLAGTTYP.                */
		macfIO.setMlagttyp(sv.accountType);
		macfIO.setEffdate(wsaaToday);
		/*    MOVE SM507-CURRFROM         TO MACF-CURRFROM.                */
		/*    MOVE SM507-CURRTO           TO MACF-CURRTO.                  */
		macfIO.setCurrfrom(wsaaToday);
		macfIO.setCurrto(varcom.vrcmMaxDate);
		macfIO.setMlparagt(sv.agntsel);
		macfIO.setMlparorc(sv.mlparorc);
		wsaaIndex.set(1);
		if (isNE(sv.repsel, SPACES)) {
			/*       MOVE SM507-REPSEL        TO AGLFLNB-REPORTAG              */
			wsbbReportag.set(sv.repsel);
			while ( !(isEQ(wsbbReportag, SPACES))) {
				getReportag3100();
			}
			
			/*              UNTIL AGLFLNB-REPORTAG = SPACES                    */
		}
		wsaaIndex.set(1);
		/*    PERFORM VARYING WSAA-INDEX FROM 1 BY 1                       */
		/*         UNTIL WSAA-INDEX > 4                                    */
		/*       MOVE WSAA-REPORTAG(WSAA-INDEX)                            */
		/*                        TO MACF-REPORTAG(WSAA-INDEX)             */
		/*    END-PERFORM.                                                 */
		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setFunction(varcom.writr);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agnum);
		aglfIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* IF AGLF-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGLF-VALIDFLAG        <V76F10>*/
		/*    MOVE AGLFREC             TO AGLF-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGLF-FUNCTION         <V76F10>*/
		/*    CALL 'AGLFIO'         USING AGLF-PARAMS           <V76F10>*/
		/*    IF AGLF-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGLF-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGLF-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		aglfIO.setReportag(sv.repsel);
		/* MOVE WSSP-COMPANY           TO AGLF-AGNTCOY.                 */
		/* MOVE SM507-AGNUM            TO AGLF-AGNTNUM.                 */
		/* MOVE UPDAT                  TO AGLF-FUNCTION.                */
		/* MOVE REWRT                  TO AGLF-FUNCTION.        <V73F02>*/
		aglfIO.setFunction(varcom.rewrt);
		/* MOVE '1'                    TO AGLF-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGLF-FUNCTION.        <V76F10>*/
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(sv.agnum);
		agntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* IF AGNT-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGNT-VALIDFLAG        <V76F10>*/
		/*    MOVE AGNTREC             TO AGNT-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGNT-FUNCTION         <V76F10>*/
		/*    CALL 'AGNTIO'         USING AGNT-PARAMS           <V76F10>*/
		/*    IF AGNT-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGNT-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGNT-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		wsaaAgntpfx.set("AG");
		wsaaAgntcoy.set(wsspcomn.company);
		wsaaAgntnum.set(sv.repsel);
		agntIO.setRepagent01(wsaaAglfrptKey);
		/* MOVE SM507-MLAGTTYP         TO AGNT-AGTYPE.                  */
		agntIO.setAgtype(sv.accountType);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(sv.agnum);
		/* MOVE REWRT                  TO AGNT-FUNCTION.                */
		/* MOVE '1'                    TO AGNT-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGNT-FUNCTION.        <V76F10>*/
		agntIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
	}

protected void reversalAgent3300()
	{
		start3300();
	}

protected void start3300()
	{
		/*  To get latest MACFPRD details and update back the previous*/
		/*  agent type , reported agent & guarantor to AGLF & AGNT and*/
		/*  delete the MACFPRD record.*/
		macfprdIO.setAgntcoy(aglfIO.getAgntcoy());
		macfprdIO.setAgntnum(sv.agnum);
		macfprdIO.setEffdate(wsaaToday);
		macfprdIO.setAgmvty(SPACES);
		macfprdIO.setStatuz(varcom.oK);
		macfprdIO.setFormat(formatsInner.macfprdrec);
		macfprdIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, macfprdIO);
		if (isNE(macfprdIO.getStatuz(), varcom.oK)
		&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
		if (isNE(sv.agnum, macfprdIO.getAgntnum())
		|| isNE(aglfIO.getAgntcoy(), macfprdIO.getAgntcoy())
		|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(macfprdIO.getAgntnum());
		agntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* IF AGNT-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGNT-VALIDFLAG        <V76F10>*/
		/*    MOVE AGNTREC             TO AGNT-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGNT-FUNCTION         <V76F10>*/
		/*    CALL 'AGNTIO'         USING AGNT-PARAMS           <V76F10>*/
		/*    IF AGNT-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGNT-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGNT-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		agntIO.setAgtype(macfprdIO.getMlagttyp());
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(macfprdIO.getAgntnum());
		/* MOVE REWRT                  TO AGNT-FUNCTION.                */
		/* MOVE '2'                    TO AGNT-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGNT-FUNCTION.        <V76F10>*/
		agntIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		macfprdIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, macfprdIO);
		if (isNE(macfprdIO.getStatuz(), varcom.oK)
		&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
	}

protected void demoteAgent3400()
	{
		/*START*/
		maglfrpIO.setStatuz(varcom.oK);
		maglfrpIO.setReportag(sv.agnum);
		maglfrpIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		maglfrpIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		if (isNE(sv.agntfm,SPACES)) {
			wsaaRecruited.set(sv.agntfm);
		}
		else {
			wsaaRecruited.set(wsaaRepnum);
		}
		while ( !(isEQ(maglfrpIO.getStatuz(), varcom.endp))) {
			updateDemote3450();
		}
		
		/*EXIT*/
	}

protected void updateDemote3450()
	{
		try {
			start3450();
			nextr3450();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3450()
	{
		SmartFileCode.execute(appVars, maglfrpIO);
		if (isNE(maglfrpIO.getStatuz(), varcom.oK)
		&& isNE(maglfrpIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(maglfrpIO.getStatuz());
			syserrrec.params.set(maglfrpIO.getParams());
			fatalError600();
		}
		if (isNE(maglfrpIO.getReportag(), sv.agnum)
		|| isEQ(maglfrpIO.getStatuz(), varcom.endp)) {
			maglfrpIO.setStatuz(varcom.endp);
		}
		if (isEQ(maglfrpIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit3450);
		}
		aglfIO.setAgntcoy(maglfrpIO.getAgntcoy());
		aglfIO.setAgntnum(maglfrpIO.getAgntnum());
		aglfIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* IF AGLF-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGLF-VALIDFLAG        <V76F10>*/
		/*    MOVE AGLFREC             TO AGLF-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGLF-FUNCTION         <V76F10>*/
		/*    CALL 'AGLFIO'         USING AGLF-PARAMS           <V76F10>*/
		/*    IF AGLF-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGLF-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGLF-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		aglfIO.setReportag(wsaaRecruited);
		/* MOVE UPDAT                  TO AGLF-FUNCTION.                */
		/* MOVE '1'                    TO AGLF-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGLF-FUNCTION.        <V76F10>*/
		aglfIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(maglfrpIO.getAgntnum());
		agntIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*                                                      <V76F10>*/
		/* IF AGNT-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGNT-VALIDFLAG        <V76F10>*/
		/*    MOVE AGNTREC             TO AGNT-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGNT-FUNCTION         <V76F10>*/
		/*    CALL 'AGNTIO'         USING AGNT-PARAMS           <V76F10>*/
		/*    IF AGNT-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGNT-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGNT-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/*    END-IF                                            <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		wsaaAgntpfx.set("AG");
		wsaaAgntcoy.set(wsspcomn.company);
		wsaaAgntnum.set(wsaaRecruited);
		agntIO.setRepagent01(wsaaAglfrptKey);
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntpfx("AG");
		agntIO.setAgntnum(maglfrpIO.getAgntnum());
		/* MOVE REWRT                  TO AGNT-FUNCTION.                */
		/* MOVE '1'                    TO AGNT-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGNT-FUNCTION.        <V76F10>*/
		agntIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)
		&& isNE(agntIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
	}

protected void nextr3450()
	{
		maglfrpIO.setFunction(varcom.nextr);
	}

protected void affectedAgent3800()
	{
		start3800();
	}

protected void start3800()
	{
		macfflvIO.setParams(SPACES);
		macfflvIO.setAgntcoy(aglfIO.getAgntcoy());
		/*    MOVE SM507-AGNUM            TO MACFFLV-REPORTAG01.           */
		macfflvIO.setZrptga(sv.agnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		macfflvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfflvIO.setFitKeysSearch("AGNTCOY", "ZRPTGA");

		while ( !(isEQ(macfflvIO.getStatuz(),varcom.endp))) {
			n1stLevel3810();
		}

		macfslvIO.setParams(SPACES);
		macfslvIO.setAgntcoy(aglfIO.getAgntcoy());
		/*    MOVE SM507-AGNUM            TO MACFSLV-REPORTAG02.           */
		macfslvIO.setZrptgb(sv.agnum);
		macfslvIO.setAgntnum(SPACES);
		macfslvIO.setEffdate(varcom.vrcmMaxDate);
		macfslvIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		macfslvIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfslvIO.setFitKeysSearch("AGNTCOY", "ZRPTGB");

		while ( !(isEQ(macfslvIO.getStatuz(),varcom.endp))) {
			n2ndLevel3820();
		}
		
	}

protected void n1stLevel3810()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3810();
				case nextr3810: 
					nextr3810();
				case exit3810: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3810()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		/*    IF MACFFLV-REPORTAG01    NOT = SM507-AGNUM                   */
		if (isNE(macfflvIO.getZrptga(), sv.agnum)
		|| isNE(macfflvIO.getAgntcoy(), aglfIO.getAgntcoy())) {
			macfflvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit3810);
		}
		if (isNE(macfflvIO.getAgntnum(), wsaaFlvlagt)) {
			wsaaFlvlagt.set(macfflvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.nextr3810);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);

		//performance improvement -- Anjali
		macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		macfinqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(), varcom.oK)
		&& isNE(macfinqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			fatalError600();
		}
		if (isNE(macfinqIO.getAgntnum(), macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(), macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3810);
		}
		/*    IF MACFINQ-REPORTAG01    NOT = MACFFLV-REPORTAG01            */
		if (isNE(macfinqIO.getZrptga(), macfflvIO.getZrptga())) {
			goTo(GotoLabel.nextr3810);
		}
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(macfflvIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(macfflvIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		macfIO.setDataArea(SPACES);
		macfIO.setMlparagt(SPACES);
		macfIO.setCurrfrom(ZERO);
		macfIO.setCurrto(ZERO);
		macfIO.setMlparorc(ZERO);
		macfIO.setCurrfrom(varcom.vrcmMaxDate);
		macfIO.setCurrto(varcom.vrcmMaxDate);
		macfIO.setAgmvty("F");
		macfIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfIO.setAgntnum(macfflvIO.getAgntnum());
		macfIO.setMlagttyp(agntIO.getAgtype());
		macfIO.setEffdate(wsaaToday);
		initialize(wsaaReportags);
		/*    MOVE AGLF-REPORTAG          TO AGLFLNB-REPORTAG.             */
		wsbbReportag.set(aglfIO.getReportag());
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag, SPACES))) {
			getReportag3100();
		}
		
		/*            UNTIL AGLFLNB-REPORTAG = SPACES.                     */
		/*    PERFORM VARYING WSAA-INDEX FROM 1 BY 1                       */
		/*         UNTIL WSAA-INDEX > 4                                    */
		/*       MOVE WSAA-REPORTAG(WSAA-INDEX)                            */
		/*                        TO MACF-REPORTAG(WSAA-INDEX)             */
		/*    END-PERFORM.                                                 */
		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setFunction(varcom.writr);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
	}

protected void nextr3810()
	{
		macfflvIO.setFunction(varcom.nextr);
	}

protected void n2ndLevel3820()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start13810();
				case nextr3820: 
					nextr3820();
				case exit3820: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start13810()
	{
		SmartFileCode.execute(appVars, macfslvIO);
		if (isNE(macfslvIO.getStatuz(), varcom.oK)
		&& isNE(macfslvIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfslvIO.getStatuz());
			syserrrec.params.set(macfslvIO.getParams());
			fatalError600();
		}
		/*    IF MACFSLV-REPORTAG02   NOT = SM507-AGNUM                    */
		if (isNE(macfslvIO.getZrptgb(), sv.agnum)
		|| isNE(macfslvIO.getAgntcoy(), aglfIO.getAgntcoy())) {
			macfslvIO.setStatuz(varcom.endp);
		}
		if (isEQ(macfslvIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit3820);
		}
		macfinqIO.setAgntcoy(macfflvIO.getAgntcoy());
		macfinqIO.setAgntnum(macfflvIO.getAgntnum());
		macfinqIO.setEffdate(varcom.vrcmMaxDate);

		//performance improvement -- Anjali
		macfinqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfinqIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

		macfinqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfinqIO);
		if (isNE(macfinqIO.getStatuz(), varcom.oK)
		&& isNE(macfinqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfinqIO.getStatuz());
			syserrrec.params.set(macfinqIO.getParams());
			fatalError600();
		}
		if (isNE(macfinqIO.getAgntnum(), macfflvIO.getAgntnum())
		|| isNE(macfinqIO.getAgntcoy(), macfflvIO.getAgntcoy())) {
			goTo(GotoLabel.nextr3820);
		}
		/*    IF MACFINQ-REPORTAG02    NOT = MACFFLV-REPORTAG02            */
		if (isNE(macfinqIO.getZrptgb(), macfflvIO.getZrptgb())) {
			goTo(GotoLabel.nextr3820);
		}
		if (isNE(macfslvIO.getAgntnum(), wsaaSlvlagt)) {
			wsaaSlvlagt.set(macfslvIO.getAgntnum());
		}
		else {
			goTo(GotoLabel.nextr3820);
		}
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(macfslvIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(macfslvIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		macfIO.setDataArea(SPACES);
		macfIO.setMlparagt(SPACES);
		macfIO.setCurrfrom(ZERO);
		macfIO.setCurrto(ZERO);
		macfIO.setMlparorc(ZERO);
		macfIO.setAgmvty("F");
		macfIO.setAgntcoy(macfslvIO.getAgntcoy());
		macfIO.setAgntnum(macfslvIO.getAgntnum());
		macfIO.setMlagttyp(agntIO.getAgtype());
		macfIO.setEffdate(wsaaToday);
		initialize(wsaaReportags);
		wsaaIndex.set(1);
		/*    MOVE AGLF-REPORTAG          TO AGLFLNB-REPORTAG.             */
		wsbbReportag.set(aglfIO.getReportag());
		while ( !(isEQ(wsbbReportag, SPACES))) {
			getReportag3100();
		}
		
		/*            UNTIL AGLFLNB-REPORTAG = SPACES.                     */
		/*    PERFORM VARYING WSAA-INDEX FROM 1 BY 1                       */
		/*         UNTIL WSAA-INDEX > 4                                    */
		/*       MOVE WSAA-REPORTAG(WSAA-INDEX)                            */
		/*                        TO MACF-REPORTAG(WSAA-INDEX)             */
		/*    END-PERFORM.                                                 */
		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setFunction(varcom.readh);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		if (isEQ(macfIO.getStatuz(), varcom.oK)) {
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(), varcom.oK)
			&& isNE(macfIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				fatalError600();
			}
			else {
				goTo(GotoLabel.nextr3820);
			}
		}
		macfIO.setFunction(varcom.writr);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
	}

protected void nextr3820()
	{
		macfslvIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner { 

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(222);
	private FixedLengthStringData wsaaDemotedAgent = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 0).init(SPACES);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 12);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 16);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 20);
	private FixedLengthStringData wsaaAgentnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 24).init(SPACES);
	private FixedLengthStringData wsaaNewagt = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 32).init(SPACES);
	private FixedLengthStringData wsaaServcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 40).init(SPACES);
	private FixedLengthStringData wsaaRnwlcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 41).init(SPACES);
	private FixedLengthStringData wsaaInalcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 42).init(SPACES);
	private FixedLengthStringData wsaaComind = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 43).init(SPACES);
	private FixedLengthStringData wsaaZrorcomm = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 44).init(SPACES);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 45).init(ZERO);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 47).init(ZERO);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 49).init(ZERO).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(165).isAPartOf(wsaaTransactionRec, 57, FILLER).init(SPACES);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData p109 = new FixedLengthStringData(4).init("P109");
	private FixedLengthStringData ml02 = new FixedLengthStringData(4).init("ML02");
	private FixedLengthStringData ml03 = new FixedLengthStringData(4).init("ML03");
	private FixedLengthStringData g772 = new FixedLengthStringData(4).init("G772");
	private FixedLengthStringData f692 = new FixedLengthStringData(4).init("F692");
	private FixedLengthStringData f116 = new FixedLengthStringData(4).init("F116");
	private FixedLengthStringData e454 = new FixedLengthStringData(4).init("E454");
	private FixedLengthStringData h926 = new FixedLengthStringData(4).init("H926");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData agntlagrec = new FixedLengthStringData(10).init("AGNTLAGREC");
	private FixedLengthStringData macfrec = new FixedLengthStringData(10).init("MACFREC");
	private FixedLengthStringData macfprdrec = new FixedLengthStringData(10).init("MACFPRDREC");
}
}
