package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5042
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5042ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(492);
	public FixedLengthStringData dataFields = new FixedLengthStringData(204).isAPartOf(dataArea, 0);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData inalcom = DD.inalcom.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData newagntnm = DD.newagntnm.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData newagt = DD.newagt.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData rnwlcom = DD.rnwlcom.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData servcom = DD.servcom.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData zrorcomm = DD.zrorcomm.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,114);
	public FixedLengthStringData servagnam = DD.servagnam.copy().isAPartOf(dataFields,117);
	public FixedLengthStringData servagnt = DD.servagnt.copy().isAPartOf(dataFields,164);
	public FixedLengthStringData servbr = DD.servbr.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData servbrname = DD.servbrname.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 204);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData inalcomErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData newagntnmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData newagtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData rnwlcomErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData servcomErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zrorcommErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData servagnamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData servagntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData servbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData servbrnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 276);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] inalcomOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] newagntnmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] newagtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] rnwlcomOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] servcomOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] zrorcommOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] servagnamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] servagntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] servbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] servbrnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(113);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(47).isAPartOf(subfileArea, 0);
	public FixedLengthStringData comagnt = DD.comagnt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData comagntnm = DD.comagntnm.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,45);
	public FixedLengthStringData sflchg = DD.sflchg.copy().isAPartOf(subfileFields,46);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 47);
	public FixedLengthStringData comagntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData comagntnmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData sflchgErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 63);
	public FixedLengthStringData[] comagntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] comagntnmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] sflchgOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 111);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5042screensflWritten = new LongData(0);
	public LongData S5042screenctlWritten = new LongData(0);
	public LongData S5042screenWritten = new LongData(0);
	public LongData S5042protectWritten = new LongData(0);
	public GeneralTable s5042screensfl = new GeneralTable(AppVars.getInstance());
	
	public FixedLengthStringData iljScreenflag = new FixedLengthStringData(1);  //ILJ-9

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5042screensfl;
	}

	public S5042ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(newagtOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(servcomOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rnwlcomOut,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(inalcomOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorcommOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, comagnt, comagntnm, sflchg};
		screenSflOutFields = new BaseData[][] {selectOut, comagntOut, comagntnmOut, sflchgOut};
		screenSflErrFields = new BaseData[] {selectErr, comagntErr, comagntnmErr, sflchgErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cntcurr, chdrstatus, premstatus, register, servagnt, servbr, servagnam, servbrname, newagt, servcom, rnwlcom, inalcom, newagntnm, comind, zrorcomm};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, chdrstatusOut, premstatusOut, registerOut, servagntOut, servbrOut, servagnamOut, servbrnameOut, newagtOut, servcomOut, rnwlcomOut, inalcomOut, newagntnmOut, comindOut, zrorcommOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, chdrstatusErr, premstatusErr, registerErr, servagntErr, servbrErr, servagnamErr, servbrnameErr, newagtErr, servcomErr, rnwlcomErr, inalcomErr, newagntnmErr, comindErr, zrorcommErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5042screen.class;
		screenSflRecord = S5042screensfl.class;
		screenCtlRecord = S5042screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5042protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5042screenctl.lrec.pageSubfile);
	}
}
