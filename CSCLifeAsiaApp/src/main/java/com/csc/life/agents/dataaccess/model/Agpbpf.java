/*

 * File: AgpbpfDAOImpl.java
 * Date: August 16, 2019
 * Author: DXC
 * Created by: sbatra9
 * 
 * Copyright DXC, all rights reserved.
 */

package com.csc.life.agents.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

public class Agpbpf {

	private long uniqueNumber;
	private String rldgcoy;
	private String rldgacct;
	private BigDecimal sacscurbal;
	private String procflg;
	private String tagsusind;
	private String taxcde;
	private String currcode;
	private String agccqind;
	private String payclt;
	private String facthous;
	private String bankkey;
	private String bankacckey;
	private BigDecimal whtax;
	private BigDecimal tcolbal;
	private BigDecimal tdeduct;
	private BigDecimal tdcchrg;
	private String paymth;
	private String usrprf;
	private String jobnm;
	private Date datime;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	
	public String getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(String rldgcoy) {
		this.rldgcoy = rldgcoy;
	}
	public String getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(String rldgacct) {
		this.rldgacct = rldgacct;
	}
	public BigDecimal getSacscurbal() {
		return sacscurbal;
	}
	public void setSacscurbal(BigDecimal sacscurbal) {
		this.sacscurbal = sacscurbal;
	}
	public String getProcflg() {
		return procflg;
	}
	public void setProcflg(String procflg) {
		this.procflg = procflg;
	}
	public String getTagsusind() {
		return tagsusind;
	}
	public void setTagsusind(String tagsusind) {
		this.tagsusind = tagsusind;
	}
	public String getTaxcde() {
		return taxcde;
	}
	public void setTaxcde(String taxcde) {
		this.taxcde = taxcde;
	}
	public String getCurrcode() {
		return currcode;
	}
	public void setCurrcode(String currcode) {
		this.currcode = currcode;
	}
	public String getAgccqind() {
		return agccqind;
	}
	public void setAgccqind(String agccqind) {
		this.agccqind = agccqind;
	}
	public String getPayclt() {
		return payclt;
	}
	public void setPayclt(String payclt) {
		this.payclt = payclt;
	}
	public String getFacthous() {
		return facthous;
	}
	public void setFacthous(String facthous) {
		this.facthous = facthous;
	}
	public String getBankkey() {
		return bankkey;
	}
	public void setBankkey(String bankkey) {
		this.bankkey = bankkey;
	}
	public String getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(String bankacckey) {
		this.bankacckey = bankacckey;
	}
	public BigDecimal getWhtax() {
		return whtax;
	}
	public void setWhtax(BigDecimal whtax) {
		this.whtax = whtax;
	}
	public BigDecimal getTcolbal() {
		return tcolbal;
	}
	public void setTcolbal(BigDecimal tcolbal) {
		this.tcolbal = tcolbal;
	}
	public BigDecimal getTdeduct() {
		return tdeduct;
	}
	public void setTdeduct(BigDecimal tdeduct) {
		this.tdeduct = tdeduct;
	}
	public BigDecimal getTdcchrg() {
		return tdcchrg;
	}
	public void setTdcchrg(BigDecimal tdcchrg) {
		this.tdcchrg = tdcchrg;
	}
	public String getPaymth() {
		return paymth;
	}
	public void setPaymth(String paymth) {
		this.paymth = paymth;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return datime;
	}
	public void setDatime(Date datime) {
		this.datime = datime;
	}

}