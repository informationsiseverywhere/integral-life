package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:33
 * Description:
 * Copybook name: MACFENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfenqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfenqKey = new FixedLengthStringData(64).isAPartOf(macfenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfenqAgntcoy = new FixedLengthStringData(1).isAPartOf(macfenqKey, 0);
  	public FixedLengthStringData macfenqAgntnum = new FixedLengthStringData(8).isAPartOf(macfenqKey, 1);
  	public PackedDecimalData macfenqEffdate = new PackedDecimalData(8, 0).isAPartOf(macfenqKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(macfenqKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}