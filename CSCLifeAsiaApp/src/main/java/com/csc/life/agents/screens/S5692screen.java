package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5692screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5692ScreenVars sv = (S5692ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5692screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5692ScreenVars screenVars = (S5692ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.age01.setClassString("");
		screenVars.age03.setClassString("");
		screenVars.age02.setClassString("");
		screenVars.age04.setClassString("");
		screenVars.incmrate01.setClassString("");
		screenVars.inprempc01.setClassString("");
		screenVars.incmrate02.setClassString("");
		screenVars.inprempc02.setClassString("");
		screenVars.incmrate04.setClassString("");
		screenVars.inprempc04.setClassString("");
		screenVars.rwcmrate02.setClassString("");
		screenVars.reprempc02.setClassString("");
		screenVars.rwcmrate04.setClassString("");
		screenVars.reprempc04.setClassString("");
		screenVars.rwcmrate01.setClassString("");
		screenVars.reprempc01.setClassString("");
		screenVars.incmrate03.setClassString("");
		screenVars.inprempc03.setClassString("");
		screenVars.rwcmrate03.setClassString("");
		screenVars.reprempc03.setClassString("");
	}

/**
 * Clear all the variables in S5692screen
 */
	public static void clear(VarModel pv) {
		S5692ScreenVars screenVars = (S5692ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.age01.clear();
		screenVars.age03.clear();
		screenVars.age02.clear();
		screenVars.age04.clear();
		screenVars.incmrate01.clear();
		screenVars.inprempc01.clear();
		screenVars.incmrate02.clear();
		screenVars.inprempc02.clear();
		screenVars.incmrate04.clear();
		screenVars.inprempc04.clear();
		screenVars.rwcmrate02.clear();
		screenVars.reprempc02.clear();
		screenVars.rwcmrate04.clear();
		screenVars.reprempc04.clear();
		screenVars.rwcmrate01.clear();
		screenVars.reprempc01.clear();
		screenVars.incmrate03.clear();
		screenVars.inprempc03.clear();
		screenVars.rwcmrate03.clear();
		screenVars.reprempc03.clear();
	}
}
