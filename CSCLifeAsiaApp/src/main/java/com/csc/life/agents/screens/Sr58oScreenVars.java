package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

/**
 * Screen Variables for Sr58o
 * @version 1.0 Generated on Fri Aug 23 13:49:56 SGT 2013
 * @author CSC
 */
public class Sr58oScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(528);
	public FixedLengthStringData dataFields = new FixedLengthStringData(208).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1); 
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9); 
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25); 
	public FixedLengthStringData oppcs = new FixedLengthStringData(10).isAPartOf(dataFields, 55);
	public FixedLengthStringData[] oppc = FLSArrayPartOfStructure(2, 5, oppcs, 0);
	public ZonedDecimalData oppc01 = DD.oppc.copyToZonedDecimal().isAPartOf(oppcs,0); 
	public ZonedDecimalData oppc02 = DD.oppc.copyToZonedDecimal().isAPartOf(oppcs,5); 
	public FixedLengthStringData prcnts = new FixedLengthStringData(30).isAPartOf(dataFields, 65);
	public FixedLengthStringData[] prcnt = FLSArrayPartOfStructure(6, 5, prcnts, 0);
	public ZonedDecimalData prcnt01 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,0); 
	public ZonedDecimalData prcnt02 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,5); 
	public ZonedDecimalData prcnt03 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,10); 
	public ZonedDecimalData prcnt04 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,15); 
	public ZonedDecimalData prcnt05 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,20); 
	public ZonedDecimalData prcnt06 = DD.prcnt.copyToZonedDecimal().isAPartOf(prcnts,25); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,95); 
	public FixedLengthStringData totamnts = new FixedLengthStringData(108).isAPartOf(dataFields, 100);
	public FixedLengthStringData[] totamnt = FLSArrayPartOfStructure(6, 18, totamnts, 0);
	public  ZonedDecimalData totamnt01 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,0); 
	public  ZonedDecimalData totamnt02 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,18); 
	public  ZonedDecimalData totamnt03 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,36); 
	public  ZonedDecimalData totamnt04 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,54); 
	public  ZonedDecimalData totamnt05 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,72); 
	public  ZonedDecimalData totamnt06 = DD.totamnt.copyToZonedDecimal().isAPartOf(totamnts,90); 


	public FixedLengthStringData errorIndicators = new FixedLengthStringData(80).isAPartOf(dataArea, 208);

	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData oppcsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] oppcErr = FLSArrayPartOfStructure(2, 4, oppcsErr, 0);
	  public FixedLengthStringData oppc01Err =new FixedLengthStringData(4).isAPartOf(oppcsErr,0); 
	  public FixedLengthStringData oppc02Err =new FixedLengthStringData(4).isAPartOf(oppcsErr,4); 
	public FixedLengthStringData prcntsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] prcntErr = FLSArrayPartOfStructure(6, 4, prcntsErr, 0);
	  public FixedLengthStringData prcnt01Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,0); 
	  public FixedLengthStringData prcnt02Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,4); 
	  public FixedLengthStringData prcnt03Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,8); 
	  public FixedLengthStringData prcnt04Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,12); 
	  public FixedLengthStringData prcnt05Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,16); 
	  public FixedLengthStringData prcnt06Err =new FixedLengthStringData(4).isAPartOf(prcntsErr,20); 
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData totamntsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] totamntErr = FLSArrayPartOfStructure(6, 4, totamntsErr, 0);
	  public FixedLengthStringData totamnt01Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,0); 
	  public FixedLengthStringData totamnt02Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,4); 
	  public FixedLengthStringData totamnt03Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,8); 
	  public FixedLengthStringData totamnt04Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,12); 
	  public FixedLengthStringData totamnt05Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,16); 
	  public FixedLengthStringData totamnt06Err =new FixedLengthStringData(4).isAPartOf(totamntsErr,20); 


	public FixedLengthStringData outputIndicators = new FixedLengthStringData(240).isAPartOf(dataArea,288);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData oppcsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] oppcOut = FLSArrayPartOfStructure(2, 12, oppcsOut, 0);
	public FixedLengthStringData[][] oppcO = FLSDArrayPartOfArrayStructure(12, 1, oppcOut, 0);
	  public FixedLengthStringData[] oppc01Out = FLSArrayPartOfStructure(12, 1, oppcsOut, 0);
	  public FixedLengthStringData[] oppc02Out = FLSArrayPartOfStructure(12, 1, oppcsOut, 12);
	public FixedLengthStringData prcntsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(6, 12, prcntsOut, 0);
	public FixedLengthStringData[][] prcntO = FLSDArrayPartOfArrayStructure(12, 1, prcntOut, 0);
	  public FixedLengthStringData[] prcnt01Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 0);
	  public FixedLengthStringData[] prcnt02Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 12);
	  public FixedLengthStringData[] prcnt03Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 24);
	  public FixedLengthStringData[] prcnt04Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 36);
	  public FixedLengthStringData[] prcnt05Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 48);
	  public FixedLengthStringData[] prcnt06Out = FLSArrayPartOfStructure(12, 1, prcntsOut, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData totamntsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(6, 12, totamntsOut, 0);
	public FixedLengthStringData[][] totamntO = FLSDArrayPartOfArrayStructure(12, 1, totamntOut, 0);
	  public FixedLengthStringData[] totamnt01Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 0);
	  public FixedLengthStringData[] totamnt02Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 12);
	  public FixedLengthStringData[] totamnt03Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 24);
	  public FixedLengthStringData[] totamnt04Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 36);
	  public FixedLengthStringData[] totamnt05Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 48);
	  public FixedLengthStringData[] totamnt06Out = FLSArrayPartOfStructure(12, 1, totamntsOut, 60);

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);


	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public LongData Sr58oscreenWritten = new LongData(0);
	public LongData Sr58owindowWritten = new LongData(0);
	public LongData Sr58ohideWritten = new LongData(0);
	public LongData Sr58oprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr58oScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmfrmOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmtoOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(oppc01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(oppc02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcnt06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(totamnt06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});



	screenFields = new BaseData[] { company,item,itmfrm,itmto,longdesc,oppc01,oppc02,prcnt01,prcnt02,prcnt03,prcnt04,prcnt05,prcnt06,tabl,totamnt01,totamnt02,totamnt03,totamnt04,totamnt05,totamnt06 };
	screenOutFields = new BaseData[][] { companyOut,itemOut,itmfrmOut,itmtoOut,longdescOut,oppc01Out,oppc02Out,prcnt01Out,prcnt02Out,prcnt03Out,prcnt04Out,prcnt05Out,prcnt06Out,tablOut,totamnt01Out,totamnt02Out,totamnt03Out,totamnt04Out,totamnt05Out,totamnt06Out };
	screenErrFields = new BaseData[] {  companyErr,itemErr,itmfrmErr,itmtoErr,longdescErr,oppc01Err,oppc02Err,prcnt01Err,prcnt02Err,prcnt03Err,prcnt04Err,prcnt05Err,prcnt06Err,tablErr,totamnt01Err,totamnt02Err,totamnt03Err,totamnt04Err,totamnt05Err,totamnt06Err  };
	screenDateFields = new BaseData[] { itmfrm,itmto  };
	screenDateErrFields = new BaseData[] { itmfrmErr,itmtoErr  };
	screenDateDispFields = new BaseData[] { itmfrmDisp,itmtoDisp   };

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr58oscreen.class;
	protectRecord = Sr58oprotect.class;

	}

 

}
