/*
 * File: Br670.java
 * Date: 29 August 2009 22:35:22
 * Author: Quipoz Limited
 * 
 * Class transformed from BR670.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.recordstructures.Zbnwcoyrec;
import com.csc.life.agents.tablestructures.Tr661rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   This is to extract company records
* ***********************************************************
*
*   In INPUT-OUTPUT SECTION, we specify the output file ZBNWPF in which
*   the agent movement records are written.
*
*   Initialise
*     - In 1000-INITIALISE SECTION We override the Database file ZBNWPF
*       for records written.
*     - We initialize the ITEMIO to read the table TR661 to read o
*       the Bonus Workbench Company.
*
*   Read
*     - In 2000-READ-FILE SECTION, we read all items from the
*       table until EOF and increment the control total for ever
*       record read.
*
*   Edit
*     - In 3000-UPDATE SECTION, for every record read
*       we write the record together with required information
*       by performing the section 3100-WRITE
*
*   Update
*     - In 4000-UPDATE SECTION, we close the output file ZBNWPF
*       and copy the file to the FTP library which is given in
*       process definition.
*     - Create the section A100-DATE-CONVERSION to  convert the
*       in the format MM/DD/YYYY.
*     - Create the section A200-PACK-STRING to pack the data string
*       such that semi colon will be added to separate the data  .
*
*   Control totals
*    .No of records read CT01
*    .No of records read CT02
*
*   ERROR PROCESSING:
*     IF A SYSTEM ERROR MOVE THE ERROR CODE INTO THE SYSR-STATUZ
*     IF A DATABASE ERROR MOVE THE XXXX-PARAMS TO SYSR-PARAMS.
*     PERFORM THE 600-FATAL-ERROR SECTION.
*
***********************************************************************
* </pre>
*/
public class Br670 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR670");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1000);
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);
		/* WSAA-CONV-DATE */
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4);
	private static final String wsaaZeroDate = "00/00/0000";

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler3 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private static final int wsaaTr661MaxItem = 500;

		/* WSAA-TR661-ITEMS */
	private FixedLengthStringData[] wsaaTr661Item = FLSInittedArray (500, 21);
	private ZonedDecimalData[] wsaaStartDate = ZDArrayPartOfArrayStructure(8, 0, wsaaTr661Item, 0);
	private FixedLengthStringData[] wsaaZbnwcoy = FLSDArrayPartOfArrayStructure(3, wsaaTr661Item, 8);
	private FixedLengthStringData[] wsaaZstate = FLSDArrayPartOfArrayStructure(2, wsaaTr661Item, 11);
	private FixedLengthStringData[] wsaaItemitem = FLSDArrayPartOfArrayStructure(8, wsaaTr661Item, 13);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String tr661 = "TR661";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Desckey wsaaDesckey = new Desckey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Zbnwcoyrec zbnwcoyrec = new Zbnwcoyrec();
	private Tr661rec tr661rec = new Tr661rec();

	public Br670() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspcomn.edterror;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaZbnwFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		zbnwpf.openOutput();
		preLoadTr6611100();
		wsaaIx.set(1);
		/*EXIT*/
	}

protected void preLoadTr6611100()
	{
					init1110();
					call1120();
				}

protected void init1110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(tr661);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMPFX", "ITEMCOY", "ITEMTABL");

		itemIO.setFormat(itemrec);
		wsaaIx.set(1);
	}

protected void call1120()
	{
		if (isGT(wsaaIx,wsaaTr661MaxItem)) {
			return ;
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItempfx(),smtpfxcpy.item)
		|| isNE(itemIO.getItemcoy(),bprdIO.getCompany())
		|| isNE(itemIO.getItemtabl(),tr661)) {
			return ;
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			tr661rec.tr661Rec.set(itemIO.getGenarea());
			wsaaStartDate[wsaaIx.toInt()].set(tr661rec.startDate);
			wsaaZbnwcoy[wsaaIx.toInt()].set(tr661rec.zbnwcoy);
			wsaaZstate[wsaaIx.toInt()].set(tr661rec.zstate);
			wsaaItemitem[wsaaIx.toInt()].set(itemIO.getItemitem());
			wsaaIx.add(1);
			itemIO.setFunction(varcom.nextr);
			call1120();
			return ;
		}
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		if (isGT(wsaaIx,wsaaTr661MaxItem)
		|| isEQ(wsaaZbnwcoy[wsaaIx.toInt()],SPACES)) {
			wsspcomn.edterror.set(varcom.endp);
			return ;
		}
		else {
			tr661rec.startDate.set(wsaaStartDate[wsaaIx.toInt()]);
			tr661rec.zbnwcoy.set(wsaaZbnwcoy[wsaaIx.toInt()]);
			tr661rec.zstate.set(wsaaZstate[wsaaIx.toInt()]);
			wsaaIx.add(1);
			contotrec.totval.set(1);
			contotrec.totno.set(ct01);
			callContot001();
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		write3100();
	}

protected void write3100()
	{
		zbnwcoyrec.bnwcoyRec.set(SPACES);
		zbnwcoyrec.bnwcoyTrcde.set("CR");
		zbnwcoyrec.bnwcoySource.set("LIFE/ASIA");
		zbnwcoyrec.bnwcoyRectype.set(SPACES);
		zbnwcoyrec.bnwcoyCompany.set(tr661rec.zbnwcoy);
		wsaaDesckey.set(SPACES);
		wsaaDesckey.descDesccoy.set(bprdIO.getCompany());
		wsaaDesckey.descDesctabl.set(tr661);
		wsaaDesckey.descDescpfx.set(smtpfxcpy.item);
		compute(wsaaDesckey.descDescitem, 0).set(wsaaItemitem[sub(wsaaIx,1).toInt()]);
		wsaaDesckey.descLanguage.set(bsscIO.getLanguage());
		a300GetDescription();
		zbnwcoyrec.bnwcoyName.set(descIO.getLongdesc());
		zbnwcoyrec.bnwcoyAddr01.set(SPACES);
		zbnwcoyrec.bnwcoyAddr02.set(SPACES);
		zbnwcoyrec.bnwcoyAddr03.set(SPACES);
		zbnwcoyrec.bnwcoyAddr04.set(SPACES);
		zbnwcoyrec.bnwcoyCity.set(SPACES);
		zbnwcoyrec.bnwcoyState.set(tr661rec.zstate);
		zbnwcoyrec.bnwcoyZip.set(SPACES);
		zbnwcoyrec.bnwcoyCountry.set(SPACES);
		/* MOVE TR661-START-DATE       TO WSAA-INPT-DATE.               */
		/* PERFORM A100-DATE-CONVERSION.                                */
		/* MOVE WSAA-CONV-DATE         TO ZCOY-BNWCOY-STRTDATE.         */
		/* MOVE WSAA-CONV-DATE         TO ZCOY-BNWCOY-CHGDATE.          */
		/* MOVE WSAA-CONV-DATE         TO ZCOY-BNWCOY-PROCDATE.         */
		zbnwcoyrec.bnwcoyStrtdate.set(wsaaZeroDate);
		zbnwcoyrec.bnwcoyChgdate.set(wsaaZeroDate);
		zbnwcoyrec.bnwcoyProcdate.set(wsaaZeroDate);
		a200PackString();
		zbnwpfRec.zbnwpfRecord.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		zbnwpf.close();
		/*  Put the file to FTP library for download*/
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaStatuz);
			stringVariable1.addExpression(bprdIO.getRunLibrary());
			stringVariable1.addExpression(wsaaZbnwFn);
			stringVariable1.addExpression(wsaaFtpLibrary);
			stringVariable1.addExpression(wsaaFtpFile);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a100DateConversion()
	{
		/*A101-CONVERSION*/
		/*A109-EXIT*/
	}

protected void a200PackString()
	{
		/*A201-PACK*/
		wsaaString.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyTrcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoySource, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyRectype, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyCompany, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyName, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyAddr01, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyAddr02, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyAddr03, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyAddr04, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyCity, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyState, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyZip, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyCountry, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyStrtdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyChgdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwcoyrec.bnwcoyProcdate, " ");
		stringVariable1.setStringInto(wsaaString);
		/*A209-EXIT*/
	}

protected void a300GetDescription()
	{
		a310Init();
	}

protected void a310Init()
	{
		descIO.setParams(SPACES);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}
}
