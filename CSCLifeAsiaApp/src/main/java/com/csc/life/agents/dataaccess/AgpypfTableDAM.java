package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpypfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:49
 * Class transformed from AGPYPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpypfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 324;
	public FixedLengthStringData agpyrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agpypfRecord = agpyrec;
	
	public FixedLengthStringData agntpfx = DD.agntpfx.copy().isAPartOf(agpyrec);
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agpyrec);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(agpyrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(agpyrec);
	public FixedLengthStringData batcpfx = DD.batcpfx.copy().isAPartOf(agpyrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(agpyrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(agpyrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(agpyrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(agpyrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(agpyrec);
	public FixedLengthStringData paypfx = DD.paypfx.copy().isAPartOf(agpyrec);
	public FixedLengthStringData paycoy = DD.paycoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(agpyrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(agpyrec);
	public FixedLengthStringData reqnpfx = DD.reqnpfx.copy().isAPartOf(agpyrec);
	public FixedLengthStringData reqncoy = DD.reqncoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData reqnbcde = DD.reqnbcde.copy().isAPartOf(agpyrec);
	public FixedLengthStringData reqnno = DD.reqnno.copy().isAPartOf(agpyrec);
	public PackedDecimalData tranamt = DD.tranamt.copy().isAPartOf(agpyrec);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(agpyrec);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(agpyrec);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(agpyrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(agpyrec);
	public PackedDecimalData jrnseq = DD.jrnseq.copy().isAPartOf(agpyrec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(agpyrec);
	public FixedLengthStringData tranref = DD.tranref.copy().isAPartOf(agpyrec);
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(agpyrec);
	public PackedDecimalData crate = DD.crate.copy().isAPartOf(agpyrec);
	public PackedDecimalData acctamt = DD.acctamt.copy().isAPartOf(agpyrec);
	public FixedLengthStringData genlcoy = DD.genlcoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData genlcur = DD.genlcur.copy().isAPartOf(agpyrec);
	public FixedLengthStringData glcode = DD.glcode.copy().isAPartOf(agpyrec);
	public FixedLengthStringData glsign = DD.glsign.copy().isAPartOf(agpyrec);
	public FixedLengthStringData postyear = DD.postyear.copy().isAPartOf(agpyrec);
	public FixedLengthStringData postmonth = DD.postmonth.copy().isAPartOf(agpyrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(agpyrec);
	public PackedDecimalData rcamt = DD.rcamt.copy().isAPartOf(agpyrec);
	public PackedDecimalData frcdate = DD.frcdate.copy().isAPartOf(agpyrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(agpyrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(agpyrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(agpyrec);
	public FixedLengthStringData rldgcoy = DD.rldgcoy.copy().isAPartOf(agpyrec);
	public FixedLengthStringData rldgacct = DD.rldgacct.copy().isAPartOf(agpyrec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(agpyrec);
	public FixedLengthStringData suprflg = DD.suprflg.copy().isAPartOf(agpyrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(agpyrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agpyrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agpyrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agpyrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgpypfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgpypfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgpypfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgpypfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpypfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgpypfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgpypfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGPYPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTPFX, " +
							"AGNTCOY, " +
							"AGNTNUM, " +
							"AGNTBR, " +
							"ARACDE, " +
							"BATCPFX, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"PAYPFX, " +
							"PAYCOY, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"REQNPFX, " +
							"REQNCOY, " +
							"REQNBCDE, " +
							"REQNNO, " +
							"TRANAMT, " +
							"SACSCODE, " +
							"SACSTYP, " +
							"RDOCNUM, " +
							"TRANNO, " +
							"JRNSEQ, " +
							"ORIGAMT, " +
							"TRANREF, " +
							"TRANDESC, " +
							"CRATE, " +
							"ACCTAMT, " +
							"GENLCOY, " +
							"GENLCUR, " +
							"GLCODE, " +
							"GLSIGN, " +
							"POSTYEAR, " +
							"POSTMONTH, " +
							"EFFDATE, " +
							"RCAMT, " +
							"FRCDATE, " +
							"TRDT, " +
							"USER_T, " +
							"TERMID, " +
							"RLDGCOY, " +
							"RLDGACCT, " +
							"ORIGCURR, " +
							"SUPRFLG, " +
							"TRTM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntpfx,
                                     agntcoy,
                                     agntnum,
                                     agntbr,
                                     aracde,
                                     batcpfx,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     paypfx,
                                     paycoy,
                                     bankkey,
                                     bankacckey,
                                     reqnpfx,
                                     reqncoy,
                                     reqnbcde,
                                     reqnno,
                                     tranamt,
                                     sacscode,
                                     sacstyp,
                                     rdocnum,
                                     tranno,
                                     jrnseq,
                                     origamt,
                                     tranref,
                                     trandesc,
                                     crate,
                                     acctamt,
                                     genlcoy,
                                     genlcur,
                                     glcode,
                                     glsign,
                                     postyear,
                                     postmonth,
                                     effdate,
                                     rcamt,
                                     frcdate,
                                     transactionDate,
                                     user,
                                     termid,
                                     rldgcoy,
                                     rldgacct,
                                     origcurr,
                                     suprflg,
                                     transactionTime,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntpfx.clear();
  		agntcoy.clear();
  		agntnum.clear();
  		agntbr.clear();
  		aracde.clear();
  		batcpfx.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		paypfx.clear();
  		paycoy.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		reqnpfx.clear();
  		reqncoy.clear();
  		reqnbcde.clear();
  		reqnno.clear();
  		tranamt.clear();
  		sacscode.clear();
  		sacstyp.clear();
  		rdocnum.clear();
  		tranno.clear();
  		jrnseq.clear();
  		origamt.clear();
  		tranref.clear();
  		trandesc.clear();
  		crate.clear();
  		acctamt.clear();
  		genlcoy.clear();
  		genlcur.clear();
  		glcode.clear();
  		glsign.clear();
  		postyear.clear();
  		postmonth.clear();
  		effdate.clear();
  		rcamt.clear();
  		frcdate.clear();
  		transactionDate.clear();
  		user.clear();
  		termid.clear();
  		rldgcoy.clear();
  		rldgacct.clear();
  		origcurr.clear();
  		suprflg.clear();
  		transactionTime.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgpyrec() {
  		return agpyrec;
	}

	public FixedLengthStringData getAgpypfRecord() {
  		return agpypfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgpyrec(what);
	}

	public void setAgpyrec(Object what) {
  		this.agpyrec.set(what);
	}

	public void setAgpypfRecord(Object what) {
  		this.agpypfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agpyrec.getLength());
		result.set(agpyrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}