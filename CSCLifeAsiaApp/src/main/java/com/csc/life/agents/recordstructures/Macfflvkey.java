package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:33
 * Description:
 * Copybook name: MACFFLVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfflvkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfflvFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfflvKey = new FixedLengthStringData(64).isAPartOf(macfflvFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfflvAgntcoy = new FixedLengthStringData(1).isAPartOf(macfflvKey, 0);
  	public FixedLengthStringData macfflvZrptga = new FixedLengthStringData(8).isAPartOf(macfflvKey, 1);
  	public FixedLengthStringData macfflvAgntnum = new FixedLengthStringData(8).isAPartOf(macfflvKey, 9);
  	public PackedDecimalData macfflvEffdate = new PackedDecimalData(8, 0).isAPartOf(macfflvKey, 17);
  	public PackedDecimalData macfflvTranno = new PackedDecimalData(5, 0).isAPartOf(macfflvKey, 22);
  	public FixedLengthStringData macfflvAgmvty = new FixedLengthStringData(2).isAPartOf(macfflvKey, 25);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(macfflvKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfflvFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfflvFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}