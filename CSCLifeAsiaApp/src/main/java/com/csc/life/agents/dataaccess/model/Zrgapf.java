/*
 * File: Zrgapf.java
 * Date: July 22, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zrgapf implements Serializable {
	private long uniqueNumber;
	private String agntcoy;
	private String agntnum;
	private String agntype;
	private String zrecruit;
	private Timestamp datime;
	private BigDecimal effdate;
	private String jobName;
	private String mlagttyp;
	private BigDecimal prcent;
	private String userProfile;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getAgntype() {
		return agntype;
	}
	public void setAgntype(String agntype) {
		this.agntype = agntype;
	}
	public String getZrecruit() {
		return zrecruit;
	}
	public void setZrecruit(String zrecruit) {
		this.zrecruit = zrecruit;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	public BigDecimal getEffdate() {
		return effdate;
	}
	public void setEffdate(BigDecimal effdate) {
		this.effdate = effdate;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getMlagttyp() {
		return mlagttyp;
	}
	public void setMlagttyp(String mlagttyp) {
		this.mlagttyp = mlagttyp;
	}
	public BigDecimal getPrcent() {
		return prcent;
	}
	public void setPrcent(BigDecimal prcent) {
		this.prcent = prcent;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	
	
	

	}
