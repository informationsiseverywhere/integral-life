package com.csc.life.agents.batchprograms;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.ZpcddagTableDAM;
import com.csc.life.agents.tablestructures.Tr58prec;
import com.csc.life.agents.tablestructures.Tr58trec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.AcmvpfDAO;
import com.csc.life.agents.dataaccess.dao.ZrfbpfDAO;
import com.csc.life.agents.dataaccess.dao.ZrcapfDAO;
import com.csc.life.agents.dataaccess.model.Zrcapf;
import com.csc.life.agents.dataaccess.model.Zrfbpf;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.ItempfDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.ConversionUtil;
import com.csc.smart400framework.utility.*;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;


public class Br58k extends Mainb {
	
	private PackedDecimalData wsaarfbamt = new PackedDecimalData(15,2);	
	private FixedLengthStringData wsaaisspol = new FixedLengthStringData(1);
	private FixedLengthStringData wsaazrecruit = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaagtyprcted = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaagtyprcter = new FixedLengthStringData(2);
	private FixedLengthStringData wsaavldrcted = new FixedLengthStringData(1);
	private FixedLengthStringData wsaavldrcter = new FixedLengthStringData(1);
	
	
	//Start : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]
	private FixedLengthStringData wsaazrcafnd = new FixedLengthStringData(1);
	private FixedLengthStringData wsaavldzrcrtd = new FixedLengthStringData(1);
	private FixedLengthStringData wsaavldzrcrtr = new FixedLengthStringData(1);
	
	//End : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]


	
	private PackedDecimalData wsaadteapp = new PackedDecimalData(8);
	private PackedDecimalData wsaadtetrm = new PackedDecimalData(8);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR58K");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private PackedDecimalData wsaafreq = new PackedDecimalData(1).init(1);
	private List<Itempf> itemAl = new ArrayList<Itempf>();
	private ItempfDAO itempfDAO =  DAOFactory.getItempfDAO();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Tr58prec tr58prec = new Tr58prec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private Tr58trec tr58trec = new Tr58trec();
	private FixedLengthStringData wsaaContitem = new FixedLengthStringData(1).init("Y");
	private Validator contitemNo = new Validator(wsaaContitem, "N");
	private Itempf itempf;
	private String tableName;
	private String itemitem;
	private Acmvpf acmvpf;
	private Zrfbpf zrfbpf;
	private Zrcapf zrcapf;
	private Zrcapf zrcapfData;
	private AcmvpfDAO acmvpfDao = DAOFactory.getAcmvpfDAO();
    private ZrfbpfDAO zrfbpfDao = getApplicationContext().getBean("zrfbpfDao", ZrfbpfDAO.class);
    private ZrcapfDAO zrcapfDao = getApplicationContext().getBean("zrcapfDao", ZrcapfDAO.class);
	private Iterator<Acmvpf> acmvpfIter = null;
	private String longdesc;
	private DescTableDAM descIO = new DescTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM(); 
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private Map<String,ArrayList<String>> tr58pmap = new LinkedHashMap<String,ArrayList<String>>();
	private ArrayList<String> tr58pdata;
	private int itmfrm = 0;
	private int genarea = 1;
	private FixedLengthStringData agntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ZpcddagTableDAM zpcddagIO = new ZpcddagTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private List<String> tr58tchdrList = new ArrayList<String>();
	private List<String> tr58tcrTableList = new ArrayList<String>();
	private List<String> t5679cnRiskList = new ArrayList<String>();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovtTableDAM covtIO = new CovtTableDAM();
	private int leerjrnseq=0;
	private int labrjrnseq=0;
	private String crttable;
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
		}
	}

private void process()
	{
		super.mainline();
	}


@Override
protected void restart0900() {
	// TODO Auto-generated method stub
	
}

@Override
protected void initialise1000() {
	tableName="TR58T";
	itemitem=bsprIO.getCompany().trim();
	readItempf();
	tr58trec.tr58tRec.set(StringUtil.rawToString(itempf.getGenarea()));
	String tr58tchdr;
	for(int i = 1 ; i < tr58trec.chdrtype.length;i++)
	{
		if(tr58trec.chdrtype[i] != null)
		{
			tr58tchdr = tr58trec.chdrtype[i].trim();
			if(!"".equals(tr58tchdr))
				{
			tr58tchdrList.add(tr58tchdr);
				}
		}
	}
	String tr58tcrtable;
	for(int i = 1 ; i < tr58trec.crtable.length;i++)
	{
		if(tr58trec.crtable[i] != null)
		{
			tr58tcrtable = tr58trec.crtable[i].trim();
			if(!"".equals(tr58tcrtable))
				{
					tr58tcrTableList.add(tr58tcrtable);
				}
		}
	}
	
	tableName = "TR58P";
	readTr58p();
	tableName="T5645";
	itemitem=wsaaProg.toString();
	readItempf();
	t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	tableName="T5679";
	itemitem=bprdIO.getAuthCode().toString();
	readItempf();
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	String t5679cnrisk;
	for(int i = 1 ; i < t5679rec.cnRiskStat.length;i++)
	{
		if(t5679rec.cnRiskStat[i] != null)
		{
		t5679cnrisk = t5679rec.cnRiskStat[i].trim();
		if(!"".equals(t5679cnrisk))
				{
			t5679cnRiskList.add(t5679cnrisk);
				}
		}
	}
	tableName="T1688";
	readT1688Descpf();
	prepAcmv1100();
	prepLifAcmv1100();
}


protected void readTr58p()
{
	wsaaContitem.set("Y");
	itdmIO.setItemcoy(bsprIO.getCompany());
	itdmIO.setItemitem(SPACE);
	itdmIO.setItemtabl(tableName);
	itdmIO.setItmfrm(varcom.vrcmMaxDate);
	itdmIO.setFunction(Varcom.begn);
	while(!contitemNo.isTrue())
	{		
		SmartFileCode.execute(appVars,itdmIO);
		if(isNE(itdmIO.getStatuz(),Varcom.oK) 
				|| isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
				|| isNE(itdmIO.getItemtabl(),tableName))
		{
			wsaaContitem.set("N");
			break;
		}		
		tr58pdata = new ArrayList<String>();
		tr58pdata.add(itmfrm,itdmIO.getItmfrm().toString());
		tr58pdata.add(genarea,itdmIO.getGenarea().toString());
		tr58pmap.put(itdmIO.getItemitem().trim(),tr58pdata);
		itdmIO.setFunction(Varcom.nextr);
	}
	if(tr58pmap.isEmpty())
	{
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
}

protected void readItempf()
{		
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(tableName);
	itempf.setItemitem(itemitem);
	itempf.setValidflag("1");
	itemAl = itempfDAO.findByExample(itempf);
	if(!itemAl.isEmpty())
	{
		itempf = itemAl.get(0);
	}
	else
	{
		syserrrec.params.set(itempf.getItemitem());
		fatalError600();
	}		
}


private void prepAcmv1100()
{
	acmvpf = new Acmvpf(); 
	acmvpf.setRldgcoy(ConversionUtil.toCharacter(bsprIO.getCompany()));
	acmvpf.setBatcactyr(bsscIO.getAcctYear().toShort());
	acmvpf.setBatcactmn(bsscIO.getAcctMonth().toByte());
	acmvpf.setTranno(0);
	acmvpf.setSacscode(ConversionUtil.toString(t5645rec.sacscode01));
	acmvpf.setSacstyp(ConversionUtil.toString(t5645rec.sacstype01));
	acmvpf.setIntextind('E');
	acmvpfIter = acmvpfDao.findByCriteriaAndOrderForBatch(acmvpf).iterator();
	
}

public void readT1688Descpf()
{
	descIO.setDataArea(SPACE);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(bsprIO.getCompany());
	descIO.setDescitem(bprdIO.getAuthCode());
	descIO.setDesctabl(tableName);
	descIO.setLanguage(appVars.getLanguageCode());
	descIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars,descIO);
	if(isEQ(descIO.getStatuz(),Varcom.oK))
	{
		longdesc = ConversionUtil.toString(descIO.getLongdesc());
	}
	else
	{
		longdesc = "";
	}
}


public void prepLifAcmv1100()
{
	lifacmvrec.initialize();
	lifacmvrec.function.set("PSTW");
	lifacmvrec.batccoy.set(batcdorrec.company);
	lifacmvrec.batcbrn.set(batcdorrec.branch);
	lifacmvrec.batcactmn.set(batcdorrec.actmonth);
	lifacmvrec.batcactyr.set(batcdorrec.actyear);
	lifacmvrec.batctrcde.set(batcdorrec.trcde);
	lifacmvrec.batcbatch.set(batcdorrec.batch);
	lifacmvrec.effdate.set(bsscIO.getEffectiveDate());
	lifacmvrec.tranno.set(ZERO);
	lifacmvrec.jrnseq.set(ZERO);
	lifacmvrec.rldgcoy.set(bsprIO.getCompany());
	lifacmvrec.trandesc.set(longdesc);
	lifacmvrec.postyear.set(SPACE);
	lifacmvrec.postmonth.set(SPACE);
	lifacmvrec.rcamt.set(ZERO);
	lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
	lifacmvrec.user.set(ZERO);
	lifacmvrec.termid.set(SPACE);
	lifacmvrec.suprflag.set(SPACE);
	lifacmvrec.genlcoy.set(bsprIO.getCompany());
	lifacmvrec.genlcur.set(SPACE);
	lifacmvrec.transactionDate.set(ZERO);
	lifacmvrec.transactionTime.set(ZERO);
	lifacmvrec.threadNumber.set(ZERO);
}
@Override
protected void readFile2000() {
	if(!acmvpfIter.hasNext())
	{
		wsspEdterror.set(Varcom.endp);
	}
	else
	{
		wsspEdterror.set(Varcom.oK);
	}
	
}

@Override
protected void edit2500() {
	// TODO Auto-generated method stub
	acmvpf = acmvpfIter.next();
	wsspEdterror.set(Varcom.oK);
	if( acmvpf.getTermid() != null && tr58tcrTableList.contains(acmvpf.getTermid().trim()))
	{
		wsspEdterror.set(SPACE);
		return;
	}	
	agntcoy.set(acmvpf.getRldgcoy());
	agntnum.set(acmvpf.getRldgacct());
	readAglf();
	wsaazrecruit.set(aglfIO.getZrecruit());
    wsaadteapp.set(aglfIO.getDteapp());	
	readAgntlag();
 	wsaaagtyprcted.set(agntlagIO.getAgtype());
	wsaavldrcted.set("N");
	if(tr58pmap.containsKey(wsaaagtyprcted.trim()))
	{
		tr58pdata = tr58pmap.get(wsaaagtyprcted.trim());
		if(isGTE(bsscIO.getEffectiveDate(),tr58pdata.get(itmfrm)))
		{
			wsaavldrcted.set("Y");
		}
	}
	wsaaagtyprcter.set(SPACE);
    wsaadtetrm.set(varcom.vrcmMaxDate);
    wsaavldrcter.set("N");
    if(isNE(wsaazrecruit,SPACE))
    {
    	agntcoy.set(acmvpf.getRldgcoy());
    	agntnum.set(wsaazrecruit);
    	readAglf();
    	wsaadtetrm.set(aglfIO.getDtetrm());
    	agntcoy.set(aglfIO.getAgntcoy());
    	agntnum.set(aglfIO.getAgntnum());
    	readAgntlag();
    	wsaaagtyprcter.set(agntlagIO.getAgtype());
    	if(tr58pmap.containsKey(wsaaagtyprcter.trim()))
    	{
    		tr58pdata = tr58pmap.get(wsaaagtyprcter.trim());
    		if(isGTE(bsscIO.getEffectiveDate(),tr58pdata.get(itmfrm)))
    		{
    			wsaavldrcter.set("Y");
    			tr58prec.tr58pRec.set(tr58pdata.get(genarea));
    		}
    	}
    }
    
    //Start : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]
	wsaazrcafnd.set("N");
    wsaavldzrcrtd.set("N");
    wsaavldzrcrtr.set("N");
    
    zrcapf.setAgntcoy(acmvpf.getRldgcoy());
    zrcapf.setAgntnum(acmvpf.getRldgacct());
    zrcapfData= zrcapfDao.getZrcapfRecord(zrcapf.getAgntcoy(), zrcapf.getAgntnum());
	/*zrcaIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, zrcaIO);
	if(isNE(zrcaIO.getStatuz(), varcom.oK)
		&& isNE(zrcaIO.getStatuz(), varcom.mrnf)){
		syserrrec.params.set(zrcaIO.getParams());
		syserrrec.statuz.set(zrcaIO.getStatuz());
		fatalError600();
	}*/
   if(zrcapfData!=null){
		wsaazrcafnd.set("Y");
	    if(tr58pmap.containsKey(zrcapfData.getAgntype()))
		{
			tr58pdata = tr58pmap.get(zrcapfData.getAgntype().trim());
			if(isGTE(bsscIO.getEffectiveDate(),tr58pdata.get(itmfrm)))
			{
				wsaavldzrcrtd.set("Y");
			}
		}
		if(tr58pmap.containsKey(zrcapfData.getMlagttyp()))
		{
			tr58pdata = tr58pmap.get(zrcapfData.getMlagttyp().trim());
			if(isGTE(bsscIO.getEffectiveDate(),tr58pdata.get(itmfrm)))
			{
				wsaavldzrcrtr.set("Y");
			}
		}
	}

	//End : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]
			 
	if(isEQ(wsaazrecruit,SPACE) || isNE(wsaavldrcted,"Y"))
	{
		wsspEdterror.set(SPACE);
		return;
	}
	if(isLTE(wsaadtetrm,bsscIO.getEffectiveDate()) || isNE(wsaavldrcter,"Y"))
	    {
	    	wsspEdterror.set(SPACE);
	    	return;
	    }
	//Start : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]

    if(isNE(wsaazrcafnd,"Y") || isNE(wsaavldzrcrtd,"Y") || isNE(wsaavldzrcrtr,"Y"))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
	//End : Modified by Sharad Kumar for Ticket #TMLII-2219 [Referral Bonus based on original FC recruiter]
    
    readHpad();
    if(isNE(hpadIO.getStatuz(),Varcom.oK))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
    datcon3rec.datcon3Rec.set(SPACE);
    datcon3rec.intDate1.set(wsaadteapp);
    datcon3rec.intDate2.set(hpadIO.getHoissdte());
    datcon3rec.frequency.set("01");
    callProgram(Datcon3.class, datcon3rec.datcon3Rec);
    if(isGT(datcon3rec.freqFactor,wsaafreq))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
    readChdrLif();
    if(isNE(chdrlifIO.getStatuz(),Varcom.oK) || tr58tchdrList.contains(chdrlifIO.getCnttype().trim()))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
    wsaaisspol.set("N");
    readZpcddag();
    if(isNE(wsaaisspol,"Y"))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
    wsaarfbamt.setRounded(mult(acmvpf.getAcctamt(),div(tr58prec.prcent,100)));
    if(isEQ(wsaarfbamt,0))
    {
    	wsspEdterror.set(SPACE);
    	return;
    }
}

protected void readAglf()
{
	aglfIO.setDataArea(SPACE);
	aglfIO.setAgntcoy(agntcoy);
	aglfIO.setAgntnum(agntnum);
	aglfIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars,aglfIO);
	if(isNE(aglfIO.getStatuz(),Varcom.oK))
	{
		syserrrec.params.set(aglfIO.getParams());
		syserrrec.statuz.set(aglfIO.getStatuz());
		fatalError600();
	}
		
}

protected void readAgntlag()
{
	agntlagIO.setDataArea(SPACE);
	agntlagIO.setAgntcoy(agntcoy);
	agntlagIO.setAgntnum(agntnum);
	agntlagIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, agntlagIO);
	if(isNE(agntlagIO.getStatuz(),Varcom.oK))
	{
		syserrrec.params.set(agntlagIO.getParams());
		syserrrec.statuz.set(agntlagIO.getStatuz());
		fatalError600();
	}
}

protected void readZpcddag()
{
	zpcddagIO.setDataArea(SPACE);
	zpcddagIO.setChdrcoy(acmvpf.getRldgcoy());
	zpcddagIO.setAgntnum(wsaazrecruit);
	zpcddagIO.setFunction(Varcom.begn);
	wsaaContitem.set("Y");
	while(!contitemNo.isTrue())
	{
		SmartFileCode.execute(appVars, zpcddagIO);
		if(isNE(zpcddagIO.getStatuz(),Varcom.oK) 
				|| isNE(acmvpf.getRldgcoy(),zpcddagIO.getChdrcoy())
				|| isNE(wsaazrecruit,zpcddagIO.getAgntnum())
				|| isEQ(wsaaisspol,"Y"))
		{
			wsaaContitem.set("N");			
		}
		else
		{
			readChdrenq();
			if(isEQ(wsaaisspol,"Y"))
			{
				wsaaContitem.set("N");
			}
		}

	}
		
}

protected void readChdrenq()
{
	chdrenqIO.setDataArea(SPACE);
	chdrenqIO.setChdrcoy(zpcddagIO.getChdrcoy());
	chdrenqIO.setChdrnum(zpcddagIO.getChdrnum());
	chdrenqIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, chdrenqIO);
	if(isEQ(chdrenqIO.getStatuz(),Varcom.oK) && isEQ(chdrenqIO.getValidflag(),"1"))
	{
		if(!t5679cnRiskList.contains(chdrenqIO.getStatcode().trim()))
		{
			wsaaisspol.set("Y");
		}
	}
}
@Override
protected void update3000() {
	// TODO Auto-generated method stub
	writeZrfb();
	callLifeAcmvLEER();
	callLifeAcmvLABR();
}


protected void writeZrfb()
{
	
	zrfbpf.setAgntcoy(acmvpf.getRldgcoy());
	zrfbpf.setZrecruit(wsaazrecruit.toString());
	zrfbpf.setAgntnum(acmvpf.getRldgacct());
	zrfbpf.setAgtype(wsaaagtyprcter.toString());
	zrfbpf.setBatcactyr(bsscIO.getAcctYear().getbigdata());
	zrfbpf.setBatcactmn(bsscIO.getAcctMonth().getbigdata());
	zrfbpf.setChdrnum(acmvpf.getRdocnum());
	zrfbpf.setLife(subString(acmvpf.getTranref(),9,2).toString());
	zrfbpf.setCoverage(subString(acmvpf.getTranref(),11,2).toString());
	zrfbpf.setRider(subString(acmvpf.getTranref(),13,2).toString());
	zrfbpf.setEffdate(acmvpf.getEffdate());
	zrfbpf.setTranno(acmvpf.getTranno());
	zrfbpf.setAcctamt(acmvpf.getAcctamt());
	zrfbpf.setOrigcurr(acmvpf.getOrigcurr());
	zrfbpf.setBonusamt(wsaarfbamt.getbigdata());
	zrfbpf.setPrcdate(bsscIO.getEffectiveDate().getbigdata());
	zrfbpf.setJobno(bsprIO.getScheduleNumber().getbigdata());
    zrfbpfDao.insertZrfbData(zrfbpf);
	//zrfbpf.setFunction(Varcom.writr);
}

protected void readCovr()
{
	covrIO.setDataArea(SPACE);
	covrIO.setChdrcoy(acmvpf.getRldgcoy());
	covrIO.setChdrnum(zrfbpf.getChdrnum());
	covrIO.setLife(zrfbpf.getLife());
	covrIO.setCoverage(zrfbpf.getCoverage());
	covrIO.setRider(zrfbpf.getRider());
	covrIO.setPlanSuffix(ZERO);
	covrIO.setFunction(Varcom.begn);
	SmartFileCode.execute(appVars, covrIO);
	if(isNE(Varcom.oK,covrIO.getStatuz())
			|| isNE(covrIO.getChdrcoy(), acmvpf.getRldgcoy())
			|| isNE(covrIO.getChdrnum(), zrfbpf.getChdrnum())
			|| isNE(covrIO.getLife(),zrfbpf.getLife())
			|| isNE(covrIO.getCoverage(),zrfbpf.getCoverage())
			|| isNE(covrIO.getRider(),zrfbpf.getRider())
			|| isNE(covrIO.getPlanSuffix(),ZERO))
	{
		readCovt();
	}
	else
	{
		crttable = covrIO.getCrtable().toString();
	}
}

protected void readCovt()
{
	covtIO.setDataArea(SPACE);
	covtIO.setChdrcoy(acmvpf.getRldgcoy());
	covtIO.setChdrnum(zrfbpf.getChdrnum());
	covtIO.setLife(zrfbpf.getLife());
	covtIO.setCoverage(zrfbpf.getCoverage());
	covtIO.setRider(zrfbpf.getRider());
	covtIO.setPlanSuffix(ZERO);
	covtIO.setFunction(Varcom.begn);
	SmartFileCode.execute(appVars, covrIO);
	if(isNE(Varcom.oK,covtIO.getStatuz())
			|| isNE(covtIO.getChdrcoy(), acmvpf.getRldgcoy())
			|| isNE(covtIO.getChdrnum(), zrfbpf.getChdrnum())
			|| isNE(covtIO.getLife(),zrfbpf.getLife())
			|| isNE(covtIO.getCoverage(),zrfbpf.getCoverage())
			|| isNE(covtIO.getRider(),zrfbpf.getRider())
			|| isNE(covtIO.getPlanSuffix(),ZERO))
	{
		crttable="";
	}
	else
	{
		crttable = covtIO.getCrtable().toString();
	}
}
protected void callLifeAcmvLEER()
{
	readCovr();
	lifacmvrec.rdocnum.set(acmvpf.getRdocnum());
	lifacmvrec.jrnseq.set(leerjrnseq++);
	lifacmvrec.sacscode.set(t5645rec.sacscode02);
	lifacmvrec.sacstyp.set(t5645rec.sacstype02);
	lifacmvrec.glcode.set(t5645rec.glmap02);
	lifacmvrec.glsign.set(t5645rec.sign02);
	lifacmvrec.contot.set(t5645rec.cnttot02);
	lifacmvrec.origcurr.set(acmvpf.getGenlcur());
	lifacmvrec.origamt.set(wsaarfbamt);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.substituteCode[06].set(crttable);
	lifacmvrec.rldgacct.set(zrfbpf.getChdrnum() + zrfbpf.getLife() + zrfbpf.getCoverage() + zrfbpf.getRider()
				+ "00");/* IJTI-1523 */
	lifacmvrec.tranref.set(zrfbpf.getZrecruit());
	// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
	lifacmvrec.ind.set("D");
	lifacmvrec.prefix.set("CH");
	callProgram(Lifacmv.class,lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,Varcom.oK)) {
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec.statuz);
		fatalError600();
	}
}
protected void callLifeAcmvLABR()
{
	lifacmvrec.jrnseq.set(labrjrnseq++);
	lifacmvrec.sacscode.set(t5645rec.sacscode03);
	lifacmvrec.sacstyp.set(t5645rec.sacstype03);
	lifacmvrec.glcode.set(t5645rec.glmap03);
	lifacmvrec.glsign.set(t5645rec.sign03);
	lifacmvrec.contot.set(t5645rec.cnttot03);
	lifacmvrec.origcurr.set(acmvpf.getGenlcur());
	lifacmvrec.origamt.set(wsaarfbamt);
	lifacmvrec.crate.set(ZERO);
	lifacmvrec.acctamt.set(ZERO);
	lifacmvrec.rldgacct.set(zrfbpf.getZrecruit());
	lifacmvrec.tranref.set(zrfbpf.getChdrnum() + zrfbpf.getLife() + zrfbpf.getCoverage() + zrfbpf.getRider()
				+ "00");/* IJTI-1523 */
	// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
	lifacmvrec.ind.set("D");
	lifacmvrec.prefix.set("CH");
	callProgram(Lifacmv.class,lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz,Varcom.oK)) {
		syserrrec.params.set(lifacmvrec.lifacmvRec);
		syserrrec.statuz.set(lifacmvrec.statuz);
		fatalError600();
	}
}


@Override
protected void commit3500() {
	// TODO Auto-generated method stub
	
}

@Override
protected void close4000() {
	// TODO Auto-generated method stub
	
}

@Override
protected void rollback3600() {
	// TODO Auto-generated method stub
	
}

protected void readHpad()
{
	hpadIO.setDataArea(SPACE);
	hpadIO.setChdrcoy(acmvpf.getRldgcoy());
	hpadIO.setChdrnum(acmvpf.getRdocnum());
	hpadIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, hpadIO);
	if (isNE(hpadIO.getStatuz(),Varcom.oK) && isNE(hpadIO.getStatuz(),Varcom.mrnf)) {
		syserrrec.params.set(hpadIO.hpadrec);
		syserrrec.statuz.set(hpadIO.statuz);
		fatalError600();
	}	
}

private void readChdrLif()
{
	chdrlifIO.setDataArea(SPACE);
	chdrlifIO.setChdrcoy(acmvpf.getRldgcoy());
	chdrlifIO.setChdrnum(acmvpf.getRdocnum());
	chdrlifIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, chdrlifIO);
	if (isNE(chdrlifIO.getStatuz(),Varcom.oK) && isNE(chdrlifIO.getStatuz(),Varcom.mrnf)) {
		syserrrec.params.set(chdrlifIO.chdrrec);
		syserrrec.statuz.set(chdrlifIO.statuz);
		fatalError600();
	}
}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	

}
