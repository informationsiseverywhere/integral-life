package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5001
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5001ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(306);
	public FixedLengthStringData dataFields = new FixedLengthStringData(66).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData agentfrom = DD.agentfrom.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData agentto = DD.agentto.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,22);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,35);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,43);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,44);
	public FixedLengthStringData initflg = DD.initflg.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData rnwlflg = DD.rnwlflg.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData servflg = DD.servflg.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData zrorind = DD.zrorind.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 66);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agentfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agenttoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData initflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData rnwlflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData servflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData zrorindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea, 126);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agentfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agenttoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] initflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] rnwlflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] servflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] zrorindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S5001screenWritten = new LongData(0);
	public LongData S5001protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5001ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agentfromOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agenttoOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(initflgOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rnwlflgOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(servflgOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, agentfrom, agentto, initflg, rnwlflg, servflg, comind, zrorind};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, agentfromOut, agenttoOut, initflgOut, rnwlflgOut, servflgOut, comindOut, zrorindOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, agentfromErr, agenttoErr, initflgErr, rnwlflgErr, servflgErr, comindErr, zrorindErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5001screen.class;
		protectRecord = S5001protect.class;
	}

}
