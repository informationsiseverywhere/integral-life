package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:07
 * Description:
 * Copybook name: AGLFRPTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfrptkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfrptFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglfrptKey = new FixedLengthStringData(256).isAPartOf(aglfrptFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfrptRepagent01 = new FixedLengthStringData(12).isAPartOf(aglfrptKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(244).isAPartOf(aglfrptKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfrptFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfrptFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}