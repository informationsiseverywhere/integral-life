/*
 * File: Zrorprv.java
 * Date: 30 August 2009 2:56:11
 * Author: Quipoz Limited
 * 
 * Class transformed from ZRORPRV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.ChdrcmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.Tr520rec;
import com.csc.life.agents.tablestructures.Tr521rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This is the reversal subroutine in conjunction with ZRORPYM,
*   Overriding Commission Release subroutine.
*
*   Overriding Commission Release Code is defined in T5687.
*   Rates are stored in TR520 for Regular Premium and TR521 for
*   Single Premium / Top-up.
*
*****************************************************************
* </pre>
*/
public class Zrorprv extends COBOLConvCodeModel {
	private static final Logger LOGGER = LoggerFactory.getLogger(Zrorprv.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaSubr = "ZRORPRV";
	private String wsaaFound = "N";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOrComm = new PackedDecimalData(13, 2);
	private ZonedDecimalData wsaaOrRate = new ZonedDecimalData(14, 2);
	private ZonedDecimalData wsaaTotRate = new ZonedDecimalData(14, 2);
		/* TABLES */
	private String tr521 = "TR521";
	private String tr520 = "TR520";
		/*Contract Header Commission Calcs.*/
	private ChdrcmcTableDAM chdrcmcIO = new ChdrcmcTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr520rec tr520rec = new Tr520rec();
	private Tr521rec tr521rec = new Tr521rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit1000, 
		exit1040, 
		exit2000, 
		exit2060, 
		dbExit9000
	}

	public Zrorprv() {
		super();
	}

public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}
	}

protected void mainline000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		wsaaCount.set(ZERO);
		wsaaFreq.set(ZERO);
		wsaaOrRate.set(ZERO);
		wsaaOrComm.set(ZERO);
		if (isEQ(comlinkrec.billfreq,"00")
		|| isEQ(comlinkrec.function,"TOPUP")) {
			singlePremium1000();
			if (isEQ(wsaaFound,"N")) {
				goTo(GotoLabel.exit090);
			}
			compute(wsaaOrComm, 2).set(div((mult(wsaaOrRate,comlinkrec.annprem)),100));
			comlinkrec.payamnt.set(wsaaOrComm);
			comlinkrec.erndamt.set(wsaaOrComm);
			comlinkrec.icommtot.set(wsaaOrComm);
		}
		else {
			regularPremium2000();
			if (isEQ(wsaaFound,"N")) {
				goTo(GotoLabel.exit090);
			}
			compute(wsaaOrComm, 2).set(div((mult(wsaaOrRate,comlinkrec.payamnt)),100));
			compute(comlinkrec.icommtot, 2).set(div((mult(wsaaTotRate,comlinkrec.icommtot)),100));
			comlinkrec.payamnt.set(wsaaOrComm);
			comlinkrec.erndamt.set(wsaaOrComm);
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void singlePremium1000()
	{
		try {
			para1000();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void para1000()
	{
		wsaaFound = "Y";
		readTr5211020();
		if (isEQ(wsaaFound,"N")) {
			goTo(GotoLabel.exit1000);
		}
		wsaaFound = "N";
	}

protected void readTr5211020()
	{
		para1020();
	}

protected void para1020()
	{
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(tr521);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(comlinkrec.zorcode);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),tr521)
		|| isNE(itdmIO.getItemitem(),comlinkrec.zorcode)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaFound = "N";
		}
		tr521rec.tr521Rec.set(itdmIO.getGenarea());
	}

protected void searchRate1040()
	{
		try {
			para1040();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void para1040()
	{
		for (wsaaCount.set(1); !(isGT(wsaaCount,10)
		|| isEQ(wsaaFound,"Y")); wsaaCount.add(1)){
			if (isEQ(tr521rec.zcomcode[wsaaCount.toInt()],comlinkrec.zcomcode)) {
				if (isEQ(comlinkrec.function,"TOPUP")) {
					wsaaOrRate.set(tr521rec.zrtuperc[wsaaCount.toInt()]);
				}
				else {
					wsaaOrRate.set(tr521rec.zrspperc[wsaaCount.toInt()]);
				}
				wsaaFound = "Y";
				goTo(GotoLabel.exit1040);
			}
		}
	}

protected void regularPremium2000()
	{
		try {
			para2000();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void para2000()
	{
		wsaaFound = "Y";
		readTr5202020();
		if (isEQ(wsaaFound,"N")) {
			goTo(GotoLabel.exit2000);
		}
		getNumberYear2040();
		wsaaFound = "N";
		if (isEQ(wsaaFound,"N")) {
			goTo(GotoLabel.exit2000);
		}
	}

protected void readTr5202020()
	{
		para2020();
	}

protected void para2020()
	{
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(tr520);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(comlinkrec.zorcode);
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError9000();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),tr520)
		|| isNE(itdmIO.getItemitem(),comlinkrec.zorcode)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaFound = "N";
		}
		tr520rec.tr520Rec.set(itdmIO.getGenarea());
	}

protected void getNumberYear2040()
	{
		start2041();
	}

protected void start2041()
	{
		chdrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrcmcIO.setChdrnum(comlinkrec.chdrnum);
		chdrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrcmcIO);
		if (isNE(chdrcmcIO.getStatuz(),varcom.oK)
		&& isNE(chdrcmcIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrcmcIO.getParams());
			syserrrec.statuz.set(chdrcmcIO.getStatuz());
			fatalError9000();
		}
		datcon3rec.intDate2.set(chdrcmcIO.getPtdate());
		datcon3rec.intDate1.set(comlinkrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(chdrcmcIO.getParams());
			syserrrec.statuz.set(chdrcmcIO.getStatuz());
			fatalError9000();
		}
		if (isLT(datcon3rec.freqFactor,1)) {
			wsaaFreq.set(1);
		}
		else {
			datcon3rec.freqFactor.add(.99999);
			wsaaFreq.set(datcon3rec.freqFactor);
		}
	}

protected void searchRate2060()
	{
		try {
			para2060();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
	}

protected void para2060()
	{
		for (wsaaCount.set(1); !(isGT(wsaaCount,10)
		|| isEQ(wsaaFound,"Y")); wsaaCount.add(1)){
			if (isEQ(tr520rec.zcomcode[wsaaCount.toInt()],comlinkrec.zcomcode)) {
				if (isEQ(wsaaCount,1)){
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc01,tr520rec.zryrperc02),tr520rec.zryrperc03),tr520rec.zryrperc04),tr520rec.zryrperc05),tr520rec.zryrperc06),tr520rec.zryrperc07));
				}
				else if (isEQ(wsaaCount,2)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,7));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc08,tr520rec.zryrperc09),tr520rec.zryrperc10),tr520rec.zryrperc11),tr520rec.zryrperc12),tr520rec.zryrperc13),tr520rec.zryrperc14));
				}
				else if (isEQ(wsaaCount,3)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,14));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc15,tr520rec.zryrperc16),tr520rec.zryrperc17),tr520rec.zryrperc18),tr520rec.zryrperc19),tr520rec.zryrperc20),tr520rec.zryrperc21));
				}
				else if (isEQ(wsaaCount,4)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,21));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc22,tr520rec.zryrperc23),tr520rec.zryrperc24),tr520rec.zryrperc25),tr520rec.zryrperc26),tr520rec.zryrperc27),tr520rec.zryrperc28));
				}
				else if (isEQ(wsaaCount,5)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,28));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc29,tr520rec.zryrperc30),tr520rec.zryrperc31),tr520rec.zryrperc32),tr520rec.zryrperc33),tr520rec.zryrperc34),tr520rec.zryrperc35));
				}
				else if (isEQ(wsaaCount,6)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,35));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc36,tr520rec.zryrperc37),tr520rec.zryrperc38),tr520rec.zryrperc39),tr520rec.zryrperc40),tr520rec.zryrperc41),tr520rec.zryrperc42));
				}
				else if (isEQ(wsaaCount,7)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,42));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc43,tr520rec.zryrperc44),tr520rec.zryrperc45),tr520rec.zryrperc46),tr520rec.zryrperc47),tr520rec.zryrperc48),tr520rec.zryrperc49));
				}
				else if (isEQ(wsaaCount,8)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,49));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc50,tr520rec.zryrperc51),tr520rec.zryrperc52),tr520rec.zryrperc53),tr520rec.zryrperc54),tr520rec.zryrperc55),tr520rec.zryrperc56));
				}
				else if (isEQ(wsaaCount,9)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,56));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc57,tr520rec.zryrperc58),tr520rec.zryrperc59),tr520rec.zryrperc60),tr520rec.zryrperc61),tr520rec.zryrperc62),tr520rec.zryrperc63));
				}
				else if (isEQ(wsaaCount,10)){
					compute(wsaaFreq, 0).set(add(wsaaFreq,63));
					compute(wsaaTotRate, 2).set(add(add(add(add(add(add(tr520rec.zryrperc64,tr520rec.zryrperc65),tr520rec.zryrperc66),tr520rec.zryrperc67),tr520rec.zryrperc68),tr520rec.zryrperc69),tr520rec.zryrperc70));
				}
				wsaaFound = "Y";
				goTo(GotoLabel.exit2060);
			}
		}
	}

protected void fatalError9000()
	{
		try {
			db9000();
		}
		catch (GOTOException e){
			LOGGER.debug("Catched Goto");
		}
		finally{
			dbExit9000();
		}
	}

protected void db9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9000);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9000()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
