/*
 * File: Zrcapf.java
 * Date: July 21, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.model;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class Zrcapf implements Serializable {
	private long uniqueNumber;
	private Character agntcoy;
	private String agntnum;
	private String agntype;
	private String mlagttyp;
	private String zrecruit;
	private String validflag;
	private BigDecimal effdate;
	private String userProfile;
	private String jobName;
	private Timestamp datime;

	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public Character getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Character agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public String getAgntype() {
		return agntype;
	}
	public void setAgntype(String agntype) {
		this.agntype = agntype;
	}
	public String getMlagttyp() {
		return mlagttyp;
	}
	public void setMlagttyp(String mlagttyp) {
		this.mlagttyp = mlagttyp;
	}
	public String getZrecruit() {
		return zrecruit;
	}
	public void setZrecruit(String zrecruit) {
		this.zrecruit = zrecruit;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public BigDecimal getEffdate() {
		return effdate;
	}
	public void setEffdate(BigDecimal effdate) {
		this.effdate = effdate;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Timestamp getDatime() {
		return new Timestamp(datime.getTime());//IJTI-316
	}
	public void setDatime(Timestamp datime) {
		this.datime = new Timestamp(datime.getTime());//IJTI-314
	}
	
			
}
