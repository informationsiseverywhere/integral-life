package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl02REC Date: 11 November 2019 Author: vdivisala
 */
public class Tjl02rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl02Rec = new FixedLengthStringData(380);
	public FixedLengthStringData qlfys = new FixedLengthStringData(80).isAPartOf(tjl02Rec, 0);
	public FixedLengthStringData[] qlfy = FLSArrayPartOfStructure(10, 8, qlfys, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(qlfys, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfy01 = DD.qlfy.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData qlfy02 = DD.qlfy.copy().isAPartOf(filler1, 8);
	public FixedLengthStringData qlfy03 = DD.qlfy.copy().isAPartOf(filler1, 16);
	public FixedLengthStringData qlfy04 = DD.qlfy.copy().isAPartOf(filler1, 24);
	public FixedLengthStringData qlfy05 = DD.qlfy.copy().isAPartOf(filler1, 32);
	public FixedLengthStringData qlfy06 = DD.qlfy.copy().isAPartOf(filler1, 40);
	public FixedLengthStringData qlfy07 = DD.qlfy.copy().isAPartOf(filler1, 48);
	public FixedLengthStringData qlfy08 = DD.qlfy.copy().isAPartOf(filler1, 56);
	public FixedLengthStringData qlfy09 = DD.qlfy.copy().isAPartOf(filler1, 64);
	public FixedLengthStringData qlfy10 = DD.qlfy.copy().isAPartOf(filler1, 72);
	public FixedLengthStringData qlfydescs = new FixedLengthStringData(300).isAPartOf(tjl02Rec, 80);
	public FixedLengthStringData[] qlfydesc = FLSArrayPartOfStructure(10, 30, qlfydescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(300).isAPartOf(qlfydescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfydesc01 = DD.qlfydesc.copy().isAPartOf(filler, 0);
	public FixedLengthStringData qlfydesc02 = DD.qlfydesc.copy().isAPartOf(filler, 30);
	public FixedLengthStringData qlfydesc03 = DD.qlfydesc.copy().isAPartOf(filler, 60);
	public FixedLengthStringData qlfydesc04 = DD.qlfydesc.copy().isAPartOf(filler, 90);
	public FixedLengthStringData qlfydesc05 = DD.qlfydesc.copy().isAPartOf(filler, 120);
	public FixedLengthStringData qlfydesc06 = DD.qlfydesc.copy().isAPartOf(filler, 150);
	public FixedLengthStringData qlfydesc07 = DD.qlfydesc.copy().isAPartOf(filler, 180);
	public FixedLengthStringData qlfydesc08 = DD.qlfydesc.copy().isAPartOf(filler, 210);
	public FixedLengthStringData qlfydesc09 = DD.qlfydesc.copy().isAPartOf(filler, 240);
	public FixedLengthStringData qlfydesc10 = DD.qlfydesc.copy().isAPartOf(filler, 270);

	public void initialize() {
		COBOLFunctions.initialize(tjl02Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl02Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}