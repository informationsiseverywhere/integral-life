/*
 * File: P5042at.java
 * Date: 29 August 2009 23:59:47
 * Author: Quipoz Limited
 *
 * Class transformed from P5042AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.recordstructures.Agtchgrec;
import com.csc.life.agents.tablestructures.T6688rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  Agent Commission Change AT module.
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
*  This AT module is called  by  the  Agent  Commission  Change
*  Program  P5042.  The  contract  number  is  passed in the AT
*  parameters area as the "primary key".
*
*  For the agent and contract selected    we  must  update  the
*  relevant records.
*
*  In  Force  contracts  require  AGCM's to be updated together
*  with PCDD's.
*
*  Proposal status contracts require either the CHDR  Servicing
*  Agent  to  be updated or the PCDD dependent on whether there
*  is more than one agent or not. Basically if only  one  agent
*  is  entered  at  New Business time no PCDD record is created
*  until Issue. It is  only  when  a  split  of  commission  is
*  entered at proposal that a PCDD record is written.
*
*  A  new  table  has been set up that is keyed by status code.
*  T6688's extra data screen will simply have one  field  which
*  determines  which  subroutine  will  be  called.  There  are
*  currently 2 major subroutines that will be called:-
*
*  AGTCHGIF - In Force contract processing
*  AGTCHGPR - Proposal contract processing.
*
*  Mainline Processing.
*  ~~~~~~~~~~~~~~~~~~~~
*  Initialise.
*
*  Get Todays date by calling DATCON1.
*
*  Readr Contract Header (CHDRLIF) and use the STATCODE of  the
*  CHDR to read T6688.
*
*  Processing.
*
*  Set up the AGTCHGREC fields and call T6688-AGTCHGSUB.
*
*  Only update the CHDR record with the new agent number if
*  the WSAA-COMIND flag is 'Y' - otherwise go to Release the
*  Softlock.
*
*  Add  1  to the Tranno and Write a new CHDR record. Noting to
* </pre>
*/
public class P5042at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5042AT");
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
		/* TABLES */
	private static final String t6688 = "T6688";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T6688rec t6688rec = new T6688rec();
	private Agtchgrec agtchgrec = new Agtchgrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Atmodrec atmodrec = new Atmodrec();
	private WsaaTransactionRecInner wsaaTransactionRecInner = new WsaaTransactionRecInner();

	public P5042at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		updateChdr4000();
		writePtrn5000();
		/*NO-UPDATES*/
		housekeepingTerminate3000();
		/*EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	* Initialise.                                                 *
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1001();
	}

protected void initialise1001()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		wsaaTransactionRecInner.wsaaTransactionRec.set(atmodrec.transArea);
		wsaaBatckey.set(atmodrec.batchKey);
		wsaaBatckey.batcBatcactyr.set(wsaaTransactionRecInner.wsaaBatcactyr);
		wsaaBatckey.batcBatcactmn.set(wsaaTransactionRecInner.wsaaBatcactmn);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		wsaaStatcode.set(chdrlifIO.getStatcode());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(t6688);
		itemIO.setItemitem(wsaaStatcode);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t6688rec.t6688Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		process2001();
	}

protected void process2001()
	{
		agtchgrec.statuz.set("****");
		agtchgrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		agtchgrec.chdrnum.set(chdrlifIO.getChdrnum());
		agtchgrec.life.set(SPACES);
		agtchgrec.coverage.set(SPACES);
		agtchgrec.rider.set(SPACES);
		agtchgrec.updateFlag.set(SPACES);
		agtchgrec.seqno.set(ZERO);
		agtchgrec.planSuffix.set(ZERO);
		agtchgrec.tranno.set(chdrlifIO.getTranno());
		agtchgrec.agntnumOld.set(wsaaTransactionRecInner.wsaaAgentnum);
		agtchgrec.agntnumNew.set(wsaaTransactionRecInner.wsaaNewagt);
		agtchgrec.initCommFlag.set(wsaaTransactionRecInner.wsaaInalcom);
		agtchgrec.rnwlCommFlag.set(wsaaTransactionRecInner.wsaaRnwlcom);
		agtchgrec.servCommFlag.set(wsaaTransactionRecInner.wsaaServcom);
		agtchgrec.orCommFlag.set(wsaaTransactionRecInner.wsaaZrorcomm);
		agtchgrec.language.set(atmodrec.language);
		agtchgrec.batckey.set(wsaaBatckey);
		agtchgrec.termid.set(wsaaTransactionRecInner.wsaaTermid);
		agtchgrec.user.set(wsaaTransactionRecInner.wsaaUser);
		agtchgrec.transactionDate.set(wsaaTransactionRecInner.wsaaEffdate);
		agtchgrec.transactionTime.set(wsaaTransactionRecInner.wsaaTransactionTime);
		callProgram(t6688rec.agtchgsub, agtchgrec.agentRec);
		if (isNE(agtchgrec.statuz, varcom.oK)) {
			syserrrec.params.set(agtchgrec.agentRec);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Housekeeping prior to termination.                          *
	* </pre>
	*/
protected void housekeepingTerminate3000()
	{
		/*HOUSEKEEPING-TERMINATE*/
		releaseSoftlock3300();
		/*EXIT*/
	}

protected void releaseSoftlock3300()
	{
		releaseSoftlock3301();
	}

protected void releaseSoftlock3301()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		/*  MOVE 'T703'                 TO SFTL-TRANSACTION.             */
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}

protected void updateChdr4000()
	{
		go4100();
	}

protected void go4100()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(atmodrec.company);
		chdrlifIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(wsaaTransactionRecInner.wsaaEffdate);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(wsaaTransactionRecInner.wsaaComind, "Y")) {
			chdrlifIO.setAgntnum(wsaaTransactionRecInner.wsaaNewagt);
		}
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(wsaaTransactionRecInner.wsaaEffdate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(wsaaTransactionRecInner.wsaaComind, "Y")) {
			callBldenrl4100();
		}
	}

protected void callBldenrl4100()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlifIO.getChdrpfx());
		bldenrlrec.company.set(chdrlifIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlifIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void writePtrn5000()
	{
		writePtrn5100();
	}

protected void writePtrn5100()
	{
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx(wsaaBatckey.batcBatcpfx);
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setTransactionDate(wsaaTransactionRecInner.wsaaTransactionDate);
		ptrnIO.setTransactionTime(wsaaTransactionRecInner.wsaaTransactionTime);
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setTermid(wsaaTransactionRecInner.wsaaTermid);
		ptrnIO.setUser(wsaaTransactionRecInner.wsaaUser);
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			xxxxFatalError();
		}
	}
/*
 * Class transformed  from Data Structure WSAA-TRANSACTION-REC--INNER
 */
private static final class WsaaTransactionRecInner {

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(209);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 4);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 8);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 12);
	private FixedLengthStringData wsaaAgentnum = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 16).init(SPACES);
	private FixedLengthStringData wsaaNewagt = new FixedLengthStringData(8).isAPartOf(wsaaTransactionRec, 24).init(SPACES);
	private FixedLengthStringData wsaaServcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 32).init(SPACES);
	private FixedLengthStringData wsaaRnwlcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 33).init(SPACES);
	private FixedLengthStringData wsaaInalcom = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 34).init(SPACES);
	private FixedLengthStringData wsaaComind = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 35).init(SPACES);
	private FixedLengthStringData wsaaZrorcomm = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 36).init(SPACES);
	private PackedDecimalData wsaaBatcactyr = new PackedDecimalData(4, 0).isAPartOf(wsaaTransactionRec, 37).init(ZERO);
	private PackedDecimalData wsaaBatcactmn = new PackedDecimalData(2, 0).isAPartOf(wsaaTransactionRec, 40).init(ZERO);
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 42).init(ZERO).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(159).isAPartOf(wsaaTransactionRec, 50, FILLER).init(SPACES);
}
}
