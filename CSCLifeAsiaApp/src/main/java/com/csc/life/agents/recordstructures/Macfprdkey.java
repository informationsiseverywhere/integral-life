package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:33
 * Description:
 * Copybook name: MACFPRDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfprdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfprdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfprdKey = new FixedLengthStringData(64).isAPartOf(macfprdFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfprdAgntcoy = new FixedLengthStringData(1).isAPartOf(macfprdKey, 0);
  	public FixedLengthStringData macfprdAgntnum = new FixedLengthStringData(8).isAPartOf(macfprdKey, 1);
  	public PackedDecimalData macfprdEffdate = new PackedDecimalData(8, 0).isAPartOf(macfprdKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(macfprdKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfprdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfprdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}