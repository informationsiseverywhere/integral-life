package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgpydocTableDAM.java
 * Date: Tue, 3 Dec 2013 04:09:50
 * Class transformed from AGPYDOC.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgpydocTableDAM extends AgpypfTableDAM {

	public AgpydocTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGPYDOC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BATCCOY"
		             + ", RDOCNUM"
		             + ", TRANNO"
		             + ", JRNSEQ";
		
		QUALIFIEDCOLUMNS = 
		            "SACSCODE, " +
		            "SACSTYP, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "RDOCNUM, " +
		            "TRANNO, " +
		            "JRNSEQ, " +
		            "ORIGAMT, " +
		            "TRANREF, " +
		            "TRANDESC, " +
		            "CRATE, " +
		            "ACCTAMT, " +
		            "GENLCOY, " +
		            "GENLCUR, " +
		            "GLCODE, " +
		            "GLSIGN, " +
		            "POSTYEAR, " +
		            "POSTMONTH, " +
		            "EFFDATE, " +
		            "RCAMT, " +
		            "FRCDATE, " +
		            "SUPRFLG, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "TERMID, " +
		            "RLDGCOY, " +
		            "RLDGACCT, " +
		            "ORIGCURR, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BATCCOY ASC, " +
		            "RDOCNUM ASC, " +
		            "TRANNO ASC, " +
		            "JRNSEQ ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "BATCCOY DESC, " +
		            "RDOCNUM DESC, " +
		            "TRANNO DESC, " +
		            "JRNSEQ DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               sacscode,
                               sacstyp,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               rdocnum,
                               tranno,
                               jrnseq,
                               origamt,
                               tranref,
                               trandesc,
                               crate,
                               acctamt,
                               genlcoy,
                               genlcur,
                               glcode,
                               glsign,
                               postyear,
                               postmonth,
                               effdate,
                               rcamt,
                               frcdate,
                               suprflg,
                               transactionDate,
                               transactionTime,
                               user,
                               termid,
                               rldgcoy,
                               rldgacct,
                               origcurr,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(241);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getBatccoy().toInternal()
					+ getRdocnum().toInternal()
					+ getTranno().toInternal()
					+ getJrnseq().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, jrnseq);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(9);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller30.setInternal(batccoy.toInternal());
	nonKeyFiller90.setInternal(rdocnum.toInternal());
	nonKeyFiller100.setInternal(tranno.toInternal());
	nonKeyFiller110.setInternal(jrnseq.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(250);
		
		nonKeyData.set(
					getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ nonKeyFiller30.toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ getOrigamt().toInternal()
					+ getTranref().toInternal()
					+ getTrandesc().toInternal()
					+ getCrate().toInternal()
					+ getAcctamt().toInternal()
					+ getGenlcoy().toInternal()
					+ getGenlcur().toInternal()
					+ getGlcode().toInternal()
					+ getGlsign().toInternal()
					+ getPostyear().toInternal()
					+ getPostmonth().toInternal()
					+ getEffdate().toInternal()
					+ getRcamt().toInternal()
					+ getFrcdate().toInternal()
					+ getSuprflg().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getTermid().toInternal()
					+ getRldgcoy().toInternal()
					+ getRldgacct().toInternal()
					+ getOrigcurr().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, origamt);
			what = ExternalData.chop(what, tranref);
			what = ExternalData.chop(what, trandesc);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, acctamt);
			what = ExternalData.chop(what, genlcoy);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, glcode);
			what = ExternalData.chop(what, glsign);
			what = ExternalData.chop(what, postyear);
			what = ExternalData.chop(what, postmonth);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, rcamt);
			what = ExternalData.chop(what, frcdate);
			what = ExternalData.chop(what, suprflg);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public PackedDecimalData getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(Object what) {
		setJrnseq(what, false);
	}
	public void setJrnseq(Object what, boolean rounded) {
		if (rounded)
			jrnseq.setRounded(what);
		else
			jrnseq.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}	
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}	
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}	
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}	
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}	
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}	
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}	
	public PackedDecimalData getOrigamt() {
		return origamt;
	}
	public void setOrigamt(Object what) {
		setOrigamt(what, false);
	}
	public void setOrigamt(Object what, boolean rounded) {
		if (rounded)
			origamt.setRounded(what);
		else
			origamt.set(what);
	}	
	public FixedLengthStringData getTranref() {
		return tranref;
	}
	public void setTranref(Object what) {
		tranref.set(what);
	}	
	public FixedLengthStringData getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(Object what) {
		trandesc.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public PackedDecimalData getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(Object what) {
		setAcctamt(what, false);
	}
	public void setAcctamt(Object what, boolean rounded) {
		if (rounded)
			acctamt.setRounded(what);
		else
			acctamt.set(what);
	}	
	public FixedLengthStringData getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(Object what) {
		genlcoy.set(what);
	}	
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(Object what) {
		genlcur.set(what);
	}	
	public FixedLengthStringData getGlcode() {
		return glcode;
	}
	public void setGlcode(Object what) {
		glcode.set(what);
	}	
	public FixedLengthStringData getGlsign() {
		return glsign;
	}
	public void setGlsign(Object what) {
		glsign.set(what);
	}	
	public FixedLengthStringData getPostyear() {
		return postyear;
	}
	public void setPostyear(Object what) {
		postyear.set(what);
	}	
	public FixedLengthStringData getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(Object what) {
		postmonth.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getRcamt() {
		return rcamt;
	}
	public void setRcamt(Object what) {
		setRcamt(what, false);
	}
	public void setRcamt(Object what, boolean rounded) {
		if (rounded)
			rcamt.setRounded(what);
		else
			rcamt.set(what);
	}	
	public PackedDecimalData getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(Object what) {
		setFrcdate(what, false);
	}
	public void setFrcdate(Object what, boolean rounded) {
		if (rounded)
			frcdate.setRounded(what);
		else
			frcdate.set(what);
	}	
	public FixedLengthStringData getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(Object what) {
		suprflg.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}	
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}	
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		batccoy.clear();
		rdocnum.clear();
		tranno.clear();
		jrnseq.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		sacscode.clear();
		sacstyp.clear();
		nonKeyFiller30.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		origamt.clear();
		tranref.clear();
		trandesc.clear();
		crate.clear();
		acctamt.clear();
		genlcoy.clear();
		genlcur.clear();
		glcode.clear();
		glsign.clear();
		postyear.clear();
		postmonth.clear();
		effdate.clear();
		rcamt.clear();
		frcdate.clear();
		suprflg.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		termid.clear();
		rldgcoy.clear();
		rldgacct.clear();
		origcurr.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}