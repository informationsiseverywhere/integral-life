package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AgcmpfDAO extends BaseDAO<Agcmpf> {
	public List<Agcmpf> searchAgcmdmnRecord(String chdrcoy,String chdrnum, String agntnum) throws SQLRuntimeException;
	public List<Agcmpf> searchAgcmdmnRecord(String chdrcoy,String chdrnum) throws SQLRuntimeException;
	public Agcmpf searchAgcmdmnRecord(long unique_number)  throws SQLRuntimeException;
    public void updateAgcmRecord(Agcmpf agcmpf);  // ILIFE-8163
    public List<Agcmpf> searchAgcmpfRecord(Agcmpf agcmpf) throws SQLRuntimeException;
}
