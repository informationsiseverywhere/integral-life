/*
 * File: Pjl02.java
 * Date: 11 November 2019
 * Author: vdivisala
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.Map;

import com.csc.life.agents.screens.Sjl02ScreenVars;
import com.csc.life.agents.tablestructures.Tjl02rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * This table is for the additional agent qualifications required (if any).
 * Otherwise, there is no need to set-up the item in this T Table.
 * Entry should be valid i.e. existing in the T Table TJL01.
 * </pre>
 */
public class Pjl02 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL02");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/* ERRORS */
	private String e207 = "E207";
	private String rl39 = "RL39";
	private String h357 = "H357";

	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private String wsaaItemlang;
	private String tjl01 = "TJL01";
	private Tjl02rec tjl02rec = new Tjl02rec();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private Map<String, Descpf> tjl01Items = null;
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl02ScreenVars sv = ScreenProgram.getScreenVars(Sjl02ScreenVars.class);

	public Pjl02() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl02", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		sv.dataArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		String longdesc = iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem,
				wsaaItemlang);
		sv.longdesc.set(longdesc);

		Itempf itempf = itemDAO.findItemBySeq(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, "1", wsaaItemseq);
		if (itempf == null) {
			return;
		} else {
			tjl02rec.tjl02Rec.set(itempf.getGenareaString());
		}

		if (isEQ(itempf.getItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		} else {
			sv.itmfrm.set(itempf.getItmfrm());
		}
		if (isEQ(itempf.getItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		} else {
			sv.itmto.set(itempf.getItmto());
		}
		sv.qlfys.set(tjl02rec.qlfys);
		if(isNE(sv.qlfys, SPACES)) {
			getQlfydesc();
		}
	}
	
	// Check for Qualification Description
	protected void getQlfydesc() {
		if(tjl01Items==null) {
			tjl01Items = descDAO.getItems(wsaaItempfx, wsaaItemcoy, tjl01, wsaaItemlang);
		}
		if (tjl01Items.size() > 0) {
			for (int i = 1; i <= 10; i++) {
				if (isNE(sv.qlfy[i], SPACES)) {
					sv.qlfydesc[i].set(tjl01Items.get(sv.qlfy[i].toString().trim()).getLongdesc());
				} else {
					sv.qlfydesc[i].set(SPACES);
				}
			}
		}
	}

	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
	}

	@SuppressWarnings("static-access")
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I") || isNE(sv.itmfrmErr, SPACES)) {
			return;
		}
		validate2010();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void validate2010() {
		// Must have at least 1 entry
		if (isEQ(sv.qlfys, SPACES)) {
			for (int i = 1; i <= 10; i++) {
				sv.qlfyErr[i].set(e207);
				sv.qlfydesc[i].set(SPACES);
			}
			return;
		} else {
			getQlfydesc();
		}
		for (int i = 1; i <= 10; i++) {
			if (isEQ(sv.qlfy[i], SPACES)) {
				// No blank entries in between
				for (int j = i + 1; j <= 10; j++) {
					if (isNE(sv.qlfy[j], SPACES)) {
						sv.qlfyErr[i].set(rl39);
						break;
					}
				}
			} else {
				// Duplicate entries not allowed
				for (int j = i + 1; j <= 10; j++) {
					if (isNE(sv.qlfy[j], SPACES) && isEQ(sv.qlfy[i], sv.qlfy[j])) {
						sv.qlfyErr[i].set(h357);
						sv.qlfyErr[j].set(h357);
					}
				}
			}
		}
	}

	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.qlfys, tjl02rec.qlfys)) {
			tjl02rec.qlfys.set(sv.qlfys);
			com.csc.smart400framework.dataaccess.model.Itempf item = new com.csc.smart400framework.dataaccess.model.Itempf();
			item.setGenarea(tjl02rec.tjl02Rec.toString().getBytes());
			item.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
					.getGENAREAJString(tjl02rec.tjl02Rec.toString().getBytes(), tjl02rec));
			item.setItempfx(wsaaItempfx);
			item.setItemcoy(wsaaItemcoy);
			item.setItemtabl(wsaaItemtabl);
			item.setItemitem(wsaaItemitem);
			item.setItemseq(wsaaItemseq);
			itemDAO.updateSmartTableItem(item);
		}
	}

	protected void whereNext4000() {
		wsspcomn.programPtr.add(1);
	}
}