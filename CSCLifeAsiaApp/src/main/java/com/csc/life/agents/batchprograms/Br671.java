/*
 * File: Br671.java
 * Date: 29 August 2009 22:35:36
 * Author: Quipoz Limited
 *
 * Class transformed from BR671.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.fsu.reinsurance.dataaccess.TtrcTableDAM;
import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwpolrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T1697rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   This is to extract of premium payments records
* ***********************************************************
*
*   In INPUT-OUTPUT SECTION, we specify the output file ZBNWPF in which
*   the agent movement records are written.
*
*   Initialise
*     - In 1000-INITIALISE SECTION We override the Database file ZBNWPF
*       for records written.
*
*   Read
*     - In 2000-READ-FILE SECTION, we fetch every record from the
*       cursor and then process those required records
*     - In 2500-EDIT SECTION,We would process those transactions
*       that set up on the table T1697 only. Bypass those
*       transactions which are not on the table.
*
*   Edit
*     - In 3000-UPDATE SECTION, for every record read
*       we write the record together with required information
*       by performing the section 3100-WRITE
*
*   Update
*     - In 4000-UPDATE SECTION, we close the output file ZBNWPF
*       and copy the file to the FTP library which is given in
*       process definition.
*     - Create the section A100-DATE-CONVERSION to  convert the
*       in the format MM/DD/YYYY.
*     - Create the section A200-PACK-STRING to pack the data string
*       such that semi colon will be added to separate the data  .
*
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of records skip
*     03  -  Number of records written
*
*   ERROR PROCESSING:
*     IF A SYSTEM ERROR MOVE THE ERROR CODE INTO THE SYSR-STATUZ
*     IF A DATABASE ERROR MOVE THE XXXX-PARAMS TO SYSR-PARAMS.
*     PERFORM THE 600-FATAL-ERROR SECTION.
*
***********************************************************************
* </pre>
*/
public class Br671 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlptrnpf1rs = null;
	private java.sql.PreparedStatement sqlptrnpf1ps = null;
	private java.sql.Connection sqlptrnpf1conn = null;
	private String sqlptrnpf1 = "";
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR671");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
		/*define the variables for extracting the records in given date*/
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLength = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcdeFound = new FixedLengthStringData(1);
	private Validator trcdeFound = new Validator(wsaaTrcdeFound, "Y");

	private FixedLengthStringData wsaaFirstIssue = new FixedLengthStringData(1);
	private Validator firstIssue = new Validator(wsaaFirstIssue, "Y");

	private FixedLengthStringData wsaaTransaction = new FixedLengthStringData(4);
	private Validator contractIssue = new Validator(wsaaTransaction, "T642");

	private FixedLengthStringData wsaaTerminated = new FixedLengthStringData(1);
	private Validator terminated = new Validator(wsaaTerminated, "Y");
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);
	private FixedLengthStringData wsaaStoredChdrnum = new FixedLengthStringData(8).init(SPACES);
	private PackedDecimalData wsaaEffectiveDate = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private static final int wsaaT1697MaxTranscd = 72;
	private ZonedDecimalData wsaaInptAmount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaIntmAmount = new ZonedDecimalData(17, 2).setUnsigned();

	private FixedLengthStringData wsaaIntmAmountX = new FixedLengthStringData(17).isAPartOf(wsaaIntmAmount, 0, REDEFINE);
	private FixedLengthStringData wsaaIntmInteger = new FixedLengthStringData(15).isAPartOf(wsaaIntmAmountX, 0);
	private FixedLengthStringData wsaaIntmDecimal = new FixedLengthStringData(2).isAPartOf(wsaaIntmAmountX, 15);

	private FixedLengthStringData wsaaNegativeSign = new FixedLengthStringData(1);
	private Validator negativeSign = new Validator(wsaaNegativeSign, "Y");
	private FixedLengthStringData wsaaConvAmount = new FixedLengthStringData(19);
	private PackedDecimalData wsaaSumCovtlnbInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2);

	private FixedLengthStringData filler5 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqx, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqn = new ZonedDecimalData(2, 0).isAPartOf(filler5, 0).setUnsigned();

		/* SQL-PTRNPF */
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(15);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(ptrnrec, 0);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(ptrnrec, 8);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(ptrnrec, 12);
	private static final String t1697 = "T1697";
	private static final String t5688 = "T5688";
	private static final String t5679 = "T5679";
	private static final String tr657 = "TR657";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler7 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler7, 5);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private TtrcTableDAM ttrcIO = new TtrcTableDAM();
	private Desckey wsaaDesckey = new Desckey();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Zbnwpolrec zbnwpolrec = new Zbnwpolrec();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();
	private T1697rec t1697rec = new T1697rec();
	private T5679rec t5679rec = new T5679rec();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfFile2080,
		exit2090,
		call3820,
		exit3890,
		call3920,
		next3980,
		exit3990,
		a430Loop,
		a440Set,
		a490Exit
	}

	public Br671() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		defineCursor1060();
	}

protected void initialise1010()
	{
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaZbnwFn);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaCompany.set(bprdIO.getCompany());
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		zbnwpf.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void defineCursor1060()
	{
		/*  Define the query required by declaring a cursor*/
		sqlptrnpf1 = " SELECT  CHDRNUM, BATCTRCDE, TRANNO" +
" FROM   " + getAppVars().getTableNameOverriden("PTRNPF") + " " +
" WHERE CHDRCOY = ?" +
" AND VALIDFLAG <> '2'" +
" AND TRDT = ?" +
" ORDER BY CHDRNUM";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlptrnpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PtrnpfTableDAM());
			sqlptrnpf1ps = getAppVars().prepareStatementEmbeded(sqlptrnpf1conn, sqlptrnpf1, "PTRNPF");
			getAppVars().setDBString(sqlptrnpf1ps, 1, wsaaCompany);
			getAppVars().setDBNumber(sqlptrnpf1ps, 2, wsaaEffectiveDate);
			sqlptrnpf1rs = getAppVars().executeQuery(sqlptrnpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		readT16971100();
		readT56791200();
		/*EXIT*/
	}

protected void readT16971100()
	{
		read1110();
	}

protected void read1110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t1697);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1697rec.t1697Rec.set(itemIO.getGenarea());
	}

protected void readT56791200()
	{
		read1210();
	}

protected void read1210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
				case endOfFile2080:
					endOfFile2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlptrnpf1rs)) {
				getAppVars().getDBObject(sqlptrnpf1rs, 1, sqlChdrnum);
				getAppVars().getDBObject(sqlptrnpf1rs, 2, sqlBatctrcde);
				getAppVars().getDBObject(sqlptrnpf1rs, 3, sqlTranno);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
			edit2510();
		}

protected void edit2510()
	{
		/* Check record is required for processing.*/
		/* Softlock the record if it is to be updated.*/
		/* Initialize parameters*/
		wsspEdterror.set(varcom.oK);
		wsaaTrcdeFound.set("N");
		wsaaNegativeSign.set("N");
		for (wsaaIx.set(1); !(isGT(wsaaIx,wsaaT1697MaxTranscd)
		|| trcdeFound.isTrue()); wsaaIx.add(1)){
			if (isEQ(sqlBatctrcde,t1697rec.transcd[wsaaIx.toInt()])) {
				trcdeFound.setTrue();
			}
		}
		if (!trcdeFound.isTrue()) {
			wsspEdterror.set(SPACES);
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			return ;
		}
		if (isEQ(sqlChdrnum,wsaaStoredChdrnum)) {
			wsspEdterror.set(SPACES);
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			return ;
		}
		else {
			wsaaStoredChdrnum.set(sqlChdrnum);
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		/* Retrieve the policy header & coverages details*/
		readChdr3100();
		/*  Update database records.*/
		write3100();
		/*EXIT*/
	}

protected void readChdr3100()
	{
		/*READ*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(bprdIO.getCompany());
		chdrlifIO.setChdrnum(sqlChdrnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)
		&& isNE(chdrlifIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void write3100()
	{
		writePara3100();
	}

protected void writePara3100()
	{
		/* Place any additional commitment processing in here.*/
		zbnwpolrec.bnwpolRec.set(SPACES);
		a600GetHpadInfo();
		a700GetTtrc();
		wsaaTransaction.set(sqlBatctrcde);
		if (contractIssue.isTrue()) {
			checkFirstTimeIssue3900();
		}
		if (contractIssue.isTrue()
		&& firstIssue.isTrue()) {
			zbnwpolrec.bnwpolTrcde.set("CR");
		}
		else {
			zbnwpolrec.bnwpolTrcde.set("UPD");
		}
		zbnwpolrec.bnwpolSource.set("LIFE/ASIA");
		/*    Polulate the company ID and state by calling*/
		/*    subroutine ZBWCMPY*/
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("POL");
		zbwcmpyrec.input.set(sqlChdrnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwpolrec.bnwpolCompany.set(zbwcmpyrec.bwCompany);
			zbnwpolrec.bnwpolState.set(zbwcmpyrec.bwCompany);
			zbnwpolrec.bnwpolResident.set(zbwcmpyrec.state);
			zbnwpolrec.bnwpolIssstate.set(zbwcmpyrec.state);
		}
		else {
			zbnwpolrec.bnwpolCompany.set(SPACES);
			zbnwpolrec.bnwpolState.set(SPACES);
			zbnwpolrec.bnwpolResident.set(SPACES);
			zbnwpolrec.bnwpolIssstate.set(SPACES);
		}
		zbnwpolrec.bnwpolChdrnum.set(sqlChdrnum);
		zbnwpolrec.bnwpolPlanid.set(chdrlifIO.getCnttype());
		zbnwpolrec.bnwpolProdcode.set(chdrlifIO.getCnttype());
		/*    Populate the plan description from T5688 via DESCIO*/
		wsaaDesckey.set(SPACES);
		wsaaDesckey.descDesccoy.set(bprdIO.getCompany());
		wsaaDesckey.descDesctabl.set(t5688);
		wsaaDesckey.descDescpfx.set(smtpfxcpy.item);
		wsaaDesckey.descDescitem.set(chdrlifIO.getCnttype());
		wsaaDesckey.descLanguage.set(bsscIO.getLanguage());
		a300GetDescription();
		zbnwpolrec.bnwpolPlandesc.set(descIO.getLongdesc());
		zbnwpolrec.bnwpolAgntnum.set(chdrlifIO.getAgntnum());
		zbnwpolrec.bnwpolGrpcde.set("I");
		zbnwpolrec.bnwpolGrpind.set(SPACES);
		wsaaInptDate.set(chdrlifIO.getOccdate());
		a100DateConversion();
		zbnwpolrec.bnwpolPlnefdte.set(wsaaConvDate);
		zbnwpolrec.bnwpolOccdate.set(wsaaConvDate);
		wsaaInptDate.set(hpadIO.getHuwdcdte());
		a100DateConversion();
		zbnwpolrec.bnwpolUwdcdte.set(wsaaConvDate);
		wsaaInptDate.set(hpadIO.getHissdte());
		a100DateConversion();
		zbnwpolrec.bnwpolIssdte.set(wsaaConvDate);
		zbnwpolrec.bnwpolBclntnum.set(SPACES);
		zbnwpolrec.bnwpolCownnum.set(chdrlifIO.getCownnum());
		/*    Retrieve the contract status description from TR657*/
		wsaaDesckey.set(SPACES);
		wsaaDesckey.descDesccoy.set(bprdIO.getCompany());
		wsaaDesckey.descDesctabl.set(tr657);
		wsaaDesckey.descDescpfx.set(smtpfxcpy.item);
		wsaaDesckey.descDescitem.set(chdrlifIO.getStatcode());
		wsaaDesckey.descLanguage.set(bsscIO.getLanguage());
		a300GetDescription();
		zbnwpolrec.bnwpolStatdesc.set(descIO.getLongdesc());
		/*    Check whether the contract has been terminated,*/
		terminated.setTrue();
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)
		|| !terminated.isTrue()); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIx.toInt()],chdrlifIO.getStatcode())) {
				wsaaTerminated.set("N");
			}
		}
		if (terminated.isTrue()) {
			wsaaInptDate.set(bsscIO.getEffectiveDate());
			a100DateConversion();
			zbnwpolrec.bnwpolTrmdate.set(wsaaConvDate);
		}
		else {
			zbnwpolrec.bnwpolTrmdate.set("99/99/9999");
		}
		zbnwpolrec.bnwpolPeriod.set(SPACES);
		wsaaInptDate.set(ttrcIO.getTtmprcdte());
		a100DateConversion();
		zbnwpolrec.bnwpolApplndte.set(wsaaConvDate);
		/*    Read the main coverage to populate the some information*/
		/*    stored in coverage level (COVRENQ for issued contract*/
		/*    while COVTLNB for proposals)*/
		if (isEQ(chdrlifIO.getValidflag(),"3")) {
			readCovt3300();
			if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
			|| isNE(covtlnbIO.getChdrcoy(),bprdIO.getCompany())
			|| isNE(covtlnbIO.getChdrnum(),sqlChdrnum)) {
				zbnwpolrec.bnwpolIssage.set(SPACES);
			}
			else {
				wsaaInptAmount.set(covtlnbIO.getAnbAtCcd01());
				a400LeftShift();
				zbnwpolrec.bnwpolIssage.set(wsaaConvAmount);
			}
		}
		else {
			readCovr3200();
			if (isEQ(covrenqIO.getStatuz(),varcom.endp)
			|| isNE(covrenqIO.getChdrcoy(),bprdIO.getCompany())
			|| isNE(covrenqIO.getChdrnum(),sqlChdrnum)) {
				zbnwpolrec.bnwpolIssage.set(SPACES);
			}
			else {
				wsaaInptAmount.set(covrenqIO.getAnbAtCcd());
				a400LeftShift();
				zbnwpolrec.bnwpolIssage.set(wsaaConvAmount);
			}
		}
		zbnwpolrec.bnwpolPayoptn.set(SPACES);
		zbnwpolrec.bnwpolPeriodc.set(SPACES);
		zbnwpolrec.bnwpolOcmscde.set(SPACES);
		zbnwpolrec.bnwpolComoptn.set(SPACES);
		/*    Left shit the modal premium CHDRLIF-INSTSTAMT06*/
		wsaaInstprem.set(chdrlifIO.getSinstamt06());
		if (isEQ(chdrlifIO.getValidflag(),"3")) {
			wsaaSumCovtlnbInstprem.set(ZERO);
			sumCovtlnbInstprem3800();
			wsaaInstprem.set(wsaaSumCovtlnbInstprem);
		}
		wsaaInptAmount.set(wsaaInstprem);
		a400LeftShift();
		zbnwpolrec.bnwpolInstprem.set(wsaaConvAmount);
		/*    Annual permium*/
		a500GetBillfreq();
		if (isEQ(payrIO.getStatuz(),varcom.mrnf)) {
			wsaaBillfreqx.set(ZERO);
		}
		else {
			wsaaBillfreqx.set(payrIO.getBillfreq());
		}
		compute(wsaaInptAmount, 2).set(mult(wsaaInstprem,wsaaBillfreqn));
		a400LeftShift();
		zbnwpolrec.bnwpolAnnprem.set(wsaaConvAmount);
		zbnwpolrec.bnwpolBillfreq.set(wsaaBillfreqn);
		zbnwpolrec.bnwpolParticipation.set(SPACES);
		zbnwpolrec.bnwpolPension.set(SPACES);
		zbnwpolrec.bnwpolQualification.set(SPACES);
		zbnwpolrec.bnwpolTarget.set(SPACES);
		zbnwpolrec.bnwpolReptype.set(SPACES);
		zbnwpolrec.bnwpolCwa.set("0");
		zbnwpolrec.bnwpolRollamt.set("0");
		/*   Read the Life file LIFELNB*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		readLifeDetails3400();
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			zbnwpolrec.bnwpolInsnum.set(SPACES);
		}
		else {
			zbnwpolrec.bnwpolInsnum.set(lifelnbIO.getLifcnum());
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(bsprIO.getFsuco());
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.function.set("LGNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		if (isEQ(namadrsrec.statuz,varcom.mrnf)) {
			zbnwpolrec.bnwpolInsname.set(SPACES);
		}
		else {
			zbnwpolrec.bnwpolInsname.set(namadrsrec.name);
		}
		wsaaInptDate.set(chdrlifIO.getOccdate());
		a100DateConversion();
		zbnwpolrec.bnwpolInssrdte.set(wsaaConvDate);
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		readLifeDetails3400();
		if (isEQ(lifelnbIO.getStatuz(),varcom.oK)) {
			namadrsrec.namadrsRec.set(SPACES);
			namadrsrec.language.set(bsscIO.getLanguage());
			namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
			namadrsrec.clntCompany.set(bsprIO.getFsuco());
			namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
			namadrsrec.function.set("LGNMN");
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz,varcom.oK)
			&& isNE(namadrsrec.statuz,varcom.mrnf)) {
				syserrrec.statuz.set(namadrsrec.statuz);
				fatalError600();
			}
			if (isEQ(namadrsrec.statuz,varcom.mrnf)) {
				zbnwpolrec.bnwpolJinsname.set(SPACES);
			}
			else {
				zbnwpolrec.bnwpolJinsname.set(namadrsrec.name);
				wsaaInptDate.set(chdrlifIO.getOccdate());
				a100DateConversion();
				zbnwpolrec.bnwpolJinssrdte.set(wsaaConvDate);
				zbnwpolrec.bnwpolJinstmdte.set("99/99/9999");
			}
		}
		else {
			zbnwpolrec.bnwpolJinsname.set(SPACES);
			zbnwpolrec.bnwpolJinssrdte.set(SPACES);
			zbnwpolrec.bnwpolJinstmdte.set(SPACES);
		}
		wsaaInptDate.set(bsscIO.getEffectiveDate());
		a100DateConversion();
		zbnwpolrec.bnwpolTrandate.set(wsaaConvDate);
		zbnwpolrec.bnwpolProcdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct03);
		callContot001();
	}

protected void readCovr3200()
	{
		read3210();
	}

protected void read3210()
	{
		covrenqIO.setParams(SPACES);
		covrenqIO.setStatuz(varcom.oK);
		covrenqIO.setChdrnum(sqlChdrnum);
		covrenqIO.setChdrcoy(bprdIO.getCompany());
		covrenqIO.setPlanSuffix(0);
		covrenqIO.setFormat(formatsInner.covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		covrenqIO.setLife(SPACES);
		covrenqIO.setCoverage(SPACES);
		covrenqIO.setRider(SPACES);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
	}

protected void readCovt3300()
	{
		read3310();
	}

protected void read3310()
	{
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setChdrcoy(bprdIO.getCompany());
		covtlnbIO.setChdrnum(sqlChdrnum);
		covtlnbIO.setLife("01");
		covtlnbIO.setCoverage("01");
		covtlnbIO.setRider(ZERO);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}

protected void readLifeDetails3400()
	{
		/*READ*/
		lifelnbIO.setChdrcoy(bprdIO.getCompany());
		lifelnbIO.setChdrnum(sqlChdrnum);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void sumCovtlnbInstprem3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para3800();
				case call3820:
					call3820();
					next3880();
				case exit3890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3800()
	{
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setChdrcoy(bprdIO.getCompany());
		covtlnbIO.setChdrnum(sqlChdrnum);
		covtlnbIO.setLife("01");
		covtlnbIO.setCoverage("01");
		covtlnbIO.setRider("00");
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
	}

protected void call3820()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)
		|| isNE(covtlnbIO.getChdrcoy(),bprdIO.getCompany())
		|| isNE(covtlnbIO.getChdrnum(),sqlChdrnum)) {
			covtlnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3890);
		}
	}

protected void next3880()
	{
		wsaaSumCovtlnbInstprem.add(covtlnbIO.getInstprem());
		covtlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3820);
	}

protected void checkFirstTimeIssue3900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para3910();
				case call3920:
					call3920();
					check3930();
				case next3980:
					next3980();
				case exit3990:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3910()
	{
		ptrnenqIO.setParams(SPACES);
		ptrnenqIO.setChdrcoy(bprdIO.getCompany());
		ptrnenqIO.setChdrnum(sqlChdrnum);
		ptrnenqIO.setTranno(sqlTranno);
		ptrnenqIO.setBatctrcde(sqlBatctrcde);
		ptrnenqIO.setFormat(ptrnrec);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		firstIssue.setTrue();
	}

protected void call3920()
	{
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		&& isNE(ptrnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		if (isEQ(ptrnenqIO.getStatuz(),varcom.endp)
		|| isNE(ptrnenqIO.getChdrcoy(),bprdIO.getCompany())
		|| isNE(ptrnenqIO.getChdrnum(),sqlChdrnum)) {
			ptrnenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3990);
		}
	}

protected void check3930()
	{
		if (isEQ(ptrnenqIO.getTranno(),sqlTranno)) {
			goTo(GotoLabel.next3980);
		}
		wsaaTransaction.set(ptrnenqIO.getBatctrcde());
		if (contractIssue.isTrue()) {
			wsaaFirstIssue.set("N");
			goTo(GotoLabel.exit3990);
		}
	}

protected void next3980()
	{
		ptrnenqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3920);
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlptrnpf1conn, sqlptrnpf1ps, sqlptrnpf1rs);
		/*  Close any open files.*/
		zbnwpf.close();
		/*  Put the file to FTP library for download*/
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaStatuz);
			stringVariable1.addExpression(bprdIO.getRunLibrary());
			stringVariable1.addExpression(wsaaZbnwFn);
			stringVariable1.addExpression(wsaaFtpLibrary);
			stringVariable1.addExpression(wsaaFtpFile);
			stringVariable1.setStringInto(syserrrec.params);
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void a100DateConversion()
	{
		/*A101-CONVERSION*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A109-EXIT*/
	}

protected void a200PackString()
	{
		/*A201-PACK*/
		wsaaString.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(zbnwpolrec.bnwpolTrcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolSource, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolCompany, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolChdrnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPlanid, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPlandesc, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolAgntnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolGrpcde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolGrpind, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPlnefdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolUwdcdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolBclntnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolCownnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolStatdesc, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolOccdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolTrmdate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPeriod, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolApplndte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolIssage, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPayoptn, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPeriodc, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolOcmscde, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolComoptn, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolInstprem, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolAnnprem, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolState, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolBillfreq, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolParticipation, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolPension, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolQualification, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolTarget, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolReptype, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolInsnum, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolInsname, "  ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolInssrdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolResident, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolJinsname, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolJinssrdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolJinstmdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolIssstate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolProdcode, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolCwa, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolRollamt, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolIssdte, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolTrandate, " ");
		stringVariable1.addExpression(";");
		stringVariable1.addExpression(zbnwpolrec.bnwpolProcdate, " ");
		stringVariable1.setStringInto(wsaaString);
		/*A209-EXIT*/
	}

protected void a300GetDescription()
	{
		a310Init();
	}

protected void a310Init()
	{
		descIO.setParams(SPACES);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void a400LeftShift()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a410Init();
					a420Loop();
				case a430Loop:
					a430Loop();
				case a440Set:
					a440Set();
				case a490Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410Init()
	{
		wsaaConvAmount.set(SPACES);
		wsaaNegativeSign.set("N");
		wsaaIx.set(0);
		compute(wsaaIy, 0).set(length(wsaaIntmDecimal) + 1);
		if (isEQ(wsaaInptAmount,ZERO)) {
			goTo(GotoLabel.a490Exit);
		}
		else {
			if (isGT(wsaaInptAmount,ZERO)) {
				wsaaIntmAmount.set(wsaaInptAmount);
			}
			else {
				compute(wsaaIntmAmount, 2).set(sub(0,wsaaInptAmount));
				negativeSign.setTrue();
			}
		}
	}

protected void a420Loop()
	{
		/* Adjust the integer part*/
		wsaaIx.add(1);
		if (isGT(wsaaIx,length(wsaaIntmInteger))) {
			goTo(GotoLabel.a490Exit);
		}
		if (isNE(subString(wsaaIntmInteger, wsaaIx, 1),"0")) {
			if (isNE(wsaaIntmDecimal,ZERO)) {
				goTo(GotoLabel.a430Loop);
			}
			else {
				wsaaIy.set(0);
				goTo(GotoLabel.a440Set);
			}
		}
		a420Loop();
		return ;
	}

protected void a430Loop()
	{
		/* Adjust the decimal part*/
		wsaaIy.subtract(1);
		if (isLT(wsaaIx,1)
		|| isNE(subString(wsaaIntmDecimal, wsaaIy, 1),"0")) {
			goTo(GotoLabel.a440Set);
		}
		a430Loop();
		return ;
	}

protected void a440Set()
	{
		compute(wsaaLength, 0).set(sub(length(wsaaIntmInteger) + 1,wsaaIx));
		if (negativeSign.isTrue()) {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("-");
				stringVariable1.addExpression(wsaaIntmInteger);
				stringVariable1.addExpression(".");
				stringVariable1.addExpression(wsaaIntmDecimal);
				stringVariable1.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable2 = new StringUtil();
				stringVariable2.addExpression("-");
				stringVariable2.addExpression(wsaaIntmInteger);
				stringVariable2.setStringInto(wsaaConvAmount);
			}
		}
		else {
			if (isGT(wsaaIy,ZERO)) {
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(wsaaIntmInteger);
				stringVariable3.addExpression(".");
				stringVariable3.addExpression(wsaaIntmDecimal);
				stringVariable3.setStringInto(wsaaConvAmount);
			}
			else {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression(wsaaIntmInteger);
				stringVariable4.setStringInto(wsaaConvAmount);
			}
		}
	}

protected void a500GetBillfreq()
	{
		a500GetBillfreqPara();
		a519Call();
	}

protected void a500GetBillfreqPara()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
	}

protected void a519Call()
	{
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/*A590-EXIT*/
	}

protected void a600GetHpadInfo()
	{
		/*A600-GET-ISSUE*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(bprdIO.getCompany());
		hpadIO.setChdrnum(sqlChdrnum);
		hpadIO.setFunction(varcom.readr);
		hpadIO.setFormat(formatsInner.hpadrec);
		/*A600-CALL*/
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		/*A600-EXIT*/
	}

protected void a700GetTtrc()
	{
		a700GetTtrcPara();
		a700Call();
	}

protected void a700GetTtrcPara()
	{
		ttrcIO.setParams(SPACES);
		ttrcIO.setChdrcoy(bprdIO.getCompany());
		ttrcIO.setChdrnum(sqlChdrnum);
		ttrcIO.setEffdate(hpadIO.getHoissdte());
		ttrcIO.setFunction(varcom.readr);
		ttrcIO.setFormat(formatsInner.ttrcrec);
	}

protected void a700Call()
	{
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(),varcom.oK)
		&& isNE(ttrcIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isEQ(ttrcIO.getStatuz(),varcom.mrnf)) {
			ttrcIO.setTtmprcno(SPACES);
			ttrcIO.setTtmprcdte(99999999);
			/****     MOVE '99/99/9999'        TO TTRC-TTMPRCDTE                */
		}
		/*A700-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData ttrcrec = new FixedLengthStringData(10).init("TTRCREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
}
}
