/*
 * File: Cmca1rv.java
 * Date: 29 August 2009 22:41:01
 * Author: Quipoz Limited
 * 
 * Class transformed from CMCA1RV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.CovrcmcTableDAM;
import com.csc.life.agents.dataaccess.LifecmcTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5565rec;
import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*      REVERSAL OF AGE/TERM BANDED INITIAL COMMISSION
*
* Obtain the commission  calculation  rules  (T5565),  keyed by
* Coverage/rider  table code  and  effective  date  (hence  use
* ITDM).  T5565 is a  two  dimensional  table  based on age and
* term.
*    To work out the term band, read COVRCMC record:-
*         Obtain the Premium Termination Date.
*  Calculate the  Term  by  submitting  the  effective date
*       (commission/risk)  and the premium termination date
*       to DATCON3  with  a  frequency  of 1 (yearly).  The
*       returned result will be the number of years for the
*       term.  If  necessary  round  this  figure up to the
*       next highest whole year.
*  Calculate the  Initial  Commission   by   applying   the
*  enhancements.
*  Firstly, access the enhancement rules table T5692, Keyed
*       on coverage/rider code  and  agent  class (READR on
*       AGLFCMC).  Based on the  age  of the life (READR on
*       LIFECMC with the key passed) access the appropriate
*       section in  the  commission  enhancement  table and
*       obtain the enhancement % of premium for the Initial
*       Commission.  Apply this  enhancement  to the % rate
*       from T5565, also accessed by age band.
*  Secondly, apply the  above  adjusted  rate to the annual
*       premium passed in  the linkage area. This gives the
*       commission amount.
*  Thirdly, apply  the  commission  enhancement  percentage
*       for initial commission from T5692 to the commission
*       amount calculated  above.  This  gives the enhanced
*       initial commission.
* The initial commission should  now be returned in the linkage
*    area.
*****************************************************************
* </pre>
*/
public class Cmca1rv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "CMCA1RV";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5692Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentClass = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 0).init(SPACES);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5692Key, 4).init(SPACES);
	private PackedDecimalData wsaaT5565 = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaInprempc = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaIncmrate = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaY = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommAmt = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaT5565Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5565Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5565Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5565Srcebus = new FixedLengthStringData(2).isAPartOf(wsaaT5565Key, 4).init(SPACES);
	private static final String h026 = "H026";
	private static final String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private static final String t5692 = "T5692";
	private static final String t5565 = "T5565";
	private static final String t1693 = "T1693";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrcmcTableDAM covrcmcIO = new CovrcmcTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifecmcTableDAM lifecmcIO = new LifecmcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private T5692rec t5692rec = new T5692rec();
	private T5565rec t5565rec = new T5565rec();
	private T1693rec t1693rec = new T1693rec();
	private Comlinkrec comlinkrec = new Comlinkrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1050, 
		continue4520, 
		continue4540, 
		exit4590
	}

	public Cmca1rv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		comlinkrec.clnkallRec = convertAndSetParam(comlinkrec.clnkallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		comlinkrec.statuz.set(varcom.oK);
		comlinkrec.icommtot.set(0);
		readChdrlnb500();
		obtainCalcRules1000();
		calcTermBand2000();
		calcInitCommission3000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readChdrlnb500()
	{
		/*START*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(comlinkrec.chdrcoy);
		chdrlnbIO.setChdrnum(comlinkrec.chdrnum);
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError8100();
		}
		/*EXIT*/
	}

protected void obtainCalcRules1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1000();
				case next1050: 
					next1050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1000()
	{
		/*  READ COMMISSION CALCULATION RULES*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5565);
		/* Change T5565 item key, add soure of business as part of the key.*/
		/* MOVE CLNK-CRTABLE           TO ITDM-ITEMITEM.                */
		wsaaT5565Crtable.set(comlinkrec.crtable);
		wsaaT5565Srcebus.set(chdrlnbIO.getSrcebus());
		itdmIO.setItemitem(wsaaT5565Key);
		/*MOVE CLNK-OCCDATE           TO ITDM-ITMFRM.*/
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
	}

protected void next1050()
	{
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5565)
		|| isNE(itdmIO.getItemitem(),wsaaT5565Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			if (isNE(itdmIO.getItemitem(),wsaaT5565Key)
			&& isNE(wsaaT5565Srcebus,"**")) {
				itdmIO.setDataKey(SPACES);
				itdmIO.setItemcoy(comlinkrec.chdrcoy);
				itdmIO.setItemtabl(t5565);
				wsaaT5565Crtable.set(comlinkrec.crtable);
				wsaaT5565Srcebus.set("**");
				itdmIO.setItemitem(wsaaT5565Key);
				itdmIO.setItmfrm(comlinkrec.effdate);
				itdmIO.setFunction(varcom.begn);
				goTo(GotoLabel.next1050);
			}
			syserrrec.statuz.set(h026);
			syserrrec.params.set(comlinkrec.crtable);
			dbError8100();
		}
		else {
			t5565rec.t5565Rec.set(itdmIO.getGenarea());
		}
	}

protected void calcTermBand2000()
	{
		obtainPremDate2020();
		calculateTerm2040();
	}

protected void obtainPremDate2020()
	{
		covrcmcIO.setChdrcoy(comlinkrec.chdrcoy);
		covrcmcIO.setChdrnum(comlinkrec.chdrnum);
		covrcmcIO.setLife(comlinkrec.life);
		covrcmcIO.setCoverage(comlinkrec.coverage);
		covrcmcIO.setRider(comlinkrec.rider);
		covrcmcIO.setPlanSuffix(comlinkrec.planSuffix);
		covrcmcIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrcmcIO);
		if (isNE(covrcmcIO.getStatuz(),varcom.oK)) {
			covrcmcIO.setParams(covrcmcIO.getParams());
			dbError8100();
		}
	}

protected void calculateTerm2040()
	{
		datcon3rec.intDate1.set(covrcmcIO.getPremCessDate());
		/*MOVE CLNK-OCCDATE           TO DTC3-INT-DATE-2.*/
		datcon3rec.intDate2.set(comlinkrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			systemError8000();
		}
		compute(wsaaTerm, 5).set(add(datcon3rec.freqFactor,0.99999));
		/*EXIT*/
	}

protected void calcInitCommission3000()
	{
		para3000();
	}

protected void para3000()
	{
		/*  READ ENHANCEMENT RULES*/
		/*  If item not found on first read '****' is moved to agent class */
		/*   so generic read is then performed.                            */
		itdmIO.setDataKey(SPACES);
		wsaaCrtable.set(comlinkrec.crtable);
		wsaaAgentClass.set(comlinkrec.agentClass);
		itdmIO.setItemcoy(comlinkrec.chdrcoy);
		itdmIO.setItemtabl(t5692);
		itdmIO.setItemitem(wsaaT5692Key);
		/*MOVE CLNK-OCCDATE           TO ITDM-ITMFRM.*/
		itdmIO.setItmfrm(comlinkrec.effdate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
		|| isNE(itdmIO.getItemtabl(),t5692)
		|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemtabl(t5692);
			wsaaCrtable.set("****");
			itdmIO.setItemitem(wsaaT5692Key);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				syserrrec.params.set(itdmIO.getParams());
				dbError8100();
			}
			if (isNE(itdmIO.getItemcoy(),comlinkrec.chdrcoy)
			|| isNE(itdmIO.getItemtabl(),t5692)
			|| isNE(itdmIO.getItemitem(),wsaaT5692Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				t5692rec.age01.set(ZERO);
				t5692rec.age02.set(ZERO);
				t5692rec.age03.set(ZERO);
				t5692rec.age04.set(ZERO);
			}
			else {
				t5692rec.t5692Rec.set(itdmIO.getGenarea());
			}
		}
		else {
			t5692rec.t5692Rec.set(itdmIO.getGenarea());
		}
		lifecmcIO.setFunction(varcom.readr);
		lifecmcIO.setChdrcoy(comlinkrec.chdrcoy);
		lifecmcIO.setChdrnum(comlinkrec.chdrnum);
		lifecmcIO.setLife(comlinkrec.life);
		if (isEQ(comlinkrec.jlife,SPACES)) {
			lifecmcIO.setJlife(ZERO);
		}
		else {
			lifecmcIO.setJlife(comlinkrec.jlife);
		}
		SmartFileCode.execute(appVars, lifecmcIO);
		if (isNE(lifecmcIO.getStatuz(),varcom.oK)) {
			dbError8100();
		}
		/* If Commencement Date is not contracts original ccdate then*/
		/* calculate current ANB using passed ccdate.*/
		/*IF CLNK-OCCDATE NOT = COVRCMC-CRRCD*/
		/*    IF CLNK-EFFDATE NOT = COVRCMC-CRRCD                          */
		/*       MOVE LIFECMC-CLTDOB     TO   DTC3-INT-DATE-1              */
		/*MOVE CLNK-OCCDATE       TO   DTC3-INT-DATE-2*/
		/*       MOVE CLNK-EFFDATE       TO   DTC3-INT-DATE-2              */
		/*       MOVE '01'               TO   DTC3-FREQUENCY               */
		/*       CALL 'DATCON3'       USING   DTC3-DATCON3-REC             */
		/*       IF DTC3-STATUZ              NOT = O-K                     */
		/*          PERFORM 8000-SYSTEM-ERROR                              */
		/*       ELSE                                                      */
		/*           COMPUTE WSAA-ANB =                                    */
		/*              DTC3-FREQ-FACTOR + 0.99999                         */
		/*          MOVE DTC3-FREQ-FACTOR        TO WSAA-ANB*/
		/*          ADD DTC3-FREQ-FACTOR, 0.999  GIVING WSAA-ANB*/
		/*          MOVE WSAA-ANB                TO LIFECMC-ANB-AT-CCD.    */
		if (isNE(comlinkrec.effdate,covrcmcIO.getCrrcd())) {
			calcAge6000();
		}
		getT5692Value4000();
		getT5565Value4500();
		compute(wsaaT5565, 3).setRounded(div(mult(wsaaT5565,wsaaInprempc),100));
		/*  CALCULATE THE COMMISSION AMOUNT*/
		compute(wsaaCommAmt, 3).setRounded(div(mult(comlinkrec.annprem,wsaaT5565),100));
		/*  CALCULATE THE ENHANCED INITIAL COMMISSION*/
		compute(wsaaCommAmt, 3).setRounded(div(mult(wsaaCommAmt,wsaaIncmrate),100));
		comlinkrec.icommtot.set(wsaaCommAmt);
	}

protected void getT5692Value4000()
	{
			para4000();
		}

protected void para4000()
	{
		wsaaInprempc.set(100);
		wsaaIncmrate.set(100);
		/*  ACCESS TABLE VALUE BASED ON ANB-AT-CCD*/
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age01)) {
			wsaaIncmrate.set(t5692rec.incmrate01);
			wsaaInprempc.set(t5692rec.inprempc01);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age02)) {
			wsaaIncmrate.set(t5692rec.incmrate02);
			wsaaInprempc.set(t5692rec.inprempc02);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age03)) {
			wsaaIncmrate.set(t5692rec.incmrate03);
			wsaaInprempc.set(t5692rec.inprempc03);
			return ;
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5692rec.age04)) {
			wsaaIncmrate.set(t5692rec.incmrate04);
			wsaaInprempc.set(t5692rec.inprempc04);
			return ;
		}
	}

protected void getT5565Value4500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4500();
				case continue4520: 
					continue4520();
				case continue4540: 
					continue4540();
				case exit4590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4500()
	{
		wsaaY.set(0);
		wsaaX.set(0);
		/*  WORK OUT THE ARRAY'S Y INDEX BASED ON ANB-AT-CCD*/
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd01)) {
			wsaaY.set(1);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd02)) {
			wsaaY.set(2);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd03)) {
			wsaaY.set(3);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd04)) {
			wsaaY.set(4);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd05)) {
			wsaaY.set(5);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd06)) {
			wsaaY.set(6);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd07)) {
			wsaaY.set(7);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd08)) {
			wsaaY.set(8);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd09)) {
			wsaaY.set(9);
			goTo(GotoLabel.continue4520);
		}
		if (isLTE(lifecmcIO.getAnbAtCcd(),t5565rec.anbAtCcd10)) {
			wsaaY.set(10);
			goTo(GotoLabel.continue4520);
		}
		goTo(GotoLabel.exit4590);
	}

protected void continue4520()
	{
		/*  WORK OUT THE ARRAY'S X INDEX BASED ON TERM*/
		if (isLTE(wsaaTerm,t5565rec.termMax01)) {
			wsaaX.set(1);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax02)) {
			wsaaX.set(2);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax03)) {
			wsaaX.set(3);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax04)) {
			wsaaX.set(4);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax05)) {
			wsaaX.set(5);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax06)) {
			wsaaX.set(6);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax07)) {
			wsaaX.set(7);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax08)) {
			wsaaX.set(8);
			goTo(GotoLabel.continue4540);
		}
		if (isLTE(wsaaTerm,t5565rec.termMax09)) {
			wsaaX.set(9);
			goTo(GotoLabel.continue4540);
		}
		goTo(GotoLabel.exit4590);
	}

protected void continue4540()
	{
		/*  WORK OUT THE APPROPRIATE VALUE FROM THE X,Y ARRAY COORDINATES*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY,9));
		wsaaIndex.add(wsaaX);
		wsaaT5565.set(t5565rec.prcnt[wsaaIndex.toInt()]);
	}

protected void calcAge6000()
	{
		init6010();
	}

protected void init6010()
	{
		/* New routine to calculate Age next/nearest/last birthday         */
		/* CHDRLNB is already read in 500 section.                         */
		/*    MOVE CHDRLNBREC             TO CHDRLNB-FORMAT.       <V71L14>*/
		/*    MOVE CLNK-CHDRCOY           TO CHDRLNB-CHDRCOY.      <V71L14>*/
		/*    MOVE CLNK-CHDRNUM           TO CHDRLNB-CHDRNUM.      <V71L14>*/
		/*    MOVE READR                  TO CHDRLNB-FUNCTION.     <V71L14>*/
		/*    CALL 'CHDRLNBIO' USING CHDRLNB-PARAMS.               <V71L14>*/
		/*    IF CHDRLNB-STATUZ           NOT = O-K                <V71L14>*/
		/*        MOVE CHDRLNB-PARAMS     TO SYSR-PARAMS           <V71L14>*/
		/*        MOVE CHDRLNB-STATUZ     TO SYSR-STATUZ           <V71L14>*/
		/*  Identify FSU Company                                           */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl(t1693);
		itemIO.setItemitem(comlinkrec.chdrcoy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifecmcIO.getCltdob());
		agecalcrec.intDate2.set(comlinkrec.effdate);
		agecalcrec.language.set(comlinkrec.language);
		agecalcrec.company.set(t1693rec.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			systemError8000();
		}
		lifecmcIO.setAnbAtCcd(agecalcrec.agerating);
	}

protected void systemError8000()
	{
			se8000();
			seExit8090();
		}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
			db8100();
			dbExit8190();
		}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		comlinkrec.statuz.set(varcom.bomb);
		exit090();
	}
}
