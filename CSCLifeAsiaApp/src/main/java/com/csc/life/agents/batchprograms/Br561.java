/*
 * File: Br561.java
 * Date: 29 August 2009 22:20:56
 * Author: Quipoz Limited
 *
 * Class transformed from BR561.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.AcmvldgTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.cls.Cr561cpyf;
import com.csc.life.agents.dataaccess.AcmvagtTableDAM;
import com.csc.life.agents.dataaccess.AgorTableDAM;
import com.csc.life.agents.dataaccess.AgorpslTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpypfTableDAM;
import com.csc.life.agents.dataaccess.ChdragtTableDAM;
import com.csc.life.agents.tablestructures.Th622rec;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.PtrnwfdTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.statistics.dataaccess.AgcmstaTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS
*
*           NEW OVERRIDE STRUCTURE BATCH PROGRAM
*
*  This batch program will create Override and his own personal
*  sale for each agent found in AGPY file and write it to ACMV
*  file.
*
*  System Paramenters used to avoid hard coding transaction codes
*
*   - BPRD-SYSTEM-PARAM01 = 'TA69' (Transaction Code)
*   - BPRD-SYSTEM-PARAM02 = 'T679' (TRansaction Code)
*
*   - BPRD-SYSTEM-PARAM03 = 3      (Agent's Level)
*
* . Begin AGPYAGT
*    Process only those equal to 'LA'  'IC'/'RC'
*    Perform until EOF
*
*     Open 'BATCDOR'
*
*     Move Agpyagt.rldgacct to Basic.Agent
*                              Working.Agent
*     Move Spaces           to ReportTo.Agent
*
*     Read COVRMJA using Tranref
*
*     Read CHDR    using agpy-rdocnum
*
*     Begin AGORPSL using Basic.agent (calculate Personal Sale)
*     Perform until EOF
*      if Agorspl-agent    = Basic.agent and
*         Agorspl-reportag =  Basic.agent
*         If covr.crrcd < agor.eff.from and
*                       > agor.eff.to
*            Move Agor-agtype01 to Basic.type
*            Perform OR-Comm-calculation
*            Call 'LIFACMV'
*         End-if
*      End-if
*     End-Perform
*
*     Begin AGOR using Working.Agent (calculate Override Comm)
*     Perform until EOF
*      if Working.agent = Agor-agent
*        If covr.crrcd < agor.eff.from and
*                      > agor.eff.to
*         If Agor.Reportag    = Agor.agent
*            If Basic.Agent = Agor.Reportag and
*               Move Agor-agtype01 to Basic.type
*               Perform OR-Comm-calculation
*               Call 'LIFACMV'
*            End-if
*            Read Next AGOR file
*         else
*            Move Agor.reportag to ReportTo.Agent
*            Perform OR-Comm-calculation
*            Call 'LIFACMV'
*            Read Next AGOR file
*         end-if
*        end-if
*      else
*         Move ReportTo.Agent to Working.agent
*         Go to Begin AGOR loop again
*      End-if
*     End-Perform
*     Read Next AGPYAGT
*    End-Perform
*
* OR-Comm-calculation
* ---------------------
*
*   . Read Table TR520 using  covr.crtable    +
*                             Basic.type      +
*                             Agor.Agtyp02
*
*   . Check Number of Years (chdr-occd)
*
*   . If Basic.Type = Agor.Agtype02
*        If not Top-UP
*           use Direct %
*           Save to Save.Percent
*           Save to Save.Prem.Comm
*           Perform calculate.or
*        else
*           use Direct Top-up %
*           Save to Save.Percent
*           Save to Save.Prem.Comm
*           Perform calculate.or
*        end-if
*     else
*        If not Top-UP
*           use Indirec %
*           Save to Save.Percent
*           Save to Save.Prem.Comm
*           Perform calculate.or
*        else
*           use Indirect Top-up %
*           Save to Save.Percent
*           Save to Save.Prem.Comm
*           Perform calculate.or
*        end-if
*     end-if
*
* Calculate OR
* -------------
*
* If not Top-Up
*    If Commission
*       OR.comm = agpy.origamt  * Save.percent
*    else
*       OR.comm = covr-instprem * Save.percent * chdr-billfreq
*    end-if
* else
*    Read ACMV using  sacscode = 'LE' and
*                     sacstyp  = 'SP' and
*                     tranno   = agpyagt-tranno
*    OR.comm = acmv-origamt * Save.percent
* end-if
*****************************************************************
* </pre>
*/
public class Br561 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AgpypfTableDAM agpypf = new AgpypfTableDAM();
	private AgpypfTableDAM agpypfRec = new AgpypfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR561");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaRecordOmit = "";
	private String wsaaTopup = "";
	private FixedLengthStringData wsaaBasicAgnt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaWorkingAgnt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaReporttoAgnt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaReporttoSave = new FixedLengthStringData(8);
	private String wsaaEndAgent = "";
	private FixedLengthStringData wsaaBasicType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaReptoType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaWorkingType = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaPercent = new ZonedDecimalData(7, 2).setUnsigned();
	private ZonedDecimalData wsaaCommAmt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCoverDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAgtchgDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIssueDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaCovrKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCovrKey, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 10);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaCovrKey, 12);
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(2, 0).isAPartOf(wsaaCovrKey, 14).setUnsigned();
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaCr561cpyfParms = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaT564507 = new FixedLengthStringData(2).isAPartOf(wsaaCr561cpyfParms, 0);
	private FixedLengthStringData wsaaCr561Company = new FixedLengthStringData(1).isAPartOf(wsaaCr561cpyfParms, 2);
	private ZonedDecimalData wsaaCr561Effdate = new ZonedDecimalData(8, 0).isAPartOf(wsaaCr561cpyfParms, 3).setUnsigned();

	private FixedLengthStringData wsbbKeyDetails = new FixedLengthStringData(18);
	private FixedLengthStringData wsbbChdrcoy = new FixedLengthStringData(1).isAPartOf(wsbbKeyDetails, 0);
	private FixedLengthStringData wsbbChdrnum = new FixedLengthStringData(8).isAPartOf(wsbbKeyDetails, 1);
	private FixedLengthStringData wsbbLife = new FixedLengthStringData(2).isAPartOf(wsbbKeyDetails, 9);
	private FixedLengthStringData wsbbCoverage = new FixedLengthStringData(2).isAPartOf(wsbbKeyDetails, 11);
	private FixedLengthStringData wsbbRider = new FixedLengthStringData(2).isAPartOf(wsbbKeyDetails, 13);
	private PackedDecimalData wsbbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(wsbbKeyDetails, 15);

	private FixedLengthStringData wsaaTh622Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh622Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh622Key, 0);
	private FixedLengthStringData wsaaTh622BasTyp = new FixedLengthStringData(2).isAPartOf(wsaaTh622Key, 4);
	private FixedLengthStringData wsaaTh622ReptoTyp = new FixedLengthStringData(2).isAPartOf(wsaaTh622Key, 6);
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaBprdCharcount = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaBprdNumrcount = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCrrcd = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTempYear = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String acmvldgrec = "ACMVLDGREC";
	private String covrmjarec = "COVRMJAREC";
	private String covtmjarec = "COVTMJAREC";
	private String chdrenqrec = "CHDRENQREC";
	private String ptrnwfdrec = "PTRNWFDREC";
	private String agcmstarec = "AGCMSTAREC";
	private String chdragtrec = "CHDRAGTREC";
	private String hpadrec = "HPADREC";
		/* TABLES */
	private String th622 = "TH622";
	private String t5645 = "T5645";
	private String t5688 = "T5688";
		/* ERRORS */
	private String e034 = "E034";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*ACCOUNT MOVEMENT FOR AGENTS TRANSACTIONS*/
	private AcmvagtTableDAM acmvagtIO = new AcmvagtTableDAM();
		/*ACMV read with Entity s part of the key*/
	private AcmvldgTableDAM acmvldgIO = new AcmvldgTableDAM();
		/*ACCOUNT MOVEMENTS LOGICAL VIEW FOR REVER*/
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
		/*Agent commission logical*/
	private AgcmstaTableDAM agcmstaIO = new AgcmstaTableDAM();
		/*Agent override details*/
	private AgorTableDAM agorIO = new AgorTableDAM();
		/*Agent OR details - override payment*/
	private AgorpslTableDAM agorpslIO = new AgorpslTableDAM();
		/*Account movements copy file for agent pa*/
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
		/*Contract Header View by Agent*/
	private ChdragtTableDAM chdragtIO = new ChdragtTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Coverage/Rider details - Major Alts*/
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
		/*PTRN Logical File for windforward*/
	private PtrnwfdTableDAM ptrnwfdIO = new PtrnwfdTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Th622rec th622rec = new Th622rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		next2550,
		exit2680,
		next2700,
		exit2700,
		exit2720,
		acmvLoop3105,
		exit3190,
		a2621Call,
		a2629Exit
	}

	public Br561() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("BR561");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT564507.set(t5645rec.sacscode07);
		wsaaCr561Company.set(bsprIO.getCompany());
		wsaaCr561Effdate.set(bsscIO.getEffectiveDate());
		callProgram(Cr561cpyf.class, wsaaCr561cpyfParms);
		agpypf.openInput();
		agpyagtIO.setRldgcoy(bsprIO.getCompany());
		agpyagtIO.setRldgacct(SPACES);
		agpyagtIO.setOrigcurr(SPACES);
		agpyagtIO.setFunction(varcom.begn);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(),varcom.oK)
		&& isNE(agpyagtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agpyagtIO.getParams());
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agpyagtIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			wsspEdterror.set(varcom.oK);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					edit2510();
				}
				case next2550: {
					next2550();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		if (isEQ(agpyagtIO.getSuprflg(),"Y")) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.next2550);
		}
		a0500CheckAfi();
		if (isEQ(wsaaRecordOmit,"Y")) {
			wsspEdterror.set(varcom.oK);
			goTo(GotoLabel.next2550);
		}
		if (isLT(agpyagtIO.getAcctamt(),0)) {
			wsspEdterror.set(varcom.oK);
			goTo(GotoLabel.next2550);
		}
		wsspEdterror.set(varcom.oK);
		wsaaCrrcd.set(ZERO);
		wsaaInstprem.set(ZERO);
		wsaaInstprem.set(ZERO);
		wsaaCrtable.set(SPACES);
		setWorkingStorage2580();
		openBatcdor2600();
		readCovr2620();
		if (isEQ(covrmjaIO.getStatuz(),varcom.mrnf)) {
			readCovt2630();
			if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
				goTo(GotoLabel.next2550);
			}
		}
		else {
			if (isNE(covrmjaIO.getStatuz(),varcom.oK)) {
				goTo(GotoLabel.next2550);
			}
		}
		readChdr2640();
		readT56882650();
		checkYear2660();
		agorpslIO.setParams(SPACES);
		agorpslIO.setStatuz(SPACES);
		agorpslIO.setAgntcoy(bsprIO.getCompany());
		agorpslIO.setAgntnum(wsaaBasicAgnt);
		agorpslIO.setReportag(wsaaBasicAgnt);
		agorpslIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		/*agorpslIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agorpslIO.setFitKeysSearch("AGNTNUM", "REPORTAG");*/

		while ( !(isEQ(agorpslIO.getStatuz(),varcom.endp))) {
			personalSale2680();
		}

		wsaaPercent.set(ZERO);
		wsaaCommAmt.set(ZERO);
		wsaaBasicType.set(SPACES);
		agorIO.setParams(SPACES);
		agorIO.setAgntcoy(bsprIO.getCompany());
		agorIO.setAgntnum(wsaaBasicAgnt);
		agorIO.setFunction(varcom.begn);
		while ( !(isEQ(wsaaEndAgent,"Y"))) {
			beginAgor2700();
		}

	}

protected void next2550()
	{
		agpyagtIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void setWorkingStorage2580()
	{
		edit2580();
	}

protected void edit2580()
	{
		wsaaWorkingAgnt.set(SPACES);
		wsaaEndAgent = "N";
		wsaaCommAmt.set(ZERO);
		wsaaPercent.set(ZERO);
		wsaaYear.set(ZERO);
		wsaaJrnseq.set(ZERO);
		wsaaCounter.set(1);
		wsaaCovrKey.set(agpyagtIO.getTranref());
		wsaaBasicAgnt.set(agpyagtIO.getRldgacct());
		wsaaWorkingAgnt.set(agpyagtIO.getRldgacct());
		if (isEQ(agpyagtIO.getSacstyp(),bprdIO.getSystemParam01())) {
			wsaaTopup = "Y";
		}
		else {
			wsaaTopup = "N";
		}
	}

protected void openBatcdor2600()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void readCovr2620()
	{
		edit2620();
	}

protected void edit2620()
	{
		covrmjaIO.setParams(SPACES);
		covrmjaIO.setChdrcoy(bsprIO.getCompany());
		covrmjaIO.setChdrnum(wsaaChdrnum);
		covrmjaIO.setLife(wsaaLife);
		covrmjaIO.setCoverage(wsaaCoverage);
		covrmjaIO.setRider(wsaaRider);
		if (isNE(wsaaPlnsfx,NUMERIC)) {
			wsaaPlnsfx.set(ZERO);
		}
		wsaaPlan.set(wsaaPlnsfx);
		covrmjaIO.setPlanSuffix(wsaaPlan);
		covrmjaIO.setFunction(varcom.readr);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isEQ(covrmjaIO.getStatuz(),varcom.oK)) {
			wsbbKeyDetails.set(SPACES);
			wsbbChdrcoy.set(covrmjaIO.getChdrcoy());
			wsbbChdrnum.set(covrmjaIO.getChdrnum());
			wsbbLife.set(covrmjaIO.getLife());
			wsbbCoverage.set(covrmjaIO.getCoverage());
			wsbbRider.set(covrmjaIO.getRider());
			wsbbPlanSuffix.set(covrmjaIO.getPlanSuffix());
			a2619GetIssueDate();
			a2620CheckAgentChange();
			wsaaCoverDate.set(covrmjaIO.getCrrcd());
			a2630GetCompareDate();
			wsaaCrtable.set(covrmjaIO.getCrtable());
			wsaaInstprem.set(covrmjaIO.getInstprem());
		}
	}

protected void readCovt2630()
	{
		edit2630();
	}

protected void edit2630()
	{
		covtmjaIO.setParams(SPACES);
		covtmjaIO.setChdrcoy(bsprIO.getCompany());
		covtmjaIO.setChdrnum(wsaaChdrnum);
		covtmjaIO.setLife(wsaaLife);
		covtmjaIO.setCoverage(wsaaCoverage);
		covtmjaIO.setRider(wsaaRider);
		if (isNE(wsaaPlnsfx,NUMERIC)) {
			wsaaPlnsfx.set(ZERO);
		}
		wsaaPlan.set(wsaaPlnsfx);
		covtmjaIO.setPlanSuffix(wsaaPlan);
		covtmjaIO.setFunction(varcom.readr);
		covtmjaIO.setFormat(covtmjarec);
		SmartFileCode.execute(appVars, covtmjaIO);
		if (isEQ(covtmjaIO.getStatuz(),varcom.oK)) {
			wsbbKeyDetails.set(SPACES);
			wsbbChdrcoy.set(covtmjaIO.getChdrcoy());
			wsbbChdrnum.set(covtmjaIO.getChdrnum());
			wsbbLife.set(covtmjaIO.getLife());
			wsbbCoverage.set(covtmjaIO.getCoverage());
			wsbbRider.set(covtmjaIO.getRider());
			wsbbPlanSuffix.set(covtmjaIO.getPlanSuffix());
			a2619GetIssueDate();
			a2620CheckAgentChange();
			wsaaCoverDate.set(covtmjaIO.getEffdate());
			a2630GetCompareDate();
			wsaaCrtable.set(covtmjaIO.getCrtable());
			wsaaInstprem.set(covtmjaIO.getInstprem());
		}
	}

protected void readChdr2640()
	{
		edit2640();
	}

protected void edit2640()
	{
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(bsprIO.getCompany());
		chdrenqIO.setChdrnum(wsaaChdrnum);
		chdrenqIO.setFormat(chdrenqrec);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getBillfreq(),"00")){
			wsaaFreq.set(1);
		}
		else if (isEQ(chdrenqIO.getBillfreq(),"01")){
			wsaaFreq.set(1);
		}
		else if (isEQ(chdrenqIO.getBillfreq(),"02")){
			wsaaFreq.set(2);
		}
		else if (isEQ(chdrenqIO.getBillfreq(),"04")){
			wsaaFreq.set(4);
		}
		else if (isEQ(chdrenqIO.getBillfreq(),"12")){
			wsaaFreq.set(12);
		}
	}

protected void readT56882650()
	{
		edit2650();
	}

protected void edit2650()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrenqIO.getCnttype());
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrenqIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkYear2660()
	{
		begin2660();
	}

protected void begin2660()
	{
		datcon3rec.intDate1.set(wsaaCrrcd);
		datcon3rec.intDate2.set(agpyagtIO.getEffdate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isLT(datcon3rec.freqFactor,1)){
			wsaaYear.set(1);
		}
		else if (isLT(datcon3rec.freqFactor,2)){
			wsaaYear.set(2);
		}
		else if (isLT(datcon3rec.freqFactor,3)){
			wsaaYear.set(3);
		}
		else if (isLT(datcon3rec.freqFactor,4)){
			wsaaYear.set(4);
		}
		else if (isLT(datcon3rec.freqFactor,5)){
			wsaaYear.set(5);
		}
		else if (isLT(datcon3rec.freqFactor,6)){
			wsaaYear.set(6);
		}
		else if (isLT(datcon3rec.freqFactor,7)){
			wsaaYear.set(7);
		}
		else if (isLT(datcon3rec.freqFactor,8)){
			wsaaYear.set(8);
		}
		else if (isLT(datcon3rec.freqFactor,9)){
			wsaaYear.set(9);
		}
		else if (isGTE(datcon3rec.freqFactor,9)){
			wsaaYear.set(10);
		}
	}

protected void personalSale2680()
	{
		try {
			edit2680();
			next2680();
		}
		catch (GOTOException e){
		}
	}

protected void edit2680()
	{
		SmartFileCode.execute(appVars, agorpslIO);
		if (isNE(agorpslIO.getStatuz(),varcom.oK)
		&& isNE(agorpslIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agorpslIO.getParams());
			syserrrec.statuz.set(agorpslIO.getStatuz());
			fatalError600();
		}
		if (isNE(agorpslIO.getStatuz(),varcom.oK)) {
			agorpslIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2680);
		}
		if (isEQ(agorpslIO.getAgntnum(),wsaaBasicAgnt)
		&& isEQ(agorpslIO.getReportag(),wsaaBasicAgnt)) {
			if ((isGTE(wsaaCrrcd,agorpslIO.getEffdate01())
			&& isLTE(wsaaCrrcd,agorpslIO.getEffdate02()))) {
				wsaaReporttoAgnt.set(wsaaBasicAgnt);
				wsaaBasicType.set(agorpslIO.getAgtype01());
				wsaaWorkingType.set(agorpslIO.getAgtype02());
				wsaaReptoType.set(agorpslIO.getAgtype02());
				orCommCalc2720();
				agorpslIO.setStatuz(varcom.endp);
			}
		}
		else {
			agorpslIO.setStatuz(varcom.endp);
		}
	}

protected void next2680()
	{
		agorpslIO.setFunction(varcom.nextr);
	}

protected void beginAgor2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					edit2700();
				}
				case next2700: {
					next2700();
				}
				case exit2700: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2700()
	{
		SmartFileCode.execute(appVars, agorIO);
		if (isNE(agorIO.getStatuz(),varcom.oK)
		&& isNE(agorIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agorIO.getParams());
			syserrrec.statuz.set(agorIO.getStatuz());
			fatalError600();
		}
		if (isNE(agorIO.getStatuz(),varcom.oK)) {
			wsaaEndAgent = "Y";
			goTo(GotoLabel.exit2700);
		}
		if (isEQ(agorIO.getAgntnum(),agorIO.getReportag())) {
			goTo(GotoLabel.next2700);
		}
		if (isEQ(agorIO.getAgntnum(),wsaaWorkingAgnt)) {
			if ((isGTE(wsaaCrrcd,agorIO.getEffdate01())
			&& isLTE(wsaaCrrcd,agorIO.getEffdate02()))) {
				if (isNE(agorIO.getAgntnum(),agorIO.getReportag())) {
					wsaaReporttoAgnt.set(agorIO.getReportag());
					wsaaWorkingType.set(agorIO.getAgtype02());
					if (isEQ(wsaaBasicType,SPACES)) {
						wsaaBasicType.set(agorIO.getAgtype01());
					}
					if (isEQ(wsaaBasicAgnt,agorIO.getAgntnum())) {
						wsaaReptoType.set(agorIO.getAgtype02());
					}
					orCommCalc2720();
					checkYear2660();
					wsaaBprdCharcount.set(bprdIO.getSystemParam03());
					wsaaBprdNumrcount.set(wsaaBprdCharcount);
					if (isGT(wsaaCounter,wsaaBprdNumrcount)
					|| isEQ(wsaaCounter,wsaaBprdNumrcount)) {
						wsaaEndAgent = "Y";
						goTo(GotoLabel.exit2700);
					}
					agorIO.setFunction(varcom.begn);
					agorIO.setAgntnum(wsaaReporttoAgnt);
					wsaaWorkingAgnt.set(wsaaReporttoAgnt);
					goTo(GotoLabel.exit2700);
				}
			}
		}
	}

protected void next2700()
	{
		agorIO.setFunction(varcom.nextr);
	}

protected void orCommCalc2720()
	{
		try {
			begin2720();
		}
		catch (GOTOException e){
		}
	}

protected void begin2720()
	{
		readTh6222740();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit2720);
		}
		if (isEQ(wsaaTopup,"Y")) {
			topupTransaction2760();
		}
		else {
			normalTransaction2780();
		}
		if (isNE(wsaaCommAmt,ZERO)) {
			callLifeacmv2800();
		}
	}

protected void readTh6222740()
	{
		begin2740();
	}

protected void begin2740()
	{
		wsaaTh622Crtable.set(wsaaCrtable);
		wsaaTh622BasTyp.set(wsaaBasicType);
		wsaaTh622ReptoTyp.set(wsaaWorkingType);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(th622);
		itdmIO.setItemitem(wsaaTh622Key);
		itdmIO.setItmfrm(wsaaIssueDate);
		itdmIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),th622)
		|| isNE(itdmIO.getItemitem(),wsaaTh622Key)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
			wsaaTh622Crtable.set("****");
			wsaaTh622BasTyp.set(wsaaBasicType);
			wsaaTh622ReptoTyp.set(wsaaWorkingType);
			itdmIO.setDataKey(SPACES);
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(th622);
			itdmIO.setItemitem(wsaaTh622Key);
			itdmIO.setItmfrm(wsaaCrrcd);
			itdmIO.setFunction(varcom.begn);

			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");

			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(itdmIO.getStatuz());
				fatalError600();
			}
			if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
			|| isNE(itdmIO.getItemtabl(),th622)
			|| isNE(itdmIO.getItemitem(),wsaaTh622Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				itdmIO.setItemitem(wsaaTh622Key);
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(e034);
				fatalError600();
			}
			else {
				th622rec.th622Rec.set(itdmIO.getGenarea());
			}
		}
		else {
			th622rec.th622Rec.set(itdmIO.getGenarea());
		}
	}

protected void topupTransaction2760()
	{
		begin2760();
	}

protected void begin2760()
	{
		acmvldgIO.setRecKeyData(SPACES);
		acmvldgIO.setRldgcoy(agpyagtIO.getRldgcoy());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			acmvldgIO.setSacscode(t5645rec.sacscode04);
			acmvldgIO.setSacstyp(t5645rec.sacstype04);
		}
		else {
			acmvldgIO.setSacscode(t5645rec.sacscode05);
			acmvldgIO.setSacstyp(t5645rec.sacstype05);
		}
		acmvldgIO.setRldgacct(agpyagtIO.getTranref());
		acmvldgIO.setOrigcurr(agpyagtIO.getOrigcurr());
		acmvldgIO.setTranno(agpyagtIO.getTranno());
		acmvldgIO.setFunction(varcom.readr);
		acmvldgIO.setFormat(acmvldgrec);
		SmartFileCode.execute(appVars, acmvldgIO);
		if (isNE(acmvldgIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acmvldgIO.getDataKey());
			fatalError600();
		}
		if (isEQ(wsaaReptoType,wsaaWorkingType)) {
			wsaaPercent.set(th622rec.zryrperc[21]);
			if (isEQ(th622rec.keyopt01,"P")) {
				compute(wsaaCommAmt, 2).set(div(mult(acmvldgIO.getOrigamt(),wsaaPercent),100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(agpyagtIO.getOrigamt(),wsaaPercent),100));
			}
		}
		else {
			compute(wsaaTempYear, 0).set(add(wsaaYear,10));
			wsaaPercent.set(th622rec.zryrperc[wsaaTempYear.toInt()]);
			if (isEQ(th622rec.keyopt02,"P")) {
				compute(wsaaCommAmt, 2).set(div(mult(acmvldgIO.getOrigamt(),wsaaPercent),100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(agpyagtIO.getOrigamt(),wsaaPercent),100));
			}
		}
	}

protected void normalTransaction2780()
	{
		/*BEGIN*/
		if (isEQ(wsaaReptoType,wsaaWorkingType)) {
			wsaaPercent.set(th622rec.zryrperc[wsaaYear.toInt()]);
			if (isEQ(th622rec.keyopt01,"P")) {
				compute(wsaaCommAmt, 2).set(div(mult(mult(wsaaInstprem,wsaaFreq),wsaaPercent),100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(agpyagtIO.getOrigamt(),wsaaPercent),100));
			}
		}
		else {
			wsaaYear.add(10);
			wsaaPercent.set(th622rec.zryrperc[wsaaYear.toInt()]);
			if (isEQ(th622rec.keyopt02,"P")) {
				compute(wsaaCommAmt, 2).set(div(mult(mult(wsaaInstprem,wsaaFreq),wsaaPercent),100));
			}
			else {
				compute(wsaaCommAmt, 2).set(div(mult(agpyagtIO.getOrigamt(),wsaaPercent),100));
			}
		}
		/*EXIT*/
	}

protected void callLifeacmv2800()
	{
		begin2800();
	}

protected void begin2800()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.batccoy.set(batcdorrec.company);
		lifacmvrec1.rldgcoy.set(batcdorrec.company);
		lifacmvrec1.genlcoy.set(batcdorrec.company);
		lifacmvrec1.batcactyr.set(batcdorrec.actyear);
		lifacmvrec1.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec1.batctrcde.set(batcdorrec.trcde);
		lifacmvrec1.batcbatch.set(batcdorrec.batch);
		lifacmvrec1.batcbrn.set(batcdorrec.branch);
		lifacmvrec1.rcamt.set(0);
		lifacmvrec1.crate.set(0);
		lifacmvrec1.acctamt.set(0);
		lifacmvrec1.user.set(0);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.trandesc.set(agpyagtIO.getTrandesc());
		lifacmvrec1.tranno.set(agpyagtIO.getTranno());
		lifacmvrec1.effdate.set(agpyagtIO.getEffdate());
		lifacmvrec1.rdocnum.set(agpyagtIO.getRdocnum());
		lifacmvrec1.crate.set(agpyagtIO.getCrate());
		lifacmvrec1.genlcur.set(agpyagtIO.getGenlcur());
		lifacmvrec1.termid.set(agpyagtIO.getTermid());
		lifacmvrec1.origcurr.set(agpyagtIO.getOrigcurr());
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.sacscode.set(t5645rec.sacscode01);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype01);
		lifacmvrec1.glcode.set(t5645rec.glmap01);
		lifacmvrec1.glsign.set(t5645rec.sign01);
		lifacmvrec1.contot.set(t5645rec.cnttot01);
		lifacmvrec1.rldgacct.set(wsaaReporttoAgnt);
		lifacmvrec1.origamt.set(wsaaCommAmt);
		lifacmvrec1.acctamt.set(wsaaCommAmt);
		lifacmvrec1.tranref.set(agpyagtIO.getTranref());
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.suprflag.set("Y");
		lifacmvrec1.substituteCode[1].set(chdrenqIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(wsaaCrtable);
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			lifacmvrec1.sacscode.set(t5645rec.sacscode02);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec1.glcode.set(t5645rec.glmap02);
			lifacmvrec1.glsign.set(t5645rec.sign02);
			lifacmvrec1.contot.set(t5645rec.cnttot02);
		}
		else {
			lifacmvrec1.sacscode.set(t5645rec.sacscode03);
			lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec1.glcode.set(t5645rec.glmap03);
			lifacmvrec1.glsign.set(t5645rec.sign03);
			lifacmvrec1.contot.set(t5645rec.cnttot03);
		}
		lifacmvrec1.substituteCode[1].set(chdrenqIO.getCnttype());
		lifacmvrec1.substituteCode[6].set(wsaaCrtable);
		lifacmvrec1.rldgacct.set(wsaaCovrKey);
		lifacmvrec1.origamt.set(wsaaCommAmt);
		lifacmvrec1.acctamt.set(wsaaCommAmt);
		lifacmvrec1.tranref.set(agpyagtIO.getTranref());
		wsaaJrnseq.add(1);
		lifacmvrec1.jrnseq.set(wsaaJrnseq);
		lifacmvrec1.frcdate.set(bsscIO.getEffectiveDate());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
		wsaaCounter.add(1);
	}

protected void update3000()
	{
		/*UPDATE*/
		/*ORC-COMPLETE*/
		updateAcmv3100();
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void updateAcmv3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					begin3100();
				}
				case acmvLoop3105: {
					acmvLoop3105();
				}
				case exit3190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3100()
	{
		acmvagtIO.setRecKeyData(SPACES);
		acmvagtIO.setFunction(varcom.begnh);
		acmvagtIO.setBatccoy(agpyagtIO.getBatccoy());
		acmvagtIO.setBatcbrn(agpyagtIO.getBatcbrn());
		acmvagtIO.setBatcactyr(agpyagtIO.getBatcactyr());
		acmvagtIO.setBatcactmn(agpyagtIO.getBatcactmn());
		acmvagtIO.setBatctrcde(agpyagtIO.getBatctrcde());
		acmvagtIO.setBatcbatch(agpyagtIO.getBatcbatch());
		acmvagtIO.setRldgacct(agpyagtIO.getRldgacct());
		acmvagtIO.setOrigcurr(agpyagtIO.getOrigcurr());
		acmvagtIO.setSacscode(agpyagtIO.getSacscode());
		acmvagtIO.setSacstyp(agpyagtIO.getSacstyp());
		acmvagtIO.setTranno(agpyagtIO.getTranno());
		acmvagtIO.setRdocnum(agpyagtIO.getRdocnum());
		acmvagtIO.setJrnseq(agpyagtIO.getJrnseq());
	}

protected void acmvLoop3105()
	{
		SmartFileCode.execute(appVars, acmvagtIO);
		if (isNE(acmvagtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acmvagtIO.getParams());
			syserrrec.statuz.set(acmvagtIO.getStatuz());
			fatalError600();
		}
		if (isNE(acmvagtIO.getBatccoy(),agpyagtIO.getBatccoy())
		|| isNE(acmvagtIO.getRldgacct(),agpyagtIO.getRldgacct())
		|| isNE(acmvagtIO.getSacscode(),agpyagtIO.getSacscode())
		|| isNE(acmvagtIO.getSacstyp(),agpyagtIO.getSacstyp())
		|| isNE(acmvagtIO.getTranno(),agpyagtIO.getTranno())
		|| isNE(acmvagtIO.getRdocnum(),agpyagtIO.getRdocnum())
		|| isNE(acmvagtIO.getJrnseq(),agpyagtIO.getJrnseq())) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(agpyagtIO.getOrigamt(),acmvagtIO.getOrigamt())) {
			acmvagtIO.setFunction(varcom.nextr);
			goTo(GotoLabel.acmvLoop3105);
		}
		if (isNE(acmvagtIO.getFrcdate(),varcom.vrcmMaxDate)) {
			acmvagtIO.setFunction(varcom.nextr);
			goTo(GotoLabel.acmvLoop3105);
		}
		acmvagtIO.setSuprflg("Y");
		acmvagtIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, acmvagtIO);
		if (isNE(acmvagtIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acmvagtIO.getParams());
			syserrrec.statuz.set(acmvagtIO.getStatuz());
			fatalError600();
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a0500CheckAfi()
	{
		a0501Ptrn();
	}

protected void a0501Ptrn()
	{
		ptrnwfdIO.setDataKey(SPACES);
		wsaaRecordOmit = "N";
		ptrnwfdIO.setChdrcoy(agpyagtIO.getRldgcoy());
		ptrnwfdIO.setChdrnum(agpyagtIO.getRdocnum());
		ptrnwfdIO.setTranno(agpyagtIO.getTranno());
		ptrnwfdIO.setFormat(ptrnwfdrec);
		ptrnwfdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ptrnwfdIO);
		if (isNE(ptrnwfdIO.getStatuz(),varcom.oK)
		&& isNE(ptrnwfdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(ptrnwfdIO.getParams());
			syserrrec.statuz.set(ptrnwfdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ptrnwfdIO.getStatuz(),varcom.oK)) {
			wsaaRecordOmit = "Y";
		}
	}

protected void a2619GetIssueDate()
	{
		a2619GetIssue();
		a2619Call();
	}

protected void a2619GetIssue()
	{
		agcmstaIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(wsbbChdrcoy);
		hpadIO.setChdrnum(wsbbChdrnum);
		hpadIO.setFunction(varcom.readr);
		hpadIO.setFormat(hpadrec);
	}

protected void a2619Call()
	{
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		wsaaIssueDate.set(hpadIO.getHoissdte());
		/*A2619-EXIT*/
	}

protected void a2620CheckAgentChange()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a2620AgentChg();
				}
				case a2621Call: {
					a2621Call();
				}
				case a2629Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a2620AgentChg()
	{
		agcmstaIO.setDataArea(SPACES);
		agcmstaIO.setChdrcoy(wsbbChdrcoy);
		agcmstaIO.setChdrnum(wsbbChdrnum);
		agcmstaIO.setLife(wsbbLife);
		agcmstaIO.setCoverage(wsbbCoverage);
		agcmstaIO.setRider(wsbbRider);
		agcmstaIO.setPlanSuffix(wsbbPlanSuffix);
		agcmstaIO.setAgntnum(ZERO);
		agcmstaIO.setFormat(agcmstarec);
		agcmstaIO.setFunction(varcom.begn);

		//performance improvement -- Anjali
		agcmstaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmstaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

	}

protected void a2621Call()
	{
		SmartFileCode.execute(appVars, agcmstaIO);
		if (isNE(agcmstaIO.getStatuz(),varcom.oK)
		&& isNE(agcmstaIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agcmstaIO.getParams());
			syserrrec.statuz.set(agcmstaIO.getStatuz());
			fatalError600();
		}
		if (isNE(agcmstaIO.getChdrcoy(),wsbbChdrcoy)
		|| isNE(agcmstaIO.getChdrnum(),wsbbChdrnum)
		|| isEQ(agcmstaIO.getStatuz(),varcom.endp)) {
			wsaaAgtchgDate.set(ZERO);
			goTo(GotoLabel.a2629Exit);
		}
		if (isEQ(agcmstaIO.getChdrcoy(),wsbbChdrcoy)
		&& isEQ(agcmstaIO.getChdrnum(),wsbbChdrnum)
		&& isEQ(agcmstaIO.getLife(),wsbbLife)
		&& isEQ(agcmstaIO.getCoverage(),wsbbCoverage)
		&& isEQ(agcmstaIO.getRider(),wsbbRider)
		&& isEQ(agcmstaIO.getPlanSuffix(),wsbbPlanSuffix)) {
			chdragtIO.setDataKey(SPACES);
			chdragtIO.setAgntpfx(fsupfxcpy.agnt);
			chdragtIO.setAgntcoy(agcmstaIO.getChdrcoy());
			chdragtIO.setAgntnum(agcmstaIO.getAgntnum());
			chdragtIO.setValidflag("2");
			chdragtIO.setChdrcoy(agcmstaIO.getChdrcoy());
			chdragtIO.setChdrnum(agcmstaIO.getChdrnum());
			chdragtIO.setFunction(varcom.readr);
			chdragtIO.setFormat(chdragtrec);
			SmartFileCode.execute(appVars, chdragtIO);
			if (isNE(chdragtIO.getStatuz(),varcom.oK)
			&& isNE(chdragtIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(chdragtIO.getParams());
				syserrrec.statuz.set(chdragtIO.getStatuz());
				fatalError600();
			}
			if (isEQ(chdragtIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.a2629Exit);
			}
			wsaaAgtchgDate.set(chdragtIO.getBtdate());
			goTo(GotoLabel.a2629Exit);
		}
		else {
			agcmstaIO.setFunction(varcom.nextr);
			goTo(GotoLabel.a2621Call);
		}
	}

protected void a2630GetCompareDate()
	{
		/*A2631-GET-DATE*/
		if ((isGT(wsaaCoverDate,wsaaAgtchgDate)
		|| isEQ(wsaaCoverDate,wsaaAgtchgDate))
		&& (isGT(wsaaCoverDate,wsaaIssueDate)
		|| isEQ(wsaaCoverDate,wsaaIssueDate))) {
			wsaaCrrcd.set(wsaaCoverDate);
		}
		else {
			if ((isGT(wsaaAgtchgDate,wsaaCoverDate)
			|| isEQ(wsaaAgtchgDate,wsaaCoverDate))
			&& (isGT(wsaaAgtchgDate,wsaaIssueDate)
			|| isEQ(wsaaAgtchgDate,wsaaIssueDate))) {
				wsaaCrrcd.set(wsaaAgtchgDate);
			}
			else {
				if ((isGT(wsaaIssueDate,wsaaCoverDate)
				|| isEQ(wsaaIssueDate,wsaaCoverDate))
				&& (isGT(wsaaIssueDate,wsaaAgtchgDate)
				|| isEQ(wsaaIssueDate,wsaaAgtchgDate))) {
					wsaaCrrcd.set(wsaaIssueDate);
				}
			}
		}
		/*A2639-EXIT*/
	}
}
