package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5040
 * @version 1.0 generated on 30/08/09 06:32
 * @author Quipoz
 */
public class S5040ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(134);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(68).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,47);
	public ZonedDecimalData ovcpc = DD.ovcpc.copyToZonedDecimal().isAPartOf(subfileFields,55);
	public FixedLengthStringData reportag = DD.reportag.copy().isAPartOf(subfileFields,60);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 68);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData ovcpcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData reportagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 84);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] ovcpcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] reportagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 132);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5040screensflWritten = new LongData(0);
	public LongData S5040screenctlWritten = new LongData(0);
	public LongData S5040screenWritten = new LongData(0);
	public LongData S5040protectWritten = new LongData(0);
	public GeneralTable s5040screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5040screensfl;
	}

	public S5040ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenSflFields = new BaseData[] {reportag, clntnum, agentname, ovcpc};
		screenSflOutFields = new BaseData[][] {reportagOut, clntnumOut, agentnameOut, ovcpcOut};
		screenSflErrFields = new BaseData[] {reportagErr, clntnumErr, agentnameErr, ovcpcErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {clntsel, cltname, agnum, agtype, agtydesc};
		screenOutFields = new BaseData[][] {clntselOut, cltnameOut, agnumOut, agtypeOut, agtydescOut};
		screenErrFields = new BaseData[] {clntselErr, cltnameErr, agnumErr, agtypeErr, agtydescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5040screen.class;
		screenSflRecord = S5040screensfl.class;
		screenCtlRecord = S5040screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5040protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5040screenctl.lrec.pageSubfile);
	}
}
