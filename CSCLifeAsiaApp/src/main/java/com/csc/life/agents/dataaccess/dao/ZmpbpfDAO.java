package com.csc.life.agents.dataaccess.dao;
 

import com.csc.life.agents.dataaccess.model.Zmpbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZmpbpfDAO extends BaseDAO<Zmpbpf> {
	public Zmpbpf getZmpbpfRecord(Zmpbpf zmpbpf);
    public int updateZmpbRecord(Zmpbpf zmpbpf);
    public int insertZmpbData(Zmpbpf zmpbpf);
    
   
}
