package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:49
 * Description:
 * Copybook name: PM509PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pm509par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(48);
  	public FixedLengthStringData agtydesc = new FixedLengthStringData(30).isAPartOf(parmRecord, 0);
  	public ZonedDecimalData datefrm = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 30);
  	public ZonedDecimalData dateto = new ZonedDecimalData(8, 0).isAPartOf(parmRecord, 38);
  	public FixedLengthStringData mlagttyp = new FixedLengthStringData(2).isAPartOf(parmRecord, 46);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}