package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5036
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5036ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(580);
	public FixedLengthStringData dataFields = new FixedLengthStringData(196).isAPartOf(dataArea, 0);
	public FixedLengthStringData agccqind = DD.agccqind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData bmaflg = DD.bmaflg.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData carLoan = DD.carln.copy().isAPartOf(dataFields,42);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData computerLoan = DD.comln.copy().isAPartOf(dataFields,100);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,101);
	public ZonedDecimalData fixprc = DD.fixprc.copyToZonedDecimal().isAPartOf(dataFields,109);
	public FixedLengthStringData houseLoan = DD.hseln.copy().isAPartOf(dataFields,113);
	public ZonedDecimalData intcrd = DD.intcrd.copyToZonedDecimal().isAPartOf(dataFields,114);
	public FixedLengthStringData irdno = DD.irdno.copy().isAPartOf(dataFields,118);
	public FixedLengthStringData officeRent = DD.offrent.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData otherLoans = DD.othln.copy().isAPartOf(dataFields,129);
	public ZonedDecimalData sprprc = DD.sprprc.copyToZonedDecimal().isAPartOf(dataFields,130);
	public FixedLengthStringData sprschm = DD.sprschm.copy().isAPartOf(dataFields,134);
	public ZonedDecimalData taxalw = DD.taxalw.copyToZonedDecimal().isAPartOf(dataFields,142);
	public FixedLengthStringData taxcde = DD.taxcde.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData taxmeth = DD.taxmeth.copy().isAPartOf(dataFields,161);
	public ZonedDecimalData tcolbal = DD.tcolbal.copyToZonedDecimal().isAPartOf(dataFields,163);
	public ZonedDecimalData tcolmax = DD.tcolmax.copyToZonedDecimal().isAPartOf(dataFields,177);
	public ZonedDecimalData tcolprct = DD.tcolprct.copyToZonedDecimal().isAPartOf(dataFields,191);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 196);
	public FixedLengthStringData agccqindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bmaflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData carlnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData comlnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData fixprcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData hselnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData intcrdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData irdnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData offrentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData othlnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData sprprcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData sprschmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData taxalwErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData taxcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData taxmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData tcolbalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData tcolmaxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData tcolprctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 292);
	public FixedLengthStringData[] agccqindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bmaflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] carlnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] comlnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] fixprcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] hselnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] intcrdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] irdnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] offrentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] othlnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] sprprcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] sprschmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] taxalwOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] taxcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] taxmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] tcolbalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] tcolmaxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] tcolprctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);

	public LongData S5036screenWritten = new LongData(0);
	public LongData S5036protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5036ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(bmaflgOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(intcrdOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fixprcOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxmethOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxcdeOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sprschmOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agccqindOut,new String[] {"15","16","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hselnOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comlnOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(carlnOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(offrentOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(othlnOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tcolprctOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tcolmaxOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {clntsel, cltname, agnum, agtype, agtydesc, dteapp, bmaflg, intcrd, fixprc, taxmeth, taxcde, taxalw, irdno, sprschm, sprprc, agccqind, houseLoan, computerLoan, carLoan, officeRent, otherLoans, tcolprct, tcolmax, tcolbal};
		screenOutFields = new BaseData[][] {clntselOut, cltnameOut, agnumOut, agtypeOut, agtydescOut, dteappOut, bmaflgOut, intcrdOut, fixprcOut, taxmethOut, taxcdeOut, taxalwOut, irdnoOut, sprschmOut, sprprcOut, agccqindOut, hselnOut, comlnOut, carlnOut, offrentOut, othlnOut, tcolprctOut, tcolmaxOut, tcolbalOut};
		screenErrFields = new BaseData[] {clntselErr, cltnameErr, agnumErr, agtypeErr, agtydescErr, dteappErr, bmaflgErr, intcrdErr, fixprcErr, taxmethErr, taxcdeErr, taxalwErr, irdnoErr, sprschmErr, sprprcErr, agccqindErr, hselnErr, comlnErr, carlnErr, offrentErr, othlnErr, tcolprctErr, tcolmaxErr, tcolbalErr};
		screenDateFields = new BaseData[] {dteapp};
		screenDateErrFields = new BaseData[] {dteappErr};
		screenDateDispFields = new BaseData[] {dteappDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5036screen.class;
		protectRecord = S5036protect.class;
	}

}
