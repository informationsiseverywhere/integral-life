package com.csc.life.agents.screens;

import com.csc.fsu.clients.screens.S2473ScreenVars;
import com.csc.life.enquiries.screens.S6222ScreenVars;
import com.csc.life.enquiries.screens.S6222screensfl;
import com.csc.smart.screens.S0026ScreenVars;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.TableModel.Subfile.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl03screensfl extends Subfile {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}; 
	public static int maxRecords = 16;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14};

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 21, 6, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl03ScreenVars sv = (Sjl03ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sjl03screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sjl03screensfl, 
			sv.Sjl03screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl03ScreenVars sv = (Sjl03ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sjl03screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl03ScreenVars sv = (Sjl03ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sjl03screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl03screensflWritten.gt(0))
		{
			sv.sjl03screensfl.setCurrentIndex(0);
			sv.Sjl03screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl03ScreenVars sv = (Sjl03ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sjl03screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl03screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		Sjl03ScreenVars sv = (Sjl03ScreenVars)pv;
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {		
		Sjl03ScreenVars sv = (Sjl03ScreenVars)pv;
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {		
		Sjl03ScreenVars screenVars = (Sjl03ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.agntqualification.clearFormatting();
		screenVars.liceno.clearFormatting();
		screenVars.quadesc.clearFormatting();
		screenVars.startdteDisp.clearFormatting();
		screenVars.enddteDisp.clearFormatting();
		screenVars.uprflag.clearFormatting();
		clearClassString(pv);
	}
	
	public static void clearClassString(VarModel pv) {
		Sjl03ScreenVars screenVars = (Sjl03ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.agntqualification.setClassString("");
		screenVars.liceno.setClassString("");
		screenVars.quadesc.setClassString("");
		screenVars.startdteDisp.setClassString("");
		screenVars.enddteDisp.setClassString("");
		screenVars.uprflag.setClassString("");
	}

/**
 * Clear all the variables in Sjl03screensfl
 */
	public static void clear(VarModel pv) {
		Sjl03ScreenVars screenVars = (Sjl03ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
	
	public static void getSubfileData(DataModel dm, COBOLAppVars av,
			 VarModel pv) {
			if (dm != null) {
				Sjl03ScreenVars screenVars = (Sjl03ScreenVars) pv;
				if (screenVars.screenIndicArea.getFieldName() == null) {
					screenVars.screenIndicArea.setFieldName("screenIndicArea");
					screenVars.agntqualification.setFieldName("agntqualification");
					screenVars.liceno.setFieldName("liceno");
					screenVars.quadesc.setFieldName("quadesc");
					screenVars.startdteDisp.setFieldName("startdteDisp");
					screenVars.enddteDisp.setFieldName("enddteDisp");
					screenVars.uprflag.setFieldName("uprflag");
				}
				screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
				screenVars.agntqualification.set(dm.getField("agntqualification"));
				screenVars.liceno.set(dm.getField("liceno"));
				screenVars.quadesc.set(dm.getField("quadesc"));
				screenVars.startdteDisp.set(dm.getField("startdteDisp"));
				screenVars.enddteDisp.set(dm.getField("enddteDisp"));
				screenVars.uprflag.set(dm.getField("uprflag"));
			}
		}
	
	public static void setSubfileData(DataModel dm, COBOLAppVars av,
			 VarModel pv) {
		if (dm != null) {
			Sjl03ScreenVars screenVars = (Sjl03ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.agntqualification.setFieldName("agntqualification");
				screenVars.liceno.setFieldName("liceno");
				screenVars.quadesc.setFieldName("quadesc");
				screenVars.startdteDisp.setFieldName("startdteDisp");
				screenVars.enddteDisp.setFieldName("enddteDisp");
				screenVars.uprflag.setFieldName("uprflag");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("agntqualification").set(screenVars.agntqualification);
			dm.getField("liceno").set(screenVars.liceno);
			dm.getField("quadesc").set(screenVars.quadesc);
			dm.getField("startdteDisp").set(screenVars.startdteDisp);
			dm.getField("enddteDisp").set(screenVars.enddteDisp);
			dm.getField("uprflag").set(screenVars.uprflag);
			}
		}
}
