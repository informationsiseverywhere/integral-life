package com.csc.life.agents.dataaccess.dao;

import com.csc.life.agents.dataaccess.dao.impl.ZqbdpfDAOImpl;
import com.csc.life.agents.dataaccess.dao.impl.ZqbhpfDAOImpl;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.dao.impl.GenericDAOImpl;
import com.quipoz.framework.util.AppVars;

public class AgentDAOFactory {

	private static GenericDAOImpl instantiateDAO(Class daoClass) {
		try {
			GenericDAOImpl dao = (GenericDAOImpl) daoClass.newInstance();
			SMARTAppVars appVars = (SMARTAppVars) AppVars.getInstance();
			dao.setSession(appVars.getHibernateSession());
			return dao;
		} catch (Exception ex) {
			throw new RuntimeException("Can not instantiate DAO: " + daoClass, ex);
		}
	}

	
	public static ZqbhpfDAO getZqbhpfDAO()
	{
		return (ZqbhpfDAO)instantiateDAO(ZqbhpfDAOImpl.class);
	}
	
	public static ZqbdpfDAO getZqbdpfDAO()
	{
		return (ZqbdpfDAO)instantiateDAO(ZqbdpfDAOImpl.class);
	}
}
