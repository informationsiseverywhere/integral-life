package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:15
 * Description:
 * Copybook name: ZBNWPRMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwprmrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwprmRec = new FixedLengthStringData(253);
  	public FixedLengthStringData bnwprmTrcde = new FixedLengthStringData(3).isAPartOf(bnwprmRec, 0);
  	public FixedLengthStringData bnwprmSource = new FixedLengthStringData(10).isAPartOf(bnwprmRec, 3);
  	public FixedLengthStringData bnwprmCategory = new FixedLengthStringData(1).isAPartOf(bnwprmRec, 13);
  	public FixedLengthStringData bnwprmAmount = new FixedLengthStringData(13).isAPartOf(bnwprmRec, 14);
  	public FixedLengthStringData bnwprmEffdate = new FixedLengthStringData(10).isAPartOf(bnwprmRec, 27);
  	public FixedLengthStringData bnwprmDuedate = new FixedLengthStringData(10).isAPartOf(bnwprmRec, 37);
  	public FixedLengthStringData bnwprmCrtable = new FixedLengthStringData(50).isAPartOf(bnwprmRec, 47);
  	public FixedLengthStringData bnwprmCompany = new FixedLengthStringData(3).isAPartOf(bnwprmRec, 97);
  	public FixedLengthStringData bnwprmChdrnum = new FixedLengthStringData(30).isAPartOf(bnwprmRec, 100);
  	public FixedLengthStringData bnwprmPlan = new FixedLengthStringData(30).isAPartOf(bnwprmRec, 130);
  	public FixedLengthStringData bnwprmPremtype = new FixedLengthStringData(20).isAPartOf(bnwprmRec, 160);
  	public FixedLengthStringData bnwprmInd = new FixedLengthStringData(1).isAPartOf(bnwprmRec, 180);
  	public FixedLengthStringData bnwprmMemo = new FixedLengthStringData(20).isAPartOf(bnwprmRec, 181);
  	public FixedLengthStringData bnwprmRollover = new FixedLengthStringData(20).isAPartOf(bnwprmRec, 201);
  	public FixedLengthStringData bnwprmPyind = new FixedLengthStringData(1).isAPartOf(bnwprmRec, 221);
  	public FixedLengthStringData bnwprmFund = new FixedLengthStringData(3).isAPartOf(bnwprmRec, 222);
  	public FixedLengthStringData bnwprmTrandate = new FixedLengthStringData(10).isAPartOf(bnwprmRec, 225);
  	public FixedLengthStringData bnwprmProcdate = new FixedLengthStringData(10).isAPartOf(bnwprmRec, 235);
  	public FixedLengthStringData bnwprmFiller = new FixedLengthStringData(1).isAPartOf(bnwprmRec, 245);
  	public FixedLengthStringData bnwprmOvrprnt = new FixedLengthStringData(6).isAPartOf(bnwprmRec, 246);
  	public FixedLengthStringData bnwprmOvrind = new FixedLengthStringData(1).isAPartOf(bnwprmRec, 252);


	public void initialize() {
		COBOLFunctions.initialize(bnwprmRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwprmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}