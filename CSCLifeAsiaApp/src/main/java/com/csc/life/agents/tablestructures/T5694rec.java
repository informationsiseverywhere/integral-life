package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:05
 * Description:
 * Copybook name: T5694REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5694rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5694Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData instalpcs = new FixedLengthStringData(55).isAPartOf(t5694Rec, 0);
  	public ZonedDecimalData[] instalpc = ZDArrayPartOfStructure(11, 5, 2, instalpcs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(instalpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData instalpc01 = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
  	public ZonedDecimalData instalpc02 = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
  	public ZonedDecimalData instalpc03 = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
  	public ZonedDecimalData instalpc04 = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
  	public ZonedDecimalData instalpc05 = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
  	public ZonedDecimalData instalpc06 = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
  	public ZonedDecimalData instalpc07 = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
  	public ZonedDecimalData instalpc08 = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
  	public ZonedDecimalData instalpc09 = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
  	public ZonedDecimalData instalpc10 = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
  	public ZonedDecimalData instalpc11 = new ZonedDecimalData(5, 2).isAPartOf(filler, 50);
  	public FixedLengthStringData toYears = new FixedLengthStringData(22).isAPartOf(t5694Rec, 55);
  	public ZonedDecimalData[] toYear = ZDArrayPartOfStructure(11, 2, 0, toYears, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(toYears, 0, FILLER_REDEFINE);
  	public ZonedDecimalData toYear01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData toYear02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData toYear03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData toYear04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData toYear05 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
  	public ZonedDecimalData toYear06 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
  	public ZonedDecimalData toYear07 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData toYear08 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
  	public ZonedDecimalData toYear09 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
  	public ZonedDecimalData toYear10 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData toYear11 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 20);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(423).isAPartOf(t5694Rec, 77, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5694Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5694Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}