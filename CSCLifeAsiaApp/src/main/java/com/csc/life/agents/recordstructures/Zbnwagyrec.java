package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:10
 * Description:
 * Copybook name: ZBNWAGYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwagyrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwagyRec = new FixedLengthStringData(391);
  	public FixedLengthStringData bnwagyTrcde = new FixedLengthStringData(3).isAPartOf(bnwagyRec, 0);
  	public FixedLengthStringData bnwagySource = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 3);
  	public FixedLengthStringData bnwagyCompany = new FixedLengthStringData(3).isAPartOf(bnwagyRec, 13);
  	public FixedLengthStringData bnwagyAgency = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 16);
  	public FixedLengthStringData bnwagyAgyname = new FixedLengthStringData(50).isAPartOf(bnwagyRec, 26);
  	public FixedLengthStringData bnwagyAddr01 = new FixedLengthStringData(60).isAPartOf(bnwagyRec, 76);
  	public FixedLengthStringData bnwagyAddr02 = new FixedLengthStringData(60).isAPartOf(bnwagyRec, 136);
  	public FixedLengthStringData bnwagyAddr03 = new FixedLengthStringData(60).isAPartOf(bnwagyRec, 196);
  	public FixedLengthStringData bnwagyCity = new FixedLengthStringData(50).isAPartOf(bnwagyRec, 256);
  	public FixedLengthStringData bnwagyState = new FixedLengthStringData(2).isAPartOf(bnwagyRec, 306);
  	public FixedLengthStringData bnwagyZip = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 308);
  	public FixedLengthStringData bnwagyCountry = new FixedLengthStringData(2).isAPartOf(bnwagyRec, 318);
  	public FixedLengthStringData bnwagyFiller1 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 320).init(SPACES);
  	public FixedLengthStringData bnwagyFiller2 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 321).init(SPACES);
  	public FixedLengthStringData bnwagyFiller3 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 322).init(SPACES);
  	public FixedLengthStringData bnwagyFiller4 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 323).init(SPACES);
  	public FixedLengthStringData bnwagyFiller5 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 324).init(SPACES);
  	public FixedLengthStringData bnwagyFiller6 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 325).init(SPACES);
  	public FixedLengthStringData bnwagyFiller7 = new FixedLengthStringData(1).isAPartOf(bnwagyRec, 326).init(SPACES);
  	public FixedLengthStringData bnwagyTaxid = new FixedLengthStringData(34).isAPartOf(bnwagyRec, 327);
  	public FixedLengthStringData bnwagyStrtdate = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 361);
  	public FixedLengthStringData bnwagyTrmdate = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 371);
  	public FixedLengthStringData bnwagyProcdate = new FixedLengthStringData(10).isAPartOf(bnwagyRec, 381);


	public void initialize() {
		COBOLFunctions.initialize(bnwagyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwagyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}