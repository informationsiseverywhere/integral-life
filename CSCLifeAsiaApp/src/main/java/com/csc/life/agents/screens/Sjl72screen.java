package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 10/08/20 05:41
 * @author agoel51
 */
public class Sjl72screen extends ScreenRecord  { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl72ScreenVars sv = (Sjl72ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl72screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl72ScreenVars screenVars = (Sjl72ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.company.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.agbrdesc.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.levelno.setClassString("");
		screenVars.leveltyp.setClassString("");
		screenVars.leveldesc.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.agtydesc.setClassString("");
		screenVars.userid.setClassString("");
		screenVars.salestatus.setClassString("");
		screenVars.reasonmod.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.reasondel.setClassString("");
		screenVars.moddate.setClassString("");
		screenVars.deldate.setClassString("");
		screenVars.saledept.setClassString("");
		screenVars.saledptdes.setClassString("");
	}

/**
 * Clear all the variables in Sjl72screen
 */
	public static void clear(VarModel pv) {
		Sjl72ScreenVars screenVars = (Sjl72ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.company.clear();
		screenVars.agntbr.clear();
		screenVars.agbrdesc.clear();
		screenVars.aracde.clear();
		screenVars.aradesc.clear();
		screenVars.levelno.clear();
		screenVars.leveltyp.clear();
		screenVars.leveldesc.clear();
		screenVars.agtype.clear();
		screenVars.agtydesc.clear();
		screenVars.userid.clear();
		screenVars.salestatus.clear();
		screenVars.reasonmod.clear();
		screenVars.resndesc.clear();
		screenVars.reasondel.clear();
		screenVars.moddate.clear();
		screenVars.deldate.clear();
		screenVars.saledept.clear();
		screenVars.saledptdes.clear();
	}

}
