package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5576screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5576ScreenVars sv = (S5576ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5576screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5576ScreenVars screenVars = (S5576ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.age01.setClassString("");
		screenVars.annumpc01.setClassString("");
		screenVars.agemax01.setClassString("");
		screenVars.minpcnt01.setClassString("");
		screenVars.maxpcnt01.setClassString("");
		screenVars.flatpcnt01.setClassString("");
		screenVars.age02.setClassString("");
		screenVars.annumpc02.setClassString("");
		screenVars.agemax02.setClassString("");
		screenVars.minpcnt02.setClassString("");
		screenVars.maxpcnt02.setClassString("");
		screenVars.flatpcnt02.setClassString("");
		screenVars.age03.setClassString("");
		screenVars.annumpc03.setClassString("");
		screenVars.agemax03.setClassString("");
		screenVars.minpcnt03.setClassString("");
		screenVars.maxpcnt03.setClassString("");
		screenVars.flatpcnt03.setClassString("");
		screenVars.age04.setClassString("");
		screenVars.annumpc04.setClassString("");
		screenVars.agemax04.setClassString("");
		screenVars.minpcnt04.setClassString("");
		screenVars.maxpcnt04.setClassString("");
		screenVars.flatpcnt04.setClassString("");
		screenVars.age05.setClassString("");
		screenVars.annumpc05.setClassString("");
		screenVars.agemax05.setClassString("");
		screenVars.minpcnt05.setClassString("");
		screenVars.maxpcnt05.setClassString("");
		screenVars.flatpcnt05.setClassString("");
		screenVars.age06.setClassString("");
		screenVars.annumpc06.setClassString("");
		screenVars.agemax06.setClassString("");
		screenVars.minpcnt06.setClassString("");
		screenVars.maxpcnt06.setClassString("");
		screenVars.flatpcnt06.setClassString("");
		screenVars.age07.setClassString("");
		screenVars.annumpc07.setClassString("");
		screenVars.agemax07.setClassString("");
		screenVars.minpcnt07.setClassString("");
		screenVars.maxpcnt07.setClassString("");
		screenVars.flatpcnt07.setClassString("");
		screenVars.age08.setClassString("");
		screenVars.annumpc08.setClassString("");
		screenVars.agemax08.setClassString("");
		screenVars.minpcnt08.setClassString("");
		screenVars.maxpcnt08.setClassString("");
		screenVars.flatpcnt08.setClassString("");
		screenVars.highyr.setClassString("");
		screenVars.highmth.setClassString("");
		screenVars.lowyr.setClassString("");
		screenVars.lowmth.setClassString("");
		screenVars.actualday.setClassString("");
	}

/**
 * Clear all the variables in S5576screen
 */
	public static void clear(VarModel pv) {
		S5576ScreenVars screenVars = (S5576ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.age01.clear();
		screenVars.annumpc01.clear();
		screenVars.agemax01.clear();
		screenVars.minpcnt01.clear();
		screenVars.maxpcnt01.clear();
		screenVars.flatpcnt01.clear();
		screenVars.age02.clear();
		screenVars.annumpc02.clear();
		screenVars.agemax02.clear();
		screenVars.minpcnt02.clear();
		screenVars.maxpcnt02.clear();
		screenVars.flatpcnt02.clear();
		screenVars.age03.clear();
		screenVars.annumpc03.clear();
		screenVars.agemax03.clear();
		screenVars.minpcnt03.clear();
		screenVars.maxpcnt03.clear();
		screenVars.flatpcnt03.clear();
		screenVars.age04.clear();
		screenVars.annumpc04.clear();
		screenVars.agemax04.clear();
		screenVars.minpcnt04.clear();
		screenVars.maxpcnt04.clear();
		screenVars.flatpcnt04.clear();
		screenVars.age05.clear();
		screenVars.annumpc05.clear();
		screenVars.agemax05.clear();
		screenVars.minpcnt05.clear();
		screenVars.maxpcnt05.clear();
		screenVars.flatpcnt05.clear();
		screenVars.age06.clear();
		screenVars.annumpc06.clear();
		screenVars.agemax06.clear();
		screenVars.minpcnt06.clear();
		screenVars.maxpcnt06.clear();
		screenVars.flatpcnt06.clear();
		screenVars.age07.clear();
		screenVars.annumpc07.clear();
		screenVars.agemax07.clear();
		screenVars.minpcnt07.clear();
		screenVars.maxpcnt07.clear();
		screenVars.flatpcnt07.clear();
		screenVars.age08.clear();
		screenVars.annumpc08.clear();
		screenVars.agemax08.clear();
		screenVars.minpcnt08.clear();
		screenVars.maxpcnt08.clear();
		screenVars.flatpcnt08.clear();
		screenVars.highyr.clear();
		screenVars.highmth.clear();
		screenVars.lowyr.clear();
		screenVars.lowmth.clear();
		screenVars.actualday.clear();
	}
}
