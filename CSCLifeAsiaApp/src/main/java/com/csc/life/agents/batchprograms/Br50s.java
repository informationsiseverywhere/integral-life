/*
 * File: Br50s.java
 * Date: December 3, 2013 2:08:13 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR50S.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.tablestructures.Tr392rec;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.agents.dataaccess.dao.AgpbpfDAO; //ILIFE-8398
import com.csc.life.agents.dataaccess.model.Agpbpf; //ILIFE-8398
import com.csc.life.agents.reports.R5338Report;
import com.csc.life.agents.tablestructures.T5622rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.reassurance.dataaccess.dao.AcmvpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
//import com.csc.smart.dataaccess.model.Agpypf;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.dao.AgpypfSearchDAO;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Acmvpf;
import com.csc.smart400framework.dataaccess.model.Agpypf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This is a skeleton for a batch mainline program.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*    Read
*     - read first primary file record.
*
*    Perform    Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock the record if it is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of agents printed
*     02  -  No. statement entries
*     03  -  Total statement amounts
*     04  -  No. Pages printed
*     05  -  Tot commission amount
*     06  -  Tot commission suspended
*     07  -  No. agents printed nopay
*     08  -  Tot insufficient Comm Amt
*     09  -  No. comm in Advance
*     10  -  No. of AGPB record write
*     11  -  Totol Comm. can be paid
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
****************************************************************** ****
* </pre>
*/
public class Br50s extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5338Report printerFile = new R5338Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(450);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR50S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	
	private static final int wsaaPageSize = 64;
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).init(66).setUnsigned();
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);
	private PackedDecimalData wsaaTotOrigAmt = new PackedDecimalData(18, 2).init(ZERO);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(17, 2).init(ZERO);

	private FixedLengthStringData wsaaChdrCurrArray = new FixedLengthStringData(1080);
	private FixedLengthStringData[] wsaaChdrCurr = FLSArrayPartOfStructure(90, 12, wsaaChdrCurrArray, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(9, wsaaChdrCurr, 0);
	private FixedLengthStringData[] wsaaCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaChdrCurr, 9);
	private PackedDecimalData wsaaAmountB4 = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaDeductn = new PackedDecimalData(15, 2).init(ZERO);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private PackedDecimalData wsaaTotWhtax = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotPrmtlr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotCrcard = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotAgadvn = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaCurColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaNewColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotColltr = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTotNetam = new PackedDecimalData(17, 2).init(ZERO);
	private PackedDecimalData wsaaTdcchrg = new PackedDecimalData(14, 2).init(ZERO);
	private FixedLengthStringData wsaaZchrgcde = new FixedLengthStringData(6).init(SPACES);


	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData wsaaBlackList = new FixedLengthStringData(1);
	private Validator commissionSusp = new Validator(wsaaBlackList, "C","X");

	private FixedLengthStringData wsaaStoredAgnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaStoredAgent = new FixedLengthStringData(8).isAPartOf(wsaaStoredAgnt, 0);

	private FixedLengthStringData r5338d01Record = new FixedLengthStringData(109);
	private FixedLengthStringData r5338d01O = new FixedLengthStringData(109).isAPartOf(r5338d01Record, 0);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5338d01O, 0);
	private FixedLengthStringData dtldesc = new FixedLengthStringData(30).isAPartOf(r5338d01O, 10);
	private FixedLengthStringData dtlref = new FixedLengthStringData(30).isAPartOf(r5338d01O, 40);
	private FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(r5338d01O, 70);
	private ZonedDecimalData orgamnt = new ZonedDecimalData(17, 2).isAPartOf(r5338d01O, 72);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(r5338d01O, 89);
	private ZonedDecimalData newamnt = new ZonedDecimalData(17, 2).isAPartOf(r5338d01O, 92);

	private FixedLengthStringData r5338t01Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t01O = new FixedLengthStringData(18).isAPartOf(r5338t01Record, 0);
	private ZonedDecimalData totcbal = new ZonedDecimalData(18, 2).isAPartOf(r5338t01O, 0);

	private FixedLengthStringData r5338t02Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t02O = new FixedLengthStringData(17).isAPartOf(r5338t02Record, 0);
	private ZonedDecimalData taxamt = new ZonedDecimalData(17, 2).isAPartOf(r5338t02O, 0);

	private FixedLengthStringData r5338t03Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t03O = new FixedLengthStringData(17).isAPartOf(r5338t03Record, 0);
	private ZonedDecimalData taxamt1 = new ZonedDecimalData(17, 2).isAPartOf(r5338t03O, 0);

	private FixedLengthStringData r5338t04Record = new FixedLengthStringData(17);
	private FixedLengthStringData r5338t04O = new FixedLengthStringData(17).isAPartOf(r5338t04Record, 0);
	private ZonedDecimalData taxamt2 = new ZonedDecimalData(17, 2).isAPartOf(r5338t04O, 0);

	private FixedLengthStringData r5338t05Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t05O = new FixedLengthStringData(18).isAPartOf(r5338t05Record, 0);
	private ZonedDecimalData accbal = new ZonedDecimalData(18, 2).isAPartOf(r5338t05O, 0);

	private FixedLengthStringData r5338t06Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5338t06O = new FixedLengthStringData(18).isAPartOf(r5338t06Record, 0);
	private ZonedDecimalData accbal1 = new ZonedDecimalData(18, 2).isAPartOf(r5338t06O, 0);
	//Start
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
//END
	private FixedLengthStringData r5338t07Record = new FixedLengthStringData(14);
	private FixedLengthStringData r5338t07O = new FixedLengthStringData(14).isAPartOf(r5338t07Record, 0);
	private ZonedDecimalData tdcchrg = new ZonedDecimalData(14, 2).isAPartOf(r5338t07O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData chdrIndex = new IntegerData();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	
	private T5645rec t5645rec = new T5645rec();
	private T5622rec t5622rec = new T5622rec();
	private Tr392rec tr392rec = new Tr392rec();
	private R5338h01RecordInner r5338h01RecordInner = new R5338h01RecordInner();
	private AcmvpfDAO acmvpfDAO =  getApplicationContext().getBean("acmvDAO", AcmvpfDAO.class);
	private List<Acmvpf> acmvpfList;
	private Acmvpf acmvpf = null;
	
	private AgpypfSearchDAO agpypfDAO =  getApplicationContext().getBean("agpypfsearchDAO", AgpypfSearchDAO.class);

	List<Agpypf> agpypfList=new ArrayList<>();
	private Agpypf agpypf = null;
	
	int stmtcnt = 0;
	int i=0;
 
	private int intBatchID=0;
	private int intBatchExtractSize;	
	private int intBatchStep=0;
	private String strCompany;
	private Iterator<Agpypf> iteratorList;
	private ItemDAO itemDAO =  getApplicationContext().getBean("itemDao", ItemDAO.class);

	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Br50s.class);//IJTI-318
	
	private Agpbpf agpbpf = new Agpbpf(); //ILIFE-8398
	private AgpbpfDAO agpbpfDAO =  getApplicationContext().getBean("agpbpfDAO", AgpbpfDAO.class); //ILIFE-8398
	
	//ILB-580
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private ClntpfDAO clntpfDAO = IntegralApplicationContext.getApplicationContext().getBean("clntpfDAO",ClntpfDAO.class);
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private Aglfpf aglfIO;
	private Descpf descpf;
	private Itempf itempf;
	private Clntpf cltsIO;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		read2240, 
		setAddr3150, 
		exit3190, 
		printCollateral3230, 
		checkDeductible3240, 
		agentAdvance2342, 
		printDeductible3240, 
		print3270, 
		read3420, 
		read3720, 
		exit3890
	}

	public Br50s() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1020();
		setUpHeadingCompany1020();
		readT56451040();
	}
protected void initialise1020(){
	strCompany = bsprIO.getCompany().toString();
	intBatchID = 0;
	intBatchStep = 0;
	agpypfList = new java.util.ArrayList<Agpypf>();
	
	if (bprdIO.systemParam03.isNumeric())
		if (bprdIO.systemParam03.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam03.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	int extractRows = agpypfDAO.populateAgypTemp(strCompany, intBatchExtractSize);
	if (extractRows > 0){
		performDataChunk();	
		if (!agpypfList.isEmpty()){		
			initialise1010();
		}			
	}else{
		 return;
	}

}
protected void initialise1010()
	{
		/* Open required files.*/
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		/* When parameter 1 on the schedule definition is not spaces,*/
		/* clear the AGPB file.*/
		if (isNE(bprdIO.getSystemParam01(), SPACES)) {
			clearAgpbpf1100();
		}
		
	}

protected void setUpHeadingCompany1020(){
	descpf = descDAO.getdescData("IT", "T1693" , bsprIO.getCompany().toString(),"0",bsscIO.getLanguage().toString());
	if (descpf == null) {
		syserrrec.params.set("Item:"+ bsprIO.getCompany().toString()+", table: T1693");
		fatalError600();
	}
	wsaaCompanynm.set(descpf.getLongdesc());
}

protected void readT56451040(){	
	itempf = new Itempf();
    itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl("T5645");
	itempf.setItemitem(wsaaProg.toString());
    itempf = itemDAO.getItempfRecord(itempf);
    if(itempf == null){
    	syserrrec.params.set("Item:"+wsaaProg.toString()+", table: T5645");
    	fatalError600();
    }
    t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}

protected void clearAgpbpf1100(){
	if (isNE(bprdIO.getSystemParam01(), SPACES)) {
		wsaaQcmdexc.set(SPACES);
		wsaaQcmdexc.set("CLRPFM FILE(AGPBPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	}
}

protected void readFile2000(){
	try {
		readFile2010();
	}
	catch (GOTOException e){
	}
}

protected void performDataChunk(){
	intBatchID = intBatchStep;		
	if (agpypfList.size() > 0){
		agpypfList.clear();
	}
	agpypfList = agpypfDAO.loadDataByBatch(intBatchID);
	if (agpypfList.isEmpty())	wsspEdterror.set(varcom.endp);
	else iteratorList = agpypfList.iterator();	
}
protected void readFile2010(){
	if (!agpypfList.isEmpty()){	
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!agpypfList.isEmpty()){			
				agpypf=new Agpypf();
				agpypf = iteratorList.next();
			}
		}else {		
			agpypf = new Agpypf();
			agpypf = iteratorList.next();
		}	
	}else{
		wsspEdterror.set(varcom.endp);
	}
	if (agpypfList.size() > 0  ) {
		//agpypfupdate = new Agpypf();
		agpypf.setRldgacct(agpypf.getRldgacct());
		agpypf.setRldgcoy(agpypf.getRldgcoy());
		agpypf.setRldgacct(agpypf.getRldgacct());
		agpypf.setEffdate(agpypf.getEffdate());
		agpypf.setGlsign(agpypf.getGlsign());
		agpypf.setOrigamt(agpypf.getOrigamt());
		agpypf.setOrigcurr(agpypf.getOrigcurr());
		agpypf.setSacstyp(agpypf.getSacstyp());
		agpypf.setTrandesc(agpypf.getTrandesc());
		agpypf.setRdocnum(agpypf.getRdocnum());
	}
	else
	{
		wsspEdterror.set(varcom.endp);
		return ;
	}
		/*SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(), varcom.oK)
		&& isNE(agpyagtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			syserrrec.params.set(agpyagtIO.getParams());
			fatalError600();
		}*/
		if (isNE(agpypf.getRldgcoy(), bsprIO.getCompany())) {
			if (isNE(wsaaStoredAgent, SPACES)) {
				pageOverflow3100();
				printTotals3200();
			}
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/* Skip Commission Effective Date > BSSC-EFFECTIVE-DATE*/
		if (isGT(agpypf.getEffdate(), bsscIO.getEffectiveDate())) {
			contotrec.totval.set(1);
			contotrec.totno.set(9);
			callContot001();
			wsspEdterror.set("SKIP");
			return ;
		}
		/*  If first time begin*/
		if (isEQ(wsaaStoredAgent, SPACES)) {
			readAglf2100();
			return ;
		}
		/*  If reach different Agent:*/
		if (isNE(agpypf.getRldgacct(), wsaaStoredAgnt)
		&& isNE(wsaaStoredAgnt, SPACES)) {
			pageOverflow3100();
			printTotals3200();
			readAglf2100();
		}
	}

protected void readAglf2100()
	{
		start2110();
		agentTax2140();
		agentCollateralWithheld2160();
	}

protected void start2110()
	{
	wsaaTotOrigAmt.set(ZERO);
	wsaaTotWhtax.set(ZERO);
	wsaaTotNetam.set(ZERO);
	wsaaAmount.set(ZERO);
	wsaaAmountB4.set(ZERO);
	wsaaTotPrmtlr.set(ZERO);
	wsaaChdrCurrArray.set(SPACES);
	wsaaLineCount.set(99);
	wsaaStoredAgnt.set(agpypf.getRldgacct());
	aglfIO = aglfpfDAO.searchAglfRecord(agpypf.getRldgcoy().toString(), wsaaStoredAgent.toString());
	if (null == aglfIO) {
		syserrrec.params.set("Agntcoy:"+bsprIO.getCompany().toString()+",Agntnum:"+aglfIO.getAgntnum()+", Table: Aglfpf");
		fatalError600();
	}
	wsaaBlackList.set(aglfIO.getTagsusind());
	pageOverflow3100();
}

protected void agentTax2140(){
	if (isEQ(aglfIO.getTaxcde(), SPACES)) {
		t5622rec.taxrate.set(ZERO);
		return ;
	}
	List<Itempf> itempfList = itemDAO.getItdmByFrmdate(bsprIO.getCompany().toString(),"T5622",aglfIO.getTaxcde(),bsscIO.getEffectiveDate().toInt());
	if(itempfList.size() > 0) {
		t5622rec.t5622Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}else{
		t5622rec.taxrate.set(ZERO);
	}
	
}

protected void agentCollateralWithheld2160(){
	wsaaCurColltr.set(ZERO);
	acmvpf = new Acmvpf();
	acmvpf.setRldgacct(aglfIO.getAgntnum());
	acmvpf.setRldgcoy(aglfIO.getAgntcoy().charAt(0));
	acmvpf.setSacscode(t5645rec.sacscode03.toString());
	acmvpf.setSacstyp(t5645rec.sacstype03.toString());
	acmvpfList = acmvpfDAO.searchAcmvpfRecord(acmvpf);
	if(acmvpfList !=null && acmvpfList.size() > 0){
		for(Acmvpf acmv : acmvpfList){
			if (isEQ(acmv.getFrcdate(), varcom.vrcmMaxDate)) {
				wsaaCurColltr.add(acmv.getAcctamt().longValue());
			}
		}
	}
}

protected void edit2500(){
	/*EDIT*/
	/*  Check record is required for processing.*/
	/*  Softlock the record if it is to be updated.*/
	stmtcnt++;
	if (isNE(wsspEdterror, "SKIP")) {
		wsspEdterror.set(varcom.oK);
	}
	else {
		//agpyagtIO.setFunction("NEXTR");
		wsspEdterror.set(SPACES);
	}
	/*EXIT*/
}

protected void update3000()
	{
		update3010();
		writeDetail3040();
	}

protected void update3010()
	{
		/*  Checking for page overflow*/
		pageOverflow3100();
		/* Update database records.*/
		if (isEQ(agpypf.getGlsign(), "+")) {
			compute(wsaaAmount, 2).set(mult(agpypf.getOrigamt(), -1));
		}
		else {
			wsaaAmount.set(agpypf.getOrigamt());
		}
		/* Set up parameters for Currency conversion*/
		if (isNE(agpypf.getOrigcurr(), aglfIO.getCurrcode()) && isNE(aglfIO.getCurrcode(), SPACES)) {//smalchi2 for ILIFE-2180
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.company.set(agpypf.getRldgcoy());
			conlinkrec.cashdate.set(bsscIO.getEffectiveDate());
			conlinkrec.currIn.set(agpypf.getOrigcurr());
			conlinkrec.currOut.set(aglfIO.getCurrcode());
			conlinkrec.amountIn.set(wsaaAmount);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("CVRT");
			Xcvrt.getInstance().mainline(conlinkrec.function);
			if (isEQ(conlinkrec.statuz, "BOMB")) {
				syserrrec.statuz.set(conlinkrec.statuz);
				syserrrec.params.set(conlinkrec.clnk002Rec);
				fatalError600();
			}
			/*       MOVE CLNK-AMOUNT-OUT     TO ZRDP-AMOUNT-IN                */
			/*       PERFORM A000-CALL-ROUNDING                                */
			/*       MOVE ZRDP-AMOUNT-OUT     TO CLNK-AMOUNT-OUT               */
			/*                                   NEWAMNT OF R5338D01-O         */
			if (isNE(conlinkrec.amountOut, ZERO)) {
				zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());
				zrdecplcPojo = a000CallRounding(zrdecplcPojo);
				conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());
				newamnt.set(zrdecplcPojo.getAmountOut());
			}
			else {
				newamnt.set(ZERO);
			}
			wsaaTotOrigAmt.add(conlinkrec.amountOut);
		}
		else {
			wsaaTotOrigAmt.add(wsaaAmount);
			newamnt.set(wsaaAmount);
		}
		sacstyp.set(agpypf.getSacstyp());
		dtldesc.set(agpypf.getTrandesc());
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(agpypf.getEffdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			getAppVars().addDiagnostic("DOCNO = "+agpypf.getRdocnum());
			getAppVars().addDiagnostic("AGTNO = "+agpypf.getRldgacct());
			getAppVars().addDiagnostic("EFFDATE = "+agpypf.getEffdate());
			getAppVars().addDiagnostic("ORGAMT  = "+agpypf.getOrigamt());
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		effdate.set(datcon1rec.extDate);
		origcurr.set(agpypf.getOrigcurr());
		dtlref.set(agpypf.getRdocnum());
		orgamnt.set(wsaaAmount);
		/*                                NEWAMNT OF R5338D01-O.        */
		sacstyp.set(agpypf.getSacstyp());
		contotrec.totno.set(2);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeDetail3040()
	{
		//System.out.println("value is :" +agpypf.getRdocnum());//IJTI-318
		LOGGER.info("value is : ", agpypf.getRdocnum());//IJTI-1561
		printerFile.printR5338d01(r5338d01Record, indicArea);
		wsaaLineCount.add(1);
		/*KEEP-CHDR-CURR*/
		chdrIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(chdrIndex, wsaaChdrCurr.length); chdrIndex.add(1)){
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()], SPACES)) {
					wsaaChdrnum[chdrIndex.toInt()].set(agpypf.getRdocnum());
					wsaaCntcurr[chdrIndex.toInt()].set(agpypf.getOrigcurr());
					break searchlabel1;
				}
				if (isEQ(wsaaChdrnum[chdrIndex.toInt()].toString(), agpypf.getRdocnum())
				&& isEQ(wsaaCntcurr[chdrIndex.toInt()].toString(), agpypf.getOrigcurr())) {
					/*NEXT_SENTENCE*/
					break searchlabel1;
				}
			}
		}
		i++;
		/*NEXT-AGPYAGT*/
		//agpyagtIO.setFunction("NEXTR");
		/*EXIT*/
	}

protected void pageOverflow3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3100();
				case setAddr3150: 
					setAddr3150();
					printR5338h013180();
				case exit3190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3100()
	{
		if (isLT(wsaaLineCount, wsaaPageSize)) {
			goTo(GotoLabel.exit3190);
		}
		printerRec.set(SPACES);
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(bsscIO.getEffectiveDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		r5338h01RecordInner.stdat.set(datcon1rec.extDate);
		r5338h01RecordInner.agnt.set(aglfIO.getAgntnum());
		r5338h01RecordInner.tagsusind.set(aglfIO.getTagsusind());
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		if (isEQ(aglfIO.getTagsusind(), "I")){
			indOn.setTrue(1);
		}
		else if (isEQ(aglfIO.getTagsusind(), "C")){
			indOn.setTrue(2);
		}
		else if (isEQ(aglfIO.getTagsusind(), "X")){
			indOn.setTrue(3);
		}
		r5338h01RecordInner.company.set(bsprIO.getCompany());
		r5338h01RecordInner.companynm.set(wsaaCompanynm);

		Agntpf agntIO=agntpfDAO.getClientData("AG",bsprIO.getCompany().toString(), aglfIO.getAgntnum());
		if (agntIO == null) {
			syserrrec.params.set("Agntcoy:"+bsprIO.getCompany().toString()+",Agntnum:"+aglfIO.getAgntnum()+", Table: Agntpf");
			fatalError600();
		}
		
		cltsIO = clntpfDAO.searchClntRecord("CN",agntIO.getClntcoy(), agntIO.getClntnum());
		if (cltsIO == null) {
			syserrrec.params.set("Clntcoy:"+agntIO.getClntcoy()+",Clntnum:"+agntIO.getClntnum()+", Table: CLNTPF");
			fatalError600();
		}
		plainname();
		r5338h01RecordInner.forattn.set(cltsIO.getForAttOf());
		/*    If statement currency empty initialise report field,*/
		/*    otherwise use it to get description to be printed in top*/
		/*    left hand corner of report.*/
		if (isEQ(aglfIO.getCurrcode(), SPACES)) {
			r5338h01RecordInner.currdesc.set(SPACES);
			goTo(GotoLabel.setAddr3150);
		}
		
		descpf = descDAO.getdescData("IT", "T3629" , aglfIO.getCurrcode(),bsprIO.getCompany().toString(),bsscIO.getLanguage().toString());
		if (descpf == null) {
			r5338h01RecordInner.currdesc.set(SPACES);
		}else{
			r5338h01RecordInner.currdesc.set(descpf.getLongdesc());
		}
	}

protected void setAddr3150()
{
	try
	{
		r5338h01RecordInner.statcurr.set(aglfIO.getCurrcode());
		r5338h01RecordInner.agntname.set(wsspLongconfname);
		r5338h01RecordInner.addr1.set(cltsIO.getCltaddr01());
		r5338h01RecordInner.addr2.set(cltsIO.getCltaddr02());
		r5338h01RecordInner.addr3.set(cltsIO.getCltaddr03());
		r5338h01RecordInner.addr4.set(cltsIO.getCltaddr04());
		r5338h01RecordInner.pcode.set(cltsIO.getCltpcode());
		contotrec.totno.set(4);
		contotrec.totval.set(1);
		callContot001();
	}
	catch(Exception ex)
	{
		
	}
}

protected void printR5338h013180()
	{
	try
	{
		printerFile.printR5338h01(r5338h01RecordInner.r5338h01Record, indicArea);
		wsaaLineCount.set(16);
	}
	catch(Exception ex)
	{
		
	}
}

protected void largename()
	{
		/*LGNM-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspLongconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspLongconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspLongconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspLongconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspLongconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void printTotals3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para3210();
					printTax3220();
				case printCollateral3230: 
					printCollateral3230();
				case checkDeductible3240: 
					checkDeductible3240();
				case agentAdvance2342: 
					agentAdvance2342();
				case printDeductible3240: 
					printDeductible3240();
					printBankCharge3250();
					printTotBal3260();
				case print3270: 
					print3270();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3210()
	{
		contotrec.totno.set(1);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(3);
		contotrec.totval.set(wsaaTotOrigAmt);
		callContot001();
		if (commissionSusp.isTrue()) {
			contotrec.totno.set(6);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
		}
		else {
			contotrec.totno.set(5);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
		}
		if (isNE(wsaaTotOrigAmt, ZERO)) {
			wsaaLineCount.add(3);
			if (isGT(wsaaLineCount, wsaaPageSize)) {
				pageOverflow3100();
				wsaaLineCount.add(3);
			}
			printerRec.set(SPACES);
			totcbal.set(wsaaTotOrigAmt);
			wsaaTotNetam.set(wsaaTotOrigAmt);
			printerFile.printR5338t01(r5338t01Record, indicArea);
		}
		wsaaTotColltr.set(ZERO);
		wsaaTotAgadvn.set(ZERO);
		wsaaTotCrcard.set(ZERO);
	}

protected void printTax3220()
	{
		if (isEQ(t5622rec.taxrate, ZERO)) {
			goTo(GotoLabel.printCollateral3230);
		}
		compute(wsaaTotWhtax, 3).setRounded(div(mult(wsaaTotOrigAmt, t5622rec.taxrate), 100));
		/*    MOVE WSAA-TOT-WHTAX         TO ZRDP-AMOUNT-IN.               */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO WSAA-TOT-WHTAX.               */
		if (isNE(wsaaTotWhtax, ZERO)) {
			zrdecplcPojo.setAmountIn(wsaaTotWhtax.getbigdata());
			zrdecplcPojo = a000CallRounding(zrdecplcPojo);;
			wsaaTotWhtax.set(zrdecplcPojo.getAmountOut());
		}
		wsaaLineCount.add(1);
		if (isGT(wsaaLineCount, wsaaPageSize)) {
			pageOverflow3100();
			wsaaLineCount.add(1);
		}
		printerRec.set(SPACES);
		compute(taxamt, 2).set(mult(wsaaTotWhtax, (-1)));
		printerFile.printR5338t02(r5338t02Record, indicArea);
		compute(wsaaTotNetam, 2).set(sub(wsaaTotOrigAmt, wsaaTotWhtax));
	}

protected void printCollateral3230()
	{
		if (isEQ(aglfIO.getTcolprct(), ZERO)) {
			goTo(GotoLabel.checkDeductible3240);
		}
		compute(wsaaTotColltr, 3).setRounded(div(mult(wsaaTotOrigAmt, aglfIO.getTcolprct()), 100));
		/*    MOVE WSAA-TOT-COLLTR        TO ZRDP-AMOUNT-IN.               */
		/*    PERFORM A000-CALL-ROUNDING.                                  */
		/*    MOVE ZRDP-AMOUNT-OUT        TO WSAA-TOT-COLLTR.              */
		if (isNE(wsaaTotColltr, ZERO)) {
			zrdecplcPojo.setAmountIn(wsaaTotColltr.getbigdata());
			zrdecplcPojo = a000CallRounding(zrdecplcPojo);;
			wsaaTotColltr.set(zrdecplcPojo.getAmountOut());
		}
		if (isNE(aglfIO.getTcolmax(), ZERO)) {
			if (isGT(aglfIO.getTcolmax(), wsaaCurColltr)) {
				compute(wsaaNewColltr, 2).set(add(wsaaCurColltr, wsaaTotColltr));
				if (isGT(wsaaNewColltr, aglfIO.getTcolmax())) {
					compute(wsaaTotColltr, 2).set(sub(aglfIO.getTcolmax(), wsaaCurColltr));
				}
			}
			else {
				goTo(GotoLabel.checkDeductible3240);
			}
		}
		wsaaLineCount.add(1);
		if (isGT(wsaaLineCount, wsaaPageSize)) {
			pageOverflow3100();
			wsaaLineCount.add(1);
		}
		printerRec.set(SPACES);
		compute(taxamt2, 2).set(mult(wsaaTotColltr, (-1)));
		printerFile.printR5338t04(r5338t04Record, indicArea);
		compute(wsaaTotNetam, 2).set(sub(wsaaTotNetam, wsaaTotColltr));
		wsaaLineCount.add(3);
		if (isGT(wsaaLineCount, wsaaPageSize)) {
			pageOverflow3100();
			wsaaLineCount.add(3);
		}
		printerRec.set(SPACES);
		accbal1.set(wsaaTotNetam);
		printerFile.printR5338t06(r5338t06Record);
	}

protected void checkDeductible3240()
	{
		wsaaAmountB4.set(wsaaTotNetam);
		//Ticket #ILIFE-763  L2NEWAGTPY job aborted start
		for (chdrIndex.set(1); !(isGT(chdrIndex, 90)
		|| isEQ(wsaaChdrnum[chdrIndex.toInt()], SPACES)); chdrIndex.add(1)){
			//Ticket #ILIFE-763  L2NEWAGTPY job aborted end
			getPremiumTolerance3400();
		}
		/* Reserve for Agent Deduction*/
		if (isEQ(t5645rec.sacscode07, SPACES)) {
			goTo(GotoLabel.agentAdvance2342);
		}
		wsaaSacscode.set(t5645rec.sacscode07);
		wsaaSacstyp.set(t5645rec.sacstype07);
		wsaaTotNetam.set(wsaaAmountB4);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction3700();
		wsaaTotCrcard.set(wsaaDeductn);
	}

protected void agentAdvance2342()
	{
		/* Reserve for Agent Advance Payment*/
		if (isEQ(t5645rec.sacscode08, SPACES)) {
			goTo(GotoLabel.printDeductible3240);
		}
		wsaaSacscode.set(t5645rec.sacscode08);
		wsaaSacstyp.set(t5645rec.sacstype08);
		wsaaTotNetam.set(wsaaAmountB4);
		wsaaDeductn.set(ZERO);
		getPremiumDeduction3700();
		wsaaTotAgadvn.set(wsaaDeductn);
		wsaaTotNetam.set(wsaaAmountB4);
	}

protected void printDeductible3240()
	{
		if (isNE(wsaaTotPrmtlr, ZERO)
		|| isNE(wsaaTotCrcard, ZERO)
		|| isNE(wsaaTotAgadvn, ZERO)) {
			wsaaLineCount.add(2);
			if (isGT(wsaaLineCount, wsaaPageSize)) {
				pageOverflow3100();
				wsaaLineCount.add(2);
			}
			printerRec.set(SPACES);
			compute(taxamt1, 2).set(mult((add(add(wsaaTotPrmtlr, wsaaTotCrcard), wsaaTotAgadvn)), (-1)));
			printerFile.printR5338t03(r5338t03Record, indicArea);
		}
	}

protected void printBankCharge3250()
	{
		if (isNE(aglfIO.getPaymth(), "DC")) {
			wsaaTdcchrg.set(ZERO);
		}
		else {
			getBankCharge3800();
		}
		if (isNE(wsaaTdcchrg, ZERO)) {
			wsaaLineCount.add(2);
			if (isGT(wsaaLineCount, wsaaPageSize)) {
				pageOverflow3100();
				wsaaLineCount.add(2);
			}
			printerRec.set(SPACES);
			//tdcchrg.set(wsaaTdcchrg);                                   //MIBT-340
			compute(tdcchrg,2).set((mult(wsaaTdcchrg, -1)));            //MIBT-340 
            printerFile.printR5338t07(r5338t07Record);
			compute(wsaaTotNetam, 2).set(sub(wsaaTotNetam, wsaaTdcchrg));
		}
	}

protected void printTotBal3260()
	{
		wsaaLineCount.add(2);
		if (commissionSusp.isTrue()) {
			goTo(GotoLabel.print3270);
		}
		if (isLT(wsaaTotOrigAmt, aglfIO.getMinsta())) {
			indOn.setTrue(4);
			/*  Number of Agent with nopay                                     */
			contotrec.totno.set(7);
			contotrec.totval.set(1);
			callContot001();
			/*  Total un-paid amount                                           */
			contotrec.totno.set(8);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
		}
		else {
			indOff.setTrue(4);
			/*  Total can paid amount                                          */
			contotrec.totno.set(11);
			contotrec.totval.set(wsaaTotOrigAmt);
			callContot001();
			/**     END-IF                                                      */
		}
	}

protected void print3270()
	{
		if (isGT(wsaaLineCount, wsaaPageSize)) {
			pageOverflow3100();
			wsaaLineCount.add(2);
			/**     END-IF                                                      */
		}
		printerRec.set(SPACES);
		accbal.set(wsaaTotNetam);
		printerFile.printR5338t05(r5338t05Record, indicArea);
		wsaaLineCount.set(99);
		/*UPDATE-AGPB*/
		if (isNE(bprdIO.getSystemParam01(), SPACES)) {
			updateAgpb3300();
		}
		/*EXIT*/
	}

protected void updateAgpb3300(){
	 //ILIFE-8398 start 	 
	agpbpf.setRldgcoy(aglfIO.getAgntcoy());
	agpbpf.setRldgacct(aglfIO.getAgntnum());
	agpbpf.setCurrcode(aglfIO.getCurrcode());
	agpbpf.setSacscurbal(wsaaTotOrigAmt.getbigdata());
	agpbpf.setTagsusind(aglfIO.getTagsusind());
	agpbpf.setTaxcde(aglfIO.getTaxcde());
	agpbpf.setCurrcode(aglfIO.getCurrcode());
	agpbpf.setAgccqind(aglfIO.getAgccqind());
	agpbpf.setPayclt(aglfIO.getPayclt());
	agpbpf.setFacthous(aglfIO.getFacthous());
	agpbpf.setBankkey(aglfIO.getBankkey());
	agpbpf.setBankacckey(aglfIO.getBankacckey());
	agpbpf.setPaymth(aglfIO.getPaymth());
	agpbpf.setTdcchrg(wsaaTdcchrg.getbigdata());
	agpbpf.setWhtax(wsaaTotWhtax.getbigdata());
	agpbpf.setTcolbal(wsaaTotColltr.getbigdata());
	setPrecision(agpbpf.getTdeduct(), 2);
	agpbpf.setTdeduct((add(add(wsaaTotPrmtlr, wsaaTotCrcard), wsaaTotAgadvn)).getbigdata());
	
	agpbpfDAO.insertAgpbpfRecord(agpbpf);
	if (isGT(agpbpf.getSacscurbal(), aglfIO.getMinsta())) {
		agpbpf.setProcflg("Y");
	}
	else {
		agpbpf.setProcflg("N");
	}
	if(agpbpf == null) {
		fatalError600();
	}
 //ILIFE-8398 end
	contotrec.totno.set(10);
	contotrec.totval.set(1);
	callContot001();
}


protected void getPremiumTolerance3400(){
	acmvpf = new Acmvpf();
	acmvpf.setRldgacct(aglfIO.getAgntnum());
	acmvpf.setRldgcoy(bsprIO.getCompany().charat(0));
	acmvpf.setSacscode(t5645rec.sacscode09.toString());
	acmvpf.setSacstyp(t5645rec.sacstype09.toString());
	
	acmvpfList = acmvpfDAO.searchAcmvpfRecord(acmvpf);
	
	if(acmvpfList !=null && acmvpfList.size() > 0)
	{
		for(Acmvpf acmv : acmvpfList)
		{
			if (isEQ(acmv.getFrcdate(), varcom.vrcmMaxDate)) {
				if (isLT(wsaaAmountB4, acmv.getOrigamt())) {
					 
				}
				if (isEQ(acmv.getRldgacct(), aglfIO.getAgntnum())) {
					wsaaTotPrmtlr.add(acmv.getOrigamt().longValue());
					wsaaAmountB4.subtract(acmv.getOrigamt().longValue());
				}
			}
		}
	}
}
protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void getPremiumDeduction3700(){
	acmvpf = new Acmvpf();
	acmvpf.setRldgacct(aglfIO.getAgntnum());
	acmvpf.setRldgcoy(bsprIO.getCompany().charat(0));
	acmvpf.setSacscode(wsaaSacscode.toString());
	acmvpf.setSacstyp(wsaaSacstyp.toString());
	
	acmvpfList = acmvpfDAO.searchAcmvpfRecord(acmvpf);
	
	if(acmvpfList !=null && acmvpfList.size() > 0){
		for(Acmvpf acmv : acmvpfList){
			if (isLTE(wsaaAmountB4, 0)) {
						return ;
				}
			if (isEQ(acmv.getFrcdate(), varcom.vrcmMaxDate)) {
				if (isGTE(wsaaAmountB4, acmv.getOrigamt())) {
					wsaaDeductn.add(acmv.getOrigamt().longValue());
					wsaaAmountB4.subtract(acmv.getOrigamt().longValue());
				}
			}
		}
	}
	 
}
protected void getBankCharge3800(){
	try {
		start3810();
		read3820();
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void start3810(){
	 Babrpf babrIO = babrpfDao.searchBabrpfByBankkey(aglfIO.getBankkey());
	 if (null == babrIO) {
		 syserrrec.params.set("Bankkey:"+ aglfIO.getBankkey()+", table: babrpf ");
		 fatalError600();
	 }
	 if (isEQ(babrIO.getFacthous01(), aglfIO.getFacthous())
	 || isEQ(babrIO.getFacthous02(), aglfIO.getFacthous())
	 || isEQ(babrIO.getFacthous03(), aglfIO.getFacthous())
	 || isEQ(babrIO.getFacthous04(), aglfIO.getFacthous())
	 || isEQ(babrIO.getFacthous05(), aglfIO.getFacthous())) {
		/*NEXT_SENTENCE*/
	 }else {
		wsaaTdcchrg.set(ZERO);
		goTo(GotoLabel.exit3890);
	 }
	 if (isEQ(babrIO.getFacthous01(), aglfIO.getFacthous())) {
		wsaaZchrgcde.set(babrIO.getZchrgcde01());
	 }
	 if (isEQ(babrIO.getFacthous02(), aglfIO.getFacthous())) {
		wsaaZchrgcde.set(babrIO.getZchrgcde02());
	 }
	 if (isEQ(babrIO.getFacthous03(), aglfIO.getFacthous())) {
		wsaaZchrgcde.set(babrIO.getZchrgcde03());
	 }
	if (isEQ(babrIO.getFacthous04(), aglfIO.getFacthous())) {
		wsaaZchrgcde.set(babrIO.getZchrgcde04());
	}
	if (isEQ(babrIO.getFacthous05(), aglfIO.getFacthous())) {
		wsaaZchrgcde.set(babrIO.getZchrgcde05());
	}
	if (isEQ(wsaaZchrgcde, SPACES)) {
		goTo(GotoLabel.exit3890);
	}
}

protected void read3820(){
	itempf = new Itempf();
    itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl("TR392");
	itempf.setItemitem(wsaaZchrgcde.toString());
    itempf = itemDAO.getItempfRecord(itempf);
    if(itempf == null){
    	syserrrec.params.set("Item:"+wsaaZchrgcde.toString()+", table: TR392");
    	fatalError600();
    }
    tr392rec.tr392Rec.set(StringUtil.rawToString(itempf.getGenarea()));
    wsaaTdcchrg.set(tr392rec.tdcchrg);
}

protected void close4000(){
	/*CLOSE-FILES*/
	/*  Close any open files.*/
	printerFile.close();
	lsaaStatuz.set(varcom.oK);
	/*EXIT*/
}

protected ZrdecplcPojo a000CallRounding(ZrdecplcPojo zrdecplcPojo){
	zrdecplcPojo.setFunction(SPACES.toString());
	zrdecplcPojo.setCompany(bsprIO.getCompany().toString());
	zrdecplcPojo.setStatuz("****");
	zrdecplcPojo.setCurrency(aglfIO.getCurrcode());
	zrdecplcPojo.setBatctrcde(bprdIO.getAuthCode().toString());
	zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
	if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
		syserrrec.statuz.set(zrdecplcPojo.getStatuz());
		fatalError600();
	}
	return zrdecplcPojo;
}

/*
 * Class transformed  from Data Structure R5338H01-RECORD--INNER
 */
private static final class R5338h01RecordInner { 

		/*   Main, standard page headings*/
	private FixedLengthStringData r5338h01Record = new FixedLengthStringData(153+DD.cltaddr.length*4);/*pmujavadiya ILIFE-3212*/ /*ILIFE-6379*/
	private FixedLengthStringData r5338h01O = new FixedLengthStringData(153+DD.cltaddr.length*4).isAPartOf(r5338h01Record, 0);
	private FixedLengthStringData stdat = new FixedLengthStringData(10).isAPartOf(r5338h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5338h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5338h01O, 11);
	private FixedLengthStringData agnt = new FixedLengthStringData(8).isAPartOf(r5338h01O, 41);
	private FixedLengthStringData forattn = new FixedLengthStringData(30).isAPartOf(r5338h01O, 49);
	private FixedLengthStringData statcurr = new FixedLengthStringData(3).isAPartOf(r5338h01O, 79);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(r5338h01O, 82);
	private FixedLengthStringData agntname = new FixedLengthStringData(30).isAPartOf(r5338h01O, 112);
	private FixedLengthStringData tagsusind = new FixedLengthStringData(1).isAPartOf(r5338h01O, 142);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143);/*pmujavadiya ILIFE-3212*/ /*ILIFE-6379*/
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(r5338h01O, 143+DD.cltaddr.length*3);
	private FixedLengthStringData pcode = new FixedLengthStringData(10).isAPartOf(r5338h01O, 143+DD.cltaddr.length*4);/*pmujavadiya ILIFE-3212*/ /*ILIFE-6379*/
}
}
