package com.csc.life.agents.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RM505.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rm505Report extends SMARTReportLayout { 

	private FixedLengthStringData agntbr = new FixedLengthStringData(2);
	private FixedLengthStringData agntname01 = new FixedLengthStringData(30);
	private FixedLengthStringData agntname02 = new FixedLengthStringData(30);
	private FixedLengthStringData agntnum01 = new FixedLengthStringData(8);
	private FixedLengthStringData agntnum02 = new FixedLengthStringData(8);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData datefrm = new FixedLengthStringData(10);
	private FixedLengthStringData dateto = new FixedLengthStringData(10);
	private ZonedDecimalData numofmbr = new ZonedDecimalData(7, 0);
	private ZonedDecimalData numpols = new ZonedDecimalData(4, 0);
	private ZonedDecimalData premium01 = new ZonedDecimalData(13, 2);
	private ZonedDecimalData premium02 = new ZonedDecimalData(13, 2);
	private FixedLengthStringData rpthead = new FixedLengthStringData(50);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData statdesc = new FixedLengthStringData(24);
	private FixedLengthStringData statuz = new FixedLengthStringData(4);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rm505Report() {
		super();
	}


	/**
	 * Print the XML for Rm505d01
	 */
	public void printRm505d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		numpols.setFieldName("numpols");
		numpols.setInternal(subString(recordData, 1, 4));
		agntnum01.setFieldName("agntnum01");
		agntnum01.setInternal(subString(recordData, 5, 8));
		agntname01.setFieldName("agntname01");
		agntname01.setInternal(subString(recordData, 13, 30));
		agntbr.setFieldName("agntbr");
		agntbr.setInternal(subString(recordData, 43, 2));
		agntnum02.setFieldName("agntnum02");
		agntnum02.setInternal(subString(recordData, 45, 8));
		agntname02.setFieldName("agntname02");
		agntname02.setInternal(subString(recordData, 53, 30));
		numofmbr.setFieldName("numofmbr");
		numofmbr.setInternal(subString(recordData, 83, 7));
		premium01.setFieldName("premium01");
		premium01.setInternal(subString(recordData, 90, 13));
		premium02.setFieldName("premium02");
		premium02.setInternal(subString(recordData, 103, 13));
		statuz.setFieldName("statuz");
		statuz.setInternal(subString(recordData, 116, 4));
		printLayout("Rm505d01",			// Record name
			new BaseData[]{			// Fields:
				numpols,
				agntnum01,
				agntname01,
				agntbr,
				agntnum02,
				agntname02,
				numofmbr,
				premium01,
				premium02,
				statuz
			}
		);

	}

	/**
	 * Print the XML for Rm505d02
	 */
	public void printRm505d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		agntnum01.setFieldName("agntnum01");
		agntnum01.setInternal(subString(recordData, 1, 8));
		agntname01.setFieldName("agntname01");
		agntname01.setInternal(subString(recordData, 9, 30));
		agntbr.setFieldName("agntbr");
		agntbr.setInternal(subString(recordData, 39, 2));
		agntnum02.setFieldName("agntnum02");
		agntnum02.setInternal(subString(recordData, 41, 8));
		agntname02.setFieldName("agntname02");
		agntname02.setInternal(subString(recordData, 49, 30));
		numofmbr.setFieldName("numofmbr");
		numofmbr.setInternal(subString(recordData, 79, 7));
		premium01.setFieldName("premium01");
		premium01.setInternal(subString(recordData, 86, 13));
		premium02.setFieldName("premium02");
		premium02.setInternal(subString(recordData, 99, 13));
		statuz.setFieldName("statuz");
		statuz.setInternal(subString(recordData, 112, 4));
		printLayout("Rm505d02",			// Record name
			new BaseData[]{			// Fields:
				agntnum01,
				agntname01,
				agntbr,
				agntnum02,
				agntname02,
				numofmbr,
				premium01,
				premium02,
				statuz
			}
		);

	}

	/**
	 * Print the XML for Rm505h01
	 */
	public void printRm505h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		rpthead.setFieldName("rpthead");
		rpthead.setInternal(subString(recordData, 1, 50));
		datefrm.setFieldName("datefrm");
		datefrm.setInternal(subString(recordData, 51, 10));
		dateto.setFieldName("dateto");
		dateto.setInternal(subString(recordData, 61, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 71, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 72, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 102, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 112, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 114, 30));
		printLayout("Rm505h01",			// Record name
			new BaseData[]{			// Fields:
				rpthead,
				datefrm,
				dateto,
				company,
				companynm,
				sdate,
				branch,
				branchnm
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rm505s01
	 */
	public void printRm505s01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		statdesc.setFieldName("statdesc");
		statdesc.setInternal(subString(recordData, 1, 24));
		numofmbr.setFieldName("numofmbr");
		numofmbr.setInternal(subString(recordData, 25, 7));
		premium01.setFieldName("premium01");
		premium01.setInternal(subString(recordData, 32, 13));
		premium02.setFieldName("premium02");
		premium02.setInternal(subString(recordData, 45, 13));
		printLayout("Rm505s01",			// Record name
			new BaseData[]{			// Fields:
				statdesc,
				numofmbr,
				premium01,
				premium02
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rm505s02
	 */
	public void printRm505s02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		numofmbr.setFieldName("numofmbr");
		numofmbr.setInternal(subString(recordData, 1, 7));
		premium01.setFieldName("premium01");
		premium01.setInternal(subString(recordData, 8, 13));
		premium02.setFieldName("premium02");
		premium02.setInternal(subString(recordData, 21, 13));
		printLayout("Rm505s02",			// Record name
			new BaseData[]{			// Fields:
				numofmbr,
				premium01,
				premium02
			}
		);

		currentPrintLine.add(3);
	}


}
