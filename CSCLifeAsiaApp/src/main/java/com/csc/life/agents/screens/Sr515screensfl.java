package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr515screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 36;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {21, 20, 22}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {10, 21, 4, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr515ScreenVars sv = (Sr515ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr515screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr515screensfl, 
			sv.Sr515screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr515ScreenVars sv = (Sr515ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr515screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr515ScreenVars sv = (Sr515ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr515screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr515screensflWritten.gt(0))
		{
			sv.sr515screensfl.setCurrentIndex(0);
			sv.Sr515screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr515ScreenVars sv = (Sr515ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr515screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr515ScreenVars screenVars = (Sr515ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dteappDisp.setFieldName("dteappDisp");
				screenVars.zrreptp.setFieldName("zrreptp");
				screenVars.deit.setFieldName("deit");
				screenVars.prcnt.setFieldName("prcnt");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.dteappDisp.set(dm.getField("dteappDisp"));
			screenVars.zrreptp.set(dm.getField("zrreptp"));
			screenVars.deit.set(dm.getField("deit"));
			screenVars.prcnt.set(dm.getField("prcnt"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr515ScreenVars screenVars = (Sr515ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dteappDisp.setFieldName("dteappDisp");
				screenVars.zrreptp.setFieldName("zrreptp");
				screenVars.deit.setFieldName("deit");
				screenVars.prcnt.setFieldName("prcnt");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("dteappDisp").set(screenVars.dteappDisp);
			dm.getField("zrreptp").set(screenVars.zrreptp);
			dm.getField("deit").set(screenVars.deit);
			dm.getField("prcnt").set(screenVars.prcnt);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr515screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr515ScreenVars screenVars = (Sr515ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.dteappDisp.clearFormatting();
		screenVars.zrreptp.clearFormatting();
		screenVars.deit.clearFormatting();
		screenVars.prcnt.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr515ScreenVars screenVars = (Sr515ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.zrreptp.setClassString("");
		screenVars.deit.setClassString("");
		screenVars.prcnt.setClassString("");
	}

/**
 * Clear all the variables in Sr515screensfl
 */
	public static void clear(VarModel pv) {
		Sr515ScreenVars screenVars = (Sr515ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.zrreptp.clear();
		screenVars.deit.clear();
		screenVars.prcnt.clear();
	}
}
