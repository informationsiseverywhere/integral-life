/*
 * File: T5695pt.java
 * Date: 30 August 2009 2:26:33
 * Author: Quipoz Limited
 * 
 * Class transformed from T5695PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.T5695rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5695.
*
*
*****************************************************************
* </pre>
*/
public class T5695pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(74);
	private FixedLengthStringData filler1 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 25, FILLER).init("Commission Deferment Payment Pattern        S5695");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(73);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 43);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 23, FILLER).init("  Valid to:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler9 = new FixedLengthStringData(73).isAPartOf(wsaaPrtLine004, 0, FILLER).init("   Freq                   %                   Max %       Earned");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(76);
	private FixedLengthStringData filler10 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" From  To             Commission             Premium    Commission %");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 2);
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 7);
	private FixedLengthStringData filler13 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 2);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 7);
	private FixedLengthStringData filler18 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 2);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 7);
	private FixedLengthStringData filler23 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine008, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 2);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 7);
	private FixedLengthStringData filler28 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine009, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine009, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 2);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 7);
	private FixedLengthStringData filler33 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine010, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine010, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 2);
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 7);
	private FixedLengthStringData filler38 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine011, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine011, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 2);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 7);
	private FixedLengthStringData filler43 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine012, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine012, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 2);
	private FixedLengthStringData filler47 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 7);
	private FixedLengthStringData filler48 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine013, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine013, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 2);
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 7);
	private FixedLengthStringData filler53 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine014, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine014, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 2);
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 7);
	private FixedLengthStringData filler58 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine015, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine015, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 67).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(73);
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 2);
	private FixedLengthStringData filler62 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 7);
	private FixedLengthStringData filler63 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine016, 9, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 24).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine016, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 46).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine016, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 67).setPattern("ZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5695rec t5695rec = new T5695rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5695pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5695rec.t5695Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5695rec.freqFrom01);
		fieldNo008.set(t5695rec.freqTo01);
		fieldNo009.set(t5695rec.cmrate01);
		fieldNo010.set(t5695rec.premaxpc01);
		fieldNo011.set(t5695rec.commenpc01);
		fieldNo012.set(t5695rec.freqFrom02);
		fieldNo013.set(t5695rec.freqTo02);
		fieldNo014.set(t5695rec.cmrate02);
		fieldNo015.set(t5695rec.premaxpc02);
		fieldNo016.set(t5695rec.commenpc02);
		fieldNo017.set(t5695rec.freqFrom03);
		fieldNo018.set(t5695rec.freqTo03);
		fieldNo019.set(t5695rec.cmrate03);
		fieldNo020.set(t5695rec.premaxpc03);
		fieldNo021.set(t5695rec.commenpc03);
		fieldNo022.set(t5695rec.freqFrom04);
		fieldNo023.set(t5695rec.freqTo04);
		fieldNo024.set(t5695rec.cmrate04);
		fieldNo025.set(t5695rec.premaxpc04);
		fieldNo026.set(t5695rec.commenpc04);
		fieldNo027.set(t5695rec.freqFrom05);
		fieldNo028.set(t5695rec.freqTo05);
		fieldNo029.set(t5695rec.cmrate05);
		fieldNo030.set(t5695rec.premaxpc05);
		fieldNo031.set(t5695rec.commenpc05);
		fieldNo032.set(t5695rec.freqFrom06);
		fieldNo033.set(t5695rec.freqTo06);
		fieldNo034.set(t5695rec.cmrate06);
		fieldNo035.set(t5695rec.premaxpc06);
		fieldNo036.set(t5695rec.commenpc06);
		fieldNo037.set(t5695rec.freqFrom07);
		fieldNo038.set(t5695rec.freqTo07);
		fieldNo039.set(t5695rec.cmrate07);
		fieldNo040.set(t5695rec.premaxpc07);
		fieldNo041.set(t5695rec.commenpc07);
		fieldNo042.set(t5695rec.freqFrom08);
		fieldNo043.set(t5695rec.freqTo08);
		fieldNo044.set(t5695rec.cmrate08);
		fieldNo045.set(t5695rec.premaxpc08);
		fieldNo046.set(t5695rec.commenpc08);
		fieldNo047.set(t5695rec.freqFrom09);
		fieldNo048.set(t5695rec.freqTo09);
		fieldNo049.set(t5695rec.cmrate09);
		fieldNo050.set(t5695rec.premaxpc09);
		fieldNo051.set(t5695rec.commenpc09);
		fieldNo052.set(t5695rec.freqFrom10);
		fieldNo053.set(t5695rec.freqTo10);
		fieldNo054.set(t5695rec.cmrate10);
		fieldNo055.set(t5695rec.premaxpc10);
		fieldNo056.set(t5695rec.commenpc10);
		fieldNo057.set(t5695rec.freqFrom11);
		fieldNo058.set(t5695rec.freqTo11);
		fieldNo059.set(t5695rec.cmrate11);
		fieldNo060.set(t5695rec.premaxpc11);
		fieldNo061.set(t5695rec.commenpc11);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
