/*
 * File: Th622pt.java
 * Date: 30 August 2009 2:37:24
 * Author: Quipoz Limited
 * 
 * Class transformed from TH622PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Th622rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH622.
*
*
*****************************************************************
* </pre>
*/
public class Th622pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("OR RATES DEFINITION FOR REGULAR PREMIUM        SH622");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(77);
	private FixedLengthStringData filler9 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(64).isAPartOf(wsaaPrtLine004, 13, FILLER).init("Comm/Prem   Year 1   Year 2   Year 3   Year 4   Year 5    Top Up");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(68);
	private FixedLengthStringData filler11 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(53).isAPartOf(wsaaPrtLine005, 15, FILLER).init("Basis     Year 6   Year 7   Year 8   Year 9   Year 10");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(21);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine006, 13, FILLER).init("(C or P)");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler15 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  Direct OR");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 17);
	private FixedLengthStringData filler16 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 52).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(67);
	private FixedLengthStringData filler22 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 52).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler27 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  Indirect OR");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 17);
	private FixedLengthStringData filler28 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 25).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 52).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 61).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 71).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(67);
	private FixedLengthStringData filler34 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 25).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 43).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 52).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 58, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(28);
	private FixedLengthStringData filler39 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th622rec th622rec = new Th622rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th622pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th622rec.th622Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo008.set(th622rec.zryrperc01);
		fieldNo009.set(th622rec.zryrperc02);
		fieldNo010.set(th622rec.zryrperc03);
		fieldNo011.set(th622rec.zryrperc04);
		fieldNo012.set(th622rec.zryrperc05);
		fieldNo014.set(th622rec.zryrperc06);
		fieldNo015.set(th622rec.zryrperc07);
		fieldNo016.set(th622rec.zryrperc08);
		fieldNo017.set(th622rec.zryrperc09);
		fieldNo018.set(th622rec.zryrperc10);
		fieldNo013.set(th622rec.zryrperc21);
		fieldNo025.set(th622rec.zryrperc22);
		fieldNo020.set(th622rec.zryrperc11);
		fieldNo021.set(th622rec.zryrperc12);
		fieldNo022.set(th622rec.zryrperc13);
		fieldNo023.set(th622rec.zryrperc14);
		fieldNo024.set(th622rec.zryrperc15);
		fieldNo026.set(th622rec.zryrperc16);
		fieldNo027.set(th622rec.zryrperc17);
		fieldNo028.set(th622rec.zryrperc18);
		fieldNo029.set(th622rec.zryrperc19);
		fieldNo030.set(th622rec.zryrperc20);
		fieldNo007.set(th622rec.keyopt01);
		fieldNo019.set(th622rec.keyopt02);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
