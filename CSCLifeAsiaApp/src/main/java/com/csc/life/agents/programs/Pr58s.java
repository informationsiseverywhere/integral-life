/*
 * File: PXXXX.java
 * Date: {{dd-mm-yyyy}}
 * Author: Autoated Code template
 * 
 * Class transformed from {{template name}}
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
//package life.agents;
package com.csc.life.agents.programs;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
//import com.csc.smart400framework.utility.Validator;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
//import com.csc.life.productdefinition.tablestructures.Th549rec;
import com.csc.life.agents.tablestructures.Tr58srec;
import com.csc.smart.recordstructures.Wsspsmart;

import com.csc.smart400framework.parent.ScreenProgram;
//import com.csc.life.productdefinition.screens.Sh549ScreenVars;
import com.csc.life.agents.screens.Sr58sScreenVars;

import com.csc.smart400framework.parent.ScreenProgCS;
//import com.csc.life.productdefinition.procedures.Th549pt;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
/********************************************
 * includes for IO classes
*******************************************/

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Pr58s extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58S");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);

	private FixedLengthStringData wsaaTemp = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCoverge = new FixedLengthStringData(4).isAPartOf(wsaaTemp, 0);
	private FixedLengthStringData wsaaRate = new FixedLengthStringData(4).isAPartOf(wsaaTemp, 4);

	private FixedLengthStringData wsaaItemSmokM = new FixedLengthStringData(2);
        /*****************************************************
         * Add the Error Codes for the Screen
		/* ERRORS */
	private String e186 = "E186";
         
	 private FixedLengthStringData errors = new FixedLengthStringData(0);

		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItdmTableDAM itmdIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr58srec tr58srec = new Tr58srec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58sScreenVars sv = ScreenProgram.getScreenVars( Sr58sScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr58s() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58s", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
{
	initialise1010();
	readRecord1031();
	moveToScreen1040();
	generalArea1045();
 }

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		FixedLengthStringData readmode =(isEQ(wsspcomn.flag,"I"))?varcom.readr:varcom.readh; 
		itmdIO.setFunction(readmode);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItempfx());
		descIO.setDesccoy(itmdIO.getItemcoy());
		descIO.setDesctabl(itmdIO.getItemtabl());
		descIO.setDescitem(itmdIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{

		sv.company.set(itmdIO.getItemcoy());
		sv.tabl.set(itmdIO.getItemtabl());
		sv.item.set(itmdIO.getItemitem());
		sv.itmfrm.set(itmdIO.getItmfrm());
		sv.itmto.set(itmdIO.getItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
                //*****************************************
                //* sets the data string 
		tr58srec.tr58sRec.set(itmdIO.getGenarea());
		
		//tr58srec.expfactor.set(ZERO);
	}

protected void generalArea1045()
	{	   
		if (isEQ(itmdIO.getItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItmfrm());
		}
		if (isEQ(itmdIO.getItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItmto());
		}
                /*******************************************
                 * now use Reflecxtion based utility to set the Screen Vars from the Io record
		 *  sv.opcdas.set(tXXXXrec.opcdas);
		 *  sv.subrtns.set(tXXXXrec.subrtns);
                 ******************************************/
                //DataUtil.Copy((sv,t1888rec);  // to , from; changed by sia on 8th April
		//BeanMapper.copytoTable(sv, tr58srec);
		 sv.comtot01.set(tr58srec.comtot01);
		 sv.comtot02.set(tr58srec.comtot02);
		 sv.comtot03.set(tr58srec.comtot03);
		 sv.comtot04.set(tr58srec.comtot04);
		 sv.comtot05.set(tr58srec.comtot05);
		 sv.comtot06.set(tr58srec.comtot06);
		 sv.comtot07.set(tr58srec.comtot07);
		 sv.comtot08.set(tr58srec.comtot08);
		 
		 sv.prcent01.set(tr58srec.prcent01);
		 sv.prcent02.set(tr58srec.prcent02);
		 sv.prcent03.set(tr58srec.prcent03);
		 sv.prcent04.set(tr58srec.prcent04);
		 sv.prcent05.set(tr58srec.prcent05);
		 sv.prcent06.set(tr58srec.prcent06);
		 sv.prcent07.set(tr58srec.prcent07);
		 sv.prcent08.set(tr58srec.prcent08);
		 
		 sv.znadjperc01.set(tr58srec.znadjperc01);
		 sv.znadjperc02.set(tr58srec.znadjperc02);
		 
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
	}

	
protected void preScreenEdit()
	{
 		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}

	}

protected void screenEdit2000()
{
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(wsspcomn.flag,"I"))
	{
          return;
        }
        //*********************************
        // Put all your validation code here - same as ORD 
	
        //******** if any errors found set this 
        if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}

}


protected void update3000()
{
	if (isEQ(wsspcomn.flag,"I")) {
          return;
	}
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")) {
		wsaaUpdateFlag = "Y";
	}
	checkChanges3100();
	if (isNE(wsaaUpdateFlag,"Y")) {
	 return;
	}
	updatePrimaryRecord3050();
	updateRecord3055();

	}



protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setTableprog(wsaaProg);
		itmdIO.setGenarea(tr58srec.tr58sRec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
	
	if (isNE(sv.comtot01, tr58srec.comtot01)) {
		tr58srec.comtot01.set(sv.comtot01);
	}
	if (isNE(sv.comtot02, tr58srec.comtot02)) {
		tr58srec.comtot02.set(sv.comtot02);
	}
	if (isNE(sv.comtot03, tr58srec.comtot03)) {
		tr58srec.comtot03.set(sv.comtot03);
	}
	if (isNE(sv.comtot04, tr58srec.comtot04)) {
		tr58srec.comtot04.set(sv.comtot04);
	}
	if (isNE(sv.comtot05, tr58srec.comtot05)) {
		tr58srec.comtot05.set(sv.comtot05);
	}
	if (isNE(sv.comtot06, tr58srec.comtot06)) {
		tr58srec.comtot06.set(sv.comtot06);
	}
	if (isNE(sv.comtot07, tr58srec.comtot07)) {
		tr58srec.comtot07.set(sv.comtot07);
	}
	if (isNE(sv.comtot08, tr58srec.comtot08)) {
		tr58srec.comtot08.set(sv.comtot08);
	}
	
	if (isNE(sv.prcent01, tr58srec.prcent01)) {
		tr58srec.prcent01.set(sv.prcent01);
	}
	if (isNE(sv.prcent02, tr58srec.prcent02)) {
		tr58srec.prcent02.set(sv.prcent02);
	}
	if (isNE(sv.prcent03, tr58srec.prcent03)) {
		tr58srec.prcent03.set(sv.prcent03);
	}
	if (isNE(sv.prcent05, tr58srec.prcent05)) {
		tr58srec.prcent05.set(sv.prcent05);
	}
	if (isNE(sv.prcent06, tr58srec.prcent06)) {
		tr58srec.prcent06.set(sv.prcent06);
	}
	if (isNE(sv.prcent07, tr58srec.prcent07)) {
		tr58srec.prcent07.set(sv.prcent07);
	}
	if (isNE(sv.prcent08, tr58srec.prcent08)) {
		tr58srec.prcent08.set(sv.prcent08);
	}
	if (isNE(sv.prcent04, tr58srec.prcent04)) {
		tr58srec.prcent04.set(sv.prcent04);
	}	
	
	if (isNE(sv.znadjperc01, tr58srec.znadjperc01)) {
		tr58srec.znadjperc01.set(sv.znadjperc01);
	}
	if (isNE(sv.znadjperc02, tr58srec.znadjperc02)) {
		tr58srec.znadjperc02.set(sv.znadjperc02);
	}
	
	 
	wsaaUpdateFlag = "Y";	
		
	}



protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}


}
