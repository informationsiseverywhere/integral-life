package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR663REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr663rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr663Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData dteapp = new ZonedDecimalData(8, 0).isAPartOf(tr663Rec, 0);
  	public FixedLengthStringData zparam = new FixedLengthStringData(2).isAPartOf(tr663Rec, 8);
  	public FixedLengthStringData zprofile = new FixedLengthStringData(3).isAPartOf(tr663Rec, 10);
  	public FixedLengthStringData ztaxid = new FixedLengthStringData(34).isAPartOf(tr663Rec, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(453).isAPartOf(tr663Rec, 47, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr663Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr663Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}