package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.agents.screens.Sr60dScreenVars;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.DateUtils;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*Function:
*     This screen is used at new business, component add and
* single premium further investment. It will record the
* deductible amount advised by the Employer and Member on
* which contributions tax will be based for a contract
* / coverage by creating a ZCTX record.
*
*     For single premium coverages the user will only be able to
* enter amount (or leave the screen blank). For regular premium
* contracts the user will only be able to enter percentages
* (or leave the screen blank).
*
*      
*
*****************************************************************
* </pre>
*/
public class Pr60d extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR60D");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
    private DescTableDAM descIO = new DescTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
    private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Sr60dScreenVars sv = ScreenProgram.getScreenVars( Sr60dScreenVars.class);
	private Zctxpf zctxpf=null;
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Tr59xrec tr59xrec = new Tr59xrec();
	SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	Chdrpf chdrpf = null;
	ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(Pr60d.class);
	String subMenu = new String("PR60S");
	String CurTaxFromDate,PreTaxFromDate;
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private Wssplife wssplife = new Wssplife();
	private boolean ausCtxFlag = false;
	//ILIFE-7916  start
		private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
		List<Zctxpf> zctxpfList = new ArrayList<Zctxpf>();
		List<Itempf> itempf;
		private Premiumrec premiumrec = new Premiumrec();
		
		BigDecimal tempriskPrem =new BigDecimal("0");
		
				//ILIFE-7916 end
	List<Rskppf> rskppfList = new ArrayList<Rskppf>();
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	 /*ILIFE-8098 Start*/
		protected CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);/*ILIFE-8098 */
		Covrpf covrpf = null; /*ILIFE-8098 */
		List<Covrpf> covrpflist = new ArrayList<Covrpf>();
		 /*ILIFE-8098 End*/
	private enum GotoLabel implements GOTOInterface {
		preExit
	}
	public Pr60d() {
		super();
		screenVars = sv;
		new ScreenModel("Sr60d", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

	try {
		super.mainline();
	} catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes

	}
}
public void processBo(Object... parmArray) {
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

	try {
		processBoMainline(sv, sv.dataArea, parmArray);
	} catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes
	}
}

protected void initialise1000()
{
	try {
		initialise1010();
	}
	catch (GOTOException e){
	}
}
protected void initialise1010()
{
	sv.dataArea.set(SPACES);
	//ILIFE-3806 starts
	sv.zdedpct.set(ZERO);
	sv.zoerpct.set(ZERO);
	sv.zsgtpct.set(ZERO);
	sv.zspspct.set(ZERO);;
	sv.zundpct.set(ZERO);
	sv.totprcnt.set(ZERO);
	//ILIFE-3806 ends
	sv.riskPrem.set(ZERO);
	retrvChdr1500();
	obtainZctxRecord1600();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	datcon2rec.intDatex1.set(datcon1rec.intDate);
	datcon2rec.frequency.set("12");
	datcon2rec.freqFactor.set(-3);
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	wsaaToday.set(datcon1rec.intDate);
	autogenerateFromTaxDate();
	
  	if(isEQ(wsspcomn.flag ,"I")){
		  sv.zsgtpctOut[varcom.pr.toInt()].set("Y");
		  sv.zoerpctOut[varcom.pr.toInt()].set("Y");
		  sv.zdedpctOut[varcom.pr.toInt()].set("Y");
		  sv.zundpctOut[varcom.pr.toInt()].set("Y");
		  sv.zspspctOut[varcom.pr.toInt()].set("Y");
		  sv.totprcntOut[varcom.pr.toInt()].set("Y");
		  sv.zslryspctOut[varcom.pr.toInt()].set("Y");//ILIFE-7345 
	  }
  	if(wsspcomn.submenu.toString().equals(subMenu)) {
  		sv.datefrmOut[varcom.pr.toInt()].set("Y");
  		sv.datetoOut[varcom.pr.toInt()].set("Y");
  		
  		sv.totamntOut[varcom.nd.toInt()].set("N");
  		if(isEQ(wsspcomn.flag ,"M")){
  			if(isEQ(sv.datefrm,CurTaxFromDate)){
  				sv.totamntOut[varcom.pr.toInt()].set("Y");
  				sv.totprcntOut[varcom.pr.toInt()].set("Y");
  			}
  			else{
  			  sv.zsgtpctOut[varcom.pr.toInt()].set("Y");
  			  sv.zoerpctOut[varcom.pr.toInt()].set("Y");
  			  sv.zdedpctOut[varcom.pr.toInt()].set("Y");
  			  sv.zundpctOut[varcom.pr.toInt()].set("Y");
  			  sv.zspspctOut[varcom.pr.toInt()].set("Y");
  			  sv.totprcntOut[varcom.pr.toInt()].set("Y");
  			  sv.zslryspctOut[varcom.pr.toInt()].set("Y"); //ILIFE-7345 
  			}
  		}
  		
  	}
  	String ctxItem = "BTPRO017";
  	ausCtxFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ctxItem, appVars, "IT");
	  
}
protected void autogenerateFromTaxDate(){
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxCurFromDate = Calendar.getInstance();
    Calendar taxPreFromDate = Calendar.getInstance();
	try
	 {
	    proposalDate.setTime(integralDateFormat.parse(wsaaToday.toString()));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7)//ILIFE-3807
	    {
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    }
	    else{ 
	    	taxCurFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxPreFromDate.set(proposalDate.get(Calendar.YEAR)-2, 6, 01);
	    }
	  }catch (ParseException e) {
			
			e.printStackTrace();
		}
	CurTaxFromDate=integralDateFormat.format(taxCurFromDate.getTime());
	PreTaxFromDate=integralDateFormat.format(taxPreFromDate.getTime());

}

protected void corpname()
{
	/*PAYEE-1001*/
	wsspcomn.longconfname.set(SPACES);
	/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
	/*        CLTS-GIVNAME         DELIMITED '  '                   */
	StringUtil stringVariable1 = new StringUtil();
	stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
	stringVariable1.addExpression(" ");
	stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
	stringVariable1.setStringInto(wsspcomn.longconfname);
	/*CORP-EXIT*/
}

protected void plainname()
{
	/*PLAIN-100*/
	wsspcomn.longconfname.set(SPACES);
	if (isEQ(cltsIO.getClttype(), "C")) {
		corpname();
		return ;
	}
	if (isNE(cltsIO.getGivname(), SPACES)) {
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getSurname(), "  ");
		stringVariable1.addExpression(", ");
		stringVariable1.addExpression(cltsIO.getGivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
	}
	else {
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}
	/*PLAIN-EXIT*/
}

protected void retrvChdr1500(){
	/*Retrv the chdrlnb record*/
	
	sv.chdrnum.set(wsspcomn.chdrChdrnum);	
	if(isNE(wsspcomn.chdrChdrnum,SPACES)){
		sv.chdrnum.set(wsspcomn.chdrChdrnum);
		sv.cnttype.set(wsspcomn.chdrCnttype);
		descIO.setDataArea(SPACES);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5688");
		descIO.setDescitem(wsspcomn.chdrCnttype);
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypdesc.fill("?");
		}
		else {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		
		}
		if(isEQ(wsspcomn.chdrChdrnum,SPACES)){
		//syserrrec.statuz.set(chdrlnbIO.getStatuz());
		syserrrec.params.set(wsspcomn.chdrChdrnum);
			fatalError600();
		}
	}

protected void obtainZctxRecord1600(){
	
	if(isEQ(wsspcomn.flag,"I")||(wsspcomn.submenu.toString().equals(subMenu))){
		zctxpf=new Zctxpf();
		zctxpf.setChdrnum(sv.chdrnum.toString());
		List<Zctxpf> list=zctxpfDAO.getZctxpfRecord(sv.chdrnum.toString());
		/*ILIFE-7916 Start*/
		if(isGT(wsspcomn.currto.toInt(),0))  // ILIFE-8006
		{
			zctxpf.setDatefrm((wsspcomn.currto.toInt())); 
		}
		else
		{
			zctxpf.setDatefrm(list.get(0).getDatefrm()); //ILIFE-7345-NFException-Fix,// ILIFE-8006		
		/*ILIFE-7916 END*/
		}
		zctxpf=zctxpfDAO.readRecordByDate(zctxpf);
	//	zctxpf=zctxpfDAO.readRecordByDate(zctxpf);  // ILIFE-8006
		if(zctxpf!=null)	{
			setUpFromZctx1620();
			if(isGT(sv.totamnt,0)){
				sv.zsgtamt.set(zctxpf.getZsgtamt());
				sv.zoeramt.set(zctxpf.getZoeramt());
				sv.zdedamt.set(zctxpf.getZdedamt());
				sv.zundamt.set(zctxpf.getZundamt());
				sv.zspsamt.set(zctxpf.getZspsamt());
				sv.zslrysamt.set(zctxpf.getZslrysamt()); //ILIFE-7345 
			}
			}		
	}
	else if(isNE(wsspcomn.flag,"I")){
	zctxpf=zctxpfDAO.readRecord(wsspcomn.company.value(), wsspcomn.chdrChdrnum.value());
	if(zctxpf!=null){
		setUpFromZctx1620();
//	ILIFE-8098 Start
		
	covrpflist = covrpfDAO.getSingpremtype(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
	if(covrpflist.size()>0){
		covrpf = covrpflist.get(0);
	if(isGT(sv.totamnt,0) && covrpf.getSingpremtype().equalsIgnoreCase("ROP") || covrpf.getSingpremtype().equalsIgnoreCase("CTX")){
		sv.zsgtamt.set(zctxpf.getZsgtamt());
		sv.zoeramt.set(zctxpf.getZoeramt());
		sv.zdedamt.set(zctxpf.getZdedamt());
		sv.zundamt.set(zctxpf.getZundamt());
		sv.zspsamt.set(zctxpf.getZspsamt());
		sv.zslrysamt.set(zctxpf.getZslrysamt()); //ILIFE-7345 
	}

}
//	ILIFE-8098 END	
	}
	else 
		initialiseScreen1610();
	 //sv.totamntOut[varcom.nd.toInt()].set("Y"); //ILIFE-7805
	}
	obtainClientDetails();
	}

protected void initialiseScreen1610(){
	sv.lifenum.set(wsspcomn.chdrCownnum);
	/*ILIFE-3693 -nnaveenkumar starts*/
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(wsspcomn.company);
	itemIO.setItemtabl("TR59X");
	itemIO.setItemitem(wsspcomn.chdrCnttype);
	itemIO.setFormat(formatsInner.itemrec);
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	if (isEQ(itemIO.getStatuz(), varcom.oK)){
		tr59xrec.tr59xrec.set(itemIO.getGenarea());
	}
	
	if(isEQ(tr59xrec.dfcontrn,"MEMNC"))
		  sv.zundpct.set(100);
	//ILIFE-3806 starts
	else if(isEQ(tr59xrec.dfcontrn,"MEMC"))
	     sv.zdedpct.set(100);
	else if(isEQ(tr59xrec.dfcontrn,"SPOUSE"))
	     sv.zspspct.set(100);
	else if(isEQ(tr59xrec.dfcontrn,"EMPSG"))
	     sv.zsgtpct.set(100);
	else if(isEQ(tr59xrec.dfcontrn,"EMPOF"))
		 sv.zoerpct.set(100);
	     sv.totprcnt.set(100);
	//ILIFE-3806 ends	
	 autogenerateFromToTaxDate();
	/*ILIFE-3693 -nnaveenkumar ends*/
       
	    
}

protected void setUpFromZctx1620(){
	sv.lifenum.set(zctxpf.getClntnum());
	sv.datefrm.set(zctxpf.getDatefrm());
	sv.dateto.set(zctxpf.getDateto());
	sv.totprcnt.set(zctxpf.getTotprcnt());
	sv.zdedpct.set(zctxpf.getZdedpct());
	sv.zoerpct.set(zctxpf.getZoerpct());
	sv.zsgtpct.set(zctxpf.getZsgtpct());
	sv.zundpct.set(zctxpf.getZundpct());
	sv.zspspct.set(zctxpf.getZspspct());
	sv.totamnt.set(zctxpf.getAmount());
	sv.zslryspct.set(zctxpf.getZslrysamt());//ILIFE-7345 
	sv.zslryspct.set(zctxpf.getZslryspct());//ILIFE-7345 

	// ILIFE-7916 start
		
		List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), "TR59X", wsspcomn.chdrCnttype.toString());
		if(!ObjectUtils.isEmpty(items)){
			tr59xrec.tr59xrec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}else{
			syserrrec.params.set("TR59X");
			fatalError600();
		}
		
	if(isNE(tr59xrec.cocontpay,"Y" ) ||isNE(tr59xrec.liscpay,"Y" ) ){
		sv.coContriPmtOut[varcom.nd.toInt()].set("Y");
		sv.receivedFromAtoOut[varcom.nd.toInt()].set("Y");
			
	}
			
	zctxpfList= zctxpfDAO.getRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString(),wsspcomn.currto.toString());
		if(!ObjectUtils.isEmpty(zctxpfList)){
			rskppfList = rskppfDAO.fetchRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString(),wsspcomn.currto.toInt());
			if(!ObjectUtils.isEmpty(rskppfList)) {
				int i = 0;
				for(Rskppf rskfetch : rskppfList) {
					tempriskPrem=tempriskPrem.add(rskfetch.getRiskprem());
					if(i <= 0 && isGT(rskfetch.getPolfee(), ZERO) && isNE(tr59xrec.polfee, SPACE)) {
						tempriskPrem=tempriskPrem.add(rskfetch.getPolfee());
						i++;
					}
				}
			}else {
				tempriskPrem = BigDecimal.ZERO;
			}
		for (Zctxpf record : zctxpfList) {
			
		if(isNE(sv.totamnt,ZERO)){
		sv.riskPrem.set(tempriskPrem);	
		}
		if(!StringUtils.isEmpty(record.getTottax())){
			sv.tottax.set(record.getTottax());	
		}
		if(!StringUtils.isEmpty(record.getProcflag())){
			sv.s290NoticeReceived.set(record.getProcflag());	
		}
		if(!StringUtils.isEmpty(record.getCocontamnt())){
			sv.coContriPmt.set(record.getCocontamnt());	
		}
		if(!StringUtils.isEmpty(record.getLiscamnt())){
			sv.receivedFromAto.set(record.getLiscamnt());	
		}
			}
			tempriskPrem =new BigDecimal("0");
		} else{
			
			zctxpfList= zctxpfDAO.getzctxpfRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString());
			if(!ObjectUtils.isEmpty(zctxpfList)){
				rskppfList = rskppfDAO.fetchRecord(wsspcomn.company.toString(),wsspcomn.chdrChdrnum.toString(),wsspcomn.currto.toInt());
				if(!ObjectUtils.isEmpty(rskppfList)) {
					int i = 0;
					for(Rskppf rskfetch : rskppfList) {
						tempriskPrem=tempriskPrem.add(rskfetch.getRiskprem());
						if(i <= 0 && isGT(rskfetch.getPolfee(), ZERO) && isNE(tr59xrec.polfee, SPACE)) {
							tempriskPrem=tempriskPrem.add(rskfetch.getPolfee());
							i++;
						}
					}
				}else {
					tempriskPrem = BigDecimal.ZERO;
				}
			for (Zctxpf record : zctxpfList) {
			
			if(isNE(sv.totamnt,ZERO)){
				sv.riskPrem.set(tempriskPrem);	
				}
			
			if(!StringUtils.isEmpty(record.getCocontamnt())){
				sv.coContriPmt.set(record.getCocontamnt());	
			}
			if(!StringUtils.isEmpty(record.getLiscamnt())){
				sv.receivedFromAto.set(record.getLiscamnt());	
			}
				}
				tempriskPrem =new BigDecimal("0");
			}
		}
		
		wsspcomn.currto.set(SPACE);
		
		
	
// ILIFE-7916 End	
	
		}
	protected void preScreenEdit()
{
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
	goTo(GotoLabel.preExit);
}

protected void screenEdit2000(){
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz, "KILL")) {
		return;
	}
	validate2020();	
	checkForErrors2080();
}


protected void validate2020(){
 if(isEQ(wsspcomn.flag,"I")){
	 return;
 }
 /*
 *    Validate fields
 */
 if (isEQ(scrnparams.statuz,varcom.calc)){
	 wsspcomn.edterror.set("Y");
 }

 if(isLT(sv.zsgtpct,0)||isGT(sv.zsgtpct,100))
	 sv.zsgtpctErr.set(errorsInner.rglm);
	 
 if(isLT(sv.zoerpct,0)||isGT(sv.zoerpct,100))
	 sv.zoerpctErr.set(errorsInner.rglm);
 if(isLT(sv.zdedpct,0)||isGT(sv.zdedpct,100))
	 sv.zdedpctErr.set(errorsInner.rglm);
 if(isLT(sv.zundpct,0)||isGT(sv.zundpct,100))
	 sv.zundpctErr.set(errorsInner.rglm);
 if(isLT(sv.zspspct,0)||isGT(sv.zspspct,100))
	 sv.zspspctErr.set(errorsInner.rglm);
 if(isLT(sv.zslryspct,0)||isGT(sv.zslryspct,100)) //ILIFE-7345 
	 sv.zslryspctErr.set(errorsInner.rglm);
 
 //ILIFE-3693 -nnaveenkumar
 validategenerateFromToTaxDate();

 	 sv.totprcnt.set(add(sv.zundpct,sv.zdedpct,sv.zoerpct,sv.zsgtpct,sv.zspspct,sv.zslryspct));
	 if(wsspcomn.submenu.toString().equals(subMenu)) {
	  		
	  			if(isNE(sv.datefrm,CurTaxFromDate))
	  		        {
	  				if(isNE(sv.totamnt,add(sv.zundamt,sv.zdedamt,sv.zoeramt,sv.zsgtamt,sv.zspsamt,sv.zslrysamt))){
	  					 sv.totamntErr.set(errorsInner.e954);
	  					 return;}
	  				else{
	  					sv.zsgtpct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zsgtamt.getbigdata()));
	  					sv.zoerpct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zoeramt.getbigdata()));
	  					sv.zdedpct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zdedamt.getbigdata()));
	  					sv.zundpct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zundamt.getbigdata()));
	  					sv.zspspct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zspsamt.getbigdata()));
	  					sv.zslryspct.set(Calculatepct(sv.totamnt.getbigdata(),sv.zslrysamt.getbigdata()));//ILIFE-7345 
	  					sv.totprcnt.set(100);
	  				} 
	  				 
	             }
	  		}
	 if(isNE(100,sv.totprcnt)){
		 sv.zdedpctErr.set(errorsInner.e631);
		 sv.zundpctErr.set(errorsInner.e631);
		 sv.zoerpctErr.set(errorsInner.e631);
		 sv.zsgtpctErr.set(errorsInner.e631);
		 sv.zspspctErr.set(errorsInner.e631);
		 sv.zslryspctErr.set(errorsInner.e631);//ILIFE-7345 
	 }
 

         }
		 
protected BigDecimal Calculatepct(BigDecimal total,BigDecimal amt){
	BigDecimal num = new BigDecimal(100);
	BigDecimal pct=amt.multiply(num);
	pct=pct.divide(total, 2, RoundingMode.HALF_UP);
	return pct;
	
}

/*ILIFE-3693 -nnaveenkumar starts*/
protected void autogenerateFromToTaxDate(){
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxFromDate = Calendar.getInstance();
    Calendar taxToDate = Calendar.getInstance();
	try
	 {
	    proposalDate.setTime(integralDateFormat.parse(wsspcomn.currfrom.toString()));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7)//ILIFE-3807
	    {
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
	    }
	    else{ 
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
	    }
	  }catch (ParseException e) {
			
			e.printStackTrace();
		}
	sv.datefrm.set(integralDateFormat.format(taxFromDate.getTime()));
	sv.dateto.set(integralDateFormat.format(taxToDate.getTime()));

}

protected void setTaxFromToDate(){  
	 Calendar taxFromDate = Calendar.getInstance();
	  Calendar taxToDate = Calendar.getInstance();
	  try {
		taxFromDate.setTime(integralDateFormat.parse(sv.datefrm.toString()));
	} catch (ParseException e) {
		// TODO Auto-generated catch block
		 LOGGER.error("Can't parse date", e);
	}
      taxToDate.set(taxFromDate.get(Calendar.YEAR)+1, 5, 30);
      sv.dateto.set(integralDateFormat.format(taxToDate.getTime()));
}

protected void validategenerateFromToTaxDate(){
	 Calendar proposalDate = Calendar.getInstance();
	 Calendar taxFromDate = Calendar.getInstance();
		try
		 {
			taxFromDate.setTime(integralDateFormat.parse(sv.datefrm.toString()));
			proposalDate.setTime(integralDateFormat.parse(wsspcomn.currfrom.toString()));
		    if(taxFromDate.get(Calendar.MONTH)+1!=7||taxFromDate.get(Calendar.DATE)!=1){
		    	 sv.datefrmErr.set(errorsInner.rfxm);
		    }
		    else{
		    	setTaxFromToDate();
		    }
		  } catch (ParseException e) {
				// TODO Auto-generated catch block
			  LOGGER.error("Can't parse date", e);
			}
}
/*ILIFE-3693 -nnaveenkumar ends*/
protected void checkForErrors2080(){
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isEQ(scrnparams.statuz,varcom.kill)) {
		return ;
	}
}


/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/

protected void update3000()
{
	try {
		update3010();  
		
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void update3010(){

    /*
    *  Update database files as required / WSSP
    */
	if(isEQ(wsspcomn.flag,"I")){
		return;
	}
	if (isEQ(scrnparams.statuz,varcom.kill)) {
		return ;
	}
	
	InsertOrUpdateZctx();
	}


protected void InsertOrUpdateZctx(){
	if(zctxpf!=null){
		zctxpf.setZdedpct(sv.zdedpct.getbigdata());
		zctxpf.setZoerpct(sv.zoerpct.getbigdata());
		zctxpf.setZsgtpct(sv.zsgtpct.getbigdata());
		zctxpf.setZundpct(sv.zundpct.getbigdata());
		zctxpf.setZspspct(sv.zspspct.getbigdata());
		zctxpf.setZsgtamt(sv.zsgtamt.getbigdata());
		zctxpf.setZoeramt(sv.zoeramt.getbigdata());
		zctxpf.setZdedamt(sv.zdedamt.getbigdata());
		zctxpf.setZundamt(sv.zundamt.getbigdata());
		zctxpf.setZspsamt(sv.zspsamt.getbigdata());
		zctxpf.setZslrysamt(sv.zslrysamt.getbigdata()); //ILIFE-7345 
		zctxpf.setZslryspct(sv.zslryspct.getbigdata());//ILIFE-7345 
		if(wsspcomn.submenu.toString().equals(subMenu)){
			zctxpfDAO.setCacheObject(zctxpf);
			return;
		}
		boolean check=zctxpfDAO.updateRecord(zctxpf);
	}
	else{
		zctxpf=new Zctxpf();
	zctxpf.setChdrcoy(wsspcomn.company.toString());
	zctxpf.setChdrnum(sv.chdrnum.toString());
	zctxpf.setCnttype(sv.cnttype.toString());
	zctxpf.setClntnum(sv.lifenum.toString());
	zctxpf.setDatefrm(sv.datefrm.toInt());
	zctxpf.setDateto(sv.dateto.toInt());
	zctxpf.setZsgtpct(sv.zsgtpct.getbigdata());
	zctxpf.setZoerpct(sv.zoerpct.getbigdata());
	zctxpf.setZdedpct(sv.zdedpct.getbigdata());
	zctxpf.setZundpct(sv.zundpct.getbigdata());
	zctxpf.setZspspct(sv.zspspct.getbigdata());
	zctxpf.setTotprcnt(sv.totprcnt.getbigdata());
	zctxpf.setAmount(sv.totamnt.getbigdata());
	zctxpf.setZsgtamt(sv.zsgtamt.getbigdata());
	zctxpf.setZoeramt(sv.zoeramt.getbigdata());
	zctxpf.setZdedamt(sv.zdedamt.getbigdata());
	zctxpf.setZundamt(sv.zundamt.getbigdata());
	zctxpf.setZspsamt(sv.zspsamt.getbigdata());
	zctxpf.setEffdate(wsspcomn.occdate.toInt());
	zctxpf.setZslrysamt(sv.zslrysamt.getbigdata()); //ILIFE-7345 
	zctxpf.setZslryspct(sv.zslryspct.getbigdata());//ILIFE-7345 
	zctxpf.setRollflag("N");
	if(ausCtxFlag)
		zctxpf.setProcflag(SPACE);
	zctxpfDAO.insertZctxpfRecord(zctxpf);
	}

}

protected void obtainClientDetails(){
	cltsIO.setDataArea(SPACES);
	cltsIO.setClntpfx("CN");
	cltsIO.setClntcoy(wsspcomn.fsuco);
	cltsIO.setClntnum(sv.lifenum);
	cltsIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, cltsIO);
	if (isNE(cltsIO.getStatuz(), varcom.oK)
			&& isNE(cltsIO.getStatuz(), varcom.mrnf)){
				syserrrec.statuz.set(cltsIO.getStatuz());
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
	}
	if(isEQ(cltsIO.getStatuz(), varcom.mrnf)){
		sv.lifedesc.set(SPACES);
	}
	plainname();
	sv.lifedesc.set(wsspcomn.longconfname);
}

protected void whereNext4000()
{
	nextProgram4010();
}

protected void nextProgram4010()
{
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspwindow.value.set(SPACES);
		scrnparams.function.set("HIDEW");
		return;
	}
	wsspcomn.programPtr.add(1);
	
	
}
private static final class FormatsInner {
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}


/*
 * Class transformed  from Data Structure ERRORS_INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData rglm = new FixedLengthStringData(4).init("RGLM");
	private FixedLengthStringData rfxm = new FixedLengthStringData(4).init("RFXM");
	private FixedLengthStringData e954 = new FixedLengthStringData(4).init("E954");
	
}

}