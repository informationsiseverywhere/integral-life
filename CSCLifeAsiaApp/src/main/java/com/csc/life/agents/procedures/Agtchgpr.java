/*
 * File: Agtchgpr.java
 * Date: 29 August 2009 20:13:45
 * Author: Quipoz Limited
 * 
 * Class transformed from AGTCHGPR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.life.agents.dataaccess.PcddchgTableDAM;
import com.csc.life.agents.recordstructures.Agtchgrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*  AGTCHGPR - Agent Change (Proposal contracts)
*  --------------------------------------------
*
*  Overview
*  ~~~~~~~~
*
*  This  provides  the  facility  to  change  agent  commission
*  details from the old agent to a new agent.
*  The   function   of   this  subroutine  is  to  support  the
*  commission change for an  individual  contract  called  from
*  either  batch  processing or an online request. T6688  which
*  is keyed on contract status will determine when this
*  program is called, i.e. the PR status contracts will  invoke
*  this processing.
*  The  parameters  passed  to  this  program are set up in the
*  copybook AGTCHGREC.
*
*  Main Processing
*  ~~~~~~~~~~~~~~~
*  During New Business processing, PCDD records are not created
*  for the proposal  unless commission splits are  entered.  If
*  split  is  entered  at  proposal,  then Issue creates a PCDD
*  record  based  on  the Servicing Agent Number entered on the
*  Contract Header.
*
*  It  is  therefore  necessary  to  update  the PCDD record.
*
*          Set CHDRLIF-PARAMS  to spaces.
*          Set CHDRLIF-CHDRCOY to AGCG-CHDRCOY.
*          Set CHDRLIF-CHDRNUM to AGCM-CHDRNUM.
*          Set CHDRLIF-FUNCTION to 'READH'
*
*          Call 'CHDRLIFIO' to read the record.
*
*          Perform normal error handling.
*
*          If CHDRLIF-STATUZ not = O-K
*             Error.
*
*          Update the CHDRLIF-AGNTNUM field with the new
*          agent number.
*
*          Rewrt the CHDRLIF record.
*
*          Set PCDDCHG-PARAMS to space.
*          Set PCDDCHG-CHDRCOY to AGCG-CHDRCOY.
*          Set PCDDCHG-CHDRNUM to AGCM-CHDRNUM.
*          Set PCDDCHG-AGNTNUM to AGCM-AGNTNUM-OLD.
*          Set PCDDCHG-FUNCTION to 'READH'
*
*          Call 'PCDDCHGIO' to read the record.
*
*          Perform normal error handling.
*
*          If PCDDCHG-STATUZ = ENDP or MRNF
*             Go to exit
*          Else
*             Continue.
*
*            DELET this PCDD record.
*
*            Move the new Agent Number to PCDD-AGNTNUM.
*
*            WRITR this new PCDD record.
*
*****************************************************************
* </pre>
*/
public class Agtchgpr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "AGTCHGPR";
		/* WSAA-AGNT-REC */
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgntnumNew = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSplitBcomm = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSplitBpts = new PackedDecimalData(5, 2);
	private FixedLengthStringData wsaaValidFlag = new FixedLengthStringData(1);
	private String wsaaNoPcddRecs = "";
	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCurrto = new PackedDecimalData(8, 0);
		/* FORMATS */
	private static final String pcddchgrec = "PCDDCHGREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private PcddchgTableDAM pcddchgIO = new PcddchgTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Agtchgrec agtchgrec = new Agtchgrec();

	public Agtchgpr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		agtchgrec.agentRec = convertAndSetParam(agtchgrec.agentRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaNoPcddRecs = "N";
		/* Only want to change the Servicing Agent on the contract*/
		/* if this flag has been set to 'Y' (it will have been set in*/
		/* the calling program).*/
		/* Once the update has been performed, this flag is set to 'U' -*/
		/* which is passed back to the calling program*/
		if (isEQ(agtchgrec.updateFlag, "Y")) {
			readUpdateChdr050();
		}
		readAgentComm100();
		if (isEQ(wsaaNoPcddRecs, "Y")) {
			return ;
		}
		deleteOldAgentComm200();
		writeNewAgentComm300();
		/*EXIT*/
		exitProgram();
	}

protected void readUpdateChdr050()
	{
		para051();
	}

protected void para051()
	{
		agtchgrec.statuz.set(varcom.oK);
		/*  Retrieve CONTRACT HEADER  Record*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setChdrcoy(agtchgrec.chdrcoy);
		chdrlifIO.setChdrnum(agtchgrec.chdrnum);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			dataBaseError400();
		}
		chdrlifIO.setAgntnum(agtchgrec.agntnumNew);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			dataBaseError400();
		}
		agtchgrec.updateFlag.set("U");
		callBldenrl060();
	}

protected void callBldenrl060()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlifIO.getChdrpfx());
		bldenrlrec.company.set(chdrlifIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlifIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			dataBaseError400();
		}
		/*EXIT*/
	}

protected void readAgentComm100()
	{
		para101();
	}

protected void para101()
	{
		/*  Retrieve AGENT COMMISSION Record*/
		pcddchgIO.setParams(SPACES);
		pcddchgIO.setFunction(varcom.readh);
		pcddchgIO.setAgntnum(agtchgrec.agntnumOld);
		pcddchgIO.setChdrcoy(agtchgrec.chdrcoy);
		pcddchgIO.setChdrnum(agtchgrec.chdrnum);
		pcddchgIO.setFormat(pcddchgrec);
		SmartFileCode.execute(appVars, pcddchgIO);
		if ((isNE(pcddchgIO.getStatuz(), varcom.oK))
		&& (isNE(pcddchgIO.getStatuz(), varcom.endp))
		&& (isNE(pcddchgIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			dataBaseError400();
		}
		if ((isEQ(pcddchgIO.getStatuz(), varcom.mrnf))
		|| (isEQ(pcddchgIO.getStatuz(), varcom.endp))) {
			wsaaNoPcddRecs = "Y";
			return ;
		}
		wsaaChdrcoy.set(pcddchgIO.getChdrcoy());
		wsaaChdrnum.set(pcddchgIO.getChdrnum());
		wsaaAgntnumNew.set(agtchgrec.agntnumNew);
		wsaaSplitBcomm.set(pcddchgIO.getSplitBcomm());
		wsaaSplitBpts.set(pcddchgIO.getSplitBpts());
		wsaaValidFlag.set(pcddchgIO.getValidflag());
		wsaaCurrfrom.set(pcddchgIO.getCurrfrom());
		wsaaCurrto.set(pcddchgIO.getCurrto());
	}

protected void deleteOldAgentComm200()
	{
		/*PARA*/
		pcddchgIO.setFunction(varcom.delet);
		pcddchgIO.setAgntnum(agtchgrec.agntnumOld);
		pcddchgIO.setChdrcoy(agtchgrec.chdrcoy);
		pcddchgIO.setChdrnum(agtchgrec.chdrnum);
		pcddchgIO.setFormat(pcddchgrec);
		SmartFileCode.execute(appVars, pcddchgIO);
		if (isNE(pcddchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			dataBaseError400();
		}
		/*EXIT*/
	}

protected void writeNewAgentComm300()
	{
		para301();
	}

protected void para301()
	{
		pcddchgIO.setChdrcoy(wsaaChdrcoy);
		pcddchgIO.setChdrnum(wsaaChdrnum);
		pcddchgIO.setAgntnum(wsaaAgntnumNew);
		pcddchgIO.setSplitBcomm(wsaaSplitBcomm);
		pcddchgIO.setSplitBpts(wsaaSplitBpts);
		pcddchgIO.setValidflag(wsaaValidFlag);
		pcddchgIO.setTranno(agtchgrec.tranno);
		pcddchgIO.setCurrfrom(wsaaCurrfrom);
		pcddchgIO.setCurrto(wsaaCurrto);
		pcddchgIO.setUser(agtchgrec.user);
		pcddchgIO.setTermid(agtchgrec.termid);
		pcddchgIO.setTransactionTime(agtchgrec.transactionTime);
		pcddchgIO.setTransactionDate(agtchgrec.transactionDate);
		pcddchgIO.setFormat(pcddchgrec);
		pcddchgIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, pcddchgIO);
		if (isNE(pcddchgIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(pcddchgIO.getParams());
			syserrrec.statuz.set(pcddchgIO.getStatuz());
			dataBaseError400();
		}
	}

protected void dataBaseError400()
	{
		error410();
		exit420();
	}

protected void error410()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit420()
	{
		agtchgrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
