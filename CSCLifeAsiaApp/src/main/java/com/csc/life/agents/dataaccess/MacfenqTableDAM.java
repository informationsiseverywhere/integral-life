package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MacfenqTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:04
 * Class transformed from MACFENQ.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MacfenqTableDAM extends MacfpfTableDAM {

	public MacfenqTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MACFENQ");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM"
		             + ", EFFDATE";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "EFFDATE, " +
		            "AGMVTY, " +
		            "MLPRVSUP01, " +
		            "MLPRVSUP02, " +
		            "MLPRVSUP03, " +
		            "MLPRVSUP04, " +
		            "ZRPTGA, " +
		            "ZRPTGB, " +
		            "ZRPTGC, " +
		            "ZRPTGD, " +
		            "TRANNO, " +
		            "MLAGTTYP, " +
		            "MLPARAGT, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "MLPARORC, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "EFFDATE DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "EFFDATE ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               agntcoy,
                               agntnum,
                               effdate,
                               agmvty,
                               mlprvsup01,
                               mlprvsup02,
                               mlprvsup03,
                               mlprvsup04,
                               zrptga,
                               zrptgb,
                               zrptgc,
                               zrptgd,
                               tranno,
                               mlagttyp,
                               mlparagt,
                               currfrom,
                               currto,
                               mlparorc,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(50);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getEffdate().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller33 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller33.setInternal(agntcoy.toInternal());
	nonKeyFiller40.setInternal(agntnum.toInternal());
	nonKeyFiller50.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(152);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller33.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getAgmvty().toInternal()
					+ getMlprvsup01().toInternal()
					+ getMlprvsup02().toInternal()
					+ getMlprvsup03().toInternal()
					+ getMlprvsup04().toInternal()
					+ getZrptga().toInternal()
					+ getZrptgb().toInternal()
					+ getZrptgc().toInternal()
					+ getZrptgd().toInternal()
					+ getTranno().toInternal()
					+ getMlagttyp().toInternal()
					+ getMlparagt().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getMlparorc().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller33);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, agmvty);
			what = ExternalData.chop(what, mlprvsup01);
			what = ExternalData.chop(what, mlprvsup02);
			what = ExternalData.chop(what, mlprvsup03);
			what = ExternalData.chop(what, mlprvsup04);
			what = ExternalData.chop(what, zrptga);
			what = ExternalData.chop(what, zrptgb);
			what = ExternalData.chop(what, zrptgc);
			what = ExternalData.chop(what, zrptgd);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, mlagttyp);
			what = ExternalData.chop(what, mlparagt);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, mlparorc);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public FixedLengthStringData getAgmvty() {
		return agmvty;
	}
	public void setAgmvty(Object what) {
		agmvty.set(what);
	}	
	public FixedLengthStringData getMlprvsup01() {
		return mlprvsup01;
	}
	public void setMlprvsup01(Object what) {
		mlprvsup01.set(what);
	}	
	public FixedLengthStringData getMlprvsup02() {
		return mlprvsup02;
	}
	public void setMlprvsup02(Object what) {
		mlprvsup02.set(what);
	}	
	public FixedLengthStringData getMlprvsup03() {
		return mlprvsup03;
	}
	public void setMlprvsup03(Object what) {
		mlprvsup03.set(what);
	}	
	public FixedLengthStringData getMlprvsup04() {
		return mlprvsup04;
	}
	public void setMlprvsup04(Object what) {
		mlprvsup04.set(what);
	}	
	public FixedLengthStringData getZrptga() {
		return zrptga;
	}
	public void setZrptga(Object what) {
		zrptga.set(what);
	}	
	public FixedLengthStringData getZrptgb() {
		return zrptgb;
	}
	public void setZrptgb(Object what) {
		zrptgb.set(what);
	}	
	public FixedLengthStringData getZrptgc() {
		return zrptgc;
	}
	public void setZrptgc(Object what) {
		zrptgc.set(what);
	}	
	public FixedLengthStringData getZrptgd() {
		return zrptgd;
	}
	public void setZrptgd(Object what) {
		zrptgd.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getMlagttyp() {
		return mlagttyp;
	}
	public void setMlagttyp(Object what) {
		mlagttyp.set(what);
	}	
	public FixedLengthStringData getMlparagt() {
		return mlparagt;
	}
	public void setMlparagt(Object what) {
		mlparagt.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getMlparorc() {
		return mlparorc;
	}
	public void setMlparorc(Object what) {
		setMlparorc(what, false);
	}
	public void setMlparorc(Object what, boolean rounded) {
		if (rounded)
			mlparorc.setRounded(what);
		else
			mlparorc.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getMlprvsups() {
		return new FixedLengthStringData(mlprvsup01.toInternal()
										+ mlprvsup02.toInternal()
										+ mlprvsup03.toInternal()
										+ mlprvsup04.toInternal());
	}
	public void setMlprvsups(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlprvsups().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlprvsup01);
		what = ExternalData.chop(what, mlprvsup02);
		what = ExternalData.chop(what, mlprvsup03);
		what = ExternalData.chop(what, mlprvsup04);
	}
	public FixedLengthStringData getMlprvsup(BaseData indx) {
		return getMlprvsup(indx.toInt());
	}
	public FixedLengthStringData getMlprvsup(int indx) {

		switch (indx) {
			case 1 : return mlprvsup01;
			case 2 : return mlprvsup02;
			case 3 : return mlprvsup03;
			case 4 : return mlprvsup04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlprvsup(BaseData indx, Object what) {
		setMlprvsup(indx.toInt(), what);
	}
	public void setMlprvsup(int indx, Object what) {

		switch (indx) {
			case 1 : setMlprvsup01(what);
					 break;
			case 2 : setMlprvsup02(what);
					 break;
			case 3 : setMlprvsup03(what);
					 break;
			case 4 : setMlprvsup04(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		effdate.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller33.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		agmvty.clear();
		mlprvsup01.clear();
		mlprvsup02.clear();
		mlprvsup03.clear();
		mlprvsup04.clear();
		zrptga.clear();
		zrptgb.clear();
		zrptgc.clear();
		zrptgd.clear();
		tranno.clear();
		mlagttyp.clear();
		mlparagt.clear();
		currfrom.clear();
		currto.clear();
		mlparorc.clear();		
	}


}