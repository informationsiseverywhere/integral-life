package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZrfbpfDAO;
import com.csc.life.agents.dataaccess.model.Zrfbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZrfbpfDAOImpl extends BaseDAOImpl<Zrfbpf> implements ZrfbpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZrfbpfDAOImpl.class);
	
	/**
	 * This method will insert record in Zrfbpf type table.
	 * 
	 */
	public void insertZrfbData(Zrfbpf zrfbData)
	{		
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ZRFBPF");
		sql.append("(AGNTCOY,ZRECRUIT,AGNTNUM,BATCACTYR,BATCACTMN,AGTYPE,CHDRNUM,LIFE,COVERAGE,RIDER,EFFDATE,TRANNO,ACCTAMT,ORIGCURR,BONUSAMT,PRCDATE,JOBNO,USRPRF,JOBNM,DATIME)");
		sql.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		int index = 1;
		 
		try {

			ps = getConnection().prepareStatement(sql.toString());
			ps.setString(index++,zrfbData.getAgntcoy().toString());
			ps.setString(index++,zrfbData.getZrecruit());
			ps.setString(index++,zrfbData.getAgntnum());			
			ps.setBigDecimal(index++,zrfbData.getBatcactyr());
			ps.setBigDecimal(index++,zrfbData.getBatcactmn());
			ps.setString(index++,zrfbData.getAgtype());
			ps.setString(index++,zrfbData.getChdrnum());
			ps.setString(index++,zrfbData.getLife());
			ps.setString(index++,zrfbData.getCoverage());
			ps.setString(index++,zrfbData.getRider());			
			ps.setInt(index++,zrfbData.getEffdate());	
			ps.setInt(index++,zrfbData.getTranno());		
			ps.setBigDecimal(index++,zrfbData.getAcctamt());
			ps.setString(index++,zrfbData.getOrigcurr());
			ps.setBigDecimal(index++,zrfbData.getBonusamt());
			ps.setBigDecimal(index++,zrfbData.getPrcdate());
			ps.setBigDecimal(index++,zrfbData.getJobno());
			ps.setString(index++,zrfbData.getUserProfile());
			ps.setString(index++,zrfbData.getJobName());
			ps.setTimestamp(index++,new Timestamp(System.currentTimeMillis()));
		    ps.executeUpdate();			 			
			
			
					
		} catch (SQLException e) {
			LOGGER.error("InsertzrfbData()", e);
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,null);
		}	
		
		
	}		
}