package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl61screensfl extends Subfile {


	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 11, 4, 57}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl61ScreenVars sv = (Sjl61ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sjl61screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sjl61screensfl, 
			sv.Sjl61screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl61ScreenVars sv = (Sjl61ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sjl61screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl61ScreenVars sv = (Sjl61ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sjl61screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl61screensflWritten.gt(0))
		{
			sv.Sjl61screensfl.setCurrentIndex(0);
			sv.Sjl61screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl61ScreenVars sv = (Sjl61ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sjl61screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl61ScreenVars screenVars = (Sjl61ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.aclntsel.setFieldName("aclntsel");
				screenVars.acltname.setFieldName("acltname");
				screenVars.agncynum.setFieldName("agncynum");				
				screenVars.regnum.setFieldName("regnum");				
				screenVars.startDateDisp.setFieldName("startDateDisp");
				screenVars.dateendDisp.setFieldName("dateendDisp");
				screenVars.validflag.setFieldName("validflag");
				screenVars.reasonmod.setFieldName("reasonmod");
				screenVars.resndesc.setFieldName("resndesc");				
				screenVars.status.setFieldName("status");				
				screenVars.regclass.setFieldName("regclass");
				screenVars.regdateDisp.setFieldName("regdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.slt.set(dm.getField("slt"));
			screenVars.aclntsel.set(dm.getField("aclntsel"));
			screenVars.acltname.set(dm.getField("acltname"));
			screenVars.agncynum.set(dm.getField("agncynum"));
			screenVars.regnum.set(dm.getField("regnum"));				
			screenVars.startDateDisp.set(dm.getField("startDateDisp"));
			screenVars.dateendDisp.set(dm.getField("dateendDisp"));
			screenVars.validflag.set(dm.getField("validflag"));
			screenVars.reasonmod.set(dm.getField("reasonmod"));
			screenVars.resndesc.set(dm.getField("resndesc"));
			screenVars.status.set(dm.getField("status"));				
			screenVars.regclass.set(dm.getField("regclass"));
			screenVars.regdateDisp.set(dm.getField("regdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl61ScreenVars screenVars = (Sjl61ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.aclntsel.setFieldName("aclntsel");
				screenVars.acltname.setFieldName("acltname");
				screenVars.agncynum.setFieldName("agncynum");				
				screenVars.regnum.setFieldName("regnum");				
				screenVars.startDateDisp.setFieldName("startDateDisp");
				screenVars.dateendDisp.setFieldName("dateendDisp");
				screenVars.validflag.setFieldName("validflag");
				screenVars.reasonmod.setFieldName("reasonmod");
				screenVars.resndesc.setFieldName("resndesc");				
				screenVars.status.setFieldName("status");				
				screenVars.regclass.setFieldName("regclass");
				screenVars.regdateDisp.setFieldName("regdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("slt").set(screenVars.slt);
			dm.getField("aclntsel").set(screenVars.aclntsel);
			dm.getField("acltname").set(screenVars.acltname);
			dm.getField("agncynum").set(screenVars.agncynum);
			dm.getField("regnum").set(screenVars.regnum);
			dm.getField("startDateDisp").set(screenVars.startDateDisp);
			dm.getField("dateendDisp").set(screenVars.dateendDisp);
			dm.getField("validflag").set(screenVars.validflag);
			dm.getField("reasonmod").set(screenVars.reasonmod);
			dm.getField("resndesc").set(screenVars.resndesc);
			dm.getField("status").set(screenVars.status);
			dm.getField("regclass").set(screenVars.regclass);
			dm.getField("regdateDisp").set(screenVars.regdateDisp);
		}

	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl61screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl61ScreenVars screenVars = (Sjl61ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.slt.clearFormatting();
		screenVars.aclntsel.clearFormatting();
		screenVars.acltname.clearFormatting();
		screenVars.agncynum.clearFormatting();
		screenVars.regnum.clearFormatting();				
		screenVars.startDateDisp.clearFormatting();
		screenVars.dateendDisp.clearFormatting();
		screenVars.validflag.clearFormatting();
		screenVars.reasonmod.clearFormatting();
		screenVars.resndesc.clearFormatting();
		screenVars.status.clearFormatting();				
		screenVars.regclass.clearFormatting();
		screenVars.regdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl61ScreenVars screenVars = (Sjl61ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.slt.setClassString("");
		screenVars.aclntsel.setClassString("");
		screenVars.acltname.setClassString("");
		screenVars.agncynum.setClassString("");
		screenVars.regnum.setClassString("");				
		screenVars.startDateDisp.setClassString("");
		screenVars.dateendDisp.setClassString("");
		screenVars.validflag.setClassString("");
		screenVars.reasonmod.setClassString("");
		screenVars.resndesc.setClassString("");
		screenVars.status.setClassString("");				
		screenVars.regclass.setClassString("");
		screenVars.regdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sjl61screensfl
 */
	public static void clear(VarModel pv) {
		Sjl61ScreenVars screenVars = (Sjl61ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.slt.clear();
		screenVars.aclntsel.clear();
		screenVars.acltname.clear();
		screenVars.agncynum.clear();		
		screenVars.regnum.clear();				
		screenVars.startDateDisp.clear();
		screenVars.dateendDisp.clear();
		screenVars.validflag.clear();
		screenVars.reasonmod.clear();
		screenVars.resndesc.clear();		
		screenVars.status.clear();				
		screenVars.regclass.clear();
		screenVars.regdateDisp.clear();
	}
}