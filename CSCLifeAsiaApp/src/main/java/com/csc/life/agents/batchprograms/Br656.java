/*
 * File: Br656.java
 * Date: 29 August 2009 22:33:27
 * Author: Quipoz Limited
 * 
 * Class transformed from BR656.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.dataaccess.ZbupTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This is to loop ZBUP and call LIFACMV to do account posting.
*   Remove the ZBUP record afger posting.
*
*   Initialise
*     Remove the default codes for parameter prompt records and
*     report printing. Setup primary file as ZBUP.
*     Initialize linkage for calling LIFACMV.
*     Read T5645 for the sub-account code, sub-account type and
*     GL account.
*
*    Read
*     Call the I/O module.
*
*      Edit
*       In this section we need to read CHDRLNB to prepare for
*       account posting.
*
*      Update
*       In this section we need to call LIFACMV twice to do account
*       posting (double-entry) as well as deleting the ZBUP record
*       after posting.
*
*      Close
*       No need to close printer file here as there is not any opened.
*
*   Control totals:
*       No of records read CT01inted
*       No of records posted CT02
*       No of records deleted CT03
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Br656 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR656");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String zbuprec = "ZBUPREC";
	private String itemrec = "ITEMREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String t5645 = "T5645";
	private String t1688 = "T1688";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5645rec t5645rec = new T5645rec();
		/*Bonus Workbench Upload Logical*/
	private ZbupTableDAM zbupIO = new ZbupTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}

	public Br656() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		zbupIO.setDataKey(SPACES);
		zbupIO.setPrcdate(ZERO);
		zbupIO.setFormat(zbuprec);
		zbupIO.setFunction(varcom.begn);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		initialize(lifacmvrec1.lifacmvRec);
		lifacmvrec1.batccoy.set(batcdorrec.company);
		lifacmvrec1.batcbrn.set(batcdorrec.branch);
		lifacmvrec1.batcactyr.set(batcdorrec.actyear);
		lifacmvrec1.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec1.batctrcde.set(batcdorrec.trcde);
		lifacmvrec1.batcbatch.set(batcdorrec.batch);
		lifacmvrec1.rldgcoy.set(batcdorrec.company);
		lifacmvrec1.genlcoy.set(batcdorrec.company);
		lifacmvrec1.postyear.set(SPACES);
		lifacmvrec1.postmonth.set(SPACES);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.contot.set(ZERO);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.termid.set(SPACES);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.function.set("PSTW");
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(bprdIO.getAuthCode());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(bsscIO.getLanguage());
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			lifacmvrec1.trandesc.set(SPACES);
		}
		else {
			lifacmvrec1.trandesc.set(getdescrec.longdesc);
		}
	}

protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, zbupIO);
		if (isNE(zbupIO.getStatuz(),varcom.oK)
		&& isNE(zbupIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zbupIO.getStatuz());
			syserrrec.params.set(zbupIO.getParams());
			fatalError600();
		}
		if (isEQ(zbupIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(zbupIO.getChdrcoy());
		chdrlnbIO.setChdrnum(zbupIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		lifacmvrec1.origamt.set(zbupIO.getOrigamt());
		lifacmvrec1.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.substituteCode[1].set(chdrlnbIO.getCnttype());
		lifacmvrec1.effdate.set(zbupIO.getPrcdate());
		lifacmvrec1.rdocnum.set(zbupIO.getChdrnum());
		lifacmvrec1.tranno.set(ZERO);
		lifacmvrec1.tranref.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(zbupIO.getChdrnum().toString());
		stringVariable1.append("-");
		stringVariable1.append(zbupIO.getZbntyp().toString());
		lifacmvrec1.tranref.setLeft(stringVariable1.toString());
		lifacmvrec1.sacscode.set(t5645rec.sacscode[1]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[1]);
		lifacmvrec1.glcode.set(t5645rec.glmap[1]);
		lifacmvrec1.glsign.set(t5645rec.sign[1]);
		lifacmvrec1.contot.set(t5645rec.cnttot[1]);
		lifacmvrec1.rldgcoy.set(zbupIO.getAgntcoy());
		lifacmvrec1.rldgacct.set(zbupIO.getAgntnum());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		lifacmvrec1.jrnseq.add(1);
		lifacmvrec1.sacscode.set(t5645rec.sacscode[2]);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype[2]);
		lifacmvrec1.glcode.set(t5645rec.glmap[2]);
		lifacmvrec1.glsign.set(t5645rec.sign[2]);
		lifacmvrec1.contot.set(t5645rec.cnttot[2]);
		lifacmvrec1.rldgcoy.set(zbupIO.getChdrcoy());
		lifacmvrec1.rldgacct.set(zbupIO.getChdrnum());
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.statuz.set(lifacmvrec1.statuz);
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		zbupIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, zbupIO);
		if (isNE(zbupIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zbupIO.getStatuz());
			syserrrec.params.set(zbupIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		zbupIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
