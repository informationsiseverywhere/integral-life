/*
 * File: PtrnpfDAOImpl.java
 * Date: March 17, 2016
 * Author: CSC
 * Created by: dpuhawan
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.dao;
 
import java.util.List;
import java.util.Map;

import com.csc.life.agents.dataaccess.model.Zldbpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZldbpfDAO extends BaseDAO<Zldbpf> {
	public boolean updateZldbPF(List<Zldbpf> zldbpfList);
	public boolean insertZldbPFRecord(List<Zldbpf> zldbpfList);
	public List<Zldbpf> getZldbpfList(Integer batcactyr,Integer batcactmn,String agntcoy,String agntnum,int flag);
	public Zldbpf readZldbpfData(Integer batcactyr,Integer batcactmn,String agntcoy,String agntnum,int flag);
   
}
