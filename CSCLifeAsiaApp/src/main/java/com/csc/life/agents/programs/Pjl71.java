package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.agents.screens.Sjl71ScreenVars;
import com.csc.life.agents.tablestructures.Tjl68rec;
import com.csc.smart.dataaccess.dao.UsrdDAO;
import com.csc.smart.dataaccess.model.Usrdpf;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl71 extends ScreenProgCS{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl71.class);
	private FixedLengthStringData wsaaAlocnoThereFlag = new FixedLengthStringData(1).init("N");
	private Validator alocnoAlreadyThere = new Validator(wsaaAlocnoThereFlag, "Y");
	private Sjl71ScreenVars sv =   getLScreenVars();
	private Batckey wsaaBatchkey = new Batckey();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL71");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private UsrdDAO usrdDAO = getApplicationContext().getBean("usrdDAO" , UsrdDAO.class);
	private Usrdpf usrdpf= new Usrdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private Agncypf agncypf;
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private Hierpf hierpf = new Hierpf();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Alocnorec alocnorec = new Alocnorec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private String longUserID;
	private String shortUserID;
	private static final String E186 = "E186";	
	private static final String JL90 = "JL90";	
	private static final String JL91 = "JL91";	
	private static final String JL94 = "JL94";
	private static final String JL89 = "JL89";
	private static final String E844 = "E844";
	private static final String E020 = "E020";
	private static final String F393 = "F393";
	private static final String JL92 = "JL92";
	private static final String JL38 = "JL38";
	private static final String JL99 = "JL99";
	private static final String RUCY = "RUCY";
	private static final String E207 = "E207";
	private static final String W516 = "W516";
	private static final String RUBN = "RUBN";
	private static final String RUCZ = "RUCZ";
	private static final String RUDT = "RUDT";
	private static final String JL88 = "JL88";
	private static final String K035 = "K035";
	private static final String RUFQ = "RUFQ";
	private static final String RUFR = "RUFR";
	private static final String JL96 = "JL96";
	private static final String JL93 = "JL93";
	private static final String RUCW = "RUCW";
	
	
	private Map<String, List<Itempf>> tjl68ListMap;
	private Tjl68rec tjl68rec = new Tjl68rec();	
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	public ZonedDecimalData date1 = new ZonedDecimalData(8, 0);
	
	boolean isExisted = true;
	
	public Pjl71() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl71", AppVars.getInstance(), sv);
	}
	final Sjl71ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sjl71ScreenVars.class);
	}
	
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
@Override
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
}
	@Override
	protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspcomn.clntkey.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsspcomn.lastprog.set("SJL71");
		longUserID = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString());
		shortUserID = UserMapingUtils.getShortUserIdValue(wsspcomn.userid.toString());
	}
	
	
	@Override
	protected void preScreenEdit() {
		return ;
		
	}
	
	@Override
	protected void screenEdit2000()
	{
		wsspcomn.edterror.set(Varcom.oK);
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)){
			if (isNE(scrnparams.statuz, "BACH"))
				validateKeys2200();
		if (isNE(sv.errorIndicators, SPACES)) 
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
 }
	protected void validateAction2100()
	{
		checkAgainstTable2110();
		checkSanctions2120();

	}

	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, Varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			exitProgram();
		}
	}

	
	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, Varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

	protected void validateKeys2200() {

		if (isEQ(subprogrec.key1, SPACES)) {
			return;
		}
		
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
		

		if (isNE(sv.salerepno, SPACE) && (isEQ(sv.action, "B")   || isEQ(sv.action, "E") || isEQ(sv.action, "F") || isEQ(sv.action, "G") || isEQ(sv.action, "D"))) {
			agsdpf = agsdpfDAO.getDatabyLevelclass(sv.salerepno.trim(),"1");
			
			if (null == agsdpf) {
				sv.salerepnoErr.set(JL91);
			} 
			
			
			if (null != agsdpf)
			{
				if(isNE(agsdpf.getActivestatus(),"1") || isNE(agsdpf.getValidflag(),"1")) 
				{
					sv.salerepnoErr.set(JL90);
				}
				
				usrdpf = usrdDAO.getUserid(agsdpf.getUserid().trim());
				
				if (null == usrdpf)
				{
					sv.salerepnoErr.set(E020);
				}

				if(null !=usrdpf)
				{

					if(isEQ(usrdpf.getValidflag(),"2"))
					{
						sv.salerepnoErr.set(JL94);
					}
					
					SimpleDateFormat yyyyMMdd = new SimpleDateFormat("yyyyMMdd");			
					date1.set(yyyyMMdd.format(usrdpf.getUenddate()));
					
					if(isGT(wsaaToday, date1)) 
						sv.salerepnoErr.set(JL94);
					
				}

				clntpf = new Clntpf();
				clntpf.setClntpfx(fsupfxcpy.clnt.toString());
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntnum(agsdpf.getClientno().trim()); 
				clntpf = clntpfDAO.selectActiveClient(clntpf);

				
				Optional<Clntpf> op1 = Optional.ofNullable(clntpf);

				if (!op1.isPresent()) 
				{	
					sv.salerepnoErr.set(F393);
				}
				else
				{
					if(!(clntpf.getCltstat().equals("AC")))
					{
						sv.salerepnoErr.set(JL89);
					}
				}
				
				if(isEQ(sv.action, "G")){
					if (agsdpf!=null && longUserID!= null && (isEQ(longUserID.trim(), agsdpf.getUserid().trim()))){
						sv.salerepnoErr.set(RUCW);
					}
				
				if (agsdpf!=null && shortUserID!= null && (isEQ(shortUserID.trim(), agsdpf.getUserid().trim()))){
					sv.salerepnoErr.set(RUCW);
				}
				}
				
			}
			
		}
		if ((isNE(sv.salebranch, SPACES)) && (isEQ(sv.action, "J") || isEQ(sv.action, "K"))) {
			String role = "";
			agsdpf = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
			if (null != agsdpf){
				if("1".equals(agsdpf.getLevelclass())) {
						sv.salebranchErr.set(JL99);
				}
				if("2".equals(agsdpf.getLevelclass())) {
					role = "SB";
					if(isNE(agsdpf.getActivestatus(),"1") && isEQ(agsdpf.getValidflag(),"1")) 
					{
						sv.salebranchErr.set(RUFQ);
					}
					if(isEQ(agsdpf.getValidflag(),"2")) {
						sv.salebranchErr.set(RUFQ);
					}
				}
				if("3".equals(agsdpf.getLevelclass())) {
					role = "SS";
					if(isNE(agsdpf.getActivestatus(),"1") && isEQ(agsdpf.getValidflag(),"1")) 
					{
						sv.salebranchErr.set(RUFR);
					}
					if(isEQ(agsdpf.getValidflag(),"2")) {
						sv.salebranchErr.set(RUFR);
					}
				}
			}
			if (null == agsdpf) {
					sv.salebranchErr.set(JL99);
			} 
			
			if(isEQ(sv.action, "K") && isEQ(sv.salebranchErr, SPACES)) {
				String levelno = role.concat(sv.salebranch.trim());
				Hierpf hrpf = hierpfDAO.getHierpfLvlNo(levelno, levelno, "1","1");
				Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
				if (isPrsnt.isPresent()) {
					sv.salebranchErr.set(RUCY);
				}
			}
		}
		
		
		
		if (isNE(sv.salerepno, SPACE) && isNE(sv.agncysel, SPACE) && isEQ(sv.action, "D"))
		{
			wsaaroleCode.set("AN");
			wsaahierCode.set(sv.agncysel.trim());
			hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim());
			Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
			if (isExists.isPresent() && isEQ(hierpf.getActivestatus(),"1")) {
				sv.agncyselErr.set(JL96);
			}
		}
		

		if (isNE(sv.agncysel, SPACE) && isEQ(sv.action, "D"))
		{
			agncypf=agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim(), 1);
			if(null != agncypf && isLTE(agncypf.getEndate(), wsaaToday))
			{
				sv.agncyselErr.set(JL92);
			}
		}
		
		if(isNE(sv.salebranch, SPACES) && isEQ(sv.action, "L")) {
			
			agsdpf = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
			if (null != agsdpf){
				if(isEQ(sv.salerepno, SPACES) && "1".equals(agsdpf.getLevelclass())){
					sv.salebranchErr.set(JL99);
				}
				if(isEQ(agsdpf.getValidflag(),"2")) {
					if("2".equals(agsdpf.getLevelclass())) {
						sv.salebranchErr.set(RUFQ);
					}
					if("3".equals(agsdpf.getLevelclass())) {
						sv.salebranchErr.set(RUFR);
					}
				}
			}else {
				sv.salebranchErr.set(JL99);
			}
		}
		
		if(isEQ(sv.action, "M")) {

			if(isEQ(sv.salebranch, SPACES) && isEQ(sv.salerepno, SPACES) && isEQ(sv.saledept, SPACES)
					&& isEQ(sv.userid, SPACES) && isEQ(sv.agncysel, SPACES)) {
				sv.salebranchErr.set(E207);
				sv.salerepnoErr.set(E207);
				sv.saledeptErr.set(E207);
				sv.useridErr.set(E207);
			}
			
			if(isNE(sv.salebranch, SPACES) && isNE(sv.salerepno, SPACES) && isNE(sv.saledept, SPACES) && isNE(sv.userid, SPACES)) {
				sv.salebranchErr.set(W516);
				sv.salerepnoErr.set(W516);
				sv.saledeptErr.set(W516);
				sv.useridErr.set(W516);
			}
			
			if(isNE(sv.salebranch, SPACES)) {
				if(isNE(sv.salerepno, SPACES)){
					sv.salebranchErr.set(W516);
					sv.salerepnoErr.set(W516);
				}
				if(isNE(sv.saledept, SPACES)){
					sv.salebranchErr.set(W516);
					sv.saledeptErr.set(W516);
				}
				if(isNE(sv.userid, SPACES)){
					sv.salebranchErr.set(W516);
					sv.useridErr.set(W516);
				}
				Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
				if(null != agsd) {
					if(isEQ(sv.salerepno, SPACES)){
						if("1".equals(agsd.getLevelclass())) {
							sv.salebranchErr.set(JL99);
						}
						if((!"1".equals(agsd.getValidflag())) && isEQ(sv.salebranchErr, SPACES)){
							if("2".equals(agsd.getLevelclass())) {
								sv.salebranchErr.set(RUFQ);
							}
							if("3".equals(agsd.getLevelclass())) {
								sv.salebranchErr.set(RUFR);
							}
						}
					}
				}
				else {
					if(isEQ(sv.salebranchErr, SPACES)) {
						sv.salebranchErr.set(JL99);
					}
				}
			}
			
			if(isNE(sv.salerepno, SPACES)) {
				if(isNE(sv.salebranch, SPACES)){
					sv.salebranchErr.set(W516);
					sv.salerepnoErr.set(W516);
				}
				if(isNE(sv.saledept, SPACES)){
					sv.salerepnoErr.set(W516);
					sv.saledeptErr.set(W516);
				}
				if(isNE(sv.userid, SPACES)){
					sv.salerepnoErr.set(W516);
					sv.useridErr.set(W516);
				}
				Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.salerepno.trim());
				if(null != agsd) {
					if(isEQ(sv.salebranch, SPACES) && "2".equals(agsd.getLevelclass())){
						sv.salerepnoErr.set(JL91);
					}
					if((!"1".equals(agsd.getValidflag()) && isEQ(sv.salerepnoErr, SPACES))){
						sv.salerepnoErr.set(JL90);
					}
				}
				else {
					if(isEQ(sv.salerepnoErr, SPACES)) {
						sv.salerepnoErr.set(JL91);
					}
				}
			}
			
			if(isNE(sv.saledept, SPACES)) {
				if(isNE(sv.salebranch, SPACES)){
					sv.salebranchErr.set(W516);
					sv.saledeptErr.set(W516);
				}
				if(isNE(sv.salerepno, SPACES)){
					sv.salerepnoErr.set(W516);
					sv.saledeptErr.set(W516);
				}
				if(isNE(sv.userid, SPACES)){
					sv.saledeptErr.set(W516);
					sv.useridErr.set(W516);
				}
				wsaaDeptcode.set(sv.saledept.toString());
				readTjl68();
				Agsdpf agsd = agsdpfDAO.getDatabyDept(wsaaSalediv.toString().trim());
				if(null != agsd) {
					return;
				}
				else {
					if(isEQ(sv.saledeptErr, SPACES)) {
						sv.saledeptErr.set(RUCZ);
					}
				}
			}
			
			if(isNE(sv.userid, SPACES)) {
				if(isNE(sv.salebranch, SPACES)){
					sv.salebranchErr.set(W516);
					sv.useridErr.set(W516);
				}
				if(isNE(sv.salerepno, SPACES)){
					sv.salerepnoErr.set(W516);
					sv.useridErr.set(W516);
				}
				if(isNE(sv.saledept, SPACES)){
					sv.saledeptErr.set(W516);
					sv.useridErr.set(W516);
				}
				Agsdpf agsd = agsdpfDAO.getDatabyUserId(sv.userid.toString().trim());
				if(null != agsd) {
					if(isEQ(agsd.getValidflag(),"2"))
					{
						sv.useridErr.set(JL94);
					}
				}
				else {
					if(isEQ(sv.useridErr, SPACES)) {
						sv.useridErr.set(E020);
					}
				}
			}
		}
		
		if(isEQ(sv.action, "N")) {
			
			if(isEQ(sv.salebranch, SPACES) && isEQ(sv.salerepno, SPACES)) {
				sv.salebranchErr.set(E207);
				sv.salerepnoErr.set(E207);
			}
			
			if(isNE(sv.salebranch, SPACES) && isNE(sv.salerepno, SPACES)) {
				sv.salebranchErr.set(W516);
				sv.salerepnoErr.set(W516);
			}
			
			if(isNE(sv.salebranch, SPACES)) {
				Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
				if(null != agsd) {
					if(isEQ(sv.salerepno, SPACES) && "1".equals(agsd.getLevelclass())){
						sv.salebranchErr.set(JL99);
					}
					if((!"1".equals(agsd.getValidflag()) || "3".equals(agsd.getRegisclass())) && isEQ(sv.salebranchErr, SPACES)){
						if("2".equals(agsd.getLevelclass())) {
							sv.salebranchErr.set(RUFQ);
						}
						if("3".equals(agsd.getLevelclass())) {
							sv.salebranchErr.set(RUFR);
						}
					}
					String role = "";
					if("2".equals(agsd.getLevelclass())) {
						role = "SB";
					}
					if("3".equals(agsd.getLevelclass())) {
						role = "SS";
					}
					String levelno = role.concat(sv.salebranch.trim());
					Hierpf hrpf = hierpfDAO.getHierpfLvlNo(levelno, levelno, "1","1");
					Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
					if (isPrsnt.isPresent() && isEQ(sv.salebranchErr, SPACES)) {
						sv.salebranchErr.set(RUCY);
					}
				}
				else {
						sv.salebranchErr.set(JL99);
				}
			}
			
			if(isNE(sv.salerepno, SPACES)) {
				Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.salerepno.trim());
				if(null != agsd) {
					if(isEQ(sv.salebranch, SPACES) && ("2".equals(agsd.getLevelclass()) || "3".equals(agsd.getLevelclass()))){
						sv.salerepnoErr.set(JL91);
					}
					if((!"1".equals(agsd.getValidflag()) || "3".equals(agsd.getRegisclass())) && isEQ(sv.salerepnoErr, SPACES)){
						sv.salerepnoErr.set(JL90);
					}
					String role = "";
					if("1".equals(agsd.getLevelclass())) {
						role = "SR";
					}
					String levelno = role.concat(sv.salerepno.trim());
					Hierpf hrpf = hierpfDAO.getHierpfLvlNo(levelno, levelno, "1","1");
					Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
					if (isPrsnt.isPresent() && isEQ(sv.salerepnoErr, SPACES)) {
						sv.salerepnoErr.set(RUCY);
					}
				}
				else {
						sv.salerepnoErr.set(JL91);
				}
			}
		}
		
		if(isEQ(sv.action, "H"))
		{
			if(isEQ(sv.salerepno, SPACES) && isNE(sv.agncysel, SPACES)) {
				
				if(isEQ(sv.agncysel, SPACES)) {
					sv.agncyselErr.set(E186);
				}
				agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim());
				if (null == agncypf) {
					sv.agncyselErr.set(JL93);
				}
				wsaaroleCode.set("AN");
				wsaahierCode.set(sv.agncysel.trim());
				Hierpf hier = hierpfDAO.getAgntkey(wsaaKey.toString(), "1");
				Optional<Hierpf> isExists = Optional.ofNullable(hier);
				if (isExists.isPresent() && null != hier.getUpperlevel()) {
						wsaaupLevel.set(hier.getUpperlevel());
						if(!"SR".equals(wsaauproleCode.toString())){
							sv.agncyselErr.set(RUDT);
						}
				}
				
			}
			if(isNE(sv.salerepno, SPACES) && isNE(sv.agncysel, SPACES))
			{
				sv.agncyselErr.set(K035);
				sv.salerepnoErr.set(K035);
			}
			
			if(isNE(sv.salerepno, SPACES))
			{
				Agsdpf	agsdpf1 = agsdpfDAO.getDatabyLevelclass(sv.salerepno.trim(),"1");
				
				if (null == agsdpf1) {
					sv.salerepnoErr.set(JL91);
				} 
			}
			if(isEQ(sv.salerepno, SPACES) && isEQ(sv.agncysel, SPACES)) {
				
				sv.agncyselErr.set(E207);
				sv.salerepnoErr.set(E207);
				
			}
		
		}
		 
		
		if (isNE(sv.salerepno, SPACE) && (isEQ(sv.action, "B")))
		{
			Hierpf hrpf = hierpfDAO.getHierpfStatus("SR".concat(sv.salerepno.toString()).trim() ,"1");
			Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
			if (isPrsnt.isPresent()) {
				sv.salebranchErr.set(JL88);
			}
		}
		
		if (isNE(sv.salerepno, SPACE) && (isEQ(sv.action, "C")))
		{
			Hierpf hrpf = hierpfDAO.getHierpfStatus("SR".concat(sv.salerepno.toString()).trim() ,"1");
			Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
			if (isPrsnt.isPresent()) {
				sv.salebranchErr.set(JL88);
			}
		}
		
		if (isEQ(sv.salerepno, SPACE) && (isEQ(sv.action, "B") || isEQ(sv.action, "C") || isEQ(sv.action, "D")
				|| isEQ(sv.action, "E") || isEQ(sv.action, "F")|| isEQ(sv.action, "G"))) {
			sv.salerepnoErr.set(E186);
		}
		
		if (isNE(sv.salerepno, SPACE) && (isEQ(sv.action, "A") || isEQ(sv.action, "I") 
				|| isEQ(sv.action, "J") || isEQ(sv.action, "K") || isEQ(sv.action, "L"))) {
			sv.salerepnoErr.set(E844);
		}
		
		if (isNE(sv.salerepno, SPACE) && isEQ(sv.action, "C")) {
			agsdpf = agsdpfDAO.getDatabyLevelclass(sv.salerepno.trim(),"1");
			
			if (null == agsdpf ) {
				sv.salerepnoErr.set(JL91);
			} 
			if (null != agsdpf && (isNE(agsdpf.getActivestatus(),"1") || isNE(agsdpf.getValidflag(),"1"))) {
				sv.salerepnoErr.set(JL90);
			} 
		}
		
		if(isNE(sv.agncysel, SPACE) &&  isEQ(sv.action, "D")){
			wsspcomn.chdrChdrnum.set(sv.agncysel.toString());
			agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim());
			if (null == agncypf || agncypf.getAgncynum().isEmpty()) {
				sv.agncyselErr.set(JL38);
			}
			else
			{
				if(isNE(agncypf.getValidFlag(),"1"))
					sv.agncyselErr.set(JL92);
			}
		}
		//ILB-1580 starts
		if(isNE(sv.salerepno, SPACE) && isEQ(sv.agncysel, SPACE) &&  isEQ(sv.action, "D")){
			wsspcomn.chdrChdrnum.set(SPACE);
		}
		//ILB-1580 ends
		if (isEQ(sv.salebranch, SPACE) && (isEQ(sv.action, "J") || isEQ(sv.action, "K") || isEQ(sv.action, "L"))) {
			sv.salebranchErr.set(E186);
		}
		
		if (isNE(sv.salebranch, SPACE) && (isEQ(sv.action, "A") || isEQ(sv.action, "B") 
				|| isEQ(sv.action, "C") || isEQ(sv.action, "D") || isEQ(sv.action, "E")
				|| isEQ(sv.action, "F") || isEQ(sv.action, "G") || isEQ(sv.action, "H") || isEQ(sv.action, "I"))) {
			sv.salebranchErr.set(E844);
		}
		
		if (isNE(sv.agncysel, SPACE) && (isEQ(sv.action, "I") || isEQ(sv.action, "G") 
				|| isEQ(sv.action, "F") || isEQ(sv.action, "E") || isEQ(sv.action, "J")
				||isEQ(sv.action, "K") || isEQ(sv.action, "L") || isEQ(sv.action, "M") ||isEQ(sv.action, "N"))) {
			sv.agncyselErr.set(E844);
		}
		
		if (isNE(sv.saledept, SPACE) && (isNE(sv.action, "M"))) {
			sv.saledeptErr.set(E844);
		}
		
		if (isNE(sv.userid, SPACE) && (isNE(sv.action, "M"))) {
			sv.useridErr.set(E844);
		}
	
	}
	
	protected void readTjl68()
    {
		tjl68ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "TJL68");
		String keyItemitem = sv.saledept.toString();
		if (null != tjl68ListMap && !tjl68ListMap.isEmpty()) {
			List<Itempf> itempfList = tjl68ListMap.get(keyItemitem.trim());
			if(itempfList != null) {
				Iterator<Itempf> iterator = itempfList.iterator();
				while (iterator.hasNext()) {
					Itempf itempf = iterator.next();
					if (itempf == null) {
						syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("TJL68").concat(wsaaProg.toString()));
						fatalError600();
					} else {
						tjl68rec.tjl68Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						wsaaArea.set(tjl68rec.aracde);
						wsaaBranch.set(tjl68rec.agntbr);
					}
				}
			}else {
				sv.saledeptErr.set(RUBN);
			}
		}else {
			if(isEQ(sv.saledeptErr, SPACES)) {
				sv.saledeptErr.set(RUBN);
			}
		}
    }

	
	protected void checkRuser2500()
	{
		begin2510();
	}

	protected void begin2510()
	{
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(Varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.agnt);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.salerepno);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
	}
	
	@Override
	protected void update3000()
	{
		updateWssp3010();
	}
	
	protected void updateWssp3010()
	{
		int i=1;
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			exitProgram();
		}
		wsspcomn.secProg[i++].set(subprogrec.nxt1prog);
		wsspcomn.secProg[i++].set(subprogrec.nxt2prog);
		wsspcomn.secProg[i++].set(subprogrec.nxt3prog);
		wsspcomn.secProg[i].set(subprogrec.nxt4prog);

		if (isEQ(scrnparams.action, "A")) {
			wsspcomn.flag.set("A");
		}
		else if(isEQ(scrnparams.action, "B")){
			wsspcomn.flag.set("B");
		}
		else if(isEQ(scrnparams.action, "C")){
			wsspcomn.flag.set("C");
		}
		else if(isEQ(scrnparams.action, "D")){
			wsspcomn.flag.set("D");
		}
		else if(isEQ(scrnparams.action, "E")){
			wsspcomn.flag.set("E");
		}
		else if(isEQ(scrnparams.action, "F")){
			wsspcomn.flag.set("F");
		}
		else if(isEQ(scrnparams.action, "G")){
			wsspcomn.flag.set("G");
		}
		else if(isEQ(scrnparams.action, "H")){
			wsspcomn.flag.set("H");
		}
		else if(isEQ(scrnparams.action, "I")){
			wsspcomn.flag.set("I");
		}
		else if(isEQ(scrnparams.action, "J")){
			wsspcomn.flag.set("J");
		}
		else if(isEQ(scrnparams.action, "K")){
			wsspcomn.flag.set("K");
		}
		else if(isEQ(scrnparams.action, "L")){
			wsspcomn.flag.set("L");
		}
		else if(isEQ(scrnparams.action, "M")){
			wsspcomn.flag.set("M");
		}
		else if(isEQ(scrnparams.action, "N")){
			wsspcomn.flag.set("N");
		}
		
		if (isEQ(sv.salerepno, SPACES)
		&& (isEQ(scrnparams.action, "A") || isEQ(scrnparams.action, "I"))){
			callAlocno3210();
			if(isEQ(sv.salerepno,SPACE)) {
				sv.salerepnoErr.set("F985");
			    wsspcomn.edterror.set("Y");
			}
		}
		
		if (isNE(sv.salerepno, SPACES)
				&& (isNE(scrnparams.action, "A") || isNE(scrnparams.action, "I"))){
			wsspcomn.chdrCownnum.set(sv.salerepno.trim());
		}
		if (isNE(sv.salebranch, SPACES)
				&& (isEQ(scrnparams.action, "J") || isEQ(scrnparams.action, "K") || isEQ(scrnparams.action, "L") || isEQ(scrnparams.action, "N"))){
			wsspcomn.chdrCownnum.set(sv.salebranch.trim());
		}
		
		if(isEQ(scrnparams.action, "M")) {
			if(isNE(sv.userid, SPACES)) {
				wsspcomn.wsspTaskId.set(sv.userid.trim());
				wsspcomn.chdrChdrnum.set(SPACES);
			}
			if(isNE(sv.salerepno, SPACES)) {
				wsspcomn.chdrChdrnum.set(sv.salerepno.trim());
				wsspcomn.wsspTaskId.set(SPACES);
			}
			if(isNE(sv.salebranch, SPACES)) {
				wsspcomn.chdrChdrnum.set(sv.salebranch.trim());
				wsspcomn.wsspTaskId.set(SPACES);
			}
			if(isNE(sv.saledept, SPACES)) {
				wsspcomn.chdrCownnum.set(wsaaSalediv.toString().trim());
			}
		}
		
		

		
		if(isEQ(scrnparams.action, "H"))
		{
			if(isEQ(sv.salerepno, SPACES) && isNE(sv.agncysel, SPACES))
			{
				wsspcomn.chdrCownnum.set(sv.agncysel.trim());
			}
			else if(isNE(sv.salerepno, SPACES) && isEQ(sv.agncysel, SPACES)) {
				wsspcomn.chdrCownnum.set(sv.salerepno.trim());
			}
		}
				
	
		if(isNE(sv.salerepno, SPACES)) {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("AG");
		if (isNE(sv.salerepno, SPACES)) {
			sftlockrec.entity.set(sv.salerepno);
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, Varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.salerepnoErr.set("G354");
		}
	}
	}

protected void callAlocno3210()
	{
		wsaaAlocnoThereFlag.set("N");
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set("SZ");
		alocnorec.company.set(wsspcomn.company);
		alocnorec.genkey.set(wsspcomn.branch);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, Varcom.oK)) {
			alocnorec.genkey.set(SPACES);
			callProgram(Alocno.class, alocnorec.alocnoRec);
		}
		if (isEQ(alocnorec.statuz, Varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, Varcom.oK)) {
			sv.salerepnoErr.set(alocnorec.statuz);
		}
		else {
			checkAlocno3230();
			if (alocnoAlreadyThere.isTrue()) {
				callAlocno3210();
			}
			else {
				sv.salerepno.set(alocnorec.alocNo.toString());
				wsspcomn.chdrCownnum.set(alocnorec.alocNo.toString());
			}
		}
	}

	protected void checkAlocno3230() {

		agsdpf = agsdpfDAO.getAgsdpfData(alocnorec.alocNo.toString(), "1", "1");
		
		if (agsdpf != null) {
			wsaaAlocnoThereFlag.set("Y");
		}
	}
	

protected void batching3080()
{
	if (isEQ(subprogrec.bchrqd, "Y")
	&& isEQ(sv.errorIndicators, SPACES)) {
		updateBatchControl3100();
	}
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void updateBatchControl3100()
{
	/*AUTOMATIC-BATCHING*/
	batcdorrec.function.set("AUTO");
	batcdorrec.tranid.set(wsspcomn.tranid);
	batcdorrec.batchkey.set(wsspcomn.batchkey);
	callProgram(Batcdor.class, batcdorrec.batcdorRec);
	if (isNE(batcdorrec.statuz, Varcom.oK)) {
		sv.actionErr.set(batcdorrec.statuz);
	}
	wsspcomn.batchkey.set(batcdorrec.batchkey);
	/*EXIT*/
}


	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
@Override
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		if(isEQ(sv.action, "N")){
			if(isNE(sv.salebranch, SPACE) && isEQ(sv.salerepno, SPACE)) {
				wsspcomn.programPtr.set(1);
			}
			if(isNE(sv.salerepno, SPACE) && isEQ(sv.salebranch, SPACE)) {
				wsspcomn.programPtr.set(2);
			}
		}
		if(isEQ(sv.action, "M")){
			if(isNE(sv.saledept, SPACE) && isEQ(sv.salerepno, SPACE) && isEQ(sv.salebranch, SPACE) && isEQ(sv.userid, SPACE)) {
				wsspcomn.programPtr.set(1);
			}
			else{
				wsspcomn.programPtr.set(2);
			}
		}
		

		if(isEQ(sv.action, "H"))
		{
			if(isEQ(sv.salerepno, SPACES) && isNE(sv.agncysel, SPACES))
			{
				wsspcomn.programPtr.set(1);
			}
			else if(isNE(sv.salerepno, SPACES) && isEQ(sv.agncysel, SPACES)) 
			{
				wsspcomn.programPtr.set(3);
			}
		}
		
	}
}
