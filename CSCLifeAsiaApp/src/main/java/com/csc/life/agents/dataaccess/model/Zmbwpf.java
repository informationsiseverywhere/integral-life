package com.csc.life.agents.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Zmbwpf implements Serializable {
	
	private static final long serialVersionUID = 6102331476215147911L;
	
	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;	
	private String agntcoy;
	private String agntnum;
	private BigDecimal batcactmn;
	private BigDecimal batcactyr;
	private BigDecimal comtot;
	private String datime;
	private String genlcur;
	private String jobName;
	private BigDecimal prcent;
	private String userProfile;
	
	public Zmbwpf() {
		super();
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public BigDecimal getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(BigDecimal batcactmn) {
		this.batcactmn = batcactmn;
	}
	public BigDecimal getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(BigDecimal batcactyr) {
		this.batcactyr = batcactyr;
	}
	public BigDecimal getComtot() {
		return comtot;
	}
	public void setComtot(BigDecimal comtot) {
		this.comtot = comtot;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public BigDecimal getPrcent() {
		return prcent;
	}
	public void setPrcent(BigDecimal prcent) {
		this.prcent = prcent;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
}
