package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl21rec Date: 03 February 2020 Author: vdivisala
 */
public class Tjl21rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl21Rec = new FixedLengthStringData(4);
	public FixedLengthStringData cmgwdattyp = DD.cmgwdattyp.copy().isAPartOf(tjl21Rec, 0);
	public FixedLengthStringData cmgwreason = DD.cmgwreason.copy().isAPartOf(tjl21Rec, 1);
	public FixedLengthStringData cmgwcngcls = DD.cmgwcngcls.copy().isAPartOf(tjl21Rec, 3);

	public void initialize() {
		COBOLFunctions.initialize(tjl21Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl21Rec.isAPartOf(baseString, Boolean.TRUE);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}