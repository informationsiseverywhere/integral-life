package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.List;
import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl49ScreenVars;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import java.util.ArrayList;
import com.csc.smart.recordstructures.Varcom;

public class Pjl49 extends ScreenProgCS{
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl49");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl49ScreenVars sv = ScreenProgram.getScreenVars(Sjl49ScreenVars.class);
	private FixedLengthStringData wsaaSdaAdditionalFields = new FixedLengthStringData(8);
	List<Agncypf> agncypfList = new ArrayList<>();
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private String Sjl49 = "Sjl49";  
	
	public Pjl49() {
		super();
		screenVars = sv;
		new ScreenModel(Sjl49, AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	
	@Override
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			throw e;
		}
	}
	
	@Override
	protected void initialise1000()
	{
		initialise1100();
		initSubfile1200();
		readAgencyForClient1300();
	}

	protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		wsaaSdaAdditionalFields.set(wsspwindow.additionalFields);
		sv.clntnum.set(wsspwindow.value);
		sv.cname.set(wsspwindow.confirmation);	
	}	
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(Sjl49, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}
	
	protected void readAgencyForClient1300()
	{
		agncypfList = agncypfDAO.getAgncyFromClient(wsspcomn.company.toString(), wsspwindow.value.toString().trim());
		wsaaCount.set(ZERO);
		loadSubfile1400();
		if (isEQ(wsaaCount, 1)) {
			wsspcomn.edterror.set(Varcom.oK);
			loadWsspValue2100();
			wsspcomn.sectionno.set("3000");
		}
	}
	
	protected void loadSubfile1400()
	{
		if (isEQ(wsaaCount,10)) {
			wsaaCount.set(20);
			return;
		}
		for(int iy=0 ; iy<agncypfList.size() ; iy++) {
				Clntpf clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), wsspwindow.value.toString());
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
				sv.slt.set(SPACES);
				sv.agncynum.set(agncypfList.get(iy).getAgncynum());
				sv.brnch.set(agncypfList.get(iy).getBrnch());
				sv.regnum.set(agncypfList.get(iy).getRegnum());
				sv.startDate.set(agncypfList.get(iy).getSrdate());
				sv.dateend.set(agncypfList.get(iy).getEndate());
				scrnparams.function.set(Varcom.sadd);
				processScreen(Sjl49, sv);
				if (isNE(scrnparams.statuz, Varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				wsaaCount.add(1);
			}
		scrnparams.subfileRrn.set(1); 
	}	
			
	protected void loadEmptySubfile1500()
	{
		sv.cltname.set(SPACES);
		sv.slt.set(SPACES);
		sv.agncynum.set(SPACES);
		sv.brnch.set(SPACES);
		sv.regnum.set(SPACES);
		sv.startDate.set(99999999);
		sv.dateend.set(99999999);
		scrnparams.function.set(Varcom.sadd);
		processScreen(Sjl49, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
	}
	
	protected void loadWsspValue2100()
	{
		wsspwindow.value.set(sv.agncynum);
	}
	
	@Override
	protected void preScreenEdit() {
		//exit
	}
	
	@Override
	protected void screenEdit2000()
	{
		screenIo2010();
		rollUp2100();
		validateSubfile2600();
		exit2090(); 
	}

	protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			exit2090();
		}
	}
	
	protected void rollUp2100()
	{
		if (isNE(scrnparams.statuz, Varcom.rolu)) {
			validateSubfile2600();
		}
		if(agncypfList.isEmpty())
		{
			loadSubfile5000();
			scrnparams.function.set(Varcom.norml);
			wsspcomn.edterror.set("Y");
			exit2090();
			if (isNE(sv.errorIndicators, SPACES)) {
				wsspcomn.edterror.set("Y");
			}
		}	
	}
	
	protected void validateSubfile2600()
	{
		scrnparams.function.set(Varcom.srnch);
		processScreen(Sjl49, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, Varcom.endp)) {
			if (isEQ(wsaaCount, 0) || isEQ(wsspwindow.value,sv.clntnum.trim())) {
				wsspwindow.value.set(SPACES);
				return ;
			}
			else {
				return ;
			}
		}
		if (isEQ(sv.slt, SPACES)) {
			validateSubfile2600();
		}
		loadWsspValue2100();
	}
	
	protected void exit2090()
	{
		//exit the function
	}
	
	protected void loadSubfile5000()
	{
		clearSubfile5100();
		start5200();
	}
	
	protected void clearSubfile5100()
	{
		if (agncypfList.isEmpty()) {
			start5200();
		}
		scrnparams.function.set(Varcom.sclr);
		processScreen("S2594", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
	}
	
	protected void start5200()
	{
		wsaaCount.set(0);
		if (isEQ(scrnparams.subfileMore, "Y")) {
			wsaaCount.add(1);
			loadSubfile1400();
		}
		scrnparams.subfileMore.set("N");
	}
	
	@Override
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
		/**     DECIDE WHICH TRANSACTION PROGRAM IS NEXT*/
	}
	
	@Override
	protected void whereNext4000()
	{
		nextProgram4100();
		hide4200();
	}
	
	protected void nextProgram4100()
	{
		if ((isEQ(sv.slt, SPACES))&& (isEQ(wsaaCount, ZERO) || (isEQ(scrnparams.statuz, Varcom.kill)))) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspwindow.value.set(SPACES);
			hide4200();
		}
		wsspcomn.programPtr.add(1);
	}
	
	protected void hide4200()
	{
		scrnparams.function.set("HIDEW");
		processScreen(Sjl49, sv);
		scrnparams.subfileMore.set("N");
	}

}
