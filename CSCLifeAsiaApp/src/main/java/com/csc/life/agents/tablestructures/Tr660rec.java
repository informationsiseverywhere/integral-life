package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR660REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr660rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr660Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zbntyp = new FixedLengthStringData(2).isAPartOf(tr660Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(tr660Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr660Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr660Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}