/******************** */
/*Author  :Liwei      */
/*Purpose :curd data for agpfpf       */
/*Date    :2018.10.26           */ 
package com.csc.life.agents.dataaccess.dao;

import com.csc.life.agents.dataaccess.model.Agpypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AgpypfDAO extends BaseDAO<Agpypf>  {
	public void insertAgpypfRecord(Agpypf agpypf);
}
