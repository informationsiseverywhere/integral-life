package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl58screen extends ScreenRecord {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl58ScreenVars sv = (Sjl58ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.sjl58screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl58ScreenVars screenVars = (Sjl58ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.company.setClassString("");
		screenVars.brnchcd.setClassString("");
		screenVars.brnchdesc.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.levelno.setClassString("");
		screenVars.leveltype.setClassString("");
		screenVars.leveldes.setClassString("");
		screenVars.saledept.setClassString("");
		screenVars.saledptdes.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.agtydesc.setClassString("");
		screenVars.uplevelno.setClassString("");
		screenVars.upleveldes.setClassString("");
		screenVars.status.setClassString("");
		screenVars.regdate.setClassString("");
		screenVars.reasonreg.setClassString("");
		screenVars.resndetl.setClassString("");
		screenVars.moddate.setClassString("");
		screenVars.deldate.setClassString("");
		screenVars.reasonmod.setClassString("");
		screenVars.reasondel.setClassString("");
	}

/**
 * Clear all the variables in Sjl58screen
 */
	public static void clear(VarModel pv) {
		Sjl58ScreenVars screenVars = (Sjl58ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.company.clear();
		screenVars.brnchcd.clear();
		screenVars.brnchdesc.clear();
		screenVars.aracde.clear();
		screenVars.aradesc.clear();
		screenVars.levelno.clear();
		screenVars.leveltype.clear();
		screenVars.leveldes.clear();
		screenVars.saledept.clear();
		screenVars.saledptdes.clear();
		screenVars.agtype.clear();
		screenVars.agtydesc.clear();
		screenVars.uplevelno.clear();
		screenVars.upleveldes.clear();
		screenVars.status.clear();
		screenVars.regdate.clear();
		screenVars.reasonreg.clear();
		screenVars.resndetl.clear();
		screenVars.moddate.clear();
		screenVars.deldate.clear();
		screenVars.reasonmod.clear();
		screenVars.reasondel.clear();
	}

}
