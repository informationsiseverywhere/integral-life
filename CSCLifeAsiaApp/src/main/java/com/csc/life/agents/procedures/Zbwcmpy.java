/*
 * File: Zbwcmpy.java
 * Date: 30 August 2009 2:55:19
 * Author: Quipoz Limited
 * 
 * Class transformed from ZBWCMPY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.agents.tablestructures.Tr661rec;
import com.csc.life.agents.tablestructures.Tr662rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*  ZBWCMPY - BONUS WORKBENCH COMPANY
*  *************************************************
*  - In the 000-MAINLINE SECTION, initialize the ZBWC-STATUZ
*    &  ZBWC-BW-COMPANY as EROR and SPACES respectively.
*
*  - Then, determine the ZBWC-FUNCTION; if  POL , read the
*    policy header file CHDRENQ to get the Agent number.
*
*  - Afterwards, read AGLF & AGNT to retrieve the agent type
*    and commission class.
*
*  - Read the Table TR662 by the key of the concatenation of    A
*    agent type and commission class
*
*  - And then read the TR661 using the TR662-ZDSTRCHNL
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*****************************************************************
* </pre>
*/
public class Zbwcmpy extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "ZBWCMPY";
	private FixedLengthStringData wsaaAgntNum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaAgntType = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaAgntClass = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaZdstrchnl = new FixedLengthStringData(6).init(SPACES);
		/* ERRORS */
	private String rl58 = "RL58";
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
		/* TABLES */
	private String tr661 = "TR661";
	private String tr662 = "TR662";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Tr661rec tr661rec = new Tr661rec();
	private Tr662rec tr662rec = new Tr662rec();
	private Varcom varcom = new Varcom();
	private Desckey wsaaDesckey = new Desckey();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit6090
	}

	public Zbwcmpy() {
		super();
	}

public void mainline(Object... parmArray)
	{
		zbwcmpyrec.bwcmpyRec = convertAndSetParam(zbwcmpyrec.bwcmpyRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			para010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubrname);
		zbwcmpyrec.statuz.set(varcom.oK);
		zbwcmpyrec.bwCompany.set(SPACES);
		if (isEQ(zbwcmpyrec.function,"POL")){
			getAgntnum1000();
			getAgnttype2000();
			getAgntcls3000();
		}
		else if (isEQ(zbwcmpyrec.function,"AGT")){
			getAgnttype2000();
			getAgntcls3000();
		}
		else{
			zbwcmpyrec.statuz.set("INVF");
			goTo(GotoLabel.exit090);
		}
		readTr6624000();
		readTr6615000();
		tr661rec.tr661Rec.set(itemIO.getGenarea());
		zbwcmpyrec.bwCompany.set(tr661rec.zbnwcoy);
		zbwcmpyrec.startDate.set(tr661rec.startDate);
		zbwcmpyrec.state.set(tr661rec.zstate);
		wsaaDesckey.set(SPACES);
		wsaaDesckey.descDesccoy.set(zbwcmpyrec.signonCompany);
		wsaaDesckey.descDesctabl.set(tr661);
		wsaaDesckey.descDescpfx.set(smtpfxcpy.item);
		wsaaDesckey.descDescitem.set(wsaaZdstrchnl);
		wsaaDesckey.descLanguage.set(zbwcmpyrec.language);
		a300GetDescription();
		zbwcmpyrec.bwCompanydesc.set(descIO.getLongdesc());
	}

protected void exit090()
	{
		exitProgram();
	}

protected void getAgntnum1000()
	{
		/*PARA*/
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setChdrcoy(zbwcmpyrec.signonCompany);
		chdrenqIO.setChdrnum(zbwcmpyrec.input);
		chdrenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		wsaaAgntNum.set(chdrenqIO.getAgntnum());
		/*EXIT*/
	}

protected void getAgnttype2000()
	{
		para2000();
	}

protected void para2000()
	{
		agntIO.setParams(SPACES);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(zbwcmpyrec.signonCompany);
		if (isEQ(zbwcmpyrec.function,"POL")) {
			agntIO.setAgntnum(wsaaAgntNum);
		}
		if (isEQ(zbwcmpyrec.function,"AGT")) {
			agntIO.setAgntnum(zbwcmpyrec.input);
			wsaaAgntNum.set(zbwcmpyrec.input);
		}
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		wsaaAgntType.set(agntIO.getAgtype());
	}

protected void getAgntcls3000()
	{
		/*PARA*/
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(zbwcmpyrec.signonCompany);
		aglfIO.setAgntnum(wsaaAgntNum);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		wsaaAgntClass.set(aglfIO.getAgentClass());
		/*EXIT*/
	}

protected void readTr6624000()
	{
		para4000();
	}

protected void para4000()
	{
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaAgntType.toString());
		stringVariable1.append(wsaaAgntClass.toString());
		wsaaZdstrchnl.setLeft(stringVariable1.toString());
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(zbwcmpyrec.signonCompany);
		itemIO.setItemtabl(tr662);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(wsaaZdstrchnl);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			readTr662Common6000();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			tr662rec.tr662Rec.set(SPACES);
			wsaaZdstrchnl.set(SPACES);
		}
		else {
			tr662rec.tr662Rec.set(itemIO.getGenarea());
			wsaaZdstrchnl.set(tr662rec.zdstrchnl);
		}
	}

protected void readTr6615000()
	{
		para5000();
	}

protected void para5000()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(zbwcmpyrec.signonCompany);
		itemIO.setItemtabl(tr661);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(wsaaZdstrchnl);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void readTr662Common6000()
	{
		try {
			para6000();
		}
		catch (GOTOException e){
		}
	}

protected void para6000()
	{
		wsaaAgntType.set("**");
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(wsaaAgntType.toString());
		stringVariable1.append(wsaaAgntClass.toString());
		wsaaZdstrchnl.setLeft(stringVariable1.toString());
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(zbwcmpyrec.signonCompany);
		itemIO.setItemtabl(tr662);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemitem(wsaaZdstrchnl);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(rl58);
			syserrrec.params.set(itemIO.getParams());
			goTo(GotoLabel.exit6090);
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		zbwcmpyrec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void a300GetDescription()
	{
		a310Init();
	}

protected void a310Init()
	{
		descIO.setParams(SPACES);
		descIO.setDataKey(wsaaDesckey);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}
}
