/******************************************************************************
 * File Name 		: Agorpf.java
 * Author			: smalchi2
 * Creation Date	: 12 January 2017
 * Project			: Integral Life
 * Description		: The Model Class for AGORPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.agents.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

public class Agslpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "AGSLPF";

	//member variables for columns
	private long uniqueNumber;
	private String agnum;
	private String clntsel;
	private String agtype;
	private String salelicensetype;
	private String tlaglicno;
	private Integer frmdate;
	private Integer todate;
	private String usrprf;
	private String jobnm;
	private String datime;

	// Constructor
	public Agslpf ( ) {};


	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getAgnum() {
		return agnum;
	}

	public void setAgnum(String agnum) {
		this.agnum = agnum;
	}

	public String getClntsel() {
		return clntsel;
	}

	public void setClntsel(String clntsel) {
		this.clntsel = clntsel;
	}

	public String getAgtype() {
		return agtype;
	}

	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}

	public String getSalelicensetype() {
		return salelicensetype;
	}

	public void setSalelicensetype(String salelicensetype) {
		this.salelicensetype = salelicensetype;
	}

	public String getTlaglicno() {
		return tlaglicno;
	}

	public void setTlaglicno(String tlaglicno) {
		this.tlaglicno = tlaglicno;
	}

	public Integer getFrmdate() {
		return frmdate;
	}

	public void setFrmdate(Integer frmdate) {
		this.frmdate = frmdate;
	}

	public Integer getTodate() {
		return todate;
	}

	public void setTodate(Integer todate) {
		this.todate = todate;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}


	public String getDatime() {
		return datime;
	}


	public void setDatime(String datime) {
		this.datime = datime;
	}


	@Override
	public String toString() {
		return "Agslpf [uniqueNumber=" + uniqueNumber + ", agnum=" + agnum + ", clntsel=" + clntsel + ", agtype="
				+ agtype + ", salelicensetype=" + salelicensetype + ", tlaglicno=" + tlaglicno + ", frmdate=" + frmdate
				+ ", todate=" + todate + ", usrprf=" + usrprf + ", jobnm=" + jobnm + ", datime=" + datime + "]";
	}


}
