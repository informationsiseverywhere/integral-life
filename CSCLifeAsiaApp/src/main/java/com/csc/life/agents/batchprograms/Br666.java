/*
 * File: Br666.java
 * Date: 29 August 2009 22:34:03
 * Author: Quipoz Limited
 * 
 * Class transformed from BR666.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.MacfTableDAM;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwmovrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.agents.tablestructures.Tr663rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not Delete>
*
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*   Extraction the agent movement records
*********************************************************
*
*   In INPUT-OUTPUT SECTION,  specify the output file
*   ZBNWPF in which the agent maintenance records are written.
*
*   Initialise
*     - In 1000-INITIALISE SECTION We override the Database file ZBNWPF
*       for records written.
*
*   Read
*     - In 2000-READ-FILE SECTION, we fetch every record from the
*       cursor and then process those required records
*
*     - In 2500-EDIT SECTION,for NOT new appointment,we
*       check if there is any change in the reporting hierarchy
*
*   Edit
*     - In 3000-UPDATE SECTION,to create termination record
*        for the old supervision relationship
*
*   Update
*     - In 4000-UPDATE SECTION, we close the output file ZBNWPF
*       and copy the file to the FTP library which is given in
*       process definition.
*
*   Control totals:
*     01  -  Number of records read
*     02  -  Number of new appointments
*     03  -  Number of hierarchy records changed
*     04  -  Number of hierarchy records not  terminated
*     04  -  Number of hierarchy relation terminated
*     05  -  Number of hierarchy relation created
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Br666 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlmacfpf1rs = null;
	private java.sql.PreparedStatement sqlmacfpf1ps = null;
	private java.sql.Connection sqlmacfpf1conn = null;
	private String sqlmacfpf1 = "";
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR666");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler3 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaHierarchyChanged = new FixedLengthStringData(1);
	private Validator hierarchyChanged = new Validator(wsaaHierarchyChanged, "Y");
	private Validator hierarchyUnchanged = new Validator(wsaaHierarchyChanged, "N");

	private FixedLengthStringData wsaaTermination = new FixedLengthStringData(1);
	private Validator termination = new Validator(wsaaTermination, "Y");

	private FixedLengthStringData wsaaReinstatement = new FixedLengthStringData(1);
	private Validator reinstatement = new Validator(wsaaReinstatement, "Y");

	private FixedLengthStringData wsaaNewAppointment = new FixedLengthStringData(1);
	private Validator newAppointment = new Validator(wsaaNewAppointment, "Y");
	private ZonedDecimalData wsaaOldEffdate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaOldZrptga = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgntMovement = new FixedLengthStringData(2);
	private String macfrec = "MACFREC";
	private String aglfrec = "AGLFREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String tr663 = "TR663";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler5, 5);

		/* SQL-MACFPF */
	private FixedLengthStringData macfrec1 = new FixedLengthStringData(26);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(macfrec1, 0);
	private PackedDecimalData sqlEffdate = new PackedDecimalData(8, 0).isAPartOf(macfrec1, 8);
	private PackedDecimalData sqlTranno = new PackedDecimalData(5, 0).isAPartOf(macfrec1, 13);
	private FixedLengthStringData sqlAgmvty = new FixedLengthStringData(2).isAPartOf(macfrec1, 16);
	private FixedLengthStringData sqlZrptga = new FixedLengthStringData(8).isAPartOf(macfrec1, 18);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agent Career History*/
	private MacfTableDAM macfIO = new MacfTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr663rec tr663rec = new Tr663rec();
	private Zbnwmovrec zbnwmovrec = new Zbnwmovrec();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090, 
		exit2590
	}

	public Br666() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		defineCursor1060();
	}

protected void initialise1010()
	{
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		wsaaCompany.set(bprdIO.getCompany());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaZbnwFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		zbnwpf.openOutput();
		wsspEdterror.set(varcom.oK);
	}

protected void defineCursor1060()
	{
		sqlmacfpf1 = " SELECT  AGNTNUM, EFFDATE, TRANNO, AGMVTY, ZRPTGA" +
" FROM   " + appVars.getTableNameOverriden("MACFPF") + " " +
" WHERE EFFDATE = ?" +
" AND AGNTCOY = ?" +
" ORDER BY AGNTNUM, TRANNO, EFFDATE";
		sqlerrorflag = false;
		try {
			sqlmacfpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.agents.dataaccess.MacfpfTableDAM());
			sqlmacfpf1ps = appVars.prepareStatementEmbeded(sqlmacfpf1conn, sqlmacfpf1, "MACFPF");
			appVars.setDBDouble(sqlmacfpf1ps, 1, wsaaEffectiveDate.toDouble());
			appVars.setDBString(sqlmacfpf1ps, 2, wsaaCompany.toString());
			sqlmacfpf1rs = appVars.executeQuery(sqlmacfpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		readTr6631100();
		/*EXIT*/
	}

protected void readTr6631100()
	{
		read1110();
	}

protected void read1110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(tr663);
		itemIO.setItemitem(bprdIO.getCompany());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr663rec.tr663Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case endOfFile2080: {
					endOfFile2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlmacfpf1rs.next()) {
				appVars.getDBObject(sqlmacfpf1rs, 1, sqlAgntnum);
				appVars.getDBObject(sqlmacfpf1rs, 2, sqlEffdate);
				appVars.getDBObject(sqlmacfpf1rs, 3, sqlTranno);
				appVars.getDBObject(sqlmacfpf1rs, 4, sqlAgmvty);
				appVars.getDBObject(sqlmacfpf1rs, 5, sqlZrptga);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct01);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		try {
			edit2510();
		}
		catch (GOTOException e){
		}
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		wsaaHierarchyChanged.set("N");
		wsaaTermination.set("N");
		wsaaReinstatement.set("N");
		wsaaNewAppointment.set("N");
		if (isLT(sqlTranno,2)) {
			newAppointment.setTrue();
			contotrec.totval.set(1);
			contotrec.totno.set(ct02);
			callContot001();
			goTo(GotoLabel.exit2590);
		}
		editMacf2600();
		if (hierarchyChanged.isTrue()) {
			contotrec.totval.set(1);
			contotrec.totno.set(ct03);
			callContot001();
		}
		else {
			contotrec.totval.set(1);
			contotrec.totno.set(ct04);
			callContot001();
			wsspEdterror.set(SPACES);
		}
	}

protected void editMacf2600()
	{
		edit2610();
	}

protected void edit2610()
	{
		macfIO.setParams(SPACES);
		macfIO.setAgntcoy(bprdIO.getCompany());
		macfIO.setAgntnum(sqlAgntnum);
		setPrecision(macfIO.getTranno(), 0);
		macfIO.setTranno(sub(sqlTranno,1));
		macfIO.setAgmvty(SPACES);
		macfIO.setEffdate(varcom.vrcmMaxDate);
		macfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		macfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");
		macfIO.setFormat(macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		if (isEQ(macfIO.getStatuz(),varcom.endp)
		|| isNE(macfIO.getAgntcoy(),bprdIO.getCompany())
		|| isNE(macfIO.getAgntnum(),sqlAgntnum)) {
			fatalError600();
		}
		if (isEQ(sqlAgmvty,"T")) {
			termination.setTrue();
		}
		if (isEQ(sqlAgmvty,"R")) {
			reinstatement.setTrue();
		}
		if (termination.isTrue()
		|| reinstatement.isTrue()
		|| isNE(macfIO.getZrptga(),sqlZrptga)) {
			hierarchyChanged.setTrue();
			wsaaOldZrptga.set(macfIO.getZrptga());
			wsaaOldEffdate.set(macfIO.getEffdate());
		}
		else {
			hierarchyUnchanged.setTrue();
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		readAgnt3300();
		readAglf3400();
		if (!newAppointment.isTrue()
		&& !reinstatement.isTrue()) {
			terminate3100();
		}
		if (!termination.isTrue()) {
			create3200();
		}
		/*EXIT*/
	}

protected void terminate3100()
	{
		update3110();
	}

protected void update3110()
	{
		zbnwmovrec.bnwmovRec.set(SPACES);
		zbnwmovrec.bnwmovTrcde.set("UPD");
		zbnwmovrec.bnwmovSource.set("External");
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("AGT");
		zbwcmpyrec.input.set(sqlAgntnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwmovrec.bnwmovCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwmovrec.bnwmovCompany.set(SPACES);
		}
		zbnwmovrec.bnwmovAgntnum.set(sqlAgntnum);
		zbnwmovrec.bnwmovProfile.set(tr663rec.zprofile);
		zbnwmovrec.bnwmovLevel.set("1");
		zbnwmovrec.bnwmovCnttype.set(SPACES);
		zbnwmovrec.bnwmovPlan.set(SPACES);
		zbnwmovrec.bnwmovRegion.set(SPACES);
		zbnwmovrec.bnwmovTranno.set(SPACES);
		zbnwmovrec.bnwmovRevlvl.set(SPACES);
		zbnwmovrec.bnwmovRevcode.set(SPACES);
		wsaaInptDate.set(wsaaOldEffdate);
		a100DateConversion();
		zbnwmovrec.bnwmovEffdate.set(wsaaConvDate);
		if (isEQ(wsaaOldZrptga,SPACES)) {
			zbnwmovrec.bnwmovSagntnum.set("**********");
			zbnwmovrec.bnwmovSprofile.set("***");
		}
		else {
			zbnwmovrec.bnwmovSagntnum.set(wsaaOldZrptga);
			zbnwmovrec.bnwmovSprofile.set(tr663rec.zprofile);
		}
		wsaaInptDate.set(aglfIO.getDteapp());
		a100DateConversion();
		zbnwmovrec.bnwmovStrtdate.set(wsaaConvDate);
		wsaaInptDate.set(sqlEffdate);
		a100DateConversion();
		zbnwmovrec.bnwmovTrmdate.set(wsaaConvDate);
		zbnwmovrec.bnwmovReason.set(SPACES);
		wsaaInptDate.set(sqlEffdate);
		a100DateConversion();
		zbnwmovrec.bnwmovChgdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct05);
		callContot001();
	}

protected void create3200()
	{
		update3210();
	}

protected void update3210()
	{
		callContot001();
		zbnwmovrec.bnwmovRec.set(SPACES);
		zbnwmovrec.bnwmovTrcde.set("CR");
		zbnwmovrec.bnwmovSource.set("External");
		zbwcmpyrec.bwcmpyRec.set(SPACES);
		zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
		zbwcmpyrec.language.set(bsscIO.getLanguage());
		zbwcmpyrec.function.set("AGT");
		zbwcmpyrec.input.set(sqlAgntnum);
		callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
		if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
			zbnwmovrec.bnwmovCompany.set(zbwcmpyrec.bwCompany);
		}
		else {
			zbnwmovrec.bnwmovCompany.set(SPACES);
		}
		zbnwmovrec.bnwmovAgntnum.set(sqlAgntnum);
		zbnwmovrec.bnwmovProfile.set(tr663rec.zprofile);
		zbnwmovrec.bnwmovLevel.set("1  ");
		zbnwmovrec.bnwmovCnttype.set(SPACES);
		zbnwmovrec.bnwmovPlan.set(SPACES);
		zbnwmovrec.bnwmovRegion.set(SPACES);
		zbnwmovrec.bnwmovTranno.set(SPACES);
		zbnwmovrec.bnwmovRevlvl.set(SPACES);
		zbnwmovrec.bnwmovRevcode.set(SPACES);
		wsaaInptDate.set(sqlEffdate);
		a100DateConversion();
		zbnwmovrec.bnwmovEffdate.set(wsaaConvDate);
		if (isEQ(sqlZrptga,SPACES)) {
			zbnwmovrec.bnwmovSagntnum.set("**********");
			zbnwmovrec.bnwmovSprofile.set("***");
		}
		else {
			zbnwmovrec.bnwmovSagntnum.set(sqlZrptga);
			zbnwmovrec.bnwmovSprofile.set(tr663rec.zprofile);
		}
		wsaaInptDate.set(aglfIO.getDteapp());
		a100DateConversion();
		zbnwmovrec.bnwmovStrtdate.set(wsaaConvDate);
		zbnwmovrec.bnwmovTrmdate.set("99/99/9999");
		zbnwmovrec.bnwmovReason.set(SPACES);
		wsaaInptDate.set(bsscIO.getEffectiveDate());
		a100DateConversion();
		zbnwmovrec.bnwmovChgdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct06);
		callContot001();
	}

protected void readAgnt3300()
	{
		/*READ*/
		agntIO.setParams(SPACES);
		agntIO.setAgntpfx(fsupfxcpy.agnt);
		agntIO.setAgntcoy(bprdIO.getCompany());
		agntIO.setAgntnum(sqlAgntnum);
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readAglf3400()
	{
		/*READ*/
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(bprdIO.getCompany());
		aglfIO.setAgntnum(sqlAgntnum);
		aglfIO.setFunction(varcom.readr);
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		appVars.freeDBConnectionIgnoreErr(sqlmacfpf1conn, sqlmacfpf1ps, sqlmacfpf1rs);
		zbnwpf.close();
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(wsaaStatuz.toString());
			stringVariable1.append(bprdIO.getRunLibrary().toString());
			stringVariable1.append(wsaaZbnwFn.toString());
			stringVariable1.append(wsaaFtpLibrary.toString());
			stringVariable1.append(wsaaFtpFile.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
	}

protected void a100DateConversion()
	{
		/*A101-CONVERSION*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A109-EXIT*/
	}

protected void a200PackString()
	{
		/*A201-PACK*/
		wsaaString.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovTrcde, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovSource, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovCompany, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovAgntnum, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovProfile, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovLevel, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovCnttype, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovPlan, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovRegion, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovTranno, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovRevlvl, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovRevcode, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovEffdate, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovSagntnum, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovSprofile, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovStrtdate, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovTrmdate, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovReason, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwmovrec.bnwmovChgdate, " "));
		wsaaString.setLeft(stringVariable1.toString());
		/*A209-EXIT*/
	}
}
