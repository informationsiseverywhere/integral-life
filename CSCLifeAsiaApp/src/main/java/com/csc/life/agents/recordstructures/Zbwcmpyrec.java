package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:16
 * Description:
 * Copybook name: ZBWCMPYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbwcmpyrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bwcmpyRec = new FixedLengthStringData(57);
  	public FixedLengthStringData signonCompany = new FixedLengthStringData(1).isAPartOf(bwcmpyRec, 0);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(bwcmpyRec, 1);
  	public FixedLengthStringData function = new FixedLengthStringData(3).isAPartOf(bwcmpyRec, 2);
  	public FixedLengthStringData input = new FixedLengthStringData(8).isAPartOf(bwcmpyRec, 5);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(bwcmpyRec, 13);
  	public FixedLengthStringData bwCompany = new FixedLengthStringData(3).isAPartOf(bwcmpyRec, 17);
  	public FixedLengthStringData bwCompanydesc = new FixedLengthStringData(30).isAPartOf(bwcmpyRec, 20);
  	public FixedLengthStringData state = new FixedLengthStringData(2).isAPartOf(bwcmpyRec, 50);
  	public PackedDecimalData startDate = new PackedDecimalData(8, 0).isAPartOf(bwcmpyRec, 52);


	public void initialize() {
		COBOLFunctions.initialize(bwcmpyRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bwcmpyRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}