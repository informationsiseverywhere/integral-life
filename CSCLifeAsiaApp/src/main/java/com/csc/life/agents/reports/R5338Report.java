package com.csc.life.agents.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R5338.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R5338Report extends SMARTReportLayout { 

	private ZonedDecimalData accbal = new ZonedDecimalData(18, 2);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData agnt = new FixedLengthStringData(8);
	private FixedLengthStringData agntname = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData dateReportVariable = new FixedLengthStringData(10);
	private FixedLengthStringData dtldesc = new FixedLengthStringData(30);
	private FixedLengthStringData dtlref = new FixedLengthStringData(30);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private FixedLengthStringData forattn = new FixedLengthStringData(30);
	private ZonedDecimalData newamnt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData orgamnt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData origcurr = new FixedLengthStringData(3);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pcode = new FixedLengthStringData(10);
	private FixedLengthStringData sacstyp = new FixedLengthStringData(2);
	private FixedLengthStringData statcurr = new FixedLengthStringData(3);
	private FixedLengthStringData stdat = new FixedLengthStringData(10);
	private FixedLengthStringData tagsusind = new FixedLengthStringData(1);
	private ZonedDecimalData taxamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData tdcchrg = new ZonedDecimalData(14, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totcbal = new ZonedDecimalData(18, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R5338Report() {
		super();
	}


	/**
	 * Print the XML for R5338d01
	 */
	public void printR5338d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 1, 10));
		dtldesc.setFieldName("dtldesc");
		dtldesc.setInternal(subString(recordData, 11, 30));
		dtlref.setFieldName("dtlref");
		dtlref.setInternal(subString(recordData, 41, 30));
		sacstyp.setFieldName("sacstyp");
		sacstyp.setInternal(subString(recordData, 71, 2));
		orgamnt.setFieldName("orgamnt");
		orgamnt.setInternal(subString(recordData, 73, 17));
		origcurr.setFieldName("origcurr");
		origcurr.setInternal(subString(recordData, 90, 3));
		newamnt.setFieldName("newamnt");
		newamnt.setInternal(subString(recordData, 93, 17));
		printLayout("R5338d01",			// Record name
			new BaseData[]{			// Fields:
				effdate,
				dtldesc,
				dtlref,
				sacstyp,
				orgamnt,
				origcurr,
				newamnt
			}
		);

	}

	/**
	 * Print the XML for R5338h01
	 */
	public void printR5338h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(3).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stdat.setFieldName("stdat");
		stdat.setInternal(subString(recordData, 1, 10));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		agnt.setFieldName("agnt");
		agnt.setInternal(subString(recordData, 42, 8));
		forattn.setFieldName("forattn");
		forattn.setInternal(subString(recordData, 50, 30));
		statcurr.setFieldName("statcurr");
		statcurr.setInternal(subString(recordData, 80, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 83, 30));
		agntname.setFieldName("agntname");
		agntname.setInternal(subString(recordData, 113, 30));
		tagsusind.setFieldName("tagsusind");
		tagsusind.setInternal(subString(recordData, 143, 1));
		addr1.setFieldName("addr1");
		addr1.setInternal(subString(recordData, 144, DD.cltaddr.length));/*pmujavadiya ILIFE-3212 starts*/
		addr2.setFieldName("addr2");
		addr2.setInternal(subString(recordData, 144+DD.cltaddr.length, DD.cltaddr.length));
		addr3.setFieldName("addr3");
		addr3.setInternal(subString(recordData, 144+DD.cltaddr.length*2, DD.cltaddr.length));
		addr4.setFieldName("addr4");
		addr4.setInternal(subString(recordData, 144+DD.cltaddr.length*3, DD.cltaddr.length));
		pcode.setFieldName("pcode");
		pcode.setInternal(subString(recordData, 144+DD.cltaddr.length*4, 10));/*pmujavadiya ILIFE-3212 ends*/
		printLayout("R5338h01",			// Record name
			new BaseData[]{			// Fields:
				stdat,
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr,
				agnt,
				forattn,
				statcurr,
				currdesc,
				agntname,
				tagsusind,
				addr1,
				addr2,
				addr3,
				addr4,
				pcode
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)},
				new Object[]{"ind02", indicArea.charAt(2)},
				new Object[]{"ind03", indicArea.charAt(3)}
			}
		);

		currentPrintLine.set(16);
	}

	/**
	 * Print the XML for R5338t01
	 */
	public void printR5338t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		totcbal.setFieldName("totcbal");
		totcbal.setInternal(subString(recordData, 1, 18));
		printLayout("R5338t01",			// Record name
			new BaseData[]{			// Fields:
				totcbal
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R5338t02
	 */
	public void printR5338t02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		taxamt.setFieldName("taxamt");
		taxamt.setInternal(subString(recordData, 1, 17));
		printLayout("R5338t02",			// Record name
			new BaseData[]{			// Fields:
				taxamt
			}
		);

	}

	/**
	 * Print the XML for R5338t03
	 */
	public void printR5338t03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		taxamt.setFieldName("taxamt");
		taxamt.setInternal(subString(recordData, 1, 17));
		printLayout("R5338t03",			// Record name
			new BaseData[]{			// Fields:
				taxamt
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5338t04
	 */
	public void printR5338t04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		taxamt.setFieldName("taxamt");
		taxamt.setInternal(subString(recordData, 1, 17));
		printLayout("R5338t04",			// Record name
			new BaseData[]{			// Fields:
				taxamt
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for R5338t05
	 */
	public void printR5338t05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		accbal.setFieldName("accbal");
		accbal.setInternal(subString(recordData, 1, 18));
		printLayout("R5338t05",			// Record name
			new BaseData[]{			// Fields:
				accbal
			}
//     Ticket ILIFE-1151(Batch L2AGENTSMT aborted)		
//			, new Object[] {			// indicators
//				new Object[]{"ind04", indicArea.charAt(4)}
//			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R5338t06
	 */
	public void printR5338t06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		accbal.setFieldName("accbal");
		accbal.setInternal(subString(recordData, 1, 18));
		printLayout("R5338t06",			// Record name
			new BaseData[]{			// Fields:
				accbal
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for R5338t07
	 */
	public void printR5338t07(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		tdcchrg.setFieldName("tdcchrg");
		tdcchrg.setInternal(subString(recordData, 1, 14));
		printLayout("R5338t07",			// Record name
			new BaseData[]{			// Fields:
				tdcchrg
			}
		);

		currentPrintLine.add(1);
	}


}
