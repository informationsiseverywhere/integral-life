package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM604
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm604ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(311);
	public FixedLengthStringData dataFields = new FixedLengthStringData(151).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData numofmbr = DD.numofmbr.copyToZonedDecimal().isAPartOf(dataFields,55);
	public FixedLengthStringData prems = new FixedLengthStringData(34).isAPartOf(dataFields, 62);
	public ZonedDecimalData[] prem = ZDArrayPartOfStructure(2, 17, 2, prems, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(prems, 0, FILLER_REDEFINE);
	public ZonedDecimalData prem01 = DD.prem.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData prem02 = DD.prem.copyToZonedDecimal().isAPartOf(filler,17);
	public FixedLengthStringData rpthead = DD.rpthead.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,146);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 151);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData numofmbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData premsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] premErr = FLSArrayPartOfStructure(2, 4, premsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(premsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prem01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData prem02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData rptheadErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 191);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] numofmbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData premsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] premOut = FLSArrayPartOfStructure(2, 12, premsOut, 0);
	public FixedLengthStringData[][] premO = FLSDArrayPartOfArrayStructure(12, 1, premOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(premsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prem01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] prem02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] rptheadOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sm604screenWritten = new LongData(0);
	public LongData Sm604protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm604ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, prem01, rpthead, numofmbr, prem02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, prem01Out, rptheadOut, numofmbrOut, prem02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, prem01Err, rptheadErr, numofmbrErr, prem02Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm604screen.class;
		protectRecord = Sm604protect.class;
	}

}
