package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class Sd5g3screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {31, 32, 64}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 21, 4, 76}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sd5g3ScreenVars sv = (Sd5g3ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sd5g3screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sd5g3screensfl, 
			sv.Sd5g3screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sd5g3ScreenVars sv = (Sd5g3ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sd5g3screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sd5g3ScreenVars sv = (Sd5g3ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sd5g3screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sd5g3screensflWritten.gt(0))
		{
			sv.sd5g3screensfl.setCurrentIndex(0);
			sv.Sd5g3screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sd5g3ScreenVars sv = (Sd5g3ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sd5g3screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5g3ScreenVars screenVars = (Sd5g3ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.salelicensetype.setFieldName("salelicensetype");
				screenVars.tlaglicno.setFieldName("tlaglicno");
				screenVars.frmdateDisp.setFieldName("frmdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.salelicensetype.set(dm.getField("salelicensetype"));
			screenVars.tlaglicno.set(dm.getField("tlaglicno"));
			screenVars.frmdateDisp.set(dm.getField("frmdateDisp"));
			screenVars.todateDisp.set(dm.getField("todateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sd5g3ScreenVars screenVars = (Sd5g3ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.salelicensetype.setFieldName("salelicensetype");
				screenVars.tlaglicno.setFieldName("tlaglicno");
				screenVars.frmdateDisp.setFieldName("frmdateDisp");
				screenVars.todateDisp.setFieldName("todateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("salelicensetype").set(screenVars.salelicensetype);
			dm.getField("tlaglicno").set(screenVars.tlaglicno);
			dm.getField("frmdateDisp").set(screenVars.frmdateDisp);
			dm.getField("todateDisp").set(screenVars.todateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sd5g3screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sd5g3ScreenVars screenVars = (Sd5g3ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.salelicensetype.clearFormatting();
		screenVars.tlaglicno.clearFormatting();
		screenVars.frmdateDisp.clearFormatting();
		screenVars.todateDisp.clearFormatting();
		
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sd5g3ScreenVars screenVars = (Sd5g3ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.salelicensetype.setClassString("");
		screenVars.tlaglicno.setClassString("");
		screenVars.frmdateDisp.setClassString("");
		screenVars.todateDisp.setClassString("");
		
	}

/**
 * Clear all the variables in Sd5g3screensfl
 */
	public static void clear(VarModel pv) {
		Sd5g3ScreenVars screenVars = (Sd5g3ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.salelicensetype.clear();
		screenVars.tlaglicno.clear();
		screenVars.frmdateDisp.clear();
		screenVars.todateDisp.clear();
	}
}
