package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl73ScreenVars;
import com.csc.life.agents.tablestructures.Tjl68rec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl73 extends ScreenProgCS{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl73.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL73");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private Sjl73ScreenVars sv = ScreenProgram.getScreenVars(Sjl73ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private Map<String,Descpf> descMap = new HashMap<>();
	private Agncypf agncypf;
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private Hierpf hierpf = new Hierpf();
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);

	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private static final String E186 = "E186";	
	private static final String JL91 = "JL91";
	private static final String HL01 = "HL01";
	private static final String JL97 = "JL97";
	private static final String JL96 = "JL96";
	private static final String JL38 = "JL38";
	private static final String JL92 = "JL92";
	
	private Tjl68rec tjl68rec = new Tjl68rec();	
	boolean isExisted = true;
	private static final String SALESREP = "Sales Representative";
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaatopLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaatoproleCode = new FixedLengthStringData(2).isAPartOf(wsaatopLevel, 0);
	private FixedLengthStringData wsaatophierCode = new FixedLengthStringData(8).isAPartOf(wsaatopLevel, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	
	public ZonedDecimalData date1 = new ZonedDecimalData(8, 0);
	
	private String wsaaGennum;

	public Pjl73() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl73", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}

	@Override
	protected void initialise1000() {
		
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		sv.regdate.set(varcom.vrcmMaxDate);
		sv.srdate.set(varcom.vrcmMaxDate);
		sv.endate.set(varcom.vrcmMaxDate);
		initialise1010();
		initialise1020();
		protect1020();
	}


	protected void initialise1010() {
		
		if (isEQ(wsspcomn.flag, "D")) {
			sv.levelno.set(wsspcomn.chdrCownnum);
			sv.leveltyp.set("1");
			sv.agncysel.set(wsspcomn.chdrChdrnum);
			loadLongDesc();
		} 
	}
	
	protected void loadLongDesc(){
		Map<String, String> itemMap = new HashMap<>();		
		itemMap.put(TJL70, "1");
		
		descMap = descdao.searchMultiDescpf(wsspcomn.company.toString(),
				wsspcomn.language.toString(), itemMap);
		if (!descMap.containsKey(TJL70)) {
			sv.leveldesc.set(SALESREP);
		} else {
			sv.leveldesc.set(descMap.get(TJL70).getLongdesc());
		}

	}
	
	private void initialise1020() {

		if (isEQ(wsspcomn.chdrCownnum, SPACE)) {
			sv.levelnoErr.set(JL91);
			return;
		}
		
		wsaaSalediv.set(SPACES);
		
		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getSurname() + " " + clntpf.getGivname());
			}
			
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.agntbr.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();
						
			}
			if (null != agsdpf.getLevelno())
				sv.levelno.set(agsdpf.getLevelno());
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltyp.set(agsdpf.getLevelclass());
				loadLongDesc();
			}
		} else {
			sv.levelnoErr.set(JL91);
		}
		
		if (isNE(wsspcomn.chdrChdrnum, SPACE)) {
			agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(), 1);
			Optional<Agncypf> isExist = Optional.ofNullable(agncypf);
			if (isExist.isPresent()) {
				if (null != agncypf.getAgncynum()){
					sv.agncysel.set(agncypf.getAgncynum());
					getAgncydesc(agncypf.getClntnum());			
				}
				if (null != agncypf.getRegnum())
					sv.regnum.set(agncypf.getRegnum());
				if (null != agncypf.getSrdate())
					sv.srdate.set(agncypf.getSrdate());
				if (null != agncypf.getEndate())
					sv.endate.set(agncypf.getEndate());
			} else {
				sv.agncyselErr.set(JL38);
			}
		}
		else{
			sv.agncysel.set(SPACES);
			sv.agncydesc.set(SPACES);
			sv.regnum.set(SPACES);
			sv.srdate.set(varcom.vrcmMaxDate);
			sv.endate.set(varcom.vrcmMaxDate);
		}
		
		if (isEQ(wsspcomn.flag, "P") || isEQ(wsspcomn.flag, "M")){
			
			wsaaroleCode.set("AN");
			wsaahierCode.set(sv.agncysel.trim());
			hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim(),"1","1","0");
			Optional<Hierpf> ifExists = Optional.ofNullable(hierpf);
			if (ifExists.isPresent()) {
				sv.regdate.set(hierpf.getEffectivedt());
				sv.reasonreg.set(hierpf.getReasoncd());
				sv.resndesc.set(hierpf.getReasondtl());
			
			}
			else
			{
				sv.regdate.set(SPACE);
				sv.reasonreg.set(SPACE);
				sv.resndesc.set(SPACE);
			}
		}

	}
	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.agntbr.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		
		
		descMap= descdao.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.agbrdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.agbrdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		

	}
	
	protected void readTjl68()
	{
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(TJL68);
		itempf.setItemitem(sv.saledept.trim());
	    itempf.setItemseq("  ");
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		
		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(TJL68).concat(wsaaProg.toString()));
			fatalError600();
		}else
		tjl68rec.tjl68Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
	}

	
	private void protect1020() {
		if (isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "Q")) {
			sv.agncyselOut[Varcom.pr.toInt()].set("Y");
		} else if (isEQ(wsspcomn.flag, "P")) {
			sv.agncyselOut[Varcom.pr.toInt()].set("Y");
			sv.regdateOut[Varcom.pr.toInt()].set("Y");
			sv.reasonregOut[Varcom.pr.toInt()].set("Y");
			sv.resndescOut[Varcom.pr.toInt()].set("Y");
		} 
	}
	

	protected void preScreenEdit() {
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "P")) 
			scrnparams.errorCode.set(JL97);
		
		if (isNE(wsspcomn.flag, "Q")) {
			sv.tranflag.set("Y");
		}
		else {
			sv.tranflag.set("N");
		}
	}
	
	@Override
	protected void screenEdit2000() {

		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			return;
		}
		
		validate2010();
		
		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	

	protected void getAgncydesc(String agncynum) {
		sv.agncydesc.set(SPACES);
		Clntpf clntpf1 = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), agncynum);
		if ("C".equals(clntpf1.getClttype())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf1.getLsurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf1.getLgivname(), "  ");
			stringVariable1.setStringInto(sv.agncydesc);
		} else if (isNE(clntpf1.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf1.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf1.getGivname(), "  ");
			stringVariable1.setStringInto(sv.agncydesc);
		} else {
			sv.agncydesc.set(clntpf1.getSurname());
		}
	}


	private void validate2010()
	{
		
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
		wsaaToday.set(datcon1rec.intDate);
		
		if (isEQ(wsspcomn.flag, "D"))
		 {
			if(isNE(sv.agncysel,SPACE)) {
				agncypf=agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim());
				if(agncypf==null || null == agncypf.getAgncynum()) {
					sv.agncyselErr.set(JL38);
					sv.agncydesc.set(SPACES);
				}
				else if(isNE(agncypf.getValidFlag(),"1")){
					sv.agncyselErr.set(JL92);
					sv.agncydesc.set(SPACES);
				}
				else {
					
					if(isLTE(agncypf.getEndate(), wsaaToday))
					{
						sv.agncyselErr.set(JL92);
					}
					
					getAgncydesc(agncypf.getClntnum());
					if (null != agncypf.getRegnum())
						sv.regnum.set(agncypf.getRegnum());
					else
						sv.regnum.set(SPACES);
					if (null != agncypf.getSrdate())
						sv.srdate.set(agncypf.getSrdate());
					else
						sv.srdate.set(varcom.vrcmMaxDate);
					if (null != agncypf.getEndate())
						sv.endate.set(agncypf.getEndate());
					else
						sv.endate.set(varcom.vrcmMaxDate);
					
					wsaaroleCode.set("AN");
					wsaahierCode.set(sv.agncysel.trim());
					hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim());
					Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
					if (isExists.isPresent() && isEQ(hierpf.getActivestatus(),"1")) {
							sv.agncyselErr.set(JL96);
					}
				}
			} else {
				sv.agncyselErr.set(E186);
				sv.agncydesc.set(SPACES);
				sv.regnum.set(SPACES);
				sv.srdate.set(varcom.vrcmMaxDate);
				sv.endate.set(varcom.vrcmMaxDate);
			}
			
		}
			
		if (isEQ(wsspcomn.flag, "D") || isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "Q"))
		{
			if (isEQ(sv.reasonreg, SPACE))
				sv.reasonregErr.set(E186);

			if (isEQ(sv.regdate, varcom.vrcmMaxDate) || isEQ(sv.regdate, SPACE))
				sv.regdateErr.set(E186);
			
			if (isNE(sv.regdate, varcom.vrcmMaxDate) && isGT(sv.regdate, wsaaToday)) 
				sv.regdateErr.set(HL01);

		}


		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

	}
	
	protected Clntpf cltsioCall2700(String clntNum) {

		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), clntNum);
	}
	
	@Override
	protected void update3000() {
		
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
			return;
		}

		if (isEQ(wsspcomn.flag, "D"))
			updateDatabase3010();
		else if (isEQ(wsspcomn.flag, "M"))
			updateDatabase3020();
		else if (isEQ(wsspcomn.flag, "P")) 
			updateDatabase3030();
		else if (isEQ(wsspcomn.flag, "Q")) 
			updateDatabase3040();
			
	}

	// Add Agency
	private void updateDatabase3010() {
		
		wsaaroleCode.set("AN");
		wsaahierCode.set(sv.agncysel.trim());
		wsaatoproleCode.set("SR");
		wsaatophierCode.set(sv.levelno.trim());
		wsaauproleCode.set("SR");
		wsaauphierCode.set(sv.levelno.trim());
		
		hierpf = new Hierpf();
		hierpf.setAgentkey(wsaaKey.toString());
		hierpf.setToplevel(wsaatopLevel.toString());
		hierpf.setUpperlevel(wsaaupLevel.toString());
		hierpf.setApprovestatus("0");
		hierpf.setEffectivedt(sv.regdate.toInt());
		hierpf.setGenerationno("1");
		hierpf.setReasoncd(sv.reasonreg.toString());
		hierpf.setReasondtl(sv.resndesc.toString());
		hierpf.setRegisclass("1");
		hierpf.setActivestatus("1");
		hierpf.setValidflag("1");
		try {
			hierpfDAO.insertHierpfDetails(hierpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert record in HIERPF :", ex);
		}
		
	}

	// Modify Before Approval
	private void updateDatabase3020() {
		
		wsaaroleCode.set("AN");
		wsaahierCode.set(sv.agncysel.trim());
		hierpf = new Hierpf();
		hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim(),"1","1","0");
		Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
		if (isExists.isPresent()) {
			hierpf.setEffectivedt(sv.regdate.toInt());
			hierpf.setReasoncd(sv.reasonreg.toString());
			hierpf.setReasondtl(sv.resndesc.toString());
			try {
				hierpfDAO.updateHierpf(hierpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update HIERPF :", ex);
			}
		}
		
		
	}

	// Delete Before Approval
	private void updateDatabase3030() {
		
		wsaaroleCode.set("AN");
		wsaahierCode.set(sv.agncysel.trim());
		hierpf = new Hierpf();
		hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim(),"1","1","0");
		Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
		if (isExists.isPresent()) {
			hierpf.setValidflag("2");
			try {
				hierpfDAO.updateHierpf(hierpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update HIERPF :", ex);
			}
		}
		
	}
	
	// Delete After Approval
	private void updateDatabase3040() {
		
		wsaaroleCode.set("AN");
		wsaahierCode.set(sv.agncysel.trim());
		hierpf = new Hierpf();
		hierpf = hierpfDAO.getHierpfData(wsaaKey.toString().trim(),"1","1","1");
		Optional<Hierpf> isExists = Optional.ofNullable(hierpf);
		if (isExists.isPresent()) {
			
			int wsaaGen=Integer.parseInt(hierpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
			
			hierpf.setEffectivedt(sv.regdate.toInt());
			hierpf.setGenerationno(wsaaGennum);
			hierpf.setReasoncd(sv.reasonreg.toString());
			hierpf.setReasondtl(sv.resndesc.toString());
			hierpf.setValidflag("1");
			hierpf.setActivestatus("1");
			hierpf.setApprovestatus("0");
			hierpf.setRegisclass("3");

			try {
				hierpfDAO.insertHierpfDetails(hierpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to insert in HIERPF :", ex);
			}

		}
	
	}


	
	@Override
	protected void whereNext4000() {
		wsspcomn.programPtr.add(1);
	}

}
