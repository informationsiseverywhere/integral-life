package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: ZbnwpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:58
 * Class transformed from ZBNWPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZbnwpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 1500;
	public FixedLengthStringData zbnwrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zbnwpfRecord = zbnwrec;
	
	public FixedLengthStringData zbnwrec1 = DD.zbnwrec.copy().isAPartOf(zbnwrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZbnwpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for ZbnwpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZbnwpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZbnwpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZbnwpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZbnwpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZbnwpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZBNWPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ZBNWREC, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     zbnwrec1,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		zbnwrec1.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZbnwrec() {
  		return zbnwrec;
	}

	public FixedLengthStringData getZbnwpfRecord() {
  		return zbnwpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZbnwrec(what);
	}

	public void setZbnwrec(Object what) {
  		this.zbnwrec.set(what);
	}

	public void setZbnwpfRecord(Object what) {
  		this.zbnwpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zbnwrec.getLength());
		result.set(zbnwrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}