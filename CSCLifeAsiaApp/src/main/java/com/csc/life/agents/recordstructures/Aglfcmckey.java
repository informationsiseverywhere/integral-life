package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:06
 * Description:
 * Copybook name: AGLFCMCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfcmckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfcmcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglfcmcKey = new FixedLengthStringData(256).isAPartOf(aglfcmcFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfcmcAgntcoy = new FixedLengthStringData(1).isAPartOf(aglfcmcKey, 0);
  	public FixedLengthStringData aglfcmcAgntnum = new FixedLengthStringData(8).isAPartOf(aglfcmcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(aglfcmcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfcmcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfcmcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}