package com.csc.life.agents.dataaccess;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: ZmbwpfTableDAM.java
 * Date: Wed Jul 17 16:29:05 SGT 2013
 * Class transformed from ZmbwPF
 * Author: csc Limited
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class ZmbwpfTableDAM extends PFAdapterDAM {
	/*********************************************************************
	 * total up the record length as thew aggregate of all field lengths */
	public int pfRecLen = 88;
	public FixedLengthStringData zmbwrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmbwpfRecord = zmbwrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zmbwrec); 
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zmbwrec); 
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(zmbwrec); 
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(zmbwrec); 
	public PackedDecimalData comtot = DD.comtot.copy().isAPartOf(zmbwrec); 
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmbwrec); 
	public FixedLengthStringData genlcur = DD.genlcur.copy().isAPartOf(zmbwrec); 
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmbwrec); 
	public PackedDecimalData prcent = DD.prcent.copy().isAPartOf(zmbwrec); 
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmbwrec); 
	private static final Logger LOGGER = LoggerFactory.getLogger(ZmbwpfTableDAM.class);//IJTI-318
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmbwpfTableDAM() {
			super();
  		setColumns();
  		journalled = true;
  		LOGGER.info("CONSTRUCTOR DEFAULT FOR Zmbw");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmbwpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
  		LOGGER.info("CONSTRUCTOR LOCK FOR Zmbw");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmbwpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
  		LOGGER.info("INFDS CONSTRUCTOR FOR Zmbw");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmbwpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMBWPF");
		LOGGER.info("SETTING TABLE FOR Zmbw");//IJTI-318
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
		QUALIFIEDCOLUMNS = 
								"AGNTCOY, " + 
								"AGNTNUM, " + 
								"BATCACTMN, " + 
								"BATCACTYR, " + 
								"COMTOT, " + 
								"DATIME, " + 
								"GENLCUR, " + 
								"JOBNM, " + 
								"PRCENT, " + 
								"USRPRF, " + 
								"UNIQUE_NUMBER";
							
	}

	public void setColumns() {
		qualifiedColumns = new BaseData[] { 
								agntcoy, 
								agntnum, 
								batcactmn, 
								batcactyr, 
								comtot, 
								datime, 
								genlcur, 
								jobName, 
								prcent, 
								userProfile, 
								unique_number  };

	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
		
								agntcoy.clear(); 
								agntnum.clear(); 
								batcactmn.clear(); 
								batcactyr.clear(); 
								comtot.clear(); 
								datime.clear(); 
								genlcur.clear(); 
								jobName.clear(); 
								prcent.clear(); 
								userProfile.clear(); 

	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmbwrec() {
  		return zmbwrec;
	}

	public FixedLengthStringData getZmbwpfRecord() {
  		return zmbwpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmbwrec(what);
	}

	public void setZmbwrec(Object what) {
  		this.zmbwrec.set(what);
	}

	public void setZmbwpfRecord(Object what) {
  		this.zmbwpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmbwrec.getLength());
		result.set(zmbwrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}
