/*
 * File: PXXXX.java
 * Date: {{dd-mm-yyyy}}
 * Author: Autoated Code template
 * 
 * Class transformed from {{template name}}
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;


import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
//import com.csc.smart400framework.utility.Validator;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.framework.util.log.QPLogger;

import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.life.agents.tablestructures.Tr58rrec;
import com.csc.smart.recordstructures.Wsspsmart;

import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.life.agents.screens.Sr58rScreenVars;


import com.csc.smart400framework.parent.ScreenProgCS;
//import com.csc.life.newbusiness.procedures.Tr58rpt;
import com.csc.life.agents.procedures.Tr58rpt;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
/********************************************
 * includes for IO classes
*******************************************/


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Pr58r extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58R");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();

	private FixedLengthStringData errors = new FixedLengthStringData(12);
private FixedLengthStringData F838 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("F838");


        /*********************************************
         * Add the Table Data Record - should always be Txxxxrec 
		 */
	private Tr58rrec tr58rrec = new Tr58rrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58rScreenVars sv = ScreenProgram.getScreenVars( Sr58rScreenVars.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(Pr58r.class);//IJTI-318

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr58r() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58r", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

/*protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
	}*/

protected void initialise1000()
{
	GotoLabel nextMethod = GotoLabel.DEFAULT;
	while (true) {
		try {
			switch (nextMethod) {
			case DEFAULT: {
				initialise1010();
				readRecord1031();
				moveToScreen1040();
			}
			case generalArea1045: {
				generalArea1045();
			}
			}
			break;
		}
		catch (GOTOException e){
			nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		sv.itmfrm.set(itemIO.getItmfrm());
		sv.itmto.set(itemIO.getItmto());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr58rrec.tr58rRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
                // set for every Field in the Screen
                // Make sure  the screen Fields match the table record &
                // will use reflection
                // put a method here to set it
		//sv.adsc.set(tr58rrec.adsc);
	}

protected void generalArea1045()
{
	if (isEQ(itemIO.getItmfrm(),0)) {
		sv.itmfrm.set(varcom.vrcmMaxDate);
	}
	else {
		sv.itmfrm.set(itemIO.getItmfrm());
	}
	if (isEQ(itemIO.getItmto(),0)) {
		sv.itmto.set(varcom.vrcmMaxDate);
	}
	else {
		sv.itmto.set(itemIO.getItmto());
	}
	
	sv.minpcnt.set(tr58rrec.minpcnt);
}

protected void preScreenEdit()
	{
 		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}

	}


protected void screenEdit2000()
{
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(wsspcomn.flag,"I"))
	{
          return;
        }
        //*********************************
        // Put all your validation code here - same as ORD 
        
        try{
			//BeanMapper.copytoSv(tr58rrec, sv);	// changed to BeanMapper after checked with Danny
        	
        	
		}catch(Exception e){
			LOGGER.info("ErrorCheck: " + e.getMessage());//IJTI-318
		}


        //******** if any errors found set this 
        if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}

	itemIO.setParams(SPACES);
itemIO.setItempfx("IT");
itemIO.setItemcoy("0");
itemIO.setItemtabl("T1693");
itemIO.setItemitem(sv.company); 
itemIO.setFunction(varcom.readr);

itemIO.execute();

if (isNE(itemIO.getStatuz(),varcom.oK)) {
	sv.company.setReverse(BaseScreenData.REVERSED);
	sv.company.setColor(BaseScreenData.RED);
	sv.companyErr.set(F838);
	wsspcomn.edterror.set("Y");
}

	
}



protected void update3000()
{
         if (isEQ(wsspcomn.flag,"I")) {
           return;
         }
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")) {
		wsaaUpdateFlag = "Y";
	}
	checkChanges3100();
	if (isNE(wsaaUpdateFlag,"Y")) {
           return;
	}
	updatePrimaryRecord3050();
	updateRecord3055();

}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tr58rrec.tr58rRec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		//if (isNE(sv.adsc,tr58rrec.adsc)) {
			//tr58rrec.adsc.set(sv.adsc);
			//wsaaUpdateFlag = "Y";
		//}
		/*EXIT*/
	if (isNE(sv.minpcnt, tr58rrec.minpcnt)) {
		tr58rrec.minpcnt.set(sv.minpcnt);
		wsaaUpdateFlag = "Y";	
	}
	
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr58rpt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
