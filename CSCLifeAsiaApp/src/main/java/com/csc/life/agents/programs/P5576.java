/*
 * File: P5576.java
 * Date: 30 August 2009 0:31:12
 * Author: Quipoz Limited
 * 
 * Class transformed from P5576.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.screens.S5576ScreenVars;
import com.csc.life.agents.tablestructures.T5576rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5576 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5576");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";

	private ZonedDecimalData wsaa1Flag = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private Validator moreThanOneFlag = new Validator(wsaa1Flag, new ValueRange("2", "9"));
	private String h354 = "H354";
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private T5576rec t5576rec = new T5576rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5576ScreenVars sv = ScreenProgram.getScreenVars( S5576ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5576() {
		super();
		screenVars = sv;
		new ScreenModel("S5576", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5576rec.t5576Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5576rec.agemax01.set(ZERO);
		t5576rec.agemax02.set(ZERO);
		t5576rec.agemax03.set(ZERO);
		t5576rec.agemax04.set(ZERO);
		t5576rec.agemax05.set(ZERO);
		t5576rec.agemax06.set(ZERO);
		t5576rec.agemax07.set(ZERO);
		t5576rec.agemax08.set(ZERO);
		t5576rec.age01.set(ZERO);
		t5576rec.age02.set(ZERO);
		t5576rec.age03.set(ZERO);
		t5576rec.age04.set(ZERO);
		t5576rec.age05.set(ZERO);
		t5576rec.age06.set(ZERO);
		t5576rec.age07.set(ZERO);
		t5576rec.age08.set(ZERO);
		t5576rec.annumpc01.set(ZERO);
		t5576rec.annumpc02.set(ZERO);
		t5576rec.annumpc03.set(ZERO);
		t5576rec.annumpc04.set(ZERO);
		t5576rec.annumpc05.set(ZERO);
		t5576rec.annumpc06.set(ZERO);
		t5576rec.annumpc07.set(ZERO);
		t5576rec.annumpc08.set(ZERO);
		t5576rec.flatpcnt01.set(ZERO);
		t5576rec.flatpcnt02.set(ZERO);
		t5576rec.flatpcnt03.set(ZERO);
		t5576rec.flatpcnt04.set(ZERO);
		t5576rec.flatpcnt05.set(ZERO);
		t5576rec.flatpcnt06.set(ZERO);
		t5576rec.flatpcnt07.set(ZERO);
		t5576rec.flatpcnt08.set(ZERO);
		t5576rec.maxpcnt01.set(ZERO);
		t5576rec.maxpcnt02.set(ZERO);
		t5576rec.maxpcnt03.set(ZERO);
		t5576rec.maxpcnt04.set(ZERO);
		t5576rec.maxpcnt05.set(ZERO);
		t5576rec.maxpcnt06.set(ZERO);
		t5576rec.maxpcnt07.set(ZERO);
		t5576rec.maxpcnt08.set(ZERO);
		t5576rec.minpcnt01.set(ZERO);
		t5576rec.minpcnt02.set(ZERO);
		t5576rec.minpcnt03.set(ZERO);
		t5576rec.minpcnt04.set(ZERO);
		t5576rec.minpcnt05.set(ZERO);
		t5576rec.minpcnt06.set(ZERO);
		t5576rec.minpcnt07.set(ZERO);
		t5576rec.minpcnt08.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.actualday.set(t5576rec.actualday);
		sv.agemaxs.set(t5576rec.agemaxs);
		sv.ages.set(t5576rec.ages);
		sv.annumpcs.set(t5576rec.annumpcs);
		sv.flatpcnts.set(t5576rec.flatpcnts);
		sv.highmth.set(t5576rec.highmth);
		sv.highyr.set(t5576rec.highyr);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.lowmth.set(t5576rec.lowmth);
		sv.lowyr.set(t5576rec.lowyr);
		sv.maxpcnts.set(t5576rec.maxpcnts);
		sv.minpcnts.set(t5576rec.minpcnts);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		wsaa1Flag.set(0);
		if (isNE(sv.actualday,SPACES)) {
			wsaa1Flag.add(1);
		}
		if (isNE(sv.lowmth,SPACES)) {
			wsaa1Flag.add(1);
		}
		if (isNE(sv.lowyr,SPACES)) {
			wsaa1Flag.add(1);
		}
		if (isNE(sv.highmth,SPACES)) {
			wsaa1Flag.add(1);
		}
		if (isNE(sv.highyr,SPACES)) {
			wsaa1Flag.add(1);
		}
		if (moreThanOneFlag.isTrue()) {
			sv.actualdayErr.set(h354);
			sv.lowmthErr.set(h354);
			sv.lowyrErr.set(h354);
			sv.highmthErr.set(h354);
			sv.highyrErr.set(h354);
		}
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			preparation3010();
			updatePrimaryRecord3050();
			updateRecord3055();
		}
		catch (GOTOException e){
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		checkChanges3100();
		itmdIO.setItemGenarea(t5576rec.t5576Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*OTHER*/
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.actualday,t5576rec.actualday)) {
			t5576rec.actualday.set(sv.actualday);
		}
		if (isNE(sv.agemaxs,t5576rec.agemaxs)) {
			t5576rec.agemaxs.set(sv.agemaxs);
		}
		if (isNE(sv.ages,t5576rec.ages)) {
			t5576rec.ages.set(sv.ages);
		}
		if (isNE(sv.annumpcs,t5576rec.annumpcs)) {
			t5576rec.annumpcs.set(sv.annumpcs);
		}
		if (isNE(sv.flatpcnts,t5576rec.flatpcnts)) {
			t5576rec.flatpcnts.set(sv.flatpcnts);
		}
		if (isNE(sv.highmth,t5576rec.highmth)) {
			t5576rec.highmth.set(sv.highmth);
		}
		if (isNE(sv.highyr,t5576rec.highyr)) {
			t5576rec.highyr.set(sv.highyr);
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
		}
		if (isNE(sv.lowmth,t5576rec.lowmth)) {
			t5576rec.lowmth.set(sv.lowmth);
		}
		if (isNE(sv.lowyr,t5576rec.lowyr)) {
			t5576rec.lowyr.set(sv.lowyr);
		}
		if (isNE(sv.maxpcnts,t5576rec.maxpcnts)) {
			t5576rec.maxpcnts.set(sv.maxpcnts);
		}
		if (isNE(sv.minpcnts,t5576rec.minpcnts)) {
			t5576rec.minpcnts.set(sv.minpcnts);
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
