package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:19
 * Description:
 * Copybook name: ZPTNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zptnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zptnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zptnKey = new FixedLengthStringData(256).isAPartOf(zptnFileKey, 0, REDEFINE);
  	public FixedLengthStringData zptnChdrcoy = new FixedLengthStringData(1).isAPartOf(zptnKey, 0);
  	public FixedLengthStringData zptnChdrnum = new FixedLengthStringData(8).isAPartOf(zptnKey, 1);
  	public FixedLengthStringData zptnLife = new FixedLengthStringData(2).isAPartOf(zptnKey, 9);
  	public FixedLengthStringData zptnCoverage = new FixedLengthStringData(2).isAPartOf(zptnKey, 11);
  	public FixedLengthStringData zptnRider = new FixedLengthStringData(2).isAPartOf(zptnKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(241).isAPartOf(zptnKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zptnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zptnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}