package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:17
 * Description:
 * Copybook name: ZCTNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zctnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zctnFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zctnKey = new FixedLengthStringData(256).isAPartOf(zctnFileKey, 0, REDEFINE);
  	public FixedLengthStringData zctnAgntcoy = new FixedLengthStringData(1).isAPartOf(zctnKey, 0);
  	public FixedLengthStringData zctnAgntnum = new FixedLengthStringData(8).isAPartOf(zctnKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(zctnKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zctnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zctnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}