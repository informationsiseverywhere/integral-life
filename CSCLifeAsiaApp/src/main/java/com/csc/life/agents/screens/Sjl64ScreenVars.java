package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl64protect;
import com.csc.life.agents.screens.Sjl64screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sjl64ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(949);
	public FixedLengthStringData dataFields = new FixedLengthStringData(485).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData tclntsel = DD.clntsel.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData tcltname = DD.cltname.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData tlevelno = DD.salelevel.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData tleveltype = DD.salelvltyp.copy().isAPartOf(dataFields,263);
	public FixedLengthStringData tleveldes = DD.saleveldes.copy().isAPartOf(dataFields,264);
	public FixedLengthStringData tagtype = DD.agtype.copy().isAPartOf(dataFields,294);
	public FixedLengthStringData uclntsel = DD.clntsel.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData ucltname = DD.cltname.copy().isAPartOf(dataFields,306);
	public FixedLengthStringData ulevelno = DD.salelevel.copy().isAPartOf(dataFields,353);
	public FixedLengthStringData uleveltype = DD.salelvltyp.copy().isAPartOf(dataFields,361);
	public FixedLengthStringData uleveldes = DD.saleveldes.copy().isAPartOf(dataFields,362);
	public FixedLengthStringData uagtype = DD.agtype.copy().isAPartOf(dataFields,392);
	public FixedLengthStringData history = DD.history.copy().isAPartOf(dataFields,394);
	public FixedLengthStringData agtdesc = DD.agtydesc.copy().isAPartOf(dataFields,395);
	public FixedLengthStringData tagtdesc = DD.agtydesc.copy().isAPartOf(dataFields,425);
	public FixedLengthStringData uagtdesc = DD.agtydesc.copy().isAPartOf(dataFields,455);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(116).isAPartOf(dataArea, 485);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData tclntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData tcltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData tlevelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData tleveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData tleveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData tagtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData uclntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ucltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ulevelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData uleveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData uleveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 94);
	public FixedLengthStringData uagtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData historyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData agtdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData tagtdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData uagtdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(348).isAPartOf(dataArea, 601); 
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] tclntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] tcltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] tlevelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] tleveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] tleveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] tagtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] uclntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ucltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ulevelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] uleveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] uleveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] uagtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] historyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] agtdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] tagtdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] uagtdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(243);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(128).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData lclntsel = DD.clntsel.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData lcltname = DD.cltname.copy().isAPartOf(subfileFields,11);
	public FixedLengthStringData llevelno = DD.salelevel.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData lleveldes = DD.saleveldes.copy().isAPartOf(subfileFields,66);
	public FixedLengthStringData lagtype = DD.agtype.copy().isAPartOf(subfileFields,96);
	public FixedLengthStringData lagtdesc = DD.agtydesc.copy().isAPartOf(subfileFields,98);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(28).isAPartOf(subfileArea, 128);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData lclntselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData lcltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData llevelnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData lleveldesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData lagtypeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData lagtdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(84).isAPartOf(subfileArea, 156);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] lclntselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] lcltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] llevelnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] lleveldesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] lagtypeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] lagtdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 240);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
//	public FixedLengthStringData numsel =  new FixedLengthStringData(10);	
	
	public LongData Sjl64screensflWritten = new LongData(0);
	public LongData Sjl64screenctlWritten = new LongData(0);
	public LongData Sjl64screenWritten = new LongData(0);
	public LongData Sjl64protectWritten = new LongData(0);
	public GeneralTable sjl64screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return false;
	}

	public Sjl64ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		
		fieldIndMap.put(clntselOut,
				new String[] { "01", "02" , "-01", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "03", "04" , "-03", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "05", "06" , "-05", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "07", "08" , "-07", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "09", "10" , "-09", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "11", "12" , "-11", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "13", "14" , "-13", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "15", "16" , "-15", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypeOut,
				new String[] { "17", "18" , "-17", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldesOut,
				new String[] { "19", "20" , "-19", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "21", "22" , "-21", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "23", "24" , "-23", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "25", "26" , "-25", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tclntselOut,
				new String[] { "27", "28" , "-27", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tcltnameOut,
				new String[] { "29", "30" , "-29", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tlevelnoOut,
				new String[] { "31", "32" , "-31", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tleveltypeOut,
				new String[] { "33", "34" , "-33", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tleveldesOut,
				new String[] { "35", "36" , "-35", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tagtypeOut,
				new String[] { "37", "38" , "-37", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(uclntselOut,
				new String[] { "39", "40" , "-39", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(ucltnameOut,
				new String[] { "41", "42" , "-41", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(ulevelnoOut,
				new String[] { "43", "44" , "-43", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(uleveltypeOut,
				new String[] { "45", "46" , "-45", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(uleveldesOut,
				new String[] { "47", "48" , "-47", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(uagtypeOut,
				new String[] { "49", "50" , "-49", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(historyOut,
				new String[] { "51", "52" , "-51", "53" , null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtdescOut,
				new String[] { "54", "55" , "-54", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(tagtdescOut,
				new String[] { "56", "57" , "-56", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(uagtdescOut,
				new String[] { "58", "59" , "-58", null , null, null, null, null, null, null, null, null });
		
		screenFields = new BaseData[] { clntsel, cltname, company, brnchcd, brnchdesc, aracde, aradesc, levelno, leveltype, leveldes, saledept, saledptdes, agtype,
										tclntsel, tcltname, tlevelno, tleveltype, tleveldes, tagtype, uclntsel, ucltname, ulevelno, uleveltype, uleveldes, uagtype, history, 
										agtdesc, tagtdesc, uagtdesc };
		screenOutFields = new BaseData[][] { clntselOut, cltnameOut, companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, levelnoOut, leveltypeOut, leveldesOut, saledeptOut, saledptdesOut,
										agtypeOut, tclntselOut, tcltnameOut, tlevelnoOut, tleveltypeOut, tleveldesOut, tagtypeOut, uclntselOut, ucltnameOut, ulevelnoOut, 
										uleveltypeOut, uleveldesOut, uagtypeOut, historyOut, agtdescOut, tagtdescOut, uagtdescOut };
		screenErrFields = new BaseData[] { clntselErr, cltnameErr, companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, levelnoErr, leveltypeErr, leveldesErr, saledeptErr, saledptdesErr,
										agtypeErr, tclntselErr, tcltnameErr, tlevelnoErr, tleveltypeErr, tleveldesErr, tagtypeErr, uclntselErr, ucltnameErr, ulevelnoErr, uleveltypeErr, uleveldesErr, 
										uagtypeErr, historyErr, agtdescErr, tagtdescErr, uagtdescErr };
										
		screenSflFields = new BaseData[] {slt, lclntsel, lcltname, llevelno, leveldes, lagtype,lagtdesc };
		screenSflOutFields = new BaseData[][] {sltOut, lclntselOut, lcltnameOut, llevelnoOut, lleveldesOut, lagtypeOut, lagtdescOut };
		screenSflErrFields = new BaseData[] {sltErr, lclntselErr, lcltnameErr, llevelnoErr, lleveldesErr, lagtypeErr, lagtdescErr };
		
		screenDateFields = new BaseData[] {};
		screenSflDateFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorSflInds = errorSubfile;
		screenSflRecord = Sjl64screensfl.class;
		screenCtlRecord = Sjl64screenctl.class;
		initialiseSubfileArea();
		screenRecord = Sjl64screen.class;
		protectRecord = Sjl64protect.class;
	}
	
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl64screenctl.lrec.pageSubfile);
	}
}