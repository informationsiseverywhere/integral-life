package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl49screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {7, 11, 4, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl49ScreenVars sv = (Sjl49ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.Sjl49screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.Sjl49screensfl, 
			sv.Sjl49screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sjl49ScreenVars sv = (Sjl49ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.Sjl49screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sjl49ScreenVars sv = (Sjl49ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.Sjl49screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sjl49screensflWritten.gt(0))
		{
			sv.Sjl49screensfl.setCurrentIndex(0);
			sv.Sjl49screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sjl49ScreenVars sv = (Sjl49ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.Sjl49screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) 
			av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl49ScreenVars screenVars = (Sjl49ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.agncynum.setFieldName("agncynum");
				screenVars.brnch.setFieldName("brnch");
				screenVars.cltname.setFieldName("cltname");
				screenVars.regnum.setFieldName("regnum");
				screenVars.startDateDisp.setFieldName("startDateDisp");
				screenVars.dateendDisp.setFieldName("dateendDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.slt.set(dm.getField("slt"));
			screenVars.agncynum.set(dm.getField("agncynum"));
			screenVars.brnch.set(dm.getField("brnch"));
			screenVars.cltname.set(dm.getField("cltname"));
			screenVars.regnum.set(dm.getField("regnum"));
			screenVars.startDateDisp.set(dm.getField("startDateDisp"));
			screenVars.dateendDisp.set(dm.getField("dateendDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sjl49ScreenVars screenVars = (Sjl49ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.slt.setFieldName("slt");
				screenVars.agncynum.setFieldName("agncynum");
				screenVars.brnch.setFieldName("brnch");
				screenVars.cltname.setFieldName("cltname");
				screenVars.regnum.setFieldName("regnum");
				screenVars.startDateDisp.setFieldName("startDateDisp");
				screenVars.dateendDisp.setFieldName("dateendDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("slt").set(screenVars.slt);
			dm.getField("agncynum").set(screenVars.agncynum);
			dm.getField("brnch").set(screenVars.brnch);
			dm.getField("cltname").set(screenVars.cltname);
			dm.getField("regnum").set(screenVars.regnum);
			dm.getField("startDateDisp").set(screenVars.startDateDisp);
			dm.getField("dateendDisp").set(screenVars.dateendDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sjl49screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sjl49ScreenVars screenVars = (Sjl49ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.slt.clearFormatting();
		screenVars.agncynum.clearFormatting();
		screenVars.brnch.clearFormatting();
		screenVars.cltname.clearFormatting();
		screenVars.regnum.clearFormatting();
		screenVars.startDateDisp.clearFormatting();
		screenVars.dateendDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sjl49ScreenVars screenVars = (Sjl49ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.slt.setClassString("");
		screenVars.agncynum.setClassString("");
		screenVars.brnch.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.regnum.setClassString("");
		screenVars.startDateDisp.setClassString("");
		screenVars.dateendDisp.setClassString("");
	}

/**
 * Clear all the variables in Sjl49screensfl
 */
	public static void clear(VarModel pv) {
		Sjl49ScreenVars screenVars = (Sjl49ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.slt.clear();
		screenVars.agncynum.clear();
		screenVars.brnch.clear();
		screenVars.cltname.clear();
		screenVars.regnum.clear();
		screenVars.startDateDisp.clear();
		screenVars.dateendDisp.clear();
	}
}
