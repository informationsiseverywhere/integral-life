package com.csc.life.agents.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr58orec extends ExternalData {

	public FixedLengthStringData tr58oRec = new FixedLengthStringData(148); 

        

public FixedLengthStringData oppcs = new FixedLengthStringData(10).isAPartOf(tr58oRec,0);
public FixedLengthStringData[] oppc = FLSArrayPartOfStructure(2,5,oppcs, 0);
public ZonedDecimalData oppc01 = new ZonedDecimalData(5,2).isAPartOf(oppcs, 0);

public ZonedDecimalData oppc02 = new ZonedDecimalData(5,2).isAPartOf(oppcs, 5);

public FixedLengthStringData prcnts = new FixedLengthStringData(30).isAPartOf(tr58oRec,10);
public FixedLengthStringData[] prcnt = FLSArrayPartOfStructure(6,5,prcnts, 0);
public ZonedDecimalData[] prct = ZDArrayPartOfArrayStructure(5, 2, prcnt, 0);
public ZonedDecimalData prcnt01 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 0);

public ZonedDecimalData prcnt02 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 5);

public ZonedDecimalData prcnt03 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 10);

public ZonedDecimalData prcnt04 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 15);

public ZonedDecimalData prcnt05 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 20);

public ZonedDecimalData prcnt06 = new ZonedDecimalData(5,2).isAPartOf(prcnts, 25);

public FixedLengthStringData totamnts = new FixedLengthStringData(108).isAPartOf(tr58oRec,40);
public FixedLengthStringData[] totamnt = FLSArrayPartOfStructure(6,18,totamnts, 0);
public ZonedDecimalData[] totamt = ZDArrayPartOfArrayStructure(18, 2, totamnt, 0);
public ZonedDecimalData totamnt01 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 0);

public ZonedDecimalData totamnt02 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 18);

public ZonedDecimalData totamnt03 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 36);

public ZonedDecimalData totamnt04 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 54);

public ZonedDecimalData totamnt05 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 72);

public ZonedDecimalData totamnt06 = new ZonedDecimalData(18,2).isAPartOf(totamnts, 90);

		
    public String[] fieldnamelist={"oppc01s","oppc02s","prcnt01s","prcnt02s","prcnt03s","prcnt04s","prcnt05s","prcnt06s","totamnt01s","totamnt02s","totamnt03s","totamnt04s","totamnt05s","totamnt06s"};
		
	public Tr58orec(){
		
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr58oRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr58oRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
    }
}
