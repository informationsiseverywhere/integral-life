/*
 * File: Pr58t.java
 * Date: 18 July 2013
 * 
 * Copyright (2012) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;


import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;


import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.life.agents.tablestructures.Tr58trec;
import com.csc.smart.recordstructures.Wsspsmart;

import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.life.agents.screens.Sr58tScreenVars;


import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.life.agents.procedures.Tr58tpt;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
/********************************************
 * includes for IO classes
*******************************************/


/**
* <pre>
*
*
*****************************************************************
* </pre>
*/
public class Pr58t extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR58T");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();

	private FixedLengthStringData errors = new FixedLengthStringData(8);
	private FixedLengthStringData F838 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("F838");
	private Tr58trec tr58trec = new Tr58trec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr58tScreenVars sv = ScreenProgram.getScreenVars( Sr58tScreenVars.class);

	

	public Pr58t() {
		super();
		screenVars = sv;
		new ScreenModel("Sr58t", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		readRecord1031();
		moveToScreen1040();
		generalArea1045();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr58trec.tr58tRec.set(itemIO.getGenarea());
	}

protected void generalArea1045()
{
           
	try{
		sv.months.set(tr58trec.months);
		sv.effperd.set(tr58trec.effperd);
		sv.chdrtypes.set(tr58trec.chdrtypes);
		//Ticket #TMLII-1918
		sv.crtables.set(tr58trec.crtables);
		
	}catch(Exception e){
	
	}
	
}

protected void preScreenEdit()
	{
 		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}

	}


protected void screenEdit2000()
{
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(wsspcomn.flag,"I"))
	{
      return;
    }
    if (isNE(sv.errorIndicators,SPACES)) {
		wsspcomn.edterror.set("Y");
	}

}



protected void update3000()
{
    if (isEQ(wsspcomn.flag,"I")) {
      return;
    }
	wsaaUpdateFlag = "N";
	if (isEQ(wsspcomn.flag,"C")||isEQ(wsspcomn.flag,"M")) {
		checkChanges3100();
	}
	if (isNE(wsaaUpdateFlag,"Y")) {
      return;
	}
	updatePrimaryRecord3050();
	updateRecord3055();

}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(tr58trec.tr58tRec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
	  if(isNE(sv.months,tr58trec.months)){
		  tr58trec.months.set(sv.months);
		  wsaaUpdateFlag = "Y";	
	  }
	  if(isNE(sv.effperd,tr58trec.effperd)){
		  tr58trec.effperd.set(sv.effperd);
		  wsaaUpdateFlag = "Y";	
	  }
	  if(isNE(sv.chdrtypes,tr58trec.chdrtypes)){
		  tr58trec.chdrtypes.set(sv.chdrtypes);
		  wsaaUpdateFlag = "Y";	
	  }
	  //Ticket #TMLII-1918
	  if(isNE(sv.crtables,tr58trec.crtables)){
		  tr58trec.crtables.set(sv.crtables);
		  wsaaUpdateFlag = "Y";	
	  }
	  //Ticket #TMLII-1918
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr58tpt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
