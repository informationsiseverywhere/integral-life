package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm509screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm509ScreenVars sv = (Sm509ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm509screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm509ScreenVars screenVars = (Sm509ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.acctmonth.setClassString("");
		screenVars.jobq.setClassString("");
		screenVars.acctyear.setClassString("");
		screenVars.scheduleName.setClassString("");
		screenVars.scheduleNumber.setClassString("");
		screenVars.bbranch.setClassString("");
		screenVars.bcompany.setClassString("");
		screenVars.datefrmDisp.setClassString("");
		screenVars.datetoDisp.setClassString("");
		screenVars.mlagttyp.setClassString("");
		screenVars.agtydesc.setClassString("");
	}

/**
 * Clear all the variables in Sm509screen
 */
	public static void clear(VarModel pv) {
		Sm509ScreenVars screenVars = (Sm509ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.acctmonth.clear();
		screenVars.jobq.clear();
		screenVars.acctyear.clear();
		screenVars.scheduleName.clear();
		screenVars.scheduleNumber.clear();
		screenVars.bbranch.clear();
		screenVars.bcompany.clear();
		screenVars.datefrmDisp.clear();
		screenVars.datefrm.clear();
		screenVars.datetoDisp.clear();
		screenVars.dateto.clear();
		screenVars.mlagttyp.clear();
		screenVars.agtydesc.clear();
	}
}
