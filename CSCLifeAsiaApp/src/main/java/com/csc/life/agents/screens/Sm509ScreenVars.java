package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM509
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm509ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(285);
	public FixedLengthStringData dataFields = new FixedLengthStringData(93).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,39);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,49);
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal().isAPartOf(dataFields,57);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(dataFields,65);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,73);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData mlagttyp = DD.mlagttyp.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(48).isAPartOf(dataArea, 93);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData datefrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData mlagttypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(144).isAPartOf(dataArea, 141);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] datefrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] mlagttypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData datefrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sm509screenWritten = new LongData(0);
	public LongData Sm509protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm509ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(datefrmOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datetoOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {effdate, acctmonth, jobq, acctyear, scheduleName, scheduleNumber, bbranch, bcompany, datefrm, dateto, mlagttyp, agtydesc};
		screenOutFields = new BaseData[][] {effdateOut, acctmonthOut, jobqOut, acctyearOut, bschednamOut, bschednumOut, bbranchOut, bcompanyOut, datefrmOut, datetoOut, mlagttypOut, agtydescOut};
		screenErrFields = new BaseData[] {effdateErr, acctmonthErr, jobqErr, acctyearErr, bschednamErr, bschednumErr, bbranchErr, bcompanyErr, datefrmErr, datetoErr, mlagttypErr, agtydescErr};
		screenDateFields = new BaseData[] {effdate, datefrm, dateto};
		screenDateErrFields = new BaseData[] {effdateErr, datefrmErr, datetoErr};
		screenDateDispFields = new BaseData[] {effdateDisp, datefrmDisp, datetoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm509screen.class;
		protectRecord = Sm509protect.class;
	}

}
