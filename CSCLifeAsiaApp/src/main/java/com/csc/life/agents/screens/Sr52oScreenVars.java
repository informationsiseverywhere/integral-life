package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52O
 * @version 1.0 generated on 11/22/13 6:36 AM
 * @author CSC
 */
public class Sr52oScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(483);
	public FixedLengthStringData dataFields = new FixedLengthStringData(211).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,22);
	public ZonedDecimalData deemdate = DD.deemdate.copyToZonedDecimal().isAPartOf(dataFields,52);
	public FixedLengthStringData descn = DD.descn.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData despdate = DD.despdate.copyToZonedDecimal().isAPartOf(dataFields,95);
	public FixedLengthStringData dlvrmode = DD.dlvrmode.copy().isAPartOf(dataFields,103);
	public ZonedDecimalData nextActDate = DD.nxtdte.copyToZonedDecimal().isAPartOf(dataFields,107);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,115);
	public ZonedDecimalData packdate = DD.packdate.copyToZonedDecimal().isAPartOf(dataFields,162);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,180);
	public ZonedDecimalData remdte = DD.remdte.copyToZonedDecimal().isAPartOf(dataFields,183);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData shortds = DD.shortds.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 211);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData deemdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData descnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData despdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData dlvrmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData nxtdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData packdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData remdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData shortdsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 279);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] deemdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] descnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] despdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] dlvrmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] nxtdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] packdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] remdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] shortdsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData deemdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData despdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData nextActDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData packdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData remdteDisp = new FixedLengthStringData(10);

	public LongData Sr52oscreenWritten = new LongData(0);
	public LongData Sr52oprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52oScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(packdateOut,new String[] {"21","02","-21","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(remdteOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(deemdateOut,new String[] {null, null, null, "05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(nxtdteOut,new String[] {null, null, null, "07",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {descn, chdrnum, cnttype, ctypedes, cntcurr, rstate, pstate, register, cownnum, ownername, dlvrmode, shortds, despdate, packdate, remdte, deemdate, nextActDate};
		screenOutFields = new BaseData[][] {descnOut, chdrnumOut, cnttypeOut, ctypedesOut, cntcurrOut, rstateOut, pstateOut, registerOut, cownnumOut, ownernameOut, dlvrmodeOut, shortdsOut, despdateOut, packdateOut, remdteOut, deemdateOut, nxtdteOut};
		screenErrFields = new BaseData[] {descnErr, chdrnumErr, cnttypeErr, ctypedesErr, cntcurrErr, rstateErr, pstateErr, registerErr, cownnumErr, ownernameErr, dlvrmodeErr, shortdsErr, despdateErr, packdateErr, remdteErr, deemdateErr, nxtdteErr};
		screenDateFields = new BaseData[] {despdate, packdate, remdte, deemdate, nextActDate};
		screenDateErrFields = new BaseData[] {despdateErr, packdateErr, remdteErr, deemdateErr, nxtdteErr};
		screenDateDispFields = new BaseData[] {despdateDisp, packdateDisp, remdteDisp, deemdateDisp, nextActDateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr52oscreen.class;
		protectRecord = Sr52oprotect.class;
	}

}
