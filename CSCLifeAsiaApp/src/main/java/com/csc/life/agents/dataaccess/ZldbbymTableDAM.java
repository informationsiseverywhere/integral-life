package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class ZldbbymTableDAM extends ZldbpfTableDAM {
	
	public ZldbbymTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZLDBBYM");
	}
	
	public String getTable() {
		return TABLE;
	}
	
	public void setKeyConstants() {
		
		KEYCOLUMNS = 
						" BATCACTYR, "
						+ " BATCACTMN, "
						+ " AGNTCOY, "
						+ " AGNTNUM ";
		
		QUALIFIEDCOLUMNS = 
						"AGNTCOY, " +
						"AGNTNUM, " +
						"BATCACTYR, " +
						"BATCACTMN, " +
						"AGTYPE, " +
						"PRCNT, " +
						"GENLCUR, " +
						"BLPREM, " +
						"PREMST, " +
						"PRMAMT, " +
						"BPAYTY, " +
						"BONUSAMT, " +
						"BNPAID,"+
						"NETPRE, " +
						"PROCFLG, " +
						"PRCDATE, " +
						"USRPRF, " +
						"JOBNM, " +
						"DATIME, " +
						"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BATCACTYR ASC, " +
		            "BATCACTMN ASC, " +
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "BATCACTYR DESC, " +
		            "BATCACTMN DESC, " +
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
				agntcoy,
				agntnum,
				batcactyr,
				batcactmn,
				agtype,
				prcnt,
				genlcur,
				blprem,
				premst,
				prmamt,
				bpayty,
				bonusamt,
				bnpaid,
				netpre,
				procflg,
				prcdate,
				usrprf,
				jobnm,
				datime,
                unique_number
                };

	}
	
	public FixedLengthStringData getBaseString() {
	FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
	result.set(getParams());
	return result;
	}
	
	public String toString() {
	return getBaseString().toString();
	}
	
	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/
	
	public FixedLengthStringData getHeader() {
	return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
	
	return setShortHeader(what);
	}
	
	private FixedLengthStringData keyFiller = new FixedLengthStringData(47);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
	FixedLengthStringData keyData = new FixedLengthStringData(64);
	
	keyData.set(getBatcactyr().toInternal()
			+ getBatcactmn().toInternal()
			+ getAgntcoy().toInternal()
			+ getAgntnum().toInternal()
			+ keyFiller.toInternal());
	return keyData;
	}
		
	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}
	// FILLER fields under -NON-KEY-DATA

	private PackedDecimalData nonKeyFiller10 = new PackedDecimalData(4,0);
	private PackedDecimalData nonKeyFiller20 = new PackedDecimalData(2,0);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(batcactyr.toInternal());
	nonKeyFiller20.setInternal(batcactmn.toInternal());
	nonKeyFiller30.setInternal(agntcoy.toInternal());
	nonKeyFiller40.setInternal(agntnum.toInternal());
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(1008);
		nonKeyData.set(
				nonKeyFiller10.toInternal()
				+ nonKeyFiller20.toInternal()
				+ nonKeyFiller30.toInternal()
				+ nonKeyFiller40.toInternal()
				+ agtype.toInternal()
				+ prcnt.toInternal()
				+ genlcur.toInternal()
				+ blprem.toInternal()
				+ premst.toInternal()
				+ prmamt.toInternal()
				+ bpayty.toInternal()
				+ bonusamt.toInternal()
				+ bnpaid.toInternal()
				+ netpre.toInternal()
				+ procflg.toInternal()
				+ prcdate.toInternal()
				+ usrprf.toInternal()
				+ jobnm.toInternal()
				+ datime.toInternal());
		return nonKeyData;
	}
	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());
			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, agtype);
			what = ExternalData.chop(what, prcnt);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, blprem);
			what = ExternalData.chop(what, premst);
			what = ExternalData.chop(what, prmamt);
			what = ExternalData.chop(what, bpayty);
			what = ExternalData.chop(what, bonusamt);
			what = ExternalData.chop(what, bnpaid);
			what = ExternalData.chop(what, netpre);
			what = ExternalData.chop(what, procflg);
			what = ExternalData.chop(what, prcdate);
			what = ExternalData.chop(what, usrprf);
			what = ExternalData.chop(what, jobnm);
			what = ExternalData.chop(what, datime);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/
	
	
	public void clearRecKeyData() {
		agntcoy.clear();
		agntnum.clear();
		batcactyr.clear();
		batcactmn.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {
		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		agntcoy.clear();
		agntnum.clear();
		batcactyr.clear();
		batcactmn.clear();
		agtype.clear();
		prcnt.clear();
		genlcur.clear();
		blprem.clear();
		premst.clear();
		prmamt.clear();
		bpayty.clear();
		bonusamt.clear();
		bnpaid.clear();
		netpre.clear();
		procflg.clear();
		prcdate.clear();
		usrprf.clear();
		jobnm.clear();
		datime.clear();
		
	}
	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}

	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}

	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}

	public void setAgntnum(Object what) {
		agntnum.set(what);
	}

	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}

	public void setBatcactyr(Object what) {
		batcactyr.set(what);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}

	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}

	public void setBatcactmn(Object what) {
		batcactmn.set(what);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}
	public FixedLengthStringData getAgtype() {
		return agtype;
	}

	public void setAgtype(Object what) {
		agtype.set(what);
	}

	public PackedDecimalData getPrcnt() {
		return prcnt;
	}

	public void setPrcnt(Object what) {
		prcnt.set(what);
	}

	public void setPrcnt(Object what, boolean rounded) {
		if (rounded)
			prcnt.setRounded(what);
		else
			prcnt.set(what);
	}
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}

	public void setGenlcur(Object what) {
		genlcur.set(what);
	}

	public PackedDecimalData getBlprem() {
		return blprem;
	}

	public void setBlprem(Object what) {
		blprem.set(what);
	}
	public void setBlprem(Object what, boolean rounded) {
		if (rounded)
			blprem.setRounded(what);
		else
			blprem.set(what);
	}
	public PackedDecimalData getPremst() {
		return premst;
	}

	public void setPremst(Object what) {
		premst.set(what);
	}
	public void setPremst(Object what, boolean rounded) {
		if (rounded)
			premst.setRounded(what);
		else
			premst.set(what);
	}

	public PackedDecimalData getBpayty() {
		return bpayty;
	}

	public void setBpayty(Object what) {
		bpayty.set(what);
	}
	public void setBpayty(Object what, boolean rounded) {
		if (rounded)
			bpayty.setRounded(what);
		else
			bpayty.set(what);
	}
	public PackedDecimalData getPrmamt() {
		return prmamt;
	}

	public void setPrmamt(Object what) {
		prmamt.set(what);
	}
	public void setPrmamt(Object what, boolean rounded) {
		if (rounded)
			prmamt.setRounded(what);
		else
			prmamt.set(what);
	}
	public PackedDecimalData getBonusamt() {
		return bonusamt;
	}

	public void setBonusamt(Object what) {
		bonusamt.set(what);
	}
	public void setBonusamt(Object what, boolean rounded) {
		if (rounded)
			bonusamt.setRounded(what);
		else
			bonusamt.set(what);
	}
	
	public PackedDecimalData getBnpaid() {
		return bnpaid;
	}

	public void setBnpaid(Object what) {
		bnpaid.set(what);
	}
	public void setBnpaid(Object what, boolean rounded) {
		if (rounded)
			bnpaid.setRounded(what);
		else
			bnpaid.set(what);
	}
	public PackedDecimalData getNetpre() {
		return netpre;
	}

	public void setNetpre(Object what) {
		netpre.set(what);
	}
	public void setNetpre(Object what, boolean rounded) {
		if (rounded)
			netpre.setRounded(what);
		else
			netpre.set(what);
	}
	public FixedLengthStringData getProcflg() {
		return procflg;
	}

	public void setProcflg(Object what) {
		procflg.set(what);
	}

	public PackedDecimalData getPrcdate() {
		return prcdate;
	}

	public void setPrcdate(Object what) {
		prcdate.set(what);
	}
	public void setPrcdate(Object what, boolean rounded) {
		if (rounded)
			prcdate.setRounded(what);
		else
			prcdate.set(what);
	}
	public FixedLengthStringData getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(Object what) {
		usrprf.set(what);
	}

	public FixedLengthStringData getJobnm() {
		return jobnm;
	}

	public void setJobnm(Object what) {
		jobnm.set(what);
	}

	public FixedLengthStringData getDatime() {
		return datime;
	}

	public void setDatime(Object what) {
		datime.set(what);
	}

}
