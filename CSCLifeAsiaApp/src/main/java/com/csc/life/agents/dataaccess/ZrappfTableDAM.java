package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZrappfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:01
 * Class transformed from ZRAPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrappfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 75;
	public FixedLengthStringData zraprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zrappfRecord = zraprec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zraprec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zraprec);
	public FixedLengthStringData reportag = DD.reportag.copy().isAPartOf(zraprec);
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(zraprec);
	public FixedLengthStringData zrorcode = DD.zrorcode.copy().isAPartOf(zraprec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zraprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zraprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zraprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zraprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZrappfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZrappfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZrappfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZrappfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrappfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZrappfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrappfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRAPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"REPORTAG, " +
							"PRCNT, " +
							"ZRORCODE, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     reportag,
                                     prcnt,
                                     zrorcode,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		reportag.clear();
  		prcnt.clear();
  		zrorcode.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZraprec() {
  		return zraprec;
	}

	public FixedLengthStringData getZrappfRecord() {
  		return zrappfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZraprec(what);
	}

	public void setZraprec(Object what) {
  		this.zraprec.set(what);
	}

	public void setZrappfRecord(Object what) {
  		this.zrappfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zraprec.getLength());
		result.set(zraprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}