package com.csc.life.agents.dataaccess.dao;
import java.util.List;

import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AgslpfDAO extends BaseDAO<Agslpf>{
	
	public void insertAgslpfRecord(List<Agslpf> agslist);
	public void deleteAgslpfRecord(List<String> delist);
	public List<Agslpf> getAgslpfByAgentNum(String agntNumber);
	public List<Agslpf> getAgslpfByAgentAndLic(String agntNumber, String licType);
	
}
