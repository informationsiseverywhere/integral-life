package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:14
 * Description:
 * Copybook name: CHDRCMCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrcmckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrcmcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData chdrcmcKey = new FixedLengthStringData(256).isAPartOf(chdrcmcFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrcmcChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrcmcKey, 0);
  	public FixedLengthStringData chdrcmcChdrnum = new FixedLengthStringData(8).isAPartOf(chdrcmcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(chdrcmcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrcmcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrcmcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}