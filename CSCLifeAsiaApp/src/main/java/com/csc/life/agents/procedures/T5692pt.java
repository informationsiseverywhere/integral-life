/*
 * File: T5692pt.java
 * Date: 30 August 2009 2:26:26
 * Author: Quipoz Limited
 * 
 * Class transformed from T5692PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.T5692rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5692.
*
*
*****************************************************************
* </pre>
*/
public class T5692pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Commission Payment - Enhancement Rules           S5692");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler9 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(57).isAPartOf(wsaaPrtLine004, 19, FILLER).init("% of     % of                               % of     % of");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(76);
	private FixedLengthStringData filler11 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" To age:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 10).setPattern("ZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 13, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine005, 19, FILLER).init("Comm     Prem            To age:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine005, 53).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 56, FILLER).init(SPACES);
	private FixedLengthStringData filler15 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 63, FILLER).init("Comm     Prem");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(78);
	private FixedLengthStringData filler16 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine006, 0, FILLER).init("   Initial");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 28).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine006, 46, FILLER).init("Initial");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 63).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine006, 72).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(78);
	private FixedLengthStringData filler21 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine007, 0, FILLER).init("   Renewal");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 28).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler24 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 46, FILLER).init("Renewal");
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 63).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine007, 72).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(56);
	private FixedLengthStringData filler26 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" To age:");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 10).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine008, 13, FILLER).init(SPACES);
	private FixedLengthStringData filler28 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 44, FILLER).init("To age:");
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler29 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine009, 0, FILLER).init("   Initial");
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 28).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler32 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine009, 46, FILLER).init("Initial");
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 63).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 72).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(78);
	private FixedLengthStringData filler34 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine010, 0, FILLER).init("   Renewal");
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 28).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 46, FILLER).init("Renewal");
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 63).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 72).setPattern("ZZZ.ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5692rec t5692rec = new T5692rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5692pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5692rec.t5692Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5692rec.age01);
		fieldNo017.set(t5692rec.age03);
		fieldNo008.set(t5692rec.age02);
		fieldNo018.set(t5692rec.age04);
		fieldNo009.set(t5692rec.incmrate01);
		fieldNo010.set(t5692rec.inprempc01);
		fieldNo011.set(t5692rec.incmrate02);
		fieldNo012.set(t5692rec.inprempc02);
		fieldNo021.set(t5692rec.incmrate04);
		fieldNo022.set(t5692rec.inprempc04);
		fieldNo015.set(t5692rec.rwcmrate02);
		fieldNo016.set(t5692rec.reprempc02);
		fieldNo025.set(t5692rec.rwcmrate04);
		fieldNo026.set(t5692rec.reprempc04);
		fieldNo013.set(t5692rec.rwcmrate01);
		fieldNo014.set(t5692rec.reprempc01);
		fieldNo019.set(t5692rec.incmrate03);
		fieldNo020.set(t5692rec.inprempc03);
		fieldNo023.set(t5692rec.rwcmrate03);
		fieldNo024.set(t5692rec.reprempc03);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
