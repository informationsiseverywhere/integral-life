package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl29rec Date: 03 February 2020 Author: vdivisala
 */
public class Tjl29rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl29Rec = new FixedLengthStringData(1);
	public FixedLengthStringData cmgwgndr = DD.cmgwgndr.copy().isAPartOf(tjl29Rec, 0);

	public void initialize() {
		COBOLFunctions.initialize(tjl29Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl29Rec.isAPartOf(baseString, Boolean.TRUE);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}