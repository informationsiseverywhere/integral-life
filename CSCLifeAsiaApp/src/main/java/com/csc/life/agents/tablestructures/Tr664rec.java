package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:20
 * Description:
 * Copybook name: TR664REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr664rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr664Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zcmsntype = new FixedLengthStringData(10).isAPartOf(tr664Rec, 0);
  	public FixedLengthStringData zcmsntyped = new FixedLengthStringData(40).isAPartOf(tr664Rec, 10);
  	public FixedLengthStringData zpremtype = new FixedLengthStringData(20).isAPartOf(tr664Rec, 50);
  	public FixedLengthStringData zpremtyped = new FixedLengthStringData(40).isAPartOf(tr664Rec, 70);
  	public FixedLengthStringData filler = new FixedLengthStringData(390).isAPartOf(tr664Rec, 110, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr664Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr664Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}