package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:51
 * Description:
 * Copybook name: TM603REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tm603rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tm603Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData actives = new FixedLengthStringData(3).isAPartOf(tm603Rec, 0);
  	public FixedLengthStringData[] active = FLSArrayPartOfStructure(3, 1, actives, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(actives, 0, FILLER_REDEFINE);
  	public FixedLengthStringData active01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData active02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData active03 = new FixedLengthStringData(1).isAPartOf(filler, 2);
  	public FixedLengthStringData agtypes = new FixedLengthStringData(20).isAPartOf(tm603Rec, 3);
  	public FixedLengthStringData[] agtype = FLSArrayPartOfStructure(10, 2, agtypes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(agtypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData agtype01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData agtype02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData agtype03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData agtype04 = new FixedLengthStringData(2).isAPartOf(filler1, 6);
  	public FixedLengthStringData agtype05 = new FixedLengthStringData(2).isAPartOf(filler1, 8);
  	public FixedLengthStringData agtype06 = new FixedLengthStringData(2).isAPartOf(filler1, 10);
  	public FixedLengthStringData agtype07 = new FixedLengthStringData(2).isAPartOf(filler1, 12);
  	public FixedLengthStringData agtype08 = new FixedLengthStringData(2).isAPartOf(filler1, 14);
  	public FixedLengthStringData agtype09 = new FixedLengthStringData(2).isAPartOf(filler1, 16);
  	public FixedLengthStringData agtype10 = new FixedLengthStringData(2).isAPartOf(filler1, 18);
  	public FixedLengthStringData mlagttyps = new FixedLengthStringData(20).isAPartOf(tm603Rec, 23);
  	public FixedLengthStringData[] mlagttyp = FLSArrayPartOfStructure(10, 2, mlagttyps, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(mlagttyps, 0, FILLER_REDEFINE);
  	public FixedLengthStringData mlagttyp01 = new FixedLengthStringData(2).isAPartOf(filler2, 0);
  	public FixedLengthStringData mlagttyp02 = new FixedLengthStringData(2).isAPartOf(filler2, 2);
  	public FixedLengthStringData mlagttyp03 = new FixedLengthStringData(2).isAPartOf(filler2, 4);
  	public FixedLengthStringData mlagttyp04 = new FixedLengthStringData(2).isAPartOf(filler2, 6);
  	public FixedLengthStringData mlagttyp05 = new FixedLengthStringData(2).isAPartOf(filler2, 8);
  	public FixedLengthStringData mlagttyp06 = new FixedLengthStringData(2).isAPartOf(filler2, 10);
  	public FixedLengthStringData mlagttyp07 = new FixedLengthStringData(2).isAPartOf(filler2, 12);
  	public FixedLengthStringData mlagttyp08 = new FixedLengthStringData(2).isAPartOf(filler2, 14);
  	public FixedLengthStringData mlagttyp09 = new FixedLengthStringData(2).isAPartOf(filler2, 16);
  	public FixedLengthStringData mlagttyp10 = new FixedLengthStringData(2).isAPartOf(filler2, 18);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(457).isAPartOf(tm603Rec, 43, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tm603Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tm603Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}