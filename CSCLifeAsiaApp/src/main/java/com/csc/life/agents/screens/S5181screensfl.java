package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class S5181screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static int maxRecords = 10;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] { 1,  5, 29,  30,  32}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {12, 21, 4, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5181ScreenVars sv = (S5181ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5181screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5181screensfl, 
			sv.S5181screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5181ScreenVars sv = (S5181ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5181screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5181ScreenVars sv = (S5181ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5181screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5181screensflWritten.gt(0))
		{
			sv.s5181screensfl.setCurrentIndex(0);
			sv.S5181screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5181ScreenVars sv = (S5181ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5181screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5181ScreenVars screenVars = (S5181ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.zrectyp.setFieldName("zrectyp");
				screenVars.fundAmount.setFieldName("fundAmount");
				screenVars.zcurprmbal.setFieldName("zcurprmbal");
				screenVars.hseqno.setFieldName("hseqno");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.updteflag.set(dm.getField("updteflag"));
			screenVars.unitVirtualFund.set(dm.getField("unitVirtualFund"));
			screenVars.zrectyp.set(dm.getField("zrectyp"));
			screenVars.fundAmount.set(dm.getField("fundAmount"));
			screenVars.zcurprmbal.set(dm.getField("zcurprmbal"));
			screenVars.hseqno.set(dm.getField("hseqno"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5181ScreenVars screenVars = (S5181ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.unitVirtualFund.setFieldName("unitVirtualFund");
				screenVars.zrectyp.setFieldName("zrectyp");
				screenVars.fundAmount.setFieldName("fundAmount");
				screenVars.zcurprmbal.setFieldName("zcurprmbal");
				screenVars.hseqno.setFieldName("hseqno");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("updteflag").set(screenVars.updteflag);
			dm.getField("unitVirtualFund").set(screenVars.unitVirtualFund);
			dm.getField("zrectyp").set(screenVars.zrectyp);
			dm.getField("fundAmount").set(screenVars.fundAmount);
			dm.getField("zcurprmbal").set(screenVars.zcurprmbal);
			dm.getField("hseqno").set(screenVars.hseqno);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5181screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5181ScreenVars screenVars = (S5181ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.updteflag.clearFormatting();
		screenVars.unitVirtualFund.clearFormatting();
		screenVars.zrectyp.clearFormatting();
		screenVars.fundAmount.clearFormatting();
		screenVars.zcurprmbal.clearFormatting();
		screenVars.hseqno.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5181ScreenVars screenVars = (S5181ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.updteflag.setClassString("");
		screenVars.unitVirtualFund.setClassString("");
		screenVars.zrectyp.setClassString("");
		screenVars.fundAmount.setClassString("");
		screenVars.zcurprmbal.setClassString("");
		screenVars.hseqno.setClassString("");
	}

/**
 * Clear all the variables in S5181screensfl
 */
	public static void clear(VarModel pv) {
		S5181ScreenVars screenVars = (S5181ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.updteflag.clear();
		screenVars.unitVirtualFund.clear();
		screenVars.zrectyp.clear();
		screenVars.fundAmount.clear();
		screenVars.zcurprmbal.clear();
		screenVars.hseqno.clear();
	}
}
