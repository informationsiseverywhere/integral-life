package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM605
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm605ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1167);
	public FixedLengthStringData dataFields = new FixedLengthStringData(367).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData mlagtprds = new FixedLengthStringData(85).isAPartOf(dataFields, 55);
	public ZonedDecimalData[] mlagtprd = ZDArrayPartOfStructure(5, 17, 2, mlagtprds, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(85).isAPartOf(mlagtprds, 0, FILLER_REDEFINE);
	public ZonedDecimalData mlagtprd01 = DD.mlagtprd.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData mlagtprd02 = DD.mlagtprd.copyToZonedDecimal().isAPartOf(filler,17);
	public ZonedDecimalData mlagtprd03 = DD.mlagtprd.copyToZonedDecimal().isAPartOf(filler,34);
	public ZonedDecimalData mlagtprd04 = DD.mlagtprd.copyToZonedDecimal().isAPartOf(filler,51);
	public ZonedDecimalData mlagtprd05 = DD.mlagtprd.copyToZonedDecimal().isAPartOf(filler,68);
	public FixedLengthStringData mlagttyps = new FixedLengthStringData(36).isAPartOf(dataFields, 140);
	public FixedLengthStringData[] mlagttyp = FLSArrayPartOfStructure(18, 2, mlagttyps, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(mlagttyps, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlagttyp01 = DD.mlagttyp.copy().isAPartOf(filler1,0);
	public FixedLengthStringData mlagttyp02 = DD.mlagttyp.copy().isAPartOf(filler1,2);
	public FixedLengthStringData mlagttyp03 = DD.mlagttyp.copy().isAPartOf(filler1,4);
	public FixedLengthStringData mlagttyp04 = DD.mlagttyp.copy().isAPartOf(filler1,6);
	public FixedLengthStringData mlagttyp05 = DD.mlagttyp.copy().isAPartOf(filler1,8);
	public FixedLengthStringData mlagttyp06 = DD.mlagttyp.copy().isAPartOf(filler1,10);
	public FixedLengthStringData mlagttyp07 = DD.mlagttyp.copy().isAPartOf(filler1,12);
	public FixedLengthStringData mlagttyp08 = DD.mlagttyp.copy().isAPartOf(filler1,14);
	public FixedLengthStringData mlagttyp09 = DD.mlagttyp.copy().isAPartOf(filler1,16);
	public FixedLengthStringData mlagttyp10 = DD.mlagttyp.copy().isAPartOf(filler1,18);
	public FixedLengthStringData mlagttyp11 = DD.mlagttyp.copy().isAPartOf(filler1,20);
	public FixedLengthStringData mlagttyp12 = DD.mlagttyp.copy().isAPartOf(filler1,22);
	public FixedLengthStringData mlagttyp13 = DD.mlagttyp.copy().isAPartOf(filler1,24);
	public FixedLengthStringData mlagttyp14 = DD.mlagttyp.copy().isAPartOf(filler1,26);
	public FixedLengthStringData mlagttyp15 = DD.mlagttyp.copy().isAPartOf(filler1,28);
	public FixedLengthStringData mlagttyp16 = DD.mlagttyp.copy().isAPartOf(filler1,30);
	public FixedLengthStringData mlagttyp17 = DD.mlagttyp.copy().isAPartOf(filler1,32);
	public FixedLengthStringData mlagttyp18 = DD.mlagttyp.copy().isAPartOf(filler1,34);
	public FixedLengthStringData mlgrppps = new FixedLengthStringData(85).isAPartOf(dataFields, 176);
	public ZonedDecimalData[] mlgrppp = ZDArrayPartOfStructure(5, 17, 2, mlgrppps, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(85).isAPartOf(mlgrppps, 0, FILLER_REDEFINE);
	public ZonedDecimalData mlgrppp01 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData mlgrppp02 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData mlgrppp03 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData mlgrppp04 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData mlgrppp05 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,68);
	public FixedLengthStringData mlperpps = new FixedLengthStringData(85).isAPartOf(dataFields, 261);
	public ZonedDecimalData[] mlperpp = ZDArrayPartOfStructure(5, 17, 2, mlperpps, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(85).isAPartOf(mlperpps, 0, FILLER_REDEFINE);
	public ZonedDecimalData mlperpp01 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData mlperpp02 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,17);
	public ZonedDecimalData mlperpp03 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,34);
	public ZonedDecimalData mlperpp04 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,51);
	public ZonedDecimalData mlperpp05 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,68);
	public FixedLengthStringData mlprcinds = new FixedLengthStringData(6).isAPartOf(dataFields, 346);
	public FixedLengthStringData[] mlprcind = FLSArrayPartOfStructure(6, 1, mlprcinds, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(6).isAPartOf(mlprcinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlprcind01 = DD.mlprcind.copy().isAPartOf(filler4,0);
	public FixedLengthStringData mlprcind02 = DD.mlprcind.copy().isAPartOf(filler4,1);
	public FixedLengthStringData mlprcind03 = DD.mlprcind.copy().isAPartOf(filler4,2);
	public FixedLengthStringData mlprcind04 = DD.mlprcind.copy().isAPartOf(filler4,3);
	public FixedLengthStringData mlprcind05 = DD.mlprcind.copy().isAPartOf(filler4,4);
	public FixedLengthStringData mlprcind06 = DD.mlprcind.copy().isAPartOf(filler4,5);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,352);
	public FixedLengthStringData toYears = new FixedLengthStringData(10).isAPartOf(dataFields, 357);
	public ZonedDecimalData[] toYear = ZDArrayPartOfStructure(5, 2, 0, toYears, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(toYears, 0, FILLER_REDEFINE);
	public ZonedDecimalData toYear01 = DD.toyear.copyToZonedDecimal().isAPartOf(filler5,0);
	public ZonedDecimalData toYear02 = DD.toyear.copyToZonedDecimal().isAPartOf(filler5,2);
	public ZonedDecimalData toYear03 = DD.toyear.copyToZonedDecimal().isAPartOf(filler5,4);
	public ZonedDecimalData toYear04 = DD.toyear.copyToZonedDecimal().isAPartOf(filler5,6);
	public ZonedDecimalData toYear05 = DD.toyear.copyToZonedDecimal().isAPartOf(filler5,8);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(200).isAPartOf(dataArea, 367);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData mlagtprdsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] mlagtprdErr = FLSArrayPartOfStructure(5, 4, mlagtprdsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(20).isAPartOf(mlagtprdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlagtprd01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData mlagtprd02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData mlagtprd03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData mlagtprd04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData mlagtprd05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData mlagttypsErr = new FixedLengthStringData(72).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData[] mlagttypErr = FLSArrayPartOfStructure(18, 4, mlagttypsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(72).isAPartOf(mlagttypsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlagttyp01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData mlagttyp02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData mlagttyp03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData mlagttyp04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData mlagttyp05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData mlagttyp06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData mlagttyp07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData mlagttyp08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData mlagttyp09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData mlagttyp10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData mlagttyp11Err = new FixedLengthStringData(4).isAPartOf(filler7, 40);
	public FixedLengthStringData mlagttyp12Err = new FixedLengthStringData(4).isAPartOf(filler7, 44);
	public FixedLengthStringData mlagttyp13Err = new FixedLengthStringData(4).isAPartOf(filler7, 48);
	public FixedLengthStringData mlagttyp14Err = new FixedLengthStringData(4).isAPartOf(filler7, 52);
	public FixedLengthStringData mlagttyp15Err = new FixedLengthStringData(4).isAPartOf(filler7, 56);
	public FixedLengthStringData mlagttyp16Err = new FixedLengthStringData(4).isAPartOf(filler7, 60);
	public FixedLengthStringData mlagttyp17Err = new FixedLengthStringData(4).isAPartOf(filler7, 64);
	public FixedLengthStringData mlagttyp18Err = new FixedLengthStringData(4).isAPartOf(filler7, 68);
	public FixedLengthStringData mlgrpppsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData[] mlgrpppErr = FLSArrayPartOfStructure(5, 4, mlgrpppsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(20).isAPartOf(mlgrpppsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlgrppp01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData mlgrppp02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData mlgrppp03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData mlgrppp04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData mlgrppp05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData mlperppsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] mlperppErr = FLSArrayPartOfStructure(5, 4, mlperppsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(20).isAPartOf(mlperppsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlperpp01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData mlperpp02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData mlperpp03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData mlperpp04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData mlperpp05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData mlprcindsErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData[] mlprcindErr = FLSArrayPartOfStructure(6, 4, mlprcindsErr, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(mlprcindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlprcind01Err = new FixedLengthStringData(4).isAPartOf(filler10, 0);
	public FixedLengthStringData mlprcind02Err = new FixedLengthStringData(4).isAPartOf(filler10, 4);
	public FixedLengthStringData mlprcind03Err = new FixedLengthStringData(4).isAPartOf(filler10, 8);
	public FixedLengthStringData mlprcind04Err = new FixedLengthStringData(4).isAPartOf(filler10, 12);
	public FixedLengthStringData mlprcind05Err = new FixedLengthStringData(4).isAPartOf(filler10, 16);
	public FixedLengthStringData mlprcind06Err = new FixedLengthStringData(4).isAPartOf(filler10, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData toyearsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData[] toyearErr = FLSArrayPartOfStructure(5, 4, toyearsErr, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(20).isAPartOf(toyearsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData toyear01Err = new FixedLengthStringData(4).isAPartOf(filler11, 0);
	public FixedLengthStringData toyear02Err = new FixedLengthStringData(4).isAPartOf(filler11, 4);
	public FixedLengthStringData toyear03Err = new FixedLengthStringData(4).isAPartOf(filler11, 8);
	public FixedLengthStringData toyear04Err = new FixedLengthStringData(4).isAPartOf(filler11, 12);
	public FixedLengthStringData toyear05Err = new FixedLengthStringData(4).isAPartOf(filler11, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(600).isAPartOf(dataArea, 567);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData mlagtprdsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] mlagtprdOut = FLSArrayPartOfStructure(5, 12, mlagtprdsOut, 0);
	public FixedLengthStringData[][] mlagtprdO = FLSDArrayPartOfArrayStructure(12, 1, mlagtprdOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(60).isAPartOf(mlagtprdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlagtprd01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] mlagtprd02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] mlagtprd03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] mlagtprd04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] mlagtprd05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData mlagttypsOut = new FixedLengthStringData(216).isAPartOf(outputIndicators, 120);
	public FixedLengthStringData[] mlagttypOut = FLSArrayPartOfStructure(18, 12, mlagttypsOut, 0);
	public FixedLengthStringData[][] mlagttypO = FLSDArrayPartOfArrayStructure(12, 1, mlagttypOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(216).isAPartOf(mlagttypsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlagttyp01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] mlagttyp02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] mlagttyp03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] mlagttyp04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] mlagttyp05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] mlagttyp06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] mlagttyp07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] mlagttyp08Out = FLSArrayPartOfStructure(12, 1, filler13, 84);
	public FixedLengthStringData[] mlagttyp09Out = FLSArrayPartOfStructure(12, 1, filler13, 96);
	public FixedLengthStringData[] mlagttyp10Out = FLSArrayPartOfStructure(12, 1, filler13, 108);
	public FixedLengthStringData[] mlagttyp11Out = FLSArrayPartOfStructure(12, 1, filler13, 120);
	public FixedLengthStringData[] mlagttyp12Out = FLSArrayPartOfStructure(12, 1, filler13, 132);
	public FixedLengthStringData[] mlagttyp13Out = FLSArrayPartOfStructure(12, 1, filler13, 144);
	public FixedLengthStringData[] mlagttyp14Out = FLSArrayPartOfStructure(12, 1, filler13, 156);
	public FixedLengthStringData[] mlagttyp15Out = FLSArrayPartOfStructure(12, 1, filler13, 168);
	public FixedLengthStringData[] mlagttyp16Out = FLSArrayPartOfStructure(12, 1, filler13, 180);
	public FixedLengthStringData[] mlagttyp17Out = FLSArrayPartOfStructure(12, 1, filler13, 192);
	public FixedLengthStringData[] mlagttyp18Out = FLSArrayPartOfStructure(12, 1, filler13, 204);
	public FixedLengthStringData mlgrpppsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 336);
	public FixedLengthStringData[] mlgrpppOut = FLSArrayPartOfStructure(5, 12, mlgrpppsOut, 0);
	public FixedLengthStringData[][] mlgrpppO = FLSDArrayPartOfArrayStructure(12, 1, mlgrpppOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(60).isAPartOf(mlgrpppsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlgrppp01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] mlgrppp02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] mlgrppp03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] mlgrppp04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] mlgrppp05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData mlperppsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] mlperppOut = FLSArrayPartOfStructure(5, 12, mlperppsOut, 0);
	public FixedLengthStringData[][] mlperppO = FLSDArrayPartOfArrayStructure(12, 1, mlperppOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(60).isAPartOf(mlperppsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlperpp01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] mlperpp02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData[] mlperpp03Out = FLSArrayPartOfStructure(12, 1, filler15, 24);
	public FixedLengthStringData[] mlperpp04Out = FLSArrayPartOfStructure(12, 1, filler15, 36);
	public FixedLengthStringData[] mlperpp05Out = FLSArrayPartOfStructure(12, 1, filler15, 48);
	public FixedLengthStringData mlprcindsOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 456);
	public FixedLengthStringData[] mlprcindOut = FLSArrayPartOfStructure(6, 12, mlprcindsOut, 0);
	public FixedLengthStringData[][] mlprcindO = FLSDArrayPartOfArrayStructure(12, 1, mlprcindOut, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(72).isAPartOf(mlprcindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlprcind01Out = FLSArrayPartOfStructure(12, 1, filler16, 0);
	public FixedLengthStringData[] mlprcind02Out = FLSArrayPartOfStructure(12, 1, filler16, 12);
	public FixedLengthStringData[] mlprcind03Out = FLSArrayPartOfStructure(12, 1, filler16, 24);
	public FixedLengthStringData[] mlprcind04Out = FLSArrayPartOfStructure(12, 1, filler16, 36);
	public FixedLengthStringData[] mlprcind05Out = FLSArrayPartOfStructure(12, 1, filler16, 48);
	public FixedLengthStringData[] mlprcind06Out = FLSArrayPartOfStructure(12, 1, filler16, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData toyearsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 540);
	public FixedLengthStringData[] toyearOut = FLSArrayPartOfStructure(5, 12, toyearsOut, 0);
	public FixedLengthStringData[][] toyearO = FLSDArrayPartOfArrayStructure(12, 1, toyearOut, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(60).isAPartOf(toyearsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] toyear01Out = FLSArrayPartOfStructure(12, 1, filler17, 0);
	public FixedLengthStringData[] toyear02Out = FLSArrayPartOfStructure(12, 1, filler17, 12);
	public FixedLengthStringData[] toyear03Out = FLSArrayPartOfStructure(12, 1, filler17, 24);
	public FixedLengthStringData[] toyear04Out = FLSArrayPartOfStructure(12, 1, filler17, 36);
	public FixedLengthStringData[] toyear05Out = FLSArrayPartOfStructure(12, 1, filler17, 48);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sm605screenWritten = new LongData(0);
	public LongData Sm605protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm605ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, mlagttyp01, mlagttyp02, mlagttyp03, mlagttyp04, mlagttyp05, mlagttyp06, mlprcind01, mlprcind02, mlprcind03, mlprcind04, mlprcind05, mlprcind06, mlagttyp07, mlagttyp08, mlagttyp09, mlagttyp10, mlagttyp11, mlagttyp12, mlagttyp13, mlagttyp14, mlagttyp15, mlagttyp16, mlagttyp17, mlagttyp18, mlperpp01, toYear01, mlagtprd01, mlgrppp01, toYear02, mlperpp02, mlagtprd02, mlgrppp02, toYear03, mlperpp03, mlagtprd03, mlgrppp03, toYear04, mlperpp04, mlagtprd04, mlgrppp04, toYear05, mlperpp05, mlagtprd05, mlgrppp05};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, mlagttyp01Out, mlagttyp02Out, mlagttyp03Out, mlagttyp04Out, mlagttyp05Out, mlagttyp06Out, mlprcind01Out, mlprcind02Out, mlprcind03Out, mlprcind04Out, mlprcind05Out, mlprcind06Out, mlagttyp07Out, mlagttyp08Out, mlagttyp09Out, mlagttyp10Out, mlagttyp11Out, mlagttyp12Out, mlagttyp13Out, mlagttyp14Out, mlagttyp15Out, mlagttyp16Out, mlagttyp17Out, mlagttyp18Out, mlperpp01Out, toyear01Out, mlagtprd01Out, mlgrppp01Out, toyear02Out, mlperpp02Out, mlagtprd02Out, mlgrppp02Out, toyear03Out, mlperpp03Out, mlagtprd03Out, mlgrppp03Out, toyear04Out, mlperpp04Out, mlagtprd04Out, mlgrppp04Out, toyear05Out, mlperpp05Out, mlagtprd05Out, mlgrppp05Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, mlagttyp01Err, mlagttyp02Err, mlagttyp03Err, mlagttyp04Err, mlagttyp05Err, mlagttyp06Err, mlprcind01Err, mlprcind02Err, mlprcind03Err, mlprcind04Err, mlprcind05Err, mlprcind06Err, mlagttyp07Err, mlagttyp08Err, mlagttyp09Err, mlagttyp10Err, mlagttyp11Err, mlagttyp12Err, mlagttyp13Err, mlagttyp14Err, mlagttyp15Err, mlagttyp16Err, mlagttyp17Err, mlagttyp18Err, mlperpp01Err, toyear01Err, mlagtprd01Err, mlgrppp01Err, toyear02Err, mlperpp02Err, mlagtprd02Err, mlgrppp02Err, toyear03Err, mlperpp03Err, mlagtprd03Err, mlgrppp03Err, toyear04Err, mlperpp04Err, mlagtprd04Err, mlgrppp04Err, toyear05Err, mlperpp05Err, mlagtprd05Err, mlgrppp05Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm605screen.class;
		protectRecord = Sm605protect.class;
	}

}
