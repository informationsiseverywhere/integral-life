/*
 * File: ZrcapfDAOImpl.java
 * Date: July 21, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.agents.dataaccess.dao.ZrcapfDAO;
import com.csc.life.agents.dataaccess.model.Zrcapf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class ZrcapfDAOImpl  extends BaseDAOImpl<Zrcapf> implements ZrcapfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ZrcapfDAOImpl.class);	
	
		
	public Zrcapf getZrcapfRecord(Character Agntcoy, String Agntnum) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM ZRCAPF WHERE AGNTCOY = ? AND AGNTNUM = ?");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Zrcapf zrcapf = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, Agntcoy.toString());
			ps.setString(2, Agntnum);
			rs = ps.executeQuery();
			while(rs.next()){
				zrcapf = new Zrcapf();
				zrcapf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zrcapf.setAgntcoy(Agntcoy);
				zrcapf.setAgntnum(rs.getString("AGNTNUM").trim());
				zrcapf.setAgntype(rs.getString("AGNTYPE").trim());
				zrcapf.setMlagttyp(rs.getString("MLAGTTYP").trim());
				zrcapf.setZrecruit(rs.getString("ZRECRUIT").trim());
				zrcapf.setValidflag(rs.getString("VALIDFLAG").trim());
				zrcapf.setEffdate(rs.getBigDecimal("EFFDATE"));
				zrcapf.setUserProfile(rs.getString("USRPRF").trim());
				zrcapf.setJobName(rs.getString("JOBNM").trim());
				zrcapf.setDatime(rs.getTimestamp("DATIME"));
				
			}
		}catch (SQLException e) {
			LOGGER.error("getZrcapfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zrcapf;
	}	
	
	
	
}
