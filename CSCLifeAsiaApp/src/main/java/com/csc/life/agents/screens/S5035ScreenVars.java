package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5035
 * @version 1.0 generated on 30/08/09 06:31
 * @author Quipoz
 */
public class S5035ScreenVars extends SmartVarModel { 


	//TMLII-281 AG-01-002
	public FixedLengthStringData dataArea = new FixedLengthStringData(1762);//ilj-4
	//TMLII-281 AG-01-002
	public FixedLengthStringData dataFields = new FixedLengthStringData(850).isAPartOf(dataArea, 0);//ilj-4
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agentClass = DD.agcls.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData agclsd = DD.agclsd.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,64);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData bcmdesc = DD.bcmdesc.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData bcmtab = DD.bcmtab.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData bctind = DD.bctind.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData clientind = DD.clientind.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,175);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,232);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,235);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,236);
	public ZonedDecimalData dtetrm = DD.dtetrm.copyToZonedDecimal().isAPartOf(dataFields,244);
	public FixedLengthStringData exclAgmt = DD.excagr.copy().isAPartOf(dataFields,252);
	public ZonedDecimalData minsta = DD.minsta.copyToZonedDecimal().isAPartOf(dataFields,253);
	public FixedLengthStringData ocmdesc = DD.ocmdesc.copy().isAPartOf(dataFields,270);
	public FixedLengthStringData ocmtab = DD.ocmtab.copy().isAPartOf(dataFields,300);
	public ZonedDecimalData ovcpc = DD.ovcpc.copyToZonedDecimal().isAPartOf(dataFields,305);
	public FixedLengthStringData payenme = DD.payenme.copy().isAPartOf(dataFields,310);
	public FixedLengthStringData payfrq = DD.payfrq.copy().isAPartOf(dataFields,357);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(dataFields,359);
	public FixedLengthStringData paysel = DD.paysel.copy().isAPartOf(dataFields,361);
	public FixedLengthStringData pyfdesc = DD.pyfdesc.copy().isAPartOf(dataFields,371);
	public FixedLengthStringData pymdesc = DD.pymdesc.copy().isAPartOf(dataFields,401);
	public FixedLengthStringData rcmdesc = DD.rcmdesc.copy().isAPartOf(dataFields,431);
	public FixedLengthStringData rcmtab = DD.rcmtab.copy().isAPartOf(dataFields,461);
	public FixedLengthStringData repname = DD.repname.copy().isAPartOf(dataFields,465);
	public FixedLengthStringData repsel = DD.repsel.copy().isAPartOf(dataFields,512);
	public FixedLengthStringData scmdsc = DD.scmdsc.copy().isAPartOf(dataFields,522);
	public FixedLengthStringData scmtab = DD.scmtab.copy().isAPartOf(dataFields,552);
	public FixedLengthStringData tagd = DD.tagd.copy().isAPartOf(dataFields,556);
	public FixedLengthStringData tagsusdesc = DD.tagsusdesc.copy().isAPartOf(dataFields,557);
	public FixedLengthStringData tagsusind = DD.tagsusind.copy().isAPartOf(dataFields,587);
	public FixedLengthStringData tlaglicno = DD.tlaglicno.copy().isAPartOf(dataFields,588);
	public ZonedDecimalData tlicexpdt = DD.tlicexpdt.copyToZonedDecimal().isAPartOf(dataFields,603);
	public FixedLengthStringData tsalesdsc = DD.tsalesdsc.copy().isAPartOf(dataFields,611);
	public FixedLengthStringData tsalesunt = DD.tsalesunt.copy().isAPartOf(dataFields,641);
	public FixedLengthStringData zrorind = DD.zrorind.copy().isAPartOf(dataFields,646);
	//TMLII-281 AG-01-002 START
	public FixedLengthStringData agtname = DD.agtname.copy().isAPartOf(dataFields,647);
	public FixedLengthStringData zrecruit = DD.zrecruit.copy().isAPartOf(dataFields, 694);
	
	
	//ICIL-12 start	
	public FixedLengthStringData agentrefcode = DD.agentcode.copy().isAPartOf(dataFields,702);
	public FixedLengthStringData agencymgr = DD.agencymgr.copy().isAPartOf(dataFields, 722);
	public FixedLengthStringData distrctcode = DD.distcode.copy().isAPartOf(dataFields, 732);
	public FixedLengthStringData bnkinternalrole = DD.bnkintrole.copy().isAPartOf(dataFields, 752);
	public FixedLengthStringData banceindicatorsel = DD.bnkind.copy().isAPartOf(dataFields, 754);	
	//ICIL-12 End
	public FixedLengthStringData agncysalic = DD.agncysalic.copy().isAPartOf(dataFields, 755);
	//ILJ-4
	public FixedLengthStringData qualification = DD.qualification.copy().isAPartOf(dataFields,756);
	public FixedLengthStringData agncysel = DD.agncysel.copy().isAPartOf(dataFields,757);
	public FixedLengthStringData agncydesc = DD.cltname.copy().isAPartOf(dataFields,765);
	public FixedLengthStringData salebranch = DD.salebranch.copy().isAPartOf(dataFields, 812);
	public FixedLengthStringData salebranchdes = DD.upleveldes.copy().isAPartOf(dataFields, 820);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 850);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agclsdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData bcmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData bcmtabErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData bctindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData clientindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData dtetrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData excagrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData minstaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData ocmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData ocmtabErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData ovcpcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData payenmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData payfrqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData paymthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData payselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData pyfdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData pymdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData rcmdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData rcmtabErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData repnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData repselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData scmdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData scmtabErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData tagdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData tagsusdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData tagsusindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData tlaglicnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData tlicexpdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData tsalesdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData tsalesuntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData zrorindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	//TMLII-281 AG-01-002 START
	public FixedLengthStringData agtnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData zrecruitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	
	//TMLII-281 AG-01-002 END		
	//TMLII-281 AG-01-002
	
	//ICIL-12 start	
	public FixedLengthStringData agentrefcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData agencymgrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);	
	public FixedLengthStringData distrctcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData bnkinternalroleErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData banceindicatorselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);		
	//ICIL-12 End
	public FixedLengthStringData agncysalicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData qualificationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);//ILJ-4
	public FixedLengthStringData agncyselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData agncydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData salebranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	public FixedLengthStringData salebranchdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(684).isAPartOf(dataArea, 1078);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agclsdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] bcmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] bcmtabOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] bctindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] clientindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] dtetrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] excagrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] minstaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] ocmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] ocmtabOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] ovcpcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] payenmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] payfrqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] paymthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] payselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] pyfdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] pymdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] rcmdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] rcmtabOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] repnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] repselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] scmdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] scmtabOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] tagdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] tagsusdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] tagsusindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] tlaglicnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] tlicexpdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] tsalesdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] tsalesuntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] zrorindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	//TMLII-281 AG-01-002 START
	public FixedLengthStringData[] agtnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] zrecruitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	
		//TMLII-281 AG-01-002 END
	//ICIL-12 start	
	public FixedLengthStringData[] agentrefcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] agencymgrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] distrctcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] bnkinternalroleOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] banceindicatorselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);	
	//ICIL-12 End	
	public FixedLengthStringData[] agncysalicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] qualificationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//ILJ-4
	public FixedLengthStringData[] agncyselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] agncydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] salebranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] salebranchdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);
	
	
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dtetrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData tlicexpdtDisp = new FixedLengthStringData(10);

	public LongData S5035screenWritten = new LongData(0);
	public LongData S5035protectWritten = new LongData(0);
	public FixedLengthStringData iljScreenflag = new FixedLengthStringData(1);  //ILJ-4
	
	
	public boolean hasSubfile() {
		return false;
	}


	public S5035ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,new String[] {"01","25","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dteappOut,new String[] {"02","21","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(excagrOut,new String[] {"20","21","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agtypeOut,new String[] {"05","81","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntbrOut,new String[] {"51","52","-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aracdeOut,new String[] {"04","21","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(repselOut,new String[] {"07","61","-07","60",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ovcpcOut,new String[] {"06","64","-06","65",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payselOut,new String[] {"08","21","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(paymthOut,new String[] {"09","21","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payfrqOut,new String[] {"10","21","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(currcodeOut,new String[] {"15","21","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minstaOut,new String[] {"23","21","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dtetrmOut,new String[] {"34","80","-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bcmtabOut,new String[] {"11","21","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(scmtabOut,new String[] {"12","21","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcmtabOut,new String[] {"13","21","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agclsOut,new String[] {"16","21","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ocmtabOut,new String[] {"14","21","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clientindOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bctindOut,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tagdOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrorindOut,new String[] {"22","62","-22","63",null, null, null, null, null, null, null, null});
		fieldIndMap.put(tlaglicnoOut,new String[] {"55","21","-55","95", null, null, null, null, null, null, null, null});
		fieldIndMap.put(tlicexpdtOut,new String[] {"56","21","-56","96", null, null, null, null, null, null, null, null});
		fieldIndMap.put(tagsusindOut,new String[] {"53","21","-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(tsalesuntOut,new String[] {"54","21","-54",null, null, null, null, null, null, null, null, null});
		//TMLII-281 AG-01-002
		fieldIndMap.put(zrecruitOut, new String[] {"68","67","-68","66",null, null, null, null, null, null, null, null});
		fieldIndMap.put(agncysalicOut, new String[] {"69","70","-69","71",null, null, null, null, null, null, null, null});
		//TMLII-281 AG-01-002 start - added new fields for recruiter
		//ICIL-12 start			
		fieldIndMap.put(agentrefcodeOut,new String[] {"84","24","-84","82", null, null, null, null, null, null, null, null});
		fieldIndMap.put(agencymgrOut,new String[] {"85","26","-85","83", null, null, null, null, null, null, null, null});
		fieldIndMap.put(distrctcodeOut,new String[] {"86","27","-86","87", null, null, null, null, null, null, null, null});		
		fieldIndMap.put(bnkinternalroleOut,new String[] {"88","28","-88","89", null, null, null, null, null, null, null, null});
		fieldIndMap.put(banceindicatorselOut,new String[] {"90","29","-90","91", null, null, null, null, null, null, null, null});
		fieldIndMap.put(agncyselOut,new String[] {"58","30","-58","93", null, null, null, null, null, null, null, null});
		fieldIndMap.put(salebranchOut,new String[] { "57", "31", "-57", "59", null, null, null, null, null, null, null, null });
		
		//ICIL-12 End	
		screenFields = new BaseData[] {agbrdesc,agentClass, agclsd,agntbr,agnum,agtydesc,agtype,aracde,aradesc,bcmdesc,bcmtab,bctind,clientind,clntsel,cltname,currcode,ddind,dteapp,dtetrm,exclAgmt,minsta,ocmdesc,ocmtab,ovcpc,payenme,payfrq,paymth,paysel,pyfdesc,pymdesc,rcmdesc,rcmtab,repname,repsel,scmdsc,scmtab,tagd,tagsusdesc,tagsusind,tlaglicno,tlicexpdt,tsalesdsc,tsalesunt,zrorind,agtname,zrecruit,agentrefcode,agencymgr,distrctcode,bnkinternalrole,banceindicatorsel,agncysalic,qualification,agncysel,agncydesc,salebranch,salebranchdes};
		screenOutFields = new BaseData[][] {agbrdescOut,agclsOut, agclsdOut,agntbrOut,agnumOut,agtydescOut,agtypeOut,aracdeOut,aradescOut,bcmdescOut,bcmtabOut,bctindOut,clientindOut,clntselOut,cltnameOut,currcodeOut,ddindOut,dteappOut,dtetrmOut,excagrOut,minstaOut,ocmdescOut,ocmtabOut,ovcpcOut,payenmeOut,payfrqOut,paymthOut,payselOut,pyfdescOut,pymdescOut,rcmdescOut,rcmtabOut,repnameOut,repselOut,scmdscOut,scmtabOut,tagdOut,tagsusdescOut,tagsusindOut,tlaglicnoOut,tlicexpdtOut,tsalesdscOut,tsalesuntOut,zrorindOut,agtnameOut,zrecruitOut,agentrefcodeOut,agencymgrOut,distrctcodeOut,bnkinternalroleOut,banceindicatorselOut,agncysalicOut,qualificationOut,agncyselOut,agncydescOut,salebranchOut,salebranchdesOut};
		screenErrFields = new BaseData[] {agbrdescErr,agclsErr, agclsdErr,agntbrErr,agnumErr,agtydescErr,agtypeErr,aracdeErr,aradescErr,bcmdescErr,bcmtabErr,bctindErr,clientindErr,clntselErr,cltnameErr,currcodeErr,ddindErr,dteappErr,dtetrmErr,excagrErr,minstaErr,ocmdescErr,ocmtabErr,ovcpcErr,payenmeErr,payfrqErr,paymthErr,payselErr,pyfdescErr,pymdescErr,rcmdescErr,rcmtabErr,repnameErr,repselErr,scmdscErr,scmtabErr,tagdErr,tagsusdescErr,tagsusindErr,tlaglicnoErr,tlicexpdtErr,tsalesdscErr,tsalesuntErr,zrorindErr,agtnameErr,zrecruitErr,agentrefcodeErr,agencymgrErr,distrctcodeErr,bnkinternalroleErr,banceindicatorselErr,agncysalicErr,qualificationErr,agncyselErr,agncydescErr,salebranchErr,salebranchdesErr};
		//TMLII-281 AG-01-002 end
		screenDateFields = new BaseData[] {dteapp, dtetrm, tlicexpdt};
		screenDateErrFields = new BaseData[] {dteappErr, dtetrmErr, tlicexpdtErr};
		screenDateDispFields = new BaseData[] {dteappDisp, dtetrmDisp, tlicexpdtDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5035screen.class;
		protectRecord = S5035protect.class;
	}

}
