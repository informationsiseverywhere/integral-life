package com.csc.life.agents.dataaccess.dao;

import com.csc.smart400framework.dataaccess.dao.GenericDAO;
import com.csc.smart400framework.dataaccess.model.Zqbhpf;

public interface ZqbhpfDAO  extends GenericDAO<Zqbhpf,Long> 
{
	public void save(Zqbhpf zqbh);
}
