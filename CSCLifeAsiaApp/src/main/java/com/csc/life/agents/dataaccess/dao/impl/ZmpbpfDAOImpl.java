/*
 * File: ZmpbpfDAOImpl.java
 * Date: July 21, 2016
 * Author: CSC
 * Created by: pmujavadiya
 * 
 * Copyright (2016) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.dataaccess.dao.impl;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.agents.dataaccess.dao.ZmpbpfDAO;
import com.csc.life.agents.dataaccess.model.Zmpbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;


public class ZmpbpfDAOImpl  extends BaseDAOImpl<Zmpbpf> implements ZmpbpfDAO {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ZmpbpfDAOImpl.class);	
	public Zmpbpf getZmpbpfRecord(Zmpbpf zmpbp) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM ZMPBPF WHERE BATCACTMN = ? AND BATCACTYR = ? AND AGNTCOY = ? AND AGNTNUM = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND  RIDER = ? AND EFFDATE = ? AND TRANNO = ? AND BATCTRCDE=?");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		Zmpbpf zmpbpf = null;
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setBigDecimal(1, zmpbp.getBatcactmn());
			ps.setBigDecimal(2, zmpbp.getBatcactyr());
			ps.setString(3,  zmpbp.getAgntcoy());
			ps.setString(4,  zmpbp.getAgntnum());
			ps.setString(5,  zmpbp.getChdrnum());
			ps.setString(6,  zmpbp.getLife());
			ps.setString(7,  zmpbp.getCoverage());//IJTI-320
			ps.setString(8,  zmpbp.getRider());
			ps.setBigDecimal(9, zmpbp.getEffdate());
			ps.setBigDecimal(10, zmpbp.getTranno());
			ps.setString(11,  zmpbp.getBatctrcde());
			rs = ps.executeQuery();
			while(rs.next()){
				zmpbpf = new Zmpbpf();
				zmpbpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zmpbpf.setAcctamt(rs.getBigDecimal("ACCTAMT"));
				zmpbpf.setAgntcoy(rs.getString("AGNTCOY"));
				zmpbpf.setAgntnum(rs.getString("AGNTNUM").trim());
				zmpbpf.setBatcactmn(rs.getBigDecimal("BATCACTMN"));
				zmpbpf.setBatcactyr(rs.getBigDecimal("BATCACTYR"));
				zmpbpf.setBatctrcde(rs.getString("BATCTRCDE").trim());
				zmpbpf.setBonusamt(rs.getBigDecimal("BONUSAMT"));
				zmpbpf.setChdrnum(rs.getString("CHDRNUM").trim());
				zmpbpf.setComtot(rs.getBigDecimal("COMTOT"));
				zmpbpf.setCoverage(rs.getString("COVERAGE"));
				zmpbpf.setDatime(rs.getTimestamp("DATIME"));
				zmpbpf.setEffdate(rs.getBigDecimal("EFFDATE"));
				zmpbpf.setJobName(rs.getString("JOBNM").trim());
				zmpbpf.setJobno(rs.getBigDecimal("JOBNO"));
				zmpbpf.setLife(rs.getString("LIFE").trim());
				zmpbpf.setOrigcurr(rs.getString("ORIGCURR").trim());
				zmpbpf.setPrcdate(rs.getBigDecimal("PRCDATE"));
				zmpbpf.setPrcent(rs.getBigDecimal("PRCENT"));
				zmpbpf.setPrcnt(rs.getBigDecimal("PRCNT"));
				zmpbpf.setRider(rs.getString("RIDER"));
				zmpbpf.setTranno(rs.getBigDecimal("TRANNO"));
				zmpbpf.setUserProfile(rs.getString("USRPRF").trim());
				
			}
		}catch (SQLException e) {
			LOGGER.error("getzmpbpfRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zmpbpf;
	}


		
	
	public int updateZmpbRecord(Zmpbpf zmpbpf){
		   String SQL_ZMPB_UPDATE = "UPDATE ZMPBPF SET COMTOT=?,PRCENT=?,PRCNT=?,BONUSAMT=?  WHERE UNIQUE_NUMBER=? ";

           PreparedStatement psZmpbUpdate = getPrepareStatement(SQL_ZMPB_UPDATE);
           int count;
           try {
                   psZmpbUpdate.setBigDecimal(1, zmpbpf.getComtot());
                   psZmpbUpdate.setBigDecimal(2, zmpbpf.getPrcent());
                   psZmpbUpdate.setBigDecimal(3, zmpbpf.getPrcnt());
                   psZmpbUpdate.setBigDecimal(4, zmpbpf.getBonusamt());
                   psZmpbUpdate.setLong(5, zmpbpf.getUniqueNumber());
                   count = psZmpbUpdate.executeUpdate();
           } catch (SQLException e) {
               LOGGER.error("updateZmpbRecord()", e);//IJTI-1561
               throw new SQLRuntimeException(e);
           } finally {
               close(psZmpbUpdate, null);
           }
       return count;
   }

	public int insertZmpbData(Zmpbpf zmpbpf)
	{		
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ZMPBPF");
		sql.append("(ACCTAMT,AGNTCOY,AGNTNUM,BATCACTMN,BATCACTYR,BATCTRCDE,BONUSAMT,CHDRNUM,COMTOT,COVERAGE,DATIME,EFFDATE,JOBNM,JOBNO,LIFE,ORIGCURR,PRCDATE,PRCENT,PRCNT,RIDER,TRANNO,USRPRF)");
		sql.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		int index = 1;
		int count = 0; 
		try {

			ps = getConnection().prepareStatement(sql.toString());
			ps.setBigDecimal(index++,zmpbpf.getAcctamt());
			ps.setString(index++,zmpbpf.getAgntcoy());
			ps.setString(index++,zmpbpf.getAgntnum());
			ps.setBigDecimal(index++,zmpbpf.getBatcactmn());
			ps.setBigDecimal(index++,zmpbpf.getBatcactyr());
			ps.setString(index++,zmpbpf.getBatctrcde());
			ps.setBigDecimal(index++,zmpbpf.getBonusamt());
			ps.setString(index++,zmpbpf.getChdrnum());
			ps.setBigDecimal(index++,zmpbpf.getComtot());
			ps.setString(index++,zmpbpf.getCoverage());
			ps.setTimestamp(index++,new Timestamp(System.currentTimeMillis()));
			ps.setBigDecimal(index++,zmpbpf.getEffdate());
			ps.setString(index++,zmpbpf.getJobName());
			ps.setBigDecimal(index++,zmpbpf.getJobno());
			ps.setString(index++,zmpbpf.getLife());
			ps.setString(index++,zmpbpf.getOrigcurr());
			ps.setBigDecimal(index++,zmpbpf.getPrcdate());
			ps.setBigDecimal(index++,zmpbpf.getPrcent());
			ps.setBigDecimal(index++,zmpbpf.getPrcnt());
			ps.setString(index++,zmpbpf.getRider());			
		    ps.setBigDecimal(index++,zmpbpf.getTranno());		
			ps.setString(index++,zmpbpf.getUserProfile());
			ps.executeUpdate();			 			
			
			
					
		} catch (SQLException e) {
			LOGGER.error("Insertzmpbpf()", e);
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,null);
		}	
		
		return count;
	}


	
	
}
