package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:09
 * Description:
 * Copybook name: AGORPSLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agorpslkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agorpslFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agorpslKey = new FixedLengthStringData(256).isAPartOf(agorpslFileKey, 0, REDEFINE);
  	public FixedLengthStringData agorpslAgntcoy = new FixedLengthStringData(1).isAPartOf(agorpslKey, 0);
  	public FixedLengthStringData agorpslAgntnum = new FixedLengthStringData(8).isAPartOf(agorpslKey, 1);
  	public FixedLengthStringData agorpslReportag = new FixedLengthStringData(8).isAPartOf(agorpslKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(239).isAPartOf(agorpslKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agorpslFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agorpslFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}