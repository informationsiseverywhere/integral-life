/*
 * File: Br669.java
 * Date: 29 August 2009 22:35:03
 * Author: Quipoz Limited
 * 
 * Class transformed from BR669.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.life.agents.cls.Cr673;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.ZbnwpfTableDAM;
import com.csc.life.agents.procedures.Zbwcmpy;
import com.csc.life.agents.recordstructures.Zbnwagyrec;
import com.csc.life.agents.recordstructures.Zbwcmpyrec;
import com.csc.life.agents.tablestructures.Tr663rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This is to extract Agency Details for Bonuw Workbench.
*
*   Extract the agnecy records.
*    .No of records read CT01
*    .No of records read CT02
*
*   In INPUT-OUTPUT SECTION, we specify the output file ZBNWPF in which
*   the agent movement records are written.
*   Define some variables for DB override
*   And copy the copybook ZBNWAGYREC.
*
*   Initialise
*     - In 1000-INITIALISE SECTION We override the Database file ZBNWPF
*       for records written.
*
*     - We read the table TR663 with the key of sign-on company to
*       determine the agency driven table
*     - We create the section 1100-READ-TR663
*       READ TR663
*       EVALUATE TR663-ZPARAM
*       Afterwards, we initialize the IO to read the agency records
*     - We create the section 1200-LOAD-AGENCY663  to read the agency
*
*      Read
*       -In 2000-READ-FILE SECTION, we read all items from the driven
*        table until EOF and increment the control total for every
*        record read.
*
*      Edit
*       - In 3000-UPDATE SECTION, for every record read
*         we write the record together with required information
*         by performing the section 3100-WRITE
*       - Create the section 3100-WRITE to write the agency details recor
*       - Create the new section 3200-COMPANY to retrieve the company cod
*         We would read the first agent under the agency parameter
*          (area code, sales unit or branch code).
*         And then use then call the subroutine ZBWCMPY to retrieve the
*          company
*
*      Update
*       - In 4000-UPDATE SECTION, we close the output file ZBNWPF
*         and copy the file to the FTP library which is given in the
*         process definition.
*       - Create the section A100-DATE-CONVERSION to  convert the date
*         in the format MM/DD/YYYY.
*       - Create the section A200-PACK-STRING to pack the data string
*         such that semi colon will be added to separate the data.
*
*
***********************************************************************
* </pre>
*/
public class Br669 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ZbnwpfTableDAM zbnwpf = new ZbnwpfTableDAM();
	private ZbnwpfTableDAM zbnwpfRec = new ZbnwpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR669");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).init(SPACES);
	private ZonedDecimalData wsaaInptDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaInptDateX = new FixedLengthStringData(8).isAPartOf(wsaaInptDate, 0, REDEFINE);
	private FixedLengthStringData wsaaInptYyyy = new FixedLengthStringData(4).isAPartOf(wsaaInptDateX, 0);
	private FixedLengthStringData wsaaInptMm = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 4);
	private FixedLengthStringData wsaaInptDd = new FixedLengthStringData(2).isAPartOf(wsaaInptDateX, 6);

	private FixedLengthStringData wsaaConvDate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaConvMm = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 2, FILLER).init("/");
	private FixedLengthStringData wsaaConvDd = new FixedLengthStringData(2).isAPartOf(wsaaConvDate, 3);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaConvDate, 5, FILLER).init("/");
	private FixedLengthStringData wsaaConvYyyy = new FixedLengthStringData(4).isAPartOf(wsaaConvDate, 6);
	private FixedLengthStringData wsaaString = new FixedLengthStringData(1500);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaZbnwFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaZbnwFn, 0, FILLER).init("ZBNW");
	private FixedLengthStringData wsaaZbnwRunid = new FixedLengthStringData(2).isAPartOf(wsaaZbnwFn, 4);
	private ZonedDecimalData wsaaZbnwJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZbnwFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(9);
	private FixedLengthStringData filler3 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaFtpLibrary = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFtpFile = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaTable = new FixedLengthStringData(5).init(SPACES);

	private FixedLengthStringData wsaaAgentinfor = new FixedLengthStringData(30000);
	private FixedLengthStringData[] wsaaAgent = FLSArrayPartOfStructure(500, 60, wsaaAgentinfor, 0);
	private FixedLengthStringData[] wsaaAgency = FLSDArrayPartOfArrayStructure(10, wsaaAgent, 0);
	private FixedLengthStringData[] wsaaAgyname = FLSDArrayPartOfArrayStructure(50, wsaaAgent, 10);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private int wsaaMaxItem = 500;
	private String itemrec = "ITEMREC";
	private String agntrec = "AGNTREC";
	private String aglfrec = "AGLFREC";
		/* TABLES */
	private String tr663 = "TR663";
	private String t5696 = "T5696";
	private String tt518 = "TT518";
	private String t1692 = "T1692";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Tr663rec tr663rec = new Tr663rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private Zbnwagyrec zbnwagyrec = new Zbnwagyrec();
	private Zbwcmpyrec zbwcmpyrec = new Zbwcmpyrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		call1220, 
		exit1290, 
		exit2090, 
		exit3390, 
		exit3490
	}

	public Br669() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspcomn.edterror;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaZbnwRunid.set(bprdIO.getSystemParam04());
		wsaaZbnwJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(ZBNWPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaZbnwFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		zbnwpf.openOutput();
		readTr6631100();
		loadAgency1200();
		wsaaIx.set(0);
		/*EXIT*/
	}

protected void readTr6631100()
	{
		para1100();
	}

protected void para1100()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(tr663);
		itemIO.setItemitem(bprdIO.getCompany());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			tr663rec.tr663Rec.set(itemIO.getGenarea());
		}
		else {
			tr663rec.tr663Rec.set(SPACES);
		}
		if (isEQ(tr663rec.zparam,"AC")){
			wsaaTable.set(t5696);
		}
		else if (isEQ(tr663rec.zparam,"SU")){
			wsaaTable.set(tt518);
		}
		else if (isEQ(tr663rec.zparam,"BR")){
			wsaaTable.set(t1692);
		}
	}

protected void loadAgency1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					init1210();
				}
				case call1220: {
					call1220();
					next1270();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init1210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bprdIO.getCompany());
		itemIO.setItemtabl(wsaaTable);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMPFX");
		itemIO.setFormat(itemrec);
		wsaaIx.set(1);
	}

protected void call1220()
	{
		if (isGT(wsaaIx,wsaaMaxItem)) {
			goTo(GotoLabel.exit1290);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bprdIO.getCompany())
		|| isNE(itemIO.getItemtabl(),wsaaTable)
		|| isNE(itemIO.getItempfx(),smtpfxcpy.item)) {
			goTo(GotoLabel.exit1290);
		}
		wsaaAgency[wsaaIx.toInt()].set(itemIO.getItemitem());
		a300DescioCall();
		wsaaAgyname[wsaaIx.toInt()].set(descIO.getLongdesc());
	}

protected void next1270()
	{
		wsaaIx.add(1);
		itemIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call1220);
	}

protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}

protected void readFile2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaIx.add(1);
		if (isGT(wsaaIx,wsaaMaxItem)
		|| isEQ(wsaaAgency[wsaaIx.toInt()],SPACES)
		|| isEQ(wsaaAgyname[wsaaIx.toInt()],SPACES)) {
			wsspcomn.edterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		else {
			contotrec.totval.set(1);
			contotrec.totno.set(ct01);
			callContot001();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		write3110();
	}

protected void write3110()
	{
		initialize(zbnwagyrec.bnwagyRec);
		zbnwagyrec.bnwagyTrcde.set("CR");
		zbnwagyrec.bnwagySource.set("LIFE/ASIA");
		company3200();
		zbnwagyrec.bnwagyCompany.set(wsaaCompany);
		zbnwagyrec.bnwagyAgency.set(wsaaAgency[wsaaIx.toInt()]);
		zbnwagyrec.bnwagyAgyname.set(wsaaAgyname[wsaaIx.toInt()]);
		zbnwagyrec.bnwagyAddr01.set(SPACES);
		zbnwagyrec.bnwagyAddr02.set(SPACES);
		zbnwagyrec.bnwagyAddr03.set(SPACES);
		zbnwagyrec.bnwagyCity.set(SPACES);
		zbnwagyrec.bnwagyState.set(SPACES);
		zbnwagyrec.bnwagyZip.set(SPACES);
		zbnwagyrec.bnwagyCountry.set(SPACES);
		zbnwagyrec.bnwagyTaxid.set(tr663rec.ztaxid);
		wsaaInptDate.set(tr663rec.dteapp);
		a100DateConversion();
		zbnwagyrec.bnwagyStrtdate.set(wsaaConvDate);
		zbnwagyrec.bnwagyTrmdate.set("99/99/9999");
		wsaaInptDate.set(bsscIO.getEffectiveDate());
		a100DateConversion();
		zbnwagyrec.bnwagyProcdate.set(wsaaConvDate);
		a200PackString();
		zbnwpfRec.set(wsaaString);
		zbnwpf.write(zbnwpfRec);
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
	}

protected void company3200()
	{
		retrive3210();
	}

protected void retrive3210()
	{
		if (isEQ(tr663rec.zparam,"AC")){
			aglfIO.setParams(SPACES);
			aglfIO.setAgntcoy(bprdIO.getCompany());
			aglfIO.setAgntnum(SPACES);
			aglfIO.setFormat(aglfrec);
			aglfIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			aglfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			aglfIO.setFitKeysSearch("AGNTCOY");
			while ( !(isEQ(aglfIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaAgency[wsaaIx.toInt()],aglfIO.getAracde()))) {
				readAglf3300();
			}
			
			if (isNE(aglfIO.getStatuz(),varcom.endp)) {
				wsaaAgntnum.set(aglfIO.getAgntnum());
			}
			else {
				wsaaAgntnum.set(SPACES);
			}
		}
		else if (isEQ(tr663rec.zparam,"SU")){
			aglfIO.setParams(SPACES);
			aglfIO.setAgntcoy(bprdIO.getCompany());
			aglfIO.setAgntnum(SPACES);
			aglfIO.setFormat(aglfrec);
			aglfIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			aglfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			aglfIO.setFitKeysSearch("AGNTCOY");
			while ( !(isEQ(aglfIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaAgency[wsaaIx.toInt()],aglfIO.getTsalesunt()))) {
				readAglf3300();
			}
			
			if (isNE(aglfIO.getStatuz(),varcom.endp)) {
				wsaaAgntnum.set(aglfIO.getAgntnum());
			}
			else {
				wsaaAgntnum.set(SPACES);
			}
		}
		else if (isEQ(tr663rec.zparam,"BR")){
			agntIO.setParams(SPACES);
			agntIO.setAgntpfx(fsupfxcpy.agnt);
			agntIO.setAgntcoy(bprdIO.getCompany());
			agntIO.setAgntnum(SPACES);
			agntIO.setFormat(agntrec);
			agntIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			agntIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			agntIO.setFitKeysSearch("AGNTPFX", "AGNTCOY");
			while ( !(isEQ(agntIO.getStatuz(),varcom.endp)
			|| isEQ(wsaaAgency[wsaaIx.toInt()],agntIO.getAgntbr()))) {
				readAgnt3400();
			}
			
			if (isNE(agntIO.getStatuz(),varcom.endp)) {
				wsaaAgntnum.set(agntIO.getAgntnum());
			}
			else {
				wsaaAgntnum.set(SPACES);
			}
		}
		if (isNE(wsaaAgntnum,SPACES)) {
			initialize(zbwcmpyrec.bwcmpyRec);
			zbwcmpyrec.signonCompany.set(bprdIO.getCompany());
			zbwcmpyrec.function.set("AGT");
			zbwcmpyrec.input.set(wsaaAgntnum);
			zbwcmpyrec.language.set(bsscIO.getLanguage());
			callProgram(Zbwcmpy.class, zbwcmpyrec.bwcmpyRec);
			if (isEQ(zbwcmpyrec.statuz,varcom.oK)) {
				wsaaCompany.set(zbwcmpyrec.bwCompany);
			}
			else {
				wsaaCompany.set(SPACES);
			}
		}
		else {
			wsaaCompany.set(SPACES);
		}
	}

protected void readAglf3300()
	{
		try {
			read3310();
		}
		catch (GOTOException e){
		}
	}

protected void read3310()
	{
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(),varcom.endp)
		|| isNE(aglfIO.getAgntcoy(),bprdIO.getCompany())) {
			aglfIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3390);
		}
		aglfIO.setFunction(varcom.nextr);
	}

protected void readAgnt3400()
	{
		try {
			read13310();
		}
		catch (GOTOException e){
		}
	}

protected void read13310()
	{
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isEQ(agntIO.getStatuz(),varcom.endp)
		|| isNE(agntIO.getAgntcoy(),bprdIO.getCompany())
		|| isNE(agntIO.getAgntpfx(),fsupfxcpy.agnt)) {
			agntIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3490);
		}
		agntIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		zbnwpf.close();
		wsaaStatuz.set(SPACES);
		wsaaFtpLibrary.set(bprdIO.getSystemParam01());
		wsaaFtpFile.set(bprdIO.getSystemParam02());
		callProgram(Cr673.class, wsaaStatuz, bprdIO.getRunLibrary(), wsaaZbnwFn, wsaaFtpLibrary, wsaaFtpFile);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(wsaaStatuz.toString());
			stringVariable1.append(bprdIO.getRunLibrary().toString());
			stringVariable1.append(wsaaZbnwFn.toString());
			stringVariable1.append(wsaaFtpLibrary.toString());
			stringVariable1.append(wsaaFtpFile.toString());
			syserrrec.params.setLeft(stringVariable1.toString());
			fatalError600();
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a100DateConversion()
	{
		/*A101-CONVERSION*/
		wsaaConvYyyy.set(wsaaInptYyyy);
		wsaaConvMm.set(wsaaInptMm);
		wsaaConvDd.set(wsaaInptDd);
		/*A109-EXIT*/
	}

protected void a200PackString()
	{
		/*A201-PACK*/
		wsaaString.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyTrcde, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagySource, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyCompany, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyAgency, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyAgyname, "  "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyAddr01, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyAddr02, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyAddr03, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyCity, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyState, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyZip, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyCountry, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller1, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller2, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller3, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller4, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller5, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller6, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyFiller7, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyTaxid, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyStrtdate, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyTrmdate, " "));
		stringVariable1.append(";");
		stringVariable1.append(delimitedExp(zbnwagyrec.bnwagyProcdate, " "));
		wsaaString.setLeft(stringVariable1.toString());
		/*A209-EXIT*/
	}

protected void a300DescioCall()
	{
		a310Description();
	}

protected void a310Description()
	{
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(wsaaTable);
		descIO.setDescitem(wsaaAgency[wsaaIx.toInt()]);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bprdIO.getCompany());
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
	}
}
