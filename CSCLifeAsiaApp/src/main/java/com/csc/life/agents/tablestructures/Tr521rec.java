package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:19:56
 * Description:
 * Copybook name: TR521REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr521rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr521Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(tr521Rec, 0);
  	public FixedLengthStringData zcomcodes = new FixedLengthStringData(40).isAPartOf(tr521Rec, 8);
  	public FixedLengthStringData[] zcomcode = FLSArrayPartOfStructure(10, 4, zcomcodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(zcomcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zcomcode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData zcomcode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData zcomcode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData zcomcode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData zcomcode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData zcomcode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData zcomcode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData zcomcode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData zcomcode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData zcomcode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData zrsppercs = new FixedLengthStringData(50).isAPartOf(tr521Rec, 48);
  	public ZonedDecimalData[] zrspperc = ZDArrayPartOfStructure(10, 5, 2, zrsppercs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(50).isAPartOf(zrsppercs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrspperc01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
  	public ZonedDecimalData zrspperc02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
  	public ZonedDecimalData zrspperc03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
  	public ZonedDecimalData zrspperc04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
  	public ZonedDecimalData zrspperc05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
  	public ZonedDecimalData zrspperc06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
  	public ZonedDecimalData zrspperc07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
  	public ZonedDecimalData zrspperc08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
  	public ZonedDecimalData zrspperc09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
  	public ZonedDecimalData zrspperc10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
  	public FixedLengthStringData zrtupercs = new FixedLengthStringData(50).isAPartOf(tr521Rec, 98);
  	public ZonedDecimalData[] zrtuperc = ZDArrayPartOfStructure(10, 5, 2, zrtupercs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(zrtupercs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zrtuperc01 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData zrtuperc02 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 5);
  	public ZonedDecimalData zrtuperc03 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 10);
  	public ZonedDecimalData zrtuperc04 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 15);
  	public ZonedDecimalData zrtuperc05 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 20);
  	public ZonedDecimalData zrtuperc06 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 25);
  	public ZonedDecimalData zrtuperc07 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 30);
  	public ZonedDecimalData zrtuperc08 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 35);
  	public ZonedDecimalData zrtuperc09 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 40);
  	public ZonedDecimalData zrtuperc10 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 45);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(352).isAPartOf(tr521Rec, 148, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr521Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr521Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}