package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl02 Date: 11 November 2019 Author: vdivisala
 */
public class Sjl02ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(856);
	public FixedLengthStringData dataFields = new FixedLengthStringData(440).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields, 9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields, 17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 55);
	public FixedLengthStringData qlfys = new FixedLengthStringData(80).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] qlfy = FLSArrayPartOfStructure(10, 8, qlfys, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(qlfys, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfy01 = DD.qlfy.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData qlfy02 = DD.qlfy.copy().isAPartOf(filler1, 8);
	public FixedLengthStringData qlfy03 = DD.qlfy.copy().isAPartOf(filler1, 16);
	public FixedLengthStringData qlfy04 = DD.qlfy.copy().isAPartOf(filler1, 24);
	public FixedLengthStringData qlfy05 = DD.qlfy.copy().isAPartOf(filler1, 32);
	public FixedLengthStringData qlfy06 = DD.qlfy.copy().isAPartOf(filler1, 40);
	public FixedLengthStringData qlfy07 = DD.qlfy.copy().isAPartOf(filler1, 48);
	public FixedLengthStringData qlfy08 = DD.qlfy.copy().isAPartOf(filler1, 56);
	public FixedLengthStringData qlfy09 = DD.qlfy.copy().isAPartOf(filler1, 64);
	public FixedLengthStringData qlfy10 = DD.qlfy.copy().isAPartOf(filler1, 72);
	public FixedLengthStringData qlfydescs = new FixedLengthStringData(300).isAPartOf(dataFields, 140);
	public FixedLengthStringData[] qlfydesc = FLSArrayPartOfStructure(10, 30, qlfydescs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(300).isAPartOf(qlfydescs, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfydesc01 = DD.qlfydesc.copy().isAPartOf(filler, 0);
	public FixedLengthStringData qlfydesc02 = DD.qlfydesc.copy().isAPartOf(filler, 30);
	public FixedLengthStringData qlfydesc03 = DD.qlfydesc.copy().isAPartOf(filler, 60);
	public FixedLengthStringData qlfydesc04 = DD.qlfydesc.copy().isAPartOf(filler, 90);
	public FixedLengthStringData qlfydesc05 = DD.qlfydesc.copy().isAPartOf(filler, 120);
	public FixedLengthStringData qlfydesc06 = DD.qlfydesc.copy().isAPartOf(filler, 150);
	public FixedLengthStringData qlfydesc07 = DD.qlfydesc.copy().isAPartOf(filler, 180);
	public FixedLengthStringData qlfydesc08 = DD.qlfydesc.copy().isAPartOf(filler, 210);
	public FixedLengthStringData qlfydesc09 = DD.qlfydesc.copy().isAPartOf(filler, 240);
	public FixedLengthStringData qlfydesc10 = DD.qlfydesc.copy().isAPartOf(filler, 270);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 440);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData qlfysErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] qlfyErr = FLSArrayPartOfStructure(10, 4, qlfysErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(qlfysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfy01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData qlfy02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData qlfy03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData qlfy04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData qlfy05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData qlfy06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData qlfy07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData qlfy08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData qlfy09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData qlfy10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData qlfydescsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] qlfydescErr = FLSArrayPartOfStructure(10, 4, qlfydescsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(qlfydescsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData qlfydesc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData qlfydesc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData qlfydesc03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData qlfydesc04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData qlfydesc05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData qlfydesc06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData qlfydesc07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData qlfydesc08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData qlfydesc09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData qlfydesc10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 544);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData qlfysOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] qlfyOut = FLSArrayPartOfStructure(10, 12, qlfysOut, 0);
	public FixedLengthStringData[][] qlfyO = FLSDArrayPartOfArrayStructure(12, 1, qlfyOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(120).isAPartOf(qlfysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] qlfy01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] qlfy02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] qlfy03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] qlfy04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] qlfy05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] qlfy06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] qlfy07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] qlfy08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] qlfy09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] qlfy10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData qlfydescsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] qlfydescOut = FLSArrayPartOfStructure(10, 12, qlfydescsOut, 0);
	public FixedLengthStringData[][] qlfydescO = FLSDArrayPartOfArrayStructure(12, 1, qlfydescOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(qlfydescsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] qlfydesc01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] qlfydesc02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] qlfydesc03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] qlfydesc04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] qlfydesc05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] qlfydesc06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] qlfydesc07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] qlfydesc08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] qlfydesc09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] qlfydesc10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sjl02screenWritten = new LongData(0);
	public LongData Sjl02protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl02ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(qlfy01Out,
				new String[] { "05", "25", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy02Out,
				new String[] { "06", "26", "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy03Out,
				new String[] { "07", "27", "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy04Out,
				new String[] { "08", "28", "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy05Out,
				new String[] { "09", "29", "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy06Out,
				new String[] { "10", "30", "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy07Out,
				new String[] { "11", "31", "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy08Out,
				new String[] { "12", "32", "-12", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy09Out,
				new String[] { "13", "33", "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(qlfy10Out,
				new String[] { "14", "34", "-14", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, itmfrm, itmto, qlfy01, qlfy02, qlfy03, qlfy04,
				qlfy05, qlfy06, qlfy07, qlfy08, qlfy09, qlfy10, qlfydesc01, qlfydesc02, qlfydesc03, qlfydesc04,
				qlfydesc05, qlfydesc06, qlfydesc07, qlfydesc08, qlfydesc09, qlfydesc10 };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, qlfy01Out,
				qlfy02Out, qlfy03Out, qlfy04Out, qlfy05Out, qlfy06Out, qlfy07Out, qlfy08Out, qlfy09Out, qlfy10Out,
				qlfydesc01Out, qlfydesc02Out, qlfydesc03Out, qlfydesc04Out, qlfydesc05Out, qlfydesc06Out, qlfydesc07Out,
				qlfydesc08Out, qlfydesc09Out, qlfydesc10Out };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, qlfy01Err,
				qlfy02Err, qlfy03Err, qlfy04Err, qlfy05Err, qlfy06Err, qlfy07Err, qlfy08Err, qlfy09Err, qlfy10Err,
				qlfydesc01Err, qlfydesc02Err, qlfydesc03Err, qlfydesc04Err, qlfydesc05Err, qlfydesc06Err, qlfydesc07Err,
				qlfydesc08Err, qlfydesc09Err, qlfydesc10Err };
		screenDateFields = new BaseData[] { itmfrm, itmto };
		screenDateErrFields = new BaseData[] { itmfrmErr, itmtoErr };
		screenDateDispFields = new BaseData[] { itmfrmDisp, itmtoDisp };

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl02screen.class;
		protectRecord = Sjl02protect.class;
	}
}