/*
 * File: Pm506.java
 * Date: 30 August 2009 1:12:59
 * Author: Quipoz Limited
 * 
 * Class transformed from PM506.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.screens.Sm506ScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This submenu program provides functions for agency movement:
*
*    -  Promote
*    -  Demote
*    -  Transfer
*    -  Inquire promotion history
*
* Agent number input is mandatory and client window is provided
*
*****************************************************************
* </pre>
*/
public class Pm506 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM506");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e186 = "E186";
	private static final String f910 = "F910";
	private static final String e994 = "E994";
	private static final String e305 = "E305";
	private static final String g772 = "G772";
		/* FORMATS */
	private static final String aglfrec = "AGLFREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Sftlockrec sftlockrec1 = new Sftlockrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Sm506ScreenVars sv = ScreenProgram.getScreenVars( Sm506ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		exit12090, 
		exit3090
	}

	public Pm506() {
		super();
		screenVars = sv;
		new ScreenModel("Sm506", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'SM506IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SM506-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.agntsel, SPACES)) {
			sv.agntselErr.set(e186);
			wsspcomn.edterror.set(SPACES);
		}
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agntsel);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), varcom.mrnf)) {
			sv.agntselErr.set(e305);
			wsspcomn.edterror.set(SPACES);
		}
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		validateKey12210();
	}

protected void validateKey12210()
	{
		/*    Validate fields*/
		if (isEQ(sv.action, "A")
		|| isEQ(sv.action, "B")
		|| isEQ(sv.action, "C")) {
			a100CheckAgentDateend();
		}
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.agnt);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.agntsel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isNE(sdasancrec.statuz, varcom.oK)) {
			sv.agntselErr.set(sdasancrec.statuz);
		}
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateWssp3010();
			keepsAgntsel3070();
			batching3080();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isEQ(sv.action, "A")) {
			wsspcomn.flag.set("P");
		}
		if (isEQ(sv.action, "B")) {
			wsspcomn.flag.set("D");
		}
		if (isEQ(sv.action, "C")) {
			/*       MOVE 'F'                 TO WSSP-FLAG.                    */
			wsspcomn.flag.set("M");
		}
		if (isEQ(sv.action, "D")) {
			wsspcomn.flag.set("I");
		}
	}

protected void keepsAgntsel3070()
	{
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agntsel);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
		/*    Soft lock contract.*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		sftlockrec1.function.set("LOCK");
		sftlockrec1.company.set(wsspcomn.company);
		sftlockrec1.enttyp.set("CH");
		sftlockrec1.entity.set(sv.agntsel);
		sftlockrec1.transaction.set(subprogrec.transcd);
		sftlockrec1.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec1.sftlockRec);
		if (isNE(sftlockrec1.statuz, varcom.oK)
		&& isNE(sftlockrec1.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec1.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec1.statuz, "LOCK")) {
			sv.agntselErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

protected void a100CheckAgentDateend()
	{
		a100CheckAgent();
	}

protected void a100CheckAgent()
	{
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agntsel);
		aglfIO.setFunction("READR");
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)
		&& isNE(aglfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), varcom.mrnf)) {
			sv.agntselErr.set(g772);
			return ;
		}
		else {
			if (isNE(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
				sv.agntselErr.set(e994);
			}
		}
	}
}
