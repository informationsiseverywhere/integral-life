package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5036screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {8, 22, 17, 4, 23, 18, 5, 24, 15, 6, 16, 7, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5036ScreenVars sv = (S5036ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5036screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5036ScreenVars screenVars = (S5036ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.cltname.setClassString("");
		screenVars.agnum.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.agtydesc.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.bmaflg.setClassString("");
		screenVars.intcrd.setClassString("");
		screenVars.fixprc.setClassString("");
		screenVars.taxmeth.setClassString("");
		screenVars.taxcde.setClassString("");
		screenVars.taxalw.setClassString("");
		screenVars.irdno.setClassString("");
		screenVars.sprschm.setClassString("");
		screenVars.sprprc.setClassString("");
		screenVars.agccqind.setClassString("");
		screenVars.houseLoan.setClassString("");
		screenVars.computerLoan.setClassString("");
		screenVars.carLoan.setClassString("");
		screenVars.officeRent.setClassString("");
		screenVars.otherLoans.setClassString("");
		screenVars.tcolprct.setClassString("");
		screenVars.tcolmax.setClassString("");
		screenVars.tcolbal.setClassString("");
	}

/**
 * Clear all the variables in S5036screen
 */
	public static void clear(VarModel pv) {
		S5036ScreenVars screenVars = (S5036ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.cltname.clear();
		screenVars.agnum.clear();
		screenVars.agtype.clear();
		screenVars.agtydesc.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.bmaflg.clear();
		screenVars.intcrd.clear();
		screenVars.fixprc.clear();
		screenVars.taxmeth.clear();
		screenVars.taxcde.clear();
		screenVars.taxalw.clear();
		screenVars.irdno.clear();
		screenVars.sprschm.clear();
		screenVars.sprprc.clear();
		screenVars.agccqind.clear();
		screenVars.houseLoan.clear();
		screenVars.computerLoan.clear();
		screenVars.carLoan.clear();
		screenVars.officeRent.clear();
		screenVars.otherLoans.clear();
		screenVars.tcolprct.clear();
		screenVars.tcolmax.clear();
		screenVars.tcolbal.clear();
	}
}
