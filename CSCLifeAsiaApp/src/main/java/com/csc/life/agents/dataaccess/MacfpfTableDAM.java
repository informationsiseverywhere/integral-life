package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MacfpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:46
 * Class transformed from MACFPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MacfpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 152;
	public FixedLengthStringData macfrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData macfpfRecord = macfrec;
	
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(macfrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(macfrec);
	public FixedLengthStringData agmvty = DD.agmvty.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlprvsup01 = DD.mlprvsup.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlprvsup02 = DD.mlprvsup.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlprvsup03 = DD.mlprvsup.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlprvsup04 = DD.mlprvsup.copy().isAPartOf(macfrec);
	public FixedLengthStringData zrptga = DD.zrptga.copy().isAPartOf(macfrec);
	public FixedLengthStringData zrptgb = DD.zrptgb.copy().isAPartOf(macfrec);
	public FixedLengthStringData zrptgc = DD.zrptgc.copy().isAPartOf(macfrec);
	public FixedLengthStringData zrptgd = DD.zrptgd.copy().isAPartOf(macfrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlagttyp = DD.mlagttyp.copy().isAPartOf(macfrec);
	public FixedLengthStringData mlparagt = DD.mlparagt.copy().isAPartOf(macfrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(macfrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(macfrec);
	public PackedDecimalData mlparorc = DD.mlparorc.copy().isAPartOf(macfrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(macfrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(macfrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(macfrec);
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(macfrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public MacfpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for MacfpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public MacfpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for MacfpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public MacfpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for MacfpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public MacfpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("MACFPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTNUM, " +
							"EFFDATE, " +
							"AGMVTY, " +
							"MLPRVSUP01, " +
							"MLPRVSUP02, " +
							"MLPRVSUP03, " +
							"MLPRVSUP04, " +
							"ZRPTGA, " +
							"ZRPTGB, " +
							"ZRPTGC, " +
							"ZRPTGD, " +
							"TRANNO, " +
							"MLAGTTYP, " +
							"MLPARAGT, " +
							"CURRFROM, " +
							"CURRTO, " +
							"MLPARORC, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"AGNTCOY, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntnum,
                                     effdate,
                                     agmvty,
                                     mlprvsup01,
                                     mlprvsup02,
                                     mlprvsup03,
                                     mlprvsup04,
                                     zrptga,
                                     zrptgb,
                                     zrptgc,
                                     zrptgd,
                                     tranno,
                                     mlagttyp,
                                     mlparagt,
                                     currfrom,
                                     currto,
                                     mlparorc,
                                     userProfile,
                                     jobName,
                                     datime,
                                     agntcoy,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntnum.clear();
  		effdate.clear();
  		agmvty.clear();
  		mlprvsup01.clear();
  		mlprvsup02.clear();
  		mlprvsup03.clear();
  		mlprvsup04.clear();
  		zrptga.clear();
  		zrptgb.clear();
  		zrptgc.clear();
  		zrptgd.clear();
  		tranno.clear();
  		mlagttyp.clear();
  		mlparagt.clear();
  		currfrom.clear();
  		currto.clear();
  		mlparorc.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		agntcoy.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getMacfrec() {
  		return macfrec;
	}

	public FixedLengthStringData getMacfpfRecord() {
  		return macfpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setMacfrec(what);
	}

	public void setMacfrec(Object what) {
  		this.macfrec.set(what);
	}

	public void setMacfpfRecord(Object what) {
  		this.macfpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(macfrec.getLength());
		result.set(macfrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}