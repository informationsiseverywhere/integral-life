package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl58protect;
import com.csc.life.agents.screens.Sjl58screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl58ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(816);
	public FixedLengthStringData dataFields = new FixedLengthStringData(432).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,198);
	public FixedLengthStringData uplevelno = DD.salebranch.copy().isAPartOf(dataFields, 228);
	public FixedLengthStringData upleveldes = DD.upleveldes.copy().isAPartOf(dataFields, 236);
	public FixedLengthStringData status = DD.salestatus.copy().isAPartOf(dataFields, 266);
	public ZonedDecimalData regdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,276);
	public FixedLengthStringData reasonreg = DD.reasoncd.copy().isAPartOf(dataFields,284);
	public FixedLengthStringData resndetl = DD.resndetl.copy().isAPartOf(dataFields,288);
	public ZonedDecimalData moddate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,408);
	public ZonedDecimalData deldate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,416);
	public FixedLengthStringData reasonmod = DD.reasoncd.copy().isAPartOf(dataFields,424);
	public FixedLengthStringData reasondel = DD.reasoncd.copy().isAPartOf(dataFields,428);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 432);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData uplevelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData upleveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData statusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData regdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData reasonregErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData resndetlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData moddateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData deldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData reasonmodErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData reasondelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 528); 
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] uplevelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] upleveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] statusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] regdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] reasonregOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] resndetlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] moddateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] deldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] reasonmodOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] reasondelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData regdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData moddateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData deldateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData scrndesc = new FixedLengthStringData(41);

	public LongData sjl58screenWritten = new LongData(0);
	public LongData sjl58protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl58ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntselOut,
				new String[] { "01", "39", "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "03", "40", "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "04", null, "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "05", "41", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "06", null, "-06", null, null, null, null, null, null, null, null, null });
		
		fieldIndMap.put(aradescOut,
				new String[] { "07", null, "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "08", null, "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypeOut,
				new String[] { "09", "42" , "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldesOut,
				new String[] { "10", null, "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "11", "43", "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "12", null, "-12", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "13", "44", "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtydescOut,
				new String[] { "14", null, "-14", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(uplevelnoOut,
				new String[] { "15", "45", "-15", "46", null, null, null, null, null, null, null, null });
		fieldIndMap.put(upleveldesOut,
				new String[] { "16", null, "-16", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(statusOut,
				new String[] { "17", null, "-17", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(regdateOut,
				new String[] { "18", "26", "-18", "27", null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasonregOut,
				new String[] { "19", "28", "-19", "29", null, null, null, null, null, null, null, null });
		fieldIndMap.put(resndetlOut,
				new String[] { "21", null, "-21", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(moddateOut,
				new String[] { "22", "30", "-22", "31", null, null, null, null, null, null, null, null });
		fieldIndMap.put(deldateOut,
				new String[] { "23", "32", "-23", "33", null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasonmodOut,
				new String[] { "24", "34", "-24", "35", null, null, null, null, null, null, null, null });
		fieldIndMap.put(reasondelOut,
				new String[] { "25", "36", "-25", "37", null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "38", null , "-38", null , null, null, null, null, null, null, null, null });
		
		
		screenFields = new BaseData[] { clntsel, cltname, company, brnchcd, brnchdesc, aracde, aradesc, levelno, leveltype, leveldes, saledept, saledptdes, agtype,
										agtydesc, uplevelno, upleveldes, status, regdate, reasonreg, resndetl, moddate, deldate, reasonmod, reasondel };
		screenOutFields = new BaseData[][] { clntselOut, cltnameOut, companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, levelnoOut, leveltypeOut, leveldesOut, saledeptOut, saledptdesOut,
										agtypeOut, agtydescOut, uplevelnoOut, upleveldesOut, statusOut, regdateOut, reasonregOut, resndetlOut, moddateOut, deldateOut, reasonmodOut, reasondelOut };
		screenErrFields = new BaseData[] { clntselErr, cltnameErr, companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, levelnoErr, leveltypeErr, leveldesErr, saledeptErr, saledptdesErr,
										agtypeErr, agtydescErr, uplevelnoErr, upleveldesErr, statusErr, regdateErr, reasonregErr, resndetlErr, moddateErr, deldateErr, reasonmodErr, reasondelErr };
		screenDateFields = new BaseData[] {regdate, moddate, deldate};
		screenDateErrFields = new BaseData[] {regdateErr, moddateErr, deldateErr};
		screenDateDispFields = new BaseData[] {regdateDisp, moddateDisp, deldateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl58screen.class;
		protectRecord = Sjl58protect.class;
	}
}
