package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZrgrpfDAO;
import com.csc.life.agents.dataaccess.model.Zrgrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZrgrpfDAOImpl extends BaseDAOImpl<Zrgrpf> implements ZrgrpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZrgrpfDAOImpl.class);
	
	/**
	 * This method will insert record in Zrgrpf type table.
	 * 
	 */
	public int insertZrgrData(Zrgrpf zrgrData)
	{		
		PreparedStatement ps = null;
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ZRGRPF");
		sql.append("(ACCTAMT,AGNTCOY,AGNTNUM,ZRECRUIT,AGTYPE,BATCACTMN,BATCACTYR,BONUSAMT,CHDRNUM,COVERAGE,DATIME,EFFDATE,JOBNM,JOBNO,LIFE,ORIGCURR,PRCDATE,RIDER,TRANNO,USRPRF,COMIND)");
		sql.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		int index = 1;
		int count = 0; 
		try {

			ps = getConnection().prepareStatement(sql.toString());
			ps.setBigDecimal(index++,zrgrData.getAcctamt());
			ps.setString(index++,zrgrData.getAgntcoy());
			ps.setString(index++,zrgrData.getAgntnum());
			ps.setString(index++,zrgrData.getZrecruit());
			ps.setString(index++,zrgrData.getAgtype());
			ps.setBigDecimal(index++,zrgrData.getBatcactmn());
			ps.setBigDecimal(index++,zrgrData.getBatcactyr());
			ps.setBigDecimal(index++,zrgrData.getBonusamt());
			ps.setString(index++,zrgrData.getChdrnum());
			ps.setString(index++,zrgrData.getCoverage());
			ps.setTimestamp(index++,new Timestamp(System.currentTimeMillis()));
			ps.setBigDecimal(index++,zrgrData.getEffdate());
			ps.setString(index++,zrgrData.getJobName());
			ps.setBigDecimal(index++,zrgrData.getJobno());
			ps.setString(index++,zrgrData.getLife());
			ps.setString(index++,zrgrData.getOrigcurr());
			ps.setBigDecimal(index++,zrgrData.getPrcdate());
			ps.setString(index++,zrgrData.getRider());			
			ps.setBigDecimal(index++,zrgrData.getTranno());		
			ps.setString(index++,zrgrData.getUserProfile());
			ps.setString(index++, zrgrData.getComind());
			ps.executeUpdate();			 			
			} catch (SQLException e) {
			LOGGER.error("InsertzrgrData()", e);
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(ps,null);
		}	
		
		return count;
	}		
}