package com.csc.life.agents.dataaccess;

import com.csc.fsu.general.dataaccess.AcmvpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AcmvagtTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:14
 * Class transformed from ACMVAGT.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AcmvagtTableDAM extends AcmvpfTableDAM {

	public AcmvagtTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ACMVAGT");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BATCCOY"
		             + ", BATCBRN"
		             + ", BATCACTYR"
		             + ", BATCACTMN"
		             + ", BATCTRCDE"
		             + ", BATCBATCH"
		             + ", RLDGACCT"
		             + ", ORIGCURR"
		             + ", SACSCODE"
		             + ", SACSTYP"
		             + ", TRANNO"
		             + ", RDOCNUM"
		             + ", JRNSEQ";
		
		QUALIFIEDCOLUMNS = 
		            "RLDGCOY, " +
		            "SACSCODE, " +
		            "RLDGACCT, " +
		            "ORIGCURR, " +
		            "SACSTYP, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "RDOCNUM, " +
		            "TRANNO, " +
		            "JRNSEQ, " +
		            "ORIGAMT, " +
		            "TRANREF, " +
		            "TRANDESC, " +
		            "CRATE, " +
		            "ACCTAMT, " +
		            "GENLCOY, " +
		            "GENLCUR, " +
		            "GLCODE, " +
		            "GLSIGN, " +
		            "POSTYEAR, " +
		            "POSTMONTH, " +
		            "EFFDATE, " +
		            "RCAMT, " +
		            "FRCDATE, " +
		            "SUPRFLG, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "TERMID, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BATCCOY ASC, " +
		            "BATCBRN ASC, " +
		            "BATCACTYR ASC, " +
		            "BATCACTMN ASC, " +
		            "BATCTRCDE ASC, " +
		            "BATCBATCH ASC, " +
		            "RLDGACCT ASC, " +
		            "ORIGCURR ASC, " +
		            "SACSCODE ASC, " +
		            "SACSTYP ASC, " +
		            "TRANNO ASC, " +
		            "RDOCNUM ASC, " +
		            "JRNSEQ ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "BATCCOY DESC, " +
		            "BATCBRN DESC, " +
		            "BATCACTYR DESC, " +
		            "BATCACTMN DESC, " +
		            "BATCTRCDE DESC, " +
		            "BATCBATCH DESC, " +
		            "RLDGACCT DESC, " +
		            "ORIGCURR DESC, " +
		            "SACSCODE DESC, " +
		            "SACSTYP DESC, " +
		            "TRANNO DESC, " +
		            "RDOCNUM DESC, " +
		            "JRNSEQ DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               rldgcoy,
                               sacscode,
                               rldgacct,
                               origcurr,
                               sacstyp,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               rdocnum,
                               tranno,
                               jrnseq,
                               origamt,
                               tranref,
                               trandesc,
                               crate,
                               acctamt,
                               genlcoy,
                               genlcur,
                               glcode,
                               glsign,
                               postyear,
                               postmonth,
                               effdate,
                               rcamt,
                               frcdate,
                               suprflg,
                               transactionDate,
                               transactionTime,
                               user,
                               termid,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(10);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getRldgacct().toInternal()
					+ getOrigcurr().toInternal()
					+ getSacscode().toInternal()
					+ getSacstyp().toInternal()
					+ getTranno().toInternal()
					+ getRdocnum().toInternal()
					+ getJrnseq().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, rldgacct);
			what = ExternalData.chop(what, origcurr);
			what = ExternalData.chop(what, sacscode);
			what = ExternalData.chop(what, sacstyp);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, rdocnum);
			what = ExternalData.chop(what, jrnseq);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(16);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(9);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(sacscode.toInternal());
	nonKeyFiller30.setInternal(rldgacct.toInternal());
	nonKeyFiller40.setInternal(origcurr.toInternal());
	nonKeyFiller50.setInternal(sacstyp.toInternal());
	nonKeyFiller60.setInternal(batccoy.toInternal());
	nonKeyFiller70.setInternal(batcbrn.toInternal());
	nonKeyFiller80.setInternal(batcactyr.toInternal());
	nonKeyFiller90.setInternal(batcactmn.toInternal());
	nonKeyFiller100.setInternal(batctrcde.toInternal());
	nonKeyFiller110.setInternal(batcbatch.toInternal());
	nonKeyFiller120.setInternal(rdocnum.toInternal());
	nonKeyFiller130.setInternal(tranno.toInternal());
	nonKeyFiller140.setInternal(jrnseq.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(250);
		
		nonKeyData.set(
					getRldgcoy().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ getOrigamt().toInternal()
					+ getTranref().toInternal()
					+ getTrandesc().toInternal()
					+ getCrate().toInternal()
					+ getAcctamt().toInternal()
					+ getGenlcoy().toInternal()
					+ getGenlcur().toInternal()
					+ getGlcode().toInternal()
					+ getGlsign().toInternal()
					+ getPostyear().toInternal()
					+ getPostmonth().toInternal()
					+ getEffdate().toInternal()
					+ getRcamt().toInternal()
					+ getFrcdate().toInternal()
					+ getSuprflg().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getTermid().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, rldgcoy);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, origamt);
			what = ExternalData.chop(what, tranref);
			what = ExternalData.chop(what, trandesc);
			what = ExternalData.chop(what, crate);
			what = ExternalData.chop(what, acctamt);
			what = ExternalData.chop(what, genlcoy);
			what = ExternalData.chop(what, genlcur);
			what = ExternalData.chop(what, glcode);
			what = ExternalData.chop(what, glsign);
			what = ExternalData.chop(what, postyear);
			what = ExternalData.chop(what, postmonth);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, rcamt);
			what = ExternalData.chop(what, frcdate);
			what = ExternalData.chop(what, suprflg);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}
	public FixedLengthStringData getRldgacct() {
		return rldgacct;
	}
	public void setRldgacct(Object what) {
		rldgacct.set(what);
	}
	public FixedLengthStringData getOrigcurr() {
		return origcurr;
	}
	public void setOrigcurr(Object what) {
		origcurr.set(what);
	}
	public FixedLengthStringData getSacscode() {
		return sacscode;
	}
	public void setSacscode(Object what) {
		sacscode.set(what);
	}
	public FixedLengthStringData getSacstyp() {
		return sacstyp;
	}
	public void setSacstyp(Object what) {
		sacstyp.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	public FixedLengthStringData getRdocnum() {
		return rdocnum;
	}
	public void setRdocnum(Object what) {
		rdocnum.set(what);
	}
	public PackedDecimalData getJrnseq() {
		return jrnseq;
	}
	public void setJrnseq(Object what) {
		setJrnseq(what, false);
	}
	public void setJrnseq(Object what, boolean rounded) {
		if (rounded)
			jrnseq.setRounded(what);
		else
			jrnseq.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getRldgcoy() {
		return rldgcoy;
	}
	public void setRldgcoy(Object what) {
		rldgcoy.set(what);
	}	
	public PackedDecimalData getOrigamt() {
		return origamt;
	}
	public void setOrigamt(Object what) {
		setOrigamt(what, false);
	}
	public void setOrigamt(Object what, boolean rounded) {
		if (rounded)
			origamt.setRounded(what);
		else
			origamt.set(what);
	}	
	public FixedLengthStringData getTranref() {
		return tranref;
	}
	public void setTranref(Object what) {
		tranref.set(what);
	}	
	public FixedLengthStringData getTrandesc() {
		return trandesc;
	}
	public void setTrandesc(Object what) {
		trandesc.set(what);
	}	
	public PackedDecimalData getCrate() {
		return crate;
	}
	public void setCrate(Object what) {
		setCrate(what, false);
	}
	public void setCrate(Object what, boolean rounded) {
		if (rounded)
			crate.setRounded(what);
		else
			crate.set(what);
	}	
	public PackedDecimalData getAcctamt() {
		return acctamt;
	}
	public void setAcctamt(Object what) {
		setAcctamt(what, false);
	}
	public void setAcctamt(Object what, boolean rounded) {
		if (rounded)
			acctamt.setRounded(what);
		else
			acctamt.set(what);
	}	
	public FixedLengthStringData getGenlcoy() {
		return genlcoy;
	}
	public void setGenlcoy(Object what) {
		genlcoy.set(what);
	}	
	public FixedLengthStringData getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(Object what) {
		genlcur.set(what);
	}	
	public FixedLengthStringData getGlcode() {
		return glcode;
	}
	public void setGlcode(Object what) {
		glcode.set(what);
	}	
	public FixedLengthStringData getGlsign() {
		return glsign;
	}
	public void setGlsign(Object what) {
		glsign.set(what);
	}	
	public FixedLengthStringData getPostyear() {
		return postyear;
	}
	public void setPostyear(Object what) {
		postyear.set(what);
	}	
	public FixedLengthStringData getPostmonth() {
		return postmonth;
	}
	public void setPostmonth(Object what) {
		postmonth.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getRcamt() {
		return rcamt;
	}
	public void setRcamt(Object what) {
		setRcamt(what, false);
	}
	public void setRcamt(Object what, boolean rounded) {
		if (rounded)
			rcamt.setRounded(what);
		else
			rcamt.set(what);
	}	
	public PackedDecimalData getFrcdate() {
		return frcdate;
	}
	public void setFrcdate(Object what) {
		setFrcdate(what, false);
	}
	public void setFrcdate(Object what, boolean rounded) {
		if (rounded)
			frcdate.setRounded(what);
		else
			frcdate.set(what);
	}	
	public FixedLengthStringData getSuprflg() {
		return suprflg;
	}
	public void setSuprflg(Object what) {
		suprflg.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		rldgacct.clear();
		origcurr.clear();
		sacscode.clear();
		sacstyp.clear();
		tranno.clear();
		rdocnum.clear();
		jrnseq.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		rldgcoy.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		origamt.clear();
		tranref.clear();
		trandesc.clear();
		crate.clear();
		acctamt.clear();
		genlcoy.clear();
		genlcur.clear();
		glcode.clear();
		glsign.clear();
		postyear.clear();
		postmonth.clear();
		effdate.clear();
		rcamt.clear();
		frcdate.clear();
		suprflg.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		termid.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}