package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:57
 * Description:
 * Copybook name: AGBNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agbnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agbnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agbnKey = new FixedLengthStringData(64).isAPartOf(agbnFileKey, 0, REDEFINE);
  	public FixedLengthStringData agbnAgntcoy = new FixedLengthStringData(1).isAPartOf(agbnKey, 0);
  	public FixedLengthStringData agbnAgntnum = new FixedLengthStringData(8).isAPartOf(agbnKey, 1);
  	public FixedLengthStringData agbnContact = new FixedLengthStringData(8).isAPartOf(agbnKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(agbnKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agbnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agbnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}