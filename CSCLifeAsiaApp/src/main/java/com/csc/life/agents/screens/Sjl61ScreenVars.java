package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.agents.screens.Sjl61protect;
import com.csc.life.agents.screens.Sjl61screen;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;


public class Sjl61ScreenVars extends SmartVarModel {
	
	private static final long serialVersionUID = 1L; 
	public FixedLengthStringData dataArea = new FixedLengthStringData(519);
	public FixedLengthStringData dataFields = new FixedLengthStringData(231).isAPartOf(dataArea, 0);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,57);
	public FixedLengthStringData brnchcd = DD.agntbr.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData brnchdesc = DD.agbrdesc.copy().isAPartOf(dataFields,60);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData levelno = DD.salelevel.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData leveltype = DD.salelvltyp.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData leveldes = DD.saleveldes.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData saledept = DD.saledept.copy().isAPartOf(dataFields, 162);
	public FixedLengthStringData saledptdes = DD.saledptdes.copy().isAPartOf(dataFields, 166);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,196);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields, 198);
	public FixedLengthStringData stattype = DD.statustype.copy().isAPartOf(dataFields, 199);
	public FixedLengthStringData dstatus = DD.salestatus.copy().isAPartOf(dataFields, 200);
	public FixedLengthStringData userid = DD.usrname.copy().isAPartOf(dataFields, 210);
	public FixedLengthStringData stattypeapp = DD.statusapp.copy().isAPartOf(dataFields, 230);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 231);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData brnchcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData brnchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData levelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData leveltypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData leveldesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData saledeptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData saledptdesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData stattypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData dstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData stattypeappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 303); 
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] brnchcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] brnchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] levelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] leveltypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] leveldesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] saledeptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] saledptdesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] stattypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] dstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] stattypeappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(480);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(269).isAPartOf(subfileArea, 0);
	public FixedLengthStringData slt = DD.slt.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData aclntsel = DD.clntsel.copy().isAPartOf(subfileFields,1);
	public FixedLengthStringData acltname = DD.cltname.copy().isAPartOf(subfileFields,11); 
	public FixedLengthStringData agncynum = DD.agncynum.copy().isAPartOf(subfileFields,58);
	public FixedLengthStringData regnum = DD.regnum.copy().isAPartOf(subfileFields,66);
	public ZonedDecimalData startDate = DD.srdate.copyToZonedDecimal().isAPartOf(subfileFields,82);
	public ZonedDecimalData dateend = DD.enddate.copyToZonedDecimal().isAPartOf(subfileFields,90);
	public FixedLengthStringData validflag = DD.slt.copy().isAPartOf(subfileFields,98);
	public FixedLengthStringData status = DD.agbrdesc.copy().isAPartOf(subfileFields,99);
	//public FixedLengthStringData regclass = DD.brnch.copy().isAPartOf(subfileFields,109); 
	public FixedLengthStringData regclass = DD.agncynum.copy().isAPartOf(subfileFields,129); 
	public ZonedDecimalData regdate = DD.occdate.copyToZonedDecimal().isAPartOf(subfileFields,137);
	public FixedLengthStringData reasonmod = DD.reasoncd.copy().isAPartOf(subfileFields,145);
	public FixedLengthStringData resndesc = DD.resndetl.copy().isAPartOf(subfileFields,149);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(52).isAPartOf(subfileArea,269);
	public FixedLengthStringData sltErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData aclntselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData acltnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData agncynumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData regnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData startDateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData dateendErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData validflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData statusErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData regclassErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData regdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData reasonmodErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(156).isAPartOf(subfileArea, 321);
	public FixedLengthStringData[] sltOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] aclntselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] acltnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] agncynumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] regnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] startDateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] dateendOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] validflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] statusOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] regclassOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] regdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] reasonmodOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 477);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();
	
	public FixedLengthStringData dateendDisp = new FixedLengthStringData(10);
	public FixedLengthStringData startDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData regdateDisp = new FixedLengthStringData(10);
	
	
	public FixedLengthStringData action1 = new FixedLengthStringData(1); 
	public FixedLengthStringData action2 = new FixedLengthStringData(8); 

	
	public LongData Sjl61screensflWritten = new LongData(0);
	public LongData Sjl61screenctlWritten = new LongData(0);
	public LongData Sjl61screenWritten = new LongData(0);
	public LongData Sjl61protectWritten = new LongData(0);
	public GeneralTable Sjl61screensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sjl61windowWritten = new LongData(0);
	public LongData Sjl61hideWritten = new LongData(0);
	
	public boolean hasSubfile() {
		return false;
	}

	public Sjl61ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected void initialiseScreenVars() {
		
		fieldIndMap.put(clntselOut,
				new String[] { "01", "02" , "-01", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(cltnameOut,
				new String[] { "03", "04" , "-03", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(companyOut,
				new String[] { "05", "06" , "-05", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchcdOut,
				new String[] { "07", "08" , "-07", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(brnchdescOut,
				new String[] { "09", "10" , "-09", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aracdeOut,
				new String[] { "11", "12" , "-11", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(aradescOut,
				new String[] { "13", "14" , "-13", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(levelnoOut,
				new String[] { "15", "16" , "-15", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveltypeOut,
				new String[] { "17", "18" , "-17", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(leveldesOut,
				new String[] { "19", "20" , "-19", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledeptOut,
				new String[] { "21", "22" , "-21", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(saledptdesOut,
				new String[] { "23", "24" , "-23", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(agtypeOut,
				new String[] { "25", "26" , "-25", null , null, null, null, null, null, null, null, null });
		fieldIndMap.put(sltOut,
				new String[] { "27", "28" , "-27", null , null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,
				new String[] { "29", "30" , "-29", null , null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] { clntsel, cltname, company, brnchcd, brnchdesc, aracde, aradesc, levelno, leveltype, leveldes, saledept, saledptdes, agtype,action,stattype, dstatus,userid,stattypeapp};
		screenOutFields = new BaseData[][] { clntselOut, cltnameOut, companyOut, brnchcdOut, brnchdescOut, aracdeOut, aradescOut, levelnoOut, leveltypeOut, leveldesOut, 
											 saledeptOut, saledptdesOut,agtypeOut,actionOut,stattypeOut, dstatusOut, useridOut,stattypeappOut};
		screenErrFields = new BaseData[] { clntselErr, cltnameErr, companyErr, brnchcdErr, brnchdescErr, aracdeErr, aradescErr, levelnoErr, leveltypeErr, leveldesErr, 
											 saledeptErr, saledptdesErr,agtypeErr,actionErr,stattypeErr, dstatusErr, useridErr,stattypeappErr};
						

		screenSflFields = new BaseData[] {slt, aclntsel, acltname, agncynum, regnum, startDate, dateend, validflag,status, regclass, regdate,reasonmod,resndesc};
		screenSflOutFields = new BaseData[][] {sltOut, aclntselOut, acltnameOut, agncynumOut, regnumOut, startDateOut, dateendOut, validflagOut,statusOut, regclassOut, regdateOut,reasonmodOut,resndescOut };
		screenSflErrFields = new BaseData[] {sltErr, aclntselErr, acltnameErr, agncynumErr, regnumErr, startDateErr, dateendErr, validflagErr,statusErr, regclassErr, regdateErr,reasonmodErr,resndescErr };
		
		screenDateFields = new BaseData[] {};
		screenSflDateFields = new BaseData[] {startDate, dateend,regdate};
		screenSflDateErrFields = new BaseData[] {startDateErr, dateendErr, regdateErr};
		screenSflDateDispFields = new BaseData[] {startDateDisp, dateendDisp,regdateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorSflInds = errorSubfile;
		screenSflRecord = Sjl61screensfl.class;
		screenCtlRecord = Sjl61screenctl.class;
		initialiseSubfileArea();
		screenRecord = Sjl61screen.class;
		protectRecord = Sjl61protect.class;
		}
		
		public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sjl61screenctl.lrec.pageSubfile);
		}
}
