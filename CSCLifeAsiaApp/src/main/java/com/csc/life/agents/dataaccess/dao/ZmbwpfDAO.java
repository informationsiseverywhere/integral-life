package com.csc.life.agents.dataaccess.dao;

import com.csc.life.agents.dataaccess.model.Zmbwpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZmbwpfDAO extends BaseDAO<Zmbwpf> {
	
	public Zmbwpf getZmbwpf(String agntcoy,String agntnum);
}
