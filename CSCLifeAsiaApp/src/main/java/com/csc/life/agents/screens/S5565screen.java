package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5565screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 78}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5565ScreenVars sv = (S5565ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5565screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5565ScreenVars screenVars = (S5565ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.termMax01.setClassString("");
		screenVars.termMax02.setClassString("");
		screenVars.termMax03.setClassString("");
		screenVars.termMax04.setClassString("");
		screenVars.termMax05.setClassString("");
		screenVars.termMax06.setClassString("");
		screenVars.termMax07.setClassString("");
		screenVars.termMax08.setClassString("");
		screenVars.termMax09.setClassString("");
		screenVars.anbAtCcd01.setClassString("");
		screenVars.prcnt01.setClassString("");
		screenVars.prcnt02.setClassString("");
		screenVars.prcnt03.setClassString("");
		screenVars.prcnt04.setClassString("");
		screenVars.prcnt05.setClassString("");
		screenVars.prcnt06.setClassString("");
		screenVars.prcnt07.setClassString("");
		screenVars.prcnt08.setClassString("");
		screenVars.prcnt09.setClassString("");
		screenVars.anbAtCcd02.setClassString("");
		screenVars.prcnt10.setClassString("");
		screenVars.prcnt11.setClassString("");
		screenVars.prcnt12.setClassString("");
		screenVars.prcnt13.setClassString("");
		screenVars.prcnt14.setClassString("");
		screenVars.prcnt15.setClassString("");
		screenVars.prcnt16.setClassString("");
		screenVars.prcnt17.setClassString("");
		screenVars.prcnt18.setClassString("");
		screenVars.anbAtCcd03.setClassString("");
		screenVars.prcnt19.setClassString("");
		screenVars.prcnt20.setClassString("");
		screenVars.prcnt21.setClassString("");
		screenVars.prcnt22.setClassString("");
		screenVars.prcnt23.setClassString("");
		screenVars.prcnt24.setClassString("");
		screenVars.prcnt25.setClassString("");
		screenVars.prcnt26.setClassString("");
		screenVars.prcnt27.setClassString("");
		screenVars.anbAtCcd04.setClassString("");
		screenVars.prcnt28.setClassString("");
		screenVars.prcnt29.setClassString("");
		screenVars.prcnt30.setClassString("");
		screenVars.prcnt31.setClassString("");
		screenVars.prcnt32.setClassString("");
		screenVars.prcnt33.setClassString("");
		screenVars.prcnt34.setClassString("");
		screenVars.prcnt35.setClassString("");
		screenVars.prcnt36.setClassString("");
		screenVars.anbAtCcd05.setClassString("");
		screenVars.prcnt37.setClassString("");
		screenVars.prcnt38.setClassString("");
		screenVars.prcnt39.setClassString("");
		screenVars.prcnt40.setClassString("");
		screenVars.prcnt41.setClassString("");
		screenVars.prcnt42.setClassString("");
		screenVars.prcnt43.setClassString("");
		screenVars.prcnt44.setClassString("");
		screenVars.prcnt45.setClassString("");
		screenVars.anbAtCcd06.setClassString("");
		screenVars.prcnt46.setClassString("");
		screenVars.prcnt47.setClassString("");
		screenVars.prcnt48.setClassString("");
		screenVars.prcnt49.setClassString("");
		screenVars.prcnt50.setClassString("");
		screenVars.prcnt51.setClassString("");
		screenVars.prcnt52.setClassString("");
		screenVars.prcnt53.setClassString("");
		screenVars.prcnt54.setClassString("");
		screenVars.anbAtCcd07.setClassString("");
		screenVars.prcnt55.setClassString("");
		screenVars.prcnt56.setClassString("");
		screenVars.prcnt57.setClassString("");
		screenVars.prcnt58.setClassString("");
		screenVars.prcnt59.setClassString("");
		screenVars.prcnt60.setClassString("");
		screenVars.prcnt61.setClassString("");
		screenVars.prcnt62.setClassString("");
		screenVars.prcnt63.setClassString("");
		screenVars.anbAtCcd08.setClassString("");
		screenVars.prcnt64.setClassString("");
		screenVars.prcnt65.setClassString("");
		screenVars.prcnt66.setClassString("");
		screenVars.prcnt67.setClassString("");
		screenVars.prcnt68.setClassString("");
		screenVars.prcnt69.setClassString("");
		screenVars.prcnt70.setClassString("");
		screenVars.prcnt71.setClassString("");
		screenVars.prcnt72.setClassString("");
		screenVars.anbAtCcd09.setClassString("");
		screenVars.prcnt73.setClassString("");
		screenVars.prcnt74.setClassString("");
		screenVars.prcnt75.setClassString("");
		screenVars.prcnt76.setClassString("");
		screenVars.prcnt77.setClassString("");
		screenVars.prcnt78.setClassString("");
		screenVars.prcnt79.setClassString("");
		screenVars.prcnt80.setClassString("");
		screenVars.prcnt81.setClassString("");
		screenVars.anbAtCcd10.setClassString("");
		screenVars.prcnt82.setClassString("");
		screenVars.prcnt83.setClassString("");
		screenVars.prcnt84.setClassString("");
		screenVars.prcnt85.setClassString("");
		screenVars.prcnt86.setClassString("");
		screenVars.prcnt87.setClassString("");
		screenVars.prcnt88.setClassString("");
		screenVars.prcnt89.setClassString("");
		screenVars.prcnt90.setClassString("");
	}

/**
 * Clear all the variables in S5565screen
 */
	public static void clear(VarModel pv) {
		S5565ScreenVars screenVars = (S5565ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.termMax01.clear();
		screenVars.termMax02.clear();
		screenVars.termMax03.clear();
		screenVars.termMax04.clear();
		screenVars.termMax05.clear();
		screenVars.termMax06.clear();
		screenVars.termMax07.clear();
		screenVars.termMax08.clear();
		screenVars.termMax09.clear();
		screenVars.anbAtCcd01.clear();
		screenVars.prcnt01.clear();
		screenVars.prcnt02.clear();
		screenVars.prcnt03.clear();
		screenVars.prcnt04.clear();
		screenVars.prcnt05.clear();
		screenVars.prcnt06.clear();
		screenVars.prcnt07.clear();
		screenVars.prcnt08.clear();
		screenVars.prcnt09.clear();
		screenVars.anbAtCcd02.clear();
		screenVars.prcnt10.clear();
		screenVars.prcnt11.clear();
		screenVars.prcnt12.clear();
		screenVars.prcnt13.clear();
		screenVars.prcnt14.clear();
		screenVars.prcnt15.clear();
		screenVars.prcnt16.clear();
		screenVars.prcnt17.clear();
		screenVars.prcnt18.clear();
		screenVars.anbAtCcd03.clear();
		screenVars.prcnt19.clear();
		screenVars.prcnt20.clear();
		screenVars.prcnt21.clear();
		screenVars.prcnt22.clear();
		screenVars.prcnt23.clear();
		screenVars.prcnt24.clear();
		screenVars.prcnt25.clear();
		screenVars.prcnt26.clear();
		screenVars.prcnt27.clear();
		screenVars.anbAtCcd04.clear();
		screenVars.prcnt28.clear();
		screenVars.prcnt29.clear();
		screenVars.prcnt30.clear();
		screenVars.prcnt31.clear();
		screenVars.prcnt32.clear();
		screenVars.prcnt33.clear();
		screenVars.prcnt34.clear();
		screenVars.prcnt35.clear();
		screenVars.prcnt36.clear();
		screenVars.anbAtCcd05.clear();
		screenVars.prcnt37.clear();
		screenVars.prcnt38.clear();
		screenVars.prcnt39.clear();
		screenVars.prcnt40.clear();
		screenVars.prcnt41.clear();
		screenVars.prcnt42.clear();
		screenVars.prcnt43.clear();
		screenVars.prcnt44.clear();
		screenVars.prcnt45.clear();
		screenVars.anbAtCcd06.clear();
		screenVars.prcnt46.clear();
		screenVars.prcnt47.clear();
		screenVars.prcnt48.clear();
		screenVars.prcnt49.clear();
		screenVars.prcnt50.clear();
		screenVars.prcnt51.clear();
		screenVars.prcnt52.clear();
		screenVars.prcnt53.clear();
		screenVars.prcnt54.clear();
		screenVars.anbAtCcd07.clear();
		screenVars.prcnt55.clear();
		screenVars.prcnt56.clear();
		screenVars.prcnt57.clear();
		screenVars.prcnt58.clear();
		screenVars.prcnt59.clear();
		screenVars.prcnt60.clear();
		screenVars.prcnt61.clear();
		screenVars.prcnt62.clear();
		screenVars.prcnt63.clear();
		screenVars.anbAtCcd08.clear();
		screenVars.prcnt64.clear();
		screenVars.prcnt65.clear();
		screenVars.prcnt66.clear();
		screenVars.prcnt67.clear();
		screenVars.prcnt68.clear();
		screenVars.prcnt69.clear();
		screenVars.prcnt70.clear();
		screenVars.prcnt71.clear();
		screenVars.prcnt72.clear();
		screenVars.anbAtCcd09.clear();
		screenVars.prcnt73.clear();
		screenVars.prcnt74.clear();
		screenVars.prcnt75.clear();
		screenVars.prcnt76.clear();
		screenVars.prcnt77.clear();
		screenVars.prcnt78.clear();
		screenVars.prcnt79.clear();
		screenVars.prcnt80.clear();
		screenVars.prcnt81.clear();
		screenVars.anbAtCcd10.clear();
		screenVars.prcnt82.clear();
		screenVars.prcnt83.clear();
		screenVars.prcnt84.clear();
		screenVars.prcnt85.clear();
		screenVars.prcnt86.clear();
		screenVars.prcnt87.clear();
		screenVars.prcnt88.clear();
		screenVars.prcnt89.clear();
		screenVars.prcnt90.clear();
	}
}
