package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.datatype.*;
import com.csc.smart400framework.SmartVarModel;
import com.csc.common.DD;

/**
 * Screen Variables for Sr58s
 * @version 1.0 Generated on Fri Jul 19 14:38:10 SGT 2013
 * @author CSC
 */
public class Sr58sScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(630);
	public FixedLengthStringData dataFields = new FixedLengthStringData(246).isAPartOf(dataArea, 0);
	//---------------data datafield contents
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0); 
	public FixedLengthStringData comtots = new FixedLengthStringData(136).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] comtot = FLSArrayPartOfStructure(8, 17, comtots, 0);
	public ZonedDecimalData comtot01 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,0); 
	public ZonedDecimalData comtot02 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,17); 
	public ZonedDecimalData comtot03 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,34); 
	public ZonedDecimalData comtot04 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,51); 
	public ZonedDecimalData comtot05 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,68); 
	public ZonedDecimalData comtot06 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,85);  
	public ZonedDecimalData comtot07 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,102); 
	public ZonedDecimalData comtot08 = DD.comtot.copyToZonedDecimal().isAPartOf(comtots,119); 
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,137); 
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,145); 
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,153); 
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,161); 
	public FixedLengthStringData prcents = new FixedLengthStringData(40).isAPartOf(dataFields, 191);
	public FixedLengthStringData[] prcent = FLSArrayPartOfStructure(8, 5, prcents, 0);
	public ZonedDecimalData prcent01 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,0); 
	public ZonedDecimalData prcent02 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,5); 
	public ZonedDecimalData prcent03 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,10); 
	public ZonedDecimalData prcent04 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,15); 
	public ZonedDecimalData prcent05 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,20); 
	public ZonedDecimalData prcent06 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,25); 
	public ZonedDecimalData prcent07 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,30); 
	public ZonedDecimalData prcent08 = DD.prcent.copyToZonedDecimal().isAPartOf(prcents,35); 
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,231); 
	public FixedLengthStringData znadjpercs = new FixedLengthStringData(10).isAPartOf(dataFields, 236);
	public FixedLengthStringData[] znadjperc = FLSArrayPartOfStructure(2, 5, znadjpercs, 0);
	public ZonedDecimalData znadjperc01 = DD.znadjperc.copyToZonedDecimal().isAPartOf(znadjpercs,0); 
	public ZonedDecimalData znadjperc02 = DD.znadjperc.copyToZonedDecimal().isAPartOf(znadjpercs,5); 


	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 246);

	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData comtotsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] comtotErr = FLSArrayPartOfStructure(8, 4, comtotsErr, 0);
	  public FixedLengthStringData comtot01Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,0); 
	  public FixedLengthStringData comtot02Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,4); 
	  public FixedLengthStringData comtot03Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,8); 
	  public FixedLengthStringData comtot04Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,12); 
	  public FixedLengthStringData comtot05Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,16); 
	  public FixedLengthStringData comtot06Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,20); 
	  public FixedLengthStringData comtot07Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,24); 
	  public FixedLengthStringData comtot08Err =new FixedLengthStringData(4).isAPartOf(comtotsErr,28); 
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData prcentsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData[] prcentErr = FLSArrayPartOfStructure(8, 4, prcentsErr, 0);
	  public FixedLengthStringData prcent01Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,0); 
	  public FixedLengthStringData prcent02Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,4); 
	  public FixedLengthStringData prcent03Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,8); 
	  public FixedLengthStringData prcent04Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,12); 
	  public FixedLengthStringData prcent05Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,16); 
	  public FixedLengthStringData prcent06Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,20); 
	  public FixedLengthStringData prcent07Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,24); 
	  public FixedLengthStringData prcent08Err =new FixedLengthStringData(4).isAPartOf(prcentsErr,28); 
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData znadjpercsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData[] znadjpercErr = FLSArrayPartOfStructure(2, 4, znadjpercsErr, 0);
	  public FixedLengthStringData znadjperc01Err =new FixedLengthStringData(4).isAPartOf(znadjpercsErr,0); 
	  public FixedLengthStringData znadjperc02Err =new FixedLengthStringData(4).isAPartOf(znadjpercsErr,4); 


	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea,342);

	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData comtotsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] comtotOut = FLSArrayPartOfStructure(8, 12, comtotsOut, 0);
	public FixedLengthStringData[][] comtotO = FLSDArrayPartOfArrayStructure(12, 1, comtotOut, 0);
	  public FixedLengthStringData[] comtot01Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 0);
	  public FixedLengthStringData[] comtot02Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 12);
	  public FixedLengthStringData[] comtot03Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 24);
	  public FixedLengthStringData[] comtot04Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 36);
	  public FixedLengthStringData[] comtot05Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 48);
	  public FixedLengthStringData[] comtot06Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 60);
	  public FixedLengthStringData[] comtot07Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 72);
	  public FixedLengthStringData[] comtot08Out = FLSArrayPartOfStructure(12, 1, comtotsOut, 84);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData prcentsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] prcentOut = FLSArrayPartOfStructure(8, 12, prcentsOut, 0);
	public FixedLengthStringData[][] prcentO = FLSDArrayPartOfArrayStructure(12, 1, prcentOut, 0);
	  public FixedLengthStringData[] prcent01Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 0);
	  public FixedLengthStringData[] prcent02Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 12);
	  public FixedLengthStringData[] prcent03Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 24);
	  public FixedLengthStringData[] prcent04Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 36);
	  public FixedLengthStringData[] prcent05Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 48);
	  public FixedLengthStringData[] prcent06Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 60);
	  public FixedLengthStringData[] prcent07Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 72);
	  public FixedLengthStringData[] prcent08Out = FLSArrayPartOfStructure(12, 1, prcentsOut, 84);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData znadjpercsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 264);
	public FixedLengthStringData[] znadjpercOut = FLSArrayPartOfStructure(2, 12, znadjpercsOut, 0);
	public FixedLengthStringData[][] znadjpercO = FLSDArrayPartOfArrayStructure(12, 1, znadjpercOut, 0);
	  public FixedLengthStringData[] znadjperc01Out = FLSArrayPartOfStructure(12, 1, znadjpercsOut, 0);
	  public FixedLengthStringData[] znadjperc02Out = FLSArrayPartOfStructure(12, 1, znadjpercsOut, 12);


	//START OF SUBFILE AREA	 


	//START OF SFL FIELDS	 

 
	//SFL TOTAL ERROR DEC


	//SFL ERROR FIELD


	//SFL OUT IND FIELD


	//SFL PAGE

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);


	//public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	//public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public GeneralTable sr58sscreensfl = new GeneralTable(AppVars.getInstance());
	public LongData Sr58sscreenctlWritten = new LongData(0);
	public LongData Sr58sscreensflWritten = new LongData(0);
	public LongData Sr58sscreenWritten = new LongData(0);
	public LongData Sr58swindowWritten = new LongData(0);
	public LongData Sr58shideWritten = new LongData(0);
	public LongData Sr58sprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr58sScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
	
	fieldIndMap.put(companyOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(comtot08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itemOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmfrmOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(itmtoOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(longdescOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent03Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent04Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent05Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent06Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent07Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(prcent08Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(tablOut,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(znadjperc01Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});
	fieldIndMap.put(znadjperc02Out,new String[] {null,null, null,null, null, null, null, null, null, null, null, null});



	screenFields = new BaseData[] { company,comtot01,comtot02,comtot03,comtot04,comtot05,comtot06,comtot07,comtot08,item,itmfrm,itmto,longdesc,prcent01,prcent02,prcent03,prcent04,prcent05,prcent06,prcent07,prcent08,tabl,znadjperc01,znadjperc02 };
	screenOutFields = new BaseData[][] { companyOut,comtot01Out,comtot02Out,comtot03Out,comtot04Out,comtot05Out,comtot06Out,comtot07Out,comtot08Out,itemOut,itmfrmOut,itmtoOut,longdescOut,prcent01Out,prcent02Out,prcent03Out,prcent04Out,prcent05Out,prcent06Out,prcent07Out,prcent08Out,tablOut,znadjperc01Out,znadjperc02Out };
	screenErrFields = new BaseData[] {  companyErr,comtot01Err,comtot02Err,comtot03Err,comtot04Err,comtot05Err,comtot06Err,comtot07Err,comtot08Err,itemErr,itmfrmErr,itmtoErr,longdescErr,prcent01Err,prcent02Err,prcent03Err,prcent04Err,prcent05Err,prcent06Err,prcent07Err,prcent08Err,tablErr,znadjperc01Err,znadjperc02Err  };
	screenDateFields = new BaseData[] { itmfrm,itmto  };
	screenDateErrFields = new BaseData[] { itmfrmErr,itmtoErr  };
	screenDateDispFields = new BaseData[] { itmfrmDisp,itmtoDisp   };

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr58sscreen.class;
	protectRecord = Sr58sprotect.class;

	}

 

}
