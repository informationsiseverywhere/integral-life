package com.csc.life.agents.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR505.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rr505Report extends SMARTReportLayout { 

	private FixedLengthStringData agnt01 = new FixedLengthStringData(8);
	private FixedLengthStringData agnt02 = new FixedLengthStringData(8);
	private FixedLengthStringData agnt03 = new FixedLengthStringData(8);
	private FixedLengthStringData agnt04 = new FixedLengthStringData(8);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum01 = new FixedLengthStringData(8);
	private ZonedDecimalData comern01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData comern02 = new ZonedDecimalData(17, 2);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData compay01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData compay02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData initcom01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData initcom02 = new ZonedDecimalData(17, 2);
	private FixedLengthStringData ovrdcat01 = new FixedLengthStringData(1);
	private FixedLengthStringData ovrdcat02 = new FixedLengthStringData(1);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData servagnt01 = new FixedLengthStringData(8);
	private FixedLengthStringData servagnt02 = new FixedLengthStringData(8);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr505Report() {
		super();
	}


	/**
	 * Print the XML for Rr505d02
	 */
	public void printRr505d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum01.setFieldName("chdrnum01");
		chdrnum01.setInternal(subString(recordData, 1, 8));
		agnt01.setFieldName("agnt01");
		agnt01.setInternal(subString(recordData, 9, 8));
		ovrdcat01.setFieldName("ovrdcat01");
		ovrdcat01.setInternal(subString(recordData, 17, 1));
		initcom01.setFieldName("initcom01");
		initcom01.setInternal(subString(recordData, 18, 17));
		compay01.setFieldName("compay01");
		compay01.setInternal(subString(recordData, 35, 17));
		agnt02.setFieldName("agnt02");
		agnt02.setInternal(subString(recordData, 52, 8));
		ovrdcat02.setFieldName("ovrdcat02");
		ovrdcat02.setInternal(subString(recordData, 60, 1));
		initcom02.setFieldName("initcom02");
		initcom02.setInternal(subString(recordData, 61, 17));
		compay02.setFieldName("compay02");
		compay02.setInternal(subString(recordData, 78, 17));
		comern01.setFieldName("comern01");
		comern01.setInternal(subString(recordData, 95, 17));
		comern02.setFieldName("comern02");
		comern02.setInternal(subString(recordData, 112, 17));
		printLayout("Rr505d02",			// Record name
			new BaseData[]{			// Fields:
				chdrnum01,
				agnt01,
				ovrdcat01,
				initcom01,
				compay01,
				agnt02,
				ovrdcat02,
				initcom02,
				compay02,
				comern01,
				comern02
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr505d03
	 */
	public void printRr505d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		servagnt01.setFieldName("servagnt01");
		servagnt01.setInternal(subString(recordData, 1, 8));
		servagnt02.setFieldName("servagnt02");
		servagnt02.setInternal(subString(recordData, 9, 8));
		printLayout("Rr505d03",			// Record name
			new BaseData[]{			// Fields:
				servagnt01,
				servagnt02
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr505h01
	 */
	public void printRr505h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 32, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 42, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 44, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		agnt03.setFieldName("agnt03");
		agnt03.setInternal(subString(recordData, 74, 8));
		agnt04.setFieldName("agnt04");
		agnt04.setInternal(subString(recordData, 82, 8));
		printLayout("Rr505h01",			// Record name
			new BaseData[]{			// Fields:
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr,
				agnt03,
				agnt04
			}
		);

		currentPrintLine.set(16);
	}


}
