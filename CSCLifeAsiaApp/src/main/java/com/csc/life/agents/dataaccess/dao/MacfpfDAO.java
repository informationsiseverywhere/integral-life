/******************** */
/*Author  :Liwei      */
/*Purpose :curd data for Macfpf       */
/*Date    :2018.10.26           */ 
package com.csc.life.agents.dataaccess.dao;

import java.util.List;

import com.csc.life.agents.dataaccess.model.Macfpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface MacfpfDAO extends BaseDAO<Macfpf>  {
	public  List<Macfpf>  searchMacfpfRecord(String agntcoy, String agnutnum, int effdate);
}
