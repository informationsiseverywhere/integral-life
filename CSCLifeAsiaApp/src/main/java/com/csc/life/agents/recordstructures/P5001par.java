package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:29
 * Description:
 * Copybook name: P5001PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class P5001par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(71);
  	public FixedLengthStringData agentfrom = new FixedLengthStringData(8).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData agentto = new FixedLengthStringData(8).isAPartOf(parmRecord, 8);
  	public FixedLengthStringData comind = new FixedLengthStringData(1).isAPartOf(parmRecord, 16);
  	public FixedLengthStringData initflg = new FixedLengthStringData(1).isAPartOf(parmRecord, 17);
  	public FixedLengthStringData rnwlflg = new FixedLengthStringData(1).isAPartOf(parmRecord, 18);
  	public FixedLengthStringData servflg = new FixedLengthStringData(1).isAPartOf(parmRecord, 19);
  	public FixedLengthStringData zrorind = new FixedLengthStringData(1).isAPartOf(parmRecord, 20);
	public FixedLengthStringData username = new FixedLengthStringData(50).isAPartOf(parmRecord, 21);//ILJ-21


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}