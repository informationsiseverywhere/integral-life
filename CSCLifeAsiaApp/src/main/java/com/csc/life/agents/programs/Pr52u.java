/*
 * File: Pr52u.java
 * Date: December 3, 2013 3:27:46 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR52U.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Varprog;
import com.csc.fsu.general.recordstructures.Varprogrec;
import com.csc.life.agents.screens.Sr52uScreenVars;
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.general.dataaccess.PovrgenTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.recordstructures.Compdelrec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.dataaccess.MrtaTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.RacdlnbTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.life.underwriting.dataaccess.UndqTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Change Plan Contract Proposal.
* ==============================
*
* This program is for changing plan (CNTTYPE) of a contract
* proposal. Contract Number is remain the same and user may
* select a new plan.
*
* Note that any records deletion introduced here should be in-line
* with the online proposal component deletion program P5306.
*
* Update section
* - Delete all COVT records
* - Delete other related component files
*
* Where Next
* - S5003 Work with Proposal screen
*
****************************************************************** ****
* </pre>
*/
public class Pr52u extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52U");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData x = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private Validator notFirstTime = new Validator(wsaaFirstTime, "N");

	private FixedLengthStringData wsaaConcatName = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 4);
	private static final String e304 = "E304";
	private static final String f054 = "F054";
	private static final String f235 = "F235";
	private static final String g297 = "G297";
	private static final String h366 = "H366";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String t5671 = "T5671";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupTableDAM flupIO = new FlupTableDAM();
	private FupeTableDAM fupeIO = new FupeTableDAM();
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MinsTableDAM minsIO = new MinsTableDAM();
	private MrtaTableDAM mrtaIO = new MrtaTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PovrgenTableDAM povrgenIO = new PovrgenTableDAM();
	private RacdlnbTableDAM racdlnbIO = new RacdlnbTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private UndlTableDAM undlIO = new UndlTableDAM();
	private UndqTableDAM undqIO = new UndqTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private Compdelrec compdelrec = new Compdelrec();
	private Varprogrec varprogrec = new Varprogrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5671rec t5671rec = new T5671rec();
	private T5688rec t5688rec = new T5688rec();
	private Sr52uScreenVars sv = ScreenProgram.getScreenVars( Sr52uScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readCovt5050, 
		a5090Exit, 
		readUndc5150, 
		readLext5250, 
		readUnlt5350, 
		readPovr5450, 
		readHbnf5550, 
		readRacd5650, 
		readUndl5750, 
		exit5790, 
		readUndq5850, 
		exit5890, 
		readLife6250, 
		readFlup6350, 
		readFupe6450, 
		readHxcl6550, 
		exit6690, 
		call6720, 
		exit6790, 
		call6820, 
		exit6890
	}

	public Pr52u() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52u", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		wsaaFirstTime.set("Y");
		/* Retreive the today date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Read payer file*/
		payrIO.setChdrcoy(wsspcomn.company);
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Setup screen variables*/
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cntcurr.set(payrIO.getCntcurr());
		sv.register.set(chdrlnbIO.getRegister());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		getClientDetails1100();
		/* Read the contract definition description from table*/
		/* T5688 for the contract held on the client header record.*/
		descIO.setDescitem(chdrlnbIO.getCnttype());
		readT5688Desc1200();
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.descrip01.set(descIO.getLongdesc());
		}
		else {
			sv.descrip01.fill("?");
		}
		sv.chdrtype01.set(chdrlnbIO.getCnttype());
		/* Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
	}

protected void getClientDetails1100()
	{
		read1110();
	}

protected void read1110()
	{
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), "1")) {
			sv.ownernameErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void readT5688Desc1200()
	{
		/*START*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		/* The new plan must not be blank and different from current plan*/
		if (isEQ(sv.chdrtype02, SPACES)) {
			sv.chdrtype02Err.set(h366);
		}
		if (isEQ(sv.chdrtype02, sv.chdrtype01)) {
			sv.chdrtype02Err.set(g297);
		}
		if (isEQ(sv.chdrtype02Err, SPACES)) {
			descIO.setDescitem(sv.chdrtype02);
			readT5688Desc1200();
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.descrip02.set(descIO.getLongdesc());
			}
			else {
				sv.descrip02.fill("?");
			}
			readT56882100();
			if (isNE(t5688rec.nbusallw, "Y")) {
				sv.chdrtype02Err.set(f235);
			}
		}
		else {
			sv.descrip02.set(SPACES);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			wsaaFirstTime.set("Y");
		}
		else {
			if (firstTime.isTrue()) {
				sv.chdrnumErr.set(f054);
				wsspcomn.edterror.set("Y");
				wsaaFirstTime.set("N");
			}
		}
		/*EXIT*/
	}

protected void readT56882100()
	{
		read2110();
	}

protected void read2110()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(sv.chdrtype02);
		itdmIO.setItmfrm(wsaaToday);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(), sv.chdrtype02)
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemcoy(), wsspcomn.company)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		deleteCovtlnb5000();
		deleteUndc5100();
		deleteLext5200();
		deleteUnltunl5300();
		deletePovrgen5400();
		deleteHbnf5500();
		deleteRacdlnb5600();
		deleteMbns6700();
		deleteMins6800();
		deleteUndl5700();
		deleteUndq5800();
		deleteOtherLife6200();
		deleteFlup6300();
		deleteFupe6400();
		deleteHxcl6500();
		deleteMrta6600();
		updateChdr6000();
		keepsLifelnb6100();
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* Use VARPROG to find the next program. This to extend the*/
		/* submenu prog switching from T1690 to T3570*/
		varprogrec.progCode.set(wsaaProg);
		varprogrec.company.set(wsspcomn.company);
		varprogrec.action.set(wsspcomn.flag);
		callProgram(Varprog.class, varprogrec.varprogRec);
		if (isNE(varprogrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(varprogrec.statuz);
			syserrrec.params.set(varprogrec.varprogRec);
			fatalError600();
		}
		wsspcomn.secProg[2].set(varprogrec.nxt1prog);
		wsspcomn.secProg[3].set(varprogrec.nxt2prog);
		wsspcomn.secProg[4].set(varprogrec.nxt3prog);
		wsspcomn.secProg[5].set(varprogrec.nxt4prog);
		wsspcomn.programPtr.add(1);
	}

protected void deleteCovtlnb5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5010();
				case readCovt5050: 
					readCovt5050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5010()
	{
		/* Delete all applicable coverage/rider transaction records.*/
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setSeqnbr(0);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
	}

protected void readCovt5050()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			return ;
		}
		a5000CallGenericSubroutine();
		covtlnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readCovt5050);
	}

protected void a5000CallGenericSubroutine()
	{
		try {
			a5010Start();
			a5030CallSubr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a5010Start()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5671);
		wsaaTrancode.set("T600");
		wsaaCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsaaConcatName);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.mrnf)
		&& isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			goTo(GotoLabel.a5090Exit);
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void a5030CallSubr()
	{
		compdelrec.compdelRec.set(SPACES);
		compdelrec.chdrcoy.set(covtlnbIO.getChdrcoy());
		compdelrec.chdrnum.set(covtlnbIO.getChdrnum());
		compdelrec.life.set(covtlnbIO.getLife());
		compdelrec.coverage.set(covtlnbIO.getCoverage());
		compdelrec.rider.set(covtlnbIO.getRider());
		for (x.set(1); !(isGT(x, 4)); x.add(1)){
			if (isNE(t5671rec.subprog[x.toInt()], SPACES)) {
				callProgram(t5671rec.subprog[x.toInt()], compdelrec.compdelRec);
				if (isNE(compdelrec.statuz, varcom.oK)) {
					syserrrec.params.set(compdelrec.compdelRec);
					syserrrec.statuz.set(compdelrec.statuz);
					fatalError600();
				}
			}
		}
	}

protected void deleteUndc5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5110();
				case readUndc5150: 
					readUndc5150();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5110()
	{
		/* Delete all applicable Underwriting Coverage records*/
		undcIO.setParams(SPACES);
		undcIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		undcIO.setChdrnum(chdrlnbIO.getChdrnum());
		undcIO.setFormat(formatsInner.undcrec);
		undcIO.setFunction(varcom.begn);
	}

protected void readUndc5150()
	{
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)
		&& isNE(undcIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(undcIO.getStatuz());
			syserrrec.params.set(undcIO.getParams());
			fatalError600();
		}
		if (isNE(undcIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(undcIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(undcIO.getStatuz(), varcom.endp)) {
			return ;
		}
		undcIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(undcIO.getStatuz());
			syserrrec.params.set(undcIO.getParams());
			fatalError600();
		}
		undcIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readUndc5150);
	}

protected void deleteLext5200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5210();
				case readLext5250: 
					readLext5250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5210()
	{
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lextIO.setChdrnum(chdrlnbIO.getChdrnum());
		lextIO.setSeqnbr(0);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.begn);
	}

protected void readLext5250()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)
		&& isNE(lextIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		if (isNE(lextIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(lextIO.getStatuz(), varcom.endp)) {
			return ;
		}
		lextIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		lextIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readLext5250);
	}

protected void deleteUnltunl5300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5310();
				case readUnlt5350: 
					readUnlt5350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5310()
	{
		unltunlIO.setParams(SPACES);
		unltunlIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		unltunlIO.setChdrnum(chdrlnbIO.getChdrnum());
		unltunlIO.setSeqnbr(ZERO);
		unltunlIO.setFormat(formatsInner.unltunlrec);
		unltunlIO.setFunction(varcom.begn);
	}

protected void readUnlt5350()
	{
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)
		&& isNE(unltunlIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(unltunlIO.getStatuz());
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		if (isNE(unltunlIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(unltunlIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(unltunlIO.getStatuz(), varcom.endp)) {
			return ;
		}
		unltunlIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(unltunlIO.getStatuz());
			syserrrec.params.set(unltunlIO.getParams());
			fatalError600();
		}
		unltunlIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readUnlt5350);
	}

protected void deletePovrgen5400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5410();
				case readPovr5450: 
					readPovr5450();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5410()
	{
		povrgenIO.setParams(SPACES);
		povrgenIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		povrgenIO.setChdrnum(chdrlnbIO.getChdrnum());
		povrgenIO.setPlanSuffix(ZERO);
		povrgenIO.setFormat(formatsInner.povrgenrec);
		povrgenIO.setFunction(varcom.begn);
	}

protected void readPovr5450()
	{
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(), varcom.oK)
		&& isNE(povrgenIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(povrgenIO.getStatuz());
			syserrrec.params.set(povrgenIO.getParams());
			fatalError600();
		}
		if (isNE(povrgenIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(povrgenIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(povrgenIO.getStatuz(), varcom.endp)) {
			return ;
		}
		povrgenIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, povrgenIO);
		if (isNE(povrgenIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(povrgenIO.getStatuz());
			syserrrec.params.set(povrgenIO.getParams());
			fatalError600();
		}
		povrgenIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readPovr5450);
	}

protected void deleteHbnf5500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5510();
				case readHbnf5550: 
					readHbnf5550();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5510()
	{
		hbnfIO.setParams(SPACES);
		hbnfIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hbnfIO.setChdrnum(chdrlnbIO.getChdrnum());
		hbnfIO.setFunction(varcom.begn);
		hbnfIO.setFormat(formatsInner.hbnfrec);
	}

protected void readHbnf5550()
	{
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(hbnfIO.getStatuz());
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		if (isNE(hbnfIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(hbnfIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(hbnfIO.getStatuz(), varcom.endp)) {
			return ;
		}
		hbnfIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hbnfIO.getStatuz());
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		hbnfIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readHbnf5550);
	}

protected void deleteRacdlnb5600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5610();
				case readRacd5650: 
					readRacd5650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5610()
	{
		racdlnbIO.setParams(SPACES);
		racdlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		racdlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		racdlnbIO.setPlanSuffix(ZERO);
		racdlnbIO.setSeqno(ZERO);
		racdlnbIO.setFormat(formatsInner.racdlnbrec);
		racdlnbIO.setFunction(varcom.begn);
	}

protected void readRacd5650()
	{
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)
		&& isNE(racdlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		if (isNE(racdlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(racdlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(racdlnbIO.getStatuz(), varcom.endp)) {
			return ;
		}
		racdlnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, racdlnbIO);
		if (isNE(racdlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(racdlnbIO.getStatuz());
			syserrrec.params.set(racdlnbIO.getParams());
			fatalError600();
		}
		racdlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readRacd5650);
	}

protected void deleteUndl5700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5710();
				case readUndl5750: 
					readUndl5750();
				case exit5790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5710()
	{
		undlIO.setParams(SPACES);
		undlIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		undlIO.setChdrnum(chdrlnbIO.getChdrnum());
		undlIO.setFunction(varcom.begn);
		undlIO.setFormat(formatsInner.undlrec);
	}

protected void readUndl5750()
	{
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(), varcom.oK)
		&& isNE(undlIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(undlIO.getStatuz());
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}
		if (isNE(undlIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(undlIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(undlIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit5790);
		}
		undlIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(undlIO.getStatuz());
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}
		undlIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readUndl5750);
	}

protected void deleteUndq5800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5810();
				case readUndq5850: 
					readUndq5850();
				case exit5890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5810()
	{
		undqIO.setParams(SPACES);
		undqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		undqIO.setChdrnum(chdrlnbIO.getChdrnum());
		undqIO.setFunction(varcom.begn);
		undqIO.setFormat(formatsInner.undqrec);
	}

protected void readUndq5850()
	{
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)
		&& isNE(undqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(undqIO.getStatuz());
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		if (isNE(undqIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(undqIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(undqIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit5890);
		}
		undqIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(undqIO.getStatuz());
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		undqIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readUndq5850);
	}

protected void updateChdr6000()
	{
		start6010();
	}

protected void start6010()
	{
		chdrlnbIO.setCnttype(sv.chdrtype02);
		chdrlnbIO.setBillfreq(SPACES);
		chdrlnbIO.setBillchnl(SPACES);
		chdrlnbIO.setBillcd(varcom.vrcmMaxDate);
		/*  Reset direct debit and group details*/
		chdrlnbIO.setMandref(SPACES);
		chdrlnbIO.setGrupkey(SPACES);
		chdrlnbIO.setMembsel(SPACES);
		chdrlnbIO.setMplnum(SPACES);
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Update PAYR - Initialise BILLFREQ and BILLCHNL*/
		payrIO.setBillfreq(SPACES);
		payrIO.setBillchnl(SPACES);
		/*  Reset direct debit and group details*/
		payrIO.setGrupnum(SPACES);
		payrIO.setGrupcoy(SPACES);
		payrIO.setMembsel(SPACES);
		payrIO.setBillnet(SPACES);
		payrIO.setBillcd(varcom.vrcmMaxDate);
		payrIO.setFunction(varcom.writd);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Update HPAD - Initialise HUWDCDTE*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		hpadIO.setHuwdcdte(varcom.vrcmMaxDate);
		hpadIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
	}

protected void keepsLifelnb6100()
	{
		start6110();
	}

protected void start6110()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		lifelnbIO.setFunction(varcom.keeps);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
	}

protected void deleteOtherLife6200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start6210();
				case readLife6250: 
					readLife6250();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6210()
	{
		/* Delete other than first life.*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.begn);
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
	}

protected void readLife6250()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(lifelnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			return ;
		}
		lifelnbIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		lifelnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readLife6250);
	}

protected void deleteFlup6300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start6310();
				case readFlup6350: 
					readFlup6350();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6310()
	{
		flupIO.setParams(SPACES);
		flupIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		flupIO.setChdrnum(chdrlnbIO.getChdrnum());
		flupIO.setFupno(ZERO);
		flupIO.setFunction(varcom.begn);
		flupIO.setFormat(formatsInner.fluprec);
	}

protected void readFlup6350()
	{
		SmartFileCode.execute(appVars, flupIO);
		if (isNE(flupIO.getStatuz(), varcom.oK)
		&& isNE(flupIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(flupIO.getStatuz());
			syserrrec.params.set(flupIO.getParams());
			fatalError600();
		}
		if (isNE(flupIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(flupIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(flupIO.getStatuz(), varcom.endp)) {
			return ;
		}
		flupIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, flupIO);
		if (isNE(flupIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(flupIO.getStatuz());
			syserrrec.params.set(flupIO.getParams());
			fatalError600();
		}
		flupIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readFlup6350);
	}

protected void deleteFupe6400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start6410();
				case readFupe6450: 
					readFupe6450();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6410()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fupeIO.setChdrnum(chdrlnbIO.getChdrnum());
		fupeIO.setFupno(ZERO);
		fupeIO.setTranno(ZERO);
		fupeIO.setFunction(varcom.begn);
		fupeIO.setFormat(formatsInner.fuperec);
	}

protected void readFupe6450()
	{
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)
		&& isNE(fupeIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fupeIO.getStatuz());
			syserrrec.params.set(fupeIO.getParams());
			fatalError600();
		}
		if (isNE(fupeIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(fupeIO.getStatuz(), varcom.endp)) {
			return ;
		}
		fupeIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(fupeIO.getStatuz());
			syserrrec.params.set(fupeIO.getParams());
			fatalError600();
		}
		fupeIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readFupe6450);
	}

protected void deleteHxcl6500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start6510();
				case readHxcl6550: 
					readHxcl6550();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start6510()
	{
		hxclIO.setParams(SPACES);
		hxclIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hxclIO.setChdrnum(chdrlnbIO.getChdrnum());
		hxclIO.setFupno(ZERO);
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFunction(varcom.begn);
		hxclIO.setFormat(formatsInner.hxclrec);
	}

protected void readHxcl6550()
	{
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(), varcom.oK)
		&& isNE(hxclIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(hxclIO.getStatuz());
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(hxclIO.getStatuz(), varcom.endp)) {
			return ;
		}
		hxclIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hxclIO.getStatuz());
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		hxclIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readHxcl6550);
	}

protected void deleteMrta6600()
	{
		try {
			init6610();
			call6620();
			delete6630();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void init6610()
	{
		mrtaIO.setParams(SPACES);
		mrtaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mrtaIO.setChdrnum(chdrlnbIO.getChdrnum());
		mrtaIO.setFormat(formatsInner.mrtarec);
		mrtaIO.setFunction(varcom.readh);
	}

protected void call6620()
	{
		SmartFileCode.execute(appVars, mrtaIO);
		if (isNE(mrtaIO.getStatuz(), varcom.oK)
		&& isNE(mrtaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mrtaIO.getStatuz());
			syserrrec.params.set(mrtaIO.getParams());
			fatalError600();
		}
		if (isEQ(mrtaIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit6690);
		}
	}

protected void delete6630()
	{
		mrtaIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mrtaIO);
		if (isNE(mrtaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mrtaIO.getStatuz());
			syserrrec.params.set(mrtaIO.getParams());
			fatalError600();
		}
	}

protected void deleteMbns6700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					init6710();
				case call6720: 
					call6720();
					delete6730();
					next6780();
				case exit6790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init6710()
	{
		mbnsIO.setParams(SPACES);
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(SPACES);
		mbnsIO.setCoverage(SPACES);
		mbnsIO.setRider(SPACES);
		mbnsIO.setYrsinf(ZERO);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
	}

protected void call6720()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)
		&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		if (isEQ(mbnsIO.getStatuz(), varcom.endp)
		|| isNE(mbnsIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(mbnsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())) {
			goTo(GotoLabel.exit6790);
		}
	}

protected void delete6730()
	{
		mbnsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
	}

protected void next6780()
	{
		mbnsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call6720);
	}

protected void deleteMins6800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					init6810();
				case call6820: 
					call6820();
					delete6830();
					next6880();
				case exit6890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void init6810()
	{
		minsIO.setParams(SPACES);
		minsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		minsIO.setChdrnum(chdrlnbIO.getChdrnum());
		minsIO.setLife(SPACES);
		minsIO.setCoverage(SPACES);
		minsIO.setRider(SPACES);
		minsIO.setFormat(formatsInner.minsrec);
		minsIO.setFunction(varcom.begn);
	}

protected void call6820()
	{
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)
		&& isNE(minsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(), varcom.endp)
		|| isNE(minsIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(minsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())) {
			goTo(GotoLabel.exit6890);
		}
	}

protected void delete6830()
	{
		minsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		minsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
	}

protected void next6880()
	{
		minsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call6820);
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData undcrec = new FixedLengthStringData(10).init("UNDCREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData racdlnbrec = new FixedLengthStringData(10).init("RACDLNBREC");
	private FixedLengthStringData povrgenrec = new FixedLengthStringData(10).init("POVRGENREC");
	private FixedLengthStringData undqrec = new FixedLengthStringData(10).init("UNDQREC");
	private FixedLengthStringData undlrec = new FixedLengthStringData(10).init("UNDLREC");
	private FixedLengthStringData fluprec = new FixedLengthStringData(10).init("FLUPREC");
	private FixedLengthStringData fuperec = new FixedLengthStringData(10).init("FUPEREC");
	private FixedLengthStringData hxclrec = new FixedLengthStringData(10).init("HXCLREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData mrtarec = new FixedLengthStringData(10).init("MRTAREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData minsrec = new FixedLengthStringData(10).init("MINSREC");
}
}
