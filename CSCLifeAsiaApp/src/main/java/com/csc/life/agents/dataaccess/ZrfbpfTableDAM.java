package com.csc.life.agents.dataaccess;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: ZrfbpfTableDAM.java
 * Date: Sat, 2 Aug 2013 07:02:52
 * Class transformed from ZRFBPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZrfbpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 126;
	public FixedLengthStringData zrfbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zrfbpfRecord = zrfbrec;
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData zrecruit = DD.zrecruit.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zrfbrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(zrfbrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zrfbrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zrfbrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zrfbrec);
	public PackedDecimalData acctamt = DD.acctamt.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(zrfbrec);
	public PackedDecimalData bonusamt = DD.bonusamt.copy().isAPartOf(zrfbrec);
	public PackedDecimalData prcdate = DD.prcdate.copy().isAPartOf(zrfbrec);
	public PackedDecimalData jobno = DD.jobno.copy().isAPartOf(zrfbrec);	
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zrfbrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zrfbrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZrfbpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZrfbpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZrfbpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZrfbpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrfbpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZrfbpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZrfbpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZRFBPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 

						"AGNTCOY,"+
						"ZRECRUIT,"+
								"AGNTNUM,"+
						"BATCACTYR,"+
								"BATCACTMN,"+
						"AGTYPE,"+
								"CHDRNUM,"+
						"LIFE,"+
								"COVERAGE,"+
						"RIDER,"+
								"EFFDATE,"+
						"TRANNO,"+
								"ACCTAMT,"+
						"ORIGCURR,"+
								"BONUSAMT,"+
						"PRCDATE,"+
								"JOBNO,"+
						"USRPRF,"+
								"JOBNM,"+
						"DATIME,"+
								"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
				agntcoy,
				zrecruit,
				agntnum,
				batcactyr,
				batcactmn,
				agtype,
				chdrnum,
				life,
				coverage,
				rider,
				effdate,
				tranno,
				acctamt,
				origcurr,
				bonusamt,
				prcdate,
				jobno,
				userProfile,
				jobName,
				datime,
				unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
		agntcoy.clear();
		zrecruit.clear();
		agntnum.clear();
		batcactyr.clear();
		batcactmn.clear();
		agtype.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		effdate.clear();
		tranno.clear();
		acctamt.clear();
		origcurr.clear();
		bonusamt.clear();
		prcdate.clear();
		jobno.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZrfbrec() {
  		return zrfbrec;
	}

	public FixedLengthStringData getZrfbpfRecord() {
  		return zrfbpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZrfbrec(what);
	}

	public void setZrfbrec(Object what) {
  		this.zrfbrec.set(what);
	}

	public void setZrfbpfRecord(Object what) {
  		this.zrfbpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zrfbrec.getLength());
		result.set(zrfbrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}