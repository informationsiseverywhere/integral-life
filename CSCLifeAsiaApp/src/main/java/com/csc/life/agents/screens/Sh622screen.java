package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh622screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh622ScreenVars sv = (Sh622ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh622screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh622ScreenVars screenVars = (Sh622ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.zryrperc01.setClassString("");
		screenVars.zryrperc02.setClassString("");
		screenVars.zryrperc03.setClassString("");
		screenVars.zryrperc04.setClassString("");
		screenVars.zryrperc05.setClassString("");
		screenVars.zryrperc06.setClassString("");
		screenVars.zryrperc07.setClassString("");
		screenVars.zryrperc08.setClassString("");
		screenVars.zryrperc09.setClassString("");
		screenVars.zryrperc10.setClassString("");
		screenVars.zryrperc21.setClassString("");
		screenVars.zryrperc22.setClassString("");
		screenVars.zryrperc11.setClassString("");
		screenVars.zryrperc12.setClassString("");
		screenVars.zryrperc13.setClassString("");
		screenVars.zryrperc14.setClassString("");
		screenVars.zryrperc15.setClassString("");
		screenVars.zryrperc16.setClassString("");
		screenVars.zryrperc17.setClassString("");
		screenVars.zryrperc18.setClassString("");
		screenVars.zryrperc19.setClassString("");
		screenVars.zryrperc20.setClassString("");
		screenVars.keyopt01.setClassString("");
		screenVars.keyopt02.setClassString("");
	}

/**
 * Clear all the variables in Sh622screen
 */
	public static void clear(VarModel pv) {
		Sh622ScreenVars screenVars = (Sh622ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.zryrperc01.clear();
		screenVars.zryrperc02.clear();
		screenVars.zryrperc03.clear();
		screenVars.zryrperc04.clear();
		screenVars.zryrperc05.clear();
		screenVars.zryrperc06.clear();
		screenVars.zryrperc07.clear();
		screenVars.zryrperc08.clear();
		screenVars.zryrperc09.clear();
		screenVars.zryrperc10.clear();
		screenVars.zryrperc21.clear();
		screenVars.zryrperc22.clear();
		screenVars.zryrperc11.clear();
		screenVars.zryrperc12.clear();
		screenVars.zryrperc13.clear();
		screenVars.zryrperc14.clear();
		screenVars.zryrperc15.clear();
		screenVars.zryrperc16.clear();
		screenVars.zryrperc17.clear();
		screenVars.zryrperc18.clear();
		screenVars.zryrperc19.clear();
		screenVars.zryrperc20.clear();
		screenVars.keyopt01.clear();
		screenVars.keyopt02.clear();
	}
}
