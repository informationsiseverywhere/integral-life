package com.csc.life.agents.programs;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgqfpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.agents.dataaccess.model.Agqfpf;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.Sjl03ScreenVars;
import com.csc.life.unitlinkedprocessing.tablestructures.T5543rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;


import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Pjl03 extends ScreenProgCS{

	BaseDAOImpl basedao = new BaseDAOImpl();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL03");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Sjl03ScreenVars sv = ScreenProgram.getScreenVars( Sjl03ScreenVars.class);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private com.csc.fsu.agents.dataaccess.model.Agntpf Agntpf = null;
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private AgqfpfDAO agqfpfDAO = getApplicationContext().getBean("agqfpfDAO", AgqfpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Agntpf agntlag = null;
	private List<Agqfpf> agpfList = null;
	private List<Agqfpf> agpfListTemp = null;
	private T5543rec t5543rec = new T5543rec();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private Agqfpf agqfTemp = null;
	private List<Agqfpf> insertAgqfpfList = null;
	private Set<String> agQualSet = null;
	private Set<String> tempSet = new LinkedHashSet<>();
	private String jl15 = "JL15";
	private String jl16 = "JL16";
	private String jl17 = "JL17";
	private String jl18 = "JL18";
	private String rl39 = "RL39";
	private String h357 = "h357";
	private String jl13 = "jl13";
	private String e844 = "e844";
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private FormatsInner formatsInner = new FormatsInner();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private String empty= "";
	private Agqfpf agqfpf;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1190, 
		preExit, 
		exit2090, 
		exit3900, 
		exit5090, 
		updateErrorIndicators2170
		
	}

	public Pjl03() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl03", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
		{
			sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
			scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
			wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
			wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
			try {
				super.mainline();
			}
			catch (COBOLExitProgramException e) {
			}
		}

	protected void initialise1000()
		{
			initialise1001();
			retrvAglflnb1020();
		}

	protected void initialise1001()
		{
			sv.dataArea.set(SPACES);
			sv.subfileArea.set(SPACES);
			scrnparams.function.set(varcom.sclr);
			processScreen("SJL03", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			agQualSet = new LinkedHashSet<>();
			retrvAgntpf1002();
			agpfList = new ArrayList<Agqfpf>();
			agpfList = agqfpfDAO.getAgqfListbyAgentNumber(sv.agnum.toString());
			wsaaCount.set(ZERO);
			while ( !(isGT(wsaaCount,10))) {
				loadSubfile1003();
			}
		}
	
	protected void retrvAglflnb1020()
	{
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

	protected void retrvAgntpf1002()
	{
		sv.agnum.set(wsspcomn.confirmationKey);
		sv.clntsel.set(wsspcomn.clntkey);
		sv.cltname.set(wsspcomn.longconfname);
		sv.agntype.set(wsspcomn.chdrClntkey);
		Descpf descpf= descDAO.getdescData("IT", "t3692", sv.agntype.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(descpf != null){
			sv.agtydesc.set(descpf.getLongdesc());
		}
	}
	
	protected void loadSubfile1003()
	{
		if (isEQ(wsaaCount,10)) {
			wsaaCount.set(20);
			return;
		}
		if(agpfList.isEmpty()) {
			loadEmptySubfile1103();
		}
		else {
			for(int iy=0 ; iy<10 ; iy++) {
				if(agpfList.size()<=iy) {
					loadEmptySubfile1103();
				}
				else {
					agQualSet.add(agpfList.get(iy).getQualification().trim());
					sv.agntqualification.set(agpfList.get(iy).getQualification());
					sv.liceno.set(agpfList.get(iy).getLicence());
					Descpf descpf= descDAO.getdescData("IT", "tjl01", sv.agntqualification.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
					if(descpf != null){
						sv.quadesc.set(descpf.getLongdesc());
					}
					sv.startdte.set(agpfList.get(iy).getSrdate());
					sv.enddte.set(agpfList.get(iy).getDateend());
					sv.uprflag.set("N");
					scrnparams.function.set(varcom.sadd);
					processScreen("SJL03", sv);
					if (isNE(scrnparams.statuz, varcom.oK)) {
						syserrrec.statuz.set(scrnparams.statuz);
						fatalError600();
					}
					wsaaCount.add(1);
				}
			}
		}
		scrnparams.subfileRrn.set(1); 
	}
	
	protected void loadEmptySubfile1103() {
		sv.agntqualification.set(SPACES);
		sv.liceno.set(SPACES);
		sv.quadesc.set(SPACES);
		sv.startdte.set(99999999);
		sv.enddte.set(99999999);
		sv.uprflag.set(SPACES);
		scrnparams.function.set(varcom.sadd);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
	}
	
	protected void preScreenEdit() {
		if (wsspcomn.flag.equals("I")) {
			sv.agntqualificationOut[varcom.pr.toInt()].set("Y");
			sv.licenoOut[varcom.pr.toInt()].set("Y");
			sv.startdteOut[varcom.pr.toInt()].set("Y");
			sv.enddteOut[varcom.pr.toInt()].set("Y");
			scrnparams.function.set(varcom.prot);
		}
		
	}
	
	protected void screenEdit2000() {
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		/*tempSet = new LinkedHashSet<>();*/
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		checkEmptySubfileInBetween2190();
		checkDuplicateEntries2191();
	}
	
	protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2110();
				}
				case updateErrorIndicators2170: {
					updateErrorIndicators2170();
					readNextModifiedRecord2180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	
	
	
	protected void validation2110()
	{
		Descpf descpf= descDAO.getdescData("IT", "tjl01", sv.agntqualification.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if(descpf != null){
			sv.quadesc.set(descpf.getLongdesc());
		}
		if(isEQ(sv.agntqualification,SPACES)) {
			sv.quadesc.set(SPACES);
		}
		/*if(tempSet.contains(sv.agntqualification.trim())) {
			sv.agntqualificationErr.set(h357);
		}
		if(tempSet.contains(sv.agntqualification.trim()) && empty.equals(sv.agntqualification.toString().trim())) {
			sv.agntqualificationErr.set(rl39);
		}*/
		
		
		if(isEQ(sv.agntqualification,SPACES)) 
		{
			if(isNE(sv.liceno,SPACES))
			{
				sv.licenoErr.set(e844);
				sv.licenoOut[varcom.ri.toInt()].set("Y");
			}
			if(isNE(sv.startdte,99999999)) {
				sv.startdteErr.set(e844);
				sv.startdteOut[varcom.ri.toInt()].set("Y");
			}
			if(isNE(sv.enddte,99999999)) {
				sv.enddteErr.set(e844);
				sv.enddteOut[varcom.ri.toInt()].set("Y");
			}
		}
		else {
			
			if(isEQ(sv.liceno,SPACES))
			{
				sv.licenoErr.set(jl18);
				sv.licenoOut[varcom.ri.toInt()].set("Y");
			}
			
			if(isNE(sv.liceno,SPACES))
			{
				if(sv.liceno.toString().trim().matches("[a-zA-Z0-9]+")) {
				}
				else {
					sv.licenoErr.set(jl13);
					sv.licenoOut[varcom.ri.toInt()].set("Y");
				}
			
			}
			
			if(isEQ(sv.startdte,SPACES) || isEQ(sv.startdte,99999999)) {
				sv.startdteErr.set(jl15);
				sv.startdteOut[varcom.ri.toInt()].set("Y");
			}
			if(isEQ(sv.enddte,SPACES)|| isEQ(sv.enddte,99999999)) {
				sv.enddteErr.set(jl16);
				sv.enddteOut[varcom.ri.toInt()].set("Y");
			}
			
			if(isEQ(sv.startdte,99999999) && isEQ(sv.enddte,99999999)) {
				return;
			}
			
			if(isNE(sv.enddte,SPACES)) {
				if(isLTE(sv.enddte,sv.startdte))
				{
					if(isNE(sv.startdte,99999999)){
					sv.enddteErr.set(jl17);
					sv.enddteOut[varcom.ri.toInt()].set("Y");
					}
				}
			}
			
			
		}
		
		
	}
	
	protected void updateErrorIndicators2170()
	{
		//scrnparams.subfileRrn.set(1);
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void readNextModifiedRecord2180()
	{
		/*if(isNE(sv.agntqualification,SPACES))
		{
			tempSet.add(sv.agntqualification.trim());
		}*/
		//tempSet.add(sv.agntqualification.trim());
		scrnparams.function.set(varcom.srnch);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	
	protected void checkEmptySubfileInBetween2190()
	{
		int a=0, b=1;
		for(int ix = 1 ; ix<10 ; ix++) {
			scrnparams.subfileRrn.set(ix);
			scrnparams.function.set(varcom.sread);
			processScreen("SJL01", sv);
			if (!scrnparams.statuz.equals(varcom.oK)
			&& !scrnparams.statuz.equals(varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			if(isNE(sv.agntqualification, SPACES)) {
				if(a == 0)
					a = ix;
				if(ix > a)
					b = ix;
				if((b-a) > 1) {
					sv.agntqualificationErr.set(rl39);
					break;
				}
				else {
					a = b;
				}
			}
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void checkDuplicateEntries2191() {
		tempSet = new LinkedHashSet<>();
		for(int ix = 1 ; ix<10 ; ix++) {
			scrnparams.subfileRrn.set(ix);
			scrnparams.function.set(varcom.sread);
			processScreen("SJL01", sv);
			if (!scrnparams.statuz.equals(varcom.oK)
			&& !scrnparams.statuz.equals(varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			if(isNE(sv.agntqualification, SPACES)) {
				if(tempSet.contains(sv.agntqualification.trim())) {
					sv.quadescErr.set(h357);
					break;
				}
				else {
					tempSet.add(sv.agntqualification.trim());
				}
			}
		}
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SJL03", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void update3000() {
			insertAgqfpfList = new ArrayList<Agqfpf>();
			for(int upflag=1 ; upflag<=10 ; upflag++) {
				scrnparams.subfileRrn.set(upflag);
				scrnparams.function.set(varcom.sread);
				processScreen("SJL03", sv);
				if (!scrnparams.statuz.equals(varcom.oK)
				&& !scrnparams.statuz.equals(varcom.endp)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
				if(isNE(sv.agntqualification, SPACES) && agpfList.isEmpty()) {
					sv.uprflag.set("A");
				}
				if(isEQ(sv.agntqualification, SPACES) && !(agpfList.isEmpty()) && (upflag<=agpfList.size())) {
					sv.uprflag.set("S");
				}
				for(Agqfpf a : agpfList) {
					if(isNE(sv.agntqualification, SPACES)) {
						agqfpf = a;
						if(a.getQualification().trim().equals(sv.agntqualification.trim())) {
							if(a.getLicence().trim().equals(sv.liceno.trim())
									&& a.getSrdate() == (sv.startdte.toInt())
									&& a.getDateend() == (sv.enddte.toInt())) {
								sv.uprflag.set("N");
								break;
							}
							else {
								sv.uprflag.set("M");
								break;
							}
						}
						else if(!a.getQualification().trim().equals(sv.agntqualification.trim()) && isEQ(sv.uprflag.trim(), "N")) {
							sv.uprflag.set("M");
						}
						else {
						if( !(agQualSet.contains(sv.agntqualification.trim()))) {
								sv.uprflag.set("A");
								break;
							}
						}
					}
				}
				if(sv.uprflag.equals("A")) {
					agqfTemp = new Agqfpf();
					agqfTemp.setAgntcoy(wsspcomn.company.toString());
					agqfTemp.setAgntnum(sv.agnum.toString());
					agqfTemp.setQualification(sv.agntqualification.toString());
					agqfTemp.setLicence(sv.liceno.toString());
					agqfTemp.setSrdate(sv.startdte.toInt());
					agqfTemp.setDateend(sv.enddte.toInt());
					agqfTemp.setValidflag("1");
					insertAgqfpfList.add(agqfTemp);
				}
				
				if(sv.uprflag.equals("M")) {
					Agqfpf tempAgqfpf = new Agqfpf(agqfpf);
					tempAgqfpf.setAgntcoy(wsspcomn.company.toString());
					tempAgqfpf.setAgntnum(sv.agnum.toString());
					tempAgqfpf.setUnique_number(tempAgqfpf.getUnique_number());
					boolean var = agqfpfDAO.updateAgqfpf(tempAgqfpf);
					if(var) {
						tempAgqfpf.setAgntcoy(wsspcomn.company.toString());
						tempAgqfpf.setAgntnum(sv.agnum.toString());
						tempAgqfpf.setQualification(sv.agntqualification.toString());
						tempAgqfpf.setLicence(sv.liceno.toString());
						tempAgqfpf.setSrdate(sv.startdte.toInt());
						tempAgqfpf.setDateend(sv.enddte.toInt());
						tempAgqfpf.setValidflag("1");
						insertAgqfpfList.add(tempAgqfpf);
					}
				}
				if(sv.uprflag.equals("S")) {
					Agqfpf tempAgqfpf = new Agqfpf();
					tempAgqfpf.setAgntcoy(agpfList.get((upflag-1)).getAgntcoy());
					tempAgqfpf.setAgntnum(agpfList.get((upflag-1)).getAgntnum());
					tempAgqfpf.setUnique_number(agpfList.get(upflag-1).getUnique_number());
					boolean var = agqfpfDAO.updateAgqfpf(tempAgqfpf);
				}
			}
		}
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		agqfpfDAO.insertAgqfpfList(insertAgqfpfList);
		agntlag = agntpfDAO.getAgntRec(wsspcomn.company.toString(), sv.agnum.toString());
		aglfIO.setAgntnum(sv.agnum.toString());
		aglfIO.setFunction(varcom.keeps);
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntlagIO.setAgntnum(sv.agnum.toString());
		agntlagIO.setFunction(varcom.keeps);
		agntlagIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
	
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	}
			
}
