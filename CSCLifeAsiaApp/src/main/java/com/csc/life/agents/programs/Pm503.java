/*
 * File: Pm503.java
 * Date: 30 August 2009 1:12:40
 * Author: Quipoz Limited
 * 
 * Class transformed from PM503.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglfrptTableDAM;
import com.csc.life.agents.screens.Sm503ScreenVars;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.statistics.dataaccess.MaprTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.tablestructures.T1698rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* The remarks section should detail the main processes of the
* program, and any special functions.
*
***********************************************************************
* </pre>
*/
public class Pm503 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM503");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaPeriodFound = new FixedLengthStringData(1);
	private Validator periodFound = new Validator(wsaaPeriodFound, "Y");
	private ZonedDecimalData wsaaMnth = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData wsaaFinYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaFinYtdPer = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalYtdPer = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPolcount = new PackedDecimalData(6, 0);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTotalAg = new ZonedDecimalData(6, 0).setUnsigned();

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaEndPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaEndYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaEndPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaEndMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEndPeriod, 4).setUnsigned();

	private FixedLengthStringData wsaaComPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaComYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaComPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaComMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaComPeriod, 4).setUnsigned();
	private ZonedDecimalData wsaaCalYr = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaBrkDaten = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaBrkDate = new FixedLengthStringData(8).isAPartOf(wsaaBrkDaten, 0, REDEFINE);
	private ZonedDecimalData wsaaBrkYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaBrkDate, 0).setUnsigned();
	private ZonedDecimalData wsaaBrkMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaBrkDate, 4).setUnsigned();
	private static final String h926 = "H926";
	private static final String g876 = "G876";
		/* TABLES */
	private static final String t3692 = "T3692";
	private static final String t1692 = "T1692";
	private static final String t1698 = "T1698";
	private static final String maprrec = "MAPRREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Logical File Layout*/
	private AglfrptTableDAM aglfrptIO = new AglfrptTableDAM();
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agency Production*/
	private MaprTableDAM maprIO = new MaprTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private T1698rec t1698rec = new T1698rec();
	private Sm503ScreenVars sv = ScreenProgram.getScreenVars( Sm503ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readItemFile2250, 
		exit2250, 
		readItemFile2260, 
		exit2260
	}

	public Pm503() {
		super();
		screenVars = sv;
		new ScreenModel("Sm503", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		/*    Dummy field initilisation for prototype version.*/
		sv.countpol.set(ZERO);
		sv.dteapp.set(varcom.maxdate);
		sv.mnth.set(ZERO);
		sv.acctyr.set(ZERO);
		sv.mldirpp01.set(ZERO);
		sv.mldirpp02.set(ZERO);
		sv.mldirpp03.set(ZERO);
		sv.mldirpp04.set(ZERO);
		sv.mldirpp05.set(ZERO);
		sv.mldirpp06.set(ZERO);
		sv.mldirpp07.set(ZERO);
		sv.mldirpp08.set(ZERO);
		sv.mldirpp09.set(ZERO);
		sv.mldirpp10.set(ZERO);
		sv.mlperpp01.set(ZERO);
		sv.mlperpp02.set(ZERO);
		sv.mlperpp03.set(ZERO);
		sv.mlperpp04.set(ZERO);
		sv.mlperpp05.set(ZERO);
		sv.mlperpp06.set(ZERO);
		sv.mlperpp07.set(ZERO);
		sv.mlperpp08.set(ZERO);
		sv.mlperpp09.set(ZERO);
		sv.mlperpp10.set(ZERO);
		sv.mlgrppp01.set(ZERO);
		sv.mlgrppp02.set(ZERO);
		sv.mlgrppp03.set(ZERO);
		sv.mlgrppp04.set(ZERO);
		sv.mlgrppp05.set(ZERO);
		sv.mlgrppp06.set(ZERO);
		sv.mlgrppp07.set(ZERO);
		sv.mlgrppp08.set(ZERO);
		sv.mlgrppp09.set(ZERO);
		sv.mlgrppp10.set(ZERO);
		wsaaYear.set(ZERO);
		wsaaMnth.set(ZERO);
		sv.mlytdperca.set(ZERO);
		sv.mlytdgrpca.set(ZERO);
		sv.mlytdperfn.set(ZERO);
		sv.mlytdgrpfn.set(ZERO);
		sv.mnth.set(wsspcomn.acctmonth);
		sv.acctyr.set(wsspcomn.acctyear);
		aglfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		sv.agntnum.set(aglfIO.getAgntnum());
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(aglfIO.getAgntcoy());
		agntIO.setAgntnum(aglfIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		/* MOVE 'E'                      TO NMAD-LANGUAGE.              */
		namadrsrec.function.set("LGNMS");
		namadrsrec.language.set(wsspcomn.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		if (isEQ(namadrsrec.statuz,varcom.oK)) {
			sv.agtname.set(namadrsrec.name);
		}
		else {
			sv.agtname.fill("?");
		}
		sv.agntbr.set(agntIO.getAgntbr());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(sv.agntbr);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip02.set(descIO.getLongdesc());
		}
		else {
			sv.descrip02.set(SPACES);
		}
		sv.agtype.set(agntIO.getAgtype());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		/* MOVE TM602                    TO DESC-DESCTABL.              */
		descIO.setDesctabl(t3692);
		descIO.setDescitem(sv.agtype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.descrip01.set(descIO.getLongdesc());
		}
		else {
			sv.descrip01.set(SPACES);
		}
		sv.dteapp.set(aglfIO.getDteapp());
		wsaaBatckey.set(wsspcomn.batchkey);
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		/*    CALL 'SM503IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SM503-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(sv.mnth,ZERO)) {
			/*      OR SM503-ACCTYR            = ZEROES*/
			sv.mnthErr.set(h926);
			wsspcomn.edterror.set("Y");
		}
		wsaaEndYr.set(sv.acctyr);
		wsaaEndMth.set(sv.mnth);
		/* IF SM503-MNTH            NOT = WSAA-MNTH                     */
		/*  OR SM503-ACCTYR         NOT = WSAA-YEAR                     */
		/*     MOVE 'Y'                TO WSSP-EDTERROR                 */
		if (isNE(sv.mnth,wsaaMnth)
		|| isNE(sv.acctyr,wsaaYear)) {
			if (isNE(scrnparams.deviceInd,"*RMT")) {
				wsspcomn.edterror.set("Y");
			}
			wsaaFinYtd.set(ZERO);
			wsaaCalYtd.set(ZERO);
			wsaaFinYtdPer.set(ZERO);
			wsaaCalYtdPer.set(ZERO);
			wsaaPolcount.set(ZERO);
			wsaaCalYr.set(ZERO);
			wsaaMnth.set(sv.mnth);
			wsaaYear.set(sv.acctyr);
			getDetails2100();
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void getDetails2100()
	{
		start2100();
		continue2100();
	}

protected void start2100()
	{
		sv.mlperpp01.set(ZERO);
		sv.mlperpp02.set(ZERO);
		sv.mlperpp03.set(ZERO);
		sv.mlperpp04.set(ZERO);
		sv.mlperpp05.set(ZERO);
		sv.mlperpp06.set(ZERO);
		sv.mlperpp07.set(ZERO);
		sv.mlperpp08.set(ZERO);
		sv.mlperpp09.set(ZERO);
		sv.mlperpp10.set(ZERO);
		sv.mlgrppp01.set(ZERO);
		sv.mlgrppp02.set(ZERO);
		sv.mlgrppp03.set(ZERO);
		sv.mlgrppp04.set(ZERO);
		sv.mlgrppp05.set(ZERO);
		sv.mlgrppp06.set(ZERO);
		sv.mlgrppp07.set(ZERO);
		sv.mlgrppp08.set(ZERO);
		sv.mlgrppp09.set(ZERO);
		sv.mlgrppp10.set(ZERO);
		sv.mldirpp01.set(ZERO);
		sv.mldirpp02.set(ZERO);
		sv.mldirpp03.set(ZERO);
		sv.mldirpp04.set(ZERO);
		sv.mldirpp05.set(ZERO);
		sv.mldirpp06.set(ZERO);
		sv.mldirpp07.set(ZERO);
		sv.mldirpp08.set(ZERO);
		sv.mldirpp09.set(ZERO);
		sv.mldirpp10.set(ZERO);
		maprIO.setRecKeyData(SPACES);
		maprIO.setStatuz(varcom.oK);
		maprIO.setAgntcoy(aglfIO.getAgntcoy());
		maprIO.setAgntnum(sv.agntnum);
		maprIO.setAcctyr(sv.acctyr);
		maprIO.setMnth(sv.mnth);
		maprIO.setFormat(maprrec);
		maprIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		maprIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		maprIO.setFitKeysSearch("AGNTCOY", "AGNTNUM", "ACCTYR", "MNTH");

		while ( !(isEQ(maprIO.getStatuz(),varcom.endp))) {
			accumulateProduction2050();
		}
		
	}

protected void continue2100()
	{
		maprIO.setRecKeyData(SPACES);
		maprIO.setStatuz(varcom.oK);
		maprIO.setAgntcoy(aglfIO.getAgntcoy());
		maprIO.setAgntnum(sv.agntnum);
		maprIO.setAcctyr(sv.acctyr);
		maprIO.setMnth(1);
		maprIO.setFormat(maprrec);
		maprIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		maprIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		maprIO.setFitKeysSearch("AGNTCOY", "AGNTNUM", "ACCTYR");


		while ( !(isEQ(maprIO.getStatuz(),varcom.endp))) {
			financialYear2200();
		}
		
		sv.mlytdperfn.set(wsaaFinYtdPer);
		sv.mlytdgrpfn.set(wsaaFinYtd);
		/* To get the calendar YTD production*/
		/*    First, map the Accounting Period to the Calendar Period*/
		mapToCalendar2250();
		if (periodFound.isTrue()) {
			/*       Now map start Calendar Period to get start Accounting Peri*/
			wsaaBrkDaten.set(t1698rec.frmdate[wsaaIndex.toInt()]);
			wsaaCalYr.set(wsaaBrkYr);
			mapToAccounting2260();
			if (periodFound.isTrue()) {
				maprIO.setRecKeyData(SPACES);
				maprIO.setAgntcoy(aglfIO.getAgntcoy());
				maprIO.setAgntnum(sv.agntnum);
				maprIO.setAcctyr(t1698rec.acctyr[wsaaIndex.toInt()]);
				maprIO.setMnth(t1698rec.acctmnth[wsaaIndex.toInt()]);
				maprIO.setStatuz(varcom.oK);
				maprIO.setFormat(maprrec);
				maprIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				maprIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				maprIO.setFitKeysSearch("AGNTCOY", "AGNTNUM");

				while ( !(isEQ(maprIO.getStatuz(),varcom.endp))) {
					calendarYear2300();
				}
				
			}
		}
		sv.mlytdperca.set(wsaaCalYtdPer);
		sv.mlytdgrpca.set(wsaaCalYtd);
		maprIO.setRecKeyData(SPACES);
		maprIO.setStatuz(varcom.oK);
		maprIO.setAgntcoy(aglfIO.getAgntcoy());
		maprIO.setAgntnum(aglfIO.getAgntnum());
		maprIO.setAcctyr(sv.acctyr);
		maprIO.setMnth(sv.mnth);
		maprIO.setCnttype(SPACES);
		maprIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		maprIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		maprIO.setFitKeysSearch("AGNTCOY", "AGNTNUM", "ACCTYR", "MNTH");
		maprIO.setFormat(maprrec);
		while ( !(isEQ(maprIO.getStatuz(),varcom.endp))) {
			getPolcount2400();
		}
		
		sv.countpol.set(wsaaPolcount);
	}

protected void accumulateProduction2050()
	{
			start2050();
		}

protected void start2050()
	{
		SmartFileCode.execute(appVars, maprIO);
		if (isNE(maprIO.getStatuz(),varcom.oK)
		&& isNE(maprIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(maprIO.getStatuz());
			syserrrec.params.set(maprIO.getParams());
			fatalError600();
		}
		if (isNE(maprIO.getAgntnum(),sv.agntnum)
		|| isNE(maprIO.getAgntcoy(),aglfIO.getAgntcoy())
		|| isNE(maprIO.getAcctyr(),sv.acctyr)
		|| isNE(maprIO.getMnth(),sv.mnth)) {
			maprIO.setStatuz(varcom.endp);
		}
		if (isEQ(maprIO.getStatuz(),varcom.endp)) {
			return ;
		}
		sv.mlperpp01.add(maprIO.getMlperpp01());
		sv.mlperpp02.add(maprIO.getMlperpp02());
		sv.mlperpp03.add(maprIO.getMlperpp03());
		sv.mlperpp04.add(maprIO.getMlperpp04());
		sv.mlperpp05.add(maprIO.getMlperpp05());
		sv.mlperpp06.add(maprIO.getMlperpp06());
		sv.mlperpp07.add(maprIO.getMlperpp07());
		sv.mlperpp08.add(maprIO.getMlperpp08());
		sv.mlperpp09.add(maprIO.getMlperpp09());
		sv.mlperpp10.add(maprIO.getMlperpp10());
		/*sv.mlgrppp01.add(maprIO.getMlgrppp01());
		sv.mlgrppp02.add(maprIO.getMlgrppp02());
		sv.mlgrppp03.add(maprIO.getMlgrppp03());
		sv.mlgrppp04.add(maprIO.getMlgrppp04());
		sv.mlgrppp05.add(maprIO.getMlgrppp05());
		sv.mlgrppp06.add(maprIO.getMlgrppp06());
		sv.mlgrppp07.add(maprIO.getMlgrppp07());
		sv.mlgrppp08.add(maprIO.getMlgrppp08());
		sv.mlgrppp09.add(maprIO.getMlgrppp09());
		sv.mlgrppp10.add(maprIO.getMlgrppp10());
		sv.mldirpp01.add(maprIO.getMldirpp01());
		sv.mldirpp02.add(maprIO.getMldirpp02());
		sv.mldirpp03.add(maprIO.getMldirpp03());
		sv.mldirpp04.add(maprIO.getMldirpp04());
		sv.mldirpp05.add(maprIO.getMldirpp05());
		sv.mldirpp06.add(maprIO.getMldirpp06());
		sv.mldirpp07.add(maprIO.getMldirpp07());
		sv.mldirpp08.add(maprIO.getMldirpp08());
		sv.mldirpp09.add(maprIO.getMldirpp09());
		sv.mldirpp10.add(maprIO.getMldirpp10());*/
		maprIO.setFunction(varcom.nextr);
	}

protected void financialYear2200()
	{
			start2200();
		}

protected void start2200()
	{
		SmartFileCode.execute(appVars, maprIO);
		if (isNE(maprIO.getStatuz(),varcom.oK)
		&& isNE(maprIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(maprIO.getStatuz());
			syserrrec.params.set(maprIO.getStatuz());
			fatalError600();
		}
		if (isNE(maprIO.getAcctyr(),sv.acctyr)
		|| isGT(maprIO.getMnth(),sv.mnth)
		|| isNE(maprIO.getAgntnum(),sv.agntnum)
		|| isNE(maprIO.getAgntcoy(),aglfIO.getAgntcoy())) {
			maprIO.setStatuz(varcom.endp);
		}
		if (isEQ(maprIO.getStatuz(),varcom.endp)) {
			return ;
		}
		int count=0;
		String effyr=maprIO.getEffdate().toString().substring(0, maprIO.getEffdate().toString().length()-4);
		count=maprIO.getAcctyr().toInt()-Integer.parseInt(effyr)+1;
		
		if(count==1)
		{
		/*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp01()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp01()));
		}
		if(count==2)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp02()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp02()));
		}
		if(count==3)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp03()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp03()));
		}
		if(count==4)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp04()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp04()));
		}
		if(count==5)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp05()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp05()));
		}
		if(count==6)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp06()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp06()));
		}
		if(count==7)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp07()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp07()));
		}
		if(count==8)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp08()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp08()));
		}
		if(count==9)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp09()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp09()));
		}
		if(count==10)
		{
	    /*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp10()));*/
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp10()));
		}
		
		/*compute(wsaaFinYtd, 2).set(add(wsaaFinYtd,maprIO.getMlgrppp01()));
		compute(wsaaFinYtdPer, 2).set(add(wsaaFinYtdPer,maprIO.getMlperpp01()));*/
		maprIO.setFunction(varcom.nextr);
	}

protected void mapToCalendar2250()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loadItemRecord2250();
				case readItemFile2250: 
					readItemFile2250();
					searchForPeriod2250();
				case exit2250: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadItemRecord2250()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t1698);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
	}

protected void readItemFile2250()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		|| isNE(itemIO.getValidflag(),1)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1698rec.t1698Rec.set(itemIO.getGenarea());
		wsaaPeriodFound.set("N");
		wsaaIndex.set(1);
	}

protected void searchForPeriod2250()
	{
		while ( !(isGT(wsaaIndex,13)
		|| periodFound.isTrue())) {
			searchTable2250();
		}
		
		if (!periodFound.isTrue()) {
			if (isNE(t1698rec.contitem,SPACES)) {
				itemIO.setItemitem(t1698rec.contitem);
				goTo(GotoLabel.readItemFile2250);
			}
			else {
				sv.acctyrErr.set(g876);
				sv.mnthErr.set(g876);
			}
		}
		goTo(GotoLabel.exit2250);
	}

protected void searchTable2250()
	{
		if (isEQ(t1698rec.acctmnth[wsaaIndex.toInt()],sv.mnth)
		&& isEQ(t1698rec.acctyr[wsaaIndex.toInt()],sv.acctyr)) {
			wsaaPeriodFound.set("Y");
		}
		else {
			wsaaIndex.add(1);
		}
	}

protected void mapToAccounting2260()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loadItemRecord2260();
				case readItemFile2260: 
					readItemFile2260();
					searchForPeriod2260();
				case exit2260: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadItemRecord2260()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t1698);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
	}

protected void readItemFile2260()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		|| isNE(itemIO.getValidflag(),1)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t1698rec.t1698Rec.set(itemIO.getGenarea());
		wsaaPeriodFound.set("N");
		wsaaIndex.set(1);
	}

protected void searchForPeriod2260()
	{
		while ( !(isGT(wsaaIndex,13)
		|| periodFound.isTrue())) {
			searchTable2260();
		}
		
		if (!periodFound.isTrue()) {
			if (isNE(t1698rec.contitem,SPACES)) {
				itemIO.setItemitem(t1698rec.contitem);
				goTo(GotoLabel.readItemFile2260);
			}
			else {
				sv.acctyrErr.set(g876);
				sv.mnthErr.set(g876);
			}
		}
		goTo(GotoLabel.exit2260);
	}

protected void searchTable2260()
	{
		wsaaBrkDaten.set(t1698rec.frmdate[wsaaIndex.toInt()]);
		if (isEQ(wsaaBrkMnth,1)
		&& isEQ(wsaaBrkYr,wsaaCalYr)) {
			wsaaPeriodFound.set("Y");
		}
		else {
			wsaaIndex.add(1);
		}
	}

protected void calendarYear2300()
	{
			start2300();
		}

protected void start2300()
	{
		SmartFileCode.execute(appVars, maprIO);
		if (isNE(maprIO.getStatuz(),varcom.oK)
		&& isNE(maprIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(maprIO.getStatuz());
			syserrrec.params.set(maprIO.getParams());
			fatalError600();
		}
		wsaaComYr.set(maprIO.getAcctyr());
		wsaaComMth.set(maprIO.getMnth());
		if (isGT(wsaaComPeriod,wsaaEndPeriod)
		|| isNE(maprIO.getAgntnum(),sv.agntnum)
		|| isNE(maprIO.getAgntcoy(),aglfIO.getAgntcoy())) {
			maprIO.setStatuz(varcom.endp);
		}
		if (isEQ(maprIO.getStatuz(),varcom.endp)) {
			return ;
		}
		int count=0;
		String effyr=maprIO.getEffdate().toString().substring(0, maprIO.getEffdate().toString().length()-4);
		count=maprIO.getAcctyr().toInt()-Integer.parseInt(effyr)+1;
		
		if(count==1)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp01()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp01()));
		}
		if(count==2)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp02()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp02()));
		}
		if(count==3)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp03()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp03()));
		}
		if(count==4)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp04()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp04()));
		}
		if(count==5)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp05()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp05()));
		}
		if(count==6)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp06()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp06()));
		}
		if(count==7)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp07()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp07()));
		}
		if(count==8)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp08()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp08()));
		}
		if(count==9)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp09()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp09()));
		}
		if(count==10)
		{
			/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp10()));*/
			compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp10()));
		}
		
		/*compute(wsaaCalYtd, 2).set(add(wsaaCalYtd,maprIO.getMlgrppp01()));
		compute(wsaaCalYtdPer, 2).set(add(wsaaCalYtdPer,maprIO.getMlperpp01()));*/
		maprIO.setFunction(varcom.nextr);
	}

protected void getPolcount2400()
	{
			start2400();
		}

protected void start2400()
	{
		SmartFileCode.execute(appVars, maprIO);
		if (isNE(maprIO.getStatuz(),varcom.oK)
		&& isNE(maprIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(maprIO.getStatuz());
			syserrrec.params.set(maprIO.getParams());
			fatalError600();
		}
		if (isNE(maprIO.getAgntnum(),aglfIO.getAgntnum())
		|| isNE(maprIO.getAgntcoy(),aglfIO.getAgntcoy())
		|| isNE(maprIO.getAcctyr(),sv.acctyr)
		|| isNE(maprIO.getMnth(),sv.mnth)) {
			maprIO.setStatuz(varcom.endp);
		}
		if (isEQ(maprIO.getStatuz(),varcom.endp)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2400-EXIT                                           */
			return ;
		}
		wsaaPolcount.add(1);
		maprIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required / WSSP*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
