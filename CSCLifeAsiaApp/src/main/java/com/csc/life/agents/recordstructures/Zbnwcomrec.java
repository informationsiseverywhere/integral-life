package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:11
 * Description:
 * Copybook name: ZBNWCOMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwcomrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwcomRec = new FixedLengthStringData(438);
  	public FixedLengthStringData bnwcomTrcde = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 0);
  	public FixedLengthStringData bnwcomSource = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 3);
  	public FixedLengthStringData bnwcomPagntnum = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 13);
  	public FixedLengthStringData bnwcomPprofile = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 23);
  	public FixedLengthStringData bnwcomPlevel = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 26);
  	public FixedLengthStringData bnwcomWagntnum = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 29);
  	public FixedLengthStringData bnwcomWagntrole = new FixedLengthStringData(2).isAPartOf(bnwcomRec, 39);
  	public FixedLengthStringData bnwcomWprofile = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 41);
  	public FixedLengthStringData bnwcomGrpcode = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 44);
  	public FixedLengthStringData bnwcomCompany = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 47);
  	public FixedLengthStringData bnwcomAgency = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 50);
  	public FixedLengthStringData bnwcomCrtable = new FixedLengthStringData(50).isAPartOf(bnwcomRec, 60);
  	public FixedLengthStringData bnwcomChdrnum = new FixedLengthStringData(30).isAPartOf(bnwcomRec, 110);
  	public FixedLengthStringData bnwcomPlan = new FixedLengthStringData(30).isAPartOf(bnwcomRec, 140);
  	public FixedLengthStringData bnwcomPlanid = new FixedLengthStringData(15).isAPartOf(bnwcomRec, 170);
  	public FixedLengthStringData bnwcomPlandesc = new FixedLengthStringData(80).isAPartOf(bnwcomRec, 185);
  	public FixedLengthStringData bnwcomPlandate = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 265);
  	public FixedLengthStringData bnwcomTotprem = new FixedLengthStringData(12).isAPartOf(bnwcomRec, 275);
  	public FixedLengthStringData bnwcomEffdate = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 287);
  	public FixedLengthStringData bnwcomTrmdate = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 297);
  	public FixedLengthStringData bnwcomAmount = new FixedLengthStringData(13).isAPartOf(bnwcomRec, 307);
  	public FixedLengthStringData bnwcomCmsntype = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 320);
  	public FixedLengthStringData bnwcomCmsnid = new FixedLengthStringData(11).isAPartOf(bnwcomRec, 330);
  	public FixedLengthStringData bnwcomCmsnprnt = new FixedLengthStringData(6).isAPartOf(bnwcomRec, 341);
  	public FixedLengthStringData bnwcomCmsnrate = new FixedLengthStringData(6).isAPartOf(bnwcomRec, 347);
  	public FixedLengthStringData bnwcomPrdprnt = new FixedLengthStringData(6).isAPartOf(bnwcomRec, 353);
  	public FixedLengthStringData bnwcomFund = new FixedLengthStringData(3).isAPartOf(bnwcomRec, 359);
  	public FixedLengthStringData bnwcomRegion = new FixedLengthStringData(5).isAPartOf(bnwcomRec, 362);
  	public FixedLengthStringData bnwcomInd = new FixedLengthStringData(1).isAPartOf(bnwcomRec, 367);
  	public FixedLengthStringData bnwcomPartner = new FixedLengthStringData(20).isAPartOf(bnwcomRec, 368);
  	public FixedLengthStringData bnwcomClrhouse = new FixedLengthStringData(30).isAPartOf(bnwcomRec, 388);
  	public FixedLengthStringData bnwcomTrandate = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 418);
  	public FixedLengthStringData bnwcomProcdate = new FixedLengthStringData(10).isAPartOf(bnwcomRec, 428);


	public void initialize() {
		COBOLFunctions.initialize(bnwcomRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwcomRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}