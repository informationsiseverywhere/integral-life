/*
 * File: Pr560.java
 * Date: 30 August 2009 1:41:11
 * Author: Quipoz Limited
 * 
 * Class transformed from PR560.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.AgorTableDAM;
import com.csc.life.agents.screens.Sr560ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
* OR DETAILS SCREEN - This is a mandatory field requires to be
*                      entered upon creation of any  new  agent
*                      record. It contains relevant information
*                      required to generate OR payment
*
*****************************************************************
* </pre>
*/
public class Pr560 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR560");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaDteapp = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaZrepto = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
		/* ERRORS */
	private String e186 = "E186";
	private String e050 = "E050";
	private String e305 = "E305";
		/* TABLES */
	private String t3692 = "T3692";
		/* FORMATS */
	private String aglfrec = "AGLFREC";
	private String agorrec = "AGORREC";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Agent override details*/
	private AgorTableDAM agorIO = new AgorTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr560ScreenVars sv = ScreenProgram.getScreenVars( Sr560ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		sadd1285, 
		exit1290, 
		preExit, 
		updateErrorIndicators2170, 
		exit3090, 
		exit3190, 
		readNextModifiedRecord3280
	}

	public Pr560() {
		super();
		screenVars = sv;
		new ScreenModel("Sr560", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1005();
		initialise1010();
		retrvAglflnb1020();
		readrAgentFile1030();
		clientDets1040();
		agentType1060();
		subfileProcess1050();
	}

protected void initialise1005()
	{
		wsaaDteapp.set(varcom.vrcmMaxDate);
		wsaaZrepto.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void retrvAglflnb1020()
	{
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isNE(aglfIO.getEffdate(),NUMERIC)) {
			aglfIO.setEffdate(varcom.vrcmMaxDate);
		}
	}

protected void readrAgentFile1030()
	{
		sv.agnum.set(aglfIO.getAgntnum());
		agntlagIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

protected void clientDets1040()
	{
		sv.clntsel.set(agntlagIO.getClntnum());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void agentType1060()
	{
		sv.agntype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntlagIO.getAgtype());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.agtydesc.set(descIO.getLongdesc());
	}

protected void subfileProcess1050()
	{
		agorIO.setDataArea(SPACES);
		agorIO.setAgntnum(aglfIO.getAgntnum());
		agorIO.setAgntcoy(wsspcomn.company);
		//performance improvement -- Anjali
		agorIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		agorIO.setFunction(varcom.begn);
		scrnparams.subfileRrn.set(1);
		wsaaCount.set(ZERO);
		while ( !(isGT(wsaaCount,20))) {
			loadSubfile1200();
		}
		
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			scrnparams.function.set(varcom.prot);
		}
		/*EXIT*/
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1210();
				}
				case sadd1285: {
					sadd1285();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1210()
	{
		if (isEQ(wsaaCount,20)) {
			wsaaCount.set(30);
			goTo(GotoLabel.exit1290);
		}
		SmartFileCode.execute(appVars, agorIO);
		if (isNE(agorIO.getStatuz(),varcom.oK)
		&& isNE(agorIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agorIO.getParams());
			syserrrec.statuz.set(agorIO.getStatuz());
			fatalError600();
		}
		if (isNE(agorIO.getAgntcoy(),wsspcomn.company)
		|| isNE(agorIO.getAgntnum(),aglfIO.getAgntnum())
		|| isEQ(agorIO.getStatuz(),varcom.endp)) {
			agorIO.setStatuz(varcom.endp);
			if (isEQ(agorIO.getFunction(),varcom.begn)) {
				sv.dteapp01.set(aglfIO.getDteapp());
				sv.dteapp02.set(varcom.vrcmMaxDate);
				sv.zrreptp.set(aglfIO.getAgntnum());
				sv.desc.set(SPACES);
				sv.agtype01.set(agntlagIO.getAgtype());
				sv.agtype02.set(agntlagIO.getAgtype());
				if (isNE(aglfIO.getReportag(),SPACES)) {
					sv.zrreptp.set(aglfIO.getReportag());
					agntlagIO.setAgntnum(aglfIO.getReportag());
					agntlagioCall2300();
					sv.agtype02.set(agntlagIO.getAgtype());
				}
			}
			else {
				sv.dteapp01.set(varcom.vrcmMaxDate);
				sv.dteapp02.set(varcom.vrcmMaxDate);
				sv.zrreptp.set(SPACES);
				sv.desc.set(SPACES);
				sv.agtype01.set(SPACES);
				sv.agtype02.set(SPACES);
			}
			goTo(GotoLabel.sadd1285);
		}
		sv.dteapp01.set(agorIO.getEffdate01());
		sv.dteapp02.set(agorIO.getEffdate02());
		sv.zrreptp.set(agorIO.getReportag());
		sv.agtype01.set(agorIO.getAgtype01());
		sv.agtype02.set(agorIO.getAgtype02());
		sv.desc.set(agorIO.getDesc());
		if (isEQ(sv.desc,SPACES)) {
			agntlagIO.setAgntnum(agorIO.getReportag());
			agntlagioCall2300();
			sv.desc.set(wsspcomn.longconfname);
		}
	}

protected void sadd1285()
	{
		scrnparams.function.set(varcom.sadd);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
		agorIO.setFunction(varcom.nextr);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			scrnparams.function.set(varcom.prot);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		scrnparams.function.set(varcom.srnch);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2100();
		}
		
		/*EXIT*/
	}

protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation2110();
				}
				case updateErrorIndicators2170: {
					updateErrorIndicators2170();
					readNextModifiedRecord2180();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2110()
	{
		if (isEQ(sv.dteapp01,varcom.vrcmMaxDate)
		&& isEQ(sv.agtype01,SPACES)
		&& isEQ(sv.agtype02,SPACES)
		&& isEQ(sv.zrreptp,SPACES)) {
			sv.desc.set(SPACES);
			goTo(GotoLabel.updateErrorIndicators2170);
		}
		if (isGT(sv.dteapp01,sv.dteapp02)) {
			sv.dteapp02Err.set(e050);
			sv.dteapp01Err.set(e050);
		}
		wsaaZrepto.set(sv.zrreptp);
		wsaaDteapp.set(sv.dteapp01);
		if (isEQ(sv.dteapp01,varcom.vrcmMaxDate)) {
			sv.dteapp01Err.set(e186);
		}
		if (isEQ(sv.zrreptp,SPACES)) {
			sv.desc.set(SPACES);
			sv.zrreptpErr.set(e186);
		}
		else {
			if (isEQ(sv.zrreptp,sv.agnum)) {
				goTo(GotoLabel.updateErrorIndicators2170);
			}
			agntlagIO.setDataKey(SPACES);
			agntlagIO.setAgntnum(sv.zrreptp);
			agntlagioCall2300();
			if (isEQ(agntlagIO.getStatuz(),varcom.mrnf)) {
				sv.zrreptpErr.set(e305);
			}
		}
	}

protected void updateErrorIndicators2170()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2180()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void agntlagioCall2300()
	{
		call2310();
	}

protected void call2310()
	{
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		if (isEQ(agntlagIO.getStatuz(),varcom.oK)) {
			cltsIO.setDataKey(SPACES);
			cltsIO.setClntnum(agntlagIO.getClntnum());
			cltsioCall2500();
			sv.desc.set(wsspcomn.longconfname);
		}
	}

protected void cltsioCall2500()
	{
		/*DESCRIPTION*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			deleteAgorFile3020();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			goTo(GotoLabel.exit3090);
		}
		aglfIO.setZrorcode(SPACES);
		aglfIO.setEffdate(wsaaDteapp);
		aglfIO.setReportag(wsaaZrepto);
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setFunction("KEEPS");
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void deleteAgorFile3020()
	{
		agorIO.setStatuz(varcom.oK);
		while ( !(isNE(agorIO.getStatuz(),varcom.oK))) {
			deleteSubfile3100();
		}
		
		/*UPDATE-AGOR-FILE*/
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR560", sv);
		if ((isNE(scrnparams.statuz,varcom.oK))
		&& (isNE(scrnparams.statuz,varcom.endp))) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			saveSubfile3200();
		}
		
	}

protected void deleteSubfile3100()
	{
		try {
			validation3110();
		}
		catch (GOTOException e){
		}
	}

protected void validation3110()
	{
		agorIO.setDataArea(SPACES);
		agorIO.setAgntnum(aglfIO.getAgntnum());
		agorIO.setAgntcoy(wsspcomn.company);
		agorIO.setFunction(varcom.readh);
		agorIO.setFormat(agorrec);
		SmartFileCode.execute(appVars, agorIO);
		if (isNE(agorIO.getStatuz(),varcom.oK)
		&& isNE(agorIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agorIO.getParams());
			syserrrec.statuz.set(agorIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agorIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3190);
		}
		agorIO.setAgntnum(aglfIO.getAgntnum());
		agorIO.setAgntcoy(wsspcomn.company);
		agorIO.setFunction(varcom.delet);
		agorIO.setFormat(agorrec);
		SmartFileCode.execute(appVars, agorIO);
		if (isNE(agorIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agorIO.getParams());
			syserrrec.statuz.set(agorIO.getStatuz());
			fatalError600();
		}
	}

protected void saveSubfile3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validation3210();
				}
				case readNextModifiedRecord3280: {
					readNextModifiedRecord3280();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation3210()
	{
		if (isEQ(sv.dteapp01,varcom.vrcmMaxDate)
		&& isEQ(sv.dteapp02,varcom.vrcmMaxDate)
		&& isEQ(sv.agtype01,SPACES)
		&& isEQ(sv.agtype02,SPACES)
		&& isEQ(sv.zrreptp,SPACES)) {
			goTo(GotoLabel.readNextModifiedRecord3280);
		}
		agorIO.setDataArea(SPACES);
		agorIO.setEffdate01(sv.dteapp01);
		agorIO.setEffdate02(sv.dteapp02);
		agorIO.setAgtype01(sv.agtype01);
		agorIO.setAgtype02(sv.agtype02);
		agorIO.setReportag(sv.zrreptp);
		agorIO.setDesc(sv.desc);
		agorIO.setAgntnum(aglfIO.getAgntnum());
		agorIO.setAgntcoy(wsspcomn.company);
		agorIO.setFunction(varcom.writr);
		agorIO.setFormat(agorrec);
		SmartFileCode.execute(appVars, agorIO);
		if (isNE(agorIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agorIO.getParams());
			syserrrec.statuz.set(agorIO.getStatuz());
			fatalError600();
		}
	}

protected void readNextModifiedRecord3280()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR560", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
