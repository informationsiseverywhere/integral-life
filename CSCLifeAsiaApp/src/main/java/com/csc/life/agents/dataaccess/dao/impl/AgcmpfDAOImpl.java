package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.AgcmpfDAO;
import com.csc.life.agents.dataaccess.model.Agcmpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgcmpfDAOImpl extends BaseDAOImpl<Agcmpf> implements AgcmpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(AgcmpfDAOImpl.class);
	
	public List<Agcmpf> searchAgcmdmnRecord(String chdrcoy,String chdrnum, String agntnum) throws SQLRuntimeException{
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("select * from AGCMPF WHERE CHDRCOY=? and CHDRNUM=? and AGNTNUM=? and OVRDCAT<>'O'  " + 
				"order by CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER ASC ");
		
		PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		ResultSet rs = null;
		List<Agcmpf> outputList = new ArrayList<Agcmpf>();
		Agcmpf agcmpf = null;
		try {
		ps.setString(1, chdrcoy);
		ps.setString(2, chdrnum);
		ps.setString(3, agntnum);
		
		rs = executeQuery(ps);
		while (rs.next()) {
			agcmpf = new Agcmpf();
			agcmpf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
			agcmpf.setChdrcoy(rs.getString("CHDRCOY"));
			agcmpf.setChdrnum(rs.getString("CHDRNUM"));
			agcmpf.setLife(rs.getString("LIFE"));
			agcmpf.setCoverage(rs.getString("COVERAGE"));
			agcmpf.setRider(rs.getString("RIDER"));
			agcmpf.setPlnsfx(rs.getInt("PLNSFX"));
			agcmpf.setTranno(rs.getInt("TRANNO"));
			agcmpf.setAgntnum(rs.getString("AGNTNUM"));
			agcmpf.setEfdate(rs.getInt("EFDATE"));
			agcmpf.setAnnprem(rs.getBigDecimal("ANNPREM"));
			agcmpf.setBascmeth(rs.getString("BASCMETH"));
			agcmpf.setInitcom(rs.getBigDecimal("INITCOM"));
			agcmpf.setBascpy(rs.getString("BASCPY"));
			agcmpf.setCompay(rs.getBigDecimal("COMPAY"));
			agcmpf.setComern(rs.getBigDecimal("COMERN"));
			agcmpf.setSrvcpy(rs.getString("SRVCPY"));
			agcmpf.setScmdue(rs.getBigDecimal("SCMDUE"));
			agcmpf.setScmearn(rs.getBigDecimal("SCMEARN"));
			agcmpf.setRnwcpy(rs.getString("RNWCPY"));
			agcmpf.setRnlcdue(rs.getBigDecimal("RNLCDUE"));
			agcmpf.setRnlcearn(rs.getBigDecimal("RNLCEARN"));
			agcmpf.setAgcls(rs.getString("AGCLS"));
			agcmpf.setTermid(rs.getString("TERMID"));
			agcmpf.setTrdt(rs.getInt("TRDT"));
			agcmpf.setUser_t(rs.getInt("USER_T"));
			agcmpf.setValidflag(rs.getString("VALIDFLAG"));
			agcmpf.setCurrfrom(rs.getInt("CURRFROM"));
			agcmpf.setCurrto(rs.getInt("CURRTO"));
			agcmpf.setPtdate(rs.getInt("PTDATE"));
			agcmpf.setSeqno(rs.getInt("SEQNO"));
			agcmpf.setCedagent(rs.getString("CEDAGENT"));
			agcmpf.setOvrdcat(rs.getString("OVRDCAT"));
			agcmpf.setDormflag(rs.getString("DORMFLAG"));
			agcmpf.setUsrprf(rs.getString("USRPRF"));
			agcmpf.setJobnm(rs.getString("JOBNM"));
			agcmpf.setDatim(rs.getDate("DATIME"));
			
			outputList.add(agcmpf);
		}
		}catch (SQLException e) {
			LOGGER.error("searchAgcmdmnRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return outputList;
	}
	
	public List<Agcmpf> searchAgcmdmnRecord(String chdrcoy,String chdrnum) throws SQLRuntimeException{
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("select * from AGCMPF WHERE CHDRCOY=? and CHDRNUM=? and VALIDFLAG = '1' and  DORMFLAG != 'Y' and OVRDCAT<>'O' "); 
		sqlSelect.append("order by CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER ASC ");
		
		PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		ResultSet rs = null;
		List<Agcmpf> outputList = new ArrayList<Agcmpf>();
		Agcmpf agcmpf = null;
		try {
		ps.setString(1, chdrcoy);
		ps.setString(2, chdrnum);
		
		rs = executeQuery(ps);
		while (rs.next()) {
			agcmpf = new Agcmpf();
			agcmpf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
			agcmpf.setChdrcoy(rs.getString("CHDRCOY"));
			agcmpf.setChdrnum(rs.getString("CHDRNUM"));
			agcmpf.setLife(rs.getString("LIFE"));
			agcmpf.setCoverage(rs.getString("COVERAGE"));
			agcmpf.setRider(rs.getString("RIDER"));
			agcmpf.setPlnsfx(rs.getInt("PLNSFX"));
			agcmpf.setTranno(rs.getInt("TRANNO"));
			agcmpf.setAgntnum(rs.getString("AGNTNUM"));
			agcmpf.setEfdate(rs.getInt("EFDATE"));
			agcmpf.setAnnprem(rs.getBigDecimal("ANNPREM"));
			agcmpf.setBascmeth(rs.getString("BASCMETH"));
			agcmpf.setInitcom(rs.getBigDecimal("INITCOM"));
			agcmpf.setBascpy(rs.getString("BASCPY"));
			agcmpf.setCompay(rs.getBigDecimal("COMPAY"));
			agcmpf.setComern(rs.getBigDecimal("COMERN"));
			agcmpf.setSrvcpy(rs.getString("SRVCPY"));
			agcmpf.setScmdue(rs.getBigDecimal("SCMDUE"));
			agcmpf.setScmearn(rs.getBigDecimal("SCMEARN"));
			agcmpf.setRnwcpy(rs.getString("RNWCPY"));
			agcmpf.setRnlcdue(rs.getBigDecimal("RNLCDUE"));
			agcmpf.setRnlcearn(rs.getBigDecimal("RNLCEARN"));
			agcmpf.setAgcls(rs.getString("AGCLS"));
			agcmpf.setTermid(rs.getString("TERMID"));
			agcmpf.setTrdt(rs.getInt("TRDT"));
			agcmpf.setUser_t(rs.getInt("USER_T"));
			agcmpf.setValidflag(rs.getString("VALIDFLAG"));
			agcmpf.setCurrfrom(rs.getInt("CURRFROM"));
			agcmpf.setCurrto(rs.getInt("CURRTO"));
			agcmpf.setPtdate(rs.getInt("PTDATE"));
			agcmpf.setSeqno(rs.getInt("SEQNO"));
			agcmpf.setCedagent(rs.getString("CEDAGENT"));
			agcmpf.setOvrdcat(rs.getString("OVRDCAT"));
			agcmpf.setDormflag(rs.getString("DORMFLAG"));
			agcmpf.setUsrprf(rs.getString("USRPRF"));
			agcmpf.setJobnm(rs.getString("JOBNM"));
			agcmpf.setDatim(rs.getDate("DATIME"));
			
			outputList.add(agcmpf);
		}
		}catch (SQLException e) {
			LOGGER.error("searchAgcmdmnRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return outputList;
	}
	
	public Agcmpf searchAgcmdmnRecord(long uniqueNumber) throws SQLRuntimeException{
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("select * from AGCMPF WHERE CHDRCOY=? and CHDRNUM=? and VALIDFLAG = '1' and  DORMFLAG != 'Y' and UNIQUE_NUMBER=? "); 
		sqlSelect.append("order by CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER ASC ");
		
		PreparedStatement ps = getPrepareStatement(sqlSelect.toString());
		ResultSet rs = null;
		Agcmpf agcmpf = null;
		try {
		ps.setLong(1, uniqueNumber);
	
		rs = executeQuery(ps);
		if (rs.next()) {
			agcmpf = new Agcmpf();
			agcmpf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
			agcmpf.setChdrcoy(rs.getString("CHDRCOY"));
			agcmpf.setChdrnum(rs.getString("CHDRNUM"));
			agcmpf.setLife(rs.getString("LIFE"));
			agcmpf.setCoverage(rs.getString("COVERAGE"));
			agcmpf.setRider(rs.getString("RIDER"));
			agcmpf.setPlnsfx(rs.getInt("PLNSFX"));
			agcmpf.setTranno(rs.getInt("TRANNO"));
			agcmpf.setAgntnum(rs.getString("AGNTNUM"));
			agcmpf.setEfdate(rs.getInt("EFDATE"));
			agcmpf.setAnnprem(rs.getBigDecimal("ANNPREM"));
			agcmpf.setBascmeth(rs.getString("BASCMETH"));
			agcmpf.setInitcom(rs.getBigDecimal("INITCOM"));
			agcmpf.setBascpy(rs.getString("BASCPY"));
			agcmpf.setCompay(rs.getBigDecimal("COMPAY"));
			agcmpf.setComern(rs.getBigDecimal("COMERN"));
			agcmpf.setSrvcpy(rs.getString("SRVCPY"));
			agcmpf.setScmdue(rs.getBigDecimal("SCMDUE"));
			agcmpf.setScmearn(rs.getBigDecimal("SCMEARN"));
			agcmpf.setRnwcpy(rs.getString("RNWCPY"));
			agcmpf.setRnlcdue(rs.getBigDecimal("RNLCDUE"));
			agcmpf.setRnlcearn(rs.getBigDecimal("RNLCEARN"));
			agcmpf.setAgcls(rs.getString("AGCLS"));
			agcmpf.setTermid(rs.getString("TERMID"));
			agcmpf.setTrdt(rs.getInt("TRDT"));
			agcmpf.setUser_t(rs.getInt("USER_T"));
			agcmpf.setValidflag(rs.getString("VALIDFLAG"));
			agcmpf.setCurrfrom(rs.getInt("CURRFROM"));
			agcmpf.setCurrto(rs.getInt("CURRTO"));
			agcmpf.setPtdate(rs.getInt("PTDATE"));
			agcmpf.setSeqno(rs.getInt("SEQNO"));
			agcmpf.setCedagent(rs.getString("CEDAGENT"));
			agcmpf.setOvrdcat(rs.getString("OVRDCAT"));
			agcmpf.setDormflag(rs.getString("DORMFLAG"));
			agcmpf.setUsrprf(rs.getString("USRPRF"));
			agcmpf.setJobnm(rs.getString("JOBNM"));
			agcmpf.setDatim(rs.getDate("DATIME"));

		}
		}catch (SQLException e) {
			LOGGER.error("searchAgcmdmnRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return agcmpf;
	}
	// ILIFE-8163 Starts
	public void updateAgcmRecord(Agcmpf agcmpf) {
		String SQL_UPDATE = "UPDATE AGCMPF SET ANNPREM=? WHERE CHDRCOY=? AND  CHDRNUM=? AND LIFE=? AND  COVERAGE=? AND RIDER=?  ";

		PreparedStatement psUpdate = getPrepareStatement(SQL_UPDATE);
		try {
			{
				psUpdate.setBigDecimal(1, agcmpf.getAnnprem());
				psUpdate.setString(2, agcmpf.getChdrcoy());
				psUpdate.setString(3, agcmpf.getChdrnum());
				psUpdate.setString(4,agcmpf.getLife());
				psUpdate.setString(5, agcmpf.getCoverage());
				psUpdate.setString(6,agcmpf.getRider());
				psUpdate.addBatch(); 
			}
			psUpdate.executeBatch(); 
		} catch (SQLException e) {
			LOGGER.error("updateAgcmRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psUpdate, null);
		}
	}
	// ILIFE-8163 Ends
	
	@Override
	public List<Agcmpf> searchAgcmpfRecord(Agcmpf agcmpf) throws SQLRuntimeException {
		String sqlSelect = "select * from AGCMCHG WHERE CHDRCOY=? and CHDRNUM=? and AGNTNUM=? and LIFE=? and COVERAGE=? and RIDER=? and PLNSFX=? order by SEQNO ASC";
		PreparedStatement ps = getPrepareStatement(sqlSelect);
		ResultSet rs = null;
		List<Agcmpf> outputList = new ArrayList<Agcmpf>();
		try {
		ps.setString(1, agcmpf.getChdrcoy());
		ps.setString(2, agcmpf.getChdrnum());
		ps.setString(3, agcmpf.getAgntnum());
		ps.setString(4, agcmpf.getLife());
		ps.setString(5, agcmpf.getCoverage());
		ps.setString(6, agcmpf.getRider());
		ps.setInt(7, agcmpf.getPlnsfx());
		rs = executeQuery(ps);
		while (rs.next()) {
			agcmpf = new Agcmpf();
			agcmpf.setUnique_Number(rs.getLong("UNIQUE_NUMBER"));
			agcmpf.setChdrcoy(rs.getString("CHDRCOY"));
			agcmpf.setChdrnum(rs.getString("CHDRNUM"));
			agcmpf.setLife(rs.getString("LIFE"));
			agcmpf.setCoverage(rs.getString("COVERAGE"));
			agcmpf.setRider(rs.getString("RIDER"));
			agcmpf.setPlnsfx(rs.getInt("PLNSFX"));
			agcmpf.setTranno(rs.getInt("TRANNO"));
			agcmpf.setAgntnum(rs.getString("AGNTNUM"));
			agcmpf.setEfdate(rs.getInt("EFDATE"));
			agcmpf.setAnnprem(rs.getBigDecimal("ANNPREM"));
			agcmpf.setBascmeth(rs.getString("BASCMETH"));
			agcmpf.setInitcom(rs.getBigDecimal("INITCOM"));
			agcmpf.setBascpy(rs.getString("BASCPY"));
			agcmpf.setCompay(rs.getBigDecimal("COMPAY"));
			agcmpf.setComern(rs.getBigDecimal("COMERN"));
			agcmpf.setSrvcpy(rs.getString("SRVCPY"));
			agcmpf.setScmdue(rs.getBigDecimal("SCMDUE"));
			agcmpf.setScmearn(rs.getBigDecimal("SCMEARN"));
			agcmpf.setRnwcpy(rs.getString("RNWCPY"));
			agcmpf.setRnlcdue(rs.getBigDecimal("RNLCDUE"));
			agcmpf.setRnlcearn(rs.getBigDecimal("RNLCEARN"));
			agcmpf.setAgcls(rs.getString("AGCLS"));
			agcmpf.setTermid(rs.getString("TERMID"));
			agcmpf.setTrdt(rs.getInt("TRDT"));
			agcmpf.setUser_t(rs.getInt("USER_T"));
			agcmpf.setValidflag(rs.getString("VALIDFLAG"));
			agcmpf.setCurrfrom(rs.getInt("CURRFROM"));
			agcmpf.setCurrto(rs.getInt("CURRTO"));
			agcmpf.setPtdate(rs.getInt("PTDATE"));
			agcmpf.setSeqno(rs.getInt("SEQNO"));
			agcmpf.setCedagent(rs.getString("CEDAGENT"));
			agcmpf.setOvrdcat(rs.getString("OVRDCAT"));
			agcmpf.setDormflag(rs.getString("DORMFLAG"));
			agcmpf.setUsrprf(rs.getString("USRPRF"));
			agcmpf.setJobnm(rs.getString("JOBNM"));
			agcmpf.setDatim(rs.getDate("DATIME"));
			outputList.add(agcmpf);
		}
		}catch (SQLException e) {
			LOGGER.error("searchAgcmpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return outputList;
	}
}