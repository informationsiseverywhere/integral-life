/*
 * File: C5001.java
 * Date: 30 August 2009 2:58:36
 * Author: $Id$
 * 
 * Class transformed from C5001.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;

import com.csc.life.agents.batchprograms.B5001;
import com.csc.smart.procedures.Passparm;
import com.csc.smart400framework.dataaccess.dao.AgcgpfDAO;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.model.Agcgpf;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C5001 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(300);
	private FixedLengthStringData conj = new FixedLengthStringData(860);

	public C5001() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(Passparm.class, new Object[] {params, conj});
					
					String parmAgntfrom = subString(conj, 1, 8).toString();
					Character parmCoy = subString(params, 6, 1).toString().charAt(0);
					
					AgcgpfDAO agcgpfDAO = DAOFactory.getAgcgpfDAO();
					agcgpfDAO.deleteAll();
					agcgpfDAO.copyTableByAgntnum("AGCMPF", parmAgntfrom);
					agcgpfDAO.copyTableByAgntnum("CHDRPF", parmAgntfrom);
					agcgpfDAO.copyTableByAgntnum("PCDDPF", parmAgntfrom);
					
					// Query Data
					Iterator<Agcgpf> agcgpfIterator;
					agcgpfIterator = agcgpfDAO.findByAgntnum(parmAgntfrom, parmCoy).iterator();
					
					callProgram(B5001.class, new Object[] {params, agcgpfIterator});
					
					// Delete data after use to avoid concurrent conflict
					agcgpfDAO.deleteAll();
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.addExtMessage("Unexpected Errors Have Occured"+".");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
