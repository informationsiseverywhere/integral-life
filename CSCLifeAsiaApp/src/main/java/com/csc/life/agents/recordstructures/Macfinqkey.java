package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:33
 * Description:
 * Copybook name: MACFINQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfinqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfinqFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfinqKey = new FixedLengthStringData(64).isAPartOf(macfinqFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfinqAgntcoy = new FixedLengthStringData(1).isAPartOf(macfinqKey, 0);
  	public FixedLengthStringData macfinqAgntnum = new FixedLengthStringData(8).isAPartOf(macfinqKey, 1);
  	public PackedDecimalData macfinqEffdate = new PackedDecimalData(8, 0).isAPartOf(macfinqKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(macfinqKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfinqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfinqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}