package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:02
 * Description:
 * Copybook name: AGCMSQYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmsqykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmsqyFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmsqyKey = new FixedLengthStringData(64).isAPartOf(agcmsqyFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmsqyChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmsqyKey, 0);
  	public FixedLengthStringData agcmsqyChdrnum = new FixedLengthStringData(8).isAPartOf(agcmsqyKey, 1);
  	public FixedLengthStringData agcmsqyLife = new FixedLengthStringData(2).isAPartOf(agcmsqyKey, 9);
  	public FixedLengthStringData agcmsqyCoverage = new FixedLengthStringData(2).isAPartOf(agcmsqyKey, 11);
  	public FixedLengthStringData agcmsqyRider = new FixedLengthStringData(2).isAPartOf(agcmsqyKey, 13);
  	public PackedDecimalData agcmsqyPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmsqyKey, 15);
  	public PackedDecimalData agcmsqySeqno = new PackedDecimalData(2, 0).isAPartOf(agcmsqyKey, 18);
  	public FixedLengthStringData agcmsqyDormantFlag = new FixedLengthStringData(1).isAPartOf(agcmsqyKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(agcmsqyKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmsqyFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmsqyFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}