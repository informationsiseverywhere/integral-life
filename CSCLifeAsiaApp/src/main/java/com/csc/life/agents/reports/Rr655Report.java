package com.csc.life.agents.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR655.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr655Report extends SMARTReportLayout { 

	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private ZonedDecimalData amtfld = new ZonedDecimalData(12, 2);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private RPGTimeData time = new RPGTimeData();
	private FixedLengthStringData trdate = new FixedLengthStringData(10);
	private FixedLengthStringData zbnplnid = new FixedLengthStringData(8);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr655Report() {
		super();
	}


	/**
	 * Print the XML for Rr655d01
	 */
	public void printRr655d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		agntnum.setFieldName("agntnum");
		agntnum.setInternal(subString(recordData, 1, 8));
		trdate.setFieldName("trdate");
		trdate.setInternal(subString(recordData, 9, 10));
		zbnplnid.setFieldName("zbnplnid");
		zbnplnid.setInternal(subString(recordData, 19, 8));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 27, 8));
		amtfld.setFieldName("amtfld");
		amtfld.setInternal(subString(recordData, 35, 12));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 47, 30));
		printLayout("Rr655d01",			// Record name
			new BaseData[]{			// Fields:
				agntnum,
				trdate,
				zbnplnid,
				chdrnum,
				amtfld,
				longdesc
			}
		);

	}

	/**
	 * Print the XML for Rr655h01
	 */
	public void printRr655h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr655h01",			// Record name
			new BaseData[]{			// Fields:
				dateReportVariable,
				company,
				companynm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(9);
	}


}
