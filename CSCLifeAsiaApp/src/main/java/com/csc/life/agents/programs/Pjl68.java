package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.nio.charset.StandardCharsets;
import java.util.List;

import com.csc.life.agents.screens.Sjl68ScreenVars;
import com.csc.life.agents.tablestructures.Tjl68rec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl68 extends ScreenProgCS {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl68");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");	
	
	private String e186 = "E186";
	private String e026 = "E026";

	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private String wsaaItemlang;
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaFirstTime = "Y";
	private List<Descpf> t1692List;
	private List<Descpf> t5696List;
	private Tjl68rec tjl68rec = new Tjl68rec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);	
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Itempf itempf = null;	
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl68ScreenVars sv = ScreenProgram.getScreenVars(Sjl68ScreenVars.class);
	private boolean isDataChanged = false;		

	public Pjl68() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl68", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}
	
	protected void initialise1000() {
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		if (isNE(wsaaFirstTime, "Y")) {
			if (isNE(wsaaSeq, ZERO)) {
				wsaaItemseq = wsaaSeq.toString();
			}
		} else {
			sv.dataArea.set(SPACES);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemlang = wsspcomn.language.toString();		
		sv.company.set(wsaaItemcoy);
		sv.tabl.set(wsaaItemtabl);
		sv.item.set(wsaaItemitem);	
		Descpf descpf = descDAO.getdescData(wsaaItempfx, wsaaItemtabl, wsaaItemitem,wsaaItemcoy,
				wsaaItemlang);
		sv.longdesc.set(descpf.getLongdesc());	
		itempf = itemDAO.findItemByItem(wsaaItemcoy,wsaaItemtabl,wsaaItemitem);
		if (itempf == null) {
			if(isEQ(wsspcomn.flag, "I")) {
				scrnparams.errorCode.set(e026);
				if(isGT(wsaaSeq,ZERO)) {
					wsaaSeq.subtract(1);
				}
			} else {
				tjl68rec.tjl68Rec.set(SPACES);
			}
		} else {
			tjl68rec.tjl68Rec.set(itempf.getGenareaString());
		}
			sv.agntbr.set(tjl68rec.agntbr);		
			sv.agbrdesc.set(tjl68rec.agbrdesc);
			sv.aracde.set(tjl68rec.aracde);		
			sv.aradesc.set(tjl68rec.aradesc);
		
		wsaaFirstTime = "N";
		
		if (isEQ(wsspcomn.flag, "I")) {
			sv.companyOut[Varcom.pr.toInt()].set("Y"); 
			sv.tablOut[Varcom.pr.toInt()].set("Y"); 
			sv.companyOut[Varcom.pr.toInt()].set("Y"); 
			sv.itemOut[Varcom.pr.toInt()].set("Y"); 
			sv.longdescOut[Varcom.pr.toInt()].set("Y"); 
			sv.agntbrOut[Varcom.pr.toInt()].set("Y"); 
			sv.agbrdescOut[Varcom.pr.toInt()].set("Y"); 
			sv.aracdeOut[Varcom.pr.toInt()].set("Y"); 
			sv.aradescOut[Varcom.pr.toInt()].set("Y"); 
		}
			
	}	
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@SuppressWarnings("static-access")
	protected void screenEdit2000() {
		wsspcomn.edterror.set(varcom.oK);
		validate2010();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void validate2010() {		
		if(isEQ(sv.agntbr, SPACES)){
			sv.agntbrErr.set(e186);
		}
		
		t1692List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "T1692", sv.agntbr.toString().trim(),wsspcomn.language.toString());
		if(!t1692List.isEmpty()) {
			for (Descpf descItem : t1692List) {
				if (descItem.getDescitem().trim().equals(sv.agntbr.toString().trim())){
					sv.agbrdesc.set(descItem.getLongdesc());
					break;
				}
				else {
					sv.agbrdesc.set(" ");
				}
			}
		}
		t5696List = descDAO.getItemByDescItem("IT", wsspcomn.company.toString(), "T5696", sv.aracde.toString().trim(),wsspcomn.language.toString());
		if(!t5696List.isEmpty()) {
			for (Descpf descItem : t5696List) {
				if (descItem.getDescitem().trim().equals(sv.aracde.toString().trim())){
					sv.aradesc.set(descItem.getLongdesc());
					break;
				}
			}
			if(isEQ(sv.aracde, SPACES)){
				sv.aradesc.set(SPACES);
			}
		}
	}
	
	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.agntbr, tjl68rec.agntbr)) {
			tjl68rec.agntbr.set(sv.agntbr);			
			isDataChanged = true;
		}
		if (isNE(sv.agbrdesc, tjl68rec.agbrdesc)) {			
			tjl68rec.agbrdesc.set(sv.agbrdesc);
			isDataChanged = true;
		}
		if (isNE(sv.aracde, tjl68rec.aracde)) {
			tjl68rec.aracde.set(sv.aracde);			
			isDataChanged = true;
		}	
		if (isNE(sv.aradesc, tjl68rec.aradesc)) {			
			tjl68rec.aradesc.set(sv.aradesc);
			isDataChanged = true;
		}
		if(isDataChanged) {
			if(itempf==null) {
				writeData();
			} else {
				itempf.setGenarea(tjl68rec.tjl68Rec.toString().getBytes(StandardCharsets.UTF_8));
				itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
						.getGENAREAJString(tjl68rec.tjl68Rec.toString().getBytes(StandardCharsets.UTF_8), tjl68rec));
				itempf.setItempfx(wsaaItempfx);
				itempf.setItemcoy(wsaaItemcoy);
				itempf.setItemtabl(wsaaItemtabl);
				itempf.setItemitem(wsaaItemitem);
				itempf.setItemseq(wsaaItemseq);
				itemDAO.updateSmartTableItem(itempf);
			}
		}
	}
	
	protected void writeData() {
		Itempf item = new Itempf();
		item.setItempfx(wsaaItempfx);
		item.setItemcoy(wsaaItemcoy);
		item.setItemtabl(wsaaItemtabl);
		item.setItemitem(wsaaItemitem);
		item.setItemseq(wsaaSeq.toString());
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		item.setTranid(varcom.vrcmCompTranid.toString());
		item.setTableprog(wsaaProg.toString());
		item.setValidflag("1");
		item.setGenarea(tjl68rec.tjl68Rec.toString().getBytes(StandardCharsets.UTF_8));
		item.setGenareaj("");
		itemDAO.insertSmartTableItem(item);
	}
	
	@SuppressWarnings("static-access")
	protected void whereNext4000() {
			wsspcomn.nextprog.set(wsaaProg); 
			wsspcomn.programPtr.add(1);
	}
}