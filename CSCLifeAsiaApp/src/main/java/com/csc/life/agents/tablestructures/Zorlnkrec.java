package com.csc.life.agents.tablestructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:37
 * Description:
 * Copybook name: ZORLNKREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zorlnkrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData zorlnkRec = new FixedLengthStringData(190);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(zorlnkRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(zorlnkRec, 5);
  	public FixedLengthStringData agent = new FixedLengthStringData(8).isAPartOf(zorlnkRec, 9);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(zorlnkRec, 17);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(zorlnkRec, 18);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(zorlnkRec, 26);
  	public PackedDecimalData annprem = new PackedDecimalData(17, 2).isAPartOf(zorlnkRec, 30);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(zorlnkRec, 39);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(zorlnkRec, 44);
  	public FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(zorlnkRec, 49);
  	public PackedDecimalData crate = new PackedDecimalData(8, 0).isAPartOf(zorlnkRec, 52);
  	public PackedDecimalData origamt = new PackedDecimalData(17, 2).isAPartOf(zorlnkRec, 57);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(zorlnkRec, 66);
  	public FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(zorlnkRec, 69);
  	public FixedLengthStringData genlcur = new FixedLengthStringData(3).isAPartOf(zorlnkRec, 99);
  	public FixedLengthStringData sacstyp = new FixedLengthStringData(2).isAPartOf(zorlnkRec, 102);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(zorlnkRec, 104);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(zorlnkRec, 108);
  	public FixedLengthStringData batchKey = new FixedLengthStringData(22).isAPartOf(zorlnkRec, 111);
  	public FixedLengthStringData tranref = new FixedLengthStringData(30).isAPartOf(zorlnkRec, 133);
  	public FixedLengthStringData clawback = new FixedLengthStringData(1).isAPartOf(zorlnkRec, 163);
  	//ILIFE-8193  start
  	public ZonedDecimalData nyear = new ZonedDecimalData(2, 0).isAPartOf(zorlnkRec, 164);
  	public FixedLengthStringData basicAgentType = new FixedLengthStringData(2).isAPartOf(zorlnkRec, 166);
  	public FixedLengthStringData reptAgentType = new FixedLengthStringData(2).isAPartOf(zorlnkRec, 168);
  	public FixedLengthStringData workAgentType = new FixedLengthStringData(2).isAPartOf(zorlnkRec, 170);
  	public PackedDecimalData commAmt = new PackedDecimalData(17, 2).isAPartOf(zorlnkRec, 172);
  	public PackedDecimalData gstAmount = new PackedDecimalData(17, 2).isAPartOf(zorlnkRec, 181);// ILIFE-8468
	//ILIFE-8193  end
	public void initialize() {
		COBOLFunctions.initialize(zorlnkRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zorlnkRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}