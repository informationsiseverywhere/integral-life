package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:36
 * Description:
 * Copybook name: ZBNWCVGREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwcvgrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwcvgRec = new FixedLengthStringData(790);
  	public FixedLengthStringData bnwcvgTrcde = new FixedLengthStringData(3).isAPartOf(bnwcvgRec, 0);
  	public FixedLengthStringData bnwcvgSource = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 3);
  	public FixedLengthStringData bnwcvgCrtable = new FixedLengthStringData(50).isAPartOf(bnwcvgRec, 13);
  	public FixedLengthStringData bnwcvgStatdesc = new FixedLengthStringData(25).isAPartOf(bnwcvgRec, 63);
  	public FixedLengthStringData bnwcvgStrtdate = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 88);
  	public FixedLengthStringData bnwcvgTrmdate = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 98);
  	public FixedLengthStringData bnwcvgPartner = new FixedLengthStringData(20).isAPartOf(bnwcvgRec, 108);
  	public FixedLengthStringData bnwcvgClrhouse = new FixedLengthStringData(30).isAPartOf(bnwcvgRec, 128);
  	public FixedLengthStringData bnwcvgSpclcls = new FixedLengthStringData(30).isAPartOf(bnwcvgRec, 158);
  	public FixedLengthStringData bnwcvgPremcls = new FixedLengthStringData(30).isAPartOf(bnwcvgRec, 188);
  	public FixedLengthStringData bnwcvgInstprem = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 218);
  	public FixedLengthStringData bnwcvgAnnprem = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 231);
  	public FixedLengthStringData bnwcvgTarcmsn = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 244);
  	public FixedLengthStringData bnwcvgLevel = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 257);
  	public FixedLengthStringData bnwcvgFaceamt = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 267);
  	public FixedLengthStringData bnwcvgCashval = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 280);
  	public FixedLengthStringData bnwcvgGuideamt = new FixedLengthStringData(13).isAPartOf(bnwcvgRec, 293);
  	public FixedLengthStringData bnwcvgPlan = new FixedLengthStringData(30).isAPartOf(bnwcvgRec, 306);
  	public FixedLengthStringData bnwcvgMarkt = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 336);
  	public FixedLengthStringData bnwcvgCompany = new FixedLengthStringData(3).isAPartOf(bnwcvgRec, 346);
  	public FixedLengthStringData bnwcvgChdrnum = new FixedLengthStringData(30).isAPartOf(bnwcvgRec, 349);
  	public FixedLengthStringData bnwcvgNewfield = new FixedLengthStringData(1).isAPartOf(bnwcvgRec, 379);
  	public FixedLengthStringData bnwcvgAgents = new FixedLengthStringData(390).isAPartOf(bnwcvgRec, 380);
  	public FixedLengthStringData[] bnwcvgAgent = FLSArrayPartOfStructure(10, 39, bnwcvgAgents, 0);
  	public FixedLengthStringData[] bnwcvgAgntnum = FLSDArrayPartOfArrayStructure(10, bnwcvgAgent, 0);
  	public FixedLengthStringData[] bnwcvgAgntrole = FLSDArrayPartOfArrayStructure(2, bnwcvgAgent, 10);
  	public FixedLengthStringData[] bnwcvgProfile = FLSDArrayPartOfArrayStructure(3, bnwcvgAgent, 12);
  	public FixedLengthStringData[] bnwcvgCmsnrtst = FLSDArrayPartOfArrayStructure(6, bnwcvgAgent, 15);
  	public FixedLengthStringData[] bnwcvgCmsnrtrnl = FLSDArrayPartOfArrayStructure(6, bnwcvgAgent, 21);
  	public FixedLengthStringData[] bnwcvgProdrtst = FLSDArrayPartOfArrayStructure(6, bnwcvgAgent, 27);
  	public FixedLengthStringData[] bnwcvgProdrtrnl = FLSDArrayPartOfArrayStructure(6, bnwcvgAgent, 33);
  	public FixedLengthStringData bnwcvgTrandate = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 770);
  	public FixedLengthStringData bnwcvgProcdate = new FixedLengthStringData(10).isAPartOf(bnwcvgRec, 780);


	public void initialize() {
		COBOLFunctions.initialize(bnwcvgRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwcvgRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}