package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZbuppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:59
 * Class transformed from ZBUPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZbuppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 91;
	public FixedLengthStringData zbuprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zbuppfRecord = zbuprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zbuprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zbuprec);
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zbuprec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zbuprec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(zbuprec);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(zbuprec);
	public PackedDecimalData prcdate = DD.prcdate.copy().isAPartOf(zbuprec);
	public FixedLengthStringData zbnplnid = DD.zbnplnid.copy().isAPartOf(zbuprec);
	public FixedLengthStringData zbntyp = DD.zbntyp.copy().isAPartOf(zbuprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zbuprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zbuprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zbuprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZbuppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZbuppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZbuppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZbuppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZbuppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZbuppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZbuppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZBUPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"AGNTCOY, " +
							"AGNTNUM, " +
							"ORIGAMT, " +
							"ORIGCURR, " +
							"PRCDATE, " +
							"ZBNPLNID, " +
							"ZBNTYP, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     agntcoy,
                                     agntnum,
                                     origamt,
                                     origcurr,
                                     prcdate,
                                     zbnplnid,
                                     zbntyp,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		agntcoy.clear();
  		agntnum.clear();
  		origamt.clear();
  		origcurr.clear();
  		prcdate.clear();
  		zbnplnid.clear();
  		zbntyp.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZbuprec() {
  		return zbuprec;
	}

	public FixedLengthStringData getZbuppfRecord() {
  		return zbuppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZbuprec(what);
	}

	public void setZbuprec(Object what) {
  		this.zbuprec.set(what);
	}

	public void setZbuppfRecord(Object what) {
  		this.zbuppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zbuprec.getLength());
		result.set(zbuprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}