package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR515
 * @version 1.0 generated on 30/08/09 07:16
 * @author Quipoz
 */
public class Sr515ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(97).isAPartOf(dataArea, 0);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,38);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 97);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 117);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(126);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(60).isAPartOf(subfileArea, 0);
	public FixedLengthStringData deit = DD.deit.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(subfileFields,37);
	public ZonedDecimalData prcnt = DD.prcnt.copyToZonedDecimal().isAPartOf(subfileFields,45);
	public FixedLengthStringData zrreptp = DD.zrreptp.copy().isAPartOf(subfileFields,50);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(16).isAPartOf(subfileArea, 60);
	public FixedLengthStringData deitErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData prcntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData zrreptpErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(48).isAPartOf(subfileArea, 76);
	public FixedLengthStringData[] deitOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] prcntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] zrreptpOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 124);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);

	public LongData Sr515screensflWritten = new LongData(0);
	public LongData Sr515screenctlWritten = new LongData(0);
	public LongData Sr515screenWritten = new LongData(0);
	public LongData Sr515protectWritten = new LongData(0);
	public GeneralTable sr515screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr515screensfl;
	}

	public Sr515ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(dteappOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrreptpOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prcntOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {dteapp, zrreptp, deit, prcnt};
		screenSflOutFields = new BaseData[][] {dteappOut, zrreptpOut, deitOut, prcntOut};
		screenSflErrFields = new BaseData[] {dteappErr, zrreptpErr, deitErr, prcntErr};
		screenSflDateFields = new BaseData[] {dteapp};
		screenSflDateErrFields = new BaseData[] {dteappErr};
		screenSflDateDispFields = new BaseData[] {dteappDisp};

		screenFields = new BaseData[] {clntsel, agnum, agtype, cltname, agtydesc};
		screenOutFields = new BaseData[][] {clntselOut, agnumOut, agtypeOut, cltnameOut, agtydescOut};
		screenErrFields = new BaseData[] {clntselErr, agnumErr, agtypeErr, cltnameErr, agtydescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr515screen.class;
		screenSflRecord = Sr515screensfl.class;
		screenCtlRecord = Sr515screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr515protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr515screenctl.lrec.pageSubfile);
	}
}
