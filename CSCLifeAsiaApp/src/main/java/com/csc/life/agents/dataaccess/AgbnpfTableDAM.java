package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgbnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:40
 * Class transformed from AGBNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgbnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 79;
	public FixedLengthStringData agbnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agbnpfRecord = agbnrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agbnrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agbnrec);
	public FixedLengthStringData contact = DD.contact.copy().isAPartOf(agbnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(agbnrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(agbnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(agbnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(agbnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agbnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agbnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agbnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgbnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgbnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgbnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgbnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgbnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgbnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgbnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGBNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"CONTACT, " +
							"USER_T, " +
							"TERMID, " +
							"TRTM, " +
							"TRDT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     contact,
                                     user,
                                     termid,
                                     transactionTime,
                                     transactionDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		contact.clear();
  		user.clear();
  		termid.clear();
  		transactionTime.clear();
  		transactionDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgbnrec() {
  		return agbnrec;
	}

	public FixedLengthStringData getAgbnpfRecord() {
  		return agbnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgbnrec(what);
	}

	public void setAgbnrec(Object what) {
  		this.agbnrec.set(what);
	}

	public void setAgbnpfRecord(Object what) {
  		this.agbnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agbnrec.getLength());
		result.set(agbnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}