package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:16
 * Description:
 * Copybook name: ZBUPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbupkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zbupFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zbupKey = new FixedLengthStringData(256).isAPartOf(zbupFileKey, 0, REDEFINE);
  	public FixedLengthStringData zbupAgntcoy = new FixedLengthStringData(1).isAPartOf(zbupKey, 0);
  	public FixedLengthStringData zbupAgntnum = new FixedLengthStringData(8).isAPartOf(zbupKey, 1);
  	public PackedDecimalData zbupPrcdate = new PackedDecimalData(8, 0).isAPartOf(zbupKey, 9);
  	public FixedLengthStringData zbupZbnplnid = new FixedLengthStringData(8).isAPartOf(zbupKey, 14);
  	public FixedLengthStringData zbupChdrcoy = new FixedLengthStringData(1).isAPartOf(zbupKey, 22);
  	public FixedLengthStringData zbupChdrnum = new FixedLengthStringData(8).isAPartOf(zbupKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(225).isAPartOf(zbupKey, 31, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zbupFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zbupFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}