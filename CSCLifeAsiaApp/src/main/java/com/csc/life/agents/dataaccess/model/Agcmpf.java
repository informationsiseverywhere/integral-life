package com.csc.life.agents.dataaccess.model;

import java.math.BigDecimal;
import java.util.Date;

public class Agcmpf {

	private long unique_Number = 0;
	private String chdrcoy = "";
	private String chdrnum = "";
	private String life = "";
	private String coverage = "";
	private String rider = "";
	private Integer plnsfx = 0;
	private Integer tranno = 0;
	private String agntnum = "";
	private Integer efdate = 0;
	private BigDecimal annprem = BigDecimal.ZERO;
	private String bascmeth = "";
	private BigDecimal initcom = BigDecimal.ZERO;
	private String bascpy = "";
	private BigDecimal compay = BigDecimal.ZERO;
	private BigDecimal comern = BigDecimal.ZERO;
	private String srvcpy = "";
	private BigDecimal scmdue = BigDecimal.ZERO;
	private BigDecimal scmearn = BigDecimal.ZERO;
	private String rnwcpy = "";
	private BigDecimal rnlcdue = BigDecimal.ZERO;
	private BigDecimal rnlcearn = BigDecimal.ZERO;
	private String agcls = "";
	private String termid = "";
	private Integer trdt = 0;
	private Integer user_t = 0;
	private String validflag = "";
	private Integer currfrom = 0;
	private Integer currto = 0;
	private Integer ptdate = 0;
	private Integer seqno = 0;
	private String cedagent = "";
	private String ovrdcat = "";
	private String dormflag = "";
	private String status = "";
	private String usrprf = "";
	private String jobnm = "";
	private Date datim;
	
	public long getUnique_Number() {
		return unique_Number;
	}
	public void setUnique_Number(long unique_Number) {
		this.unique_Number = unique_Number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public Integer getEfdate() {
		return efdate;
	}
	public void setEfdate(Integer efdate) {
		this.efdate = efdate;
	}
	public BigDecimal getAnnprem() {
		return annprem;
	}
	public void setAnnprem(BigDecimal annprem) {
		this.annprem = annprem;
	}
	public String getBascmeth() {
		return bascmeth;
	}
	public void setBascmeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}
	public BigDecimal getInitcom() {
		return initcom;
	}
	public void setInitcom(BigDecimal initcom) {
		this.initcom = initcom;
	}
	public String getBascpy() {
		return bascpy;
	}
	public void setBascpy(String bascpy) {
		this.bascpy = bascpy;
	}
	public BigDecimal getCompay() {
		return compay;
	}
	public void setCompay(BigDecimal compay) {
		this.compay = compay;
	}
	public BigDecimal getComern() {
		return comern;
	}
	public void setComern(BigDecimal comern) {
		this.comern = comern;
	}
	public String getSrvcpy() {
		return srvcpy;
	}
	public void setSrvcpy(String srvcpy) {
		this.srvcpy = srvcpy;
	}
	public BigDecimal getScmdue() {
		return scmdue;
	}
	public void setScmdue(BigDecimal scmdue) {
		this.scmdue = scmdue;
	}
	public BigDecimal getScmearn() {
		return scmearn;
	}
	public void setScmearn(BigDecimal scmearn) {
		this.scmearn = scmearn;
	}
	public String getRnwcpy() {
		return rnwcpy;
	}
	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}
	public BigDecimal getRnlcdue() {
		return rnlcdue;
	}
	public void setRnlcdue(BigDecimal rnlcdue) {
		this.rnlcdue = rnlcdue;
	}
	public BigDecimal getRnlcearn() {
		return rnlcearn;
	}
	public void setRnlcearn(BigDecimal rnlcearn) {
		this.rnlcearn = rnlcearn;
	}
	public String getAgcls() {
		return agcls;
	}
	public void setAgcls(String agcls) {
		this.agcls = agcls;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getUser_t() {
		return user_t;
	}
	public void setUser_t(Integer user_t) {
		this.user_t = user_t;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public Integer getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Integer currfrom) {
		this.currfrom = currfrom;
	}
	public Integer getCurrto() {
		return currto;
	}
	public void setCurrto(Integer currto) {
		this.currto = currto;
	}
	public Integer getPtdate() {
		return ptdate;
	}
	public void setPtdate(Integer ptdate) {
		this.ptdate = ptdate;
	}
	public Integer getSeqno() {
		return seqno;
	}
	public void setSeqno(Integer seqno) {
		this.seqno = seqno;
	}
	public String getCedagent() {
		return cedagent;
	}
	public void setCedagent(String cedagent) {
		this.cedagent = cedagent;
	}
	public String getOvrdcat() {
		return ovrdcat;
	}
	public void setOvrdcat(String ovrdcat) {
		this.ovrdcat = ovrdcat;
	}
	public String getDormflag() {
		return dormflag;
	}
	public void setDormflag(String dormflag) {
		this.dormflag = dormflag;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatim() {
		return datim;
	}
	public void setDatim(Date datim) {
		this.datim = datim;
	}
	
}