/*
 * File: P5035.java
 * Date: 29 August 2009 23:58:40
 * Author: Quipoz Limited
 *
 * Class transformed from P5035.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgntcypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgqfpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agntcypf;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.agents.dataaccess.model.Agqfpf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.agents.dataaccess.model.Hierpf;
import com.csc.fsu.agents.dataaccess.dao.HierpfDAO;
import com.csc.fsu.agents.recordstructures.Agntkey;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Cltskey;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.fsu.financials.tablestructures.T3672rec;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.MacfTableDAM;
import com.csc.life.agents.dataaccess.MacfflvTableDAM;
import com.csc.life.agents.dataaccess.dao.AgbnpfDAO;
import com.csc.life.agents.dataaccess.dao.AgorpfDAO;
import com.csc.life.agents.dataaccess.dao.AgslpfDAO;
import com.csc.life.agents.dataaccess.model.Agbnpf;
import com.csc.life.agents.dataaccess.model.Agorpf;
import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.life.agents.screens.S5035ScreenVars;
import com.csc.life.agents.tablestructures.T5690rec;
import com.csc.life.contractservicing.dataaccess.dao.AglflnbpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Aglflnbpf;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.smart.dataaccess.dao.UsrdDAO;
import com.csc.smart.dataaccess.model.Usrdpf;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!    S9503>
*REMARKS.
*
* Initialise
* ----------
*
*   Skip  this  section  if  returning from an optional selection
*   (current stack position action flag = '*').
*
*   Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*   information.
*
*   Using  the  information  retrieved  from AGLF, READR AGNT. If
*   there is no record on this file for the agent number, this is
*   a new agency agreement to be created. Otherwise, it is an old
*   one  to  be  modified.  (If in enquiry mode, this record must
*   exist).
*
*   If the agreement does not exist:
*
*        - default the agent branch to the sign-on branch look up
*             its description  from  the  branch  table  (DESC  -
*             T1692).
*
*   If the agreement already exists:
*
*        - load  the  screen  from  the  two records read (AGNT &
*             AGLF) looking up all descriptions,
*
*        - if there  are  bank  details on AGLF, put a '+' in the
*             bank account indicator,
*
*        - if there are  and broker contact records (READR AGBN),
*             put a '+' in the broker contacts indicator,
*
*        - If any of the  tied  agency details are not blank (see
*             S5036 for list of  fields),  put  a '+' in the tied
*             agent details indicator.
*
*   If  in enquiry  mode,  protect  all  fields  except  for  the
*   indicators at the bottom.
*
*  Validation
*  ----------
*
*   Skip this section  if  returning  from  an optional selection
*   (current stack position action flag = '*').
*
*   If in enquiry mode, skip all field validation.  The selection
*   fields must still be validated though.
*
*   Validate the screen  according  to  the  rules defined by the
*   field help. For all fields which have descriptions, look them
*   up again if  the  field  has been changed (CHG indicator from
*   DDS keyword).
*
*   If 'CALC' was pressed, re-display the screen.
*
*  Updating
*  --------
*
*   Skip this section  if  returning  from  an optional selection
*   (current stack position action flag = '*').
*
*   If any of the  options  have  been  selected, store the agent
*   details in the AGLF and AGNT I/O modules.
*
*   If options have not been selected,
*
*        - update the CLRR  (client  roles)  file  for  the Agent
*             role.   If  the  client  number  has  change  (AGNT
*             compared to  screen),  delete the "old" client role
*             and add the new  one.  If there was no agent record
*             before, just add a client role:
*             - client prefix to CN
*             - client company to FSU company
*             - role to AG
*             - foreign key prefix to AG
*             - foreign key company to sign-on company
*             - foreign key to agent number
*
*        - update the CLRR  (client  roles)  file  for  the Payee
*             role.  If the  payee client number has change (AGNT
*             compared to  screen),  delete the "old" client role
*             and add the new  one.  (NB,  if  the  payee is left
*             blank, the main  client  number  is used.) If there
*             was no agent record before, just add a client role:
*             - client prefix to CN
*             - client company to FSU company
*             - role to PE
*             - foreign key prefix to AG
*             - foreign key company to sign-on company
*             - foreign key to agent number
*
*        - update the AGNT file:
*             - prefix to AG
*             - company to sign-on company
*             - valid flag to 1
*             - life agent flag to Y
*             - client prefix to CN
*             - client company to FSO company
*             - client number as entered
*
*        - update the AGLF file:
*             - all fields from the screen (or blank/zero)
*
*        - write an AGMO record:
*             - batch key from WSSP
*             - all fields from the screen (or blank/zero)
*             - transaction  effective   date   to   TDAY   (from
*                  DATCON1),
*
*        -  update the batch header  by  calling  BATCUP  with  a
*             function of WRITS and the following parameters:
*             - transaction count = 1,
*             - all other amounts zero,
*             - batch key from WSSP.
*
*        - release the soft  lock on the agent (call SFTLOCK with
*             function UNLK).
*
*  Next Program
*  ------------
*
*   Check each 'selection'  indicator  in  turn.  If an option is
*   selected,  call  GENSSWCH  with  the  appropriate  action  to
*   retrieve the optional program switching:
*             A - Tied Agent details
*             B - Bank Account details
*             C - Agency/Broker contacts
*             D - Client Details
*
*   For the first one selected, save the next set of programs (8)
*   from the program  stack  and  flag the current positions with
*   '*'.
*
*   If a selection  had  been  made  previously, its select field
*   would contain a  '?'.  In  this  case,  retrieve  the  latest
*   versions  of AGNT  and  AGLF  and  cross  check  the  details
*   according to the rules defined in the initialisation section.
*   Set the selection to  blank  if  there are now no details, or
*   '+' if there are.
*
*   If a selection  is made (selection field X), load the program
*   stack with the programs returned by GENSSWCH, replace the 'X'
*   in the selection field  with  a '?', add 1 to the pointer and
*   exit.
*
*   Once all the selections have been serviced, re-load the saved
*   programs onto the stack  and  blank out the '*'. Set the next
*   program name to  the  current  screen name (to re-display the
*   screen).
*
*   If nothing is  selected,  continue  by just adding one to the
*   program pointer.
*
*****************************************************************
* </pre>
*/
public class P5035 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(P5035.class);
	//ILJ-4
	private static final String feaconfigID_ILJ = "AGMTN019";
	private boolean iljFlag = false;
	//end
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5035");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaOldClntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaOldPayee = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgnrefcd = new FixedLengthStringData(20);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private String wsaaTrigger = "";
	private String wsaaRecordFound = "";
	private FixedLengthStringData wsaaInitRepsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaZrptgb = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZrptgc = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaZrptgd = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbReportag = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStore1Agnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStore1Zrptga = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStore1Effdate = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaStore1Agmvty = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStore1Tranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaStorexAgnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStorexZrptga = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStorexEffdate = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaStorexAgmvty = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStorexTranno = new PackedDecimalData(5, 0);
	private String wsaaTmpAgmvty = "";
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private FixedLengthStringData wsaaOrindic = new FixedLengthStringData(1).init(" ");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(5, 8, wsaaReportags, 0);
	private FixedLengthStringData wsaaSaveFlag = new FixedLengthStringData(1);
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

		/*    Flags to determine whether new or existing Agent details.*/
	protected FixedLengthStringData wsaaAgentExistFlag = new FixedLengthStringData(1);
	private Validator wsaaAgreemntYes = new Validator(wsaaAgentExistFlag, "Y");
	private Validator wsaaAgreemntNo = new Validator(wsaaAgentExistFlag, "N");
	private FixedLengthStringData wsaaSaveClntsel = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaRepsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRepselAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaRepsel, 0);
	private FixedLengthStringData wsaaRepselFill = new FixedLengthStringData(2).isAPartOf(wsaaRepsel, 8);

	private FixedLengthStringData wsaaAglfrptKey = new FixedLengthStringData(11);
	private FixedLengthStringData wsaaAgntpfx = new FixedLengthStringData(2).isAPartOf(wsaaAglfrptKey, 0);
	private FixedLengthStringData wsaaAgntcoy = new FixedLengthStringData(1).isAPartOf(wsaaAglfrptKey, 2);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAglfrptKey, 3);
	protected AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private MacfTableDAM macfIO = new MacfTableDAM();
	private MacfflvTableDAM macfflvIO = new MacfflvTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Agntkey wsaaAgntkey = new Agntkey();
	private Cltskey wsaaCltskey = new Cltskey();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private T3672rec t3672rec = new T3672rec();
	private T5690rec t5690rec = new T5690rec();
	private Th605rec th605rec = new Th605rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private S5035ScreenVars sv = ScreenProgram.getScreenVars( S5035ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaStoredScreenFieldsInner wsaaStoredScreenFieldsInner = new WsaaStoredScreenFieldsInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> T5690ListMap;
	private Map<String, List<Itempf>> T3672ListMap; 
	private Map<String, List<Itempf>> TH605ListMap;
	private List<Descpf> t5630List;
	private List<Descpf> t5644List;
	private List<Descpf> t5697List;
	private List<Descpf> t5696List;
	private List<Descpf>  t3692List;
	private List<Descpf>  t5690List;
	private List<Descpf>  t5699List;
	private List<Descpf>  TT518List;
	private List<Descpf>  t1692List;
	private List<Descpf>  T5699List;
	private List<Descpf>  tt518List;
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	protected AgntpfDAO agntpfDAO =  getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	protected Agntpf agntlag = null;
	private Agntpf reportag = null;
	private Agntpf recruiter = null;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	//ILIFE-6896
	private AgbnpfDAO agbnpfDAO = getApplicationContext().getBean("agbnpfDAO", AgbnpfDAO.class);
	private AglflnbpfDAO aglflnbpfDAO = getApplicationContext().getBean("aglflnbpfDAOP5079",AglflnbpfDAO.class);
	private AgorpfDAO agorpfDAO = getApplicationContext().getBean("agorpfDAO", AgorpfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private Itempf itempf = null;
	private boolean isAgentMainteance=false;
	private boolean isAgencyMainteance=false;
	private boolean branchError1=false;
	private boolean areaError1=false;
	private boolean branchError2=false;
	private boolean areaError2=false;
	
	private String bankOutlet = "BO";
	private Aglfpf aglfpf= new Aglfpf();
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private Map<String,Descpf> descMap = new HashMap<>();
	
	private Agslpf agslpf = new Agslpf();
	private AgslpfDAO agslpfDAO = getApplicationContext().getBean("agslpfDAO", AgslpfDAO.class);
	private boolean isLicenseFields= false;

	private String bnkIntrnalRole="BM";
	private AgqfpfDAO agqfpfDAO = getApplicationContext().getBean("agqfpfDAO", AgqfpfDAO.class);
	private AgntcypfDAO agntcypfDAO = getApplicationContext().getBean("agntcypfDAO", AgntcypfDAO.class);
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private Agntcypf agntcypf;
	private Agncypf agncypf;

	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private Hierpf hierpf = new Hierpf();
	private HierpfDAO hierpfDAO = getApplicationContext().getBean("hierpfDAO",HierpfDAO.class);
	
	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);
	
	private FixedLengthStringData wsaaKey = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaroleCode = new FixedLengthStringData(2).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaahierCode = new FixedLengthStringData(8).isAPartOf(wsaaKey, 2);
	private FixedLengthStringData wsaatopLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaatoproleCode = new FixedLengthStringData(2).isAPartOf(wsaatopLevel, 0);
	private FixedLengthStringData wsaatophierCode = new FixedLengthStringData(8).isAPartOf(wsaatopLevel, 2);
	private FixedLengthStringData wsaaupLevel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaauproleCode = new FixedLengthStringData(2).isAPartOf(wsaaupLevel, 0);
	private FixedLengthStringData wsaauphierCode = new FixedLengthStringData(8).isAPartOf(wsaaupLevel, 2);
	
	private String wsaaGennum;
	private String role3;
	private FixedLengthStringData wsaaLevelno = new FixedLengthStringData(8);
	

	private static final String JL99 = "JL99";
	private static final String RUD0 = "RUD0";
	private static final String RUD1 = "RUD1";
	private static final String RUD2 = "RUD2";
	private static final String RUD4 = "RUD4";
	private static final String RUD5 = "RUD5";
	private static final String RUDU = "RUDU";
	private static final String RUDV = "RUDV";
	private static final String RUFQ = "RUFQ";
	private static final String RUFR = "RUFR";
	
	
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1090,
		recruiter1255,
		payee1260,
		payMethod1270,
		serviceCommDesc1420,
		renewalCommDesc1430,
		bonusCommDesc1440,
		continue2115,
		exit2179,
		exit2209,
		writeAgentClrr3215,
		clrrPayeeUpdte3220,
		writePayeeClrr3225,
		agntUpdte3230,
		m200UpdCont,
		m200Exit,
		alreadyKept4040,
		endChoices4050,
		exit4090,
		m001LevelCall,
		m001Next,
		m001Exit,
		m00xLevelCall,
		m00xNext,
		m00xExit,
		m00yLevelCall,
		m00yNext,
		m00yExit
	}

	public P5035() {
		super();
		screenVars = sv;
		new ScreenModel("S5035", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename(Clntpf clntpf)
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		wsspcomn.longconfname.set(clntpf.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname(Clntpf clntpf)
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename(Clntpf clntpf)
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if ("C".equals(clntpf.getClttype())) {
			corpname(clntpf);
			return ;
		}
		if ("1".equals(clntpf.getEthorig())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpf.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpf.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpf.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname(Clntpf clntpf)
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			retrvAglflnb1010();
			readrAgentFile1020();
			agentProcess1030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
	//ilj-4
	
		iljFlag = FeaConfg.isFeatureExist("9", feaconfigID_ILJ, appVars, "IT");
		
		//end
	
		/*    Skip  this section if  returning from an optional selection*/
		/*    (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit1090);
		}
		/*    Initialise working storage and screen fields.*/
		aglfIO.setDataArea(SPACES);
//		agntlagIO.setDataArea(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreAgtype.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreAracde.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreRepsel.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStorePayfrq.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreBcmtab.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreScmtab.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreRcmtab.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreOcmtab.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreAgentClass.set(SPACES);
		wsaaStoredScreenFieldsInner.wsaaStoreTsalesunt.set(SPACES);
		wsspcomn.confirmationKey.set(SPACES);
		wsspcomn.msgarea.set(SPACES);
		wsaaSaveClntsel.set(SPACES);
		sv.tlaglicno.set(SPACES);
		sv.tagsusind.set(SPACES);
		wsaaReportags.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.dteapp.set(varcom.vrcmMaxDate);
		sv.tlicexpdt.set(varcom.vrcmMaxDate);
		sv.dtetrm.set(varcom.vrcmMaxDate);
		sv.minsta.set(ZERO);
		sv.ovcpc.set(ZERO);	
		sv.agncysel.set(SPACE);
		sv.agncydesc.set(SPACE);
		isAgencyMainteance = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN020", appVars, "IT");
		if(isAgencyMainteance){
			sv.agncyselOut[varcom.nd.toInt()].set("N");
			sv.salebranchOut[Varcom.nd.toInt()].set("N");
			sv.salebranch.set(SPACE);
			sv.salebranchdes.set(SPACE);
			
		}
		else{
			sv.agncyselOut[varcom.nd.toInt()].set("Y");
			sv.salebranchOut[Varcom.nd.toInt()].set("Y");
			
		}
			
		//ICIL-12 Start
		isAgentMainteance = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN015", appVars, "IT");
		if(!isAgentMainteance){
			sv.agentrefcodeOut[varcom.nd.toInt()].set("Y");
			sv.agencymgrOut[varcom.nd.toInt()].set("Y");
			sv.distrctcodeOut[varcom.nd.toInt()].set("Y");
			sv.bnkinternalroleOut[varcom.nd.toInt()].set("Y");
			sv.banceindicatorselOut[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.agentrefcodeOut[varcom.nd.toInt()].set("N");
			sv.agencymgrOut[varcom.nd.toInt()].set("N");
			sv.distrctcodeOut[varcom.nd.toInt()].set("N");
			sv.bnkinternalroleOut[varcom.nd.toInt()].set("N");
			sv.banceindicatorselOut[varcom.nd.toInt()].set("N");
		}
		//ICIL-12 End
		
		//ILJ-4 start
		if(iljFlag) {
			sv.iljScreenflag.set("Y");
		}
		else {
			sv.iljScreenflag.set("N");
		}
		//end
			
	}

protected void retrvAglflnb1010()
	{
		/*    Retreive the life agent details.*/
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		sv.agnum.set(aglfIO.getAgntnum());
		wsspcomn.confirmationKey.set(aglfIO.getAgntnum());
	}

protected void readrAgentFile1020()
	{
	agntlag = agntpfDAO.getAgntRec(wsspcomn.company.toString(), aglfIO.getAgntnum().toString());
	if (agntlag == null) {
		if (isEQ(wsspcomn.flag, "I")) {
			sv.repselErr.set("E475");
		}
		else {
			wsaaAgentExistFlag.set("N");
		}
	} else {
		wsaaAgentExistFlag.set("Y");
	}
	getOrindic1800();

	}

protected void agentProcess1030()
	{
		/*    Perform sections according to agent details flag.*/
		if (wsaaAgreemntNo.isTrue()) {
			agentNonExist1100();
		}
		if (wsaaAgreemntYes.isTrue()) {
			agentExists1200();
		}
		//#ILJ-53----Start
		if (isEQ(wsspcomn.flag, "A")){
		if (isEQ(th605rec.indic, "Y") && isEQ(th605rec.crtind, "Y"))  {
			/*       MOVE 'Y'                 TO S5035-REPSEL-OUT(PR)  <V5L001>*/     
			/*       MOVE 'Y'                 TO S5035-REPSEL-OUT(ND)  <V5L001>*/
			sv.ovcpcOut[varcom.pr.toInt()].set("Y");
			sv.ovcpcOut[varcom.nd.toInt()].set("Y");
			sv.repselOut[varcom.pr.toInt()].set("N");
			sv.repselOut[varcom.nd.toInt()].set("N");
			sv.zrorindOut[varcom.pr.toInt()].set("N");
			sv.zrorindOut[varcom.nd.toInt()].set("N");
			
		}else if (isEQ(th605rec.indic, "Y") && isEQ(th605rec.crtind, ""))  {
			
			sv.ovcpcOut[varcom.pr.toInt()].set("Y");
			sv.ovcpcOut[varcom.nd.toInt()].set("Y");
			sv.repselOut[varcom.pr.toInt()].set("N");
			sv.repselOut[varcom.nd.toInt()].set("N");
			sv.zrorindOut[varcom.pr.toInt()].set("N");
			sv.zrorindOut[varcom.nd.toInt()].set("N");
			
		}else if (isEQ(th605rec.indic, "") && isEQ(th605rec.crtind, ""))  {
			
			sv.ovcpcOut[varcom.pr.toInt()].set("N");
			sv.ovcpcOut[varcom.nd.toInt()].set("N");
			sv.repselOut[varcom.pr.toInt()].set("N");
			sv.repselOut[varcom.nd.toInt()].set("N");
			sv.zrorindOut[varcom.pr.toInt()].set("Y");
			sv.zrorindOut[varcom.nd.toInt()].set("Y");
			
		}else if (isEQ(th605rec.indic, "") && isEQ(th605rec.crtind, "Y")){ 							
			
			sv.ovcpcOut[varcom.pr.toInt()].set("N");
			sv.ovcpcOut[varcom.nd.toInt()].set("N");
			sv.repselOut[varcom.pr.toInt()].set("N");
			sv.repselOut[varcom.nd.toInt()].set("N");
			sv.zrorindOut[varcom.pr.toInt()].set("Y");
		    sv.zrorindOut[varcom.nd.toInt()].set("Y");
			
		}
		}
		
		if (isEQ(wsspcomn.flag, "M")){
			if (isEQ(th605rec.indic, "Y") && isEQ(th605rec.crtind, "Y"))  {
				/*       MOVE 'Y'                 TO S5035-REPSEL-OUT(PR)  <V5L001>*/     
				/*       MOVE 'Y'                 TO S5035-REPSEL-OUT(ND)  <V5L001>*/
						
				sv.ovcpcOut[varcom.pr.toInt()].set("Y");
				sv.ovcpcOut[varcom.nd.toInt()].set("Y");
				sv.repselOut[varcom.pr.toInt()].set("Y");
				sv.repselOut[varcom.nd.toInt()].set("Y");
				sv.zrorindOut[varcom.pr.toInt()].set("N");
				sv.zrorindOut[varcom.nd.toInt()].set("N");
				
			}else if (isEQ(th605rec.indic, "Y") && isEQ(th605rec.crtind, ""))  {
				
				sv.ovcpcOut[varcom.pr.toInt()].set("Y");
				sv.ovcpcOut[varcom.nd.toInt()].set("Y");
				sv.repselOut[varcom.pr.toInt()].set("N");
				sv.repselOut[varcom.nd.toInt()].set("N");
				sv.zrorindOut[varcom.pr.toInt()].set("N");
				sv.zrorindOut[varcom.nd.toInt()].set("N");
				
			}else if (isEQ(th605rec.indic, "") && isEQ(th605rec.crtind, ""))  {
				
				sv.ovcpcOut[varcom.pr.toInt()].set("N");
				sv.ovcpcOut[varcom.nd.toInt()].set("N");
				sv.repselOut[varcom.pr.toInt()].set("N");
				sv.repselOut[varcom.nd.toInt()].set("N");
				sv.zrorindOut[varcom.pr.toInt()].set("Y");
				sv.zrorindOut[varcom.nd.toInt()].set("Y");
				
			}else if (isEQ(th605rec.indic, "") && isEQ(th605rec.crtind, "Y")){ 							
				
				sv.ovcpcOut[varcom.pr.toInt()].set("N");
				sv.ovcpcOut[varcom.nd.toInt()].set("N");
				sv.repselOut[varcom.pr.toInt()].set("Y");
				sv.repselOut[varcom.nd.toInt()].set("Y");
				sv.zrorindOut[varcom.pr.toInt()].set("Y");
			    sv.zrorindOut[varcom.nd.toInt()].set("Y");
				
			}
			}
		//#ILJ-53----End
		
		t1692List = descDAO.getItems("IT", wsspcomn.company.toString(), "T1692");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t1692List) {
			if (descItem.getDescitem().trim().equals(sv.agntbr.toString().trim())){
				sv.agbrdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.agbrdesc.set("??????????????????????????????");
		}	
		
		/*    If entering on enquiry then protect the client number.*/
		if (isEQ(wsspcomn.flag, "I")) {
			sv.clntselOut[varcom.pr.toInt()].set("Y");
			sv.repselOut[varcom.pr.toInt()].set("Y");
			sv.ovcpcOut[varcom.pr.toInt()].set("Y");
			sv.dteappOut[varcom.pr.toInt()].set("Y");
			sv.agtypeOut[varcom.pr.toInt()].set("Y");
			sv.agntbrOut[varcom.pr.toInt()].set("Y");
			sv.dtetrmOut[varcom.pr.toInt()].set("Y");
			//TMLI-281 AG-01-002
			sv.zrecruitOut[varcom.pr.toInt()].set("Y");

			//ICIL-12 start	
			sv.agentrefcodeOut[varcom.pr.toInt()].set("Y");
			sv.agencymgrOut[varcom.pr.toInt()].set("Y");
			sv.distrctcodeOut[varcom.pr.toInt()].set("Y");
			sv.bnkinternalroleOut[varcom.pr.toInt()].set("Y");
			sv.banceindicatorselOut[varcom.pr.toInt()].set("Y");
			//ICIL-12 end	
			sv.agncyselOut[varcom.pr.toInt()].set("Y");
			sv.salebranchOut[Varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.flag, "T")
		|| isEQ(wsspcomn.flag, "R")) {
			sv.agbrdescOut[varcom.pr.toInt()].set("Y");
			sv.agclsOut[varcom.pr.toInt()].set("Y");
			sv.agclsdOut[varcom.pr.toInt()].set("Y");
			sv.agntbrOut[varcom.pr.toInt()].set("Y");
			sv.agnumOut[varcom.pr.toInt()].set("Y");
			sv.agtydescOut[varcom.pr.toInt()].set("Y");
			sv.aracdeOut[varcom.pr.toInt()].set("Y");
			sv.bcmdescOut[varcom.pr.toInt()].set("Y");
			sv.bcmtabOut[varcom.pr.toInt()].set("Y");
			sv.bctindOut[varcom.pr.toInt()].set("Y");
			sv.clientindOut[varcom.pr.toInt()].set("Y");
			sv.clntselOut[varcom.pr.toInt()].set("Y");
			sv.cltnameOut[varcom.pr.toInt()].set("Y");
			sv.currcodeOut[varcom.pr.toInt()].set("Y");
			sv.ddindOut[varcom.pr.toInt()].set("Y");
			sv.dteappOut[varcom.pr.toInt()].set("Y");
			sv.excagrOut[varcom.pr.toInt()].set("Y");
			sv.minstaOut[varcom.pr.toInt()].set("Y");
			sv.ocmdescOut[varcom.pr.toInt()].set("Y");
			sv.ocmtabOut[varcom.pr.toInt()].set("Y");
			sv.payfrqOut[varcom.pr.toInt()].set("Y");
			sv.paymthOut[varcom.pr.toInt()].set("Y");
			sv.pyfdescOut[varcom.pr.toInt()].set("Y");
			sv.pymdescOut[varcom.pr.toInt()].set("Y");
			sv.rcmdescOut[varcom.pr.toInt()].set("Y");
			sv.rcmtabOut[varcom.pr.toInt()].set("Y");
			sv.repselOut[varcom.pr.toInt()].set("Y");
			sv.ovcpcOut[varcom.pr.toInt()].set("Y");
			sv.repnameOut[varcom.pr.toInt()].set("Y");
			sv.scmdscOut[varcom.pr.toInt()].set("Y");
			sv.scmtabOut[varcom.pr.toInt()].set("Y");
			sv.tagdOut[varcom.pr.toInt()].set("Y");
			sv.agtypeOut[varcom.pr.toInt()].set("Y");
			sv.agntbrOut[varcom.pr.toInt()].set("Y");
			//TMLI-281 AG-01-002
			sv.zrecruitOut[varcom.pr.toInt()].set("Y");

			//ICIL-12 start	
			sv.agentrefcodeOut[varcom.pr.toInt()].set("Y");
			sv.agencymgrOut[varcom.pr.toInt()].set("Y");
			sv.distrctcodeOut[varcom.pr.toInt()].set("Y");
			sv.bnkinternalroleOut[varcom.pr.toInt()].set("Y");
			sv.banceindicatorselOut[varcom.pr.toInt()].set("Y");
			
			//ICIL-12 end	
			sv.agncyselOut[varcom.pr.toInt()].set("Y");
			sv.salebranchOut[Varcom.pr.toInt()].set("Y");
		}
		/* Reporting To and Date Terminated fields are protected if        */
		/* agency movement functions are activated.                        */
		if (isEQ(th605rec.crtind, "Y")) {
			if (isNE(wsspcomn.flag, "A")) {
				sv.agnumOut[varcom.pr.toInt()].set("Y");
				//#ILJ-53--Start
				if (isNE(wsspcomn.flag, "A") && (isNE(wsspcomn.flag, "M"))) {
				sv.ovcpcOut[varcom.pr.toInt()].set("Y");
				sv.repselOut[varcom.pr.toInt()].set("Y");
				}
				//#ILJ-53--End
			}
		}
		if (isEQ(th605rec.crtind, "Y")) {
			if (isNE(wsspcomn.flag, "T")
			&& isNE(wsspcomn.flag, "R")) {
				sv.dtetrmOut[varcom.pr.toInt()].set("Y");
			}
		}
		
		/**
		 * ICIL-6 
		 * 
		 */
		isLicenseFields = FeaConfg.isFeatureExist(aglfIO.getAgntcoy().toString().trim(), "AGMTN014", appVars, "IT");//BSD-ICIL-6
		
		if(isLicenseFields){
			sv.agncysalicOut[varcom.nd.toInt()].set("N");
		}else{
			sv.agncysalicOut[varcom.nd.toInt()].set("Y");
		}
	}

protected void agentNonExist1100()
	{
		/*BRANCH-DETAILS*/
		sv.agntbr.set(wsspcomn.branch);
		wsaaOldClntnum.set(SPACES);
		wsaaOldPayee.set(SPACES);
		/*EXIT*/
	}

protected void agentExists1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					loadScreen1210();
					clientDets1220();
					agentType1230();
					agentArea1240();
					reportingTo1250();
					//TMLII-281 AG-01-002 START
					case recruiter1255:{
						
						recruiter1255();
					}
					//TMLII-281 AG-01-002 END

				case payee1260:
					payee1260();
				case payMethod1270:
					payMethod1270();
					payFrequency1280();
					agentClass1300();
					checkSelectIndicators1310();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loadScreen1210()
	{
		/*    Load screen with existing details.*/
		sv.agntbr.set(agntlag.getAgntbr());
		sv.currcode.set(aglfIO.getCurrcode());
		sv.agnum.set(aglfIO.getAgntnum());
		sv.dteapp.set(aglfIO.getDteapp());
		sv.exclAgmt.set(aglfIO.getExclAgmt());
		sv.ovcpc.set(aglfIO.getOvcpc());
		sv.minsta.set(aglfIO.getMinsta());
		sv.tagsusind.set(aglfIO.getTagsusind());
		sv.tlaglicno.set(aglfIO.getTlaglicno());
		sv.tlicexpdt.set(aglfIO.getTlicexpdt());
		//ICIL-12 start
		if(isAgentMainteance){
		sv.agentrefcode.set(agntlag.getAgentrefcode());
		wsaaAgnrefcd.set(sv.agentrefcode);
		sv.agencymgr.set(agntlag.getAgencymgr());
		sv.distrctcode.set(agntlag.getDistrctcde());
		sv.bnkinternalrole.set(agntlag.getBnkinternalrole());
		sv.banceindicatorsel.set(agntlag.getBanceindicatorsel());
		}		
		//ICIL-12 end
		if (isNE(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
			sv.dtetrm.set(aglfIO.getDtetrm());
		}
		else {
			sv.dtetrm.set(varcom.vrcmMaxDate);
		}
		wsaaOldClntnum.set(agntlag.getClntnum());
		if (isNE(aglfIO.getPayclt(), SPACES)) {
			wsaaOldPayee.set(aglfIO.getPayclt());
		}
		else {
			wsaaOldPayee.set(agntlag.getClntnum());
		}
		if(isAgencyMainteance){
			agntcypf=new Agntcypf();
			agntcypf=agntcypfDAO.getAgncy(wsspcomn.company.toString(), sv.agnum.toString(), 1);
			if(null !=agntcypf) {
				sv.agncysel.set(agntcypf.getAgncynum());
				agncypf=agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim(), 1);
				if(agncypf!=null) {
					getAgncydesc(agncypf.getClntnum());
				}
			}
			
			sv.salebranch.set(SPACES);
			sv.salebranchdes.set(SPACES);
			
			String role = "AG";
			String levelno = role.concat(sv.agnum.trim());
			
			Hierpf hrpf = hierpfDAO.getHierpfData(levelno);
			Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
			if (isPrsnt.isPresent())
			{
				 if (isEQ(wsspcomn.flag, "M"))
				 {
					 if(isEQ(hrpf.getValidflag(),"1") && isEQ(hrpf.getActivestatus(),"1"))
					 {
						 	sv.salebranch.set(hrpf.getUpperlevel().substring(2));
						 	
						 	salesbranchName();
				
					 }
					 else
					 {
						 sv.salebranch.set(SPACES);
						 sv.salebranchdes.set(SPACES);
					 }
					 
					 if (isNE(aglfIO.getDtetrm(), varcom.vrcmMaxDate)) {
						 sv.agncyselOut[varcom.pr.toInt()].set("Y");
						 sv.salebranchOut[Varcom.pr.toInt()].set("Y");
						 sv.salebranch.set(hrpf.getUpperlevel().substring(2));
						 salesbranchName();
					 }
				 }
				 else if(isEQ(wsspcomn.flag, "I"))
				 {
					 if(isEQ(hrpf.getValidflag(),"1") && isEQ(hrpf.getActivestatus(),"2") && isEQ(hrpf.getRegisclass(),"3")){
					 	 sv.salebranch.set(SPACES);
					 	 sv.salebranchdes.set(SPACES);
					 }
					 else
					 {
						 sv.salebranch.set(hrpf.getUpperlevel().substring(2));
						 salesbranchName();
					 }
				 }
				 else 
				 {
					 sv.salebranch.set(hrpf.getUpperlevel().substring(2));
					 salesbranchName();
				 }
				wsaaLevelno.set(sv.salebranch.trim());
			}
			else{
				sv.salebranch.set(SPACES);
				sv.salebranchdes.set(SPACES);
			}
		}
	}

	protected void getAgncydesc(String agncynum) {
		sv.agncydesc.set(SPACES);
		Clntpf clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), agncynum);
		if ("C".equals(clntpf.getClttype())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getLsurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpf.getLgivname(), "  ");
			stringVariable1.setStringInto(sv.agncydesc);
		} else if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(sv.agncydesc);
		} else {
			sv.agncydesc.set(clntpf.getSurname());
		}
	}

protected void clientDets1220()
	{
		/*    Read the Clients file for processing the clients name.*/
		sv.clntsel.set(agntlag.getClntnum());
		wsaaSaveClntsel.set(agntlag.getClntnum());
		cltsioCall1700(agntlag.getClntnum());
		sv.cltname.set(wsspcomn.longconfname);
		sv.clientind.set("+");
	}

protected void agentType1230()
	{
		/*    Read the Agent type and description .*/
		sv.agtype.set(agntlag.getAgtype());
		t3692List = descDAO.getItems("IT", wsspcomn.company.toString(), "T3692");/* IJTI-1386 */		
		boolean itemFound = false;
		for (Descpf descItem : t3692List) {
			if (descItem.getDescitem().trim().equals(agntlag.getAgtype().trim())){/* IJTI-1386 */
				sv.agtydesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.agtydesc.set("??????????????????????????????");
		}	
	}




protected void salesbranchName()
	{
		Agsdpf agsd = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
		if(agsd != null) {
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsd.getClientno().trim());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.salebranchdes.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
		}
	}

protected void agentArea1240()
	{
		/*    Read the Agent Area and description.*/
		sv.aracde.set(aglfIO.getAracde());
		sv.tsalesunt.set(aglfIO.getTsalesunt());
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put("T5696", aglfIO.getAracde().toString().trim());
		itemMap.put("TT518", aglfIO.getTsalesunt().toString().trim());
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);
		boolean itemFound = false;
		Descpf t5696Desc= new Descpf();
		if( descMap==null || descMap.isEmpty() || descMap.get("T5696") ==null) {
			sv.aradesc.set("??????????????????????????????");
		}
		else {
			t5696Desc = descMap.get("T5696");
			sv.aradesc.set(t5696Desc.getLongdesc());
		}
		/*t5696List = descDAO.getItems("IT".toString(), wsspcomn.company.toString(), "T5696");			
		for (Descpf descItem : t5696List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getAracde().toString().trim())){
				sv.aradesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}*/
		Descpf tt518Desc= new Descpf();
		if( descMap==null || descMap.isEmpty() || descMap.get("TT518") ==null) {
			sv.tsalesdsc.set("??????????????????????????????");
		}
		else
		{
			tt518Desc = descMap.get("TT518");
			sv.tsalesdsc.set(tt518Desc.getLongdesc());
		}
		/*tt518List = descDAO.getItems("IT".toString(), wsspcomn.company.toString(), "TT518");		
		itemFound = false;		
		for (Descpf descItem : tt518List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getTsalesunt().toString().trim())){
				sv.tsalesdsc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}		
		if (!itemFound) {
			sv.tsalesdsc.set("??????????????????????????????");
		}	*/			
		
		if (isEQ(sv.tsalesunt, SPACES)) {
			sv.tsalesdsc.set(SPACES);
		}
	}

protected void reportingTo1250()
	{
		/*    Read the Agents reporting to name from the clients file.*/
		sv.repsel.set(aglfIO.getReportag());
		wsaaInitRepsel.set(aglfIO.getReportag());
		sv.repname.set(SPACES);
		if (isEQ(aglfIO.getReportag(), SPACES)) {
			//goTo(GotoLabel.payee1260);
			/*TMLII-281*/
			goTo(GotoLabel.recruiter1255);

		}
		reportag = agntlagioCall1500(aglfIO.getReportag().toString());
		if (reportag == null) {
			syserrrec.params.set(aglfIO.getReportag());
			fatalError600();
		}
		cltsioCall1700(reportag.getClntnum());
		sv.repname.set(wsspcomn.longconfname);
	}
//TMLII-281 AG-01-002 START
protected void recruiter1255()
{
	sv.zrecruit.set(aglfIO.getZrecruit());
	sv.agtname.set(SPACES);
	if (isEQ(aglfIO.getZrecruit(),SPACES)) {
		goTo(GotoLabel.payee1260);
	}
	recruiter = agntlagioCall1500(aglfIO.getZrecruit().toString());
	if (recruiter == null) {
		syserrrec.params.set(aglfIO.getZrecruit());
		fatalError600();
	}
	cltsioCall1700(recruiter.getClntnum());
	sv.agtname.set(wsspcomn.longconfname);
}
//TMLII-281 AG-01-002 END

protected void payee1260()
	{
		/*    Read the Payee name from the clients file.*/
		if (isEQ(aglfIO.getPayclt(), SPACES)) {
			goTo(GotoLabel.payMethod1270);
		}
		if (isEQ(aglfIO.getPayclt(), agntlag.getClntnum())
		&& isNE(aglfIO.getAgccqind(), "Y")) {
			sv.paysel.set(SPACES);
			goTo(GotoLabel.payMethod1270);
		}
		sv.paysel.set(aglfIO.getPayclt());
		sv.payenme.set(SPACES);
		cltsioCall1700(aglfIO.getPayclt().toString());
		sv.payenme.set(wsspcomn.longconfname);
	}

protected void payMethod1270()
	{
		/*    Read the Payment method and description.*/
		sv.paymth.set(aglfIO.getPaymth());
		t5690List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5690");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5690List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getPaymth().toString().trim())){
				sv.pymdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.pymdesc.set("??????????????????????????????");
		}
	}

protected void payFrequency1280()
	{
		/*    Read the Payment Frequency and its description.*/
		sv.payfrq.set(aglfIO.getPayfrq());
		
		t5630List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5630");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5630List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getPayfrq().toString().trim())){
				sv.pyfdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.pyfdesc.set("??????????????????????????????");
		}		
		/*COMMISSION-RATES*/
		/*    The four commissions are handled in a distinct section.*/
		commTableCall1400();
	}

protected void agentClass1300()
	{
		/*    Read the agent Class description.*/
		sv.agentClass.set(aglfIO.getAgentClass());
		T5699List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5699");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : T5699List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getAgentClass().toString().trim())){
				sv.agclsd.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.agclsd.set("??????????????????????????????");
		}	
		
		
		/* Protect the Client Number.                                      */
		/*  MOVE 'Y'                    TO S5035-CLNTSEL-OUT(PR).        */
		sv.clntselOut[varcom.pr.toInt()].set("Y");
	}

protected void checkSelectIndicators1310()
	{
		/*    The following have been named A,B & C for use within the*/
		/*    1000 section and the 4000 section.*/
		/*    The first will check for changes in the bank details through*/
		/*    the s5037 screen.*/
		/*    The second  will check  for  changes to  the Broker Contacts*/
		/*    details through screen s5038.*/
		/*    The third  will check for  changes to the Tied Agent details*/
		/*    through screen s5036.*/
		/*    Note: 1000 section will only set the flags according to what*/
		/*          it finds in the AGLF and AGNTLAG I/O Modules.*/
		a100BankDetsCheck();
		b100BrokerContacts();
		c100TiedAgencies();
		/* PERFORM D100-OR-DETAILS.                                     */
		d200OrDetails();
		/*EXIT*/
		checkQalification();
	}

protected void commTableCall1400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					basicCommDesc1410();
				case serviceCommDesc1420:
					serviceCommDesc1420();
				case renewalCommDesc1430:
					renewalCommDesc1430();
				case bonusCommDesc1440:
					bonusCommDesc1440();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void basicCommDesc1410()
	{
		/*    Find the basic commission description.*/
		sv.bcmtab.set(aglfIO.getBcmtab());
		if (isEQ(sv.bcmtab, SPACES)) {
			goTo(GotoLabel.serviceCommDesc1420);
		}
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getBcmtab().toString().trim())){
				sv.bcmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.bcmdesc.set("??????????????????????????????");
		}	
		
	}

protected void serviceCommDesc1420()
	{
		/*    Find the Service commission description.*/
		sv.scmtab.set(aglfIO.getScmtab());
		if (isEQ(sv.scmtab, SPACES)) {
			goTo(GotoLabel.renewalCommDesc1430);
		}
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getScmtab().toString().trim())){
				sv.scmdsc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.scmdsc.set("??????????????????????????????");
		}			
	}

protected void renewalCommDesc1430()
	{
		/*    Find the Renewal commission description.*/
		sv.rcmtab.set(aglfIO.getRcmtab());
		if (isEQ(sv.rcmtab, SPACES)) {
			goTo(GotoLabel.bonusCommDesc1440);
		}
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getRcmtab().toString().trim())){
				sv.rcmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.rcmdesc.set("??????????????????????????????");
		}	
		
	}

protected void bonusCommDesc1440()
	{
		/*    Find the Bonus commission description.*/
		sv.ocmtab.set(aglfIO.getOcmtab());
		if (isEQ(sv.ocmtab, SPACES)) {
			return ;
		}
		t5697List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5697");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5697List) {
			if (descItem.getDescitem().trim().equals(aglfIO.getOcmtab().toString().trim())){
				sv.ocmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.ocmdesc.set("??????????????????????????????");
		}	
		
		
		/*EXIT*/
	}

	protected Agntpf agntlagioCall1500(String agentNum) {
		/* CALL */
		/* Read the Agent details. */
		return agntpfDAO.getAgntRec(wsspcomn.company.toString(), agentNum);
		/* EXIT */
	}

	protected void cltsioCall1700(String clntNum) {
		Clntpf clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),
				clntNum);
		if (clntpf == null) {
			syserrrec.params.set(clntNum);
			fatalError600();
		}
		/* Call the Subroutine to format the Clients Name. */
		plainname(clntpf);
		/* EXIT */
	}

protected void getOrindic1800(){
	TH605ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "TH605");
	String keyItemitem = wsspcomn.company.toString();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;
	if (TH605ListMap.containsKey(keyItemitem.trim())) 
	{
		itempfList = TH605ListMap.get(keyItemitem.trim());
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {

			Itempf itempf = new Itempf();
			itempf = iterator.next();
			if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
				 
					th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				 

			} else {
					th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
			}
		}
	}
}

protected void a100BankDetsCheck()
	{
		/*A110-BANK-DETS-CHECK*/
		if (isNE(aglfIO.getBankkey(), SPACES)
		|| isNE(aglfIO.getBankacckey(), SPACES)) {
			sv.ddind.set("+");
		}
		else {
			sv.ddind.set(" ");
		}
		/*A100-EXIT*/
	}

	protected void b100BrokerContacts() {
		// ILIFE-8896
		List<Agbnpf> agbnList = agbnpfDAO.searchAgbnpfRecord(wsspcomn.company.toString(), aglfIO.getAgntnum().toString());
		if (agbnList.size() > 0) {
			sv.bctind.set("+");
		} else {
			sv.bctind.set(" ");
		}

	}

protected void c100TiedAgencies()
	{
		/*C110-TIED-AGENCIES*/
		if (isNE(aglfIO.getTaxmeth(), SPACES)
		|| isNE(aglfIO.getIrdno(), SPACES)
		|| isNE(aglfIO.getTaxcde(), SPACES)
		|| isNE(aglfIO.getTaxalw(), ZERO)
		|| isNE(aglfIO.getSprschm(), SPACES)
		|| isNE(aglfIO.getSprprc(), ZERO)
		|| isNE(aglfIO.getIntcrd(), ZERO)
		|| isNE(aglfIO.getTcolprct(), ZERO)
		|| isNE(aglfIO.getTcolmax(), ZERO)
		|| isNE(aglfIO.getFixprc(), ZERO)
		|| isNE(aglfIO.getBmaflg(), SPACES)
		|| isNE(aglfIO.getHouseLoan(), SPACES)
		|| isNE(aglfIO.getComputerLoan(), SPACES)
		|| isNE(aglfIO.getCarLoan(), SPACES)
		|| isNE(aglfIO.getOfficeRent(), SPACES)
		|| isNE(aglfIO.getOtherLoans(), SPACES)
		|| isEQ(aglfIO.getAgccqind(), "Y")) {
			sv.tagd.set("+");
		}
		else {
			sv.tagd.set(" ");
		}
		/*C100-EXIT*/
	}

	/**
	* <pre>
	***************************                               <V4L014>
	* </pre>
	*/
	protected void d200OrDetails() {
		//ILIFE-6896
		Agorpf agorpf = new Agorpf();
		agorpf.setAgntnum(aglfIO.getAgntnum().toString().trim());
		agorpf.setAgntcoy(aglfIO.getAgntcoy().toString().trim());

		List<Agorpf> agorpfList = agorpfDAO.searchAgorpfRecord(agorpf);
		if (agorpfList.size() <= 0) {
			sv.zrorind.set(" ");
			return;
		} else {
			sv.zrorind.set("+");
		}

	}
protected void checkagencysaleindi(){
	List<Agslpf> list = agslpfDAO.getAgslpfByAgentNum(aglfIO.getAgntnum().toString());
	if(list.size() > 0){
		sv.agncysalic.set("+");
	}else{
		sv.agncysalic.set(" ");
	}
}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    Skip  this section if  returning from an optional selection  */
		/*    (current stack position action flag = '*').                  */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validateSelect2020();
		enquiryCheck2030();
	}

protected void screenIo2010()
	{
		/*    CALL 'S5035IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5035-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateSelect2020()
	{
		/*    Error if the Selections do not equal Space, + or X.*/
		if (isNE(sv.clientind, " ")
		&& isNE(sv.clientind, "+")
		&& isNE(sv.clientind, "X")) {
			sv.clientindErr.set("H118");
		}
		if (isNE(sv.ddind, " ")
		&& isNE(sv.ddind, "+")
		&& isNE(sv.ddind, "X")) {
			sv.ddindErr.set("H118");
		}
		/*MOVE  E186               TO S5035-DDIND-ERR.              */
		if (isNE(sv.bctind, " ")
		&& isNE(sv.bctind, "+")
		&& isNE(sv.bctind, "X")) {
			sv.bctindErr.set("H118");
		}
		/*MOVE  E186               TO S5035-BCTIND-ERR.             */
		/* IF S5035-ZRORIND         NOT = ' ' AND '+' AND 'X'           */
		/*    MOVE  "H118"               TO S5035-ZRORIND-ERR             */
		/* END-IF.                                                      */
		if (isNE(sv.zrorind, " ")
		&& isNE(sv.zrorind, "+")
		&& isNE(sv.zrorind, "X")) {
			sv.zrorindErr.set("H118");
		}
		if (isEQ(sv.dtetrm, varcom.vrcmMaxDate)
		&& isEQ(wsspcomn.flag, "T")) {
			sv.dtetrmErr.set("E186");
		}
		if (isNE(sv.dtetrm, varcom.vrcmMaxDate)
		&& isEQ(wsspcomn.flag, "R")) {
			sv.dtetrmErr.set("EV03");
		}
		if (isNE(sv.tagd, " ")
		&& isNE(sv.tagd, "+")
		&& isNE(sv.tagd, "X")) {
			sv.tagdErr.set("H118");
		}
	}

	/**
	* <pre>
	********MOVE  E186               TO S5035-TAGD-ERR.
	* </pre>
	*/
protected void enquiryCheck2030()
	{
		/*    If entering on enquiry then skip Validation.*/
		wsspcomn.chdrClntkey.set(sv.agtype);
		wsspcomn.clntkey.set(sv.clntsel);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		validateAllFields2100();
		/*CHECK-FOR-ERRORS*/
		/*    If an error exists on Validation set flag to loop through*/
		/*    2000 section.*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*REDISPLAY-ON-CALC*/
		/*    If 'CALC' was entered then re-display the screen.*/
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateAllFields2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					validateAllFields2110();
				case continue2115:
					continue2115();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateAllFields2110()
	{
		/*    Validate all necessary fields.*/
	/*    Validate Agency number.*/
	if(isAgencyMainteance) {
		if(isNE(sv.agncysel,SPACE)) {
			agncypf=agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim(), 1);
			if(agncypf==null || null == agncypf.getAgncynum()) {
				sv.agncyselErr.set("JL38");
				sv.agncydesc.set(SPACES);
			}else {
				getAgncydesc(agncypf.getClntnum());
			
			if (isEQ(sv.agncyselErr, SPACES))
			{
				if(null != agncypf.getBrnch()){
					if(isNE(sv.agntbr,agncypf.getBrnch())) {
						sv.agncyselErr.set(RUD1);
					}
				}
				if(null != agncypf.getAreacode()){
					if(isNE(sv.aracde,agncypf.getAreacode())){
						sv.agncyselErr.set(RUD2);
					}
				}
			 }
			}
			
		} else {
			sv.agncydesc.set(SPACES);
		}
		
		if (isNE(sv.salebranch, SPACE)) {
			
			salesbranchName();
			
			String role = "";
			agsdpf = agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
			if (null != agsdpf){
				if("1".equals(agsdpf.getLevelclass())) {
					sv.salebranchErr.set(JL99);
				}
				else if("2".equals(agsdpf.getLevelclass())) {
					if(isNE(agsdpf.getActivestatus(),"1") || isNE(agsdpf.getValidflag(),"1")) 
					{
						sv.salebranchErr.set(RUFQ);
					}
				}
				else if("3".equals(agsdpf.getLevelclass())) {
					if(isNE(agsdpf.getActivestatus(),"1") || isNE(agsdpf.getValidflag(),"1")) 
					{
						sv.salebranchErr.set(RUFR);
					}
				}
				
				if (isEQ(sv.salebranchErr, SPACES))
				{
					wsaaSalediv.set(agsdpf.getSalesdiv());
					if(isNE(sv.agntbr,wsaaBranch)) {
						
						if("2".equals(agsdpf.getLevelclass())){

							branchError1 = true;
						}
							
						
						else if("3".equals(agsdpf.getLevelclass())) {
							branchError2 = true;
						}
							
					}
				
					if(isNE(sv.aracde,wsaaArea)){
						
						if("2".equals(agsdpf.getLevelclass())) {
							areaError1 = true;
						}
							
						
						else if("3".equals(agsdpf.getLevelclass())) {
							areaError2 = true;
						}
							
					}
					
					if(branchError1)
					{
						sv.salebranchErr.set(RUD4);
					}
					if(branchError2)
					{
						sv.salebranchErr.set(RUDV);
					}
					if(areaError1)
					{
						sv.salebranchErr.set(RUD5);
					}
					if(areaError2)
					{
						sv.salebranchErr.set(RUDU);
					}
					if(branchError1 && areaError1)
					{
						sv.salebranchErr.set(RUD4);
						sv.salebranchdesErr.set(RUD5);
					}
					if(branchError2 && areaError2)
					{
						sv.salebranchErr.set(RUDV);
						sv.salebranchdesErr.set(RUDU);
					}
					
					branchError1=false;
					branchError2=false;
					areaError1=false;
					areaError2=false;
				}
			}
			
			if (null == agsdpf) {
					sv.salebranchErr.set(JL99);
			} 
			
		} else {
			sv.salebranch.set(SPACES);
			sv.salebranchdes.set(SPACES);
		}
		
	
		
		if (isNE(sv.salebranch, SPACE)  && isNE(sv.agncysel,SPACE)) {
			sv.salebranchErr.set(RUD0);
			sv.agncyselErr.set(RUD0);
		}
		
	}
	/*    Validate Client number.*/
		if (isEQ(sv.clntsel, SPACES)) {
			sv.clntselErr.set("E186");
		}
		else {
			/*       IF S5035-CLNTSEL-OUT (CHG)    = 'Y'*/
			chgClient2120();
		}
		if ((isNE(sv.clntsel, wsaaSaveClntsel))
		&& (isEQ(sv.ddind, "+"))) {
			sv.ddind.set("X");
			wsaaSaveClntsel.set(sv.clntsel);
		}
		if ((isEQ(sv.clntsel, wsaaSaveClntsel))
		&& (isEQ(sv.ddind, "+"))) {
			aglfIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, aglfIO);
			if (isNE(aglfIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(aglfIO.getParams());
				syserrrec.statuz.set(aglfIO.getStatuz());
				fatalError600();
			}
			if ((isNE(aglfIO.getPayclt(), SPACES))
			&& (isNE(sv.paysel, SPACES))) {
				if ((isNE(aglfIO.getPayclt(), sv.paysel))) {
					sv.ddind.set("X");
					goTo(GotoLabel.continue2115);
				}
			}
			//ILIFE-6896
			String clntnum= isNE(sv.paysel, SPACES)?sv.paysel.toString():sv.clntsel.toString();
			
			Clbapf clbl = clbapfDAO.searchClblData(aglfIO.getBankkey().toString().trim(), aglfIO.getBankacckey().toString().trim(), wsspcomn.fsuco.toString().trim(), 
					clntnum.trim());
			
			if (isEQ(sv.paysel, SPACES)) {
				if (isNE(sv.clntsel, clbl.getClntnum())) {
					sv.ddind.set("X");
				}
			}
			else {
				if (isNE(sv.paysel, clbl.getClntnum())) {
					sv.ddind.set("X");
				}
			}
			/**       IF S5035-CLNTSEL     NOT = CLBL-CLNTNUM              <021>*/
			/**          MOVE 'X'              TO S5035-DDIND              <021>*/
			/**       END-IF                                               <021>*/
		}
	}

protected void continue2115()
	{
		/*    Validate Agent type.*/
		if (isEQ(sv.agtype, SPACES)) {
			sv.agtypeErr.set("E186");
		}
		else {
			/*       IF S5035-AGTYPE-OUT (CHG)   = 'Y'                         */
			if (isNE(sv.agtype, wsaaStoredScreenFieldsInner.wsaaStoreAgtype)) {
				/*          MOVE S5035-AGTYPE     TO WSAA-STORE-AGTYPE             */
				/*          PERFORM 2160-CHG-AGTYPE.                               */
				chgAgtype2160();
				/*IF S5035-AGTYPE-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreAgtype.set(sv.agtype);
			}
		}
		/*    Validate Area Code.*/
		if (isEQ(sv.aracde, SPACES)) {
			sv.aracdeErr.set("F176");
		}
		else {
			/*       IF S5035-ARACDE-OUT (CHG)     = 'Y'                       */
			if (isNE(sv.aracde, wsaaStoredScreenFieldsInner.wsaaStoreAracde)) {
				/*          MOVE S5035-ARACDE     TO WSAA-STORE-ARACDE             */
				/*          PERFORM 2140-CHG-ARACDE.                               */
				chgAracde2140();
				/*IF S5035-ARACDE-ERR  = SPACES                     <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreAracde.set(sv.aracde);
			}
		}
		if (isEQ(sv.aracdeErr, SPACES)) {
			valBranchArea2150();
		}
		/*    Validate Sales Unit                                          */
		if (isEQ(th605rec.tsalesind, "Y")
		&& isEQ(sv.tsalesunt, SPACES)) {
			sv.tsalesuntErr.set("TL13");
		}
		else {
			if (isNE(sv.tsalesunt, wsaaStoredScreenFieldsInner.wsaaStoreTsalesunt)) {
				chgTsalesunt2900();
				wsaaStoredScreenFieldsInner.wsaaStoreTsalesunt.set(sv.tsalesunt);
			}
		}
		/*    Validate Reporting to field.*/
		if (isEQ(sv.repsel, SPACES)) {
			sv.repname.set(SPACES);
			wsaaStoredScreenFieldsInner.wsaaStoreRepsel.set(SPACES);
		}
		else {
			/*      IF S5035-REPSEL-OUT (CHG)   = 'Y'                          */
			if (isNE(sv.repsel, wsaaStoredScreenFieldsInner.wsaaStoreRepsel)) {
				/*          MOVE S5035-REPSEL     TO WSAA-STORE-REPSEL             */
				/*          PERFORM 2170-CHG-REPSEL.                               */
				chgRepsel2170();
				/*IF S5035-REPSEL-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreRepsel.set(sv.repsel);
			}
		}
		
		//TMLII-281 AG-01-002 - Validation for recruiter field start
		if(isEQ(sv.zrecruit, SPACES)){
			sv.agtname.set(SPACES);
		}else{
				chgZrecruit2130();
		}
		//TMLII-281 AG-01-002 - Validation for recruiter field end		

		/* Do a RETRV on the AGLF file at this point to ensure that        */
		/* there are no details for the overriding agent left in the       */
		/* AGLF parameters                                                 */
		aglfIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*    Validate Overrider percentage.*/
		if (isNE(sv.ovcpc, 0)
		&& isEQ(sv.repsel, SPACES)) {
			sv.ovcpcErr.set("E374");
		}
		/*    Validate Black List Suspend Indicator.*/
		/* AS THE NEW TABLE FOR THE BLACK LIST HAS BEEN CREATED THEN THE   */
		/* HARD-CODED CAN BE REMOVED.                                      */
		/*  IF S5035-TAGSUSIND       NOT = SPACE AND 'I'           <S01> */
		/*                           AND 'C' AND 'X'               <S01> */
		/*       MOVE F490              TO S5035-TAGSUSIND-ERR     <S01> */
		/*  END-IF.                                                <S01> */
		/*    Validate Payee number.*/
		if (isEQ(sv.paysel, SPACES)) {
			sv.payenme.set(SPACES);
		}
		else {
			/*       IF S5035-PAYSEL          = S5035-CLNTSEL          <V5L003>*/
			if (isEQ(sv.paysel, sv.clntsel)
			&& isNE(aglfIO.getAgccqind(), "Y")) {
				sv.clntselErr.set("G297");
				sv.payselErr.set("T005");
			}
			else {
				/*           IF S5035-PAYEESEL-OUT (CHG) = 'Y'*/
				chgPaysel2190();
			}
		}
		/*    Validate Payment Method.*/
		if (isEQ(sv.paymth, SPACES)) {
			sv.paymthErr.set("E186");
		}
		else {
			/*       IF S5035-PAYMTH-OUT (CHG) = 'Y'                           */
			/*027    IF S5035-PAYMTH NOT = WSAA-STORE-PAYMTH                   */
			/*          MOVE S5035-PAYMTH     TO WSAA-STORE-PAYMTH             */
			/*          PERFORM 2200-CHG-PAYMTH.                               */
			chgPaymth2200();
			/*IF S5035-PAYMTH-ERR = SPACES                      <023>*/
		}
		/*    Validate Payment Frequency.*/
		if (isEQ(sv.payfrq, SPACES)) {
			sv.payfrqErr.set("E186");
		}
		else {
			/*       IF S5035-PAYFRQ-OUT (CHG) = 'Y'                           */
			if (isNE(sv.payfrq, wsaaStoredScreenFieldsInner.wsaaStorePayfrq)) {
				/*          MOVE S5035-PAYFRQ     TO WSAA-STORE-PAYFRQ             */
				/*          PERFORM 2210-CHG-PAYFRQ.                               */
				chgPayfrq2210();
				/*IF S5035-PAYFRQ-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStorePayfrq.set(sv.payfrq);
			}
		}
		if (isEQ(sv.paymthErr, SPACES)
		&& isEQ(sv.payfrqErr, SPACES)) {
			valMethFreq2220();
		}
		/*    Validate Currency                                            */
		if (isEQ(sv.currcode, SPACES)) {
			sv.currcodeErr.set("F982");
		}
		/*    Validate Date appointed.*/
		if (isEQ(sv.dteapp, varcom.vrcmMaxDate)) {
			sv.dteappErr.set("E186");
		}
		if ((isNE(sv.dtetrm, SPACES))
		&& (isNE(sv.dteapp, SPACES))) {
			if (isGT(sv.dteapp, sv.dtetrm)) {
				sv.dteappErr.set("H067");
			}
		}
		/* If terminating an agent, ensure that they have no-one        */
		/* reporting to them.  If this is not the case, display an      */
		/* error.                                                       */
		if (isNE(sv.dtetrm, varcom.vrcmMaxDate)) {
			reportingAgents2280();
		}
		/*    Validate Exclusive Agreement.*/
		if (isEQ(sv.exclAgmt, SPACES)) {
			sv.excagrErr.set("E186");
		} else if(isNE(sv.exclAgmt, "Y")
			&& isNE(sv.exclAgmt, "N")) {
			sv.excagrErr.set("E315");
		}
		/*    Validate Basic Commission Payment.*/
		if (isEQ(sv.bcmtab, SPACES)) {
			sv.bcmdesc.set(SPACES);
			wsaaStoredScreenFieldsInner.wsaaStoreBcmtab.set(SPACES);
		}
		else {
			/*       IF S5035-BCMTAB-OUT (CHG)   = 'Y'                         */
			if (isNE(sv.bcmtab, wsaaStoredScreenFieldsInner.wsaaStoreBcmtab)) {
				/*          MOVE S5035-BCMTAB     TO WSAA-STORE-BCMTAB             */
				/*          PERFORM 2230-CHG-BCMTAB.                               */
				chgBcmtab2230();
				/*IF S5035-BCMTAB-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreBcmtab.set(sv.bcmtab);
			}
		}
		/*    Validate Servicing Commission Payment.*/
		if (isEQ(sv.scmtab, SPACES)) {
			sv.scmdsc.set(SPACES);
			wsaaStoredScreenFieldsInner.wsaaStoreScmtab.set(SPACES);
		}
		else {
			/*       IF S5035-SCMTAB-OUT (CHG)   = 'Y'                         */
			if (isNE(sv.scmtab, wsaaStoredScreenFieldsInner.wsaaStoreScmtab)) {
				/*          MOVE S5035-SCMTAB     TO WSAA-STORE-SCMTAB             */
				/*          PERFORM 2240-CHG-SCMTAB.                               */
				chgScmtab2240();
				/*IF S5035-SCMTAB-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreScmtab.set(sv.scmtab);
			}
		}
		/*    Validate Renewal Commission Payment.*/
		if (isEQ(sv.rcmtab, SPACES)) {
			sv.rcmdesc.set(SPACES);
			wsaaStoredScreenFieldsInner.wsaaStoreRcmtab.set(SPACES);
		}
		else {
			/*       IF S5035-RCMTAB-OUT (CHG)   = 'Y'                         */
			if (isNE(sv.rcmtab, wsaaStoredScreenFieldsInner.wsaaStoreRcmtab)) {
				/*          MOVE S5035-RCMTAB     TO WSAA-STORE-RCMTAB             */
				/*          PERFORM 2250-CHG-RCMTAB.                               */
				chgRcmtab2250();
				/*IF S5035-RCMTAB-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreRcmtab.set(sv.rcmtab);
			}
		}
		/*    Validate Bonus Commission Payment.*/
		if (isEQ(sv.ocmtab, SPACES)) {
			sv.ocmdesc.set(SPACES);
			wsaaStoredScreenFieldsInner.wsaaStoreOcmtab.set(SPACES);
		}
		else {
			/*       IF S5035-OCMTAB-OUT (CHG)   = 'Y'                         */
			if (isNE(sv.ocmtab, wsaaStoredScreenFieldsInner.wsaaStoreOcmtab)) {
				/*          MOVE S5035-OCMTAB     TO WSAA-STORE-OCMTAB             */
				/*          PERFORM 2260-CHG-OCMTAB.                               */
				chgOcmtab2260();
				/*IF S5035-OCMTAB-ERR = SPACES                      <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreOcmtab.set(sv.ocmtab);
			}
		}
		/*    Validate Agent Class.*/
		if (isEQ(sv.agentClass, SPACES)) {
			sv.agclsErr.set("E186");
		}
		else {
			/*       IF S5035-AGCLS-OUT (CHG)   = 'Y'                          */
			if (isNE(sv.agentClass, wsaaStoredScreenFieldsInner.wsaaStoreAgentClass)) {
				/*          MOVE S5035-AGENT-CLASS  TO WSAA-STORE-AGENT-CLASS      */
				/*          PERFORM 2270-CHG-AGCLS.                                */
				chgAgcls2270();
				/*IF S5035-AGCLS-ERR = SPACES                       <023>*/
				wsaaStoredScreenFieldsInner.wsaaStoreAgentClass.set(sv.agentClass);
			}
		}
		/*    Validate Agent's Licence Number and its Expiry Date.         */

		if (!isLicenseFields && isNE(sv.tlaglicno, SPACES)
		&& isEQ(sv.tlicexpdt, varcom.vrcmMaxDate)) {
			sv.tlicexpdtErr.set("I032");
		}

		/*    Validate Agent's Consolidated Cheque indicator               */
		if (isEQ(aglfIO.getAgccqind(), "Y")
		&& isEQ(sv.paysel, SPACES)) {
			sv.payselErr.set("E186");
		}
		if (!isLicenseFields && isEQ(sv.tlaglicno, SPACES)
		&& isNE(sv.tlicexpdt, varcom.vrcmMaxDate)) {
			sv.tlicexpdtErr.set("H400");
		}
		/*    GO TO 2290-EXIT.                                             */
		/* Conduct currency rounding check                                 */
		if (isNE(sv.minsta, ZERO)
		&& isNE(sv.currcode, SPACES)) {
			zrdecplrec.amountIn.set(sv.minsta);
			zrdecplrec.currency.set(sv.currcode);
			a100CallRounding();
			if (isNE(zrdecplrec.amountOut, sv.minsta)) {
				sv.minstaErr.set("RFIK");
			}
		}
		
		
		if(isAgentMainteance){
			if(isEQ(sv.banceindicatorsel, "Y")){
				if(isEQ(sv.bnkinternalrole, SPACES)){
					sv.bnkinternalroleErr.set("E186");
				}			
			
				if(isEQ(sv.agentrefcode, SPACES)){
						sv.agentrefcodeErr.set("E186");
				}	
			 
				 if(isEQ(sv.bnkinternalrole, bankOutlet) && isEQ(sv.agencymgr,SPACES)){
					sv.agencymgrErr.set("E186");	
				}
			
			}
			
					
			if ((isEQ(wsspcomn.flag, "A")) || (isEQ(wsspcomn.flag, "M"))){
				
				if(isGT(sv.agencymgr.toString().trim().length(),0)){
				 
				 long agencymgrCount = 0;
				 agencymgrCount = agntpfDAO.findAgencyManagerCount(wsspcomn.company.toString().trim(),sv.agencymgr.toString(),bnkIntrnalRole);
				 
				 if (agencymgrCount==0 ) {
					 sv.agencymgrErr.set("RRAZ");	
					}
				}
			 }
			
			if ((isEQ(wsspcomn.flag, "A")) || (isEQ(wsspcomn.flag, "M") && isNE(wsaaAgnrefcd,sv.agentrefcode)) ) {
				
				if(isGT(sv.agentrefcode.toString().trim().length(),0)){
				 
				 long recordCount = 0;
				 recordCount = agntpfDAO.findTotalRecordCount(wsspcomn.company.toString(),sv.agentrefcode.toString());
				 
				 if (recordCount>0 ) {
						sv.agentrefcodeErr.set("RRBX");			
					}
				}
			}
		}
		
		
		
		
	}

	/**
	* <pre>
	*   Performed paragraphs.
	*   Changed them to sections.
	*2120-CHG-CLIENT.
	* </pre>
	*/
protected void chgClient2120()
{
		/*    Validate client number*/
		Clntpf clntpf = cltsioCall2700(sv.clntsel.toString());
		if (clntpf == null) {
			/*        MOVE E186               TO S5035-CLNTSEL-ERR             */
			sv.clntselErr.set("E058");
			return ;
		}
		if (!"1".equals(clntpf.getValidflag())) {
			sv.clntselErr.set("E329");
			return ;
		}
		if (isEQ(sv.payselErr, SPACES)
			&& null != clntpf.getCltdod() //ILIFE-7953
			&& clntpf.getCltdod() != 0
			&& isNE(clntpf.getCltdod(), varcom.vrcmMaxDate)) {
				sv.clntselErr.set("F782");
				return ;
		}
		if (isEQ(wsaaSaveClntsel, SPACES)) {
			wsaaSaveClntsel.set(sv.clntsel);
		}
		/*    IF S5035-CLNTSEL-ERR = SPACES                                */
		plainname(clntpf);
		wsspcomn.clntkey.set(sv.clntsel);
		sv.cltname.set(wsspcomn.longconfname);
	}
//TMLII-281 AG-01-002 start
protected void chgZrecruit2130()
{
	try {
		go2135();
	}
	catch (GOTOException e){
	}
}

protected void go2135()
{
	recruiter = agntlagioCall1500(sv.zrecruit.toString());
	if (recruiter == null) {
		sv.agtname.set(SPACES);
		sv.zrecruitErr.set("E305");
		goTo(GotoLabel.exit2179);
	}
	if (isEQ(sv.zrecruitErr,SPACES)) {
		cltsioCall1700(recruiter.getClntnum());
		sv.agtname.set(wsspcomn.longconfname);
	}
	if (!"1".equals(recruiter.getValidflag())) {
		sv.zrecruitErr.set("E475");
		goTo(GotoLabel.exit2179);
	}
	if (!"Y".equals(recruiter.getLifagnt())) { //ILIFE-7794
		sv.zrecruitErr.set("F188");
		goTo(GotoLabel.exit2179);
	}
	if (isEQ(sv.zrecruit,sv.agnum)) {
		sv.zrecruitErr.set("G747");
		goTo(GotoLabel.exit2179);
	}
	// aglfIO.setZrecruit(sv.zrecruit); //ILIFE-4370
	if (!(isEQ(sv.zrecruit, SPACES) || isNE(sv.zrecruitErr, SPACES))) { //ILIFE-4370
		zrecruitCheck2136();
	}

}

protected void zrecruitCheck2136()
{
	/*GO*/
	/*aglfIO.setAgntnum(sv.zrecruit); //ILIFE-4370
	aglfIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, aglfIO);
	if (isNE(aglfIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(aglfIO.getParams());
		fatalError600();
	}
	if (isEQ(aglfIO.getZrecruit(),sv.agnum)) {
		sv.zrecruitErr.set("E542");
	}*/
	/*EXIT*/
	
	aglfpf.setAgntnum(sv.zrecruit.toString());
	aglfpf = aglfpfDAO.searchAglflnb(wsspcomn.company.toString(), aglfpf.getAgntnum());
	if(null == aglfpf)
	{
		fatalError600();
	}
	if(aglfpf.getZrecruit().equals(sv.agnum.toString())){
		sv.zrecruitErr.set("E542");
	}
}
//TMLII-281 AG-01-002 end

	/**
	* <pre>
	*2140-CHG-ARACDE.
	* </pre>
	*/
protected void chgAracde2140()
	{
		t5696List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5696");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5696List) {
			if (descItem.getDescitem().trim().equals(sv.aracde.toString().trim())){
				sv.aradesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.aradesc.set("??????????????????????????????");
		}
	}

	/**
	* <pre>
	*2150-VAL-BRANCH-AREA.
	* </pre>
	*/
protected void valBranchArea2150()
	{
		/*GO*/
		/*    Cross validate that the agent area is covered by the branch*/
		//ILIFE-6896
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sv.agntbr);
		stringVariable1.addExpression(sv.aracde);
		itempf = new Itempf();
		itempf.setItemtabl("T5628");
		itempf.setItemitem(stringVariable1.toString());
		itempf = itemioCall2800(itempf);
		if (itempf == null) {
			/*    MOVE E305                TO S5035-ARACDE-ERR.             */
			sv.aracdeErr.set("F005");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*2160-CHG-AGTYPE.
	* </pre>
	*/
protected void chgAgtype2160()
	{
		t3692List = descDAO.getItems("IT", wsspcomn.company.toString(), "T3692");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t3692List) {
			if (descItem.getDescitem().trim().equals(sv.agtype.toString().trim())){
				sv.agtydesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.agtydesc.set("??????????????????????????????");
		}	
		
		/*EXIT*/
	}

	/**
	* <pre>
	*2170-CHG-REPSEL.
	* </pre>
	*/
protected void chgRepsel2170()
	{
		/*    Validate agent reported to*/
		wsaaRepsel.set(sv.repsel);
		if (isNE(wsaaRepselFill, SPACES)) {
			sv.repselErr.set("E305");
			return ;
		}
		reportag = this.agntlagioCall1500(sv.repsel.toString());
		if (reportag == null) {
			sv.repselErr.set("E305");
			return ;
		}
		/*    If OK, look up client name*/
		if (isEQ(sv.repselErr, SPACES)) {
			cltsioCall1700(reportag.getClntnum());
			sv.repname.set(wsspcomn.longconfname);
		}
		if (!"1".equals(reportag.getValidflag())) {
			sv.repselErr.set("E475");
			return ;
		}
		/*if (!"1".equals(reportag.getValidflag())) {//ILIFE-7793
			sv.repselErr.set("F188");
			return ;
		}*/
		if (isEQ(sv.repsel, sv.agnum)) {
			sv.repselErr.set("G747");
			return ;
		}
		//aglfIO.setReportag(sv.repsel);
		aglfpf.setReportag(sv.repsel.toString());
		while ( !(isEQ(aglfpf.getReportag(), SPACES)
		|| isNE(sv.repselErr, SPACES))) {
			reportToCheck2180();
		}

	}

	/**
	* <pre>
	*2180-REPORT-TO-CHECK.
	* </pre>
	*/
protected void reportToCheck2180()
	{
		/*GO*/
		/*    Check Agent Reporting to is not equal to Agent number.*/
		/*aglfIO.setAgntnum(aglfIO.getReportag());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getReportag(), sv.agnum)) {
			sv.repselErr.set("E542");
		}*/
			aglfpf.setAgntnum(aglfpf.getReportag());/* IJTI-1386 */
			aglfpf = aglfpfDAO.searchAglfRecord(wsspcomn.company.toString(), aglfpf.getAgntnum());
			if(null == aglfpf)
			{
				fatalError600();
			}
			if(aglfpf.getReportag().equals(sv.agnum.toString()))
			{
				sv.repselErr.set("E542");
			}
		/*EXIT*/
	}

	/**
	* <pre>
	*2190-CHG-PAYEESEL.
	* </pre>
	*/
protected void chgPaysel2190()
	{
		Clntpf clntpf = cltsioCall2700(sv.paysel.toString());
		if (clntpf == null) {
			sv.payselErr.set("E058");
			return ;
		}
		if (!"1".equals(clntpf.getValidflag())) {
			sv.payselErr.set("E329");
			return ;
		}
		if (isEQ(sv.payselErr, SPACES)
			&& null != clntpf.getCltdod() // ILIFE-7953	
			&& clntpf.getCltdod() != 0
			&& isNE(clntpf.getCltdod(), varcom.vrcmMaxDate)) {
				sv.payselErr.set("F782");
				return ;
		}
		if (isEQ(sv.payselErr, SPACES)) {
			plainname(clntpf);
			sv.payenme.set(wsspcomn.longconfname);
		}
	}

	/**
	* <pre>
	*2200-CHG-PAYMTH.
	* </pre>
	*/
protected void chgPaymth2200()
	{
		try {
			go2205();
			checkPayDetls2206();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void go2205()
	{	
		t5690List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5690");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5690List) {
			if (descItem.getDescitem().trim().equals(sv.paymth.toString().trim())){
				sv.pymdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.pymdesc.set("??????????????????????????????");
			goTo(GotoLabel.exit2209);
		}
	}

protected void checkPayDetls2206()
	{
		/*    Using the T5690 XDS, look up T3672 to see if a any further   */
		/*    details are required for this payment type.                  */
		T5690ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T5690");
		String keyItemitem = sv.paymth.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (T5690ListMap.containsKey(keyItemitem.trim())) {
			itempfList = T5690ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
	
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					 
						t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
					 
	
				} else {
					t5690rec.t5690Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					itemFound = true;
				}
			}
		}
		
		if (itemFound==false)
		{
			sv.paymthErr.set("F177");
			return;
		}
		
		if (itemFound==true)
		{
		T3672ListMap = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), "T3672");
		keyItemitem = t5690rec.paymentMethod.toString();
		List<Itempf> itempfListData = new ArrayList<Itempf>();
		itemFound = false;
		if (T3672ListMap.containsKey(keyItemitem.trim())) {
			itempfListData = T3672ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfListData.iterator();
			while (iterator.hasNext()) 
			{
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					 
						t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
				} 
				else
				{
						t3672rec.t3672Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						itemFound = true;
				}
			}
		}
		}
		
		
		if (itemFound==false)
		{
			sv.paymthErr.set("G447");
			return ;
		}
		
		if (isEQ(t3672rec.bankaccreq, "Y")
		&& (isEQ(aglfIO.getBankkey(), SPACES)
		|| isEQ(aglfIO.getBankacckey(), SPACES))) {
			sv.ddind.set("X");
		}
		/*    Check for the change from direct credit to any other         */
		/*    payment type.                                                */
		if (isEQ(t3672rec.bankaccreq, "N")) {
			if (isEQ(sv.ddind, "X")) {
				sv.ddindErr.set("E492");
			}
			else {
				sv.ddind.set(" ");
			}
		}
		if (isEQ(t3672rec.bankaccreq, "N")
		&& (isNE(aglfIO.getBankkey(), SPACES)
		|| isNE(aglfIO.getBankacckey(), SPACES))) {
			aglfIO.setBankkey(SPACES);
			aglfIO.setBankacckey(SPACES);
		}
	}

	/**
	* <pre>
	*2210-CHG-PAYFRQ.
	* </pre>
	*/
protected void chgPayfrq2210()
	{
		/*GO*/
		
		t5630List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5630");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5630List) {
			if (descItem.getDescitem().trim().equals(sv.payfrq.toString().trim())){
				sv.pyfdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.pyfdesc.set("??????????????????????????????");
		}	
		
		/*EXIT*/
	}

	/**
	* <pre>
	*2220-VAL-METH-FREQ.
	* </pre>
	*/
protected void valMethFreq2220()
	{
		go2225();
	}

protected void go2225()
	{
		/*    Cross validate payment method and frequency*/
		//ILIFE-6896
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sv.paymth);
		stringVariable1.addExpression(sv.payfrq);
		itempf = new Itempf();
		itempf.setItemtabl("T5629");
		itempf.setItemitem(stringVariable1.toString());
		itempf = itemioCall2800(itempf);
		if (itempf == null) {
			sv.paymthErr.set("F199");
			sv.payfrqErr.set("F199");
			return ;
		}
		if (isNE(itempf.getValidflag(), "1")) {
			sv.paymthErr.set("F199");
			sv.payfrqErr.set("F199");
		}
	}

	/**
	* <pre>
	*2230-CHG-BCMTAB.
	* </pre>
	*/
protected void chgBcmtab2230()
	{
		/*GO*/
		/*    Look up basic commission method description*/
		/*MOVE SPACES                 TO DESC-DATA-KEY.*/
		
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(sv.bcmtab.toString().trim())){
				sv.bcmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.bcmdesc.set("??????????????????????????????");
		}	
		
		/*EXIT*/
	}

	/**
	* <pre>
	*2240-CHG-SCMTAB.
	* </pre>
	*/
protected void chgScmtab2240()
	{
		/*GO*/
		/*    Look up servicing commission method description*/
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(sv.scmtab.toString().trim())){
				sv.scmdsc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.scmdsc.set("??????????????????????????????");
		}	
					
		/*EXIT*/
	}

	/**
	* <pre>
	*2250-CHG-RCMTAB.
	* </pre>
	*/
protected void chgRcmtab2250()
	{
		/*GO*/
		/*    Look up renewal commission method description*/
		
		t5644List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5644");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5644List) {
			if (descItem.getDescitem().trim().equals(sv.rcmtab.toString().trim())){
				sv.rcmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.rcmdesc.set("??????????????????????????????");
		}	
		
		/*EXIT*/
	}

	/**
	* <pre>
	*2260-CHG-OCMTAB.
	* </pre>
	*/
protected void chgOcmtab2260()
	{
		/*GO*/
		/*    Look up bonus commission method description*/
		t5697List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5697");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5697List) {
			if (descItem.getDescitem().trim().equals(sv.ocmtab.toString().trim())){
				sv.ocmdesc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.ocmdesc.set("??????????????????????????????");
		}	
		/*EXIT*/
	}

	/**
	* <pre>
	*2270-CHG-AGCLS.
	* </pre>
	*/
protected void chgAgcls2270()
	{
		/*GO*/
		/*    Look up agent commission class description*/
		t5699List = descDAO.getItems("IT", wsspcomn.company.toString(), "T5699");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : t5699List) {
			if (descItem.getDescitem().trim().equals(sv.agentClass.toString().trim())){
				sv.agclsd.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.agclsd.set("??????????????????????????????");
		}	
		/*EXIT*/
	}

	protected void reportingAgents2280() {
		// ILIFE-6896
		wsaaAgntpfx.set("AG");
		wsaaAgntcoy.set(wsspcomn.company);
		wsaaAgntnum.set(sv.agnum);
		Agntpf aglfrpt = agntpfDAO.getAglfrptDataByRepagent(wsaaAglfrptKey.toString());
		if (aglfrpt == null) {
			return;
		}
		if (isNE(aglfrpt.getAgntnum(), SPACES)) {
			sv.dtetrmErr.set("E535");
		}
	}



	protected Clntpf cltsioCall2700(String clntNum) {
		Clntpf clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);
		return clntpf;
	}

protected Itempf itemioCall2800(Itempf itempf){
		/*ITEM-TABLE*/
		/*    Call the Item details I/O Module for Table Look Ups.*/
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString().trim());
		return itemDAO.getItemRecordByItemkey(itempf);
}

protected void chgTsalesunt2900()
	{
		/*GO*/
		/*Look up sales unit description */
		TT518List = descDAO.getItems("IT", wsspcomn.company.toString(), "TT518");/* IJTI-1386 */
		boolean itemFound = false;
		for (Descpf descItem : TT518List) {
			if (descItem.getDescitem().trim().equals(sv.tsalesunt.toString().trim())){
				sv.tsalesdsc.set(descItem.getLongdesc());
				itemFound=true;
				break;
			}
		}
		if (!itemFound) {
			sv.tsalesdsc.set("??????????????????????????????");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
		updateCustomerSpecificFields(aglfIO.getAgntcoy().toString().trim(),
				aglfIO.getAgntnum().toString().trim());
	}

protected void updateDatabase3010()
	{
		/*    Skip  this section if  returning from an optional selection*/
		/*    (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*IF-SELECTION*/
		/*    Only update the files if no additional screens are selected.*/
		if (isEQ(sv.ddind, "X")
		|| isEQ(sv.bctind, "X")
		|| isEQ(sv.zrorind, "X")
		|| isEQ(sv.tagd, "X")
		|| isEQ(sv.clientind, "X")
		|| isEQ(sv.agncysalic, "X") 
		|| isEQ(sv.qualification, "X")){
			selectKeeps3100();
		}
		else {
			if (isNE(wsspcomn.flag, "I")) {
				noSelectUpdte3200();
				if(isAgencyMainteance) 
				{
					agntcypf=agntcypfDAO.getAgncy(wsspcomn.company.toString(), sv.agnum.toString(), 1);	//ILJ-683
					if(null !=agntcypf)
						agntcyUpdte3240();
					else
						keepsAgntcy13130();
					
					if (isNE(sv.salebranch, SPACE)) 
					{
						String role = "AG";
						String levelno = role.concat(sv.agnum.trim());
						
						Hierpf hrpf = hierpfDAO.getHierpfData(levelno);
						Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);
						if (!isPrsnt.isPresent())
						{
					    	if (isEQ(wsspcomn.flag, "A"))
					    	{
					    		Agsdpf agsdpf1=agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
					    		Optional<Agsdpf> ifPrsnt = Optional.ofNullable(agsdpf1);
					    		if (ifPrsnt.isPresent()) 
					    		{
					    			String role2;
					    			if(isEQ(agsdpf1.getLevelclass(),"2"))
					    			{
					    				role2 = "SB";
					    				String levelno1 = role2.concat(sv.salebranch.trim());
						    			hierpfinsert3260(levelno1,role2);
					    			}
					    			else if(isEQ(agsdpf1.getLevelclass(),"3"))
					    			{
					    				role2 = "SS";
					    				String levelno1 = role2.concat(sv.salebranch.trim());
						    			hierpfinsert3260(levelno1,role2);
					    			}
							
					    		}
					    	}
						}
						else
						{
							if (isEQ(wsspcomn.flag, "T") && isEQ(hrpf.getValidflag(),"1") && isEQ(hrpf.getActivestatus(),"1"))
							{
								updateHierpf3280(hrpf);
							}
							else if (isEQ(wsspcomn.flag, "R") && isEQ(hrpf.getValidflag(),"1") && 
									isEQ(hrpf.getActivestatus(),"2") && isEQ(hrpf.getRegisclass(),"3"))
							{
								updateHierpf3480(hrpf);
							}
						}
						
					}
					
						if (isEQ(wsspcomn.flag, "M") && isEQ(aglfIO.getDtetrm(), varcom.vrcmMaxDate))
						 {
							 
							 	String role = "AG";
								String levelno = role.concat(sv.agnum.trim());
								
								Hierpf hrpf = hierpfDAO.getHierpfData(levelno);
								Optional<Hierpf> isPrsnt = Optional.ofNullable(hrpf);

								boolean hierpfflag=false;
								
								if(isPrsnt.isPresent())
								{
									Hierpf hrpf1 = hierpfDAO.getHierpfStatus(hrpf.getUpperlevel().trim(),"2",levelno);
									Optional<Hierpf> ifPrsnt = Optional.ofNullable(hrpf1);
									if(ifPrsnt.isPresent())
									{
										hierpfflag=true;
									}
									else
									{
										Hierpf hrpf2 = hierpfDAO.getHierpfStatus(hrpf.getUpperlevel().trim(),"2",levelno);
										Optional<Hierpf> ifPrsnt2 = Optional.ofNullable(hrpf2);
										if(ifPrsnt2.isPresent())
										{
											hierpfflag=true;
										}
										
									}
									
									int wsaaGen=Integer.parseInt(hrpf.getGenerationno().trim());
									int wsaaGenno=wsaaGen+1;
									wsaaGennum= String.valueOf(wsaaGenno);
									
								}
								else
								{
									wsaaGennum = "1";
								}
							
								 	if(isEQ(sv.salebranch, SPACE) && isNE(wsaaLevelno, SPACE))   //Sales Branch/Section Remove
									{
								 		updateHierpf3680(hrpf);
									}
								 	else if(isNE(sv.salebranch, SPACE) && isNE(wsaaLevelno, SPACE) && 
								 			isNE(wsaaLevelno,sv.salebranch)) // Sales Branch/Section Change
								 	{	
								 		updateHierpf3880(hrpf, role);
								 	}
								 	else if(isNE(sv.salebranch, SPACE) && isEQ(wsaaLevelno, SPACE) && (!isPrsnt.isPresent() || (isPrsnt.isPresent() && hierpfflag))) //Sales Branch/Section Added
									{
								 		Agsdpf agsdpf1=agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
							    		Optional<Agsdpf> ifPrsnt = Optional.ofNullable(agsdpf1);
							    		if (ifPrsnt.isPresent()) 
							    		{
							    			String role2;
							    			if(isEQ(agsdpf1.getLevelclass(),"2"))
							    			{
							    				role2 = "SB";
							    				String levelno1 = role2.concat(sv.salebranch.trim());
								    			hierpfinsert3260(levelno1,role2);
							    			}
							    			else if(isEQ(agsdpf1.getLevelclass(),"3"))
							    			{
							    				role2 = "SS";
							    				String levelno1 = role2.concat(sv.salebranch.trim());
								    			hierpfinsert3260(levelno1,role2);
							    			}
									
							    		}
								 			
									  } 
						 }
			    	}
			   }
		 }
	}

protected void selectKeeps3100()
	{
		keepsAglf3110();
		keepsAgnt3120();
	}

protected void keepsAglf3110()
	{
		/*    Move fields from the screen to the life agents file.*/
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agnum);
		aglfIO.setDteapp(sv.dteapp);
		aglfIO.setExclAgmt(sv.exclAgmt);
		aglfIO.setAracde(sv.aracde);
		aglfIO.setTsalesunt(sv.tsalesunt);
		aglfIO.setReportag(sv.repsel);
		//TMLII-281 AG-01-002 
		aglfIO.setZrecruit(sv.zrecruit);		
		aglfIO.setOvcpc(sv.ovcpc);
		aglfIO.setPayclt(sv.paysel);
		aglfIO.setPaymth(sv.paymth);
		aglfIO.setPayfrq(sv.payfrq);
		aglfIO.setCurrcode(sv.currcode);
		aglfIO.setMinsta(sv.minsta);
		aglfIO.setBcmtab(sv.bcmtab);
		aglfIO.setScmtab(sv.scmtab);
		aglfIO.setRcmtab(sv.rcmtab);
		aglfIO.setOcmtab(sv.ocmtab);
		aglfIO.setAgentClass(sv.agentClass);
		aglfIO.setDtetrm(sv.dtetrm);
		aglfIO.setTagsusind(sv.tagsusind);
		aglfIO.setTlaglicno(sv.tlaglicno);
		aglfIO.setTlicexpdt(sv.tlicexpdt);
		/* MOVE '1'                    TO AGLF-VALIDFLAG.       <V76F10>*/
		aglfIO.setValidflag("1");
		if (wsaaAgreemntNo.isTrue()
		&& isEQ(aglfIO.getAgccqind(), SPACES)) {
			if (isEQ(th605rec.agccqind, "Y")) {
				aglfIO.setAgccqind(th605rec.agccqind);
			}
			else {
				aglfIO.setAgccqind("N");
			}
		}
		aglfIO.setFunction("KEEPS");
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void keepsAgnt3120()
	{
		/*    Move fields from the screen to the FSU agent details*/
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setAgntnum(sv.agnum);
		agntlagIO.setAgntpfx("AG");
		agntlagIO.setClntpfx("CN");
		agntlagIO.setClntcoy(wsspcomn.fsuco);
		agntlagIO.setClntnum(sv.clntsel);
		agntlagIO.setAgtype(sv.agtype);
		agntlagIO.setAgntrel("AG");
		agntlagIO.setAgntbr(sv.agntbr);
		agntlagIO.setValidflag("1");
		if (isEQ(sv.repsel, SPACES)) {
			agntlagIO.setRepagent01(SPACES);
			agntlagIO.setReplvl(1);
		}
		else {
			wsaaAgntkey.agntAgntpfx.set("AG");
			wsaaAgntkey.agntAgntcoy.set(wsspcomn.company);
			wsaaAgntkey.agntAgntnum.set(sv.repsel);
			agntlagIO.setRepagent01(wsaaAgntkey);
			agntlagIO.setReplvl(2);
		}
		agntlagIO.setLifagnt("Y");
		agntlagIO.setFunction("KEEPS");
		agntlagIO.setFormat(formatsInner.agntlagrec);
		agntlagioCall3800();
	}

 private void keepsAgntcy13130(){
	 agntcypf=new Agntcypf();
	 agntcypf.setAgncycoy(wsspcomn.company.toString());
	 agntcypf.setAgncynum(sv.agncysel.toString());
	 agntcypf.setAgntnum(sv.agnum.toString());
	 agntcypf.setSrdate(sv.dteapp.toInt());
	 agntcypf.setEndate(sv.dtetrm.toInt());
	 agntcypfDAO.insertAgncy(agntcypf);
}
protected void noSelectUpdte3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					clrrAgentUpdte3210();
				case writeAgentClrr3215:
					writeAgentClrr3215();
				case clrrPayeeUpdte3220:
					clrrPayeeUpdte3220();
				case writePayeeClrr3225:
					writePayeeClrr3225();
				case agntUpdte3230:
					agntUpdte3230();
					aglfUpdte3240();
					updteBatchHeader3260();
					unlkSoftLock3270();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void clrrAgentUpdte3210()
	{
		/*    If the client number has not changed, skip this role.*/
		if (isEQ(wsaaOldClntnum, sv.clntsel)) {
			goTo(GotoLabel.clrrPayeeUpdte3220);
		}
		/*    MOVE SPACES                 TO CLRR-DATA-AREA*/
		cltrelnrec.clrrrole.set("AG");
		if (isEQ(wsaaOldClntnum, SPACES)) {
			goTo(GotoLabel.writeAgentClrr3215);
		}
		cltrelnrec.clntnum.set(wsaaOldClntnum);
		cltrelnrec.function.set("DEL  ");
		clrrioCall3700();
	}

	/**
	* <pre>
	*    MOVE DELET                  TO CLRR-FUNCTION.
	*    PERFORM 3700-CLRRIO-CALL.
	* </pre>
	*/
protected void writeAgentClrr3215()
	{
		cltrelnrec.clntnum.set(sv.clntsel);
		cltrelnrec.function.set("ADD  ");
		clrrioCall3700();
	}

protected void clrrPayeeUpdte3220()
	{
		/*    If the payee number has not changed, skip this role.*/
		/*    IF S5035-PAYSEL = SPACES                                     */
		/*       IF WSAA-OLD-PAYEE        = S5035-CLNTSEL                  */
		/*          GO TO 3230-AGNT-UPDTE                                  */
		/*    ELSE                                                         */
		/*       IF WSAA-OLD-PAYEE        = S5035-PAYSEL                   */
		/*          GO TO 3230-AGNT-UPDTE.                                 */
		if (isEQ(sv.paysel, SPACES)
		&& isEQ(wsaaOldPayee, sv.clntsel)) {
			goTo(GotoLabel.agntUpdte3230);
		}
		if (isEQ(wsaaOldPayee, sv.paysel)
		&& isNE(wsaaOldClntnum, SPACES)) {
			goTo(GotoLabel.agntUpdte3230);
		}
		cltrelnrec.clrrrole.set("PE");
		if (isEQ(wsaaOldPayee, SPACES)) {
			goTo(GotoLabel.writePayeeClrr3225);
		}
		cltrelnrec.clntnum.set(wsaaOldPayee);
		cltrelnrec.function.set("DEL  ");
		clrrioCall3700();
	}

	/**
	* <pre>
	*    MOVE DELET                  TO CLRR-FUNCTION.
	*    PERFORM 3700-CLRRIO-CALL.
	* </pre>
	*/
protected void writePayeeClrr3225()
	{
		if (isEQ(sv.paysel, SPACES)) {
			cltrelnrec.clntnum.set(sv.clntsel);
		}
		else {
			cltrelnrec.clntnum.set(sv.paysel);
		}
		cltrelnrec.function.set("ADD  ");
		clrrioCall3700();
	}

protected void agntUpdte3230()
	{
		/*    Release the I/O module for updating.*/
		agntlagIO.setFunction(varcom.rlse);
		agntlagioCall3800();
		/*    Update the agent details from the screen.*/
		/* MOVE WSSP-COMPANY           TO AGNTLAG-AGNTCOY.      <V76F10>*/
		/* MOVE S5035-AGNUM            TO AGNTLAG-AGNTNUM.      <V76F10>*/
		/* MOVE AGNTLAGREC             TO AGNTLAG-FORMAT.       <V76F10>*/
		/* MOVE READH                  TO AGNTLAG-FUNCTION.     <V76F10>*/
		/* CALL 'AGNTLAGIO'         USING AGNTLAG-PARAMS.       <V76F10>*/
		/* IF AGNTLAG-STATUZ        NOT = O-K AND MRNF          <V76F10>*/
		/*    MOVE AGNTLAG-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*    MOVE AGNTLAG-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*    PERFORM 600-FATAL-ERROR                           <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		/*                                                      <V76F10>*/
		/* IF AGNTLAG-STATUZ            = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGNTLAG-VALIDFLAG     <V76F10>*/
		/*    MOVE AGNTLAGREC          TO AGNTLAG-FORMAT        <V76F10>*/
		/*    MOVE REWRT               TO AGNTLAG-FUNCTION      <V76F10>*/
		/*    CALL 'AGNTLAGIO'         USING AGNTLAG-PARAMS     <V76F10>*/
		/*    IF AGNTLAG-STATUZ        NOT = O-K                <V76F10>*/
		/*       MOVE AGNTLAG-STATUZ      TO SYSR-STATUZ        <V76F10>*/
		/*       MOVE AGNTLAG-PARAMS      TO SYSR-PARAMS        <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setAgntnum(sv.agnum);
		agntlagIO.setAgntpfx("AG");
		agntlagIO.setClntpfx("CN");
		agntlagIO.setClntcoy(wsspcomn.fsuco);
		agntlagIO.setClntnum(sv.clntsel);
		agntlagIO.setAgntrel("AG");
		agntlagIO.setAgtype(sv.agtype);
		agntlagIO.setAgntbr(sv.agntbr);
		agntlagIO.setValidflag("1");
		//ICIL-12 start
		if(isAgentMainteance){
		agntlagIO.setAgentrefcode(sv.agentrefcode);
		agntlagIO.setAgencymgr(sv.agencymgr);
		agntlagIO.setDistrctcode(sv.distrctcode);
		agntlagIO.setBnkinternalrole(sv.bnkinternalrole);
		agntlagIO.setBanceindicatorsel(sv.banceindicatorsel);
		}
		//ICIL-12 end
		if (isEQ(sv.repsel, SPACES)) {
			agntlagIO.setRepagent01(SPACES);
			agntlagIO.setReplvl(1);
		}
		else {
			wsaaAgntkey.agntAgntpfx.set("AG");
			wsaaAgntkey.agntAgntcoy.set(wsspcomn.company);
			wsaaAgntkey.agntAgntnum.set(sv.repsel);
			agntlagIO.setRepagent01(wsaaAgntkey);
			agntlagIO.setReplvl(2);
		}
		agntlagIO.setLifagnt("Y");
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		agntlagIO.setTranid(varcom.vrcmCompTranid);
		agntlagIO.setFormat(formatsInner.agntlagrec);
		/* MOVE UPDAT                  TO AGNTLAG-FUNCTION.             */
		/* MOVE WRITR                  TO AGNTLAG-FUNCTION.     <V76F10>*/
		agntlagIO.setFunction(varcom.updat);
		agntlagioCall3800();
	}

protected void aglfUpdte3240()
	{
		/*    Release the I/O module for updating.*/
		aglfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*    Update the life agent details from the screen.*/
		/* MOVE WSSP-COMPANY           TO AGLF-AGNTCOY.         <V76F10>*/
		/* MOVE S5035-AGNUM            TO AGLF-AGNTNUM.         <V76F10>*/
		/* MOVE AGLFREC                TO AGLF-FORMAT.          <V76F10>*/
		/* MOVE READH                  TO AGLF-FUNCTION.        <V76F10>*/
		/* CALL 'AGLFIO'            USING AGLF-PARAMS.          <V76F10>*/
		/* IF AGLF-STATUZ              NOT = O-K AND MRNF       <V76F10>*/
		/*    MOVE AGLF-PARAMS         TO SYSR-PARAMS           <V76F10>*/
		/*    MOVE AGLF-STATUZ         TO SYSR-STATUZ           <V76F10>*/
		/*    PERFORM 600-FATAL-ERROR                           <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		/*                                                      <V76F10>*/
		/* IF AGLF-STATUZ               = O-K                   <V76F10>*/
		/*    MOVE '2'                 TO AGLF-VALIDFLAG        <V76F10>*/
		/*    MOVE AGLFREC             TO AGLF-FORMAT           <V76F10>*/
		/*    MOVE REWRT               TO AGLF-FUNCTION         <V76F10>*/
		/*    CALL 'AGLFIO'         USING AGLF-PARAMS           <V76F10>*/
		/*    IF AGLF-STATUZ        NOT = O-K                   <V76F10>*/
		/*       MOVE AGLF-STATUZ      TO SYSR-STATUZ           <V76F10>*/
		/*       MOVE AGLF-PARAMS      TO SYSR-PARAMS           <V76F10>*/
		/*       PERFORM 600-FATAL-ERROR                        <V76F10>*/
		/* END-IF.                                              <V76F10>*/
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setAgntnum(sv.agnum);
		aglfIO.setDteapp(sv.dteapp);
		aglfIO.setExclAgmt(sv.exclAgmt);
		aglfIO.setAracde(sv.aracde);
		aglfIO.setTsalesunt(sv.tsalesunt);
		aglfIO.setReportag(sv.repsel);
		//TMLII-281 AG-01-002
		aglfIO.setZrecruit(sv.zrecruit);
		aglfIO.setOvcpc(sv.ovcpc);
		aglfIO.setPayclt(sv.paysel);
		aglfIO.setPaymth(sv.paymth);
		if (isEQ(t3672rec.bankaccreq, "N")
		&& (isNE(aglfIO.getBankkey(), SPACES)
		|| isNE(aglfIO.getBankacckey(), SPACES))) {
			aglfIO.setBankkey(SPACES);
			aglfIO.setBankacckey(SPACES);
		}
		/*    IF WSSP-FLAG            NOT  = 'M'                   <LA1174>*/
		/*       PERFORM M200-UPDATE-MACF                          <LA1174>*/
		/*    END-IF.                                              <LA1174>*/
		m200UpdateMacf();
		aglfIO.setPayfrq(sv.payfrq);
		aglfIO.setCurrcode(sv.currcode);
		aglfIO.setMinsta(sv.minsta);
		aglfIO.setBcmtab(sv.bcmtab);
		aglfIO.setScmtab(sv.scmtab);
		aglfIO.setRcmtab(sv.rcmtab);
		aglfIO.setOcmtab(sv.ocmtab);
		aglfIO.setAgentClass(sv.agentClass);
		/*    IF WSSP-FLAG                = 'R'                            */
		/*       MOVE VRCM-MAX-DATE       TO AGLF-DTETRM                   */
		/*    ELSE                                                         */
		aglfIO.setDtetrm(sv.dtetrm);
		/*    END-IF.                                                      */
		/*    MOVE S5035-DTETRM           TO AGLF-DTETRM.          <V4L016>*/
		aglfIO.setTagsusind(sv.tagsusind);
		aglfIO.setTlaglicno(sv.tlaglicno);
		aglfIO.setTlicexpdt(sv.tlicexpdt);
		aglfIO.setTermid(varcom.vrcmTermid);
		aglfIO.setUser(varcom.vrcmUser);
		aglfIO.setTransactionDate(varcom.vrcmDate);
		aglfIO.setTransactionTime(varcom.vrcmTime);
		/* MOVE UPDAT                  TO AGLF-FUNCTION.                */
		/* MOVE '1'                    TO AGLF-VALIDFLAG.       <V76F10>*/
		/* MOVE WRITR                  TO AGLF-FUNCTION.        <V76F10>*/
		aglfIO.setFunction(varcom.updat);
		aglfIO.setValidflag("1");
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(th605rec.crtind, "N")
		|| isEQ(wsaaTrigger, "Y")) {
			m001UpdateLevel1();
		}
		if (isEQ(wsspcomn.sbmaction, "A")) {
			callBldenrl3300();
		}
	}

	protected void agntcyUpdte3240() {
	
		agntcypf=new Agntcypf();
		agntcypf.setAgncynum(sv.agncysel.toString());
		agntcypf.setSrdate(sv.dteapp.toInt());
		agntcypf.setEndate(sv.dtetrm.toInt());
		agntcypf.setAgntnum(sv.agnum.toString());		
		agntcypfDAO.updateAgncypf(agntcypf);
		
	}
	
	
	protected void hierpfinsert3260(String toplevel,String role) {
	
		hierpf = new Hierpf();
		
		wsaaroleCode.set("AG");
		wsaahierCode.set(sv.agnum.trim());
		//ILB-1587 Starts
		Hierpf hier = hierpfDAO.getHierpfData(toplevel);
		Optional<Hierpf> isPrsnt = Optional.ofNullable(hier);
		if (isPrsnt.isPresent()) {
			wsaatopLevel.set(hier.getToplevel());
		} else {
			wsaatopLevel.set(SPACE);
		}
		//ILB-1587 End
		wsaauproleCode.set(role);
		wsaauphierCode.set(sv.salebranch.trim());
		hierpf.setAgentkey(wsaaKey.toString());
		hierpf.setToplevel(wsaatopLevel.toString());
		hierpf.setUpperlevel(wsaaupLevel.toString());
		hierpf.setApprovestatus(SPACE);
		hierpf.setEffectivedt(wsaaToday.toInt());
		if (isEQ(wsspcomn.flag, "M"))
			hierpf.setGenerationno(wsaaGennum);
		else
			hierpf.setGenerationno("1");
		hierpf.setReasoncd(SPACE);
		hierpf.setReasondtl(SPACE);
		hierpf.setRegisclass("1");
		hierpf.setActivestatus("1");
		hierpf.setValidflag("1");
		try {
			hierpfDAO.insertHierpfDetails(hierpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert record in HIERPF :", ex);
		}
	}
	
	// Agent Terminate
	protected void updateHierpf3280(Hierpf hrpf) {
		
		int wsaaGen=Integer.parseInt(hrpf.getGenerationno().trim());
		int wsaaGenno=wsaaGen+1;
		wsaaGennum= String.valueOf(wsaaGenno);
		
		hrpf.setValidflag("1");
		hrpf.setActivestatus("2");
		try {
			hierpfDAO.updateHierpf(hrpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update HIERPF:", ex);
		}
		
		hrpf.setValidflag("1");
		hrpf.setActivestatus("2");
		hrpf.setRegisclass("3");
		hrpf.setGenerationno(wsaaGennum);
		hrpf.setEffectivedt(wsaaToday.toInt());
		try {
			hierpfDAO.insertHierpfDetails(hrpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert record in HIERPF:", ex);
		}
		
	}
	
	// Reinstate Terminated Agent
	protected void updateHierpf3480(Hierpf hrpf) {
		
		int wsaaGen=Integer.parseInt(hrpf.getGenerationno().trim());
		int wsaaGenno=wsaaGen+1;
		wsaaGennum= String.valueOf(wsaaGenno);
		
		hrpf.setValidflag("1");
		hrpf.setActivestatus("2");
		try {
			hierpfDAO.updateHierpf(hrpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to update records in HIERPF :", ex);
		}
		
		hrpf.setValidflag("1");
		hrpf.setActivestatus("1");
		hrpf.setRegisclass("4");
		hrpf.setGenerationno(wsaaGennum);
		hrpf.setEffectivedt(wsaaToday.toInt());
		try {
			hierpfDAO.insertHierpfDetails(hrpf);
		} catch (Exception ex) {
			LOGGER.error("Failed to insert record:", ex);
		}
		
	}
	
	// Modify Agent - Sales Branch/Section Remove
		protected void updateHierpf3680(Hierpf hrpf) {
			
			int wsaaGen=Integer.parseInt(hrpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
			
			hrpf.setValidflag("1");
			hrpf.setActivestatus("2");
			try {
				hierpfDAO.updateHierpf(hrpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to  update HIERPF :", ex);
			}
			
			hrpf.setValidflag("1");
			hrpf.setActivestatus("2");
			hrpf.setRegisclass("3");
			hrpf.setGenerationno(wsaaGennum);
			hrpf.setEffectivedt(wsaaToday.toInt());
			try {
				hierpfDAO.insertHierpfDetails(hrpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to insert record in  HIERPF :", ex);
			}
			
		}
		
		// Modify Agent - Sales Branch/Section Change
		protected void updateHierpf3880(Hierpf hrpf, String role) {
			
			int wsaaGen=Integer.parseInt(hrpf.getGenerationno().trim());
			int wsaaGenno=wsaaGen+1;
			wsaaGennum= String.valueOf(wsaaGenno);
			
			hrpf.setValidflag("1");
			hrpf.setActivestatus("2");
			try {
				hierpfDAO.updateHierpf(hrpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to update HIERPF: ", ex);
			}
	
			Agsdpf agsdpf1=agsdpfDAO.getDatabyLevelno(sv.salebranch.trim());
    		Optional<Agsdpf> ifPresent = Optional.ofNullable(agsdpf1);
    		if (ifPresent.isPresent()) 
    		{
    			if(isEQ(agsdpf1.getLevelclass(),"2"))
    			{
    				role3 = "SB";
    			}
    			else if(isEQ(agsdpf1.getLevelclass(),"3"))
    			{
    				role3 = "SS";
    			}
		
    		}
    		
			String role1 = "SS";
    		String levelno1 = role1.concat(sv.salebranch.trim());
    		Hierpf hierpf1 = hierpfDAO.getHierpfData(levelno1);
    		Optional<Hierpf> ifPrsnt = Optional.ofNullable(hierpf1);
    		if (ifPrsnt.isPresent()) 
    		{
    			 wsaatopLevel.set(hierpf1.getToplevel());
    		}
    		else
    		{
    			String role2 = "SB";
        		String levelno2 = role2.concat(sv.salebranch.trim());
        		Hierpf hierpf2 = hierpfDAO.getHierpfData(levelno2);
        		Optional<Hierpf> isPrsnt = Optional.ofNullable(hierpf2);
        		if (isPrsnt.isPresent()) 
        		{
        			 wsaatopLevel.set(hierpf2.getToplevel());
        		}
    		} 	
    		
    		wsaauproleCode.set(role3);
    		wsaauphierCode.set(sv.salebranch.trim());
    		hrpf.setToplevel(wsaatopLevel.toString().trim());
    		hrpf.setUpperlevel(wsaaupLevel.toString().trim());
			hrpf.setValidflag("1");
			hrpf.setActivestatus("1");
			hrpf.setRegisclass("2");
			hrpf.setGenerationno(wsaaGennum);
			hrpf.setEffectivedt(wsaaToday.toInt());
			wsaauproleCode.set(role);
			wsaauphierCode.set(sv.salebranch.trim());
			try {
				hierpfDAO.insertHierpfDetails(hrpf);
			} catch (Exception ex) {
				LOGGER.error("Failed to insert record in  HIERPF:", ex);
			}
			
		}
				
	
	
	/**
	* <pre>
	*3250-AGMO-WRITE.
	*    Move fields from the screen to the contract header.
	*****MOVE TDAY                   TO DTC1-FUNCTION
	*****CALL 'DATCON1' USING DTC1-DATCON1-REC
	*    Move fields from the screen to the contract header.
	*****MOVE SPACES                 TO AGMO-DATA-KEY.
	*****MOVE WSSP-COMPANY           TO AGMO-AGNTCOY.
	*****MOVE AGLF-AGNTNUM           TO AGMO-AGNTNUM.
	*****MOVE WSKY-BATC-BATCCOY      TO AGMO-BATCCOY.
	*****MOVE WSKY-BATC-BATCBRN      TO AGMO-BATCBRN.
	*****MOVE WSKY-BATC-BATCACTYR    TO AGMO-BATCACTYR.
	*****MOVE WSKY-BATC-BATCACTMN    TO AGMO-BATCACTMN.
	*****MOVE WSKY-BATC-BATCTRCDE    TO AGMO-BATCTRCDE.
	*****MOVE WSKY-BATC-BATCBATCH    TO AGMO-BATCBATCH.
	*****MOVE AGLF-AGNTNUM           TO AGMO-NAGNUM.
	*****MOVE DTC1-INT-DATE          TO AGMO-EFFDATE.
	*****MOVE VRCM-TERMID            TO AGMO-TERMID.
	*****MOVE VRCM-USER              TO AGMO-USER.
	*****MOVE VRCM-DATE              TO AGMO-TRANSACTION-DATE.
	*****MOVE VRCM-TIME              TO AGMO-TRANSACTION-TIME.
	*****MOVE WRITR                  TO AGMO-FUNCTION.
	*****MOVE AGMOREC                TO AGMO-FORMAT.
	*****CALL 'AGMOIO' USING AGMO-PARAMS.
	*****IF AGMO-STATUZ              NOT = O-K
	********MOVE AGMO-PARAMS         TO SYSR-PARAMS
	********PERFORM 600-FATAL-ERROR.
	* </pre>
	*/
protected void updteBatchHeader3260()
	{
		/*    Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsspcomn.batchkey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void unlkSoftLock3270()
	{
		/*    Release the soft lock on the contract.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(aglfIO.getAgntnum());
		sftlockrec.enttyp.set("AG");
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callBldenrl3300()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.userid.set(wsspcomn.userid);
		bldenrlrec.prefix.set(fsupfxcpy.agnt);
		bldenrlrec.company.set(wsspcomn.company);
		bldenrlrec.uentity.set(sv.agnum);
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void m200UpdateMacf()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					m200Start();
				case m200UpdCont:
					m200UpdCont();
				case m200Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m200Start()
	{
		macfIO.setDataArea(SPACES);
		if (isEQ(wsspcomn.flag, "T")) {
			macfIO.setAgmvty("T");
			wsaaTmpAgmvty = "T";
		}
		else {
			if (isEQ(wsspcomn.flag, "R")) {
				macfIO.setAgmvty("R");
				wsaaTmpAgmvty = "R";
			}
			else {
				macfIO.setAgmvty("A");
				wsaaTmpAgmvty = "A";
			}
		}
		wsaaRecordFound = "N";
		wsaaTrigger = "N";
		wsaaTranno.set(ZERO);
		macfIO.setMlagttyp(sv.agtype);
		macfIO.setAgntcoy(wsspcomn.company);
		macfIO.setAgntnum(sv.agnum);
		macfIO.setTranno(99999);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		macfIO.setEffdate(wsaaToday);
		macfIO.setAgmvty(SPACES);
		macfIO.setFunction(varcom.begnh);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)
		&& isNE(macfIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		if ((isEQ(wsspcomn.flag, "T")
		|| isEQ(wsspcomn.flag, "R"))
		&& isEQ(macfIO.getStatuz(), varcom.oK)
		&& isEQ(macfIO.getAgntnum(), sv.agnum)
		&& isEQ(macfIO.getAgntcoy(), wsspcomn.company)) {
			macfIO.setCurrto(wsaaToday);
			wsaaTranno.set(macfIO.getTranno());
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.m200UpdCont);
		}
		if (isEQ(macfIO.getStatuz(), varcom.oK)
		&& isEQ(th605rec.crtind, "Y")) {
			if (isNE(macfIO.getAgntnum(), sv.agnum)
			|| isNE(macfIO.getAgntcoy(), wsspcomn.company)) {
				wsaaTrigger = "Y";
				goTo(GotoLabel.m200UpdCont);
			}
			else {
				goTo(GotoLabel.m200Exit);
			}
		}
		if (isEQ(macfIO.getStatuz(), varcom.oK)
		&& isEQ(macfIO.getAgntcoy(), wsspcomn.company)
		&& isEQ(macfIO.getAgntnum(), sv.agnum)
		&& isEQ(macfIO.getCurrto(), varcom.vrcmMaxDate)
		&& isEQ(th605rec.crtind, "N")) {
			if (isLT(macfIO.getEffdate(), wsaaToday)
			|| isNE(macfIO.getAgmvty(), "A")) {
				macfIO.setCurrto(wsaaToday);
				wsaaTranno.set(macfIO.getTranno());
				macfIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, macfIO);
				if (isNE(macfIO.getStatuz(), varcom.oK)) {
					syserrrec.statuz.set(macfIO.getStatuz());
					syserrrec.params.set(macfIO.getParams());
					fatalError600();
				}
			}
			else {
				if (isEQ(macfIO.getEffdate(), wsaaToday)) {
					wsaaTranno.set(macfIO.getTranno());
					wsaaRecordFound = "Y";
				}
			}
		}
	}

protected void m200UpdCont()
	{
		macfIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		/* If a record is rewritten at this point then UPDAT will create   */
		/* a new one with incrementation of Tranno .                       */
		macfIO.setDataArea(SPACES);
		macfIO.setMlagttyp(sv.agtype);
		macfIO.setAgntcoy(wsspcomn.company);
		macfIO.setAgntnum(sv.agnum);
		macfIO.setAgmvty(wsaaTmpAgmvty);
		macfIO.setEffdate(wsaaToday);
		if (isNE(wsaaRecordFound, "Y")) {
			wsaaTranno.add(1);
		}
		macfIO.setTranno(wsaaTranno);
		wsaaIndex.set(1);
		if (isEQ(sv.repsel, SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			/*    MOVE S5035-REPSEL        TO AGLFLNB-REPORTAG      <LA1174>*/
			wsbbReportag.set(sv.repsel);
			while ( !(isEQ(wsbbReportag, SPACES))) {
				/*           UNTIL AGLFLNB-REPORTAG = SPACES                    */
				m100GetReportag();
			}

		}
		wsaaIndex.set(1);
		/*    PERFORM VARYING WSAA-INDEX FROM 1 BY 1               <LA1174>*/
		/*         UNTIL WSAA-INDEX > 4                            <LA1174>*/
		/*       MOVE WSAA-REPORTAG(WSAA-INDEX)                    <LA1174>*/
		/*                        TO MACF-REPORTAG(WSAA-INDEX)     <LA1174>*/
		/*    END-PERFORM.                                         <LA1174>*/
		macfIO.setZrptga(wsaaReportag[1]);
		macfIO.setZrptgb(wsaaReportag[2]);
		macfIO.setZrptgc(wsaaReportag[3]);
		macfIO.setZrptgd(wsaaReportag[4]);
		macfIO.setCurrfrom(varcom.vrcmMaxDate);
		macfIO.setCurrto(varcom.vrcmMaxDate);
		macfIO.setCurrfrom(wsaaToday);
		macfIO.setMlparorc(ZERO);
		macfIO.setFunction(varcom.updat);
		macfIO.setFormat(formatsInner.macfrec);
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
	}

protected void clrrioCall3700()
	{
		/*CALL*/
		/*    Call the Role I/O Module.*/
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.forepfx.set("AG");
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(sv.agnum);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void agntlagioCall3800()
	{
		/*CALL*/
		/*    Call the FSU Agent I/O module.*/
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextProgram4010();
					valPrevSelect4020();
					checkSelections4030();
				case alreadyKept4040:
					alreadyKept4040();
				case endChoices4050:
					endChoices4050();
				case exit4090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If returning from an optional selection then retreive the*/
		/*    Life Agent details.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			aglfIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, aglfIO);
			if (isNE(aglfIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(aglfIO.getParams());
				fatalError600();
			}
		}
		/*    If returning from an optional selection then retreive the*/
		/*    FSU Agent details.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			agntlagIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, agntlagIO);
			if (isNE(agntlagIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agntlagIO.getParams());
				fatalError600();
			}
		}
		/*    If returning from an optional selection then release any*/
		/*    held Client details.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			cltsIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(cltsIO.getParams());
				fatalError600();
			}
		}
		/*    Initialise Gen Switch.*/
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		/*    If first time into this section (stack action blank) save*/
		/*    next eight programs in stack*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				saveProgram4100();
			}
		}
	}

protected void valPrevSelect4020()
	{
		/*    Check if bank details selected previously*/
		if (isEQ(sv.ddind, "?")) {
			a100BankDetsCheck();
		}
		/*    Check if broker contacts selected previously*/
		if (isEQ(sv.bctind, "?")) {
			b100BrokerContacts();
		}
		/*    Check if OR Detalis selected previously                      */
		/* IF S5035-ZRORIND             = '?'                           */
		/*    PERFORM D100-OR-DETAILS                                   */
		/* END-IF.                                                      */
		if (isEQ(sv.zrorind, "?")) {
			d200OrDetails();
		}
		/*    Check if tied agent details selected previously*/
		if (isEQ(sv.tagd, "?")) {
			c100TiedAgencies();
		}
		/*    Check if Client details selected previously*/
		if (isEQ(sv.clientind, "?")) {
			wsspcomn.flag.set(wsaaSaveFlag);
			sv.clientind.set("+");
		}
		/*    Check if Agency Details License selected previously*/
		if (isEQ(sv.agncysalic, "?")) {
			checkagencysaleindi();
		}
		
		
		if (isEQ(sv.qualification, "?")) {
			checkQalification();
			
		}
	}

protected void checkQalification() {
		
	List<Agqfpf> agqfpfList = agqfpfDAO.getAgqfListbyAgentNumber(aglfIO.getAgntnum().toString().trim());
	if (agqfpfList.size() <= 0) {
		sv.qualification.set(" ");
		return;
	} else {
		sv.qualification.set("+");
	}
	
}

protected void checkSelections4030()
	{
		/*    Check if bank details is selected*/
		if (isEQ(sv.ddind, "X")) {
			gensswrec.function.set("B");
			sv.ddind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		/*    Check if OR details is selected                              */
		/* IF S5035-ZRORIND              = 'X'                          */
		/*    MOVE 'F'                 TO GENS-FUNCTION                 */
		/*    MOVE '?'                 TO S5035-ZRORIND                 */
		/*    PERFORM 4300-CALL-GENSSW                                  */
		/*    GO TO 4090-EXIT                                           */
		/* END-IF.                                                      */
		if (isEQ(sv.zrorind, "X")) {
			gensswrec.function.set("F");
			sv.zrorind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		/*    Check if broker contacts selected*/
		if (isEQ(sv.bctind, "X")) {
			gensswrec.function.set("C");
			sv.bctind.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		/*    Check if tied details selected*/
		if (isEQ(sv.tagd, "X")) {
			gensswrec.function.set("A");
			sv.tagd.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		/* ICIL - 46*/
		if (isEQ(sv.agncysalic, "X")) {
			gensswrec.function.set("G");
			sv.agncysalic.set("?");
			callGenssw4300();
			goTo(GotoLabel.exit4090);
		}
		
		//ILJ-4 start
				if ((iljFlag) && isEQ(sv.qualification, "X")) {
					gensswrec.function.set("G");
					sv.qualification.set("?");
					callGenssw4300();
					goTo(GotoLabel.exit4090);
				}
				//end
				
		/*    Check if Client Details selected*/
		if (isNE(sv.clientind, "X")) {
			goTo(GotoLabel.endChoices4050);
		}
		
		
		/*MOVE 'D'                    TO GENS-FUNCTION.                */
		/*PERFORM 4300-CALL-GENSSW.                                    */
		wsaaCltskey.cltsClntpfx.set("CN");
		/* MOVE WSSP-COMPANY           TO CLTS-CLNTCOY.                 */
		wsaaCltskey.cltsClntcoy.set(wsspcomn.fsuco);
		wsaaCltskey.cltsClntnum.set(agntlagIO.getClntnum());
		Clntpf clntpf = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), agntlagIO.getClntnum().toString());
		if (clntpf == null) {
			syserrrec.params.set(agntlagIO.getClntnum().toString());
			fatalError600();
		}
		wsspcomn.clntkey.set(wsaaCltskey);
		if ("P".equals(clntpf.getClttype())) {
			gensswrec.function.set("D");
		}
		else {
			gensswrec.function.set("E");
		}
		callGenssw4300();
		/* AGLF and AGNTLAG records have already been kept in 3100-section */
		/* for all actions except for inquiry. The main reason to put this */
		/* in is because at agent create, those two records would not have */
		/* been written yet, so the following READS would casue a database */
		/* error with MRNF ststus!                                         */
		if (isNE(wsspcomn.flag, "I")) {
			goTo(GotoLabel.alreadyKept4040);
		}
		agntlagIO.setAgntnum(aglfIO.getAgntnum());
		aglfIO.setFunction("READS");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		agntlagIO.setAgntcoy(wsspcomn.company);
		agntlagIO.setAgntnum(sv.agnum);
		agntlagIO.setFunction(varcom.reads);
		agntlagIO.setFormat(formatsInner.agntlagrec);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

protected void alreadyKept4040()
	{
		wsaaSaveFlag.set(wsspcomn.flag);
		wsspcomn.flag.set("I");
		sv.clientind.set("?");
		goTo(GotoLabel.exit4090);
	}

protected void endChoices4050()
	{
		/*    No more selected (or none)*/
		/*       - restore stack form wsaa to wssp*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsaaX.set(wsspcomn.programPtr);
			wsaaY.set(0);
			for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
				restoreProgram4200();
			}
		}
		/*    If current stack action is * then re-display screen*/
		/*       (in this case, some other option(s) were requested)*/
		/*    Otherwise continue as normal.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.programPtr.add(1);
		}
	}

protected void saveProgram4100()
	{
		/*SAVE*/
		wsaaX.add(1);
		wsaaY.add(1);
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		/*EXIT*/
	}

protected void restoreProgram4200()
	{
		/*RESTORE*/
		wsaaX.add(1);
		wsaaY.add(1);
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		/*EXIT*/
	}

protected void callGenssw4300()
	{
		callSubroutine4310();
	}

protected void callSubroutine4310()
	{
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set("H093");
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    Load from gensw to WSSP.*/
		compute(wsaaX, 0).set(add(1, wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void loadProgram4400()
	{
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}

protected void a100CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A190-EXIT*/
	}

protected void m001UpdateLevel1()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					m001Reportag01();
				case m001LevelCall:
					m001LevelCall();
				case m001Next:
					m001Next();
				case m001Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m001Reportag01()
	{
		if (isEQ(sv.repsel, wsaaInitRepsel)) {
			goTo(GotoLabel.m001Exit);
		}
		macfflvIO.setDataKey(SPACES);
		wsaaAgnum.set(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(wsspcomn.company);
		macfflvIO.setZrptga(sv.agnum);
		wsaaAgnum.set(sv.agnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(formatsInner.macfflvrec);
	}

protected void m001LevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.endp)
		|| isNE(macfflvIO.getAgntcoy(), wsspcomn.company)
		|| isNE(macfflvIO.getZrptga(), wsaaAgnum)) {
			goTo(GotoLabel.m001Exit);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getCurrto(), varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m001LevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)) {
			wsaaStore1Agnum.set(SPACES);
			wsaaStore1Zrptga.set(SPACES);
			wsaaStore1Effdate.set(varcom.vrcmMaxDate);
			wsaaStore1Zrptga.set(macfflvIO.getZrptga());
			wsaaStore1Agnum.set(macfflvIO.getAgntnum());
			wsaaStore1Effdate.set(macfflvIO.getEffdate());
			wsaaStore1Tranno.set(macfflvIO.getTranno());
			wsaaStore1Agmvty.set(macfflvIO.getAgmvty());
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(), wsspcomn.company)
		&& isEQ(macfflvIO.getZrptga(), wsaaAgnum)) {
			/*       MACFFLV-ZRPTGB        NOT = S5035-PREPSEL                 */
			if (isLT(macfflvIO.getEffdate(), wsaaToday)
			|| isNE(macfflvIO.getAgmvty(), "A")) {
				macfflvIO.setCurrto(wsaaToday);
				wsaaTranno.set(macfflvIO.getTranno());
				macfflvIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, macfflvIO);
				if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
					syserrrec.statuz.set(macfflvIO.getStatuz());
					syserrrec.params.set(macfflvIO.getParams());
					fatalError600();
				}
				macfflvIO.setAgmvty("A");
				wsaaTranno.add(1);
				macfflvIO.setTranno(wsaaTranno);
				macfflvIO.setFunction(varcom.writr);
				macfflvIO.setEffdate(wsaaToday);
				macfflvIO.setCurrfrom(wsaaToday);
				macfflvIO.setCurrto(varcom.vrcmMaxDate);
			}
			else {
				if (isEQ(macfflvIO.getEffdate(), wsaaToday)) {
					macfflvIO.setFunction(varcom.rewrt);
				}
				else {
					goTo(GotoLabel.m001Next);
				}
			}
		}
		wsbbReportag.set(sv.agnum);
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag, SPACES))) {
			m100GetReportag();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		if (isEQ(macfflvIO.getMlagttyp(), "AG")) {
			goTo(GotoLabel.m001Next);
		}
		else {
			wsaaAgnum.set(macfflvIO.getAgntnum());
			m00xUpdateLevelX();
		}
	}

protected void m001Next()
	{
		/*   Reset Initial Key values                                      */
		wsaaTranno.set(ZERO);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m001LevelCall);
	}

protected void m00xUpdateLevelX()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					m00xReportag0x();
				case m00xLevelCall:
					m00xLevelCall();
				case m00xNext:
					m00xNext();
				case m00xExit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m00xReportag0x()
	{
		macfflvIO.setDataKey(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(wsspcomn.company);
		macfflvIO.setZrptga(wsaaAgnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(formatsInner.macfflvrec);
	}

protected void m00xLevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.endp)
		|| isNE(macfflvIO.getAgntcoy(), wsspcomn.company)
		|| isNE(macfflvIO.getZrptga(), wsaaAgnum)) {
			macfflvIO.setDataKey(SPACES);
			wsaaAgnum.set(SPACES);
			macfflvIO.setAgntcoy(wsspcomn.company);
			macfflvIO.setZrptga(wsaaStore1Zrptga);
			wsaaAgnum.set(wsaaStore1Zrptga);
			macfflvIO.setAgntnum(wsaaStore1Agnum);
			macfflvIO.setEffdate(wsaaStore1Effdate);
			macfflvIO.setTranno(wsaaStore1Tranno);
			macfflvIO.setAgmvty(wsaaStore1Agmvty);
			macfflvIO.setFunction(varcom.begnh);
			macfflvIO.setFormat(formatsInner.macfflvrec);
			SmartFileCode.execute(appVars, macfflvIO);
			if (isNE(macfflvIO.getStatuz(), varcom.oK)
			&& isNE(macfflvIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(macfflvIO.getStatuz());
				syserrrec.params.set(macfflvIO.getParams());
				fatalError600();
			}
			goTo(GotoLabel.m00xExit);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getCurrto(), varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m00xLevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)) {
			wsaaStorexAgnum.set(SPACES);
			wsaaStorexZrptga.set(SPACES);
			wsaaStorexEffdate.set(varcom.vrcmMaxDate);
			wsaaStorexZrptga.set(macfflvIO.getZrptga());
			wsaaStorexAgnum.set(macfflvIO.getAgntnum());
			wsaaStorexEffdate.set(macfflvIO.getEffdate());
			wsaaStorexTranno.set(macfflvIO.getTranno());
			wsaaStorexAgmvty.set(macfflvIO.getAgmvty());
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(), wsspcomn.company)
		&& isEQ(macfflvIO.getZrptga(), wsaaAgnum)) {
			if (isLT(macfflvIO.getEffdate(), wsaaToday)
			|| isNE(macfflvIO.getAgmvty(), "A")) {
				wsaaTranno.set(macfflvIO.getTranno());
				macfflvIO.setCurrto(wsaaToday);
				macfflvIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, macfflvIO);
				if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
					syserrrec.statuz.set(macfflvIO.getStatuz());
					syserrrec.params.set(macfflvIO.getParams());
					fatalError600();
				}
				macfflvIO.setAgmvty("A");
				wsaaTranno.add(1);
				macfflvIO.setTranno(wsaaTranno);
				macfflvIO.setFunction(varcom.writr);
				macfflvIO.setEffdate(wsaaToday);
				macfflvIO.setCurrfrom(wsaaToday);
				macfflvIO.setCurrto(varcom.vrcmMaxDate);
			}
			else {
				if (isEQ(macfflvIO.getEffdate(), wsaaToday)) {
					macfflvIO.setFunction(varcom.rewrt);
				}
				else {
					goTo(GotoLabel.m00xNext);
				}
			}
		}
		wsbbReportag.set(wsaaAgnum);
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag, SPACES))) {
			m100GetReportag();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		if (isEQ(macfflvIO.getMlagttyp(), "AG")) {
			goTo(GotoLabel.m00xNext);
		}
		else {
			wsaaAgnum.set(macfflvIO.getAgntnum());
			m00yUpdateLevelY();
		}
	}

protected void m00xNext()
	{
		macfflvIO.setDataArea(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(wsspcomn.company);
		macfflvIO.setZrptga(wsaaStorexZrptga);
		macfflvIO.setAgntnum(wsaaStorexAgnum);
		wsaaAgnum.set(wsaaStorexZrptga);
		macfflvIO.setEffdate(wsaaStorexEffdate);
		macfflvIO.setAgmvty(wsaaStorexAgmvty);
		macfflvIO.setTranno(wsaaStorexTranno);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m00xLevelCall);
	}

protected void m00yUpdateLevelY()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					m00yReportag0y();
				case m00yLevelCall:
					m00yLevelCall();
				case m00yNext:
					m00yNext();
				case m00yExit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void m00yReportag0y()
	{
		macfflvIO.setDataKey(SPACES);
		wsaaTranno.set(ZERO);
		macfflvIO.setAgntcoy(wsspcomn.company);
		macfflvIO.setZrptga(wsaaAgnum);
		macfflvIO.setAgntnum(SPACES);
		macfflvIO.setEffdate(varcom.vrcmMaxDate);
		macfflvIO.setTranno(99999);
		macfflvIO.setFunction(varcom.begnh);
		macfflvIO.setFormat(formatsInner.macfflvrec);
	}

protected void m00yLevelCall()
	{
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.endp)
		|| isNE(macfflvIO.getAgntcoy(), wsspcomn.company)
		|| isNE(macfflvIO.getZrptga(), wsaaAgnum)) {
			goTo(GotoLabel.m00yExit);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isNE(macfflvIO.getCurrto(), varcom.vrcmMaxDate)) {
			macfflvIO.setFunction(varcom.nextr);
			goTo(GotoLabel.m00yLevelCall);
		}
		if (isEQ(macfflvIO.getStatuz(), varcom.oK)
		&& isEQ(macfflvIO.getAgntcoy(), wsspcomn.company)
		&& isEQ(macfflvIO.getZrptga(), wsaaAgnum)) {
			/*      (MACFFLV-ZRPTGB        NOT = WSAA-ZRPTGB   OR              */
			/*       MACFFLV-ZRPTGC        NOT = WSAA-ZRPTGC   OR              */
			/*       MACFFLV-ZRPTGD        NOT = WSAA-ZRPTGD)                  */
			if (isLT(macfflvIO.getEffdate(), wsaaToday)
			|| isNE(macfflvIO.getAgmvty(), "A")) {
				wsaaTranno.set(macfflvIO.getTranno());
				macfflvIO.setCurrto(wsaaToday);
				macfflvIO.setFunction(varcom.rewrt);
				SmartFileCode.execute(appVars, macfflvIO);
				if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
					syserrrec.statuz.set(macfflvIO.getStatuz());
					syserrrec.params.set(macfflvIO.getParams());
					fatalError600();
				}
				macfflvIO.setAgmvty("A");
				wsaaTranno.add(1);
				macfflvIO.setTranno(wsaaTranno);
				macfflvIO.setFunction(varcom.writr);
				macfflvIO.setEffdate(wsaaToday);
				macfflvIO.setCurrfrom(wsaaToday);
				macfflvIO.setCurrto(varcom.vrcmMaxDate);
			}
			else {
				if (isEQ(macfflvIO.getEffdate(), wsaaToday)) {
					macfflvIO.setFunction(varcom.rewrt);
				}
				else {
					goTo(GotoLabel.m00yNext);
				}
			}
		}
		wsbbReportag.set(wsaaAgnum);
		wsaaReportags.set(SPACES);
		wsaaIndex.set(1);
		while ( !(isEQ(wsbbReportag, SPACES))) {
			m100GetReportag();
		}

		macfflvIO.setZrptgb(wsaaReportag[2]);
		macfflvIO.setZrptgc(wsaaReportag[3]);
		macfflvIO.setZrptgd(wsaaReportag[4]);
		SmartFileCode.execute(appVars, macfflvIO);
		if (isNE(macfflvIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(macfflvIO.getStatuz());
			syserrrec.params.set(macfflvIO.getParams());
			fatalError600();
		}
	}

protected void m00yNext()
	{
		wsaaTranno.set(ZERO);
		macfflvIO.setFunction(varcom.nextr);
		goTo(GotoLabel.m00yLevelCall);
	}

	protected void m100GetReportag() {
		// ILIFE-6896
		Aglflnbpf aglflnbpf = aglflnbpfDAO.getAglflnb(wsbbReportag.toString().trim(), wsspcomn.company.toString().trim());
		if (aglflnbpf != null) {
			wsaaReportag[wsaaIndex.toInt()].set(aglflnbpf.getAgntnum());
			wsaaIndex.add(1);
			wsbbReportag.set(aglflnbpf.getReportag());
		}

	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
	private FixedLengthStringData agntlagrec = new FixedLengthStringData(10).init("AGNTLAGREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData clblrec = new FixedLengthStringData(10).init("CLBLREC");
	private FixedLengthStringData aglfrptrec = new FixedLengthStringData(10).init("AGLFRPTREC");
	private FixedLengthStringData macfrec = new FixedLengthStringData(10).init("MACFREC");
	private FixedLengthStringData macfflvrec = new FixedLengthStringData(10).init("MACFFLVREC");
}
/*
 * Class transformed  from Data Structure WSAA-STORED-SCREEN-FIELDS--INNER
 */
private static final class WsaaStoredScreenFieldsInner {
		/* WSAA-STORED-SCREEN-FIELDS */
	private FixedLengthStringData wsaaStoreAgtype = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreAracde = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStoreRepsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaStorePaymth = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStorePayfrq = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoreBcmtab = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStoreScmtab = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStoreRcmtab = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStoreOcmtab = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaStoreAgentClass = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaStoreTsalesunt = new FixedLengthStringData(5);
}
public void updateCustomerSpecificFields(String agntCoy, String agntNum){
	
}
}
