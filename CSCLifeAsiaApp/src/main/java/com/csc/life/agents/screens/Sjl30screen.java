package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sjl30screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 2, 79}); 
	}


	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl30ScreenVars sv = (Sjl30ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.Sjl30screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl30ScreenVars screenVars = (Sjl30ScreenVars)pv;
		ScreenRecord.setClassStringFormatting(pv);
	
	}
	
	public static void clear(VarModel pv) {
		Sjl30ScreenVars screenVars = (Sjl30ScreenVars) pv;
		ScreenRecord.clear(pv);
	}
}
