package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM507
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm507ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(908);
	public FixedLengthStringData dataFields = new FixedLengthStringData(460).isAPartOf(dataArea, 0);
	public FixedLengthStringData acctype = DD.acctype.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData acdess = new FixedLengthStringData(60).isAPartOf(dataFields, 2);
	public FixedLengthStringData[] acdes = FLSArrayPartOfStructure(2, 30, acdess, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(acdess, 0, FILLER_REDEFINE);
	public FixedLengthStringData acdes01 = DD.acdes.copy().isAPartOf(filler,0);
	public FixedLengthStringData acdes02 = DD.acdes.copy().isAPartOf(filler,30);
	public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(dataFields,62);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData agntfm = DD.agntfm.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData agnum = DD.agnum.copy().isAPartOf(dataFields,112);
	public FixedLengthStringData agtydesc = DD.agtydesc.copy().isAPartOf(dataFields,120);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,150);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,152);
	public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(dataFields,155);
	public FixedLengthStringData accountType = DD.atype.copy().isAPartOf(dataFields,185);
	public FixedLengthStringData clntsel = DD.clntsel.copy().isAPartOf(dataFields,187);
	public FixedLengthStringData cltname = DD.cltname.copy().isAPartOf(dataFields,197);
	public ZonedDecimalData currfrom = DD.currfrom.copyToZonedDecimal().isAPartOf(dataFields,244);
	public ZonedDecimalData currto = DD.currto.copyToZonedDecimal().isAPartOf(dataFields,252);
	public FixedLengthStringData descn = DD.descn.copy().isAPartOf(dataFields,260);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,295);
	public FixedLengthStringData exclAgmt = DD.excagr.copy().isAPartOf(dataFields,303);
	public FixedLengthStringData gacc = DD.gacc.copy().isAPartOf(dataFields,304);
	public ZonedDecimalData mlparorc = DD.mlparorc.copyToZonedDecimal().isAPartOf(dataFields,312);
	public FixedLengthStringData mlprcind = DD.mlprcind.copy().isAPartOf(dataFields,317);
	public FixedLengthStringData repname = DD.repname.copy().isAPartOf(dataFields,318);
	public FixedLengthStringData reportag = DD.reportag.copy().isAPartOf(dataFields,365);
	public FixedLengthStringData reportton = DD.reportton.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData repsel = DD.repsel.copy().isAPartOf(dataFields,420);
	public FixedLengthStringData tydesc = DD.tydesc.copy().isAPartOf(dataFields,430);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 460);
	public FixedLengthStringData acctypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acdessErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] acdesErr = FLSArrayPartOfStructure(2, 4, acdessErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(acdessErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData acdes01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData acdes02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData agbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData agntfmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData agnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData agtydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData aradescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData atypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData clntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData cltnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData currfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData currtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData descnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData excagrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData gaccErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mlparorcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData mlprcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData repnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData reportagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData reporttonErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData repselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData tydescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 572);
	public FixedLengthStringData[] acctypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData acdessOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] acdesOut = FLSArrayPartOfStructure(2, 12, acdessOut, 0);
	public FixedLengthStringData[][] acdesO = FLSDArrayPartOfArrayStructure(12, 1, acdesOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(acdessOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] acdes01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] acdes02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] agbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] agntfmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] agnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] agtydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] aradescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] atypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] clntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] cltnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] currfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] currtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] descnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] excagrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] gaccOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] mlparorcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] mlprcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] repnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] reportagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] reporttonOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] repselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] tydescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData currfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData currtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);

	public LongData Sm507screenWritten = new LongData(0);
	public LongData Sm507protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm507ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(atypeOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(repselOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntfmOut,new String[] {"04","44","-04","44",null, null, null, null, null, null, null, null});
		fieldIndMap.put(descnOut,new String[] {null, null, null, "44",null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlprcindOut,new String[] {null, "48",null, "47",null, null, null, null, null, null, null, null});
		fieldIndMap.put(currfromOut,new String[] {"06","46","-06","45",null, null, null, null, null, null, null, null});
		fieldIndMap.put(currtoOut,new String[] {"07","46","-07","45",null, null, null, null, null, null, null, null});
		fieldIndMap.put(mlparorcOut,new String[] {null, "46",null, "45",null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntselOut,new String[] {"05","46","-05","45",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {clntsel, cltname, agnum, dteapp, agtydesc, agbrdesc, aradesc, repname, accountType, exclAgmt, aracde, reportag, gacc, agntbr, agtype, tydesc, reportton, repsel, agntfm, descn, mlprcind, currfrom, currto, mlparorc, agntsel, acdes01, acctype, acdes02};
		screenOutFields = new BaseData[][] {clntselOut, cltnameOut, agnumOut, dteappOut, agtydescOut, agbrdescOut, aradescOut, repnameOut, atypeOut, excagrOut, aracdeOut, reportagOut, gaccOut, agntbrOut, agtypeOut, tydescOut, reporttonOut, repselOut, agntfmOut, descnOut, mlprcindOut, currfromOut, currtoOut, mlparorcOut, agntselOut, acdes01Out, acctypeOut, acdes02Out};
		screenErrFields = new BaseData[] {clntselErr, cltnameErr, agnumErr, dteappErr, agtydescErr, agbrdescErr, aradescErr, repnameErr, atypeErr, excagrErr, aracdeErr, reportagErr, gaccErr, agntbrErr, agtypeErr, tydescErr, reporttonErr, repselErr, agntfmErr, descnErr, mlprcindErr, currfromErr, currtoErr, mlparorcErr, agntselErr, acdes01Err, acctypeErr, acdes02Err};
		screenDateFields = new BaseData[] {dteapp, currfrom, currto};
		screenDateErrFields = new BaseData[] {dteappErr, currfromErr, currtoErr};
		screenDateDispFields = new BaseData[] {dteappDisp, currfromDisp, currtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm507screen.class;
		protectRecord = Sm507protect.class;
	}

}
