package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Alocno;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.agents.screens.Sjl30ScreenVars;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl30 extends ScreenProgCS{

	private FixedLengthStringData wsaaAlocnoThereFlag = new FixedLengthStringData(1).init("N");
	private Validator alocnoAlreadyThere = new Validator(wsaaAlocnoThereFlag, "Y");
	private Sjl30ScreenVars sv =   getLScreenVars();
	private FixedLengthStringData wsaaAgncynum = new FixedLengthStringData(8);
	private Batckey wsaaBatchkey = new Batckey();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL30");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sanctnrec sanctnrec = new Sanctnrec();
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private AgncypfDAO agncypfDAO =  getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private Agncypf agncypf= new Agncypf();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Alocnorec alocnorec = new Alocnorec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();

		private enum GotoLabel implements GOTOInterface {
			DEFAULT,
			exit2190,
			exit2490,
			passAgentDetails3020,
			batching3080,
			exit3090,
			callAlocno3210
	}
	
	public Pjl30() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl30", AppVars.getInstance(), sv);
	}
	final Sjl30ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sjl30ScreenVars.class);
	}
	
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
		}
	
	
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
}
	
	protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspcomn.clntkey.set(SPACES);
		wsaaAgncynum.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsspcomn.lastprog.set("SJL30");
	}
	
	
	@Override
	protected void preScreenEdit() {
		return ;
		
	}
	
	protected void screenEdit2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)){
			if (isNE(scrnparams.statuz, "BACH"))
				validateKeys2200();
		if (isNE(sv.errorIndicators, SPACES)) 
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
 }
	protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			e.printStackTrace();
		}
	}

	protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

	
	protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

	protected void validateKeys2200() {

		if (isEQ(subprogrec.key1, SPACES)) {
			return;
		}
		if (isNE(sv.agncysel, SPACE) && isEQ(sv.action, "A")) {
			agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim(), 1);
			if (null == agncypf || agncypf.getAgncynum().isEmpty()) {
				sv.agncyselErr.set("JL38");
				return;
			} else {
				sv.agncyselErr.set("RP87");
				return;
			}
		}
		if (isEQ(sv.agncysel, SPACE) && isNE(sv.action, "A")) {
			sv.agncyselErr.set("JL38");
		} else if(isNE(sv.agncysel, SPACE)){
			wsspcomn.chdrChdrnum.set(sv.agncysel.toString());
			agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), sv.agncysel.toString().trim(), 1);
			if (null == agncypf || agncypf.getAgncynum().isEmpty() && isEQ(subprogrec.key1, "Y")) {
				sv.agncyselErr.set("JL38");
				return;
			}
			if (isEQ(subprogrec.key1, "Y")) {
				checkRuser2500();
				if (isNE(sdasancrec.statuz, varcom.oK)) {
					sv.agncyselErr.set(sdasancrec.statuz);
					return;
				}
			}
			
		}
	}
	
	protected void checkRuser2500()
	{
		begin2510();
	}

protected void begin2510()
	{
		initialize(sdasancrec.sancRec);
		sdasancrec.function.set("VENTY");
		sdasancrec.statuz.set(varcom.oK);
		sdasancrec.userid.set(wsspcomn.userid);
		sdasancrec.entypfx.set(fsupfxcpy.agnt);
		sdasancrec.entycoy.set(wsspcomn.company);
		sdasancrec.entynum.set(sv.agncysel);
		callProgram(Sdasanc.class, sdasancrec.sancRec);
		if (isEQ(sdasancrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sdasancrec.statuz);
			syserrrec.params.set(sdasancrec.sancRec);
			fatalError600();
		}
	}
	protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateWssp3010();
				case passAgentDetails3020:
					passAgentDetails3020();
				case exit3090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void updateWssp3010()
	{
		int i=1;
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[i++].set(subprogrec.nxt1prog);
		wsspcomn.secProg[i++].set(subprogrec.nxt2prog);
		wsspcomn.secProg[i++].set(subprogrec.nxt3prog);
		wsspcomn.secProg[i].set(subprogrec.nxt4prog);

		if (isEQ(scrnparams.action, "A")) {
				wsspcomn.flag.set("A");
			}
			else {
				if (isEQ(scrnparams.action, "C")) {
					wsspcomn.flag.set("I");
				}
				else {
					if (isEQ(scrnparams.action, "D")) {
						wsspcomn.flag.set("T");
					}
					else {
						if(isEQ(scrnparams.action, "E")) {
							wsspcomn.flag.set("R");
						}else {
						wsspcomn.flag.set("M");
						}
					}
				}
			}
		
		if (isEQ(sv.agncysel, SPACES)
		&& isEQ(subprogrec.key1, "N")) {
			allocateNumber3200();
			if(isEQ(sv.agncysel,SPACE)) {
				sv.agncyselErr.set("F985");
			    wsspcomn.edterror.set("Y");
			}
		}
		
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.passAgentDetails3020);
		}
		if(isNE(sv.agncysel, SPACES)) {
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("AG");
		if (isNE(sv.agncysel, SPACES)) {
			sftlockrec.entity.set(agncypf.getAgncynum());
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.agncyselErr.set("G354");
		}
	}
	}

protected void passAgentDetails3020()
	{
		if (isEQ(subprogrec.key1, "N")) {
			initialiseHeader3300();
		}
		
	}

protected void batching3080()
{
	if (isEQ(subprogrec.bchrqd, "Y")
	&& isEQ(sv.errorIndicators, SPACES)) {
		updateBatchControl3100();
	}
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
}

protected void updateBatchControl3100()
{
	/*AUTOMATIC-BATCHING*/
	batcdorrec.function.set("AUTO");
	batcdorrec.tranid.set(wsspcomn.tranid);
	batcdorrec.batchkey.set(wsspcomn.batchkey);
	callProgram(Batcdor.class, batcdorrec.batcdorRec);
	if (isNE(batcdorrec.statuz, varcom.oK)) {
		sv.actionErr.set(batcdorrec.statuz);
	}
	wsspcomn.batchkey.set(batcdorrec.batchkey);
	/*EXIT*/
}

protected void allocateNumber3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case callAlocno3210:
					callAlocno3210();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void callAlocno3210()
	{
		wsaaAlocnoThereFlag.set("N");
		alocnorec.function.set("NEXT");
		alocnorec.prefix.set("AN");
		alocnorec.company.set(wsspcomn.company);
		alocnorec.genkey.set(wsspcomn.branch);
		callProgram(Alocno.class, alocnorec.alocnoRec);
		if (isNE(alocnorec.statuz, varcom.oK)) {
			alocnorec.genkey.set(SPACES);
			callProgram(Alocno.class, alocnorec.alocnoRec);
		}
		if (isEQ(alocnorec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(alocnorec.statuz);
			fatalError600();
		}
		if (isNE(alocnorec.statuz, varcom.oK)) {
			sv.agncyselErr.set(alocnorec.statuz);
		}
		else {
			checkAlocno3230();
			if (alocnoAlreadyThere.isTrue()) {
				goTo(GotoLabel.callAlocno3210);
			}
			else {
				agncypf.setAgncynum(alocnorec.alocNo.toString());
				sv.agncysel.set(alocnorec.alocNo);
				wsspcomn.chdrCownnum.set(sv.agncysel);
			}
		}
	}

	protected void checkAlocno3230() {
		// ILIFE-6896
		Agncypf agncylag = agncypfDAO.getAgncy(wsspcomn.company.toString(),alocnorec.alocNo.toString(),1);
		if (agncylag != null) {
			wsaaAlocnoThereFlag.set("Y");
		}
	}

protected void initialiseHeader3300()
	{
	
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
