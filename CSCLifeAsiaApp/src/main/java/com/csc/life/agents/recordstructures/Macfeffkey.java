package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:32
 * Description:
 * Copybook name: MACFEFFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Macfeffkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData macfeffFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData macfeffKey = new FixedLengthStringData(64).isAPartOf(macfeffFileKey, 0, REDEFINE);
  	public FixedLengthStringData macfeffAgntcoy = new FixedLengthStringData(1).isAPartOf(macfeffKey, 0);
  	public FixedLengthStringData macfeffAgntnum = new FixedLengthStringData(8).isAPartOf(macfeffKey, 1);
  	public PackedDecimalData macfeffEffdate = new PackedDecimalData(8, 0).isAPartOf(macfeffKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(macfeffKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(macfeffFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		macfeffFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}