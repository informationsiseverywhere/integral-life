package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:07
 * Description:
 * Copybook name: AGLFLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglflnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglflnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglflnbKey = new FixedLengthStringData(256).isAPartOf(aglflnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglflnbAgntcoy = new FixedLengthStringData(1).isAPartOf(aglflnbKey, 0);
  	public FixedLengthStringData aglflnbAgntnum = new FixedLengthStringData(8).isAPartOf(aglflnbKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(aglflnbKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglflnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglflnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}