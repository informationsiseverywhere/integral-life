package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5644
 * @version 1.0 generated on 30/08/09 06:46
 * @author Quipoz
 */
public class S5644ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(177);
	public FixedLengthStringData dataFields = new FixedLengthStringData(65).isAPartOf(dataArea, 0);
	public FixedLengthStringData compysubr = DD.commpysubr.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData subrev = DD.subrev.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData rfdsbrtine = DD.rfdsbrtine.copy().isAPartOf(dataFields,58);//IBPLIFE-5252
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 65);
	public FixedLengthStringData commpysubrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData subrevErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData rfdsbrtineErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 93);
	public FixedLengthStringData[] commpysubrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] subrevOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] rfdsbrtineOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5644screenWritten = new LongData(0);
	public LongData S5644protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5644ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {compysubr, subrev, company, tabl, item, longdesc, rfdsbrtine};
		screenOutFields = new BaseData[][] {commpysubrOut, subrevOut, companyOut, tablOut, itemOut, longdescOut, rfdsbrtineOut};
		screenErrFields = new BaseData[] {commpysubrErr, subrevErr, companyErr, tablErr, itemErr, longdescErr, rfdsbrtineErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5644screen.class;
		protectRecord = S5644protect.class;
	}

}
