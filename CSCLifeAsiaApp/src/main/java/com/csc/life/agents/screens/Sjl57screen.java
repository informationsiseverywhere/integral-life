package com.csc.life.agents.screens;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;
/**
 * Screen record for SCREEN
 * @version 1.0 generated on 06/09/20 05:43
 * @author agoel51
 */
public class Sjl57screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 9, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl57ScreenVars sv = (Sjl57ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl57screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl57ScreenVars screenVars = (Sjl57ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.clntsel.setClassString("");
		screenVars.company.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.agbrdesc.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.aradesc.setClassString("");
		screenVars.levelno.setClassString("");
		screenVars.leveltyp.setClassString("");
		screenVars.leveldesc.setClassString("");
		screenVars.saledept.setClassString("");
		screenVars.saledptdes.setClassString("");
		screenVars.clntname.setClassString("");
		screenVars.indxflg.setClassString("");
		screenVars.select.setClassString("");
	}

/**
 * Clear all the variables in Sjl57screen
 */
	public static void clear(VarModel pv) {
		Sjl57ScreenVars screenVars = (Sjl57ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.clntsel.clear();
		screenVars.company.clear();
		screenVars.agntbr.clear();
		screenVars.agbrdesc.clear();
		screenVars.aracde.clear();
		screenVars.aradesc.clear();
		screenVars.levelno.clear();
		screenVars.leveltyp.clear();
		screenVars.leveldesc.clear();
		screenVars.saledept.clear();
		screenVars.saledptdes.clear();
		screenVars.clntname.clear();
		screenVars.indxflg.clear();
		screenVars.select.clear();
	}
}