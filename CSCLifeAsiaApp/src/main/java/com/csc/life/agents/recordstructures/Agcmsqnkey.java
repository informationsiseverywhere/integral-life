package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:01
 * Description:
 * Copybook name: AGCMSQNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmsqnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmsqnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmsqnKey = new FixedLengthStringData(64).isAPartOf(agcmsqnFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmsqnChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmsqnKey, 0);
  	public FixedLengthStringData agcmsqnChdrnum = new FixedLengthStringData(8).isAPartOf(agcmsqnKey, 1);
  	public FixedLengthStringData agcmsqnLife = new FixedLengthStringData(2).isAPartOf(agcmsqnKey, 9);
  	public FixedLengthStringData agcmsqnCoverage = new FixedLengthStringData(2).isAPartOf(agcmsqnKey, 11);
  	public FixedLengthStringData agcmsqnRider = new FixedLengthStringData(2).isAPartOf(agcmsqnKey, 13);
  	public PackedDecimalData agcmsqnPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmsqnKey, 15);
  	public PackedDecimalData agcmsqnSeqno = new PackedDecimalData(2, 0).isAPartOf(agcmsqnKey, 18);
  	public FixedLengthStringData agcmsqnDormantFlag = new FixedLengthStringData(1).isAPartOf(agcmsqnKey, 20);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(agcmsqnKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmsqnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmsqnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}