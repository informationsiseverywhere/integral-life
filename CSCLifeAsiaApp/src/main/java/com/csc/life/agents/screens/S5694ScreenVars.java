package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5694
 * @version 1.0 generated on 30/08/09 06:49
 * @author Quipoz
 */
public class S5694ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(585);
	public FixedLengthStringData dataFields = new FixedLengthStringData(137).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData instalpcs = new FixedLengthStringData(55).isAPartOf(dataFields, 1);
	public ZonedDecimalData[] instalpc = ZDArrayPartOfStructure(11, 5, 2, instalpcs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(instalpcs, 0, FILLER_REDEFINE);
	public ZonedDecimalData instalpc01 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData instalpc02 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,5);
	public ZonedDecimalData instalpc03 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData instalpc04 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData instalpc05 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,20);
	public ZonedDecimalData instalpc06 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,25);
	public ZonedDecimalData instalpc07 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,30);
	public ZonedDecimalData instalpc08 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,35);
	public ZonedDecimalData instalpc09 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,40);
	public ZonedDecimalData instalpc10 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,45);
	public ZonedDecimalData instalpc11 = DD.instalpc.copyToZonedDecimal().isAPartOf(filler,50);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,56);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,64);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,72);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,110);
	public FixedLengthStringData toYears = new FixedLengthStringData(22).isAPartOf(dataFields, 115);
	public ZonedDecimalData[] toYear = ZDArrayPartOfStructure(11, 2, 0, toYears, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(toYears, 0, FILLER_REDEFINE);
	public ZonedDecimalData toYear01 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData toYear02 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,2);
	public ZonedDecimalData toYear03 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData toYear04 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData toYear05 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData toYear06 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData toYear07 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData toYear08 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,14);
	public ZonedDecimalData toYear09 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,16);
	public ZonedDecimalData toYear10 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData toYear11 = DD.toyear.copyToZonedDecimal().isAPartOf(filler1,20);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 137);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData instalpcsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] instalpcErr = FLSArrayPartOfStructure(11, 4, instalpcsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(44).isAPartOf(instalpcsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData instalpc01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData instalpc02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData instalpc03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData instalpc04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData instalpc05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData instalpc06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData instalpc07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData instalpc08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData instalpc09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData instalpc10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData instalpc11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData toyearsErr = new FixedLengthStringData(44).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] toyearErr = FLSArrayPartOfStructure(11, 4, toyearsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(44).isAPartOf(toyearsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData toyear01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData toyear02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData toyear03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData toyear04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData toyear05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData toyear06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData toyear07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData toyear08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData toyear09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData toyear10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData toyear11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 249);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData instalpcsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] instalpcOut = FLSArrayPartOfStructure(11, 12, instalpcsOut, 0);
	public FixedLengthStringData[][] instalpcO = FLSDArrayPartOfArrayStructure(12, 1, instalpcOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(132).isAPartOf(instalpcsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] instalpc01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] instalpc02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] instalpc03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] instalpc04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] instalpc05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] instalpc06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] instalpc07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] instalpc08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] instalpc09Out = FLSArrayPartOfStructure(12, 1, filler4, 96);
	public FixedLengthStringData[] instalpc10Out = FLSArrayPartOfStructure(12, 1, filler4, 108);
	public FixedLengthStringData[] instalpc11Out = FLSArrayPartOfStructure(12, 1, filler4, 120);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData toyearsOut = new FixedLengthStringData(132).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] toyearOut = FLSArrayPartOfStructure(11, 12, toyearsOut, 0);
	public FixedLengthStringData[][] toyearO = FLSDArrayPartOfArrayStructure(12, 1, toyearOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(132).isAPartOf(toyearsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] toyear01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] toyear02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] toyear03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] toyear04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] toyear05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] toyear06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] toyear07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] toyear08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] toyear09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] toyear10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public FixedLengthStringData[] toyear11Out = FLSArrayPartOfStructure(12, 1, filler5, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData S5694screenWritten = new LongData(0);
	public LongData S5694protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5694ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, toYear01, instalpc01, toYear02, instalpc02, toYear03, instalpc03, toYear04, instalpc04, toYear05, instalpc05, toYear06, instalpc06, toYear07, instalpc07, toYear08, instalpc08, toYear09, instalpc09, toYear10, instalpc10, toYear11, instalpc11};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, toyear01Out, instalpc01Out, toyear02Out, instalpc02Out, toyear03Out, instalpc03Out, toyear04Out, instalpc04Out, toyear05Out, instalpc05Out, toyear06Out, instalpc06Out, toyear07Out, instalpc07Out, toyear08Out, instalpc08Out, toyear09Out, instalpc09Out, toyear10Out, instalpc10Out, toyear11Out, instalpc11Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, toyear01Err, instalpc01Err, toyear02Err, instalpc02Err, toyear03Err, instalpc03Err, toyear04Err, instalpc04Err, toyear05Err, instalpc05Err, toyear06Err, instalpc06Err, toyear07Err, instalpc07Err, toyear08Err, instalpc08Err, toyear09Err, instalpc09Err, toyear10Err, instalpc10Err, toyear11Err, instalpc11Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5694screen.class;
		protectRecord = S5694protect.class;
	}

}
