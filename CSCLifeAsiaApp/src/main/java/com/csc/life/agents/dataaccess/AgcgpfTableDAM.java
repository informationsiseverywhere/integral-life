package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: AgcgpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:40
 * Class transformed from AGCGPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgcgpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 63;
	public FixedLengthStringData agcgrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agcgpfRecord = agcgrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(agcgrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(agcgrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agcgrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agcgrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agcgrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agcgrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgcgpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgcgpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgcgpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgcgpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcgpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgcgpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcgpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGCGPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"AGNTNUM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     agntnum,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		agntnum.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgcgrec() {
  		return agcgrec;
	}

	public FixedLengthStringData getAgcgpfRecord() {
  		return agcgpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgcgrec(what);
	}

	public void setAgcgrec(Object what) {
  		this.agcgrec.set(what);
	}

	public void setAgcgpfRecord(Object what) {
  		this.agcgpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agcgrec.getLength());
		result.set(agcgrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}