package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgntcypfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgsdpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.agents.dataaccess.model.Agsdpf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.agents.screens.Sjl63ScreenVars;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;


public class Pjl63 extends ScreenProgCS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl63.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL63");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sjl63ScreenVars sv = ScreenProgram.getScreenVars(Sjl63ScreenVars.class);
	protected Subprogrec subprogrec = new Subprogrec();
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData wsaaSalediv = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 0);
	private FixedLengthStringData wsaaArea = new FixedLengthStringData(2).isAPartOf(wsaaSalediv, 2);
	private FixedLengthStringData wsaaDeptcode = new FixedLengthStringData(4).isAPartOf(wsaaSalediv, 4);

	private Batckey wsaaBatckey = new Batckey();
	private AgsdpfDAO agsdpfDAO =  getApplicationContext().getBean("agsdpfDAO", AgsdpfDAO.class);
	private Agsdpf agsdpf= new Agsdpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private Agncypf agncypf;
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	List<Map<String, Object>> agntcypfList;
	private AgntcypfDAO agntcypfDAO = getApplicationContext().getBean("agntcypfDAO", AgntcypfDAO.class);
	List<Map<String, Object>> agncypfList;
	private Map<String,Descpf> descMap = new HashMap<>();
	private static final String TJL70 = "TJL70";
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";
	private static final String TJL68 = "TJL68";

	
	private String Sjl63 = "Sjl63";

	
	public Pjl63() {
		super();
		screenVars = sv;
		new ScreenModel(Sjl63, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			LOGGER.info("processBo:", e);
		}
	}
	
	@Override
	protected void initialise1000() {
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.action1.set(SPACES);
		syserrrec.subrname.set(wsaaProg);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		wsspcomn.confirmationKey.set(SPACES);
		sv.validflag.set(SPACES);
		sv.srdate.set(SPACES);
		sv.endate.set(SPACES);
		initSubfile1200();
		initialise1010();
		protect1020();
		/*To display all records of agentkey*/
		getSubfile1040();
	}
	
	protected void initSubfile1200()
	{
		scrnparams.function.set(Varcom.sclr);
		processScreen(Sjl63, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
	
	protected void initialise1010() {

		agsdpf = agsdpfDAO.getDatabyLevelno(wsspcomn.chdrCownnum.toString().trim());
		Optional<Agsdpf> isExists = Optional.ofNullable(agsdpf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agsdpf.getClientno());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agsdpf.getClientno());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agsdpf.getSalesdiv())
			{
				wsaaSalediv.set(agsdpf.getSalesdiv());
				sv.brnchcd.set(wsaaBranch);
				if(isEQ(wsaaArea,SPACES))
					sv.aracde.set("  ");	
				else	
					sv.aracde.set(wsaaArea);
				
				sv.saledept.set(wsaaDeptcode);
				loadDesc();		
			}
			if (null != agsdpf.getLevelclass())
			{
				sv.leveltype.set(agsdpf.getLevelclass());
				loadDesc();
			}
			if (null != agsdpf.getAgentclass()) {
				sv.agtype.set(agsdpf.getAgentclass());
			}
			if (null != agsdpf.getUserid()) {
				sv.userid.set(agsdpf.getUserid());
			}
			sv.levelno.set(agsdpf.getLevelno());
		}
		
		
		if (isNE(wsspcomn.chdrChdrnum, SPACE)) {
			agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim());
			Optional<Agncypf> isExist = Optional.ofNullable(agncypf);
			if (isExist.isPresent()) {
				if (null != agncypf.getAgncynum()){
					sv.agncysel.set(agncypf.getAgncynum());	
				}
				if (null != agncypf.getRegnum())
					sv.regnum.set(agncypf.getRegnum());
				if (null != agncypf.getSrdate())
					sv.srdate.set(agncypf.getSrdate());
				if (null != agncypf.getEndate())
					sv.endate.set(agncypf.getEndate());
				if (null != agncypf.getValidFlag())
					sv.validflag.set(agncypf.getValidFlag());
				if (null != agncypf.getClntnum()){
				sv.agclntsel.set(agncypf.getClntnum());
				clntpf = new Clntpf();
				clntpf = cltsioCall2700(agncypf.getClntnum());
				Optional<Clntpf> op = Optional.ofNullable(clntpf);
				if (op.isPresent()) {
					sv.agcltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
				}
			}
				
			}
		}
		else{
			sv.agclntsel.set(SPACES);
			sv.agcltname.set(SPACES);
			sv.agncysel.set(SPACES);
			sv.agncydesc.set(SPACES);
			sv.regnum.set(SPACES);
			sv.srdate.set(SPACES);
			sv.endate.set(SPACES);
			sv.validflag.set(SPACES);
		}
	}
		
	protected void getSubfile1040() {
	
		agntcypfList = agntcypfDAO.getAgntbyAgncy(sv.agncysel.toString().trim());
		try {
			if(agntcypfList.size()>0)
			{	
				for (int iy = 0; iy < agntcypfList.size(); iy++) {
					Map<String, Object> agncy = agntcypfList.get(iy);
					
						sv.agntnum.set(agncy.get("AGNTNUM"));
						sv.liscno.set(agncy.get("TLAGLICNO"));
						sv.appdate.set(agncy.get("DTEAPP"));
						sv.termdate.set(agncy.get("DTETRM"));
						sv.aclntsel.set(agncy.get("CLNTNUM"));
						sv.agnttyp.set(agncy.get("AGTYPE"));
						
						Clntpf clntpf1 = clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), sv.aclntsel.toString().trim());
						Optional<Clntpf> op = Optional.ofNullable(clntpf1);
						if (op.isPresent()) {
						sv.acltname.set(clntpf1.getGivname() + " " + clntpf1.getSurname());
						sv.gender.set(clntpf1.getCltsex());
						sv.dob.set(clntpf1.getCltdob());
						}
						

						scrnparams.function.set(Varcom.sadd);
						processScreen(Sjl63, sv);
						if (isNE(scrnparams.statuz, Varcom.oK)) {
							syserrrec.statuz.set(scrnparams.statuz);
							fatalError600();
						}
				}
			}
		
		}	catch (Exception ex) {
			LOGGER.error("Failed to get records from agncypf:", ex);
		}

	}

	
	protected void loadDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		itemMap.put(T1692, sv.brnchcd.trim());
		itemMap.put(TJL68, sv.saledept.trim());
		itemMap.put(TJL70, sv.leveltype.trim());
		
		
		descMap= descDAO.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.brnchdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.brnchdesc.set(t1692Desc.getLongdesc());
		}
		
		Descpf tjl68Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL68) ==null) {
			sv.saledptdes.set(SPACE);
		}
		else
		{
			tjl68Desc = descMap.get(TJL68);
			sv.saledptdes.set(tjl68Desc.getLongdesc());
		}
		
		Descpf tjl70Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(TJL70) ==null) {
			sv.leveldes.set(SPACE);
		}
		else
		{
			tjl70Desc = descMap.get(TJL70);
			sv.leveldes.set(tjl70Desc.getLongdesc());
		}
	}
	
	protected Clntpf cltsioCall2700(String clntNum) {
		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(),clntNum);	
	}
	

	protected void protect1020() {
		sv.clntselOut[Varcom.pr.toInt()].set("Y");
		sv.cltnameOut[Varcom.pr.toInt()].set("Y");
		sv.companyOut[Varcom.pr.toInt()].set("Y");
		sv.brnchcdOut[Varcom.pr.toInt()].set("Y");
		sv.brnchdescOut[Varcom.pr.toInt()].set("Y");
		sv.aracdeOut[Varcom.pr.toInt()].set("Y");
		sv.aradescOut[Varcom.pr.toInt()].set("Y");
		sv.levelnoOut[Varcom.pr.toInt()].set("Y");
		sv.leveltypeOut[Varcom.pr.toInt()].set("Y");
		sv.leveldesOut[Varcom.pr.toInt()].set("Y");
		sv.saledeptOut[Varcom.pr.toInt()].set("Y");
		sv.saledptdesOut[Varcom.pr.toInt()].set("Y");
		sv.agtypeOut[Varcom.pr.toInt()].set("Y");
	}
	
	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		return;
	}
	
	@Override
	protected void screenEdit2000() {
		
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		wsspcomn.edterror.set(Varcom.oK);
		screenIo2010();
		
	}
	
	
	protected void screenIo9000()
	{
		/*BEGIN*/
		processScreen(Sjl63, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
				&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}
	
	protected void screenIo2010()
	{	
		scrnparams.subfileRrn.set(1); 
		wsspcomn.edterror.set(Varcom.oK);
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SCREEN*/
		screenEditSrnch();
	}
	
	protected void screenEditSrnch() {
		
		scrnparams.function.set(Varcom.srnch);
		processScreen(Sjl63, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
		&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}


	@Override
	protected void update3000() {
		
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
            return;
        }
		
	}
	
	@Override
	protected void whereNext4000() {
		if (isEQ(scrnparams.statuz, Varcom.kill)) {
		wsspcomn.programPtr.subtract(1);
		if(isEQ(wsspcomn.flag,"1"))
			wsspcomn.chdrCownnum.set(sv.agncysel.trim());  
		else
			wsspcomn.chdrCownnum.set(sv.levelno);
		
		
			return;
        }

		if(isEQ(wsspcomn.flag,"1"))
			wsspcomn.programPtr.add(2);
		else
			wsspcomn.programPtr.add(1);
	}	

}
