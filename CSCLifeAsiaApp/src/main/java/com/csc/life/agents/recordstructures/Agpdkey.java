package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:09
 * Description:
 * Copybook name: AGPDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agpdkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agpdFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agpdKey = new FixedLengthStringData(64).isAPartOf(agpdFileKey, 0, REDEFINE);
  	public PackedDecimalData agpdEffdate = new PackedDecimalData(8, 0).isAPartOf(agpdKey, 0);
  	public FixedLengthStringData agpdAgntcoy = new FixedLengthStringData(1).isAPartOf(agpdKey, 5);
  	public FixedLengthStringData agpdAgntnum = new FixedLengthStringData(8).isAPartOf(agpdKey, 6);
  	public FixedLengthStringData agpdChdrnum = new FixedLengthStringData(8).isAPartOf(agpdKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(agpdKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agpdFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agpdFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}