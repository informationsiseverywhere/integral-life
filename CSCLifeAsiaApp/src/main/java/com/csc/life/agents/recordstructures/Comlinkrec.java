package com.csc.life.agents.recordstructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:13
 * Description:
 * Copybook name: COMLINKREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Comlinkrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData clnkallRec = new FixedLengthStringData(264);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(clnkallRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(clnkallRec, 5);
  	public FixedLengthStringData agent = new FixedLengthStringData(8).isAPartOf(clnkallRec, 9);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(clnkallRec, 17);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(clnkallRec, 18);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(clnkallRec, 26);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(clnkallRec, 28);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(clnkallRec, 30);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(clnkallRec, 32);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(clnkallRec, 35);
  	public FixedLengthStringData jlife = new FixedLengthStringData(2).isAPartOf(clnkallRec, 39);
  	public FixedLengthStringData method = new FixedLengthStringData(4).isAPartOf(clnkallRec, 41);
  	public FixedLengthStringData agentClass = new FixedLengthStringData(4).isAPartOf(clnkallRec, 45);
  	public PackedDecimalData annprem = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 49);
  	public PackedDecimalData instprem = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 58);
  	public PackedDecimalData icommtot = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 67);
  	public PackedDecimalData icommpd = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 76);
  	public PackedDecimalData icommernd = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 85);
  	public PackedDecimalData payamnt = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 94);
  	public PackedDecimalData erndamt = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 103);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 112);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(clnkallRec, 117);
  	public PackedDecimalData currto = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 119);
  	public PackedDecimalData targetPrem = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 124);
  	public PackedDecimalData seqno = new PackedDecimalData(2, 0).isAPartOf(clnkallRec, 133);
  	public FixedLengthStringData zorcode = new FixedLengthStringData(8).isAPartOf(clnkallRec, 135);
  	public FixedLengthStringData zcomcode = new FixedLengthStringData(4).isAPartOf(clnkallRec, 143);
  	public PackedDecimalData efdate = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 147);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(clnkallRec, 152);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 153);
  	/*ILIFE-3771 Code Promotion for VPMS externalization of LIFE Basic Commission Calculation Start*/
  	public PackedDecimalData cltdob =  new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 158);
  	public PackedDecimalData premCessDate =  new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 163);
  	/*ILIFE-3771 End*/
 // ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization start
  	public FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(clnkallRec, 168);
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(clnkallRec, 170);
  	public FixedLengthStringData agentype = new FixedLengthStringData(2).isAPartOf(clnkallRec, 173);
  	public PackedDecimalData anbccd = new PackedDecimalData(3, 0).isAPartOf(clnkallRec, 175);
   // ILIFE-5830 LIFE VPMS Externalization - Code promotion for Commission Payment calculation externalization end
  	public PackedDecimalData gstAmount = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 177);
  	//ILIFE-7965
  	public PackedDecimalData neticommtot = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 186);
  	public PackedDecimalData neterndamt = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 195);
	public PackedDecimalData initcommpay = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 204); // ILIFE-8392
	public FixedLengthStringData reinsCode = new FixedLengthStringData(8).isAPartOf(clnkallRec, 213);
	public PackedDecimalData reinsCommprem = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 221);
	public PackedDecimalData crrcd = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 230);
	public PackedDecimalData covrcd = new PackedDecimalData(8, 0).isAPartOf(clnkallRec, 238);
	public PackedDecimalData commprem = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 246);
	public PackedDecimalData rcommtot = new PackedDecimalData(17, 2).isAPartOf(clnkallRec, 255);


	public void initialize() {
		COBOLFunctions.initialize(clnkallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		clnkallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}