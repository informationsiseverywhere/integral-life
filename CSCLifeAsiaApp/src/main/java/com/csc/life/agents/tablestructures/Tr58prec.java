package com.csc.life.agents.tablestructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr58prec extends ExternalData {

	public FixedLengthStringData tr58pRec = new FixedLengthStringData(500); 
	public ZonedDecimalData prcent = new ZonedDecimalData(5,2).isAPartOf(tr58pRec,0);
	public FixedLengthStringData filler = new FixedLengthStringData(495).isAPartOf(tr58pRec, 5, FILLER);


    public void initialize() {
		COBOLFunctions.initialize(tr58pRec);
	}	


	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr58pRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}

}