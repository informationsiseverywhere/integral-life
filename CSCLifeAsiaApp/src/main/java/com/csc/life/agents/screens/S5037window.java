package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for WINDOW
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5037window extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 15, 28, 37}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5037ScreenVars sv = (S5037ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5037windowWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5037ScreenVars screenVars = (S5037ScreenVars)pv;
		screenVars.bankkey.setClassString("");
	}

/**
 * Clear all the variables in S5037window
 */
	public static void clear(VarModel pv) {
		S5037ScreenVars screenVars = (S5037ScreenVars) pv;
		screenVars.bankkey.clear();
	}
}
