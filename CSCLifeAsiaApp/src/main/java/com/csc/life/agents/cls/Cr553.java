/*
 * File: Cr553.java
 * Date: 30 August 2009 2:59:21
 * Author: $Id$
 * 
 * Class transformed from CR553.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.cls;

import com.quipoz.COBOLFramework.printing.PrintManager;
import com.quipoz.COBOLFramework.printing.PrintConfiguration;
import com.quipoz.COBOLFramework.dataqueue.DataQueue;
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.util.ResidentUtilityModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.quipoz.COBOLFramework.query.Opnqryf;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.COBOLFramework.common.exception.*;
import com.quipoz.COBOLFramework.util.*;
import com.quipoz.framework.util.log.QPLogger;
import com.quipoz.framework.exception.TransferProgramException;
import static com.csc.smart400framework.batch.cls.BatchCLFunctions.*;




import com.csc.life.agents.batchprograms.Br553;
import com.csc.smart400framework.parent.SMARTCodeModel;

public class Cr553 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData statuz = new FixedLengthStringData(4);
	private FixedLengthStringData bsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData bprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData buparec = new FixedLengthStringData(1024);
	private FixedLengthStringData file = new FixedLengthStringData(10).init("AGP1PF");

	public Cr553() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		buparec = convertAndSetParam(buparec, parmArray, 4);
		bprdrec = convertAndSetParam(bprdrec, parmArray, 3);
		bsprrec = convertAndSetParam(bsprrec, parmArray, 2);
		bsscrec = convertAndSetParam(bsscrec, parmArray, 1);
		statuz = convertAndSetParam(statuz, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				try {
					switch (qState) {
					case QS_START: 
						FileCode.checkExistence(file);
					default:{
						qState = QS_END;
					}
					}
				}
				catch (ExtMsgException ex1){
					if (ex1.messageMatches("CPF9801")) {
						FileCode.createPhysicalFile(file, 147, "Agency Compensation Temporary Extract File", null, null, null, null, null, null);
					}
					else {
						throw ex1;
					}
				}
				FileCode.clearMember(file);
				callProgram(Br553.class, new Object[] {statuz, bsscrec, bsprrec, bprdrec, buparec});
				copyToPcDocument(file, "LIFEFLR/PCEXTR", "AGP1PC" + "." + "TXT", true);
				 returnVar: ;
				return ;
				/* UNREACHABLE CODE
				 error: ;
				appVars.sendMessageToQueue("Unexpected errors occurred", "*");
				statuz.set("BOMB");
				qState = returnVar;
				break;
				 */
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
