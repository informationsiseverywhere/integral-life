package com.csc.life.agents.dataaccess.model;

import java.util.Date;
import java.math.BigDecimal;

public class Zldbpf {
	private long uniqueNumber;
	private String agntcoy;
	private String agntnum;
	private Integer batcactyr;
	private Integer batcactmn;
	private String agtype;
	private Integer prcnt;
	private String genlcur;
	private BigDecimal blprem;
	private Integer premst;
	private Integer bpayty;
	private BigDecimal prmamt;
	private BigDecimal bonusamt;
	private BigDecimal bnpaid;	
	private BigDecimal netpre;	
	private String procflg;	
	private Integer prcdate;
	private String usrprf;	
	private String jobnm;	
	private Date datime;
	
	
	
/*public Zldbpf(String agntcoy,String agntnum,Integer batcactyr,Integer batcactmn,BigDecimal blprem,Integer bpayty,BigDecimal netpre,String procflg) {
		
		this.agntcoy = agntcoy;
		this.agntnum = agntnum;
		this.batcactyr = batcactyr;
		this.batcactmn = batcactmn;
		this.blprem = blprem;
		this.bpayty = bpayty;
		this.netpre = netpre;
		this.procflg = procflg;
		this.prmamt = prmamt;
	}*/

	public Zldbpf() {
		
	// TODO Auto-generated constructor stub
}

	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	
	public Integer getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Integer batcactyr) {
		this.batcactyr = batcactyr;
	}
	
	public Integer getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Integer batcactmn) {
		this.batcactmn = batcactmn;
	}
	
	public String getAgtype() {
		return agtype;
	}
	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}	
	
	public Integer getPrcnt() {
		return prcnt;
	}
	public void setPrcnt(Integer prcnt) {
		this.prcnt = prcnt;
	}
	
	public String getGenlcur() {
		return genlcur;
	}
	public void setGenlcur(String genlcur) {
		this.genlcur = genlcur;
	}
	
	public BigDecimal getBlprem() {
		return blprem;
	}
	public void setBlprem(BigDecimal blprem) {
		this.blprem = blprem;
	}
	
	public Integer getPremst() {
		return premst;
	}
	public void setPremst(Integer premst) {
		this.premst = premst;
	}
	
	public Integer getBpayty() {
		return bpayty;
	}
	public void setBpayty(Integer bpayty) {
		this.bpayty = bpayty;
	}
	
	public BigDecimal getPrmamt() {
		return prmamt;
	}
	public void setPrmamt(BigDecimal prmamt) {
		this.prmamt = prmamt;
	}
	
	public BigDecimal getBonusamt() {
		return bonusamt;
	}
	public void setBonusamt(BigDecimal bonusamt) {
		this.bonusamt = bonusamt;
	}
	
	public BigDecimal getBnpaid() {
		return bnpaid;
	}
	public void setBnpaid(BigDecimal bnpaid) {
		this.bnpaid = bnpaid;
	}
	
	public BigDecimal getNetpre() {
		return netpre;
	}
	public void setNetpre(BigDecimal netpre) {
		this.netpre = netpre;
	}
	
	public String getProcflg() {
		return procflg;
	}
	public void setProcflg(String procflg) {
		this.procflg = procflg;
	}
	
	public Integer getPrcdate() {
		return prcdate;
	}
	public void setPrcdate(Integer prcdate) {
		this.prcdate = prcdate;
	}
	
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	
}
