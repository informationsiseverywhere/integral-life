package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 02:59:40
 * Description:
 * Copybook name: ACMVAGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Acmvagpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData acmvagpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData acmvagpKey = new FixedLengthStringData(64).isAPartOf(acmvagpFileKey, 0, REDEFINE);
  	public FixedLengthStringData acmvagpBatccoy = new FixedLengthStringData(1).isAPartOf(acmvagpKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(63).isAPartOf(acmvagpKey, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(acmvagpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		acmvagpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}