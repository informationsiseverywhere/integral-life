package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:42
 * @author Quipoz
 */
public class S5564screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5564ScreenVars sv = (S5564ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5564screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5564ScreenVars screenVars = (S5564ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.freq01.setClassString("");
		screenVars.itmkey01.setClassString("");
		screenVars.freq02.setClassString("");
		screenVars.itmkey02.setClassString("");
		screenVars.freq03.setClassString("");
		screenVars.itmkey03.setClassString("");
		screenVars.freq04.setClassString("");
		screenVars.itmkey04.setClassString("");
		screenVars.freq05.setClassString("");
		screenVars.itmkey05.setClassString("");
		screenVars.freq06.setClassString("");
		screenVars.itmkey06.setClassString("");
		screenVars.freq07.setClassString("");
		screenVars.itmkey07.setClassString("");
		screenVars.freq08.setClassString("");
		screenVars.itmkey08.setClassString("");
		screenVars.freq09.setClassString("");
		screenVars.itmkey09.setClassString("");
		screenVars.freq10.setClassString("");
		screenVars.itmkey10.setClassString("");
		screenVars.freq11.setClassString("");
		screenVars.itmkey11.setClassString("");
		screenVars.freq12.setClassString("");
		screenVars.itmkey12.setClassString("");
	}

/**
 * Clear all the variables in S5564screen
 */
	public static void clear(VarModel pv) {
		S5564ScreenVars screenVars = (S5564ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.freq01.clear();
		screenVars.itmkey01.clear();
		screenVars.freq02.clear();
		screenVars.itmkey02.clear();
		screenVars.freq03.clear();
		screenVars.itmkey03.clear();
		screenVars.freq04.clear();
		screenVars.itmkey04.clear();
		screenVars.freq05.clear();
		screenVars.itmkey05.clear();
		screenVars.freq06.clear();
		screenVars.itmkey06.clear();
		screenVars.freq07.clear();
		screenVars.itmkey07.clear();
		screenVars.freq08.clear();
		screenVars.itmkey08.clear();
		screenVars.freq09.clear();
		screenVars.itmkey09.clear();
		screenVars.freq10.clear();
		screenVars.itmkey10.clear();
		screenVars.freq11.clear();
		screenVars.itmkey11.clear();
		screenVars.freq12.clear();
		screenVars.itmkey12.clear();
	}
}
