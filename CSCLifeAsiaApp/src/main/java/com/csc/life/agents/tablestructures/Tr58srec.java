package com.csc.life.agents.tablestructures;

/**
 * File: T5998rec.java
 * Date: 25 june 2013 
 *
 * Copyright (2013) CSC Asia, all rights reserved.
 */
import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.quipoz.COBOLFramework.COBOLFunctions;

public class Tr58srec extends ExternalData {

  	// public FixedLengthStringData T5998Rec = new FixedLengthStringData(521); 
public FixedLengthStringData tr58sRec = new FixedLengthStringData(186); 

public FixedLengthStringData comtots = new FixedLengthStringData(136).isAPartOf(tr58sRec,0);
public ZonedDecimalData[] comtot = ZDArrayPartOfStructure(8,17,2,comtots, 0);
public ZonedDecimalData comtot01 = new ZonedDecimalData(17,2).isAPartOf(comtots, 0);
public ZonedDecimalData comtot02 = new ZonedDecimalData(17,2).isAPartOf(comtots, 17);
public ZonedDecimalData comtot03 = new ZonedDecimalData(17,2).isAPartOf(comtots, 34);
public ZonedDecimalData comtot04 = new ZonedDecimalData(17,2).isAPartOf(comtots, 51);
public ZonedDecimalData comtot05 = new ZonedDecimalData(17,2).isAPartOf(comtots, 68);
public ZonedDecimalData comtot06 = new ZonedDecimalData(17,2).isAPartOf(comtots, 85);
public ZonedDecimalData comtot07 = new ZonedDecimalData(17,2).isAPartOf(comtots, 102);
public ZonedDecimalData comtot08 = new ZonedDecimalData(17,2).isAPartOf(comtots, 119);

public FixedLengthStringData prcents = new FixedLengthStringData(40).isAPartOf(tr58sRec,136);
public ZonedDecimalData[] prcent = ZDArrayPartOfStructure(8,5,2,prcents, 0);
public ZonedDecimalData prcent01 = new ZonedDecimalData(5,2).isAPartOf(prcents, 0);
public ZonedDecimalData prcent02 = new ZonedDecimalData(5,2).isAPartOf(prcents, 5);
public ZonedDecimalData prcent03 = new ZonedDecimalData(5,2).isAPartOf(prcents, 10);
public ZonedDecimalData prcent04 = new ZonedDecimalData(5,2).isAPartOf(prcents, 15);
public ZonedDecimalData prcent05 = new ZonedDecimalData(5,2).isAPartOf(prcents, 20);
public ZonedDecimalData prcent06 = new ZonedDecimalData(5,2).isAPartOf(prcents, 25);
public ZonedDecimalData prcent07 = new ZonedDecimalData(5,2).isAPartOf(prcents, 30);
public ZonedDecimalData prcent08 = new ZonedDecimalData(5,2).isAPartOf(prcents, 35);

public FixedLengthStringData znadjpercs = new FixedLengthStringData(10).isAPartOf(tr58sRec,176);
public ZonedDecimalData[] znadjperc = ZDArrayPartOfStructure(2,5,2,znadjpercs, 0);
public ZonedDecimalData znadjperc01 = new ZonedDecimalData(5,2).isAPartOf(znadjpercs, 0);
public ZonedDecimalData znadjperc02 = new ZonedDecimalData(5,2).isAPartOf(znadjpercs, 5);
private static final Logger LOGGER = LoggerFactory.getLogger(Tr58srec.class);//IJTI-318
		
    public String[] fieldnamelist={"companys","items","itmfrms","itmtos","longdescs","comtot01s","comtot02s","comtot03s","comtot04s","comtot05s","comtot06s","comtot07s","comtot08s","prcent01s","prcent02s","prcent03s","prcent04s","prcent05s","prcent06s","prcent07s","prcent08s","znadjperc01s","znadjperc02s"};
		
	public Tr58srec(){
		LOGGER.info("test- DE:Tr58srec constuctor called");//IJTI-318
	}
	
	public void initialize() {
		COBOLFunctions.initialize(tr58sRec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr58sRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	public String[] getFieldNames(){
		return Arrays.copyOf(fieldnamelist, fieldnamelist.length);//IJTI-316
	}
}
