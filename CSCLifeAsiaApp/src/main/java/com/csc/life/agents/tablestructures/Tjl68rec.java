package com.csc.life.agents.tablestructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * @author tsaxena3
 *
 */
public class Tjl68rec extends ExternalData {
	
		private static final long serialVersionUID = 1L; 
		public FixedLengthStringData tjl68Rec = new FixedLengthStringData(65);
		public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(tjl68Rec, 0);
		public FixedLengthStringData agbrdesc = DD.agbrdesc.copy().isAPartOf(tjl68Rec, 2);
	    public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(tjl68Rec, 32);
	    public FixedLengthStringData aradesc = DD.aradesc.copy().isAPartOf(tjl68Rec, 35);
	
	public void initialize() {
		COBOLFunctions.initialize(tjl68Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl68Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}


}
