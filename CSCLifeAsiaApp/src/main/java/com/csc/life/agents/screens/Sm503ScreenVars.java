package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM503
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm503ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1409);
	public FixedLengthStringData dataFields = new FixedLengthStringData(705).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData agntbr = DD.agntbr.copy().isAPartOf(dataFields,4);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData agtname = DD.agtname.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData agtype = DD.agtype.copy().isAPartOf(dataFields,61);
	public ZonedDecimalData countpol = DD.countpol.copyToZonedDecimal().isAPartOf(dataFields,63);
	public FixedLengthStringData descrips = new FixedLengthStringData(60).isAPartOf(dataFields, 69);
	public FixedLengthStringData[] descrip = FLSArrayPartOfStructure(2, 30, descrips, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(descrips, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01 = DD.descrip.copy().isAPartOf(filler,0);
	public FixedLengthStringData descrip02 = DD.descrip.copy().isAPartOf(filler,30);
	public ZonedDecimalData dteapp = DD.dteapp.copyToZonedDecimal().isAPartOf(dataFields,129);
	public FixedLengthStringData mldirpps = new FixedLengthStringData(170).isAPartOf(dataFields, 137);
	public ZonedDecimalData[] mldirpp = ZDArrayPartOfStructure(10, 17, 2, mldirpps, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(170).isAPartOf(mldirpps, 0, FILLER_REDEFINE);
	public ZonedDecimalData mldirpp01 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData mldirpp02 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,17);
	public ZonedDecimalData mldirpp03 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,34);
	public ZonedDecimalData mldirpp04 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,51);
	public ZonedDecimalData mldirpp05 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,68);
	public ZonedDecimalData mldirpp06 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,85);
	public ZonedDecimalData mldirpp07 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,102);
	public ZonedDecimalData mldirpp08 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,119);
	public ZonedDecimalData mldirpp09 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,136);
	public ZonedDecimalData mldirpp10 = DD.mldirpp.copyToZonedDecimal().isAPartOf(filler1,153);
	public FixedLengthStringData mlgrppps = new FixedLengthStringData(170).isAPartOf(dataFields, 307);
	public ZonedDecimalData[] mlgrppp = ZDArrayPartOfStructure(10, 17, 2, mlgrppps, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(170).isAPartOf(mlgrppps, 0, FILLER_REDEFINE);
	public ZonedDecimalData mlgrppp01 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData mlgrppp02 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,17);
	public ZonedDecimalData mlgrppp03 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,34);
	public ZonedDecimalData mlgrppp04 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,51);
	public ZonedDecimalData mlgrppp05 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,68);
	public ZonedDecimalData mlgrppp06 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,85);
	public ZonedDecimalData mlgrppp07 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,102);
	public ZonedDecimalData mlgrppp08 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,119);
	public ZonedDecimalData mlgrppp09 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,136);
	public ZonedDecimalData mlgrppp10 = DD.mlgrppp.copyToZonedDecimal().isAPartOf(filler2,153);
	public FixedLengthStringData mlperpps = new FixedLengthStringData(170).isAPartOf(dataFields, 477);
	public ZonedDecimalData[] mlperpp = ZDArrayPartOfStructure(10, 17, 2, mlperpps, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(170).isAPartOf(mlperpps, 0, FILLER_REDEFINE);
	public ZonedDecimalData mlperpp01 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData mlperpp02 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,17);
	public ZonedDecimalData mlperpp03 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,34);
	public ZonedDecimalData mlperpp04 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,51);
	public ZonedDecimalData mlperpp05 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,68);
	public ZonedDecimalData mlperpp06 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,85);
	public ZonedDecimalData mlperpp07 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,102);
	public ZonedDecimalData mlperpp08 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,119);
	public ZonedDecimalData mlperpp09 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,136);
	public ZonedDecimalData mlperpp10 = DD.mlperpp.copyToZonedDecimal().isAPartOf(filler3,153);
	public ZonedDecimalData mlytdgrpca = DD.mlytdgrpca.copyToZonedDecimal().isAPartOf(dataFields,647);
	public ZonedDecimalData mlytdgrpfn = DD.mlytdgrpfn.copyToZonedDecimal().isAPartOf(dataFields,661);
	public ZonedDecimalData mlytdperca = DD.mlytdperca.copyToZonedDecimal().isAPartOf(dataFields,675);
	public ZonedDecimalData mlytdperfn = DD.mlytdperfn.copyToZonedDecimal().isAPartOf(dataFields,689);
	public ZonedDecimalData mnth = DD.mnth.copyToZonedDecimal().isAPartOf(dataFields,703);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 705);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntbrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agtnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData agtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData countpolErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData descripsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] descripErr = FLSArrayPartOfStructure(2, 4, descripsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(descripsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData descrip01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData descrip02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData dteappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData mldirppsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] mldirppErr = FLSArrayPartOfStructure(10, 4, mldirppsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(mldirppsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mldirpp01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData mldirpp02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData mldirpp03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData mldirpp04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData mldirpp05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData mldirpp06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData mldirpp07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData mldirpp08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData mldirpp09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData mldirpp10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData mlgrpppsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData[] mlgrpppErr = FLSArrayPartOfStructure(10, 4, mlgrpppsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(mlgrpppsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlgrppp01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData mlgrppp02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData mlgrppp03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData mlgrppp04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData mlgrppp05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData mlgrppp06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData mlgrppp07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData mlgrppp08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData mlgrppp09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData mlgrppp10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	public FixedLengthStringData mlperppsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData[] mlperppErr = FLSArrayPartOfStructure(10, 4, mlperppsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(mlperppsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mlperpp01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData mlperpp02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData mlperpp03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData mlperpp04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData mlperpp05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData mlperpp06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData mlperpp07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData mlperpp08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData mlperpp09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData mlperpp10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData mlytdgrpcaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData mlytdgrpfnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData mlytdpercaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData mlytdperfnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData mnthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 881);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntbrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agtnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] agtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] countpolOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData descripsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(2, 12, descripsOut, 0);
	public FixedLengthStringData[][] descripO = FLSDArrayPartOfArrayStructure(12, 1, descripOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(descripsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] descrip01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] descrip02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] dteappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData mldirppsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] mldirppOut = FLSArrayPartOfStructure(10, 12, mldirppsOut, 0);
	public FixedLengthStringData[][] mldirppO = FLSDArrayPartOfArrayStructure(12, 1, mldirppOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(120).isAPartOf(mldirppsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mldirpp01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] mldirpp02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] mldirpp03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] mldirpp04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] mldirpp05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] mldirpp06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] mldirpp07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] mldirpp08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] mldirpp09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] mldirpp10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	public FixedLengthStringData mlgrpppsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 228);
	public FixedLengthStringData[] mlgrpppOut = FLSArrayPartOfStructure(10, 12, mlgrpppsOut, 0);
	public FixedLengthStringData[][] mlgrpppO = FLSDArrayPartOfArrayStructure(12, 1, mlgrpppOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(mlgrpppsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlgrppp01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] mlgrppp02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] mlgrppp03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] mlgrppp04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] mlgrppp05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] mlgrppp06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] mlgrppp07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] mlgrppp08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] mlgrppp09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] mlgrppp10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	public FixedLengthStringData mlperppsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 348);
	public FixedLengthStringData[] mlperppOut = FLSArrayPartOfStructure(10, 12, mlperppsOut, 0);
	public FixedLengthStringData[][] mlperppO = FLSDArrayPartOfArrayStructure(12, 1, mlperppOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(mlperppsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mlperpp01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] mlperpp02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] mlperpp03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] mlperpp04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] mlperpp05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] mlperpp06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] mlperpp07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] mlperpp08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] mlperpp09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] mlperpp10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	public FixedLengthStringData[] mlytdgrpcaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] mlytdgrpfnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] mlytdpercaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] mlytdperfnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] mnthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dteappDisp = new FixedLengthStringData(10);

	public LongData Sm503screenWritten = new LongData(0);
	public LongData Sm503protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm503ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mnthOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acctyrOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {agntnum, agtname, dteapp, agtype, descrip01, agntbr, descrip02, mnth, acctyr, mlgrppp01, mlgrppp02, mldirpp02, mlgrppp03, mldirpp03, mlgrppp04, mldirpp04, mlgrppp05, mldirpp05, mlgrppp06, mldirpp06, mlgrppp07, mldirpp07, mlgrppp08, mldirpp08, mlgrppp09, mldirpp09, mlgrppp10, mldirpp10, mlytdperfn, countpol, mldirpp01, mlperpp01, mlperpp02, mlperpp03, mlperpp04, mlperpp05, mlperpp06, mlperpp07, mlperpp08, mlperpp09, mlperpp10, mlytdperca, mlytdgrpca, mlytdgrpfn};
		screenOutFields = new BaseData[][] {agntnumOut, agtnameOut, dteappOut, agtypeOut, descrip01Out, agntbrOut, descrip02Out, mnthOut, acctyrOut, mlgrppp01Out, mlgrppp02Out, mldirpp02Out, mlgrppp03Out, mldirpp03Out, mlgrppp04Out, mldirpp04Out, mlgrppp05Out, mldirpp05Out, mlgrppp06Out, mldirpp06Out, mlgrppp07Out, mldirpp07Out, mlgrppp08Out, mldirpp08Out, mlgrppp09Out, mldirpp09Out, mlgrppp10Out, mldirpp10Out, mlytdperfnOut, countpolOut, mldirpp01Out, mlperpp01Out, mlperpp02Out, mlperpp03Out, mlperpp04Out, mlperpp05Out, mlperpp06Out, mlperpp07Out, mlperpp08Out, mlperpp09Out, mlperpp10Out, mlytdpercaOut, mlytdgrpcaOut, mlytdgrpfnOut};
		screenErrFields = new BaseData[] {agntnumErr, agtnameErr, dteappErr, agtypeErr, descrip01Err, agntbrErr, descrip02Err, mnthErr, acctyrErr, mlgrppp01Err, mlgrppp02Err, mldirpp02Err, mlgrppp03Err, mldirpp03Err, mlgrppp04Err, mldirpp04Err, mlgrppp05Err, mldirpp05Err, mlgrppp06Err, mldirpp06Err, mlgrppp07Err, mldirpp07Err, mlgrppp08Err, mldirpp08Err, mlgrppp09Err, mldirpp09Err, mlgrppp10Err, mldirpp10Err, mlytdperfnErr, countpolErr, mldirpp01Err, mlperpp01Err, mlperpp02Err, mlperpp03Err, mlperpp04Err, mlperpp05Err, mlperpp06Err, mlperpp07Err, mlperpp08Err, mlperpp09Err, mlperpp10Err, mlytdpercaErr, mlytdgrpcaErr, mlytdgrpfnErr};
		screenDateFields = new BaseData[] {dteapp};
		screenDateErrFields = new BaseData[] {dteappErr};
		screenDateDispFields = new BaseData[] {dteappDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm503screen.class;
		protectRecord = Sm503protect.class;
	}

}
