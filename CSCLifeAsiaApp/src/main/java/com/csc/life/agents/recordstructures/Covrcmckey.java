package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:22
 * Description:
 * Copybook name: COVRCMCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrcmckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrcmcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrcmcKey = new FixedLengthStringData(64).isAPartOf(covrcmcFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrcmcChdrcoy = new FixedLengthStringData(1).isAPartOf(covrcmcKey, 0);
  	public FixedLengthStringData covrcmcChdrnum = new FixedLengthStringData(8).isAPartOf(covrcmcKey, 1);
  	public FixedLengthStringData covrcmcLife = new FixedLengthStringData(2).isAPartOf(covrcmcKey, 9);
  	public FixedLengthStringData covrcmcCoverage = new FixedLengthStringData(2).isAPartOf(covrcmcKey, 11);
  	public FixedLengthStringData covrcmcRider = new FixedLengthStringData(2).isAPartOf(covrcmcKey, 13);
  	public PackedDecimalData covrcmcPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrcmcKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrcmcKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrcmcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrcmcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}