/*
 * File: P5036.java
 * Date: 29 August 2009 23:59:00
 * Author: Quipoz Limited
 * 
 * Class transformed from P5036.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.screens.S5036ScreenVars;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    TIED AGENT DETAIL SCREEN.
*                    =========================
* Initialise
* ----------
*
*  Read  AGLF  (RETRV)  in   order  to  obtain  the  life  agent
*  information. Load the screen from the record read.
*
*  If in enquiry mode, protect all fields.
*
* Validation
* ----------
*
*  If in enquiry mode, skip all field validation.
*
*  Validate the screen  according  to  the  rules defined by the
*  field help. For all fields which have descriptions, look them
*  up again if  the  field  has been changed (CHG indicator from
*  DDS keyword).
*
*  If 'CALC' was pressed, re-display the screen.
*
* Updating
* --------
*
*  Store the information entered in the I/O module (AGLF).
*
* Next Program
* ------------
*
*  Add one to the pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5036 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5036");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String e315 = "E315";
	private String e686 = "E686";
	private String f199 = "E199";
	private String g239 = "G239";
	private String g709 = "G709";
	private String g797 = "G797";
	private String e133 = "E133";
		/* TABLES */
	private String t3692 = "T3692";
	private String t5597 = "T5597";
	private String t5598 = "T5598";
	private String t5622 = "T5622";
	private String t5645 = "T5645";
		/* FORMATS */
	private String aglfrec = "AGLFREC";
	private String acmvrec = "ACMVREC";
		/*SUB ACCOUNT MOVEMENT*/
	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5036ScreenVars sv = ScreenProgram.getScreenVars( S5036ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		read1120, 
		exit1190, 
		preExit, 
		exit2090, 
		exit2190
	}

	public P5036() {
		super();
		screenVars = sv;
		new ScreenModel("S5036", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		retrvAglflnb1020();
		clientDets1040();
		loadScreen1050();
		agentType1060();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.dteapp.set(varcom.vrcmMaxDate);
		sv.fixprc.set(ZERO);
		sv.intcrd.set(ZERO);
		sv.sprprc.set(ZERO);
		sv.taxalw.set(ZERO);
		sv.tcolprct.set(ZERO);
		sv.tcolmax.set(ZERO);
		sv.tcolbal.set(ZERO);
	}

protected void retrvAglflnb1020()
	{
		aglfIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*READR-AGENT-FILE*/
		agntlagIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)
		&& isNE(agntlagIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
	}

protected void clientDets1040()
	{
		sv.clntsel.set(agntlagIO.getClntnum());
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntnum(agntlagIO.getClntnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.cltname.set(wsspcomn.longconfname);
	}

protected void loadScreen1050()
	{
		sv.agnum.set(aglfIO.getAgntnum());
		sv.dteapp.set(aglfIO.getDteapp());
		sv.bmaflg.set(aglfIO.getBmaflg());
		sv.intcrd.set(aglfIO.getIntcrd());
		sv.fixprc.set(aglfIO.getFixprc());
		sv.taxmeth.set(aglfIO.getTaxmeth());
		sv.taxcde.set(aglfIO.getTaxcde());
		sv.taxalw.set(aglfIO.getTaxalw());
		sv.irdno.set(aglfIO.getIrdno());
		sv.sprschm.set(aglfIO.getSprschm());
		sv.sprprc.set(aglfIO.getSprprc());
		sv.houseLoan.set(aglfIO.getHouseLoan());
		sv.carLoan.set(aglfIO.getCarLoan());
		sv.computerLoan.set(aglfIO.getComputerLoan());
		sv.officeRent.set(aglfIO.getOfficeRent());
		sv.otherLoans.set(aglfIO.getOtherLoans());
		sv.tcolprct.set(aglfIO.getTcolprct());
		sv.tcolmax.set(aglfIO.getTcolmax());
		sv.agccqind.set(aglfIO.getAgccqind());
		getOutsCollateral1100();
	}

protected void agentType1060()
	{
		sv.agtype.set(agntlagIO.getAgtype());
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntlagIO.getAgtype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.agtydesc.set(descIO.getLongdesc());
	}

protected void getOutsCollateral1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1110();
				}
				case read1120: {
					read1120();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("P5036");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			initialize(t5645rec.t5645Rec);
			goTo(GotoLabel.exit1190);
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setAcmvrecKeyData(SPACES);
		acmvsacIO.setRldgcoy(wsspcomn.company);
		acmvsacIO.setSacscode(t5645rec.sacscode01);
		acmvsacIO.setRldgacct(aglfIO.getAgntnum());
		acmvsacIO.setSacstyp(t5645rec.sacstype01);
		acmvsacIO.setOrigcurr(SPACES);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		acmvsacIO.setFitKeysSearch("RLDGCOY", "SACSCODE", "RLDGACCT", "SACSTYP");
		acmvsacIO.setFormat(acmvrec);
	}

protected void read1120()
	{
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		|| isNE(acmvsacIO.getRldgcoy(),wsspcomn.company)
		|| isNE(acmvsacIO.getSacscode(),t5645rec.sacscode01)
		|| isNE(acmvsacIO.getRldgacct(),aglfIO.getAgntnum())
		|| isNE(acmvsacIO.getSacstyp(),t5645rec.sacstype01)) {
			goTo(GotoLabel.exit1190);
		}
		if (isEQ(acmvsacIO.getFrcdate(),varcom.vrcmMaxDate)) {
			sv.tcolbal.add(acmvsacIO.getAcctamt());
		}
		acmvsacIO.setFunction(varcom.nextr);
		goTo(GotoLabel.read1120);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*ENQUIRY-CHECK*/
		if (isEQ(wsspcomn.flag,"I")
		|| isEQ(wsspcomn.flag,"T")
		|| isEQ(wsspcomn.flag,"R")) {
			goTo(GotoLabel.exit2090);
		}
		validateAllFields2100();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*REDISPLAY-ON-CALC*/
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateAllFields2100()
	{
		try {
			validateAllFields2110();
		}
		catch (GOTOException e){
		}
	}

protected void validateAllFields2110()
	{
		if (isNE(sv.bmaflg,"R")
		&& isNE(sv.bmaflg,SPACES)
		&& isNE(sv.bmaflg,"Q")) {
			sv.bmaflgErr.set(g709);
		}
		if (isEQ(sv.taxmeth,SPACES)) {
			sv.taxmeth.set(SPACES);
		}
		else {
			if (isEQ(sv.taxmethOut[varcom.chg.toInt()],"Y")) {
				valTaxmeth2120();
			}
		}
		if (isEQ(sv.taxmeth,SPACES)) {
			if (isEQ(sv.taxcde,SPACES)) {
				sv.taxcde.set(SPACES);
			}
			else {
				sv.taxmethErr.set(g239);
				sv.taxcdeErr.set(g239);
			}
		}
		else {
			if (isEQ(sv.taxmethOut[varcom.chg.toInt()],"Y")) {
				valTaxcde2130();
			}
		}
		if (isEQ(sv.taxmeth,SPACES)) {
			if (isEQ(sv.taxalw,ZERO)) {
				sv.taxalw.set(ZERO);
			}
			else {
				sv.taxmethErr.set(g239);
				sv.taxalwErr.set(g239);
			}
		}
		if (isEQ(sv.taxmeth,SPACES)) {
			if (isEQ(sv.irdno,SPACES)) {
				sv.irdno.set(SPACES);
			}
			else {
				sv.taxmethErr.set(g239);
				sv.irdnoErr.set(g239);
			}
		}
		if (isEQ(sv.sprschm,SPACES)) {
			sv.sprschm.set(SPACES);
		}
		else {
			if (isEQ(sv.sprschmOut[varcom.chg.toInt()],"Y")) {
				valSprschm2140();
			}
		}
		if (isEQ(sv.sprschm,SPACES)) {
			if (isEQ(sv.sprprc,ZERO)) {
				sv.sprprc.set(ZERO);
			}
			else {
				sv.sprschmErr.set(e686);
				sv.sprprcErr.set(e686);
			}
		}
		valAgccqind2150();
		goTo(GotoLabel.exit2190);
	}

protected void valTaxmeth2120()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(t5598);
		itemIO.setItemitem(sv.taxmeth);
		itemioCall2200();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.taxmethErr.set(f199);
		}
		if (isNE(itemIO.getValidflag(),"1")) {
			sv.taxmethErr.set(f199);
		}
	}

protected void valTaxcde2130()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(t5622);
		itemIO.setItemitem(sv.taxcde);
		itemioCall2200();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.taxcdeErr.set(f199);
		}
		if (isNE(itemIO.getValidflag(),"1")) {
			sv.taxcdeErr.set(f199);
		}
	}

protected void valSprschm2140()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItemtabl(t5597);
		itemIO.setItemitem(sv.sprschm);
		itemioCall2200();
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.sprschmErr.set(f199);
		}
		if (isNE(itemIO.getValidflag(),"1")) {
			sv.sprschmErr.set(f199);
		}
	}

protected void valAgccqind2150()
	{
		if (isNE(sv.agccqind,"Y")
		&& isNE(sv.agccqind,"N")) {
			sv.agccqindErr.set(e315);
		}
		if (isEQ(sv.agccqind,"Y")
		&& isEQ(aglfIO.getPayclt(),SPACES)) {
			sv.agccqindErr.set(e133);
		}
		if (isNE(sv.agccqind,"Y")
		&& isEQ(aglfIO.getPayclt(),agntlagIO.getClntnum())) {
			sv.agccqindErr.set(g797);
		}
	}

protected void itemioCall2200()
	{
		/*ITEM-TABLE*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void update3000()
	{
		keepsAglf3010();
	}

protected void keepsAglf3010()
	{
		aglfIO.setAgntcoy(wsspcomn.company);
		aglfIO.setBmaflg(sv.bmaflg);
		aglfIO.setIntcrd(sv.intcrd);
		aglfIO.setFixprc(sv.fixprc);
		aglfIO.setTaxmeth(sv.taxmeth);
		aglfIO.setTaxcde(sv.taxcde);
		aglfIO.setTaxalw(sv.taxalw);
		aglfIO.setIrdno(sv.irdno);
		aglfIO.setSprschm(sv.sprschm);
		aglfIO.setSprprc(sv.sprprc);
		aglfIO.setHouseLoan(sv.houseLoan);
		aglfIO.setCarLoan(sv.carLoan);
		aglfIO.setComputerLoan(sv.computerLoan);
		aglfIO.setOfficeRent(sv.officeRent);
		aglfIO.setOtherLoans(sv.otherLoans);
		aglfIO.setTcolprct(sv.tcolprct);
		aglfIO.setTcolmax(sv.tcolmax);
		aglfIO.setAgccqind(sv.agccqind);
		aglfIO.setFunction("KEEPS");
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
