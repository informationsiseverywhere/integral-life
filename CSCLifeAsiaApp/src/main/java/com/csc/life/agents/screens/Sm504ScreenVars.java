package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM504
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm504ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(293);
	public FixedLengthStringData dataFields = new FixedLengthStringData(181).isAPartOf(dataArea, 0);
	public FixedLengthStringData agntsels = new FixedLengthStringData(30).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] agntsel = FLSArrayPartOfStructure(3, 10, agntsels, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(agntsels, 0, FILLER_REDEFINE);
	public FixedLengthStringData agntsel01 = DD.agntsel.copy().isAPartOf(filler,0);
	public FixedLengthStringData agntsel02 = DD.agntsel.copy().isAPartOf(filler,10);
	public FixedLengthStringData agntsel03 = DD.agntsel.copy().isAPartOf(filler,20);
	public FixedLengthStringData agtnames = new FixedLengthStringData(141).isAPartOf(dataFields, 30);
	public FixedLengthStringData[] agtname = FLSArrayPartOfStructure(3, 47, agtnames, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(141).isAPartOf(agtnames, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtname01 = DD.agtname.copy().isAPartOf(filler1,0);
	public FixedLengthStringData agtname02 = DD.agtname.copy().isAPartOf(filler1,47);
	public FixedLengthStringData agtname03 = DD.agtname.copy().isAPartOf(filler1,94);
	public FixedLengthStringData userid = DD.userid.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 181);
	public FixedLengthStringData agntselsErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] agntselErr = FLSArrayPartOfStructure(3, 4, agntselsErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(agntselsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agntsel01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData agntsel02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData agntsel03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData agtnamesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] agtnameErr = FLSArrayPartOfStructure(3, 4, agtnamesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(agtnamesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData agtname01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData agtname02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData agtname03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData useridErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 209);
	public FixedLengthStringData agntselsOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(3, 12, agntselsOut, 0);
	public FixedLengthStringData[][] agntselO = FLSDArrayPartOfArrayStructure(12, 1, agntselOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(agntselsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agntsel01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] agntsel02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] agntsel03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData agtnamesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] agtnameOut = FLSArrayPartOfStructure(3, 12, agtnamesOut, 0);
	public FixedLengthStringData[][] agtnameO = FLSDArrayPartOfArrayStructure(12, 1, agtnameOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(agtnamesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] agtname01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] agtname02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] agtname03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] useridOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sm504screenWritten = new LongData(0);
	public LongData Sm504windowWritten = new LongData(0);
	public LongData Sm504hideWritten = new LongData(0);
	public LongData Sm504protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm504ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agntsel01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntsel02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntsel03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {userid, agntsel01, agtname01, agntsel02, agtname02, agntsel03, agtname03};
		screenOutFields = new BaseData[][] {useridOut, agntsel01Out, agtname01Out, agntsel02Out, agtname02Out, agntsel03Out, agtname03Out};
		screenErrFields = new BaseData[] {useridErr, agntsel01Err, agtname01Err, agntsel02Err, agtname02Err, agntsel03Err, agtname03Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm504screen.class;
		protectRecord = Sm504protect.class;
		hideRecord = Sm504hide.class;
	}

}
