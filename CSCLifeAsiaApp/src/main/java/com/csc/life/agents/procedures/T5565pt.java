/*
 * File: T5565pt.java
 * Date: 30 August 2009 2:23:00
 * Author: Quipoz Limited
 * 
 * Class transformed from T5565PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.T5565rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5565.
*
*
*****************************************************************
* </pre>
*/
public class T5565pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5565rec t5565rec = new T5565rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5565pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5565rec.t5565Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5565rec.termMax01);
		generalCopyLinesInner.fieldNo008.set(t5565rec.termMax02);
		generalCopyLinesInner.fieldNo009.set(t5565rec.termMax03);
		generalCopyLinesInner.fieldNo010.set(t5565rec.termMax04);
		generalCopyLinesInner.fieldNo011.set(t5565rec.termMax05);
		generalCopyLinesInner.fieldNo012.set(t5565rec.termMax06);
		generalCopyLinesInner.fieldNo013.set(t5565rec.termMax07);
		generalCopyLinesInner.fieldNo014.set(t5565rec.termMax08);
		generalCopyLinesInner.fieldNo015.set(t5565rec.termMax09);
		generalCopyLinesInner.fieldNo016.set(t5565rec.anbAtCcd01);
		generalCopyLinesInner.fieldNo017.set(t5565rec.prcnt01);
		generalCopyLinesInner.fieldNo018.set(t5565rec.prcnt02);
		generalCopyLinesInner.fieldNo019.set(t5565rec.prcnt03);
		generalCopyLinesInner.fieldNo020.set(t5565rec.prcnt04);
		generalCopyLinesInner.fieldNo021.set(t5565rec.prcnt05);
		generalCopyLinesInner.fieldNo022.set(t5565rec.prcnt06);
		generalCopyLinesInner.fieldNo023.set(t5565rec.prcnt07);
		generalCopyLinesInner.fieldNo024.set(t5565rec.prcnt08);
		generalCopyLinesInner.fieldNo025.set(t5565rec.prcnt09);
		generalCopyLinesInner.fieldNo026.set(t5565rec.anbAtCcd02);
		generalCopyLinesInner.fieldNo027.set(t5565rec.prcnt10);
		generalCopyLinesInner.fieldNo028.set(t5565rec.prcnt11);
		generalCopyLinesInner.fieldNo029.set(t5565rec.prcnt12);
		generalCopyLinesInner.fieldNo030.set(t5565rec.prcnt13);
		generalCopyLinesInner.fieldNo031.set(t5565rec.prcnt14);
		generalCopyLinesInner.fieldNo032.set(t5565rec.prcnt15);
		generalCopyLinesInner.fieldNo033.set(t5565rec.prcnt16);
		generalCopyLinesInner.fieldNo034.set(t5565rec.prcnt17);
		generalCopyLinesInner.fieldNo035.set(t5565rec.prcnt18);
		generalCopyLinesInner.fieldNo036.set(t5565rec.anbAtCcd03);
		generalCopyLinesInner.fieldNo037.set(t5565rec.prcnt19);
		generalCopyLinesInner.fieldNo038.set(t5565rec.prcnt20);
		generalCopyLinesInner.fieldNo039.set(t5565rec.prcnt21);
		generalCopyLinesInner.fieldNo040.set(t5565rec.prcnt22);
		generalCopyLinesInner.fieldNo041.set(t5565rec.prcnt23);
		generalCopyLinesInner.fieldNo042.set(t5565rec.prcnt24);
		generalCopyLinesInner.fieldNo043.set(t5565rec.prcnt25);
		generalCopyLinesInner.fieldNo044.set(t5565rec.prcnt26);
		generalCopyLinesInner.fieldNo045.set(t5565rec.prcnt27);
		generalCopyLinesInner.fieldNo046.set(t5565rec.anbAtCcd04);
		generalCopyLinesInner.fieldNo047.set(t5565rec.prcnt28);
		generalCopyLinesInner.fieldNo048.set(t5565rec.prcnt29);
		generalCopyLinesInner.fieldNo049.set(t5565rec.prcnt30);
		generalCopyLinesInner.fieldNo050.set(t5565rec.prcnt31);
		generalCopyLinesInner.fieldNo051.set(t5565rec.prcnt32);
		generalCopyLinesInner.fieldNo052.set(t5565rec.prcnt33);
		generalCopyLinesInner.fieldNo053.set(t5565rec.prcnt34);
		generalCopyLinesInner.fieldNo054.set(t5565rec.prcnt35);
		generalCopyLinesInner.fieldNo055.set(t5565rec.prcnt36);
		generalCopyLinesInner.fieldNo056.set(t5565rec.anbAtCcd05);
		generalCopyLinesInner.fieldNo057.set(t5565rec.prcnt37);
		generalCopyLinesInner.fieldNo058.set(t5565rec.prcnt38);
		generalCopyLinesInner.fieldNo059.set(t5565rec.prcnt39);
		generalCopyLinesInner.fieldNo060.set(t5565rec.prcnt40);
		generalCopyLinesInner.fieldNo061.set(t5565rec.prcnt41);
		generalCopyLinesInner.fieldNo062.set(t5565rec.prcnt42);
		generalCopyLinesInner.fieldNo063.set(t5565rec.prcnt43);
		generalCopyLinesInner.fieldNo064.set(t5565rec.prcnt44);
		generalCopyLinesInner.fieldNo065.set(t5565rec.prcnt45);
		generalCopyLinesInner.fieldNo066.set(t5565rec.anbAtCcd06);
		generalCopyLinesInner.fieldNo067.set(t5565rec.prcnt46);
		generalCopyLinesInner.fieldNo068.set(t5565rec.prcnt47);
		generalCopyLinesInner.fieldNo069.set(t5565rec.prcnt48);
		generalCopyLinesInner.fieldNo070.set(t5565rec.prcnt49);
		generalCopyLinesInner.fieldNo071.set(t5565rec.prcnt50);
		generalCopyLinesInner.fieldNo072.set(t5565rec.prcnt51);
		generalCopyLinesInner.fieldNo073.set(t5565rec.prcnt52);
		generalCopyLinesInner.fieldNo074.set(t5565rec.prcnt53);
		generalCopyLinesInner.fieldNo075.set(t5565rec.prcnt54);
		generalCopyLinesInner.fieldNo076.set(t5565rec.anbAtCcd07);
		generalCopyLinesInner.fieldNo077.set(t5565rec.prcnt55);
		generalCopyLinesInner.fieldNo078.set(t5565rec.prcnt56);
		generalCopyLinesInner.fieldNo079.set(t5565rec.prcnt57);
		generalCopyLinesInner.fieldNo080.set(t5565rec.prcnt58);
		generalCopyLinesInner.fieldNo081.set(t5565rec.prcnt59);
		generalCopyLinesInner.fieldNo082.set(t5565rec.prcnt60);
		generalCopyLinesInner.fieldNo083.set(t5565rec.prcnt61);
		generalCopyLinesInner.fieldNo084.set(t5565rec.prcnt62);
		generalCopyLinesInner.fieldNo085.set(t5565rec.prcnt63);
		generalCopyLinesInner.fieldNo086.set(t5565rec.anbAtCcd08);
		generalCopyLinesInner.fieldNo087.set(t5565rec.prcnt64);
		generalCopyLinesInner.fieldNo088.set(t5565rec.prcnt65);
		generalCopyLinesInner.fieldNo089.set(t5565rec.prcnt66);
		generalCopyLinesInner.fieldNo090.set(t5565rec.prcnt67);
		generalCopyLinesInner.fieldNo091.set(t5565rec.prcnt68);
		generalCopyLinesInner.fieldNo092.set(t5565rec.prcnt69);
		generalCopyLinesInner.fieldNo093.set(t5565rec.prcnt70);
		generalCopyLinesInner.fieldNo094.set(t5565rec.prcnt71);
		generalCopyLinesInner.fieldNo095.set(t5565rec.prcnt72);
		generalCopyLinesInner.fieldNo096.set(t5565rec.anbAtCcd09);
		generalCopyLinesInner.fieldNo097.set(t5565rec.prcnt73);
		generalCopyLinesInner.fieldNo098.set(t5565rec.prcnt74);
		generalCopyLinesInner.fieldNo099.set(t5565rec.prcnt75);
		generalCopyLinesInner.fieldNo100.set(t5565rec.prcnt76);
		generalCopyLinesInner.fieldNo101.set(t5565rec.prcnt77);
		generalCopyLinesInner.fieldNo102.set(t5565rec.prcnt78);
		generalCopyLinesInner.fieldNo103.set(t5565rec.prcnt79);
		generalCopyLinesInner.fieldNo104.set(t5565rec.prcnt80);
		generalCopyLinesInner.fieldNo105.set(t5565rec.prcnt81);
		generalCopyLinesInner.fieldNo106.set(t5565rec.anbAtCcd10);
		generalCopyLinesInner.fieldNo107.set(t5565rec.prcnt82);
		generalCopyLinesInner.fieldNo108.set(t5565rec.prcnt83);
		generalCopyLinesInner.fieldNo109.set(t5565rec.prcnt84);
		generalCopyLinesInner.fieldNo110.set(t5565rec.prcnt85);
		generalCopyLinesInner.fieldNo111.set(t5565rec.prcnt86);
		generalCopyLinesInner.fieldNo112.set(t5565rec.prcnt87);
		generalCopyLinesInner.fieldNo113.set(t5565rec.prcnt88);
		generalCopyLinesInner.fieldNo114.set(t5565rec.prcnt89);
		generalCopyLinesInner.fieldNo115.set(t5565rec.prcnt90);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(74);
	private FixedLengthStringData filler1 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(51).isAPartOf(wsaaPrtLine001, 23, FILLER).init("Term / Age Banded Initial Commission          S5565");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(73);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 43);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(45);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 13);
	private FixedLengthStringData filler8 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 23, FILLER).init("  Valid to:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(52);
	private FixedLengthStringData filler9 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine004, 31, FILLER).init("Term      Percentages");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler11 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(72).isAPartOf(wsaaPrtLine005, 7, FILLER).init("------------------------------------------------------------------------");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(78);
	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Age   |");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 11).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 14, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 19).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 27).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 30, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 35).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 38, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 43).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 51).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 54, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 59).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 67).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine006, 70, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(8);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 7, FILLER).init("|");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(80);
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 1).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine008, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(80);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 1).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine009, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(80);
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 1).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine010, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(80);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 1).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine011, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(80);
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 1).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine012, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(80);
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 1).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine013, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(80);
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 1).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine014, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(80);
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 1).setPattern("ZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine015, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(80);
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 1).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine016, 74).setPattern("ZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(80);
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine017, 1).setPattern("ZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine017, 4, FILLER).init("   |");
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 10).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 16, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 18).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 26).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 34).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 42).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler120 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 50).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler121 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 58).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 66).setPattern("ZZZ.ZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(5, 2).isAPartOf(wsaaPrtLine017, 74).setPattern("ZZZ.ZZ");
	}
}
