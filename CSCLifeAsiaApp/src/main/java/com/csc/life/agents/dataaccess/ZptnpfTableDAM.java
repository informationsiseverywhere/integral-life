package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZptnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:00
 * Class transformed from ZPTNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZptnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 103;
	public FixedLengthStringData zptnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zptnpfRecord = zptnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zptnrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zptnrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zptnrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zptnrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zptnrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zptnrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zptnrec);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(zptnrec);
	public FixedLengthStringData transCode = DD.trcde.copy().isAPartOf(zptnrec);
	public PackedDecimalData billcd = DD.billcd.copy().isAPartOf(zptnrec);
	public PackedDecimalData instfrom = DD.instfrom.copy().isAPartOf(zptnrec);
	public PackedDecimalData instto = DD.instto.copy().isAPartOf(zptnrec);
	public FixedLengthStringData zprflg = DD.zprflg.copy().isAPartOf(zptnrec);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(zptnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zptnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zptnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zptnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZptnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZptnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZptnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZptnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZptnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZptnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZptnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZPTNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TRANNO, " +
							"EFFDATE, " +
							"ORIGAMT, " +
							"TRCDE, " +
							"BILLCD, " +
							"INSTFROM, " +
							"INSTTO, " +
							"ZPRFLG, " +
							"TRANDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     tranno,
                                     effdate,
                                     origamt,
                                     transCode,
                                     billcd,
                                     instfrom,
                                     instto,
                                     zprflg,
                                     trandate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		tranno.clear();
  		effdate.clear();
  		origamt.clear();
  		transCode.clear();
  		billcd.clear();
  		instfrom.clear();
  		instto.clear();
  		zprflg.clear();
  		trandate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZptnrec() {
  		return zptnrec;
	}

	public FixedLengthStringData getZptnpfRecord() {
  		return zptnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZptnrec(what);
	}

	public void setZptnrec(Object what) {
  		this.zptnrec.set(what);
	}

	public void setZptnpfRecord(Object what) {
  		this.zptnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zptnrec.getLength());
		result.set(zptnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}