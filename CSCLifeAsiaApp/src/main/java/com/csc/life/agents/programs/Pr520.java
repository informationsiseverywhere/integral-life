/*
 * File: Pr520.java
 * Date: 30 August 2009 1:38:10
 * Author: Quipoz Limited
 * 
 * Class transformed from PR520.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.procedures.Tr520pt;
import com.csc.life.agents.screens.Sr520ScreenVars;
import com.csc.life.agents.tablestructures.Tr520rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Pr520 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR520");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Dated items by from date*/
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr520rec tr520rec = new Tr520rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr520ScreenVars sv = ScreenProgram.getScreenVars( Sr520ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr520() {
		super();
		screenVars = sv;
		new ScreenModel("Sr520", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr520rec.tr520Rec.set(itmdIO.getItemGenarea());
		if (isNE(itmdIO.getItemGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr520rec.zryrperc01.set(ZERO);
		tr520rec.zryrperc02.set(ZERO);
		tr520rec.zryrperc03.set(ZERO);
		tr520rec.zryrperc04.set(ZERO);
		tr520rec.zryrperc05.set(ZERO);
		tr520rec.zryrperc06.set(ZERO);
		tr520rec.zryrperc07.set(ZERO);
		tr520rec.zryrperc08.set(ZERO);
		tr520rec.zryrperc09.set(ZERO);
		tr520rec.zryrperc10.set(ZERO);
		tr520rec.zryrperc11.set(ZERO);
		tr520rec.zryrperc12.set(ZERO);
		tr520rec.zryrperc13.set(ZERO);
		tr520rec.zryrperc14.set(ZERO);
		tr520rec.zryrperc15.set(ZERO);
		tr520rec.zryrperc16.set(ZERO);
		tr520rec.zryrperc17.set(ZERO);
		tr520rec.zryrperc18.set(ZERO);
		tr520rec.zryrperc19.set(ZERO);
		tr520rec.zryrperc20.set(ZERO);
		tr520rec.zryrperc21.set(ZERO);
		tr520rec.zryrperc22.set(ZERO);
		tr520rec.zryrperc23.set(ZERO);
		tr520rec.zryrperc24.set(ZERO);
		tr520rec.zryrperc25.set(ZERO);
		tr520rec.zryrperc26.set(ZERO);
		tr520rec.zryrperc27.set(ZERO);
		tr520rec.zryrperc28.set(ZERO);
		tr520rec.zryrperc29.set(ZERO);
		tr520rec.zryrperc30.set(ZERO);
		tr520rec.zryrperc31.set(ZERO);
		tr520rec.zryrperc32.set(ZERO);
		tr520rec.zryrperc33.set(ZERO);
		tr520rec.zryrperc34.set(ZERO);
		tr520rec.zryrperc35.set(ZERO);
		tr520rec.zryrperc36.set(ZERO);
		tr520rec.zryrperc37.set(ZERO);
		tr520rec.zryrperc38.set(ZERO);
		tr520rec.zryrperc39.set(ZERO);
		tr520rec.zryrperc40.set(ZERO);
		tr520rec.zryrperc41.set(ZERO);
		tr520rec.zryrperc42.set(ZERO);
		tr520rec.zryrperc43.set(ZERO);
		tr520rec.zryrperc44.set(ZERO);
		tr520rec.zryrperc45.set(ZERO);
		tr520rec.zryrperc46.set(ZERO);
		tr520rec.zryrperc47.set(ZERO);
		tr520rec.zryrperc48.set(ZERO);
		tr520rec.zryrperc49.set(ZERO);
		tr520rec.zryrperc50.set(ZERO);
		tr520rec.zryrperc51.set(ZERO);
		tr520rec.zryrperc52.set(ZERO);
		tr520rec.zryrperc53.set(ZERO);
		tr520rec.zryrperc54.set(ZERO);
		tr520rec.zryrperc55.set(ZERO);
		tr520rec.zryrperc56.set(ZERO);
		tr520rec.zryrperc57.set(ZERO);
		tr520rec.zryrperc58.set(ZERO);
		tr520rec.zryrperc59.set(ZERO);
		tr520rec.zryrperc60.set(ZERO);
		tr520rec.zryrperc61.set(ZERO);
		tr520rec.zryrperc62.set(ZERO);
		tr520rec.zryrperc63.set(ZERO);
		tr520rec.zryrperc64.set(ZERO);
		tr520rec.zryrperc65.set(ZERO);
		tr520rec.zryrperc66.set(ZERO);
		tr520rec.zryrperc67.set(ZERO);
		tr520rec.zryrperc68.set(ZERO);
		tr520rec.zryrperc69.set(ZERO);
		tr520rec.zryrperc70.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.contitem.set(tr520rec.contitem);
		if (isEQ(itmdIO.getItemItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.zcomcodes.set(tr520rec.zcomcodes);
		sv.zryrpercs.set(tr520rec.zryrpercs);
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr520rec.tr520Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.contitem,tr520rec.contitem)) {
			tr520rec.contitem.set(sv.contitem);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zcomcodes,tr520rec.zcomcodes)) {
			tr520rec.zcomcodes.set(sv.zcomcodes);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zryrpercs,tr520rec.zryrpercs)) {
			tr520rec.zryrpercs.set(sv.zryrpercs);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Tr520pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
