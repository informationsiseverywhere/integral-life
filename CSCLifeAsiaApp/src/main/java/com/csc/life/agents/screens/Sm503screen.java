package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm503screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 21, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm503ScreenVars sv = (Sm503ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm503screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm503ScreenVars screenVars = (Sm503ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.agntnum.setClassString("");
		screenVars.agtname.setClassString("");
		screenVars.dteappDisp.setClassString("");
		screenVars.agtype.setClassString("");
		screenVars.descrip01.setClassString("");
		screenVars.agntbr.setClassString("");
		screenVars.descrip02.setClassString("");
		screenVars.mnth.setClassString("");
		screenVars.acctyr.setClassString("");
		screenVars.mlgrppp01.setClassString("");
		screenVars.mlgrppp02.setClassString("");
		screenVars.mldirpp02.setClassString("");
		screenVars.mlgrppp03.setClassString("");
		screenVars.mldirpp03.setClassString("");
		screenVars.mlgrppp04.setClassString("");
		screenVars.mldirpp04.setClassString("");
		screenVars.mlgrppp05.setClassString("");
		screenVars.mldirpp05.setClassString("");
		screenVars.mlgrppp06.setClassString("");
		screenVars.mldirpp06.setClassString("");
		screenVars.mlgrppp07.setClassString("");
		screenVars.mldirpp07.setClassString("");
		screenVars.mlgrppp08.setClassString("");
		screenVars.mldirpp08.setClassString("");
		screenVars.mlgrppp09.setClassString("");
		screenVars.mldirpp09.setClassString("");
		screenVars.mlgrppp10.setClassString("");
		screenVars.mldirpp10.setClassString("");
		screenVars.mlytdperfn.setClassString("");
		screenVars.countpol.setClassString("");
		screenVars.mldirpp01.setClassString("");
		screenVars.mlperpp01.setClassString("");
		screenVars.mlperpp02.setClassString("");
		screenVars.mlperpp03.setClassString("");
		screenVars.mlperpp04.setClassString("");
		screenVars.mlperpp05.setClassString("");
		screenVars.mlperpp06.setClassString("");
		screenVars.mlperpp07.setClassString("");
		screenVars.mlperpp08.setClassString("");
		screenVars.mlperpp09.setClassString("");
		screenVars.mlperpp10.setClassString("");
		screenVars.mlytdperca.setClassString("");
		screenVars.mlytdgrpca.setClassString("");
		screenVars.mlytdgrpfn.setClassString("");
	}

/**
 * Clear all the variables in Sm503screen
 */
	public static void clear(VarModel pv) {
		Sm503ScreenVars screenVars = (Sm503ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.agntnum.clear();
		screenVars.agtname.clear();
		screenVars.dteappDisp.clear();
		screenVars.dteapp.clear();
		screenVars.agtype.clear();
		screenVars.descrip01.clear();
		screenVars.agntbr.clear();
		screenVars.descrip02.clear();
		screenVars.mnth.clear();
		screenVars.acctyr.clear();
		screenVars.mlgrppp01.clear();
		screenVars.mlgrppp02.clear();
		screenVars.mldirpp02.clear();
		screenVars.mlgrppp03.clear();
		screenVars.mldirpp03.clear();
		screenVars.mlgrppp04.clear();
		screenVars.mldirpp04.clear();
		screenVars.mlgrppp05.clear();
		screenVars.mldirpp05.clear();
		screenVars.mlgrppp06.clear();
		screenVars.mldirpp06.clear();
		screenVars.mlgrppp07.clear();
		screenVars.mldirpp07.clear();
		screenVars.mlgrppp08.clear();
		screenVars.mldirpp08.clear();
		screenVars.mlgrppp09.clear();
		screenVars.mldirpp09.clear();
		screenVars.mlgrppp10.clear();
		screenVars.mldirpp10.clear();
		screenVars.mlytdperfn.clear();
		screenVars.countpol.clear();
		screenVars.mldirpp01.clear();
		screenVars.mlperpp01.clear();
		screenVars.mlperpp02.clear();
		screenVars.mlperpp03.clear();
		screenVars.mlperpp04.clear();
		screenVars.mlperpp05.clear();
		screenVars.mlperpp06.clear();
		screenVars.mlperpp07.clear();
		screenVars.mlperpp08.clear();
		screenVars.mlperpp09.clear();
		screenVars.mlperpp10.clear();
		screenVars.mlytdperca.clear();
		screenVars.mlytdgrpca.clear();
		screenVars.mlytdgrpfn.clear();
	}
}
