package com.csc.life.agents.dataaccess;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: ZmpbpfTableDAM.java
 * Date: Wed Jul 17 17:17:08 SGT 2013
 * Class transformed from ZmpbPF
 * Author: csc Limited
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class ZmpbpfTableDAM extends PFAdapterDAM {
	/*********************************************************************
	 * total up the record length as thew aggregate of all field lengths */
	public int pfRecLen = 184;
	public FixedLengthStringData zmpbrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zmpbpfRecord = zmpbrec;
	
	public PackedDecimalData acctamt = DD.acctamt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData bonusamt = DD.bonusamt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData comtot = DD.comtot.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData jobno = DD.jobno.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData life = DD.life.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData prcdate = DD.prcdate.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData prcent = DD.prcent.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData prcnt = DD.prcnt.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(zmpbrec); 
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zmpbrec); 
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zmpbrec); 
	private static final Logger LOGGER = LoggerFactory.getLogger(ZmpbpfTableDAM.class);//IJTI-318
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZmpbpfTableDAM() {
			super();
  		setColumns();
  		journalled = true;
		LOGGER.info("CONSTRUCTOR DEFAULT FOR Zmpb");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZmpbpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
  		LOGGER.info("CONSTRUCTOR LOCK FOR Zmpb");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpbpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
  		LOGGER.info("INFDS CONSTRUCTOR FOR Zmpb");//IJTI-318
	}

	/**
	* Constructor for ClntpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZmpbpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZMPBPF");
		LOGGER.info("SETTING TABLE FOR Zmpb");//IJTI-318
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
		QUALIFIEDCOLUMNS = 
								"ACCTAMT, " + 
								"AGNTCOY, " + 
								"AGNTNUM, " + 
								"BATCACTMN, " + 
								"BATCACTYR, " + 
								"BATCTRCDE, " + 
								"BONUSAMT, " + 
								"CHDRNUM, " + 
								"COMTOT, " + 
								"COVERAGE, " + 
								"DATIME, " + 
								"EFFDATE, " + 
								"JOBNM, " + 
								"JOBNME, " + 
								"JOBNO, " + 
								"LIFE, " + 
								"ORIGCURR, " + 
								"PRCDATE, " + 
								"PRCENT, " + 
								"PRCNT, " + 
								"RIDER, " + 
								"TRANNO, " + 
								"USRPRF, " + 
								"UNIQUE_NUMBER";
							
	}

	public void setColumns() {
		qualifiedColumns = new BaseData[] { 
								acctamt, 
								agntcoy, 
								agntnum, 
								batcactmn, 
								batcactyr, 
								batctrcde, 
								bonusamt, 
								chdrnum, 
								comtot, 
								coverage, 
								datime, 
								effdate, 
								jobName, 
								jobno, 
								life, 
								origcurr, 
								prcdate, 
								prcent, 
								prcnt, 
								rider, 
								tranno, 
								userProfile, 
								unique_number  };

	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
		
								acctamt.clear(); 
								agntcoy.clear(); 
								agntnum.clear(); 
								batcactmn.clear(); 
								batcactyr.clear(); 
								batctrcde.clear(); 
								bonusamt.clear(); 
								chdrnum.clear(); 
								comtot.clear(); 
								coverage.clear(); 
								datime.clear(); 
								effdate.clear(); 
								jobName.clear(); 
								jobno.clear(); 
								life.clear(); 
								origcurr.clear(); 
								prcdate.clear(); 
								prcent.clear(); 
								prcnt.clear(); 
								rider.clear(); 
								tranno.clear(); 
								userProfile.clear(); 

	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZmpbrec() {
  		return zmpbrec;
	}

	public FixedLengthStringData getZmpbpfRecord() {
  		return zmpbpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZmpbrec(what);
	}

	public void setZmpbrec(Object what) {
  		this.zmpbrec.set(what);
	}

	public void setZmpbpfRecord(Object what) {
  		this.zmpbpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zmpbrec.getLength());
		result.set(zmpbrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}
