/*
 * File: Br553.java
 * Date: 29 August 2009 22:19:36
 * Author: Quipoz Limited
 * 
 * Class transformed from BR553.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.Agp1pfTableDAM;
import com.csc.life.agents.dataaccess.AgpdTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.LifeenqTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*              Agency Compensation Extract
*              ---------------------------
*
*  This batch program will extract all records from AGPY and
*  after re-formatting will write the appropriate details to
*  2 files.
*
*  The 1st file (AGPD) will keep a permanent record of all the
*  agent commissions paid for each agent on every of his
*  contract on a particular effective date.
*
*  The 2nd file will show basically the same information, but
*  formatted such that it can be processed by a PC application
*  after being downloaded.
*
*****************************************************************
* </pre>
*/
public class Br553 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Agp1pfTableDAM agp1pf = new Agp1pfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR553");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaFirstRead = "Y";
		/*                                          "'LIFEFLR/PCEXTR'".    */
	private FixedLengthStringData wsaaFlr = new FixedLengthStringData(16).init(SPACES);
	private FixedLengthStringData wsaaFlrLib = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaFlrName = new FixedLengthStringData(10).init(SPACES);

	private FixedLengthStringData wsaaAgp1Para = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaFilename = new FixedLengthStringData(4).isAPartOf(wsaaAgp1Para, 0);
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaAgp1Para, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAgp1Para, 6).setUnsigned();

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaTotals = new FixedLengthStringData(36);
	private PackedDecimalData wsaaInitComm = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 0).init(ZERO);
	private PackedDecimalData wsaaRenewComm = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 9).init(ZERO);
	private PackedDecimalData wsaaOvrrComm = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 18).init(ZERO);
	private PackedDecimalData wsaaSingPrem = new PackedDecimalData(17, 2).isAPartOf(wsaaTotals, 27).init(ZERO);

		/* WSAA-FLAGS */
	private FixedLengthStringData wsaaWriteSummary = new FixedLengthStringData(1).init("N");
	private Validator writeSummary = new Validator(wsaaWriteSummary, "Y");

	private FixedLengthStringData wsaaCommType = new FixedLengthStringData(2);
	private Validator commInit = new Validator(wsaaCommType, "IC");
	private Validator commRenew = new Validator(wsaaCommType, "RC");
	private Validator commOvrr = new Validator(wsaaCommType, "OC");
	private Validator validCommType = new Validator(wsaaCommType, "IC","RC","OC");
		/* WSAA-SPACES */
	private FixedLengthStringData n2Spaces = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData n1Space = new FixedLengthStringData(1).init(SPACES);
		/* WSAA-AGENT */
	private FixedLengthStringData wsaaAgntcoy = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).init(SPACES);
		/* TABLES */
	private static final String t5696 = "T5696";
		/* ERRORS */
	private static final String z509 = "Z509";
		/* FORMATS */
	private static final String agpdrec = "AGPDREC";
	private static final String agpyagtrec = "AGPYAGTREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private static final String cltsrec = "CLTSREC";
	private static final String descrec = "DESCREC";
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AgpdTableDAM agpdIO = new AgpdTableDAM();
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LifeenqTableDAM lifeenqIO = new LifeenqTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agp1pfRecordInner agp1pfRecordInner = new Agp1pfRecordInner();

	public Br553() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		wsaaAgntnum.set(SPACES);
		wsaaAgntcoy.set(SPACES);
		wsaaChdrnum.set(SPACES);
		agpyagtIO.setRecKeyData(SPACES);
		agpyagtIO.setRecNonKeyData(SPACES);
		wsaaFirstRead = "Y";
		agpyagtIO.setFunction(varcom.begn);
		agpyagtIO.setFormat(agpyagtrec);
		override1500();
		/*    OPEN OUTPUT AGP1PF-FILE.                                     */
		agp1pf.openOutput();
		/*EXIT*/
	}

protected void override1500()
	{
		/*OVERRIDE*/
		/*    Construct the name of the temporary file which we will be    */
		/*    working with....                                             */
		wsaaFilename.set(bprdIO.getSystemParam01());
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaFlrLib.set(bprdIO.getSystemParam02());
		wsaaFlrName.set(bprdIO.getSystemParam03());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsaaFlrLib, SPACES);
		stringVariable1.addExpression(wsaaFlrName, SPACES);
		stringVariable1.setStringInto(wsaaFlr);
		/*    Do the override.                                             */
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(AGP1PF)    TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaAgp1Para);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThread);
		stringVariable2.addExpression(") ");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFile2010();
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, agpyagtIO);
		if (isNE(agpyagtIO.getStatuz(), varcom.oK)
		&& isNE(agpyagtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agpyagtIO.getParams());
			syserrrec.statuz.set(agpyagtIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agpyagtIO.getStatuz(), varcom.endp)) {
			if (isEQ(wsaaFirstRead, "N")) {
				writeSummaryRec3100();
			}
			/*    PERFORM 3100-WRITE-SUMMARY-REC*/
			wsspEdterror.set(varcom.endp);
			return ;
		}
		wsaaFirstRead = "N";
		agpyagtIO.setFunction(varcom.nextr);
	}

protected void edit2500()
	{
		/*EDIT*/
		if (isNE(wsaaAgntnum, SPACES)
		&& (isNE(agpyagtIO.getRldgacct(), wsaaAgntnum)
		|| isNE(agpyagtIO.getRldgcoy(), wsaaAgntcoy)
		|| isNE(agpyagtIO.getRdocnum(), wsaaChdrnum)
		|| isEQ(agpyagtIO.getStatuz(), varcom.endp))) {
			wsaaWriteSummary.set("Y");
			return ;
		}
		if (isEQ(wsaaAgntnum, SPACES)) {
			wsaaAgntcoy.set(agpyagtIO.getRldgcoy());
			wsaaAgntnum.set(agpyagtIO.getRldgacct());
			wsaaChdrnum.set(agpyagtIO.getRdocnum());
		}
		/*EXIT*/
	}

protected void update3000()
	{
		updateFiles3010();
	}

protected void updateFiles3010()
	{
		if (writeSummary.isTrue()) {
			writeSummaryRec3100();
			initialize(wsaaTotals);
			wsaaAgntcoy.set(agpyagtIO.getRldgcoy());
			wsaaAgntnum.set(agpyagtIO.getRldgacct());
			wsaaChdrnum.set(agpyagtIO.getRdocnum());
			wsaaWriteSummary.set("N");
		}
		/* Read contract details....*/
		readChdr3200();
		/* Move details to Extracted file & AGPD file.....*/
		agp1pfRecordInner.billfreq.set(chdrenqIO.getBillfreq());
		agpdIO.setBillfreq(chdrenqIO.getBillfreq());
		agp1pfRecordInner.chdrnum.set(chdrenqIO.getChdrnum());
		agpdIO.setChdrnum(chdrenqIO.getChdrnum());
		agp1pfRecordInner.occdate.set(chdrenqIO.getOccdate());
		agpdIO.setOccdate(chdrenqIO.getOccdate());
		agp1pfRecordInner.origcurr.set(agpyagtIO.getOrigcurr());
		agpdIO.setOrigcurr(agpyagtIO.getOrigcurr());
		wsaaCommType.set(agpyagtIO.getSacstyp());
		/* Check type of commission, Initial / Renewal / Overidding /*/
		/* single premium contract's commission*/
		/* If in the case of Initial commission...*/
		if (commInit.isTrue()) {
			wsaaInitComm.add(agpyagtIO.getOrigamt());
			if (isEQ(chdrenqIO.getBillfreq(), "00")) {
				wsaaSingPrem.add(agpyagtIO.getOrigamt());
			}
		}
		/* If in the case of Renewal commission...*/
		if (commRenew.isTrue()) {
			wsaaRenewComm.add(agpyagtIO.getOrigamt());
		}
		/* If in the case of Overidding commission...*/
		if (commOvrr.isTrue()) {
			wsaaOvrrComm.add(agpyagtIO.getOrigamt());
		}
		/* If other than any of the above ...*/
		if (!validCommType.isTrue()) {
			conlogrec.params.set(SPACES);
			conlogrec.error.set(z509);
			callConlog003();
		}
	}

protected void writeSummaryRec3100()
	{
		write3110();
		readAglf3120();
		getAreaDesc3130();
		getAgentName3140();
		getPolicyHolderName3150();
		writeDetails3160();
	}

protected void write3110()
	{
		agp1pfRecordInner.sign01.set("+");
		agp1pfRecordInner.sign02.set("+");
		agp1pfRecordInner.sign03.set("+");
		agp1pfRecordInner.sign04.set("+");
		agpdIO.setEffdate(bsscIO.getEffectiveDate());
		agp1pfRecordInner.effectiveDate.set(bsscIO.getEffectiveDate());
		agpdIO.setAcctmnth(bsscIO.getAcctMonth());
		agp1pfRecordInner.acctmnth.set(bsscIO.getAcctMonth());
		agpdIO.setAcctyr(bsscIO.getAcctYear());
		agp1pfRecordInner.acctyr.set(bsscIO.getAcctYear());
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrenqIO.getOccdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("01");
		datcon3rec.freqFactor.set(ZERO);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		compute(agp1pfRecordInner.xcomd, 5).set(add(0.99999, datcon3rec.freqFactor));
		setPrecision(agpdIO.getXcomd(), 5);
		agpdIO.setXcomd(add(0.99999, datcon3rec.freqFactor));
		agpdIO.setAgntcoy(wsaaAgntcoy);
		agp1pfRecordInner.agentCoy.set(wsaaAgntcoy);
		agpdIO.setAgntnum(wsaaAgntnum);
		agp1pfRecordInner.agentNo.set(wsaaAgntnum);
		agpdIO.setInitcom(wsaaInitComm);
		agp1pfRecordInner.initComm.set(wsaaInitComm);
		if (isLT(wsaaInitComm, 0)) {
			agp1pfRecordInner.sign01.set("-");
		}
		agpdIO.setRnlcdue(wsaaRenewComm);
		agp1pfRecordInner.renewComm.set(wsaaRenewComm);
		if (isLT(wsaaRenewComm, 0)) {
			agp1pfRecordInner.sign02.set("-");
		}
		agpdIO.setOrannl(wsaaOvrrComm);
		agp1pfRecordInner.orannl.set(wsaaOvrrComm);
		if (isLT(wsaaOvrrComm, 0)) {
			agp1pfRecordInner.sign03.set("-");
		}
		agpdIO.setSinglePremium(wsaaSingPrem);
		agp1pfRecordInner.singPrem.set(wsaaSingPrem);
		if (isLT(wsaaSingPrem, 0)) {
			agp1pfRecordInner.sign04.set("-");
		}
	}

protected void readAglf3120()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setNonKeyData(SPACES);
		aglflnbIO.setAgntcoy(wsaaAgntcoy);
		aglflnbIO.setAgntnum(wsaaAgntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(aglflnbIO.getParams());
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			fatalError600();
		}
		agpdIO.setDteapp(aglflnbIO.getDteapp());
		agp1pfRecordInner.dateAppointed.set(aglflnbIO.getDteapp());
		agpdIO.setDtetrm(aglflnbIO.getDtetrm());
		if (isEQ(aglflnbIO.getDtetrm(), varcom.vrcmMaxDate)) {
			agp1pfRecordInner.dateTerminated.set(ZERO);
		}
		else {
			agp1pfRecordInner.dateTerminated.set(aglflnbIO.getDtetrm());
		}
	}

protected void getAreaDesc3130()
	{
		descIO.setRecKeyData(SPACES);
		descIO.setRecNonKeyData(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(aglflnbIO.getAgntcoy());
		descIO.setDesctabl(t5696);
		descIO.setDescitem(aglflnbIO.getAracde());
		descIO.setLanguage("E");
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		agpdIO.setAreadesc(descIO.getShortdesc());
		agp1pfRecordInner.areaDesc.set(descIO.getShortdesc());
	}

protected void getAgentName3140()
	{
		cltsIO.setRecKeyData(SPACES);
		cltsIO.setRecNonKeyData(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getSurname(), n2Spaces);
		stringVariable1.addExpression(n1Space);
		stringVariable1.addExpression(cltsIO.getGivname());
		stringVariable1.setStringInto(agpdIO.getAgentname());
		agp1pfRecordInner.agentName.set(agpdIO.getAgentname());
	}

protected void getPolicyHolderName3150()
	{
		lifeenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		lifeenqIO.setChdrnum(chdrenqIO.getChdrnum());
		lifeenqIO.setLife("01");
		lifeenqIO.setJlife("00");
		lifeenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifeenqIO);
		if (isNE(lifeenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifeenqIO.getParams());
			fatalError600();
		}
		cltsIO.setClntnum(lifeenqIO.getLifcnum());
		cltsIO.setClntcoy(bsprIO.getFsuco());
		cltsIO.setClntpfx("CN");
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getSurname(), n2Spaces);
		stringVariable1.addExpression(n1Space);
		stringVariable1.addExpression(cltsIO.getGivname());
		stringVariable1.setStringInto(agpdIO.getOwnername());
		agp1pfRecordInner.ownername.set(agpdIO.getOwnername());
	}

protected void writeDetails3160()
	{
		agp1pf.write(agp1pfRecordInner.agp1pfRecord);
		agpdIO.setFormat(agpdrec);
		agpdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agpdIO);
		if (isNE(agpdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agpdIO.getParams());
			syserrrec.statuz.set(agpdIO.getStatuz());
			fatalError600();
		}
		/*INITIALIZE*/
		initialize(agp1pfRecordInner.agp1pfRecord);
		agpdIO.setParams(SPACES);
		/*EXIT*/
	}

protected void readChdr3200()
	{
		/*ENTER*/
		chdrenqIO.setRecKeyData(SPACES);
		chdrenqIO.setRecNonKeyData(SPACES);
		chdrenqIO.setChdrcoy(agpyagtIO.getRldgcoy());
		chdrenqIO.setChdrnum(agpyagtIO.getRdocnum());
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrenqIO.getParams());
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
	}

protected void closeFiles4010()
	{
		/*    CLOSE AGP1PF-FILE.                                           */
		agp1pf.close();
		lsaaStatuz.set(varcom.oK);
		if (isEQ(wsaaFirstRead, "Y")) {
			return ;
		}
		if (isEQ(wsaaFlr, SPACES)) {
			return ;
		}
		/*CPYTOPCD*/
		wsaaQcmdexc.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("CPYTOPCD FROMFILE(");
		stringVariable1.addExpression(wsaaAgp1Para);
		stringVariable1.addExpression(") TOFLR(");
		stringVariable1.addExpression(wsaaFlr);
		stringVariable1.addExpression(") TODOC(AGP1PC.TXT) REPLACE(*YES)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
	}
/*
 * Class transformed  from Data Structure AGP1PF-RECORD--INNER
 */
private static final class Agp1pfRecordInner { 

		/*FD  AGP1PF-FILE                 LABEL RECORDS STANDARD.          */
	private FixedLengthStringData agp1pfRecord = new FixedLengthStringData(250);
	private FixedLengthStringData effectiveDate = new FixedLengthStringData(8).isAPartOf(agp1pfRecord, 0);
	private FixedLengthStringData agentCoy = new FixedLengthStringData(1).isAPartOf(agp1pfRecord, 8);
	private FixedLengthStringData agentNo = new FixedLengthStringData(8).isAPartOf(agp1pfRecord, 9);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(agp1pfRecord, 17);
	private FixedLengthStringData agentName = new FixedLengthStringData(47).isAPartOf(agp1pfRecord, 25);
	private FixedLengthStringData ownername = new FixedLengthStringData(47).isAPartOf(agp1pfRecord, 72);
	private FixedLengthStringData areaDesc = new FixedLengthStringData(10).isAPartOf(agp1pfRecord, 119);
	private ZonedDecimalData dateAppointed = new ZonedDecimalData(8, 0).isAPartOf(agp1pfRecord, 129).setUnsigned();
	private ZonedDecimalData dateTerminated = new ZonedDecimalData(8, 0).isAPartOf(agp1pfRecord, 137).setUnsigned();
	private FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(agp1pfRecord, 145);
	private ZonedDecimalData occdate = new ZonedDecimalData(8, 0).isAPartOf(agp1pfRecord, 147).setUnsigned();
	private ZonedDecimalData acctmnth = new ZonedDecimalData(2, 0).isAPartOf(agp1pfRecord, 155).setUnsigned();
	private ZonedDecimalData acctyr = new ZonedDecimalData(2, 0).isAPartOf(agp1pfRecord, 157).setUnsigned();
	private FixedLengthStringData origcurr = new FixedLengthStringData(3).isAPartOf(agp1pfRecord, 159);
	private ZonedDecimalData xcomd = new ZonedDecimalData(2, 0).isAPartOf(agp1pfRecord, 162).setUnsigned();
	private ZonedDecimalData initComm = new ZonedDecimalData(17, 2).isAPartOf(agp1pfRecord, 164).setUnsigned();
	private FixedLengthStringData sign01 = new FixedLengthStringData(1).isAPartOf(agp1pfRecord, 181);
	private ZonedDecimalData renewComm = new ZonedDecimalData(17, 2).isAPartOf(agp1pfRecord, 182).setUnsigned();
	private FixedLengthStringData sign02 = new FixedLengthStringData(1).isAPartOf(agp1pfRecord, 199);
	private ZonedDecimalData orannl = new ZonedDecimalData(17, 2).isAPartOf(agp1pfRecord, 200).setUnsigned();
	private FixedLengthStringData sign03 = new FixedLengthStringData(1).isAPartOf(agp1pfRecord, 217);
	private ZonedDecimalData singPrem = new ZonedDecimalData(17, 2).isAPartOf(agp1pfRecord, 218).setUnsigned();
	private FixedLengthStringData sign04 = new FixedLengthStringData(1).isAPartOf(agp1pfRecord, 235);
	private FixedLengthStringData filler = new FixedLengthStringData(14).isAPartOf(agp1pfRecord, 236, FILLER);
}
}
