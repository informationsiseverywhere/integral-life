/*
 * File: Br604.java
 * Date: 29 August 2009 22:22:41
 * Author: Quipoz Limited
 * 
 * Class transformed from BR604.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.MacfTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   MACFPF  - One time Conversion - Program 2
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Br604 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR511");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevAgntnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaCurrEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaSubtractedTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsbbAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsbbReportag = new FixedLengthStringData(8);
	private String wsaaFirstTime = "";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(5, 8, wsaaReportags, 0);
	private String macfrec = "MACFREC";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Agent Career History*/
	private MacfTableDAM macfIO = new MacfTableDAM();

	public Br604() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		macfIO.setDataKey(SPACES);
		macfIO.setEffdate(99999999);
		macfIO.setTranno(99999);
		wsaaPrevAgntcoy.set(SPACES);
		wsaaPrevAgntnum.set(SPACES);
		macfIO.setFunction(varcom.begnh);
		macfIO.setFormat(macfrec);
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		SmartFileCode.execute(appVars, macfIO);
		if (isNE(macfIO.getStatuz(),varcom.oK)
		&& isNE(macfIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(macfIO.getStatuz());
			syserrrec.params.set(macfIO.getParams());
			fatalError600();
		}
		if (isEQ(macfIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		if (isEQ(macfIO.getStatuz(),varcom.oK)
		&& (isNE(macfIO.getAgntnum(),wsaaPrevAgntnum)
		|| isNE(macfIO.getAgntcoy(),wsaaPrevAgntcoy))) {
			wsaaFirstTime = "Y";
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(macfIO.getCurrfrom(),varcom.vrcmMaxDate)
		|| isEQ(macfIO.getCurrfrom(),ZERO)) {
			macfIO.setCurrfrom(macfIO.getEffdate());
			macfIO.setCurrto(varcom.vrcmMaxDate);
			if (isEQ(wsaaFirstTime,"Y")
			&& isNE(macfIO.getZrptga(),SPACES)) {
				wsbbAgntcoy.set(macfIO.getAgntcoy());
				wsbbReportag.set(macfIO.getAgntnum());
				wsaaReportags.set(SPACES);
				wsaaIndex.set(1);
				while ( !(isEQ(wsbbReportag,SPACES))) {
					getReportag3100();
				}
				
				macfIO.setZrptga(wsaaReportag[2]);
				macfIO.setZrptgb(wsaaReportag[3]);
				macfIO.setZrptgc(wsaaReportag[4]);
				macfIO.setZrptgd(wsaaReportag[5]);
			}
			if (isEQ(wsaaFirstTime,"N")
			&& isEQ(macfIO.getCurrto(),varcom.vrcmMaxDate)
			&& isEQ(macfIO.getAgntcoy(),wsaaPrevAgntcoy)
			&& isEQ(macfIO.getAgntnum(),wsaaPrevAgntnum)
			&& isEQ(macfIO.getTranno(),wsaaSubtractedTranno)) {
				macfIO.setCurrto(wsaaCurrEffdate);
			}
			wsaaCurrEffdate.set(macfIO.getEffdate());
			wsaaPrevAgntcoy.set(macfIO.getAgntcoy());
			wsaaPrevAgntnum.set(macfIO.getAgntnum());
			wsaaSubtractedTranno.set(macfIO.getTranno());
			compute(wsaaSubtractedTranno, 0).set(sub(wsaaSubtractedTranno,1));
			wsaaFirstTime = "N";
			macfIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, macfIO);
			if (isNE(macfIO.getStatuz(),varcom.oK)
			&& isNE(macfIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(macfIO.getStatuz());
				syserrrec.params.set(macfIO.getParams());
				fatalError600();
			}
		}
		macfIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void getReportag3100()
	{
		reportag3100();
	}

protected void reportag3100()
	{
		aglflnbIO.setAgntnum(wsbbReportag);
		aglflnbIO.setAgntcoy(wsbbAgntcoy);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		if (isEQ(aglflnbIO.getStatuz(),varcom.oK)) {
			wsaaReportag[wsaaIndex.toInt()].set(aglflnbIO.getAgntnum());
			wsaaIndex.add(1);
			wsbbReportag.set(aglflnbIO.getReportag());
		}
	}
}
