package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:07
 * Description:
 * Copybook name: LIFECMCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifecmckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifecmcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifecmcKey = new FixedLengthStringData(256).isAPartOf(lifecmcFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifecmcChdrcoy = new FixedLengthStringData(1).isAPartOf(lifecmcKey, 0);
  	public FixedLengthStringData lifecmcChdrnum = new FixedLengthStringData(8).isAPartOf(lifecmcKey, 1);
  	public FixedLengthStringData lifecmcLife = new FixedLengthStringData(2).isAPartOf(lifecmcKey, 9);
  	public FixedLengthStringData lifecmcJlife = new FixedLengthStringData(2).isAPartOf(lifecmcKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifecmcKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifecmcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifecmcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}