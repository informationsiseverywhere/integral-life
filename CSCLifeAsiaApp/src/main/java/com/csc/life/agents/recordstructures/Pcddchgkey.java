package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PCDDCHGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pcddchgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pcddchgFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData pcddchgKey = new FixedLengthStringData(64).isAPartOf(pcddchgFileKey, 0, REDEFINE);
  	public FixedLengthStringData pcddchgChdrcoy = new FixedLengthStringData(1).isAPartOf(pcddchgKey, 0);
  	public FixedLengthStringData pcddchgChdrnum = new FixedLengthStringData(8).isAPartOf(pcddchgKey, 1);
  	public FixedLengthStringData pcddchgAgntnum = new FixedLengthStringData(8).isAPartOf(pcddchgKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(pcddchgKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pcddchgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pcddchgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}