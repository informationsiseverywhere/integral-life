package com.csc.life.agents.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.hibernate.HibernateException;

import com.csc.fsu.agents.dataaccess.dao.AgncypfDAO;
import com.csc.fsu.agents.dataaccess.model.Agncypf;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.CltrelnDTO;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.life.agents.screens.Sjl31ScreenVars;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;

public class Pjl31 extends ScreenProgCS {

	private Sjl31ScreenVars sv = ScreenProgram.getScreenVars(Sjl31ScreenVars.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL31");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private AgncypfDAO agncypfDAO = getApplicationContext().getBean("agncypfDAO", AgncypfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Agncypf agncypf = new Agncypf();
	private Clntpf clntpf = null;
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private CltrelnDTO cltrelnDTO;
	private Map<String,Descpf> descMap = new HashMap<>();
	private DescDAO descdao = getApplicationContext().getBean("descDAO",DescDAO.class);
	
	private static final String T5696 = "T5696";
	private static final String T1692 = "T1692";


	public Pjl31() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl31", AppVars.getInstance(), sv);
	}
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}
	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		}
	}
	@Override
	protected void initialise1000() {
		sv.dataArea.set(SPACES);
		initialise1010();
		protect1020();
	}

	protected void initialise1010() {

		if (isEQ(wsspcomn.flag, "A")) {
			sv.clntsel.set(SPACE);
			sv.cltname.set(SPACE);
			sv.agncynum.set(wsspcomn.chdrCownnum);
			sv.regnum.set(SPACE);
			sv.agntbr.set(SPACE);
			sv.aracde.set(SPACE);
			sv.srdate.set(varcom.vrcmMaxDate);
			sv.endate.set(varcom.vrcmMaxDate);
			sv.ipdate.set(varcom.vrcmMaxDate);
			sv.ppdate.set(varcom.vrcmMaxDate);
		} else {
			initialise1020();
		}
	}

	private void initialise1020() {

		if (isEQ(wsspcomn.chdrChdrnum, SPACE)) {
			sv.agncynumErr.set("E305");
			return;
		}
		agncypf = agncypfDAO.getAgncy(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString().trim(), 1);
		Optional<Agncypf> isExists = Optional.ofNullable(agncypf);
		if (isExists.isPresent()) {
			sv.clntsel.set(agncypf.getClntnum());
			clntpf = new Clntpf();
			clntpf = cltsioCall2700(agncypf.getClntnum());
			Optional<Clntpf> op = Optional.ofNullable(clntpf);
			if (op.isPresent()) {
				sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
			}
			if (null != agncypf.getAgncynum())
				sv.agncynum.set(agncypf.getAgncynum());
			if (null != agncypf.getRegnum())
				sv.regnum.set(agncypf.getRegnum());
			if (null != agncypf.getBrnch()){
				sv.agntbr.set(agncypf.getBrnch());
				loadBranchDesc();
			}
			if (null != agncypf.getAreacode()){
				sv.aracde.set(agncypf.getAreacode());
				loadAreaDesc();
			}
			if (null != agncypf.getSrdate())
				sv.srdate.set(agncypf.getSrdate());
			if (null != agncypf.getEndate())
				sv.endate.set(agncypf.getEndate());
			if (null != agncypf.getIppdate())
				sv.ipdate.set(agncypf.getIppdate());
			if (null != agncypf.getPpdate())
				sv.ppdate.set(agncypf.getPpdate());	
		} else {
			sv.agncynumErr.set("E305");
		}
	}

	@SuppressWarnings("static-access")
	private void protect1020() {
		if (isEQ(wsspcomn.flag, "T")) {
			sv.clntselOut[varcom.pr.toInt()].set("Y");
			sv.regnumOut[varcom.pr.toInt()].set("Y");
			sv.agntbrOut[varcom.pr.toInt()].set("Y");
			sv.aracdeOut[varcom.pr.toInt()].set("Y");
			sv.srdateOut[varcom.pr.toInt()].set("Y");
			sv.ipdateOut[varcom.pr.toInt()].set("Y");
			sv.ppdateOut[varcom.pr.toInt()].set("Y");
		} else if (isEQ(wsspcomn.flag, "M")) {
			sv.clntselOut[varcom.pr.toInt()].set("Y");
		}
	}

	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "R")) {
			scrnparams.function.set(varcom.prot);
		}
	}
	@Override
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
		}
		if(isEQ(wsspcomn.flag, "I")) {
			return;
		}
		
		setClntName();
		validate2010();
		loadBranchDesc();
		loadAreaDesc();
	}

	private void setClntName() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(sv.clntsel.toString());
		Optional<Clntpf> op = Optional.ofNullable(clntpf);

		if (op.isPresent()) {
			sv.cltname.set(clntpf.getGivname() + " " + clntpf.getSurname());
		}
	}

	private void validate2010() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(sv.clntsel.toString());
		Optional<Clntpf> op = Optional.ofNullable(clntpf);

		if (op.isPresent()) {
			if (!clntpf.getValidflag().equals("1"))
				sv.clntselErr.set("F782");
		}
		if (!op.isPresent())
			sv.clntselErr.set("F393");

		if (isEQ(wsspcomn.flag, "T") && isEQ(sv.endate, varcom.vrcmMaxDate))
			sv.endateErr.set("E186");
		
		if (isEQ(wsspcomn.flag, "A") || isEQ(wsspcomn.flag, "M")) {
			if (isEQ(sv.clntsel, SPACE))
				sv.clntselErr.set("E186");

			if (isEQ(sv.srdate, varcom.vrcmMaxDate))
				sv.srdateErr.set("E186");
			
			if (isEQ(sv.regnum, SPACE))
				sv.regnumErr.set("E186");
			
			if(isEQ(sv.ipdate, varcom.vrcmMaxDate))
				sv.ipdateErr.set("E186");
		}
		if (isGT(sv.srdate, sv.endate))
			sv.srdateErr.set("JL56");

		if (isNE(sv.errorIndicators, SPACES))
			wsspcomn.edterror.set("Y");

	}
	
	protected void loadBranchDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T1692, sv.agntbr.trim());
		descMap= descdao.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t1692Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T1692) ==null) {
			sv.agbrdesc.set(SPACE);
		}
		else
		{
			t1692Desc = descMap.get(T1692);
			sv.agbrdesc.set(t1692Desc.getLongdesc());
		}


	}
	
	protected void loadAreaDesc()
	{
		Map<String,String> itemMap =  new HashMap<>();
		itemMap.put(T5696, sv.aracde.trim());
		
		
		descMap= descdao.searchMultiDescpf(wsspcomn.company.toString(), wsspcomn.language.toString(), itemMap);

		Descpf t5696Desc;
		if( descMap==null || descMap.isEmpty() || descMap.get(T5696) ==null) {
			sv.aradesc.set(SPACE);
		}
		else {
			t5696Desc = descMap.get(T5696);
			sv.aradesc.set(t5696Desc.getLongdesc());
		}

	}
	
	@Override
	protected void update3000() {
		if(isEQ(wsspcomn.flag, "I")) {
			return;
		}

		if (isEQ(wsspcomn.flag, "M"))
			updateDatabase3020();
		else if (isEQ(wsspcomn.flag, "T"))
			updateDatabase3030();
		else if (isEQ(wsspcomn.flag, "R"))
			updateDatabase3040();
		else if (isEQ(wsspcomn.flag, "A")) {
			updateDatabase3010();
			updateClntpf();
			insertClrrpf();
		}
	}

	// Insert
	private void updateDatabase3010() {

		agncypf.setAgncycoy(wsspcomn.company.toString());
		agncypf.setAgncynum(sv.agncynum.toString());
		agncypf.setClntnum(sv.clntsel.toString());
		agncypf.setRegnum(sv.regnum.toString());
		agncypf.setBrnch(sv.agntbr.toString());
		agncypf.setAreacode(sv.aracde.toString());
		agncypf.setSrdate(sv.srdate.toInt());
		agncypf.setEndate(sv.endate.toInt());
		agncypf.setIppdate(sv.ipdate.toInt());
		agncypf.setPpdate(sv.ppdate.toInt());
		agncypf.setValidFlag(1);
		try {
			agncypfDAO.insertAgncyDetails(agncypf);
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}

	}

	// Modify
	private void updateDatabase3020() {

		agncypf.setAgncynum(wsspcomn.chdrChdrnum.toString().trim());
		agncypf.setClntnum(sv.clntsel.toString());
		agncypf.setRegnum(sv.regnum.toString());
		agncypf.setBrnch(sv.agntbr.toString());
		agncypf.setAreacode(sv.aracde.toString());
		agncypf.setSrdate(sv.srdate.toInt());
		agncypf.setEndate(sv.endate.toInt());
		agncypf.setIppdate(sv.ipdate.toInt());
		agncypf.setPpdate(sv.ppdate.toInt());
		try {
			agncypfDAO.updateAgncypf(agncypf);
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}
	}

	// Terminate
	private void updateDatabase3030() {

		agncypf.setAgncynum(wsspcomn.chdrChdrnum.toString().trim());
		agncypf.setEndate(sv.endate.toInt());
		try {
			agncypfDAO.updateTerminate(agncypf);
		} catch (HibernateException ex) {
			ex.printStackTrace();
		}
	}

	// Reinstate
	private void updateDatabase3040() {

		agncypf.setAgncynum(wsspcomn.chdrChdrnum.toString().trim());
		agncypf.setEndate(varcom.vrcmMaxDate.toInt());
		agncypfDAO.updateReinstate(agncypf);
	}

	private void updateClntpf() {
		clntpf = new Clntpf();
		clntpf = cltsioCall2700(agncypf.getClntnum());
		clntpfDAO.updateClntRoleflag(36, "Y", " ", clntpf);
	}
	protected Clntpf cltsioCall2700(String clntNum) {

		return clntpfDAO.getClntpf("CN", wsspcomn.fsuco.toString(), clntNum);
	}
	
	private void insertClrrpf() {
		cltrelnDTO=new CltrelnDTO();
		cltrelnDTO.setClntcoy("9");
		cltrelnDTO.setClntpfx("CN");
		cltrelnDTO.setClntnum(agncypf.getClntnum());
		cltrelnDTO.setClrrrole("AN");
		cltrelnDTO.setForepfx("AN");
		cltrelnDTO.setForecoy("1");
		cltrelnDTO.setForenum(sv.agncynum.toString());
		clrrpfDAO.intsertClrrpf(cltrelnDTO);
		}
	
	@Override
	protected void whereNext4000() {
		wsspcomn.programPtr.add(1);
	}
}
