package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Tr58qrec  extends ExternalData{
	
	public FixedLengthStringData tr58qRec = new FixedLengthStringData(500); 
	public ZonedDecimalData anntarprm = new ZonedDecimalData(17,2).isAPartOf(tr58qRec,0);
	public ZonedDecimalData blprem = new ZonedDecimalData(17,2).isAPartOf(tr58qRec,17);
	public ZonedDecimalData premst = new ZonedDecimalData(17,2).isAPartOf(tr58qRec,34);
	public ZonedDecimalData zlocamt = new ZonedDecimalData(15,2).isAPartOf(tr58qRec,51);
	public ZonedDecimalData znadjperc01 = new ZonedDecimalData(5,2).isAPartOf(tr58qRec,66);
	public ZonedDecimalData znadjperc02 = new ZonedDecimalData(5,2).isAPartOf(tr58qRec,71);
  	public FixedLengthStringData filler = new FixedLengthStringData(424).isAPartOf(tr58qRec, 76, FILLER);


    public void initialize() {
		COBOLFunctions.initialize(tr58qRec);
	}	


	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr58qRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
