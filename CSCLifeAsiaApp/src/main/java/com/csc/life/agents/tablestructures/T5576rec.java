package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:15:02
 * Description:
 * Copybook name: T5576REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5576rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5576Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData actualday = new FixedLengthStringData(1).isAPartOf(t5576Rec, 0);
  	public FixedLengthStringData agemaxs = new FixedLengthStringData(24).isAPartOf(t5576Rec, 1);
  	public ZonedDecimalData[] agemax = ZDArrayPartOfStructure(8, 3, 0, agemaxs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(agemaxs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData agemax01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData agemax02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public ZonedDecimalData agemax03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData agemax04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
  	public ZonedDecimalData agemax05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData agemax06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
  	public ZonedDecimalData agemax07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData agemax08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
  	public FixedLengthStringData ages = new FixedLengthStringData(24).isAPartOf(t5576Rec, 25);
  	public ZonedDecimalData[] age = ZDArrayPartOfStructure(8, 3, 0, ages, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ages, 0, FILLER_REDEFINE);
  	public ZonedDecimalData age01 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData age02 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 3);
  	public ZonedDecimalData age03 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 6);
  	public ZonedDecimalData age04 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 9);
  	public ZonedDecimalData age05 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 12);
  	public ZonedDecimalData age06 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData age07 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 18);
  	public ZonedDecimalData age08 = new ZonedDecimalData(3, 0).isAPartOf(filler1, 21);
  	public FixedLengthStringData annumpcs = new FixedLengthStringData(40).isAPartOf(t5576Rec, 49);
  	public ZonedDecimalData[] annumpc = ZDArrayPartOfStructure(8, 5, 2, annumpcs, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(40).isAPartOf(annumpcs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData annumpc01 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 0);
  	public ZonedDecimalData annumpc02 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 5);
  	public ZonedDecimalData annumpc03 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 10);
  	public ZonedDecimalData annumpc04 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 15);
  	public ZonedDecimalData annumpc05 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 20);
  	public ZonedDecimalData annumpc06 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 25);
  	public ZonedDecimalData annumpc07 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 30);
  	public ZonedDecimalData annumpc08 = new ZonedDecimalData(5, 2).isAPartOf(filler2, 35);
  	public FixedLengthStringData flatpcnts = new FixedLengthStringData(40).isAPartOf(t5576Rec, 89);
  	public ZonedDecimalData[] flatpcnt = ZDArrayPartOfStructure(8, 5, 2, flatpcnts, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(flatpcnts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData flatpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData flatpcnt02 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 5);
  	public ZonedDecimalData flatpcnt03 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 10);
  	public ZonedDecimalData flatpcnt04 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 15);
  	public ZonedDecimalData flatpcnt05 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 20);
  	public ZonedDecimalData flatpcnt06 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 25);
  	public ZonedDecimalData flatpcnt07 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 30);
  	public ZonedDecimalData flatpcnt08 = new ZonedDecimalData(5, 2).isAPartOf(filler3, 35);
  	public FixedLengthStringData highmth = new FixedLengthStringData(1).isAPartOf(t5576Rec, 129);
  	public FixedLengthStringData highyr = new FixedLengthStringData(1).isAPartOf(t5576Rec, 130);
  	public FixedLengthStringData lowmth = new FixedLengthStringData(1).isAPartOf(t5576Rec, 131);
  	public FixedLengthStringData lowyr = new FixedLengthStringData(1).isAPartOf(t5576Rec, 132);
  	public FixedLengthStringData maxpcnts = new FixedLengthStringData(40).isAPartOf(t5576Rec, 133);
  	public ZonedDecimalData[] maxpcnt = ZDArrayPartOfStructure(8, 5, 2, maxpcnts, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(maxpcnts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData maxpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData maxpcnt02 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 5);
  	public ZonedDecimalData maxpcnt03 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 10);
  	public ZonedDecimalData maxpcnt04 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 15);
  	public ZonedDecimalData maxpcnt05 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 20);
  	public ZonedDecimalData maxpcnt06 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 25);
  	public ZonedDecimalData maxpcnt07 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 30);
  	public ZonedDecimalData maxpcnt08 = new ZonedDecimalData(5, 2).isAPartOf(filler4, 35);
  	public FixedLengthStringData minpcnts = new FixedLengthStringData(40).isAPartOf(t5576Rec, 173);
  	public ZonedDecimalData[] minpcnt = ZDArrayPartOfStructure(8, 5, 2, minpcnts, 0);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(minpcnts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData minpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 0);
  	public ZonedDecimalData minpcnt02 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 5);
  	public ZonedDecimalData minpcnt03 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 10);
  	public ZonedDecimalData minpcnt04 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 15);
  	public ZonedDecimalData minpcnt05 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 20);
  	public ZonedDecimalData minpcnt06 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 25);
  	public ZonedDecimalData minpcnt07 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 30);
  	public ZonedDecimalData minpcnt08 = new ZonedDecimalData(5, 2).isAPartOf(filler5, 35);
  	public FixedLengthStringData filler6 = new FixedLengthStringData(287).isAPartOf(t5576Rec, 213, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5576Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5576Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}