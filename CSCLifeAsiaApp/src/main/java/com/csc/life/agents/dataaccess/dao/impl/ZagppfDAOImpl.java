package com.csc.life.agents.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZagppfDAO;
import com.csc.life.agents.dataaccess.model.Zagppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZagppfDAOImpl extends BaseDAOImpl<Zagppf> implements ZagppfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZagppfDAOImpl.class);

	@Override
	public Zagppf getZagppf(Zagppf zagppf) {

		String query = new String(
				"SELECT AGNTCOY,AGNTNUM,AGNTYPE01,AGNTYPE02,DATIME,JOBNAME,MTH,PRCNT01,PRCNT02,USERPROFILE,YEAR FROM ZAGPPF WHERE AGNTNUM=? AND AGNTCOY=? AND YEAR=? AND MTH=?");

		PreparedStatement stmt = null;
		ResultSet rs = null;
		Zagppf zagppfrs = null;

		try {
			stmt = getPrepareStatement(query);/* IJTI-1523 */
			stmt.setString(1, zagppf.getAgntnum());
			stmt.setInt(2, zagppf.getAgntcoy());
			stmt.setShort(3, zagppf.getYear());
			stmt.setByte(4, zagppf.getMth());

			rs = stmt.executeQuery();

			while (rs.next()) {
				zagppfrs = new Zagppf();
				zagppfrs.setAgntcoy(rs.getInt(1));
				zagppfrs.setAgntnum(rs.getString(2));
				zagppfrs.setAgntype01(rs.getString(3));
				zagppfrs.setAgntype02(rs.getString(4));
				zagppfrs.setDatime(rs.getString(5));
				zagppfrs.setJobName(rs.getString(6));
				zagppfrs.setMth(rs.getByte(7));
				zagppfrs.setPrcnt01(rs.getDouble(8));
				zagppfrs.setPrcnt02(rs.getDouble(9));
				zagppfrs.setUserProfile(rs.getString(10));
				zagppfrs.setYear(rs.getShort(11));
			}

		} catch (SQLException e) {
			LOGGER.error("getZmbwpf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		return zagppf;
	}

}
