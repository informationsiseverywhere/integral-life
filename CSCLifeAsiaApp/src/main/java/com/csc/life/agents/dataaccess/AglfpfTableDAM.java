package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AglfpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:43
 * Class transformed from AGLFPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AglfpfTableDAM extends PFAdapterDAM {
	//TMLII-281 AG-01-002
	public int pfRecLen = 307;
	public FixedLengthStringData aglfrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData aglfpfRecord = aglfrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(aglfrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(aglfrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(aglfrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(aglfrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(aglfrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(aglfrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(aglfrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(aglfrec);
	public PackedDecimalData dteapp = DD.dteapp.copy().isAPartOf(aglfrec);
	public PackedDecimalData dtetrm = DD.dtetrm.copy().isAPartOf(aglfrec);
	public FixedLengthStringData trmcde = DD.trmcde.copy().isAPartOf(aglfrec);
	public PackedDecimalData dteexp = DD.dteexp.copy().isAPartOf(aglfrec);
	public FixedLengthStringData pftflg = DD.pftflg.copy().isAPartOf(aglfrec);
	public FixedLengthStringData rasflg = DD.rasflg.copy().isAPartOf(aglfrec);
	public FixedLengthStringData bcmtab = DD.bcmtab.copy().isAPartOf(aglfrec);
	public FixedLengthStringData rcmtab = DD.rcmtab.copy().isAPartOf(aglfrec);
	public FixedLengthStringData scmtab = DD.scmtab.copy().isAPartOf(aglfrec);
	public FixedLengthStringData agentClass = DD.agcls.copy().isAPartOf(aglfrec);
	public FixedLengthStringData ocmtab = DD.ocmtab.copy().isAPartOf(aglfrec);
	public FixedLengthStringData reportag = DD.reportag.copy().isAPartOf(aglfrec);
	public PackedDecimalData ovcpc = DD.ovcpc.copy().isAPartOf(aglfrec);
	public FixedLengthStringData taxmeth = DD.taxmeth.copy().isAPartOf(aglfrec);
	public FixedLengthStringData irdno = DD.irdno.copy().isAPartOf(aglfrec);
	public FixedLengthStringData taxcde = DD.taxcde.copy().isAPartOf(aglfrec);
	public PackedDecimalData taxalw = DD.taxalw.copy().isAPartOf(aglfrec);
	public FixedLengthStringData sprschm = DD.sprschm.copy().isAPartOf(aglfrec);
	public PackedDecimalData sprprc = DD.sprprc.copy().isAPartOf(aglfrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(aglfrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(aglfrec);
	public FixedLengthStringData payfrq = DD.payfrq.copy().isAPartOf(aglfrec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(aglfrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(aglfrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(aglfrec);
	public PackedDecimalData dtepay = DD.dtepay.copy().isAPartOf(aglfrec);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(aglfrec);
	public PackedDecimalData intcrd = DD.intcrd.copy().isAPartOf(aglfrec);
	public PackedDecimalData fixprc = DD.fixprc.copy().isAPartOf(aglfrec);
	public FixedLengthStringData bmaflg = DD.bmaflg.copy().isAPartOf(aglfrec);
	public FixedLengthStringData exclAgmt = DD.excagr.copy().isAPartOf(aglfrec);
	public FixedLengthStringData houseLoan = DD.hseln.copy().isAPartOf(aglfrec);
	public FixedLengthStringData computerLoan = DD.comln.copy().isAPartOf(aglfrec);
	public FixedLengthStringData carLoan = DD.carln.copy().isAPartOf(aglfrec);
	public FixedLengthStringData officeRent = DD.offrent.copy().isAPartOf(aglfrec);
	public FixedLengthStringData otherLoans = DD.othln.copy().isAPartOf(aglfrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(aglfrec);
	public PackedDecimalData minsta = DD.minsta.copy().isAPartOf(aglfrec);
	public FixedLengthStringData zrorcode = DD.zrorcode.copy().isAPartOf(aglfrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(aglfrec);
	public FixedLengthStringData tagsusind = DD.tagsusind.copy().isAPartOf(aglfrec);
	public FixedLengthStringData tlaglicno = DD.tlaglicno.copy().isAPartOf(aglfrec);
	public PackedDecimalData tlicexpdt = DD.tlicexpdt.copy().isAPartOf(aglfrec);
	public PackedDecimalData tcolprct = DD.tcolprct.copy().isAPartOf(aglfrec);
	public PackedDecimalData tcolmax = DD.tcolmax.copy().isAPartOf(aglfrec);
	public FixedLengthStringData tsalesunt = DD.tsalesunt.copy().isAPartOf(aglfrec);
	public FixedLengthStringData prdagent = DD.prdagent.copy().isAPartOf(aglfrec);
	public FixedLengthStringData agccqind = DD.agccqind.copy().isAPartOf(aglfrec);
	//TMLII-281 AG-01-002 - added 'zrecruit'
	public FixedLengthStringData zrecruit = DD.zrecruit.copy().isAPartOf(aglfrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(aglfrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(aglfrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(aglfrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(aglfrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AglfpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AglfpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AglfpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AglfpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AglfpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AglfpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AglfpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGLFPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"CURRFROM, " +
							"CURRTO, " +
							"DTEAPP, " +
							"DTETRM, " +
							"TRMCDE, " +
							"DTEEXP, " +
							"PFTFLG, " +
							"RASFLG, " +
							"BCMTAB, " +
							"RCMTAB, " +
							"SCMTAB, " +
							"AGCLS, " +
							"OCMTAB, " +
							"REPORTAG, " +
							"OVCPC, " +
							"TAXMETH, " +
							"IRDNO, " +
							"TAXCDE, " +
							"TAXALW, " +
							"SPRSCHM, " +
							"SPRPRC, " +
							"PAYCLT, " +
							"PAYMTH, " +
							"PAYFRQ, " +
							"FACTHOUS, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"DTEPAY, " +
							"CURRCODE, " +
							"INTCRD, " +
							"FIXPRC, " +
							"BMAFLG, " +
							"EXCAGR, " +
							"HSELN, " +
							"COMLN, " +
							"CARLN, " +
							"OFFRENT, " +
							"OTHLN, " +
							"ARACDE, " +
							"MINSTA, " +
							"ZRORCODE, " +
							"EFFDATE, " +
							"TAGSUSIND, " +
							"TLAGLICNO, " +
							"TLICEXPDT, " +
							"TCOLPRCT, " +
							"TCOLMAX, " +
							"TSALESUNT, " +
							"PRDAGENT, " +
							"AGCCQIND, " +
							"VALIDFLAG, " +
							"USRPRF, " +
							//TMLII-281 AG-01-002
							"ZRECRUIT, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     currfrom,
                                     currto,
                                     dteapp,
                                     dtetrm,
                                     trmcde,
                                     dteexp,
                                     pftflg,
                                     rasflg,
                                     bcmtab,
                                     rcmtab,
                                     scmtab,
                                     agentClass,
                                     ocmtab,
                                     reportag,
                                     ovcpc,
                                     taxmeth,
                                     irdno,
                                     taxcde,
                                     taxalw,
                                     sprschm,
                                     sprprc,
                                     payclt,
                                     paymth,
                                     payfrq,
                                     facthous,
                                     bankkey,
                                     bankacckey,
                                     dtepay,
                                     currcode,
                                     intcrd,
                                     fixprc,
                                     bmaflg,
                                     exclAgmt,
                                     houseLoan,
                                     computerLoan,
                                     carLoan,
                                     officeRent,
                                     otherLoans,
                                     aracde,
                                     minsta,
                                     zrorcode,
                                     effdate,
                                     tagsusind,
                                     tlaglicno,
                                     tlicexpdt,
                                     tcolprct,
                                     tcolmax,
                                     tsalesunt,
                                     prdagent,
                                     agccqind,
                                     validflag,
                                     //TMLII-281 AG-01-002
                                     zrecruit,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		currfrom.clear();
  		currto.clear();
  		dteapp.clear();
  		dtetrm.clear();
  		trmcde.clear();
  		dteexp.clear();
  		pftflg.clear();
  		rasflg.clear();
  		bcmtab.clear();
  		rcmtab.clear();
  		scmtab.clear();
  		agentClass.clear();
  		ocmtab.clear();
  		reportag.clear();
  		ovcpc.clear();
  		taxmeth.clear();
  		irdno.clear();
  		taxcde.clear();
  		taxalw.clear();
  		sprschm.clear();
  		sprprc.clear();
  		payclt.clear();
  		paymth.clear();
  		payfrq.clear();
  		facthous.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		dtepay.clear();
  		currcode.clear();
  		intcrd.clear();
  		fixprc.clear();
  		bmaflg.clear();
  		exclAgmt.clear();
  		houseLoan.clear();
  		computerLoan.clear();
  		carLoan.clear();
  		officeRent.clear();
  		otherLoans.clear();
  		aracde.clear();
  		minsta.clear();
  		zrorcode.clear();
  		effdate.clear();
  		tagsusind.clear();
  		tlaglicno.clear();
  		tlicexpdt.clear();
  		tcolprct.clear();
  		tcolmax.clear();
  		tsalesunt.clear();
  		prdagent.clear();
  		agccqind.clear();
  		//TMLII-281 AG-01-002
  		zrecruit.clear();
  		validflag.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAglfrec() {
  		return aglfrec;
	}

	public FixedLengthStringData getAglfpfRecord() {
  		return aglfpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAglfrec(what);
	}

	public void setAglfrec(Object what) {
  		this.aglfrec.set(what);
	}

	public void setAglfpfRecord(Object what) {
  		this.aglfpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(aglfrec.getLength());
		result.set(aglfrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}