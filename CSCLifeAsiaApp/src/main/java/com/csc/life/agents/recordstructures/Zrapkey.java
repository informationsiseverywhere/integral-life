package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:20
 * Description:
 * Copybook name: ZRAPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zrapkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zrapFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zrapKey = new FixedLengthStringData(64).isAPartOf(zrapFileKey, 0, REDEFINE);
  	public FixedLengthStringData zrapAgntcoy = new FixedLengthStringData(1).isAPartOf(zrapKey, 0);
  	public FixedLengthStringData zrapAgntnum = new FixedLengthStringData(8).isAPartOf(zrapKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(zrapKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zrapFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zrapFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}