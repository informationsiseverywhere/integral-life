package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr60dscreen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {0, 0, 0, 0}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr60dScreenVars sv = (Sr60dScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr60dscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr60dScreenVars screenVars = ( Sr60dScreenVars )pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttype.setClassString("");
		screenVars.ctypdesc.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifedesc.setClassString("");
		screenVars.datefrm.setClassString("");
		screenVars.dateto.setClassString("");
		screenVars.zsgtpct.setClassString("");
		screenVars.zoerpct.setClassString("");
		screenVars.zdedpct.setClassString("");
		screenVars.zundpct.setClassString("");
		screenVars.zspspct.setClassString("");
		screenVars.totprcnt.setClassString("");
		screenVars.zsgtamt.setClassString("");
		screenVars.zoeramt.setClassString("");
		screenVars.zdedamt.setClassString("");
		screenVars.zundamt.setClassString("");
		screenVars.zspsamt.setClassString("");
		screenVars.totamnt.setClassString("");
		screenVars.zslrysamt.setClassString("");//ILIFE-7345
		screenVars.zslryspct.setClassString("");//ILIFE-7345
		// ILIFE-7916 Start 
		screenVars.s290NoticeReceived.setClassString("");
		screenVars.riskPrem.setClassString("");
		screenVars.tottax.setClassString("");
		screenVars.coContriPmt.setClassString("");
		screenVars.receivedFromAto.setClassString("");
		
			//ILIFE-7916 End -->
		
	}
	
	/**
	 * Clear all the variables in Sr60dscreen
	 */
	public static void clear(VarModel pv) {
		Sr60dScreenVars screenVars = (Sr60dScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttype.clear();
		screenVars.ctypdesc.clear();
		screenVars.lifenum.clear();
		screenVars.lifedesc.clear();
		screenVars.datefrm.clear();
		screenVars.dateto.clear();
		screenVars.zsgtpct.clear();
		screenVars.zoerpct.clear();
		screenVars.zdedpct.clear();
		screenVars.zundpct.clear();
		screenVars.zspspct.clear();
		screenVars.totprcnt.clear();
		screenVars.zsgtamt.clear();
		screenVars.zoeramt.clear();
		screenVars.zdedamt.clear();
		screenVars.zundamt.clear();
		screenVars.zspsamt.clear();
		screenVars.totamnt.clear();
		screenVars.zslrysamt.clear();//ILIFE-7345
		screenVars.zslrysamt.clear();//ILIFE-7345
		// ILIFE-7916 Start 
		screenVars.s290NoticeReceived.clear();
		screenVars.riskPrem.clear();
		screenVars.tottax.clear();
		screenVars.coContriPmt.clear();
		screenVars.receivedFromAto.clear();
		//ILIFE-7916 End -->
	}

}
