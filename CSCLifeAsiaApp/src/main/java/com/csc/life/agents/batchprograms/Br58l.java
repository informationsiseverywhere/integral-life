/*
 * File: Bh579.java
 * Date: 29 August 2009 21:35:51
 * Author: Quipoz Limited

 * Vibhor Khare for Ticket #TMLII-288 [AG-06-008 RM Gets RM Bonus (RGR) calculation]
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.dao.ZrcapfDAO;
import com.csc.life.agents.dataaccess.dao.ZrfbpfDAO;
import com.csc.life.agents.dataaccess.dao.ZrgapfDAO;
import com.csc.life.agents.dataaccess.dao.ZrgrpfDAO;
import com.csc.life.agents.dataaccess.model.Zrcapf;
import com.csc.life.agents.dataaccess.model.Zrfbpf;
import com.csc.life.agents.dataaccess.model.Zrgapf;
import com.csc.life.agents.dataaccess.model.Zrgrpf;
import com.csc.life.agents.tablestructures.Tr58trec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Br58l extends Mainb {

	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlAcmvpfRS = null;
	private java.sql.PreparedStatement sqlAcmvpfPS = null;
	private java.sql.Connection sqlAcmvpfConn = null;
	private String sqlAcmvpf = "";
	
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData formats = new FixedLengthStringData(110);
	private FixedLengthStringData bmsgrec = new FixedLengthStringData(10).isAPartOf(formats, 0).init("BMSGREC");
	private FixedLengthStringData bprdrec = new FixedLengthStringData(10).isAPartOf(formats, 10).init("BPRDREC");
	private FixedLengthStringData bsprrec = new FixedLengthStringData(10).isAPartOf(formats, 20).init("BSPRREC");
	private FixedLengthStringData bsscrec = new FixedLengthStringData(10).isAPartOf(formats, 30).init("BSSCREC");
	private FixedLengthStringData buparec = new FixedLengthStringData(10).isAPartOf(formats, 40).init("BUPAREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).isAPartOf(formats, 50).init("DESCREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).isAPartOf(formats, 60).init("CHDRLIFREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(formats, 70).init("ITEMREC");
	private FixedLengthStringData fluprec = new FixedLengthStringData(10).isAPartOf(formats, 80).init("FLUPREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).isAPartOf(formats, 90).init("CLTSREC");
	private FixedLengthStringData letcrec = new FixedLengthStringData(10).isAPartOf(formats, 100).init("LETCREC");
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();

		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Zrgrpf zrgrpf;
	private Zrgapf zrgapf;
	private Zrgapf zrgapfData;
	private ZrgrpfDAO zrgrpfDao = getApplicationContext().getBean("zrgrpfDao", ZrgrpfDAO.class);
    private ZrgapfDAO zrgapfDao = getApplicationContext().getBean("zrgapfDao", ZrgapfDAO.class);

	private PackedDecimalData wsaaRgrAmt = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaPlnSfx = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaComprcd = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaCrTable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaAgTypRcted = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaVldRmted = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaComInd = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR58L");
	
	private String tr58t = "TR58T";
	private String t5645 = "T5645";
	private Tr58trec tr58trec = new Tr58trec();	
	private T5645rec t5645rec = new T5645rec();
	private Lifacmvrec lifacmvrec1 = new Lifacmvrec();
	private String zrgarec = "ZRGAREC";
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private String covrrec = "COVRREC";
	private CovtTableDAM covtIO = new CovtTableDAM();
	private String covtrec = "COVTREC";
	private String zrgrrec = "ZRGRREC";
	private Datcon3rec datcon3rec = new Datcon3rec();
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(3, 0).setUnsigned();
	public FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	
	public int pfRecLen = 320;
	public FixedLengthStringData acmvrec1 = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData acmvpfRecord = acmvrec1;
	int counter=0;
	
	public FixedLengthStringData rdocpfx = DD.rdocpfx.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData rdoccoy = DD.rdoccoy.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData rdocnum = DD.rdocnum.copy().isAPartOf(acmvrec1);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(acmvrec1);
	public PackedDecimalData jrnseq = DD.jrnseq.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData rldgpfx = DD.rldgpfx.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData rldgcoy = DD.rldgcoy.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData rldgacct = DD.rldgacct.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData sacscode = DD.sacscode.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData sacstyp = DD.sacstyp.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(acmvrec1);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(acmvrec1);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData origcurr = DD.origcurr.copy().isAPartOf(acmvrec1);
	public PackedDecimalData origamt = DD.origamt.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData tranref = DD.tranref.copy().isAPartOf(acmvrec1);
	private FixedLengthStringData wsaaTrefChdrnum = new FixedLengthStringData(8).isAPartOf(tranref, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(tranref, 8);
	private FixedLengthStringData wsaaCover = new FixedLengthStringData(2).isAPartOf(tranref, 10);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(tranref, 12);
	private FixedLengthStringData wsaaPlnSfxA = new FixedLengthStringData(2).isAPartOf(tranref, 14);
	private FixedLengthStringData filler = new FixedLengthStringData(14).isAPartOf(tranref, 16, FILLER);
	
	public FixedLengthStringData trandesc = DD.trandesc.copy().isAPartOf(acmvrec1);
	public PackedDecimalData crate = DD.crate.copy().isAPartOf(acmvrec1);
	public PackedDecimalData acctamt = DD.acctamt.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData genlcoy = DD.genlcoy.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData genlcur = DD.genlcur.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData glcode = DD.glcode.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData glsign = DD.glsign.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData postyear = DD.postyear.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData postmonth = DD.postmonth.copy().isAPartOf(acmvrec1);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(acmvrec1);
	public PackedDecimalData dateto = DD.dateto.copy().isAPartOf(acmvrec1);
	public PackedDecimalData rcamt = DD.rcamt.copy().isAPartOf(acmvrec1);
	public PackedDecimalData frcdate = DD.frcdate.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData intextind = DD.intextind.copy().isAPartOf(acmvrec1);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(acmvrec1);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(acmvrec1);
	public PackedDecimalData user = DD.user.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData reconsbr = DD.reconsbr.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData suprflg = DD.suprflg.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData taxcode = DD.taxcode.copy().isAPartOf(acmvrec1);
	public PackedDecimalData reconref = DD.reconref.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData stmtsort = DD.stmtsort.copy().isAPartOf(acmvrec1);
	public PackedDecimalData creddte = DD.creddte.copy().isAPartOf(acmvrec1);
	public PackedDecimalData trandate = DD.trandate.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(acmvrec1);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(acmvrec1);
	private PackedDecimalData wsaaRrn = new PackedDecimalData(9, 0).isAPartOf(acmvrec1);
	private List<String> tr58tcrTableList = new ArrayList<String>();
	
	
	public Br58l() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		wsspEdterror.set(varcom.oK);
	
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tr58t);
		itemIO.setItemitem(bsprIO.getCompany());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr58trec.tr58tRec.set(itemIO.getGenarea());
		String tr58tcrtable;
		for(int i = 1 ; i < tr58trec.crtable.length;i++)
		{
			if(tr58trec.crtable[i] != null)
			{
				tr58tcrtable = tr58trec.crtable[i].trim();
				if(!"".equals(tr58tcrtable))
					{
						tr58tcrTableList.add(tr58tcrtable);
					}
			}
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		
		prepAcmv1000();
		
		sqlAcmvpf = " SELECT RDOCPFX, RDOCCOY, RDOCNUM, TRANNO, JRNSEQ, RLDGPFX, RLDGCOY, " +
				"RLDGACCT, SACSCODE, SACSTYP, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, " +
				"BATCTRCDE, BATCBATCH, ORIGCURR, ORIGAMT, TRANREF, TRANDESC, CRATE, " +
				"ACCTAMT, GENLCOY, GENLCUR, GLCODE, GLSIGN, POSTYEAR, POSTMONTH, " +
				"EFFDATE, DATETO, RCAMT, FRCDATE, INTEXTIND, TRDT, TRTM, USER_T, " +
				"TERMID, RECONSBR, SUPRFLG, TAXCODE, RECONREF, STMTSORT, CREDDTE, TRANDATE, " +
				"USRPRF, JOBNM, DATIME, UNIQUE_NUMBER " +
				" FROM   " + appVars.getTableNameOverriden("ACMVPF") + " " +
				" WHERE RLDGCOY  = ?" +
				" AND BATCACTYR = ?" +
				" AND BATCACTMN = ?" +
				" AND TRANNO <> ?" +
				" AND ((SACSCODE = ? AND SACSTYP = ? ) OR(SACSCODE = ? AND SACSTYP = ? )) " +
				" AND INTEXTIND = ? " +
				" ORDER BY RLDGCOY, RLDGACCT, RDOCNUM";
				
		sqlerrorflag = false;
		try {
			sqlAcmvpfConn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.fsu.general.dataaccess.AcmvTableDAM());
			sqlAcmvpfPS = appVars.prepareStatementEmbeded(sqlAcmvpfConn, sqlAcmvpf, "ACMVPF");
			appVars.setDBString(sqlAcmvpfPS, 1, bsprIO.getCompany());
			appVars.setDBString(sqlAcmvpfPS, 2, bsscIO.getAcctYear() );
			appVars.setDBString(sqlAcmvpfPS, 3, bsscIO.getAcctMonth());
			appVars.setDBString(sqlAcmvpfPS, 4, ZERO);
			appVars.setDBString(sqlAcmvpfPS, 5, t5645rec.sacscode01);
			appVars.setDBString(sqlAcmvpfPS, 6, t5645rec.sacstype01);
			appVars.setDBString(sqlAcmvpfPS, 7, t5645rec.sacscode02);
			appVars.setDBString(sqlAcmvpfPS, 8, t5645rec.sacstype02);
			appVars.setDBString(sqlAcmvpfPS, 9, "E");
			sqlAcmvpfRS = appVars.executeQuery(sqlAcmvpfPS);
			
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}		
	}

	protected void prepAcmv1000()
	{
		lifacmvrec1.lifacmvRec.set(SPACES);
		lifacmvrec1.function.set("PSTW");
		lifacmvrec1.batccoy.set(batcdorrec.company);
		lifacmvrec1.batcbrn.set(batcdorrec.branch);
		lifacmvrec1.batcactyr.set(batcdorrec.actyear);
		lifacmvrec1.batcactmn.set(batcdorrec.actmonth);
		lifacmvrec1.batctrcde.set(batcdorrec.trcde);
		lifacmvrec1.batcbatch.set(batcdorrec.batch);
		lifacmvrec1.effdate.set(bsscIO.getEffectiveDate());
		lifacmvrec1.tranno.set(ZERO);
		lifacmvrec1.jrnseq.set(ZERO);
		lifacmvrec1.rldgcoy.set(bsprIO.getCompany());
		readDesc();
		lifacmvrec1.trandesc.set(descIO.getLongdesc());
		lifacmvrec1.postyear.set(SPACE);
		lifacmvrec1.postmonth.set(SPACE);
		lifacmvrec1.rcamt.set(ZERO);
		lifacmvrec1.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec1.user.set(ZERO);
		lifacmvrec1.termid.set(SPACE);
		lifacmvrec1.suprflag.set(SPACE);
		lifacmvrec1.genlcoy.set(bsprIO.getCompany());
		lifacmvrec1.genlcur.set(SPACE);
		lifacmvrec1.transactionDate.set(ZERO);
		lifacmvrec1.transactionTime.set(ZERO);
		lifacmvrec1.threadNumber.set(ZERO);
	}

	protected void readDesc()
	{
		descIO.setDataKey(SPACES);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl("T1688");
		descIO.setDescitem(bprdIO.getAuthCode());
		descIO.setDescpfx("IT");
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc(SPACES);
		}
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}
	
protected void readFile2000()
	{
		
					readFile2010();
				
					endOfFile2080();
				
	}

protected void readFile2010()
	{
	/*SELECT RDOCPFX, RDOCCOY, RDOCNUM, TRANNO, JRNSEQ, RLDGPFX, RLDGCOY, " +
	"RLDGACCT, SACSCODE, SACSTYP, BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, " +
	"BATCTRCDE, BATCBATCH, ORIGCURR, ORIGAMT, TRANREF, TRANDESC, CRATE, " +
	"ACCTAMT, GENLCOY, GENLCUR, GLCODE, GLSIGN, POSTYEAR, POSTMONTH, " +
	"EFFDATE, DATETO, RCAMT, FRCDATE, INTEXTIND, TRDT, TRTM, USER_T, " +
	"TERMID, RECONSBR, SUPRFLG, TAXCODE, RECONREF, STMTSORT, CREDDTE, TRANDATE, " +
	"USRPRF, JOBNM, DATIME, UNIQUE_NUMBER
*/		sqlerrorflag = false;
		try {
			if (sqlAcmvpfRS.next()) {
				appVars.getDBObject(sqlAcmvpfRS, 1, rdocpfx);
				appVars.getDBObject(sqlAcmvpfRS, 2, rdoccoy);
				appVars.getDBObject(sqlAcmvpfRS, 3, rdocnum);
				appVars.getDBObject(sqlAcmvpfRS, 4, tranno);
				appVars.getDBObject(sqlAcmvpfRS, 5, jrnseq);
				appVars.getDBObject(sqlAcmvpfRS, 6, rldgpfx);
				appVars.getDBObject(sqlAcmvpfRS, 7, rldgcoy);
				appVars.getDBObject(sqlAcmvpfRS, 8, rldgacct);
				appVars.getDBObject(sqlAcmvpfRS, 9, sacscode);
				appVars.getDBObject(sqlAcmvpfRS, 10, sacstyp);
				appVars.getDBObject(sqlAcmvpfRS, 11, batccoy);
				appVars.getDBObject(sqlAcmvpfRS, 12, batcbrn);
				appVars.getDBObject(sqlAcmvpfRS, 13, batcactyr);
				appVars.getDBObject(sqlAcmvpfRS, 14, batcactmn);
				appVars.getDBObject(sqlAcmvpfRS, 15, batctrcde);
				appVars.getDBObject(sqlAcmvpfRS, 16, batcbatch);
				appVars.getDBObject(sqlAcmvpfRS, 17, origcurr);
				appVars.getDBObject(sqlAcmvpfRS, 18, origamt);
				appVars.getDBObject(sqlAcmvpfRS, 19, tranref);
				appVars.getDBObject(sqlAcmvpfRS, 20, trandesc);
				appVars.getDBObject(sqlAcmvpfRS, 21, crate);
				appVars.getDBObject(sqlAcmvpfRS, 22, acctamt);
				appVars.getDBObject(sqlAcmvpfRS, 23, genlcoy);
				appVars.getDBObject(sqlAcmvpfRS, 24, genlcur);
				appVars.getDBObject(sqlAcmvpfRS, 25, glcode);
				appVars.getDBObject(sqlAcmvpfRS, 26, glsign);
				appVars.getDBObject(sqlAcmvpfRS, 27, postyear);
				appVars.getDBObject(sqlAcmvpfRS, 28, postmonth);
				appVars.getDBObject(sqlAcmvpfRS, 29, effdate);
				appVars.getDBObject(sqlAcmvpfRS, 30, dateto);
				appVars.getDBObject(sqlAcmvpfRS, 31, rcamt);
				appVars.getDBObject(sqlAcmvpfRS, 32, frcdate);
				appVars.getDBObject(sqlAcmvpfRS, 33, intextind);
				appVars.getDBObject(sqlAcmvpfRS, 34, transactionDate);
				appVars.getDBObject(sqlAcmvpfRS, 35, transactionTime);
				appVars.getDBObject(sqlAcmvpfRS, 36, user);
				appVars.getDBObject(sqlAcmvpfRS, 37, termid);
				appVars.getDBObject(sqlAcmvpfRS, 38, reconsbr);
				appVars.getDBObject(sqlAcmvpfRS, 39, suprflg);
				appVars.getDBObject(sqlAcmvpfRS, 40, taxcode);
				appVars.getDBObject(sqlAcmvpfRS, 41, reconref);
				appVars.getDBObject(sqlAcmvpfRS, 42, stmtsort);
				appVars.getDBObject(sqlAcmvpfRS, 43, creddte);
				appVars.getDBObject(sqlAcmvpfRS, 44, trandate);
				appVars.getDBObject(sqlAcmvpfRS, 45, userProfile);
				appVars.getDBObject(sqlAcmvpfRS, 46, jobName);
				appVars.getDBObject(sqlAcmvpfRS, 47, datime);
				appVars.getDBObject(sqlAcmvpfRS, 48, wsaaRrn);
				//end
			}
			else {
				endOfFile2080();
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		wsspEdterror.set(Varcom.oK);
		
		if (isNE(wsaaRldgacct, rldgacct))
		{
			chkVldRm2600();
		}
		if (isNE(wsaaVldRmted, "Y")){
			wsspEdterror.set(SPACE);
		}
		
		if(tr58tcrTableList.contains(termid.toString().trim()))
		{
			wsspEdterror.set(SPACE);
		}	
		
		readChdrlif5200();
		for(int i=1; i<=30;i++){
			if (isEQ(tr58trec.chdrtype[i],chdrlifIO.getCnttype())){
				wsspEdterror.set(SPACE);
			}
		}
		
		compRcd2700();
		callDatacon3();
		
		compute(wsaaRgrAmt, 2).setRounded(div(mult(acctamt, zrgapf.getPrcent()),100));
		if(isEQ(wsaaRgrAmt, ZERO)){
			wsspEdterror.set(SPACE);
		}
		
		wsaaRldgacct.set(rldgacct);
	}

protected void callDatacon3()
	{
		datcon3rec.intDate1.set(wsaaComprcd);
		datcon3rec.intDate2.set(effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaYear.set(datcon3rec.freqFactor);
		if (isGTE(wsaaYear,2)){
			wsspEdterror.set(SPACE);
		}
		if (isLT(wsaaYear,1)){
			wsaaComInd.set("F");
		}
		else{
			wsaaComInd.set("S");
		}
	}

protected void chkVldRm2600()
	{
	
				chkVldRm2600_1();
			
	}

protected void chkVldRm2600_1()
	{
		wsaaVldRmted.set("N");
		zrgapf.setAgntcoy(rldgcoy.toString());
		zrgapf.setAgntnum(rldgacct.toString());
		/*zrgaIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zrgaIO);*/
		zrgapfData=zrgapfDao.getZrgapfRecord(zrgapf.getAgntcoy(), zrgapf.getAgntnum());
		if(zrgapfData!=null && isEQ(zrgapfData.getAgntcoy(),rldgcoy) 
				&& isEQ(zrgapfData.getAgntnum(),rldgacct) ){
			wsaaVldRmted.set("Y");
			zrgapf=zrgapfData;
			return;
		}
		
		aglfIO.setAgntcoy(rldgcoy);
		aglfIO.setAgntnum(rldgacct);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		while(isNE(wsaaVldRmted, "Y") 
				&& isEQ(aglfIO.getStatuz(), Varcom.oK) 
				&& isNE(aglfIO.getReportag(), SPACE)){
			zrgapf.setAgntcoy(aglfIO.getAgntcoy().toString());
			zrgapf.setAgntnum(aglfIO.getReportag().toString());
			zrgapfData=zrgapfDao.getZrgapfRecord(zrgapf.getAgntcoy(), zrgapf.getAgntnum());
			/*zrgaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, zrgaIO);*/
			if(zrgapfData!=null
					&& isEQ(zrgapfData.getAgntcoy(),aglfIO.getAgntcoy()) 
					&& isEQ(zrgapfData.getAgntnum(),aglfIO.getReportag()) ){
				wsaaVldRmted.set("Y");
				zrgapf=zrgapfData;
				return;
			}
			else
			{
				aglfIO.setAgntcoy(aglfIO.getAgntcoy());
				aglfIO.setAgntnum(aglfIO.getReportag());
				aglfIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, aglfIO);
				if (isNE(aglfIO.getStatuz(),varcom.oK)
				&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
					syserrrec.statuz.set(aglfIO.getStatuz());
					syserrrec.params.set(aglfIO.getParams());
					fatalError600();
				}
			}
		}
	}

protected void compRcd2700()
{
	
				compRcd2700_1();
			
		
}

protected void compRcd2700_1()
	{
		wsaaCrTable.set("SPACE"); 
		wsaaComprcd.set(chdrlifIO.getOccdate());

		if(wsaaPlnSfxA.isNumeric()){
			wsaaPlnSfx.set(wsaaPlnSfxA);
		}
		else
		{
			wsaaPlnSfx.set(ZERO);
		}
		covrIO.setDataArea(SPACES);
		covrIO.setChdrcoy(rldgcoy);
		covrIO.setChdrnum(rdocnum);
		covrIO.setLife(wsaaLife);
		covrIO.setCoverage(wsaaCover);
		covrIO.setRider(wsaaRider);
		covrIO.setPlanSuffix(wsaaPlnSfx);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covrIO);
		
/*		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}*/
		if(isEQ(covrIO.getStatuz(),Varcom.oK) ){
			wsaaComprcd.set(covrIO.getCrrcd());
			wsaaCrTable.set(covrIO.getCrtable());
			return;
		}
		
		covtIO.setDataArea(SPACES);
		covtIO.setChdrcoy(rldgcoy);
		covtIO.setChdrnum(rdocnum);
		covtIO.setLife(wsaaLife);
		covtIO.setCoverage(wsaaCover);
		covtIO.setRider(wsaaRider);
		covtIO.setPlanSuffix("9999");
		covtIO.setFormat(covtrec);
		covtIO.setFunction(varcom.begn);
		covtIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtIO.setFitKeysSearch("CHDRCOY","CHDRNUM","LIFE","COVERAGE","RIDER");
		SmartFileCode.execute(appVars, covtIO);
	/*	if (isNE(covtIO.getStatuz(),varcom.oK)
		&& isNE(covtIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtIO.getParams());
			fatalError600();
		}*/
		if(isEQ(covtIO.getStatuz(),Varcom.oK) 
				&& isEQ(covtIO.getChdrcoy(),rldgcoy) 
				&& isEQ(covtIO.getChdrnum(),rdocnum) 
				&& isEQ(covtIO.getLife(),wsaaLife) 
				&& isEQ(covtIO.getCoverage(),wsaaCover) 
				&& isEQ(covtIO.getRider(), wsaaRider)){
			wsaaComprcd.set(covtIO.getEffdate());
			wsaaCrTable.set(covtIO.getCrtable());
			return;
		}
	}

protected void update3000()
	{
	//For each valid SQL-ACMVPF record: query
		zrgrpf.setAgntcoy(rldgcoy.toString());
		zrgrpf.setZrecruit(zrgapf.getZrecruit());
		zrgrpf.setAgntnum(zrgapf.getAgntnum());
		zrgrpf.setBatcactyr(bsscIO.getAcctYear().getbigdata());
		zrgrpf.setBatcactmn(bsscIO.getAcctMonth().getbigdata());
		zrgrpf.setAgtype(zrgapf.getMlagttyp());
		zrgrpf.setChdrnum(rdocnum.toString());
		zrgrpf.setLife(wsaaLife.toString());
		zrgrpf.setCoverage(wsaaCover.toString());
		zrgrpf.setRider(wsaaRider.toString());
		zrgrpf.setEffdate(effdate.getbigdata());
		zrgrpf.setTranno(tranno.getbigdata());
		zrgrpf.setAcctamt(acctamt.getbigdata());
		zrgrpf.setOrigcurr(origcurr.toString());
		zrgrpf.setComind(wsaaComInd.toString());
		zrgrpf.setBonusamt(wsaaRgrAmt.getbigdata());
		zrgrpf.setPrcdate(bsscIO.getEffectiveDate().getbigdata());
		zrgrpf.setJobno(bsprIO.getScheduleNumber().getbigdata());
	     int a = zrgrpfDao.insertZrgrData(zrgrpf);
		/*zrgrIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, zrgrIO);
		if (isNE(zrgrIO.getStatuz(),"****")) {
			syserrrec.params.set(zrgrIO.getParams());
			sqlError500();
		}*/
		
		lifacmvrec1.rdocnum.set(rdocnum);
		lifacmvrec1.jrnseq.add(1);
		lifacmvrec1.sacscode.set(t5645rec.sacscode03);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype03);
		lifacmvrec1.glcode.set(t5645rec.glmap03);
		lifacmvrec1.glsign.set(t5645rec.sign03);
		lifacmvrec1.contot.set(t5645rec.cnttot03);
		lifacmvrec1.origcurr.set(genlcur);
		lifacmvrec1.origamt.set(wsaaRgrAmt);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.substituteCode[06].set(wsaaCrTable); 
		String tempRldgacct = zrgrpf.getChdrnum() + wsaaLife + wsaaCover + wsaaRider + "00";/* IJTI-1523 */
		lifacmvrec1.rldgacct.set(tempRldgacct);
		
		lifacmvrec1.tranref.set(zrgrpf.getZrecruit());
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		lifacmvrec1.ind.set("D");
		lifacmvrec1.prefix.set("CH");
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		lifacmvrec1.ind.set("D");
		lifacmvrec1.prefix.set("CH");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
		
		lifacmvrec1.jrnseq.add(1);
		lifacmvrec1.sacscode.set(t5645rec.sacscode04);
		lifacmvrec1.sacstyp.set(t5645rec.sacstype04);
		lifacmvrec1.glcode.set(t5645rec.glmap04);
		lifacmvrec1.glsign.set(t5645rec.sign04);
		lifacmvrec1.contot.set(t5645rec.cnttot04);
		lifacmvrec1.origcurr.set(genlcur);
		lifacmvrec1.origamt.set(wsaaRgrAmt);
		lifacmvrec1.crate.set(ZERO);
		lifacmvrec1.acctamt.set(ZERO);
		lifacmvrec1.substituteCode[06].set(wsaaCrTable); 
		lifacmvrec1.rldgacct.set(zrgrpf.getZrecruit());
		String tempTranref = zrgrpf.getChdrnum() + wsaaLife + wsaaCover + wsaaRider + "00";/* IJTI-1523 */
		lifacmvrec1.tranref.set(tempTranref);
		// Modified by Vibhor Khare for Ticket #TMLII-296 [AC-01-009 Provide GL transaction file for manual Interfund Journal to SUN Account]
		lifacmvrec1.ind.set("D");
		lifacmvrec1.prefix.set("CH");
		callProgram(Lifacmv.class, lifacmvrec1.lifacmvRec);
		if (isNE(lifacmvrec1.statuz,varcom.oK)) {
			syserrrec.params.set(lifacmvrec1.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec1.statuz);
			fatalError600();
		}
	}


protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlAcmvpfConn, sqlAcmvpfPS, sqlAcmvpfRS);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readChdrlif5200()
	{
		/*START*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrpfx("CH");
		chdrlifIO.setChdrcoy(rldgcoy);
		chdrlifIO.setChdrnum(rdocnum);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}


}
