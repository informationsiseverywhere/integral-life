package com.csc.life.agents.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:14:55
 * Description:
 * Copybook name: T5564REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5564rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5564Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData freqs = new FixedLengthStringData(24).isAPartOf(t5564Rec, 0);
  	public FixedLengthStringData[] freq = FLSArrayPartOfStructure(12, 2, freqs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(freqs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData freq01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData freq02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData freq03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData freq04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData freq05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData freq06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData freq07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData freq08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData freq09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData freq10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData freq11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData freq12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData itmkeys = new FixedLengthStringData(96).isAPartOf(t5564Rec, 24);
  	public FixedLengthStringData[] itmkey = FLSArrayPartOfStructure(12, 8, itmkeys, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(96).isAPartOf(itmkeys, 0, FILLER_REDEFINE);
  	public FixedLengthStringData itmkey01 = new FixedLengthStringData(8).isAPartOf(filler1, 0);
  	public FixedLengthStringData itmkey02 = new FixedLengthStringData(8).isAPartOf(filler1, 8);
  	public FixedLengthStringData itmkey03 = new FixedLengthStringData(8).isAPartOf(filler1, 16);
  	public FixedLengthStringData itmkey04 = new FixedLengthStringData(8).isAPartOf(filler1, 24);
  	public FixedLengthStringData itmkey05 = new FixedLengthStringData(8).isAPartOf(filler1, 32);
  	public FixedLengthStringData itmkey06 = new FixedLengthStringData(8).isAPartOf(filler1, 40);
  	public FixedLengthStringData itmkey07 = new FixedLengthStringData(8).isAPartOf(filler1, 48);
  	public FixedLengthStringData itmkey08 = new FixedLengthStringData(8).isAPartOf(filler1, 56);
  	public FixedLengthStringData itmkey09 = new FixedLengthStringData(8).isAPartOf(filler1, 64);
  	public FixedLengthStringData itmkey10 = new FixedLengthStringData(8).isAPartOf(filler1, 72);
  	public FixedLengthStringData itmkey11 = new FixedLengthStringData(8).isAPartOf(filler1, 80);
  	public FixedLengthStringData itmkey12 = new FixedLengthStringData(8).isAPartOf(filler1, 88);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(380).isAPartOf(t5564Rec, 120, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5564Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5564Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}