package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5181
 * @version 1.0 generated on 11/22/13 6:32 AM
 * @author CSC
 */
public class S5181ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(351);
	public FixedLengthStringData dataFields = new FixedLengthStringData(143).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,13);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,43);
	public ZonedDecimalData instprem = DD.instprem.copyToZonedDecimal().isAPartOf(dataFields,51);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields,70);
	public ZonedDecimalData numpols = DD.numpols.copyToZonedDecimal().isAPartOf(dataFields,78);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,82);
	public ZonedDecimalData planSuffix = DD.plnsfx.copyToZonedDecimal().isAPartOf(dataFields,129);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,133);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,141);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 143);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData instpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData numpolsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData plnsfxErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 195);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] instpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] numpolsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] plnsfxOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(141);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(43).isAPartOf(subfileArea, 0);
	public ZonedDecimalData fundAmount = DD.fundamnt.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData hseqno = DD.hseqno.copyToZonedDecimal().isAPartOf(subfileFields,17);
	public FixedLengthStringData updteflag = DD.updteflag.copy().isAPartOf(subfileFields,20);
	public FixedLengthStringData unitVirtualFund = DD.vrtfnd.copy().isAPartOf(subfileFields,21);
	public ZonedDecimalData zcurprmbal = DD.zcurprmbal.copyToZonedDecimal().isAPartOf(subfileFields,25);
	public FixedLengthStringData zrectyp = DD.zrectyp.copy().isAPartOf(subfileFields,42);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 43);
	public FixedLengthStringData fundamntErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hseqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData updteflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData vrtfndErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData zcurprmbalErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData zrectypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 67);
	public FixedLengthStringData[] fundamntOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hseqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] updteflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] vrtfndOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] zcurprmbalOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] zrectypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 139);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5181screensflWritten = new LongData(0);
	public LongData S5181screenctlWritten = new LongData(0);
	public LongData S5181screenWritten = new LongData(0);
	public LongData S5181protectWritten = new LongData(0);
	public GeneralTable s5181screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5181screensfl;
	}

	public S5181ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(vrtfndOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zrectypOut,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fundamntOut,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zcurprmbalOut,new String[] {"30",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plnsfxOut,new String[] {null, null, "05",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {updteflag, unitVirtualFund, zrectyp, fundAmount, zcurprmbal, hseqno};
		screenSflOutFields = new BaseData[][] {updteflagOut, vrtfndOut, zrectypOut, fundamntOut, zcurprmbalOut, hseqnoOut};
		screenSflErrFields = new BaseData[] {updteflagErr, vrtfndErr, zrectypErr, fundamntErr, zcurprmbalErr, hseqnoErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {crtabdesc, chdrnum, life, coverage, rider, lifenum, ownername, ptdate, numpols, cntcurr, effdate, planSuffix, instprem};
		screenOutFields = new BaseData[][] {crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifenumOut, ownernameOut, ptdateOut, numpolsOut, cntcurrOut, effdateOut, plnsfxOut, instpremOut};
		screenErrFields = new BaseData[] {crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifenumErr, ownernameErr, ptdateErr, numpolsErr, cntcurrErr, effdateErr, plnsfxErr, instpremErr};
		screenDateFields = new BaseData[] {ptdate, effdate};
		screenDateErrFields = new BaseData[] {ptdateErr, effdateErr};
		screenDateDispFields = new BaseData[] {ptdateDisp, effdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5181screen.class;
		screenSflRecord = S5181screensfl.class;
		screenCtlRecord = S5181screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5181protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5181screenctl.lrec.pageSubfile);
	}
}
