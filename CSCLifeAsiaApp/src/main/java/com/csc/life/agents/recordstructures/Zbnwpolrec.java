package com.csc.life.agents.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:14
 * Description:
 * Copybook name: ZBNWPOLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zbnwpolrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData bnwpolRec = new FixedLengthStringData(719);
  	public FixedLengthStringData bnwpolTrcde = new FixedLengthStringData(3).isAPartOf(bnwpolRec, 0);
  	public FixedLengthStringData bnwpolSource = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 3);
  	public FixedLengthStringData bnwpolCompany = new FixedLengthStringData(3).isAPartOf(bnwpolRec, 13);
  	public FixedLengthStringData bnwpolChdrnum = new FixedLengthStringData(30).isAPartOf(bnwpolRec, 16);
  	public FixedLengthStringData bnwpolPlanid = new FixedLengthStringData(15).isAPartOf(bnwpolRec, 46);
  	public FixedLengthStringData bnwpolPlandesc = new FixedLengthStringData(80).isAPartOf(bnwpolRec, 61);
  	public FixedLengthStringData bnwpolAgntnum = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 141);
  	public FixedLengthStringData bnwpolGrpcde = new FixedLengthStringData(1).isAPartOf(bnwpolRec, 151);
  	public FixedLengthStringData bnwpolGrpind = new FixedLengthStringData(1).isAPartOf(bnwpolRec, 152);
  	public FixedLengthStringData bnwpolPlnefdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 153);
  	public FixedLengthStringData bnwpolUwdcdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 163);
  	public FixedLengthStringData bnwpolBclntnum = new FixedLengthStringData(1).isAPartOf(bnwpolRec, 173);
  	public FixedLengthStringData bnwpolCownnum = new FixedLengthStringData(15).isAPartOf(bnwpolRec, 174);
  	public FixedLengthStringData bnwpolStatdesc = new FixedLengthStringData(25).isAPartOf(bnwpolRec, 189);
  	public FixedLengthStringData bnwpolOccdate = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 214);
  	public FixedLengthStringData bnwpolTrmdate = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 224);
  	public FixedLengthStringData bnwpolPeriod = new FixedLengthStringData(6).isAPartOf(bnwpolRec, 234);
  	public FixedLengthStringData bnwpolApplndte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 240);
  	public FixedLengthStringData bnwpolIssage = new FixedLengthStringData(3).isAPartOf(bnwpolRec, 250);
  	public FixedLengthStringData bnwpolPayoptn = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 253);
  	public FixedLengthStringData bnwpolPeriodc = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 263);
  	public FixedLengthStringData bnwpolOcmscde = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 273);
  	public FixedLengthStringData bnwpolComoptn = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 283);
  	public FixedLengthStringData bnwpolInstprem = new FixedLengthStringData(13).isAPartOf(bnwpolRec, 293);
  	public FixedLengthStringData bnwpolAnnprem = new FixedLengthStringData(13).isAPartOf(bnwpolRec, 306);
  	public FixedLengthStringData bnwpolState = new FixedLengthStringData(5).isAPartOf(bnwpolRec, 319);
  	public FixedLengthStringData bnwpolBillfreq = new FixedLengthStringData(5).isAPartOf(bnwpolRec, 324);
  	public FixedLengthStringData bnwpolParticipation = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 329);
  	public FixedLengthStringData bnwpolPension = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 339);
  	public FixedLengthStringData bnwpolQualification = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 349);
  	public FixedLengthStringData bnwpolTarget = new FixedLengthStringData(1).isAPartOf(bnwpolRec, 359);
  	public FixedLengthStringData bnwpolReptype = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 360);
  	public FixedLengthStringData bnwpolInsnum = new FixedLengthStringData(60).isAPartOf(bnwpolRec, 370);
  	public FixedLengthStringData bnwpolInsname = new FixedLengthStringData(80).isAPartOf(bnwpolRec, 430);
  	public FixedLengthStringData bnwpolInssrdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 510);
  	public FixedLengthStringData bnwpolResident = new FixedLengthStringData(3).isAPartOf(bnwpolRec, 520);
  	public FixedLengthStringData bnwpolJinsname = new FixedLengthStringData(80).isAPartOf(bnwpolRec, 523);
  	public FixedLengthStringData bnwpolJinssrdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 603);
  	public FixedLengthStringData bnwpolJinstmdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 613);
  	public FixedLengthStringData bnwpolIssstate = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 623);
  	public FixedLengthStringData bnwpolProdcode = new FixedLengthStringData(30).isAPartOf(bnwpolRec, 633);
  	public FixedLengthStringData bnwpolCwa = new FixedLengthStringData(13).isAPartOf(bnwpolRec, 663);
  	public FixedLengthStringData bnwpolRollamt = new FixedLengthStringData(13).isAPartOf(bnwpolRec, 676);
  	public FixedLengthStringData bnwpolIssdte = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 689);
  	public FixedLengthStringData bnwpolTrandate = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 699);
  	public FixedLengthStringData bnwpolProcdate = new FixedLengthStringData(10).isAPartOf(bnwpolRec, 709);


	public void initialize() {
		COBOLFunctions.initialize(bnwpolRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		bnwpolRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}