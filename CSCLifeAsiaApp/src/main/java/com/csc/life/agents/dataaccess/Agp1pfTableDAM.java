package com.csc.life.agents.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: Agp1pfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:45
 * Class transformed from AGP1PF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agp1pfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 250;
	public FixedLengthStringData agp1rec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agp1pfRecord = agp1rec;
	
	public FixedLengthStringData textfld = DD.textfld.copy().isAPartOf(agp1rec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public Agp1pfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for Agp1pfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public Agp1pfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for Agp1pfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public Agp1pfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for Agp1pfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public Agp1pfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGP1PF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"TEXTFLD, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     textfld,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		textfld.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgp1rec() {
  		return agp1rec;
	}

	public FixedLengthStringData getAgp1pfRecord() {
  		return agp1pfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgp1rec(what);
	}

	public void setAgp1rec(Object what) {
  		this.agp1rec.set(what);
	}

	public void setAgp1pfRecord(Object what) {
  		this.agp1pfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agp1rec.getLength());
		result.set(agp1rec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}