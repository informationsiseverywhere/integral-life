/*

 * File: AgpbpfDAOImpl.java
 * Date: August 16, 2019
 * Author: DXC
 * Created by: sbatra9
 * 
 * Copyright DXC, all rights reserved.
 */

package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.agents.dataaccess.dao.AgpbpfDAO;
import com.csc.life.agents.dataaccess.model.Agpbpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgpbpfDAOImpl extends BaseDAOImpl<Agpbpf> implements AgpbpfDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(AgpbpfDAOImpl.class);

	public void insertAgpbpfRecord(Agpbpf agpbpf) {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO AGPBPF(RLDGCOY, RLDGACCT, SACSCURBAL, PROCFLG, TAGSUSIND, TAXCDE, CURRCODE, AGCCQIND, PAYCLT, FACTHOUS, ");
		sb.append(" BANKKEY,BANKACCKEY, WHTAX, TCOLBAL, TDEDUCT, TDCCHRG, PAYMTH, JOBNM, USRPRF, DATIME)");
		sb.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		try {
			ps.setString(1, agpbpf.getRldgcoy());
			ps.setString(2, agpbpf.getRldgacct());
			ps.setBigDecimal(3, agpbpf.getSacscurbal());
			ps.setString(4, agpbpf.getProcflg());
			ps.setString(5, agpbpf.getTagsusind());
			ps.setString(6, agpbpf.getTaxcde());
			ps.setString(7, agpbpf.getCurrcode());
			ps.setString(8, agpbpf.getAgccqind());
			ps.setString(9, agpbpf.getPayclt());
			ps.setString(10, agpbpf.getFacthous());
			ps.setString(11, agpbpf.getBankkey());
			ps.setString(12, agpbpf.getBankacckey());
			ps.setBigDecimal(13, agpbpf.getWhtax());
			ps.setBigDecimal(14, agpbpf.getTcolbal());
			ps.setBigDecimal(15, agpbpf.getTdeduct());
			ps.setBigDecimal(16, agpbpf.getTdcchrg());
			ps.setString(17, agpbpf.getPaymth());
			ps.setString(18, getUsrprf());
			ps.setString(19, getJobnm());
			ps.setTimestamp(20,new Timestamp(System.currentTimeMillis()));
			
			ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.error("insertAgpbpfRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
		}
	}
