package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR521
 * @version 1.0 generated on 30/08/09 07:18
 * @author Quipoz
 */
public class Sr521ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(800);
	public FixedLengthStringData dataFields = new FixedLengthStringData(208).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData contitem = DD.contitem.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,9);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,17);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,25);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData zcomcodes = new FixedLengthStringData(40).isAPartOf(dataFields, 68);
	public FixedLengthStringData[] zcomcode = FLSArrayPartOfStructure(10, 4, zcomcodes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(zcomcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData zcomcode01 = DD.zcomcode.copy().isAPartOf(filler,0);
	public FixedLengthStringData zcomcode02 = DD.zcomcode.copy().isAPartOf(filler,4);
	public FixedLengthStringData zcomcode03 = DD.zcomcode.copy().isAPartOf(filler,8);
	public FixedLengthStringData zcomcode04 = DD.zcomcode.copy().isAPartOf(filler,12);
	public FixedLengthStringData zcomcode05 = DD.zcomcode.copy().isAPartOf(filler,16);
	public FixedLengthStringData zcomcode06 = DD.zcomcode.copy().isAPartOf(filler,20);
	public FixedLengthStringData zcomcode07 = DD.zcomcode.copy().isAPartOf(filler,24);
	public FixedLengthStringData zcomcode08 = DD.zcomcode.copy().isAPartOf(filler,28);
	public FixedLengthStringData zcomcode09 = DD.zcomcode.copy().isAPartOf(filler,32);
	public FixedLengthStringData zcomcode10 = DD.zcomcode.copy().isAPartOf(filler,36);
	public FixedLengthStringData zrsppercs = new FixedLengthStringData(50).isAPartOf(dataFields, 108);
	public ZonedDecimalData[] zrspperc = ZDArrayPartOfStructure(10, 5, 2, zrsppercs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(50).isAPartOf(zrsppercs, 0, FILLER_REDEFINE);
	public ZonedDecimalData zrspperc01 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData zrspperc02 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,5);
	public ZonedDecimalData zrspperc03 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData zrspperc04 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData zrspperc05 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,20);
	public ZonedDecimalData zrspperc06 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,25);
	public ZonedDecimalData zrspperc07 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData zrspperc08 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,35);
	public ZonedDecimalData zrspperc09 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,40);
	public ZonedDecimalData zrspperc10 = DD.zrspperc.copyToZonedDecimal().isAPartOf(filler1,45);
	public FixedLengthStringData zrtupercs = new FixedLengthStringData(50).isAPartOf(dataFields, 158);
	public ZonedDecimalData[] zrtuperc = ZDArrayPartOfStructure(10, 5, 2, zrtupercs, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(zrtupercs, 0, FILLER_REDEFINE);
	public ZonedDecimalData zrtuperc01 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData zrtuperc02 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,5);
	public ZonedDecimalData zrtuperc03 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,10);
	public ZonedDecimalData zrtuperc04 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData zrtuperc05 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,20);
	public ZonedDecimalData zrtuperc06 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,25);
	public ZonedDecimalData zrtuperc07 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData zrtuperc08 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,35);
	public ZonedDecimalData zrtuperc09 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,40);
	public ZonedDecimalData zrtuperc10 = DD.zrtuperc.copyToZonedDecimal().isAPartOf(filler2,45);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(148).isAPartOf(dataArea, 208);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData contitemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData zcomcodesErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] zcomcodeErr = FLSArrayPartOfStructure(10, 4, zcomcodesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(zcomcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zcomcode01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData zcomcode02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData zcomcode03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData zcomcode04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData zcomcode05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData zcomcode06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData zcomcode07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData zcomcode08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData zcomcode09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData zcomcode10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData zrsppercsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData[] zrsppercErr = FLSArrayPartOfStructure(10, 4, zrsppercsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(40).isAPartOf(zrsppercsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrspperc01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData zrspperc02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData zrspperc03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData zrspperc04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData zrspperc05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData zrspperc06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData zrspperc07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData zrspperc08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData zrspperc09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData zrspperc10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData zrtupercsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData[] zrtupercErr = FLSArrayPartOfStructure(10, 4, zrtupercsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(zrtupercsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zrtuperc01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData zrtuperc02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData zrtuperc03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData zrtuperc04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData zrtuperc05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData zrtuperc06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData zrtuperc07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData zrtuperc08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData zrtuperc09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData zrtuperc10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(444).isAPartOf(dataArea, 356);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] contitemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData zcomcodesOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] zcomcodeOut = FLSArrayPartOfStructure(10, 12, zcomcodesOut, 0);
	public FixedLengthStringData[][] zcomcodeO = FLSDArrayPartOfArrayStructure(12, 1, zcomcodeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(zcomcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zcomcode01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] zcomcode02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] zcomcode03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] zcomcode04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] zcomcode05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] zcomcode06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] zcomcode07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] zcomcode08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] zcomcode09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] zcomcode10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData zrsppercsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 204);
	public FixedLengthStringData[] zrsppercOut = FLSArrayPartOfStructure(10, 12, zrsppercsOut, 0);
	public FixedLengthStringData[][] zrsppercO = FLSDArrayPartOfArrayStructure(12, 1, zrsppercOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(120).isAPartOf(zrsppercsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrspperc01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] zrspperc02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] zrspperc03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] zrspperc04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] zrspperc05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] zrspperc06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] zrspperc07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] zrspperc08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] zrspperc09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] zrspperc10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData zrtupercsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 324);
	public FixedLengthStringData[] zrtupercOut = FLSArrayPartOfStructure(10, 12, zrtupercsOut, 0);
	public FixedLengthStringData[][] zrtupercO = FLSDArrayPartOfArrayStructure(12, 1, zrtupercOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(120).isAPartOf(zrtupercsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zrtuperc01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] zrtuperc02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] zrtuperc03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] zrtuperc04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] zrtuperc05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] zrtuperc06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] zrtuperc07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] zrtuperc08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] zrtuperc09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] zrtuperc10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sr521screenWritten = new LongData(0);
	public LongData Sr521protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr521ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, zcomcode01, zrspperc01, zrtuperc01, zcomcode02, zcomcode03, zrspperc03, zrtuperc03, zrspperc02, zrtuperc02, contitem, zcomcode04, zrspperc04, zrtuperc04, zcomcode05, zrspperc05, zrtuperc05, zcomcode06, zrspperc06, zrtuperc06, zcomcode07, zrspperc07, zrtuperc07, zcomcode08, zrspperc08, zrtuperc08, zcomcode09, zrspperc09, zrtuperc09, zcomcode10, zrspperc10, zrtuperc10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, zcomcode01Out, zrspperc01Out, zrtuperc01Out, zcomcode02Out, zcomcode03Out, zrspperc03Out, zrtuperc03Out, zrspperc02Out, zrtuperc02Out, contitemOut, zcomcode04Out, zrspperc04Out, zrtuperc04Out, zcomcode05Out, zrspperc05Out, zrtuperc05Out, zcomcode06Out, zrspperc06Out, zrtuperc06Out, zcomcode07Out, zrspperc07Out, zrtuperc07Out, zcomcode08Out, zrspperc08Out, zrtuperc08Out, zcomcode09Out, zrspperc09Out, zrtuperc09Out, zcomcode10Out, zrspperc10Out, zrtuperc10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, zcomcode01Err, zrspperc01Err, zrtuperc01Err, zcomcode02Err, zcomcode03Err, zrspperc03Err, zrtuperc03Err, zrspperc02Err, zrtuperc02Err, contitemErr, zcomcode04Err, zrspperc04Err, zrtuperc04Err, zcomcode05Err, zrspperc05Err, zrtuperc05Err, zcomcode06Err, zrspperc06Err, zrtuperc06Err, zcomcode07Err, zrspperc07Err, zrtuperc07Err, zcomcode08Err, zrspperc08Err, zrtuperc08Err, zcomcode09Err, zrspperc09Err, zrtuperc09Err, zcomcode10Err, zrspperc10Err, zrtuperc10Err};
		screenDateFields = new BaseData[] {itmfrm, itmto};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr521screen.class;
		protectRecord = Sr521protect.class;
	}

}
