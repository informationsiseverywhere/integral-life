package com.csc.life.agents.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:06
 * Description:
 * Copybook name: AGLFCHGKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Aglfchgkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData aglfchgFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData aglfchgKey = new FixedLengthStringData(256).isAPartOf(aglfchgFileKey, 0, REDEFINE);
  	public FixedLengthStringData aglfchgAgntcoy = new FixedLengthStringData(1).isAPartOf(aglfchgKey, 0);
  	public FixedLengthStringData aglfchgAgntnum = new FixedLengthStringData(8).isAPartOf(aglfchgKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(aglfchgKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(aglfchgFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		aglfchgFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}