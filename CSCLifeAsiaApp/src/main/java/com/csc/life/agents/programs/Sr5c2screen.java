package com.csc.life.agents.programs;

import com.csc.life.agents.screens.Sr5c2ScreenVars;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5c2screen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {10}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5c2ScreenVars sv = (Sr5c2ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr5c2screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr5c2ScreenVars screenVars = (Sr5c2ScreenVars)pv;
		screenVars.cnfrm.setClassString("");
		}

/**
 * Clear all the variables in Msgboxscreen
 */
	public static void clear(VarModel pv) {
		Sr5c2ScreenVars screenVars = (Sr5c2ScreenVars) pv;
		screenVars.cnfrm.clear();
		
	}


}
