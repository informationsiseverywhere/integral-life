package com.csc.life.agents.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S1673 Date: 03 February 2020 Author: vdivisala
 */
public class S1673ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(143);
	public FixedLengthStringData dataFields = new FixedLengthStringData(47).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 6);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 14);
	public FixedLengthStringData cmgwmop = DD.cmgwmop.copy().isAPartOf(dataFields, 44);
	public FixedLengthStringData cmgwspltyp = DD.cmgwspltyp.copy().isAPartOf(dataFields, 45);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 47);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cmgwmopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cmgwspltypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 71);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cmgwmopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cmgwspltypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData S1673screenWritten = new LongData(0);
	public LongData S1673protectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public S1673ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected final void initialiseScreenVars() {
		fieldIndMap.put(cmgwmopOut, new String[] { "05", "25", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(cmgwspltypOut, new String[] { "06", "26", "-06", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, cmgwmop, cmgwspltyp };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, cmgwmopOut, cmgwspltypOut };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, cmgwmopErr, cmgwspltypErr };
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S1673screen.class;
		protectRecord = S1673protect.class;
	}
}