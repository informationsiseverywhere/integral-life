package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZmbwpfDAO;
import com.csc.life.agents.dataaccess.model.Zmbwpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZmbwpfDAOImpl extends BaseDAOImpl<Zmbwpf> implements ZmbwpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZmbwpfDAOImpl.class);

	@Override
	public Zmbwpf getZmbwpf(String Agntcoy, String Agntnum) {

		String query = new String(
				"SELECT AGNTCOY,AGNTNUM,BATCACTMN,BATCACTYR,COMTOT,DATIME,GENLCUR,JOBNAME,PRCENT,USERPROFILE FROM ZMBWPF WHERE AGNTCOY=? AND AGNTNUM=?");

		PreparedStatement stmt = null;
		ResultSet rs = null;
		Zmbwpf zmbwpf = null;

		try {
			stmt = getPrepareStatement(query);/* IJTI-1523 */
			stmt.setString(1, Agntcoy);
			stmt.setString(2, Agntnum);

			rs = stmt.executeQuery();

			while (rs.next()) {
				zmbwpf = new Zmbwpf();
				zmbwpf.setAgntcoy(rs.getString(1));
				zmbwpf.setAgntnum(rs.getString(2));
				zmbwpf.setBatcactmn(rs.getBigDecimal(3));
				zmbwpf.setBatcactyr(rs.getBigDecimal(4));
				zmbwpf.setComtot(rs.getBigDecimal(5));
				zmbwpf.setDatime(rs.getString(6));
				zmbwpf.setGenlcur(rs.getString(7));
				zmbwpf.setJobName(rs.getString(8));
				zmbwpf.setPrcent(rs.getBigDecimal(9));
				zmbwpf.setUserProfile(rs.getString(10));
			}

		} catch (SQLException e) {
			LOGGER.error("getZmbwpf()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		return zmbwpf;
	}
}
