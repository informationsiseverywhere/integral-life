package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 11/22/13 6:31 AM
 * @author CSC
 */
public class Sr52uscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr52uScreenVars sv = (Sr52uScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr52uscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr52uScreenVars screenVars = (Sr52uScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.rstate.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.chdrtype01.setClassString("");
		screenVars.descrip01.setClassString("");
		screenVars.chdrtype02.setClassString("");
		screenVars.descrip02.setClassString("");
	}

/**
 * Clear all the variables in Sr52uscreen
 */
	public static void clear(VarModel pv) {
		Sr52uScreenVars screenVars = (Sr52uScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cntcurr.clear();
		screenVars.rstate.clear();
		screenVars.register.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.chdrtype01.clear();
		screenVars.descrip01.clear();
		screenVars.chdrtype02.clear();
		screenVars.descrip02.clear();
	}
}
