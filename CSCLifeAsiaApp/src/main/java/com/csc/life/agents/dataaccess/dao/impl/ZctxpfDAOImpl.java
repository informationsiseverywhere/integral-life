package com.csc.life.agents.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.math.BigDecimal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import java.util.ArrayList;
import java.util.List;
public class ZctxpfDAOImpl extends BaseDAOImpl<Zctxpf>implements ZctxpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(ZctxpfDAOImpl.class);
	
	public Zctxpf readRecord(String chdrcoy,String chdrnum){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From ZCTXPF where CHDRCOY=? and CHDRNUM=? ORDER BY DATEFRM");
		Zctxpf zctxpfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		  ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    	zctxpfRec = new Zctxpf();
			    zctxpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    zctxpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    zctxpfRec.setClntnum(rs.getString("CLNTNUM"));
			    zctxpfRec.setDatefrm(rs.getInt("DATEFRM"));
			    zctxpfRec.setDateto(rs.getInt("DATETO"));
			    zctxpfRec.setZsgtpct(rs.getBigDecimal("ZSGTPCT"));
			    zctxpfRec.setZoerpct(rs.getBigDecimal("ZOERPCT"));
			    zctxpfRec.setZdedpct(rs.getBigDecimal("ZDEDPCT"));
			    zctxpfRec.setZundpct(rs.getBigDecimal("ZUNDPCT"));
			    zctxpfRec.setZspspct(rs.getBigDecimal("ZSPSPCT"));
			    zctxpfRec.setTotprcnt(rs.getBigDecimal("TOTPRCNT"));
			    zctxpfRec.setAmount(rs.getBigDecimal("AMOUNT"));
			    zctxpfRec.setZslryspct(rs.getBigDecimal("ZSLRYSPCT")); //ILIFE-7345 
			    zctxpfRec.setZslrysamt(rs.getBigDecimal("ZSLRYSAMT")); //ILIFE-7345 
				}
		}
			catch(SQLException e)
			{
				LOGGER.error(" ZctxpfDAOImpl.readZctxpfData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);	
			}
			
			return zctxpfRec;

		
	}
	
	public boolean updateRecord(Zctxpf zctxpf){
		
		StringBuilder sql_zctxpf_update = new StringBuilder("");
			sql_zctxpf_update.append("UPDATE ZCTXPF  ");
			sql_zctxpf_update.append("SET ");		 
			sql_zctxpf_update.append("ZSGTPCT=?,ZOERPCT=?,ZDEDPCT=?,ZUNDPCT=?,ZSPSPCT=?,ZSGTAMT=?,ZOERAMT=?,ZDEDAMT=?,ZUNDAMT=?,ZSPSAMT=?,ZSLRYSPCT=?,ZSLRYSAMT=?,USRPRF=?,JOBNM=?,DATIME=? ");
			sql_zctxpf_update.append("WHERE CHDRNUM='"+zctxpf.getChdrnum()+"'");
			sql_zctxpf_update.append("AND CHDRCOY='"+zctxpf.getChdrcoy()+"'");
			sql_zctxpf_update.append("AND DATEFRM='"+zctxpf.getDatefrm()+"'");
			PreparedStatement psUpdate = null;
			boolean isUpdated = false;
			try{
				psUpdate = getPrepareStatement(sql_zctxpf_update.toString());
				
				psUpdate.setBigDecimal(1, zctxpf.getZsgtpct());
				psUpdate.setBigDecimal(2, zctxpf.getZoerpct());
				psUpdate.setBigDecimal(3, zctxpf.getZdedpct());
				psUpdate.setBigDecimal(4, zctxpf.getZundpct());
				psUpdate.setBigDecimal(5, zctxpf.getZspspct());
				psUpdate.setBigDecimal(6, zctxpf.getZsgtamt());
				psUpdate.setBigDecimal(7, zctxpf.getZoeramt());
				psUpdate.setBigDecimal(8, zctxpf.getZdedamt());
				psUpdate.setBigDecimal(9, zctxpf.getZundamt());
				psUpdate.setBigDecimal(10, zctxpf.getZspsamt());
				psUpdate.setBigDecimal(11, zctxpf.getZslryspct()); //ILIFE-7345 
				psUpdate.setBigDecimal(12, zctxpf.getZslrysamt()); //ILIFE-7345 
				psUpdate.setString(13, this.getUsrprf());
				psUpdate.setString(14, this.getJobnm());
				psUpdate.setTimestamp(15, new Timestamp(System.currentTimeMillis()));
				psUpdate.addBatch();	
				int[] rowCount=psUpdate.executeBatch();
				isUpdated = true; 
			}
			catch ( Exception e) {
				LOGGER.error("updatezctxpf()", e); /* IJTI-1479 */
					
			} finally {
				close(psUpdate, null);			
			}		
			return isUpdated;
		}
public boolean updateClient(Zctxpf zctxpf){
		
		StringBuilder sql_zctxpf_update = new StringBuilder("");
			sql_zctxpf_update.append("UPDATE ZCTXPF  ");
			sql_zctxpf_update.append("SET ");		 
			sql_zctxpf_update.append("CLNTNUM=? ");
			sql_zctxpf_update.append("WHERE CHDRNUM='"+zctxpf.getChdrnum()+"'");
			sql_zctxpf_update.append("AND CHDRCOY='"+zctxpf.getChdrcoy()+"'");
			PreparedStatement psUpdate = null;
			boolean isUpdated = false;
			try{
				psUpdate = getPrepareStatement(sql_zctxpf_update.toString());
				
				psUpdate.setString(1, zctxpf.getClntnum());
				psUpdate.addBatch();	
				int[] rowCount=psUpdate.executeBatch();
				isUpdated = true; 
			}
			//IJTI-851-Overly Broad Catch
			
			catch ( SQLException e) {
				LOGGER.error("updatezctxpf()", e); /* IJTI-1479 */
				
			} finally {
				close(psUpdate, null);			
			}		
			return isUpdated;
		}
	@Override
	public void insertZctxpfRecord(Zctxpf zctxpf) {
		if (zctxpf != null) {
		      
			StringBuilder sql_zctxpf_insert = new StringBuilder();
			sql_zctxpf_insert.append("INSERT INTO ZCTXPF");
			sql_zctxpf_insert.append("(CHDRCOY,CHDRNUM,CNTTYPE,CLNTNUM,DATEFRM,DATETO,ZSGTPCT,ZOERPCT,ZDEDPCT,ZUNDPCT,ZSPSPCT,TOTPRCNT,EFFDATE,USRPRF,AMOUNT,ZSGTAMT,ZOERAMT,ZDEDAMT,ZUNDAMT,ZSPSAMT,ZSLRYSPCT,ZSLRYSAMT,PROCFLAG,JOBNM,DATIME,ROLLFLAG)");
			sql_zctxpf_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_zctxpf_insert.toString());
			try
			{
					psInsert.setString(1, zctxpf.getChdrcoy());
					psInsert.setString(2, zctxpf.getChdrnum());
					psInsert.setString(3, zctxpf.getCnttype());
					psInsert.setString(4, zctxpf.getClntnum());
					psInsert.setInt(5, zctxpf.getDatefrm());
					psInsert.setInt(6, zctxpf.getDateto());
					psInsert.setBigDecimal(7, zctxpf.getZsgtpct());
					psInsert.setBigDecimal(8, zctxpf.getZoerpct());
					psInsert.setBigDecimal(9, zctxpf.getZdedpct());
					psInsert.setBigDecimal(10, zctxpf.getZundpct());
					psInsert.setBigDecimal(11, zctxpf.getZspspct());
					psInsert.setBigDecimal(12, zctxpf.getTotprcnt());
					psInsert.setInt(13, zctxpf.getEffdate());
					psInsert.setString(14, this.getUsrprf());
					psInsert.setBigDecimal(15, zctxpf.getAmount());
					psInsert.setBigDecimal(16, zctxpf.getZsgtamt());
					psInsert.setBigDecimal(17, zctxpf.getZoeramt());
					psInsert.setBigDecimal(18, zctxpf.getZdedamt());
					psInsert.setBigDecimal(19, zctxpf.getZundamt());
					psInsert.setBigDecimal(20, zctxpf.getZspsamt());
					psInsert.setBigDecimal(21, zctxpf.getZslryspct());//ILIFE-7345 
					psInsert.setBigDecimal(22, zctxpf.getZslrysamt()); //ILIFE-7345 
					psInsert.setString(23, zctxpf.getProcflag());
					psInsert.setString(24, this.getJobnm());
					psInsert.setTimestamp(25, new Timestamp(System.currentTimeMillis()));
					psInsert.setString(26, zctxpf.getRollflag());
					psInsert.addBatch();
					int[] rowCount=psInsert.executeBatch();	
			}
			catch (SQLException e) {
				LOGGER.error("insertzctxpfRecord()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}
		
	}

	@Override
	public List<Zctxpf> getZctxpfRecord(String chdrnum) {
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"select * From ZCTXPF where CHDRNUM=?");
		Zctxpf zctxpfRec = null;
		List<Zctxpf> zctxpflist = new ArrayList<Zctxpf>();
		try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());
		 	ps.setString(1, chdrnum);
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    	zctxpfRec = new Zctxpf();
			    zctxpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    zctxpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    zctxpfRec.setClntnum(rs.getString("CLNTNUM"));
			    zctxpfRec.setDatefrm(rs.getInt("DATEFRM"));
			    zctxpfRec.setDateto(rs.getInt("DATETO"));
			    zctxpfRec.setZsgtpct(rs.getBigDecimal("ZSGTPCT"));
			    zctxpfRec.setZoerpct(rs.getBigDecimal("ZOERPCT"));
			    zctxpfRec.setZdedpct(rs.getBigDecimal("ZDEDPCT"));
			    zctxpfRec.setZundpct(rs.getBigDecimal("ZUNDPCT"));
			    zctxpfRec.setZspspct(rs.getBigDecimal("ZSPSPCT"));
			    zctxpfRec.setTotprcnt(rs.getBigDecimal("TOTPRCNT"));
			    zctxpfRec.setAmount(rs.getBigDecimal("AMOUNT"));
			    zctxpflist.add(zctxpfRec);
			    
				}
		}
			catch(SQLException e)
			{
				LOGGER.error(" ZctxpfDAOImpl.readZctxpfData()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);	
			}
			
			return zctxpflist;
	}

	@Override
	public Zctxpf readRecordByDate(Zctxpf zctxpf) {
		StringBuilder sql_zctxpf_read = new StringBuilder("");
		sql_zctxpf_read.append("SELECT * FROM ZCTXPF  ");
		sql_zctxpf_read.append("WHERE CHDRNUM='"+zctxpf.getChdrnum()+"'");
		sql_zctxpf_read.append("AND DATEFRM='"+zctxpf.getDatefrm()+"'");
		
		Zctxpf zctxpfRec = null;
		
		try {
		  PreparedStatement ps = getPrepareStatement(sql_zctxpf_read.toString());
			ResultSet rs = null;
				rs = ps.executeQuery();
			    while (rs.next()) {		
			    zctxpfRec = new Zctxpf();
			    zctxpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    zctxpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    zctxpfRec.setClntnum(rs.getString("CLNTNUM"));
			    zctxpfRec.setDatefrm(rs.getInt("DATEFRM"));
			    zctxpfRec.setDateto(rs.getInt("DATETO"));
			    zctxpfRec.setZsgtpct(rs.getBigDecimal("ZSGTPCT"));
			    zctxpfRec.setZoerpct(rs.getBigDecimal("ZOERPCT"));
			    zctxpfRec.setZdedpct(rs.getBigDecimal("ZDEDPCT"));
			    zctxpfRec.setZundpct(rs.getBigDecimal("ZUNDPCT"));
			    zctxpfRec.setZspspct(rs.getBigDecimal("ZSPSPCT"));
			    zctxpfRec.setTotprcnt(rs.getBigDecimal("TOTPRCNT"));
			    zctxpfRec.setZsgtamt(rs.getBigDecimal("ZSGTAMT"));
			    zctxpfRec.setZoeramt(rs.getBigDecimal("ZOERAMT"));
			    zctxpfRec.setZdedamt(rs.getBigDecimal("ZDEDAMT"));
			    zctxpfRec.setZundamt(rs.getBigDecimal("ZUNDAMT"));
			    zctxpfRec.setZspsamt(rs.getBigDecimal("ZSPSAMT"));
			    zctxpfRec.setAmount(rs.getBigDecimal("AMOUNT"));
			    zctxpfRec.setZslryspct(rs.getBigDecimal("ZSLRYSPCT")); //ILIFE-7345 
			    zctxpfRec.setZslrysamt(rs.getBigDecimal("ZSLRYSAMT")); //ILIFE-7345 
			    zctxpfRec.setCocontamnt(rs.getBigDecimal("COCONTAMNT"));
			    zctxpfRec.setLiscamnt(rs.getBigDecimal("LISCAMNT"));
			    zctxpfRec.setRollflag(rs.getString("ROLLFLAG"));
				}
		}
			catch(SQLException e)
			{
				LOGGER.error(" ZctxpfDAOImpl.readRecordByDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);	
			}
			
			return zctxpfRec;
	}
public int getZctxpfCnt(String chdrcoy,String chdrnum) {
		//IJTI-306 START
		StringBuilder sql = new StringBuilder("SELECT COUNT(*) FROM ZCTXPF where CHDRCOY like ? and CHDRNUM like ? ");
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			stmt = getPrepareStatement(sql.toString());
			stmt.setString(1, "%"+chdrcoy.trim()+"%");
			stmt.setString(2, "%"+chdrnum.trim()+"%");
			rs =  stmt.executeQuery();
			//IJTI-306 END
			if(rs.next())
			{
				return rs.getInt(1);
			}								
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			LOGGER.error("getBakxCount()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		}
		finally
		{
			close(stmt,rs);		
		}
		return 0;
	}

public List<Zctxpf> readContributionTaxList(String chdrcoy,String chdrnum) {
	StringBuilder sqlSchemeSelect1 = new StringBuilder(
			"select * From ZCTXPF where  CHDRCOY like '%"+chdrcoy.trim()+"%' and CHDRNUM like '%"+chdrnum.trim()+"%' ORDER BY DATEFRM DESC");
	Zctxpf zctxpfRec = null;
	List<Zctxpf> zctxpfList = new ArrayList<Zctxpf>();
	try {
		  PreparedStatement ps = getPrepareStatement(sqlSchemeSelect1.toString());		  
		  ResultSet rs = null;
		  rs = ps.executeQuery();
		  while (rs.next()) {		
			   	zctxpfRec = new Zctxpf();
			    zctxpfRec.setChdrcoy(rs.getString("CHDRCOY"));
			    zctxpfRec.setChdrnum(rs.getString("CHDRNUM"));
			    zctxpfRec.setClntnum(rs.getString("CLNTNUM"));
			    zctxpfRec.setDatefrm(rs.getInt("DATEFRM"));
			    zctxpfRec.setDateto(rs.getInt("DATETO"));
			    zctxpfRec.setZsgtpct(rs.getBigDecimal("ZSGTPCT"));
			    zctxpfRec.setZoerpct(rs.getBigDecimal("ZOERPCT"));
			    zctxpfRec.setZdedpct(rs.getBigDecimal("ZDEDPCT"));
			    zctxpfRec.setZundpct(rs.getBigDecimal("ZUNDPCT"));
			    zctxpfRec.setZspspct(rs.getBigDecimal("ZSPSPCT"));
			    zctxpfRec.setTotprcnt(rs.getBigDecimal("TOTPRCNT"));
			    zctxpfRec.setAmount(rs.getBigDecimal("AMOUNT"));
			    zctxpfList.add(zctxpfRec);
		}
	}
		catch(SQLException e)
		{
			LOGGER.error(" ZctxpfDAOImpl.readZctxpfData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);	
		}
		
		return zctxpfList;

	
}
public boolean updateAmount(Zctxpf zctxpf){
	StringBuilder sql_zctxpf_update = new StringBuilder("");
	sql_zctxpf_update.append("UPDATE ZCTXPF  ");
	sql_zctxpf_update.append("SET ");		 
	sql_zctxpf_update.append("AMOUNT=?,ZSGTAMT=?,ZOERAMT=?,ZDEDAMT=?,ZUNDAMT=?,ZSPSAMT=?,ZSLRYSAMT=?,USRPRF=?,JOBNM=?,DATIME=?,ROLLFLAG=? ");
	sql_zctxpf_update.append("WHERE CHDRNUM='"+zctxpf.getChdrnum()+"'");
	sql_zctxpf_update.append("AND DATEFRM='"+zctxpf.getDatefrm()+"'");
	PreparedStatement psUpdate = null;
	boolean isUpdated = false;
	try{
		psUpdate = getPrepareStatement(sql_zctxpf_update.toString());
		
		psUpdate.setBigDecimal(1, zctxpf.getAmount());
		psUpdate.setBigDecimal(2, zctxpf.getZsgtamt());
		psUpdate.setBigDecimal(3, zctxpf.getZoeramt());
		psUpdate.setBigDecimal(4, zctxpf.getZdedamt());
		psUpdate.setBigDecimal(5, zctxpf.getZundamt());
		psUpdate.setBigDecimal(6, zctxpf.getZspsamt());
		psUpdate.setBigDecimal(7, zctxpf.getZslrysamt()); //ILIFE-7345 
		psUpdate.setString(8, this.getUsrprf());
		psUpdate.setString(9, this.getJobnm());
		psUpdate.setTimestamp(10, new Timestamp(System.currentTimeMillis()));
		psUpdate.setString(11, zctxpf.getRollflag());
		psUpdate.addBatch();	
		int[] rowCount=psUpdate.executeBatch();
		isUpdated = true; 
	}
	//IJTI-851-Overly Broad Catch
	catch ( SQLException e) {
		LOGGER.error("updatezctxpf()", e); /* IJTI-1479 */
			
	} finally {
		close(psUpdate, null);			
	}		
	return isUpdated;

}

	public Map<String, Zctxpf> readContributionDetails(String chdrcoy, List<String> chdrnum, int date, String procflag){
		StringBuilder zctxpfSql = new StringBuilder();
		Map<String, Zctxpf> zctxpfMap =  null;
		Zctxpf zctxpf;
		zctxpfSql.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, CNTTYPE, CLNTNUM, DATEFRM, DATETO, ZSGTPCT, ZOERPCT, ZDEDPCT, ");
		zctxpfSql.append("ZUNDPCT, ZSPSPCT, ZSLRYSPCT, TOTPRCNT, ZSGTAMT, ZOERAMT, ZDEDAMT, ZUNDAMT, ZSPSAMT, ZSLRYSAMT FROM ");
		zctxpfSql.append("ZCTXPF WHERE CHDRCOY = ? AND ");
		if(chdrnum != null && !chdrnum.isEmpty())
			zctxpfSql.append(getSqlInStr("CHDRNUM", chdrnum)).append(" AND ");
		zctxpfSql.append("DATETO = ? AND PROCFLAG = ? ORDER BY UNIQUE_NUMBER ASC, CHDRNUM ASC");
		PreparedStatement ps = getPrepareStatement(zctxpfSql.toString());
		ResultSet rs = null;
		try {
			zctxpfMap = new HashMap<>();
			ps.setString(1, chdrcoy);
			ps.setInt(2, date);
			ps.setString(3, procflag);
			rs = ps.executeQuery();
			while(rs.next()) {
				zctxpf = new Zctxpf();
				zctxpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				zctxpf.setChdrcoy(rs.getString("CHDRCOY"));
				zctxpf.setChdrnum(rs.getString("CHDRNUM"));
				zctxpf.setCnttype(rs.getString("CNTTYPE"));
				zctxpf.setClntnum(rs.getString("CLNTNUM"));
				zctxpf.setDatefrm(rs.getInt("DATEFRM"));
				zctxpf.setDateto(rs.getInt("DATETO"));
				zctxpf.setZsgtpct(rs.getBigDecimal("ZSGTPCT"));
				zctxpf.setZoerpct(rs.getBigDecimal("ZOERPCT"));
				zctxpf.setZdedpct(rs.getBigDecimal("ZDEDPCT"));
				zctxpf.setZundpct(rs.getBigDecimal("ZUNDPCT"));
				zctxpf.setZspspct(rs.getBigDecimal("ZSPSPCT"));
				zctxpf.setZslryspct(rs.getBigDecimal("ZSLRYSPCT"));
				zctxpf.setTotprcnt(rs.getBigDecimal("TOTPRCNT"));
				zctxpf.setZsgtamt(rs.getBigDecimal("ZSGTAMT"));
				zctxpf.setZoeramt(rs.getBigDecimal("ZOERAMT"));
				zctxpf.setZdedamt(rs.getBigDecimal("ZDEDAMT"));
				zctxpf.setZundamt(rs.getBigDecimal("ZUNDAMT"));
				zctxpf.setZspsamt(rs.getBigDecimal("ZSPSAMT"));
				zctxpf.setZslrysamt(rs.getBigDecimal("ZSLRYSAMT"));
				
				zctxpfMap.put(rs.getString("CHDRNUM"), zctxpf);
			}
		}
		catch(Exception e) {
			LOGGER.error("readContributionDetails()", e); /* IJTI-1479 */
		}
		finally {
			close(ps, rs, null);			
		}
		return zctxpfMap;
	}

	public boolean insertRecord(List<Zctxpf> zctxpf) {
		boolean isInserted = false;
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO ZCTXPF (CHDRCOY, CHDRNUM, CNTTYPE, CLNTNUM, DATEFRM, DATETO, ZSGTPCT, ZOERPCT, ZDEDPCT, ZUNDPCT, ZSPSPCT, ");
		sb.append(" TOTPRCNT, EFFDATE, USRPRF, JOBNM, DATIME, AMOUNT, ZSGTAMT, ZOERAMT, ZDEDAMT, ZUNDAMT, ZSPSAMT, ZSLRYSPCT, ZSLRYSAMT, PROCFLAG)"); 
		sb.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for(Zctxpf zctx : zctxpf) {
				int i=0;
				ps.setString(++i, zctx.getChdrcoy());
				ps.setString(++i, zctx.getChdrnum());
				ps.setString(++i, zctx.getCnttype());
				ps.setString(++i, zctx.getClntnum());
				ps.setInt(++i, zctx.getDatefrm());
				ps.setInt(++i, zctx.getDateto());
				ps.setBigDecimal(++i, zctx.getZsgtpct());
				ps.setBigDecimal(++i, zctx.getZoerpct());
				ps.setBigDecimal(++i, zctx.getZdedpct());
				ps.setBigDecimal(++i, zctx.getZundpct());
				ps.setBigDecimal(++i, zctx.getZspspct());
				ps.setBigDecimal(++i, zctx.getTotprcnt());
				ps.setInt(++i, zctx.getEffdate());
				ps.setString(++i, getUsrprf());
				ps.setString(++i, getJobnm());
				ps.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
				ps.setBigDecimal(++i, zctx.getAmount());
				ps.setBigDecimal(++i, zctx.getZsgtamt());
				ps.setBigDecimal(++i, zctx.getZoeramt());
				ps.setBigDecimal(++i, zctx.getZdedamt());
				ps.setBigDecimal(++i, zctx.getZundamt());
				ps.setBigDecimal(++i, zctx.getZspsamt());
				ps.setBigDecimal(++i, zctx.getZslryspct());
				ps.setBigDecimal(++i, zctx.getZslrysamt());
				ps.setString(++i, zctx.getProcflag());
				ps.addBatch();
			}
			if(ps.executeBatch().length > 0) {
				isInserted = true;
			}
		}
		catch(SQLException e) {
			LOGGER.error("insertRecord()", e);
			throw new SQLRuntimeException(e);
		}
		finally {
			close(ps,null);
		}
		return isInserted;
	}

	public boolean updateRecords(List<Zctxpf> zctxpf){
		StringBuilder zctxpfSql = new StringBuilder();
		zctxpfSql.append("UPDATE ZCTXPF SET PROCFLAG = ?, USRPRF = ?, JOBNM = ?, DATIME = ? ");
		zctxpfSql.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND UNIQUE_NUMBER = ?");
		PreparedStatement psUpdate = null;
		boolean isUpdated = false;
		try{
			psUpdate = getPrepareStatement(zctxpfSql.toString());
			for(Zctxpf zctx : zctxpf) {
				int i=0;
				psUpdate.setString(++i, zctx.getProcflag());
				psUpdate.setString(++i, getUsrprf());
				psUpdate.setString(++i, getJobnm());
				psUpdate.setTimestamp(++i, new Timestamp(System.currentTimeMillis()));
				psUpdate.setString(++i,  zctx.getChdrcoy());
				psUpdate.setString(++i,  zctx.getChdrnum());
				psUpdate.setLong(++i, zctx.getUniqueNumber());
				psUpdate.addBatch();
			}
			if(psUpdate.executeBatch().length > 0)
				isUpdated = true; 
		}
		catch ( Exception e) {
			LOGGER.error("updatezctxpf()", e); /* IJTI-1479 */
		} finally {
			close(psUpdate, null);			
		}		
		return isUpdated;
	}
	
	//ILIFE-7899
		public boolean updateCocolisc(String chdrcoy,String chdrnum, BigDecimal cocoamnt, BigDecimal liscamnt) {
			StringBuilder zctxpf_update = new StringBuilder("");
			zctxpf_update.append("UPDATE ZCTXPF SET COCONTAMNT = ?,LISCAMNT = ? WHERE CHDRCOY = ? AND CHDRNUM = ? ");
			PreparedStatement psUpdate = null;
			boolean isUpdated = false;
			try {
				psUpdate = getPrepareStatement(zctxpf_update.toString());
				
				psUpdate.setBigDecimal(1, cocoamnt);
				psUpdate.setBigDecimal(2, liscamnt);
				psUpdate.setString(3, chdrcoy);
				psUpdate.setString(4, chdrnum);
				psUpdate.addBatch();
				int[] rowCount=psUpdate.executeBatch();
				isUpdated = true;
			}
			catch ( Exception e) {
				LOGGER.error("updateCocolisc()", e); /* IJTI-1479 */
					
			} finally {
				close(psUpdate, null);			
			}		
			return isUpdated;
		}
		//ILIFE-7899
		//ILIFE-7916 Start
		@Override
		public List<Zctxpf> getRecord(String chdrcoy, String chdrnum,String startDate ) {
			Zctxpf zctxpf = null;
			List<Zctxpf> zctxSearchResult = new ArrayList<Zctxpf>();
			StringBuilder sb = new StringBuilder();
		//	sb.append("select  YCTX.TOTALTAXAMT as tottax, ZCTX.PROCFLAG as s290NoticeReceived, ZCTX.COCONTAMNT as coContriPmt, ZCTX.LISCAMNT as receivedFromATO,covr.Crtable as crtable, covr.instprem as instprem  from covrpf covr,ZCTXPF ZCTX, YCTXPF YCTX where ZCTX.CHDRNUM = YCTX.CHDRNUM and  ZCTX.CHDRNUM = covr.CHDRNUM and covr.VALIDFLAG='1' and YCTX.VALIDFLAG='1' and  covr.CHDRNUM=? and covr.CHDRCOY=? and  RTRIM(ZCTX.PROCFLAG) != ''  order by ZCTX.UNIQUE_NUMBER DESC ,YCTX.UNIQUE_NUMBER DESC;");
			sb.append(" select  YCTX.TOTALTAXAMT as tottax, ");
			sb.append(" YCTX.NOTRCVD as s290NoticeReceived, ZCTX.COCONTAMNT as coContriPmt,ZCTX.LISCAMNT as receivedFromATO,");
			sb.append(" covr.Crtable as crtable,  covr.instprem as instprem  ");
			sb.append(" from covrpf covr, ");
			sb.append(" ( select * from (SELECT ZCTX.CHDRNUM, ZCTX.PROCFLAG , ZCTX.COCONTAMNT ,ZCTX.LISCAMNT , RANK() OVER (PARTITION BY  ZCTX.CHDRNUM  ORDER BY UNIQUE_NUMBER desc ) rownum FROM  ZCTXPF ZCTX)A where  CHDRNUM=? and  rownum=1) ZCTX, ");
			sb.append(" YCTXPF YCTX   where   covr.CHDRNUM = ZCTX.CHDRNUM ");
			sb.append(" and covr.CHDRNUM = YCTX.CHDRNUM ");
			
			sb.append(" and covr.VALIDFLAG='1'  and YCTX.VALIDFLAG='1' and YCTX.STAXDT= ? and  covr.CHDRNUM= ? ");
			sb.append(" and covr.CHDRCOY= ? order by YCTX.UNIQUE_NUMBER DESC ;");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, chdrnum);
				ps.setString(2, startDate);
				ps.setString(3, chdrnum);
				ps.setString(4, chdrcoy);
			
				rs = ps.executeQuery();
				while(rs.next()) {
					zctxpf = new Zctxpf();
					zctxpf.setTottax(rs.getBigDecimal("tottax"));
					zctxpf.setProcflag(rs.getString("s290NoticeReceived"));
					zctxpf.setCocontamnt(rs.getBigDecimal("coContriPmt"));
					zctxpf.setLiscamnt(rs.getBigDecimal("receivedFromATO"));
					zctxpf.setCrtable(rs.getString("crtable"));
					zctxpf.setInstprem(rs.getBigDecimal("instprem"));
				
					zctxSearchResult.add(zctxpf);
				}
			}
			catch(SQLException e) {
				LOGGER.error("fetchRecords()", e);
				throw new SQLRuntimeException(e);
			}
			finally {
				close(ps, rs, null);
			}
			return zctxSearchResult;
		}
		
		@Override
		public List<Zctxpf> getzctxpfRecord(String chdrcoy, String chdrnum) {
			Zctxpf zctxpf = null;
			List<Zctxpf> zctxSearchResult = new ArrayList<Zctxpf>();
			StringBuilder sb = new StringBuilder();
		//	sb.append("select  YCTX.TOTALTAXAMT as tottax, ZCTX.PROCFLAG as s290NoticeReceived, ZCTX.COCONTAMNT as coContriPmt, ZCTX.LISCAMNT as receivedFromATO,covr.Crtable as crtable, covr.instprem as instprem  from covrpf covr,ZCTXPF ZCTX, YCTXPF YCTX where ZCTX.CHDRNUM = YCTX.CHDRNUM and  ZCTX.CHDRNUM = covr.CHDRNUM and covr.VALIDFLAG='1' and YCTX.VALIDFLAG='1' and  covr.CHDRNUM=? and covr.CHDRCOY=? and  RTRIM(ZCTX.PROCFLAG) != ''  order by ZCTX.UNIQUE_NUMBER DESC ,YCTX.UNIQUE_NUMBER DESC;");
		
			sb.append(" select covr.Crtable as crtable,  covr.instprem as instprem, ");
			sb.append(" ZCTX.COCONTAMNT as coContriPmt,ZCTX.LISCAMNT as receivedFromATO ");
			sb.append(" from covrpf covr , ");
			sb.append(" ( select * from (SELECT ZCTX.CHDRNUM,  ZCTX.COCONTAMNT ,ZCTX.LISCAMNT , RANK() OVER (PARTITION BY  ZCTX.CHDRNUM  ORDER BY UNIQUE_NUMBER desc ) rownum FROM  ZCTXPF ZCTX)A where  CHDRNUM= ? and  rownum= 1) ZCTX  ");
			sb.append(" where   covr.CHDRNUM = ZCTX.CHDRNUM ");
		    sb.append(" and covr.VALIDFLAG='1'   and  covr.CHDRNUM= ?");
			sb.append(" and covr.CHDRCOY= ? order by covr.UNIQUE_NUMBER DESC ;");
			PreparedStatement ps = null;
			ResultSet rs = null;
			try {
				ps = getPrepareStatement(sb.toString());
				ps.setString(1, chdrnum);
				ps.setString(2, chdrnum);
				ps.setString(3, chdrcoy);
			
				rs = ps.executeQuery();
				while(rs.next()) {
					zctxpf = new Zctxpf();
					zctxpf.setCocontamnt(rs.getBigDecimal("coContriPmt"));
					zctxpf.setLiscamnt(rs.getBigDecimal("receivedFromATO"));
					zctxpf.setCrtable(rs.getString("crtable"));
					zctxpf.setInstprem(rs.getBigDecimal("instprem"));
					zctxSearchResult.add(zctxpf);
				}
			}
			catch(SQLException e) {
				LOGGER.error("fetchRecords()", e);
				throw new SQLRuntimeException(e);
			}
			finally {
				close(ps, rs, null);
			}
			return zctxSearchResult;
		}
		//ILIFE-7916 END
					
						}
