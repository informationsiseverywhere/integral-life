package com.csc.life.agents.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgorpslTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:50
 * Class transformed from AGORPSL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgorpslTableDAM extends AgorpfTableDAM {

	public AgorpslTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGORPSL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM"
		             + ", REPORTAG";
		
		QUALIFIEDCOLUMNS = 
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "REPORTAG, " +
		            "AGTYPE01, " +
		            "AGTYPE02, " +
		            "EFFDATE01, " +
		            "EFFDATE02, " +
		            "DESC_T, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "REPORTAG ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "REPORTAG DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               agntcoy,
                               agntnum,
                               reportag,
                               agtype01,
                               agtype02,
                               effdate01,
                               effdate02,
                               desc,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(47);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getReportag().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, reportag);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(agntcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller30.setInternal(reportag.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(127);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getAgtype01().toInternal()
					+ getAgtype02().toInternal()
					+ getEffdate01().toInternal()
					+ getEffdate02().toInternal()
					+ getDesc().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, agtype01);
			what = ExternalData.chop(what, agtype02);
			what = ExternalData.chop(what, effdate01);
			what = ExternalData.chop(what, effdate02);
			what = ExternalData.chop(what, desc);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getReportag() {
		return reportag;
	}
	public void setReportag(Object what) {
		reportag.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getAgtype01() {
		return agtype01;
	}
	public void setAgtype01(Object what) {
		agtype01.set(what);
	}	
	public FixedLengthStringData getAgtype02() {
		return agtype02;
	}
	public void setAgtype02(Object what) {
		agtype02.set(what);
	}	
	public PackedDecimalData getEffdate01() {
		return effdate01;
	}
	public void setEffdate01(Object what) {
		setEffdate01(what, false);
	}
	public void setEffdate01(Object what, boolean rounded) {
		if (rounded)
			effdate01.setRounded(what);
		else
			effdate01.set(what);
	}	
	public PackedDecimalData getEffdate02() {
		return effdate02;
	}
	public void setEffdate02(Object what) {
		setEffdate02(what, false);
	}
	public void setEffdate02(Object what, boolean rounded) {
		if (rounded)
			effdate02.setRounded(what);
		else
			effdate02.set(what);
	}	
	public FixedLengthStringData getDesc() {
		return desc;
	}
	public void setDesc(Object what) {
		desc.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getEffdates() {
		return new FixedLengthStringData(effdate01.toInternal()
										+ effdate02.toInternal());
	}
	public void setEffdates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getEffdates().getLength()).init(obj);
	
		what = ExternalData.chop(what, effdate01);
		what = ExternalData.chop(what, effdate02);
	}
	public PackedDecimalData getEffdate(BaseData indx) {
		return getEffdate(indx.toInt());
	}
	public PackedDecimalData getEffdate(int indx) {

		switch (indx) {
			case 1 : return effdate01;
			case 2 : return effdate02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setEffdate(BaseData indx, Object what) {
		setEffdate(indx, what, false);
	}
	public void setEffdate(BaseData indx, Object what, boolean rounded) {
		setEffdate(indx.toInt(), what, rounded);
	}
	public void setEffdate(int indx, Object what) {
		setEffdate(indx, what, false);
	}
	public void setEffdate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setEffdate01(what, rounded);
					 break;
			case 2 : setEffdate02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAgtypes() {
		return new FixedLengthStringData(agtype01.toInternal()
										+ agtype02.toInternal());
	}
	public void setAgtypes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAgtypes().getLength()).init(obj);
	
		what = ExternalData.chop(what, agtype01);
		what = ExternalData.chop(what, agtype02);
	}
	public FixedLengthStringData getAgtype(BaseData indx) {
		return getAgtype(indx.toInt());
	}
	public FixedLengthStringData getAgtype(int indx) {

		switch (indx) {
			case 1 : return agtype01;
			case 2 : return agtype02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAgtype(BaseData indx, Object what) {
		setAgtype(indx.toInt(), what);
	}
	public void setAgtype(int indx, Object what) {

		switch (indx) {
			case 1 : setAgtype01(what);
					 break;
			case 2 : setAgtype02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		reportag.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		agtype01.clear();
		agtype02.clear();
		effdate01.clear();
		effdate02.clear();
		desc.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}