package com.csc.life.agents.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr560screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 18, 5, 24, 15, 16, 1, 2, 3, 21}; 
	public static int maxRecords = 30;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {21, 30, 22, 23, 24, 25}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 20, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr560ScreenVars sv = (Sr560ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr560screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr560screensfl, 
			sv.Sr560screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr560ScreenVars sv = (Sr560ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr560screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr560ScreenVars sv = (Sr560ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr560screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr560screensflWritten.gt(0))
		{
			sv.sr560screensfl.setCurrentIndex(0);
			sv.Sr560screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr560ScreenVars sv = (Sr560ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr560screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr560ScreenVars screenVars = (Sr560ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dteapp01Disp.setFieldName("dteapp01Disp");
				screenVars.zrreptp.setFieldName("zrreptp");
				screenVars.desc.setFieldName("desc");
				screenVars.agtype01.setFieldName("agtype01");
				screenVars.dteapp02Disp.setFieldName("dteapp02Disp");
				screenVars.agtype02.setFieldName("agtype02");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.dteapp01Disp.set(dm.getField("dteapp01Disp"));
			screenVars.zrreptp.set(dm.getField("zrreptp"));
			screenVars.desc.set(dm.getField("desc"));
			screenVars.agtype01.set(dm.getField("agtype01"));
			screenVars.dteapp02Disp.set(dm.getField("dteapp02Disp"));
			screenVars.agtype02.set(dm.getField("agtype02"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr560ScreenVars screenVars = (Sr560ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.dteapp01Disp.setFieldName("dteapp01Disp");
				screenVars.zrreptp.setFieldName("zrreptp");
				screenVars.desc.setFieldName("desc");
				screenVars.agtype01.setFieldName("agtype01");
				screenVars.dteapp02Disp.setFieldName("dteapp02Disp");
				screenVars.agtype02.setFieldName("agtype02");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("dteapp01Disp").set(screenVars.dteapp01Disp);
			dm.getField("zrreptp").set(screenVars.zrreptp);
			dm.getField("desc").set(screenVars.desc);
			dm.getField("agtype01").set(screenVars.agtype01);
			dm.getField("dteapp02Disp").set(screenVars.dteapp02Disp);
			dm.getField("agtype02").set(screenVars.agtype02);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr560screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr560ScreenVars screenVars = (Sr560ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.dteapp01Disp.clearFormatting();
		screenVars.zrreptp.clearFormatting();
		screenVars.desc.clearFormatting();
		screenVars.agtype01.clearFormatting();
		screenVars.dteapp02Disp.clearFormatting();
		screenVars.agtype02.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr560ScreenVars screenVars = (Sr560ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.dteapp01Disp.setClassString("");
		screenVars.zrreptp.setClassString("");
		screenVars.desc.setClassString("");
		screenVars.agtype01.setClassString("");
		screenVars.dteapp02Disp.setClassString("");
		screenVars.agtype02.setClassString("");
	}

/**
 * Clear all the variables in Sr560screensfl
 */
	public static void clear(VarModel pv) {
		Sr560ScreenVars screenVars = (Sr560ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.dteapp01Disp.clear();
		screenVars.dteapp01.clear();
		screenVars.zrreptp.clear();
		screenVars.desc.clear();
		screenVars.agtype01.clear();
		screenVars.dteapp02Disp.clear();
		screenVars.dteapp02.clear();
		screenVars.agtype02.clear();
	}
}
