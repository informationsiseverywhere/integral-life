/*
 * File: Tm603pt.java
 * Date: 30 August 2009 2:38:26
 * Author: Quipoz Limited
 * 
 * Class transformed from TM603PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.agents.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.agents.tablestructures.Tm603rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TM603.
*
*
*****************************************************************
* </pre>
*/
public class Tm603pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(42).isAPartOf(wsaaPrtLine001, 34, FILLER).init("Production Group                     SM603");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(49);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective  . :");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 23);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 33, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 39);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(26);
	private FixedLengthStringData filler9 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine004, 9, FILLER).init("UPDATE PRODUCTION");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(32);
	private FixedLengthStringData filler11 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine005, 16, FILLER).init("Personal");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 31);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(32);
	private FixedLengthStringData filler13 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine006, 16, FILLER).init("Direct Group");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 31);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(32);
	private FixedLengthStringData filler15 = new FixedLengthStringData(16).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine007, 16, FILLER).init("Group");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 31);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(75);
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine008, 9, FILLER).init("UPDATE DIRECT PRODUCTION of");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 37);
	private FixedLengthStringData filler19 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41);
	private FixedLengthStringData filler20 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 45);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 53);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 61);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 65);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69);
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 73);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(75);
	private FixedLengthStringData filler28 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler29 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine009, 9, FILLER).init("UPDATE GROUP PRODUCTION of");
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 37);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41);
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 53);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57);
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 61);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 65);
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 67, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 73);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(28);
	private FixedLengthStringData filler39 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tm603rec tm603rec = new Tm603rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tm603pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tm603rec.tm603Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo008.set(tm603rec.active01);
		fieldNo010.set(tm603rec.agtype01);
		fieldNo011.set(tm603rec.agtype02);
		fieldNo012.set(tm603rec.agtype03);
		fieldNo013.set(tm603rec.agtype04);
		fieldNo009.set(tm603rec.active02);
		fieldNo020.set(tm603rec.mlagttyp01);
		fieldNo021.set(tm603rec.mlagttyp02);
		fieldNo022.set(tm603rec.mlagttyp03);
		fieldNo023.set(tm603rec.mlagttyp04);
		fieldNo014.set(tm603rec.agtype05);
		fieldNo015.set(tm603rec.agtype06);
		fieldNo016.set(tm603rec.agtype07);
		fieldNo017.set(tm603rec.agtype08);
		fieldNo018.set(tm603rec.agtype09);
		fieldNo019.set(tm603rec.agtype10);
		fieldNo024.set(tm603rec.mlagttyp05);
		fieldNo025.set(tm603rec.mlagttyp06);
		fieldNo026.set(tm603rec.mlagttyp07);
		fieldNo027.set(tm603rec.mlagttyp08);
		fieldNo028.set(tm603rec.mlagttyp09);
		fieldNo029.set(tm603rec.mlagttyp10);
		fieldNo007.set(tm603rec.active03);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
