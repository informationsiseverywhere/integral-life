/*
 * File: Arcasgn.java
 * Date: December 3, 2013 1:47:52 AM ICT
 * Author: CSC
 * 
 * Class transformed from ARCASGN.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.archiving.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ArczpfTableDAM;
import com.csc.fsu.general.recordstructures.Arcprocrec;
import com.csc.life.archiving.dataaccess.AsgnarcTableDAM;
import com.csc.life.contractservicing.dataaccess.AsgnpfTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*         PROCESSING SUBROUTINE FOR ASGNPF.
*
*REMARKS.
*
*   This is the archiving processing subroutine for ASGNPF.
*
*   This program supports two modes of processing. these are
*   EXTRACT and UPDATE. The following functions are carried out
*   by these two modes.
*
*   This routine will support the transfer of records from any
*   given library to any other given library. It may therefore
*   be used for both Archive and Restore of records.
*
*   Extract Mode
*   ------------
*
*   > Read through the ASGNPF and count the number of records
*     which are to be processed from this file.
*
*   Update Mode
*   -----------
*
*   The same processing is carried out as with the extract above.
*
*   >  For each record, add the records to be archived to the
*      Archive file in Library2.
*
*      Records will be copied to Library2 using native reads and
*      writes. An override to the file in the correct Library is
*      first carried out to overcome the fact that Library2 is
*      variable.
*   >  For each record, delete from the database.
*
*
*   200-Initialise
*   --------------
*
*   The override to the file in Library2 is carried out.
*
*   300-Initial-Read
*   ----------------
*
*   Carry out an initial read on the ASGNPF using the ASGNARC
*   logical.
*
*   400-Loop
*   --------
*
*   Loop through the records using the ASGNARC logical reading
*   all records for this Contract Header Company and Contract
*   Header Number.
*
*   Count the number of Records.
*
*   If this is function UPDATE,
*
*       Perform 500-Update
*               ----------
*               This will add the record to the Archive File in
*               Library2.
*               This will carry out a direct read of the file
*               using the Relative Record Number from the logical
*               File. This is done so that all the fields are
*               read in.
*               The Record for the file read is then moved to
*               the output for the ASGNA file (which points to
*               the archive Library) and the record is written.
*
*       Perform 600-Delete
*               ----------
*               Delete the record from the ASGNPF using a DELT   D
*               on the ASGNARC File.
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ.
*     If a database error move the ASGNARC-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
****************************************************************** ****
* </pre>
*/
public class Arcasgn extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AsgnpfTableDAM asgnrrn = new AsgnpfTableDAM();
	private ArczpfTableDAM asgna = new ArczpfTableDAM();
	private AsgnpfTableDAM asgnrrnRec = new AsgnpfTableDAM();
	private AsgnpfTableDAM asgnaRec = new AsgnpfTableDAM();
	private final String wsaaSubr = "ARCASGN";
	private ZonedDecimalData wsaaRecCount = new ZonedDecimalData(5, 0).init(ZERO);
	private String wsaaFunction = "";
	private String wsaaFormat = "";
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private BinaryData asgnrrnRrn = new BinaryData(9, 0);
	private FixedLengthStringData statusCode = new FixedLengthStringData(2);
		/* FORMATS */
	private static final String asgnarcrec = "ASGNARCREC";
	private AsgnarcTableDAM asgnarcIO = new AsgnarcTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Arcprocrec arcprocrec = new Arcprocrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9490
	}

	public Arcasgn() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		arcprocrec.procRec = convertAndSetParam(arcprocrec.procRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main100()
	{
		/*PARA*/
		initialise200();
		initialRead300();
		while ( !(isEQ(asgnarcIO.getStatuz(), varcom.endp))) {
			loop400();
		}
		
		arcprocrec.statuz.set(varcom.endp);
		statusCode.set(asgnrrn.close());
		arcprocrec.records.set(wsaaRecCount);
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		wsaaRecCount.set(ZERO);
		arcprocrec.statuz.set(varcom.oK);
		/*  Validate the Input function value.*/
		if (isNE(arcprocrec.function, "REPT")
		&& isNE(arcprocrec.function, "UPDT")) {
			syserrrec.params.set(arcprocrec.procRec);
			syserrrec.statuz.set("FUNC");
			systemError9500();
		}
		if (isEQ(arcprocrec.function, "UPDT")) {
			prepare250();
		}
		statusCode.set(asgnrrn.openInput());
		/*EXIT*/
	}

protected void prepare250()
	{
		/*START*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ARCZPF) TOFILE(");
		stringVariable1.addExpression(arcprocrec.library2, SPACES);
		stringVariable1.addExpression("/ASGNPF)");
		stringVariable1.addExpression(" LVLCHK(*NO) ");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		statusCode.set(asgna.openAppend());
		/*EXIT*/
	}

protected void initialRead300()
	{
		start300();
	}

protected void start300()
	{
		asgnarcIO.setDataArea(SPACES);
		asgnarcIO.setChdrnum(arcprocrec.chdrnum);
		asgnarcIO.setChdrcoy(arcprocrec.chdrcoy);
		asgnarcIO.setFunction(varcom.begn);
		asgnarcIO.setFormat(asgnarcrec);
		SmartFileCode.execute(appVars, asgnarcIO);
		if (isNE(asgnarcIO.getStatuz(), varcom.endp)
		&& isNE(asgnarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(asgnarcIO.getParams());
			databaseError9600();
		}
		if (isNE(asgnarcIO.getChdrcoy(), arcprocrec.chdrcoy)
		|| isNE(asgnarcIO.getChdrnum(), arcprocrec.chdrnum)) {
			asgnarcIO.setStatuz(varcom.endp);
		}
	}

protected void loop400()
	{
		start400();
	}

protected void start400()
	{
		wsaaRecCount.add(1);
		if (isEQ(arcprocrec.function, "UPDT")
		&& (isEQ(arcprocrec.mode, SPACES)
		|| isEQ(arcprocrec.mode, "2"))) {
			update500();
			delete600();
		}
		asgnarcIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, asgnarcIO);
		if (isNE(asgnarcIO.getStatuz(), varcom.endp)
		&& isNE(asgnarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(asgnarcIO.getParams());
			databaseError9600();
		}
		if (isNE(asgnarcIO.getChdrcoy(), arcprocrec.chdrcoy)
		|| isNE(asgnarcIO.getChdrnum(), arcprocrec.chdrnum)) {
			asgnarcIO.setStatuz(varcom.endp);
		}
	}

protected void update500()
	{
		/*START*/
		/*  Carry out a Direct Read of the Database File using the*/
		/*  RRN from the Logical.*/
		/*  READ ASGNRRN.                                                */
		statusCode.set(asgnrrn.read(asgnrrnRec));
		wsaaFunction = "READ";
		wsaaFormat = "ASGNRRN";
		fileError9400();
		/*  Write the record to the file in Library2 using a 'direct'*/
		/*  WRITE.*/
		asgnaRec.set(asgnrrnRec);
		statusCode.set(asgna.write(asgnaRec));
		wsaaFunction = "WRITE";
		wsaaFormat = "ASGNA";
		fileError9400();
	}

protected void delete600()
	{
		/*START*/
		asgnarcIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, asgnarcIO);
		if (isNE(asgnarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(asgnarcIO.getParams());
			databaseError9600();
		}
		/*EXIT*/
	}

protected void fileError9400()
	{
		try {
			para9400();
			error9400();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para9400()
	{
		if (isEQ(statusCode, "00")) {
			arcprocrec.statuz.set(varcom.oK);
			goTo(GotoLabel.exit9490);
		}
		if (isEQ(statusCode, "22")) {
			arcprocrec.statuz.set("DUPR");
		}
		else {
			if (isEQ(statusCode, "9D")) {
				arcprocrec.statuz.set("HELD");
			}
			else {
				if (isEQ(statusCode, "9K")) {
					arcprocrec.statuz.set("IVRF");
				}
				else {
					if (isEQ(statusCode, "23")) {
						arcprocrec.statuz.set("MRNF");
					}
					else {
						arcprocrec.statuz.set(statusCode);
					}
				}
			}
		}
	}

protected void error9400()
	{
		if (isNE(arcprocrec.statuz, varcom.oK)) {
			asgnarcIO.setFunction(wsaaFunction);
			asgnarcIO.setFormat(wsaaFormat);
			syserrrec.params.set(asgnarcIO.getParams());
			syserrrec.statuz.set(arcprocrec.statuz);
			databaseError9600();
		}
	}

protected void systemError9500()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		arcprocrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void databaseError9600()
	{
		/*PARA*/
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		arcprocrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
