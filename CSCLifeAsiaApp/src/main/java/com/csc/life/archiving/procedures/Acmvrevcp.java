/*
 * File: Acmvrevcp.java
 * Date: December 3, 2013 1:45:32 AM ICT
 * Author: CSC
 * 
 * Class transformed from ACMVREVCP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.archiving.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.AcmvpfTableDAM;

import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.smart.recordstructures.Coldrlarec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* ACMVREV COLDPOINT ACCESS MODULE
* -------------------------------
*
* This routine sets up the relevant information to perform a call
* to the COLDPOINT software. COLDPOINT must be installed for this
* module to work successfully.
*
* This Reversal View is called from Batch mode only and therefore
* requires no Screen Information Message to appear.
*
* The routine simulates  what a SMART  IO routine would do except
* that the data is being retrieved from  COLDPOINT through an API
* supplied by COLDPOINT, called COLDRLA. The following parameters
* are required by the API:-
*
* Inputs:-
*
* Member Name             10 Alphanumerics
* File Name               10 Alphanumerics
* Library Name            10 Alphanumerics
* Database Number          7 Alphanumerics
* Key String              99 Alphanumerics
* Number of Key Fields     2 Alphanumerics
* Operation Code           5 Alphanumerics
* LR Indicator             1 Alphanumerics
*
* Outputs:-
*
* High Indicator           1 Alphanumerics
* Low Indicator            1 Alphanumerics
* Equal Indicator          1 Alphanumerics
* Relative Record No.      9 Alphanumerics
* Return Code              2 Alphanumerics
* Record Buffer         9000 Alphanumerics
*
* On successful reads from COLDPOINT, the Record Buffer is loaded
* with the appropriate data and moved in to ACMVREV- values. This
* simply forces the data in to the ACMVREVSKM.
*
*****************************************************************
* </pre>
*/
public class Acmvrevcp extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Acmvrevcp.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("ACMVREVCP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaAcmvkey = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaRldgcoy = new FixedLengthStringData(1).isAPartOf(wsaaAcmvkey, 0);
	private FixedLengthStringData wsaaRdocnum = new FixedLengthStringData(8).isAPartOf(wsaaAcmvkey, 1);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaAcmvkey, 10);
		/* WSAA-ACCT-PERIOD */
	private FixedLengthStringData wsaaBatcactyr = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBatcactmn = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaLibrary = new FixedLengthStringData(10);
		/* ERRORS */
	private static final String h430 = "H430";
	private static final String h431 = "H431";
	private static final String h432 = "H432";
	private static final String h433 = "H433";
	private static final String h434 = "H434";
	private static final String h435 = "H435";
	private static final String h436 = "H436";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();
	private AcmvpfTableDAM acmvpfRec = new AcmvpfTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Coldrlarec coldrlarec = new Coldrlarec();

	public Acmvrevcp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		acmvrevIO.setParams(convertAndSetParam(acmvrevIO.getParams(), parmArray, 0));
		try {
			initialise1000();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.debug("Flow Ended");
		}

		setReturningParam(acmvrevIO.getParams(), parmArray, 0);
	}

protected void initialise1000()
	{
		initialise1010();
		close1050();
	}

protected void initialise1010()
	{
		syserrrec.statuz.set(varcom.oK);
		acmvrevIO.setStatuz(varcom.oK);
		if (isEQ(acmvrevIO.getFunction(), "CLOSE")) {
			return ;
		}
		/* The first time through we must call the COLDRLA API with an OPEN*/
		/* call. If this is successful we must then attempt to read the fir*/
		/* record via a read using the CHAIN function. After a CHAIN call h*/
		/* been performed then a READE is performed for all subsequent read*/
		/* A CHAIN read will retrieve the First record in the File with the*/
		/* specified key value.*/
		/* A READE Retrieves the next record in the file with the same key*/
		/* value as previously read.*/
		if (isEQ(acmvrevIO.getFunction(), varcom.begn)) {
			getOptical1100();
			if (isEQ(syserrrec.statuz, varcom.oK)) {
				coldrlarec.operationCode.set("CHAIN");
				processAcmvOp1200();
			}
		}
		if (isEQ(acmvrevIO.getFunction(), varcom.nextr)) {
			coldrlarec.operationCode.set("READE");
			processAcmvOp1200();
		}
		if (isNE(syserrrec.statuz, varcom.oK)
		&& isNE(syserrrec.statuz, varcom.endp)) {
			fatalError600();
		}
	}

protected void close1050()
	{
		if (isEQ(syserrrec.statuz, varcom.endp)
		|| isEQ(acmvrevIO.getFunction(), "CLOSE")) {
			/* Set the Last Record indicator to '1' to force the end of file.*/
			/* Without this the CLOSE will work OK but the next time you attemp*/
			/* a call to COLDRLA it will fail.*/
			coldrlarec.lrIndicator.set("1");
			coldrlarec.operationCode.set("CLOSE");
			coldrlaCall1300();
			removeLibl1400();
			acmvrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
		exitProgram();
	}

protected void getOptical1100()
	{
		para1110();
	}

protected void para1110()
	{
		/*    This is the processing for the OPTICAL data retrieval.*/
		/*    ACMV records are archived by Accounting Month.*/
		/*    ACMV records are copied to a new file ACMVyymm each month.*/
		/*    Reads the ACMVyymm record for the PASSED A/c Year & Month.*/
		/*    The Accounting Year & Month are moved to alphanumeric*/
		/*    fields so that they can be moved in to COLD-MEMBER-NAME.*/
		wsaaBatcactyr.set(acmvrevIO.getBatcactyr());
		wsaaBatcactmn.set(acmvrevIO.getBatcactmn());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("ACMV");
		stringVariable1.addExpression(wsaaBatcactyr);
		stringVariable1.addExpression(wsaaBatcactmn);
		stringVariable1.setStringInto(coldrlarec.memberName);
		coldrlarec.fileName.set(coldrlarec.memberName);
//		callProgram(Coldrlacl.class, wsaaLibrary);
		throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
//		coldrlarec.libraryName.set(wsaaLibrary);
//		coldrlarec.databaseNumber.set(SPACES);
//		wsaaRldgcoy.set(acmvrevIO.getRldgcoy());
//		wsaaRdocnum.set(acmvrevIO.getRdocnum());
//		wsaaTranno.set(acmvrevIO.getTranno());
//		coldrlarec.keyString.set(SPACES);
//		coldrlarec.keyString.set(wsaaAcmvkey);
//		coldrlarec.numberKeyFields.set("03");
//		coldrlarec.lrIndicator.set(SPACES);
//		coldrlarec.highIndicator.set(SPACES);
//		coldrlarec.lowIndicator.set(SPACES);
//		coldrlarec.equalIndicator.set(SPACES);
//		coldrlarec.rrn.set(SPACES);
//		coldrlarec.returnCode.set(SPACES);
//		coldrlarec.recordBuffer.set(SPACES);
//		/* Perform 1st call to COLDPOINT routine COLDRLA with Operation*/
//		/* Code set to OPEN.*/
//		coldrlarec.operationCode.set("OPEN ");
//		coldrlaCall1300();
	}

protected void processAcmvOp1200()
	{
		para1210();
	}

protected void para1210()
	{
		/* Call COLDPOINT Routine 'COLDRLA'*/
		coldrlaCall1300();
		/*    Set up the Transaction details from the ACMVPF record.*/
		if (isNE(syserrrec.statuz, varcom.oK)) {
			return ;
		}
		acmvrevIO.setDataArea(SPACES);
		acmvrevIO.setRldgcoy(acmvpfRec.rldgcoy);
		acmvrevIO.setRdocnum(acmvpfRec.rdocnum);
		acmvrevIO.setTranno(acmvpfRec.tranno);
		acmvrevIO.setBatccoy(acmvpfRec.batccoy);
		acmvrevIO.setBatcbrn(acmvpfRec.batcbrn);
		acmvrevIO.setBatcactyr(acmvpfRec.batcactyr);
		acmvrevIO.setBatcactmn(acmvpfRec.batcactmn);
		acmvrevIO.setBatctrcde(acmvpfRec.batctrcde);
		acmvrevIO.setBatcbatch(acmvpfRec.batcbatch);
		acmvrevIO.setGlcode(acmvpfRec.glcode);
		acmvrevIO.setSacscode(acmvpfRec.sacscode);
		acmvrevIO.setSacstyp(acmvpfRec.sacstyp);
		/*    IF   GLSIGN                 = '-'*/
		/*         COMPUTE ORIGAMT        = ORIGAMT  * -1*/
		/*         COMPUTE ACCTAMT        = ACCTAMT  * -1.*/
		acmvrevIO.setOrigamt(acmvpfRec.origamt);
		acmvrevIO.setOrigcurr(acmvpfRec.origcurr);
		acmvrevIO.setAcctamt(acmvpfRec.acctamt);
		acmvrevIO.setGenlcur(acmvpfRec.genlcur);
		acmvrevIO.setGlsign(acmvpfRec.glsign);
		acmvrevIO.setRldgacct(acmvpfRec.rldgacct);
		acmvrevIO.setJrnseq(acmvpfRec.jrnseq);
		acmvrevIO.setTranref(acmvpfRec.tranref);
		acmvrevIO.setTrandesc(acmvpfRec.trandesc);
		acmvrevIO.setCrate(acmvpfRec.crate);
		acmvrevIO.setGenlcoy(acmvpfRec.genlcoy);
		acmvrevIO.setPostyear(acmvpfRec.postyear);
		acmvrevIO.setPostmonth(acmvpfRec.postmonth);
		acmvrevIO.setEffdate(acmvpfRec.effdate);
		acmvrevIO.setFrcdate(acmvpfRec.frcdate);
		acmvrevIO.setTransactionDate(acmvpfRec.transactionDate);
		acmvrevIO.setTransactionTime(acmvpfRec.transactionTime);
		acmvrevIO.setUser(acmvpfRec.user);
		acmvrevIO.setTermid(acmvpfRec.termid);
	}

protected void coldrlaCall1300()
	{
		para1310();
	}

protected void para1310()
	{
		/* Can NOT call with 01 level.*/
//		callProgram(COLDRLA.class, coldrlarec.memberName, coldrlarec.fileName, coldrlarec.libraryName, coldrlarec.databaseNumber, coldrlarec.keyString, coldrlarec.numberKeyFields, coldrlarec.operationCode, coldrlarec.lrIndicator, coldrlarec.highIndicator, coldrlarec.lowIndicator, coldrlarec.equalIndicator, coldrlarec.rrn, coldrlarec.returnCode, coldrlarec.recordBuffer);
        throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
		/* Only check on Low/Equal Indicator if Function is READE OR CHAIN*/
//		if (isEQ(coldrlarec.operationCode, "READE")) {
//			if (isNE(coldrlarec.lowIndicator, "0")) {
//				syserrrec.statuz.set(varcom.mrnf);
//			}
//			if (isNE(coldrlarec.equalIndicator, "0")) {
//				syserrrec.statuz.set(varcom.endp);
//			}
//		}
//		if (isEQ(coldrlarec.operationCode, "CHAIN")) {
//			if (isNE(coldrlarec.lowIndicator, "0")) {
//				syserrrec.statuz.set(varcom.mrnf);
//			}
//			if (isNE(coldrlarec.highIndicator, "0")) {
//				syserrrec.statuz.set(varcom.endp);
//			}
//		}
//		if (isEQ(coldrlarec.returnCode, "5A")) {
//			syserrrec.statuz.set(h430);
//		}
//		if (isEQ(coldrlarec.returnCode, "5B")) {
//			syserrrec.statuz.set(h431);
//		}
//		if (isEQ(coldrlarec.returnCode, "5C")) {
//			syserrrec.statuz.set(h432);
//		}
//		if (isEQ(coldrlarec.returnCode, "5D")) {
//			syserrrec.statuz.set(h433);
//		}
//		if (isEQ(coldrlarec.returnCode, "9A")) {
//			syserrrec.statuz.set(h434);
//		}
//		if (isEQ(coldrlarec.returnCode, "9B")) {
//			syserrrec.statuz.set(h435);
//		}
//		if (isEQ(coldrlarec.returnCode, "92")) {
//			syserrrec.statuz.set(h436);
//		}
//		if (isEQ(coldrlarec.returnCode, "9Z")) {
//			syserrrec.statuz.set(h436);
//		}
//		/* If a READE or CHAIN is performed it checks that the key trying t*/
//		/* be read is actually found. Therefore, it is not necessary to*/
//		/* compare returned key against input key as would normally be the*/
//		/* case in a BEGN/NEXTR COBOL scenario.*/
//		if (isEQ(coldrlarec.operationCode, "READE")
//		|| isEQ(coldrlarec.operationCode, "CHAIN")) {
//			if (isEQ(syserrrec.statuz, varcom.oK)) {
//				acmvpfRec.set(coldrlarec.recordBuffer);
//			}
//		}
	}

protected void removeLibl1400()
	{
		/*PARA*/
//		callProgram(Coldrlacl2.class);
        throw new RuntimeException("Integral 7.7 - Missing program (Temporally disable for compilation)");
		/*EXIT*/
	}

protected void fatalError600()
	{
		error600();
		exit602();
	}

protected void error600()
	{
		if (isEQ(syserrrec.statuz, "BOMB")) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit602()
	{
		acmvrevIO.setStatuz("BOMB");
		/*EXIT*/
		exitProgram();
	}
}
