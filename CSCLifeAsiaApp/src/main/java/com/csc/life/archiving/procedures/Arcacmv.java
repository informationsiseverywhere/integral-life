/*
 * File: Arcacmv.java
 * Date: December 3, 2013 1:47:48 AM ICT
 * Author: CSC
 * 
 * Class transformed from ARCACMV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.archiving.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.accounting.dataaccess.AcmvarcTableDAM;
import com.csc.fsu.general.dataaccess.AcmvpfTableDAM;
import com.csc.fsu.general.dataaccess.ArczpfTableDAM;
import com.csc.fsu.general.recordstructures.Arcprocrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*         PROCESSING SUBROUTINE FOR ACMVPF.
*
*REMARKS.
*
*   This is the archiving processing subroutine for ACMVPF.
*
*   This program supports two modes of processing. these are
*   EXTRACT and UPDATE. The following functions are carried out
*   by these two modes.
*
*   This routine will support the transfer of records from any
*   given library to any other given library. It may therefore
*   be used for both Archive and Restore of records.
*
*   Extract Mode
*   ------------
*
*   > Read through the ACMVPF and count the number of records
*     which are to be processed from this file.
*
*   Update Mode
*   -----------
*
*   The same processing is carried out as with the extract above.
*
*   >  For each record, add the records to be archived to the
*      Archive file in Library2.
*
*      Records will be copied to Library2 using native reads and
*      writes. An override to the file in the correct Library is
*      first carried out to overcome the fact that Library2 is
*      variable.
*   >  For each record, delete from the database.
*
*
*   200-Initialise
*   --------------
*
*   The override to the file in Library2 is carried out.
*
*   300-Initial-Read
*   ----------------
*
*   Carry out an initial read on the ACMVPF using the ACMVARC
*   logical.
*
*   400-Loop
*   --------
*
*   Loop through the records using the ACMVARC logical reading
*   all records for this Contract Header Company and Contract
*   Header Number.
*
*   Count the number of Records.
*
*   If this is function UPDATE,
*
*       Perform 500-Update
*               ----------
*               This will add the record to the Archive File in
*               Library2.
*               This will carry out a direct read of the file
*               using the Relative Record Number from the logical
*               File. This is done so that all the fields are
*               read in.
*               The Record for the file read is then moved to
*               the output for the ACMVA file (which points to
*               the archive Library) and the record is written.
*
*       Perform 600-Delete
*               ----------
*               Delete the record from the ACMVPF using a DELT   D
*               on the ACMVARC File.
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ.
*     If a database error move the ACMVARC-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
****************************************************************** ****
* </pre>
*/
public class Arcacmv extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private AcmvpfTableDAM acmvrrn = new AcmvpfTableDAM();
	private ArczpfTableDAM acmva = new ArczpfTableDAM();
	private AcmvpfTableDAM acmvrrnRec = new AcmvpfTableDAM();
	private AcmvpfTableDAM acmvaRec = new AcmvpfTableDAM();
	private final String wsaaSubr = "ARCACMV";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0);
	private ZonedDecimalData wsaaRecCount = new ZonedDecimalData(5, 0).init(ZERO);

		/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Rec = FLSInittedArray (6, 2);
	private FixedLengthStringData[] wsaaSacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 0);
	private String wsaaFunction = "";
	private String wsaaFormat = "";
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private BinaryData acmvrrnRrn = new BinaryData(9, 0);
	private FixedLengthStringData statusCode = new FixedLengthStringData(2);
		/* ERRORS */
	private static final String h134 = "H134";
		/* TABLES */
	private static final String t5645 = "T5645";
		/* FORMATS */
	private static final String acmvarcrec = "ACMVARCREC";
	private static final String itemrec = "ITEMREC";
	private IntegerData cIndex = new IntegerData();
	private AcmvarcTableDAM acmvarcIO = new AcmvarcTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private T5645rec t5645rec = new T5645rec();
	private Arcprocrec arcprocrec = new Arcprocrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit9490
	}

	public Arcacmv() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		arcprocrec.procRec = convertAndSetParam(arcprocrec.procRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main100()
	{
		/*PARA*/
		initialise200();
		initialRead300();
		while ( !(isEQ(acmvarcIO.getStatuz(), varcom.endp))) {
			loop400();
		}
		
		arcprocrec.statuz.set(varcom.endp);
		statusCode.set(acmvrrn.close());
		arcprocrec.records.set(wsaaRecCount);
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		start200();
	}

protected void start200()
	{
		/*  Load the item ARCACMV from T5645 and store it in WSAA.*/
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(arcprocrec.chdrcoy);
		itemIO.setItemtabl(t5645);
		itemIO.setFormat(itemrec);
		itemIO.setItemitem("ARCACMV");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			systemError9500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(h134);
			syserrrec.params.set(itemIO.getParams());
			systemError9500();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			wsaaSacscode[wsaaSub.toInt()].set(t5645rec.sacscode[wsaaSub.toInt()]);
		}
		syserrrec.subrname.set(wsaaSubr);
		wsaaRecCount.set(ZERO);
		arcprocrec.statuz.set(varcom.oK);
		/*  Validate the Input function value.*/
		if (isNE(arcprocrec.function, "REPT")
		&& isNE(arcprocrec.function, "UPDT")) {
			syserrrec.params.set(arcprocrec.procRec);
			syserrrec.statuz.set("FUNC");
			systemError9500();
		}
		if (isEQ(arcprocrec.function, "UPDT")) {
			prepare250();
		}
		statusCode.set(acmvrrn.openInput());
	}

protected void prepare250()
	{
		/*START*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(ARCZPF) TOFILE(");
		stringVariable1.addExpression(arcprocrec.library2, SPACES);
		stringVariable1.addExpression("/ACMVPF)");
		stringVariable1.addExpression(" LVLCHK(*NO) ");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		statusCode.set(acmva.openAppend());
		/*EXIT*/
	}

protected void initialRead300()
	{
		start300();
	}

protected void start300()
	{
		acmvarcIO.setDataArea(SPACES);
		acmvarcIO.getRldgacct().setSub1String(1, 8, arcprocrec.chdrnum);
		acmvarcIO.setRldgcoy(arcprocrec.chdrcoy);
		acmvarcIO.setFunction(varcom.begn);
		acmvarcIO.setFormat(acmvarcrec);
		SmartFileCode.execute(appVars, acmvarcIO);
		if (isNE(acmvarcIO.getStatuz(), varcom.endp)
		&& isNE(acmvarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvarcIO.getParams());
			databaseError9600();
		}
		if (isNE(acmvarcIO.getRldgcoy(), arcprocrec.chdrcoy)
		|| isNE(subString(acmvarcIO.getRldgacct(), 1, 8), arcprocrec.chdrnum)) {
			acmvarcIO.setStatuz(varcom.endp);
		}
	}

protected void loop400()
	{
		start400();
	}

protected void start400()
	{
		cIndex.set(1);
		 searchlabel1:
		{
			for (; isLT(cIndex, wsaaT5645Rec.length); cIndex.add(1)){
				if (isEQ(wsaaSacscode[cIndex.toInt()], acmvarcIO.getSacscode())) {
					wsaaRecCount.add(1);
					if (isEQ(arcprocrec.function, "UPDT")
					&& (isEQ(arcprocrec.mode, SPACES)
					|| isEQ(arcprocrec.mode, "2"))) {
						update500();
						delete600();
					}
					break searchlabel1;
				}
			}
			/*CONTINUE_STMT*/
		}
		acmvarcIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, acmvarcIO);
		if (isNE(acmvarcIO.getStatuz(), varcom.endp)
		&& isNE(acmvarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvarcIO.getParams());
			databaseError9600();
		}
		if (isNE(acmvarcIO.getRldgcoy(), arcprocrec.chdrcoy)
		|| isNE(subString(acmvarcIO.getRldgacct(), 1, 8), arcprocrec.chdrnum)) {
			acmvarcIO.setStatuz(varcom.endp);
		}
	}

protected void update500()
	{
		/*START*/
		/*  Carry out a Direct Read of the Database File using the*/
		/*  RRN from the Logical.*/
		/*  READ ACMVRRN                                                 */
		statusCode.set(acmvrrn.read(acmvrrnRec));
		wsaaFunction = "READ";
		wsaaFormat = "ACMVRRN";
		fileError9400();
		/*  Write the record to the file in Library2 using a 'direct'*/
		/*  WRITE.*/
		acmvaRec.set(acmvrrnRec);
		statusCode.set(acmva.write(acmvaRec));
		wsaaFunction = "WRITE";
		wsaaFormat = "ACMVA";
		fileError9400();
		/*EXIT*/
	}

protected void delete600()
	{
		/*START*/
		acmvarcIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, acmvarcIO);
		if (isNE(acmvarcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(acmvarcIO.getParams());
			databaseError9600();
		}
		/*EXIT*/
	}

protected void fileError9400()
	{
		try {
			para9400();
			error9400();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para9400()
	{
		if (isEQ(statusCode, "00")) {
			arcprocrec.statuz.set(varcom.oK);
			goTo(GotoLabel.exit9490);
		}
		if (isEQ(statusCode, "22")) {
			arcprocrec.statuz.set("DUPR");
		}
		else {
			if (isEQ(statusCode, "9D")) {
				arcprocrec.statuz.set("HELD");
			}
			else {
				if (isEQ(statusCode, "9K")) {
					arcprocrec.statuz.set("IVRF");
				}
				else {
					if (isEQ(statusCode, "23")) {
						arcprocrec.statuz.set("MRNF");
					}
					else {
						arcprocrec.statuz.set(statusCode);
					}
				}
			}
		}
	}

protected void error9400()
	{
		if (isNE(arcprocrec.statuz, varcom.oK)) {
			acmvarcIO.setFunction(wsaaFunction);
			acmvarcIO.setFormat(wsaaFormat);
			syserrrec.params.set(acmvarcIO.getParams());
			syserrrec.statuz.set(arcprocrec.statuz);
			databaseError9600();
		}
	}

protected void systemError9500()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		arcprocrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}

protected void databaseError9600()
	{
		/*PARA*/
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		arcprocrec.statuz.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
