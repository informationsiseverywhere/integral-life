/*
 * File: Pcplttrc.java
 * Date: 30 August 2009 1:03:11
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLTTRC.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.fsu.reinsurance.dataaccess.TtrcTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Contract Receipt No
*  002  Contract Receipt Date
*  003  Temporary Receipt No
*  004  Temporary Receipt Date
*
***********************************************************************
* </pre>
*/
public class Pcplttrc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLTTRC";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
		/* The following array is configured to store up to 2 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (4, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String rtrnrec = "RTRNREC";
	private static final String ttrcrec = "TTRCREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private TtrcTableDAM ttrcIO = new TtrcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplttrc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getFormatFirstReceipt200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getFormatFirstReceipt200()
	{
		start210();
		formatReceiptNo201();
		formatReceiptDate201();
		formatTempReceiptNo201();
		formatTempReceiptDate201();
	}

protected void start210()
	{
		chdrlnbIO.setChdrcoy(letcIO.getChdrcoy());
		chdrlnbIO.setChdrnum(letcIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		rtrnsacIO.setRldgcoy(letcIO.getChdrcoy());
		rtrnsacIO.setSacscode("LP");
		rtrnsacIO.setRldgacct(letcIO.getChdrnum());
		rtrnsacIO.setSacstyp("S");
		rtrnsacIO.setOrigccy(chdrlnbIO.getBillcurr());
		rtrnsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnsacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		rtrnsacIO.setFitKeysSearch("RLDGCOY", "RLDGACCT", "ORIGCCY");
		rtrnsacIO.setFormat(rtrnrec);
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			fatalError600();
		}
		if (isNE(rtrnsacIO.getRldgcoy(),letcIO.getChdrcoy())
		|| isNE(rtrnsacIO.getRldgacct(),letcIO.getChdrnum())
		|| isNE(rtrnsacIO.getOrigccy(),chdrlnbIO.getBillcurr())
		|| isNE(rtrnsacIO.getStatuz(),varcom.oK)) {
			rtrnsacIO.setRdocnum(SPACES);
			rtrnsacIO.setEffdate(varcom.vrcmMaxDate);
		}
		ttrcIO.setDataKey(SPACES);
		ttrcIO.setChdrcoy(letcIO.getChdrcoy());
		ttrcIO.setChdrnum(letcIO.getChdrnum());
		ttrcIO.setEffdate(varcom.vrcmMaxDate);
		ttrcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ttrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ttrcIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		ttrcIO.setFormat(ttrcrec);
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(),varcom.oK)
		&& isNE(ttrcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isNE(ttrcIO.getStatuz(),varcom.oK)
		|| isNE(ttrcIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(ttrcIO.getChdrnum(),letcIO.getChdrnum())) {
			ttrcIO.setTtmprcno(SPACES);
			ttrcIO.setTtmprcdte(varcom.vrcmMaxDate);
		}
	}

protected void formatReceiptNo201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(rtrnsacIO.getRdocnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], rtrnsacIO.getRdocnum());
	}

protected void formatReceiptDate201()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(rtrnsacIO.getEffdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatTempReceiptNo201()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(ttrcIO.getTtmprcno()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], ttrcIO.getTtmprcno());
	}

protected void formatTempReceiptDate201()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(ttrcIO.getTtmprcdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
