/*
 * File: Pcplsusp.java
 * Date: 30 August 2009 1:03:03
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLSUSP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Currency
*  002  Suspense Amount
*
***********************************************************************
* </pre>
*/
public class Pcplsusp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLSUSP";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private PackedDecimalData wsaaSuspAmt = new PackedDecimalData(15, 2).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private String wsaaSuspInd = "";
	private FixedLengthStringData wsaaCurr = new FixedLengthStringData(3);
		/* The following array is configured to store up to 2 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (4, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private static final String t5645 = "T5645";
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplsusp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getAcblenqFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaAmount.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaSuspAmt.set(ZERO);
		offset.set(ZERO);
		wsaaCurr.set(SPACES);
		wsaaSuspInd = "N";
		/*EXIT*/
	}

protected void getAcblenqFormat200()
	{
		start210();
		format201();
		format202();
	}

protected void start210()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaSubrname);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(letcIO.getRdoccoy());
		chdrlnbIO.setChdrnum(letcIO.getRdocnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		acblenqIO.setParams(SPACES);
		acblenqIO.setRldgcoy(letcIO.getRdoccoy());
		acblenqIO.setRldgacct(letcIO.getRdocnum());
		acblenqIO.setSacscode(t5645rec.sacscode01);
		acblenqIO.setSacstyp(t5645rec.sacstype01);
		wsaaSuspAmt.set(0);
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)
		|| isEQ(wsaaSuspInd,"Y")); wsaaSub.add(1)){
			checkSuspense300();
		}
	}

protected void format201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(wsaaCurr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurr);
	}

protected void format202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(wsaaSuspAmt,ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			wsaaAmount.set(wsaaSuspAmt);
		}
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*EXIT*/
	}

protected void checkSuspense300()
	{
		locateSuspense300();
	}

protected void locateSuspense300()
	{
		if (isEQ(wsaaSub,1)) {
			acblenqIO.setOrigcurr(chdrlnbIO.getCntcurr());
			acblenqIO.setFunction(varcom.readr);
		}
		else {
			if (isEQ(wsaaSub,2)) {
				acblenqIO.setOrigcurr(chdrlnbIO.getBillcurr());
				acblenqIO.setFunction(varcom.readr);
			}
			else {
				acblenqIO.setOrigcurr(SPACES);
				acblenqIO.setFunction(varcom.begn);
			}
		}
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(),varcom.oK))
		&& (isNE(acblenqIO.getStatuz(),varcom.mrnf))
		&& (isNE(acblenqIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if ((isEQ(acblenqIO.getStatuz(),varcom.oK))
		&& (isEQ(acblenqIO.getRldgcoy(),letcIO.getRdoccoy()))
		&& (isEQ(acblenqIO.getRldgacct(),letcIO.getRdocnum()))
		&& (isEQ(acblenqIO.getSacscode(),t5645rec.sacscode01))
		&& (isEQ(acblenqIO.getSacstyp(),t5645rec.sacstype01))
		&& (isNE(acblenqIO.getSacscurbal(),ZERO))) {
			wsaaSuspInd = "Y";
			wsaaSuspAmt.set(acblenqIO.getSacscurbal());
			wsaaCurr.set(acblenqIO.getOrigcurr());
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
