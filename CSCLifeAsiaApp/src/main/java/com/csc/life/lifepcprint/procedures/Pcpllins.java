package com.csc.life.lifepcprint.procedures;


import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Pcpllins extends COBOLConvCodeModel {
	

	public static final String ROUTINE = QPUtilities.getThisClass();
	
	private String wsaaSubrname = "PCPLLINS";
	private PackedDecimalData offset = new PackedDecimalData(6, 0);
		/* FORMATS */
	private String linsrnlrec = "LINSRNLREC";
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private ChdrlifTableDAM chdrIO = new ChdrlifTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private String oK = Varcom.oK.trim();
	private String endp = Varcom.endp.trim();
	private String chdrnum;
	private String chdrcoy;
	private int ptDate;
	//TMLII-1724
	//private double totalPremiumAmount;
	private ZonedDecimalData prmAmount = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData totalPremiumAmount = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private Map<Integer,List<String>> data = new LinkedHashMap<Integer,List<String>>();
	private List<String> totalPremium = new ArrayList<String>();
	private List<String> billcdDate = new ArrayList<String>();
	private List<String> premiumamount = new ArrayList<String>();
	private List<String> billamount = new ArrayList<String>();//ILIFE-5975
	private List<String> billccy = new ArrayList<String>();
	private List<String> cntccy = new ArrayList<String>();
	
	boolean checkdata;
	String statuz;
	int offSet;
	String record;
	public Pcpllins() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

public void main()
{
	if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
		offSet = 1;
		data.clear();
		totalPremium.clear();
		billcdDate.clear();
		premiumamount.clear();
		billamount.clear();
		wsaaStoreRrn.set(letcIO.getRrn());
		chdrnum = letcIO.getRdocnum().trim();
		chdrcoy = letcIO.getRdoccoy().trim();
		readChdrpf();
		getLins();
	}
	offset.set(pcpdatarec.fldOffset);
	if (isGT(pcpdatarec.fldOccur,data.get(offset.toInt()).size())) {
		pcpdatarec.statuz.set("GEND");
		exitProgram();
	}
	record = data.get(offset.toInt()).get(pcpdatarec.fldOccur.toInt()-1);
	pcpdatarec.data.set(record);	
	pcpdatarec.dataLen.set(record.length());
	pcpdatarec.statuz.set(Varcom.oK);
	/*EXIT*/
	exitProgram();
}
public void readChdrpf()
{
	chdrIO.setParams(SPACE);
	chdrIO.setChdrnum(chdrnum);
	chdrIO.setChdrcoy(chdrcoy);
	chdrIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars,chdrIO);
	if(!oK.equals(chdrIO.getStatuz().trim()))
	{
		fatalError600();
	}
	else
	{
		ptDate = chdrIO.getPtdate().toInt();
	}
}


public void getLins()
{
	checkdata = true;
	linsrnlIO.setParams(SPACE);
	linsrnlIO.setChdrnum(letcIO.getRdocnum());
	linsrnlIO.setChdrcoy(letcIO.getRdoccoy());
	linsrnlIO.setInstfrom(ptDate);
	linsrnlIO.setFunction(Varcom.begn);
	linsrnlIO.setFormat(linsrnlrec);
	readLins();
	//TMLII-1724
	totalPremiumAmount.set(0);
	while(checkdata)
	{	
		if("O".equals(linsrnlIO.getPayflag().trim()))
		{
			//TMLII-1724
//			premiumamount.add(linsrnlIO.getCbillamt().toString());
			prmAmount.set(linsrnlIO.getCbillamt());
			premiumamount.add(prmAmount.toString());
			
			datcon1rec.intDate.set(linsrnlIO.getBillcd());
			datcon1rec.function.set(Varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			billcdDate.add(datcon1rec.extDate.trim());
			
			//TMLII-1724
//			totalPremiumAmount = totalPremiumAmount+linsrnlIO.getCbillamt().getbigdata().doubleValue();
			totalPremiumAmount.add(prmAmount);
		}
		linsrnlIO.setFunction(Varcom.nextr);
		readLins();
	}
	billamount.add(prmAmount.toString());//ILIFE-5975
	totalPremium.add(totalPremiumAmount.toString());
	billccy.add(chdrIO.getBillcurr().toString());
	cntccy.add(chdrIO.getCntcurr().toString());
	data.put(offSet++, totalPremium);
	data.put(offSet++,billcdDate);
	data.put(offSet++,cntccy);
	data.put(offSet++,premiumamount);	
	data.put(offSet++,billccy);
	data.put(offSet++,billamount);//ILIFE-5975
}

protected void readLins()
{
	SmartFileCode.execute(appVars, linsrnlIO);
	statuz = linsrnlIO.getStatuz().trim();
	if(!oK.equals(statuz)
			&& !endp.equals(statuz))
			{
		fatalError600();	
			}
	if(!oK.equals(statuz)
|| !chdrnum.equals(linsrnlIO.getChdrnum().trim())
		||!chdrcoy.equals(linsrnlIO.getChdrcoy().trim())
		|| ptDate > linsrnlIO.getInstfrom().toInt())
	{
		checkdata = false;
	}
	
	
}

protected void fatalError600()
{
	/*FATAL*/
	syserrrec.subrname.set(wsaaSubrname);
	pcpdatarec.statuz.set(Varcom.bomb);
	if (isNE(syserrrec.statuz,SPACES)
	|| isNE(syserrrec.syserrStatuz,SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT*/
	exitProgram();
}

}
