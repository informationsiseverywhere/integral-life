/*
 * File: Pcplbnfyeq.java
 * Date: 30 August 2009 1:00:12
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLBNFYEQ.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.enquiries.dataaccess.BnfyenqTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

//ILAE-63 START by dpuhawan
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
//ILAE-63 END

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Beneficiary Number
*  002  Beneficiary Name
*  003  Beneficiary Relation	////TMLII-561
*  004  Beneficiary Percentage	////TMLII-561
*  005  Beneficiary Address 1	////
*  006  Beneficiary Address 2	////
*  007  Beneficiary Address 3	////
*  008  Beneficiary Address 4	////
*  009  Beneficiary Postal Code	////
*  010  Beneficiary Salutation	////
*  011  Beneficiary Address 5	//TMLII-1272
*  012  Beneficiary First Name //TMLII-1708
*  013  Beneficiary Last Name //TMLII-1708
*
*  Overview.
*  ---------
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplbnfyeq extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLBNFYEQ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0).init(ZERO);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(2, 0).init(13);
	private PackedDecimalData wsaaIx = new PackedDecimalData(5, 0);
	private String wsaaFirstTime = "Y";
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(2400).init(SPACES);
	private FixedLengthStringData wsaaBnyclt = new FixedLengthStringData(8);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (130, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String bnfyenqrec = "BNFYENQREC";
	private BnfyenqTableDAM bnfyenqIO = new BnfyenqTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	
	//ILAE-63 START by dpuhawan
	private DescTableDAM descIO = new DescTableDAM();
	private String t5663 = "T5663";
	private String t3583 = "T3583";
	private FixedLengthStringData bnfyRelation = new FixedLengthStringData(30);
	private CltsTableDAM cltsIO = new CltsTableDAM();
	//ILAE-63 END

	public Pcplbnfyeq() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		/* For an occurance field of more than 1, the PCPD-IDCODE-COUNT*/
		/* works fine i.e. only read a time of the database. However, if*/
		/* the 2nd field comes in contact, then this PCPD-IDCODE-COUNT*/
		/* will be updated by PCP400 thus reading the database again.*/
		/* For efficiency, this database should not be read again as the*/
		/* data has already been extracted by the first field and are*/
		/* stored in the array.*/
		/* To avoid this, we nned to check the RRN as well.*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)
		&& isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			for (wsaaIx.set(1); !(isGT(wsaaIx,pcpdatarec.fldOccurMax)
			|| isEQ(bnfyenqIO.getStatuz(),varcom.endp)); wsaaIx.add(1)){
				formatBnfy200();
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
			para101();
		}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaStoreRrn.set(letcIO.getRrn());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		bnfyenqIO.setStatuz(varcom.oK);
		bnfyenqIO.setFormat(bnfyenqrec);
		if (isEQ(pcpdatarec.function,varcom.nextr)) {
			return ;
		}
		bnfyenqIO.setChdrcoy(letcIO.getChdrcoy());
		bnfyenqIO.setChdrnum(letcIO.getChdrnum());
		bnfyenqIO.setBnyclt(SPACES);
		bnfyenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bnfyenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfyenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		
	}

protected void formatBnfy200()
	{
		/*FORMAT*/
		callBnfyenqioSeq300();
		if (isEQ(bnfyenqIO.getStatuz(),varcom.oK)) {
			getBeneficiary400();
		}
		/*EXIT*/
	}

protected void callBnfyenqioSeq300()
	{
		/*BEGIN*/
		SmartFileCode.execute(appVars, bnfyenqIO);
		if (isNE(bnfyenqIO.getStatuz(),varcom.oK)
		&& isNE(bnfyenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(bnfyenqIO.getStatuz());
			fatalError600();
		}
		if (isEQ(bnfyenqIO.getStatuz(),varcom.endp)
		|| isNE(bnfyenqIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(bnfyenqIO.getChdrnum(),letcIO.getChdrnum())) {
			bnfyenqIO.setStatuz(varcom.endp);
			return ;
		}
		bnfyenqIO.setFunction(varcom.nextr);
		wsaaCounter.add(1);
		/*EXIT*/
	}

protected void getBeneficiary400()
	{
		begin410();
		prepareField315();
		
		//ILAE-63 START by dpuhawan
		//TMLII-561 start //
		getBeneficiaryRelationPC();
		//TMLII-561 end //
		
		//Start TMLI Requirement LT-01-012
		getBeneficiaryAddr1();
		getBeneficiaryAddr2();
		getBeneficiaryAddr3();
		getBeneficiaryAddr4();
		getBeneficiaryPCode();
		getBeneficiarySalut();
		//TMLII-1272
		getBeneficiaryAddr5();
		//End TMLI Requirement LT-01-012
		
		//TMLII-1708 start
		getBeneficiaryFirstName();
		getBeneficiaryLastName();
		//TMLII-1708 end
		//ILAE-63 END
	}

protected void begin410()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1); //offset = 1 Owner Number
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(pcpdatarec.fsuco);
		namadrsrec.clntNumber.set(bnfyenqIO.getBnyclt());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}

protected void prepareField315()
	{
		/*  Build the Owners Number*/
		wsaaBnyclt.set(bnfyenqIO.getBnyclt());
		fldlen[offset.toInt()].set(length(wsaaBnyclt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBnyclt);
		/*  Build the Owners Names field*/
		offset.add(1); //offset = 2 Owners Name
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

//ILAE-63 START by dpuhawan
protected void getBeneficiaryRelationPC() {
	descIO.setDataKey(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(bnfyenqIO.getChdrcoy());
	descIO.setDesctabl(t5663);
	descIO.setDescitem(bnfyenqIO.getBnyrln());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(descIO.getStatuz());
		syserrrec.dbparams.set(descIO.getParams());
		fatalError600();
	}
	if (isNE(descIO.getStatuz(),varcom.mrnf)) {
		bnfyRelation.set(descIO.getLongdesc());
	}
	else 
	{
		bnfyRelation.set(SPACES);
	}
	offset.add(1); //offset = 3 Relation
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(bnfyRelation, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(bnfyRelation, 1, fldlen[offset.toInt()]));
	
	offset.add(1); //offset = 4 Percentage
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(bnfyenqIO.getBnypc(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(bnfyenqIO.getBnypc(), 1, fldlen[offset.toInt()]));
}

protected void getBeneficiaryAddr1()
{
	offset.add(1); //Offset = 5 Address1
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr1, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr1, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryAddr2()
{
	offset.add(1); //Offset = 6 Address2
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr2, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr2, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryAddr3()
{
	offset.add(1); //Offset = 7 Address3
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr3, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr3, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryAddr4()
{
	offset.add(1); //Offset = 8 Address4
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr4, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr4, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryPCode()
{
	offset.add(1); //Offset = 9 Postal Code
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.pcode, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.pcode, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiarySalut()
{
	cltsIO.setDataArea(SPACES);
	cltsIO.setClntpfx(namadrsrec.clntPrefix);
	cltsIO.setClntcoy(namadrsrec.clntCompany);
	cltsIO.setClntnum(namadrsrec.clntNumber);
	cltsIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, cltsIO);
	if(isNE(cltsIO.getStatuz(), varcom.oK)){
		syserrrec.params.set(cltsIO.getParams());
		syserrrec.statuz.set(cltsIO.getStatuz());
		fatalError600();
	}
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(namadrsrec.clntCompany);
	descIO.setLanguage(pcpdatarec.language);
	descIO.setDesctabl(t3583);
	descIO.setDescitem(cltsIO.getSalutl());
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)) {
		descIO.setLongdesc(SPACES);
	}
	offset.add(1); //Offset = 10 Owner Salutation
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(descIO.getShortdesc(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getShortdesc(), 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

//TMLII-1272 START
protected void getBeneficiaryAddr5()
{
	offset.add(1); //Offset = 11 Address5
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr5, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr5, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryFirstName()
{
	offset.add(1); //Offset = 12 First Name
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set("CN");
	namadrsrec.clntCompany.set(pcpdatarec.fsuco);
	namadrsrec.clntNumber.set(bnfyenqIO.getBnyclt());
	namadrsrec.language.set(pcpdatarec.language);
	namadrsrec.function.set("GIVNM");
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.params.set(namadrsrec.namadrsRec);
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
	
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}

protected void getBeneficiaryLastName()
{
	offset.add(1); //Offset = 13 Last Name
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set("CN");
	namadrsrec.clntCompany.set(pcpdatarec.fsuco);
	namadrsrec.clntNumber.set(bnfyenqIO.getBnyclt());
	namadrsrec.language.set(pcpdatarec.language);
	namadrsrec.function.set("PLNAM");
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.params.set(namadrsrec.namadrsRec);
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError600();
	}
	
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	/*EXIT*/
}
//ILAE-63 END
}
