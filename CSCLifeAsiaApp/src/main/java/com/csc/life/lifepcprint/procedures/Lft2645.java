/*
 * File: Lft2645.java
 * Date: 29 August 2009 22:58:01
 * Author: Quipoz Limited
 * 
 * Class transformed from LFT2645.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This subroutin returns T2645 item keys for Life Letter
* requests.
*
***********************************************************************
* </pre>
*/
public class Lft2645 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("LFT2645");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	public Lft2645() {
		super();
	}

public void mainline(Object... parmArray)
	{
		itemIO.setParams(convertAndSetParam(itemIO.getParams(), parmArray, 1));
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 0));
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(itemIO.getParams(), parmArray, 1);
		setReturningParam(letcIO.getParams(), parmArray, 0);
	}

protected void mainline1000()
	{
		main1010();
		exit1090();
	}

protected void main1010()
	{
		itemIO.setItemitem(SPACES);
		if (isEQ(letcIO.getRdocpfx(),"CH")) {
			chdrIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
			chdrIO.setChdrpfx(letcIO.getRdocpfx());
			chdrIO.setChdrcoy(letcIO.getRdoccoy());
			chdrIO.setChdrnum(letcIO.getRdocnum());
			chdrIO.setCurrfrom(varcom.vrcmMaxDate);
			chdrIO.setTranno(9999);
			chdrIO.setValidflag("1");
			SmartFileCode.execute(appVars, chdrIO);
			if (isNE(chdrIO.getStatuz(),varcom.oK)
			&& isNE(chdrIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(chdrIO.getParams());
				fatalError600();
			}
			if (isEQ(chdrIO.getStatuz(),varcom.oK)
			&& (isNE(chdrIO.getChdrpfx(),letcIO.getRdocpfx())
			|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
			|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum()))) {
				syserrrec.params.set(chdrIO.getParams());
				fatalError600();
			}
			itemIO.getItemitem().setSub1String(1, 3, chdrIO.getCnttype());
			itemIO.getItemitem().setSub1String(4, 5, "U");
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
			if (letcokcpy.fsuSweepupSubmenu.isTrue()) {
				itemIO.getItemitem().setSub1String(1, 3, "***");
				itemIO.getItemitem().setSub1String(4, 5, "*");
			}
			if (letcokcpy.lifeUnderwriting.isTrue()) {
				itemIO.getItemitem().setSub1String(1, 3, "***");
				itemIO.getItemitem().setSub1String(4, 5, "U");
			}
		}
	}

protected void exit1090()
	{
		exitProgram();
	}

protected void fatalError600()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("4");
		}
		else {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		itemIO.setStatuz(varcom.bomb);
		letcIO.setStatuz(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
