/*
 * File: Pcplhpad.java
 * Date: 30 August 2009 1:01:44
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLHPAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* Offset. Description...............................................
*
*  001  Proposal Date
*  002  Proposal Received Date
*  003  Policy Issue Date
*  004  Policy Issue Year
*  005  U/W Decision Date
*  006  First Issue Date
*  007  Doctor Client Number
*  008  Non-forfeiture option
*  009  Non-forfeiture Description
*  010  Doctor's Name
*  011  Doctor's Address1
*  012  Doctor's Address2
*  013  Doctor's Address3
*  014  Doctor's Address4
*  015  Doctor's Address5
*  016  Doctor's Name - for introduction
*  017  Proposal Date - Format 2
*
*****************************************************************
* </pre>
*/
public class Pcplhpad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLHPAD";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(16).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYr = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDatetext1 = new FixedLengthStringData(1).isAPartOf(wsaaDatetext, 0);
	private FixedLengthStringData wsaaDatetext2 = new FixedLengthStringData(21).isAPartOf(wsaaDatetext, 1);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(2000).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

		/*    03  WSAA-START-AND-LENGTH   OCCURS 20.                       */
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (25, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String hpadrec = "HPADREC";
		/* TABLES */
	private static final String th586 = "TH586";
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		formatDoctorAddr1209a, 
		formatProposalDate2209a, 
		exit299
	}

	public Pcplhpad() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getHpadAndFormat200();
		}
		if (isEQ(pcpdatarec.fldOffset,1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getHpadAndFormat200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					getData200();
					formatPropDate201();
					formatRecvDate202();
					formatPolIssueDte203();
					formatPolIssueYr204();
					formatUwDecisionDte205();
					formatIssueDate206();
					formatDoctorClntnum207();
					formatNonForfDesc209();
					formatDoctorName209a();
				case formatDoctorAddr1209a: 
					formatDoctorAddr1209a();
					formatDoctorAddr2209a();
					formatDoctorAddr3209a();
					formatDoctorAddr4209a();
					formatDoctorAddr5209a();
					formatDoctorName2209a();
				case formatProposalDate2209a: 
					formatProposalDate2209a();
					formatPolDeliveryMode218();
					formatPolDespatchDate219();
					formatPolAckDate220();
					formatPolReminderDate221();
					formatPolDeemedRcvDate222();
					formatPolNextActDate223();
				case exit299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getData200()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setFunction(varcom.readr);
		hpadIO.setStatuz(varcom.oK);
		hpadIO.setChdrcoy(letcIO.getChdrcoy());
		hpadIO.setChdrnum(letcIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			fatalError900();
		}
		if (isEQ(hpadIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit299);
		}
	}

protected void formatPropDate201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(1);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		datcon1rec.intDate.set(hpadIO.getHpropdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatRecvDate202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(hpadIO.getHprrcvdt());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatPolIssueDte203()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(hpadIO.getHissdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatPolIssueYr204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaDate.set(hpadIO.getHissdte());
		fldlen[offset.toInt()].set(length(wsaaYr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaYr);
	}

protected void formatUwDecisionDte205()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(hpadIO.getHuwdcdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatIssueDate206()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(hpadIO.getHoissdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatDoctorClntnum207()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(hpadIO.getZdoctor()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], hpadIO.getZdoctor());
		/*FORMAT-NON-FORF-OPTION*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(hpadIO.getZnfopt()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], hpadIO.getZnfopt());
	}

protected void formatNonForfDesc209()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(th586);
		descIO.setDescitem(hpadIO.getZnfopt());
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(hpadIO.getChdrcoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatDoctorName209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(hpadIO.getZdoctor(),SPACES)) {
			namadrsrec.name.set(SPACES);
			namadrsrec.addr1.set(SPACES);
			namadrsrec.addr2.set(SPACES);
			namadrsrec.addr3.set(SPACES);
			namadrsrec.addr4.set(SPACES);
			namadrsrec.addr5.set(SPACES);
			namadrsrec.pcode.set(SPACES);
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
			goTo(GotoLabel.formatDoctorAddr1209a);
		}
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.clntNumber.set(hpadIO.getZdoctor());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError900();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorAddr1209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr1, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr1, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorAddr2209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr2, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr2, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorAddr3209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr3, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr3, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorAddr4209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr4, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr4, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorAddr5209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr5, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.addr5, 1, fldlen[offset.toInt()]));
	}

protected void formatDoctorName2209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(hpadIO.getZdoctor(),SPACES)) {
			namadrsrec.name.set(SPACES);
			namadrsrec.addr1.set(SPACES);
			namadrsrec.addr2.set(SPACES);
			namadrsrec.addr3.set(SPACES);
			namadrsrec.addr4.set(SPACES);
			namadrsrec.addr5.set(SPACES);
			namadrsrec.pcode.set(SPACES);
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
			goTo(GotoLabel.formatProposalDate2209a);
		}
		namadrsrec.function.set("PYNMD");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError900();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatProposalDate2209a()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.intDate1.set(hpadIO.getHpropdte());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			datcon6rec.intDate2.set(datcon6rec.statuz);
		}
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetext1,SPACES)) {
			wsaaDatetext.set(wsaaDatetext2);
		}
		fldlen[offset.toInt()].set(length(wsaaDatetext));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDatetext);
	}

protected void formatPolDeliveryMode218()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(hpadIO.getDlvrmode()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], hpadIO.getDlvrmode());
	}

protected void formatPolDespatchDate219()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(hpadIO.getDespdate(), varcom.vrcmMaxDate)
		|| isEQ(hpadIO.getDespdate(), ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			datcon1rec.intDate.set(hpadIO.getDespdate());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void formatPolAckDate220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(hpadIO.getPackdate(), varcom.vrcmMaxDate)
		|| isEQ(hpadIO.getPackdate(), ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			datcon1rec.intDate.set(hpadIO.getPackdate());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void formatPolReminderDate221()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(hpadIO.getRemdte(), varcom.vrcmMaxDate)
		|| isEQ(hpadIO.getRemdte(), ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			datcon1rec.intDate.set(hpadIO.getRemdte());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void formatPolDeemedRcvDate222()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(hpadIO.getDeemdate(), varcom.vrcmMaxDate)
		|| isEQ(hpadIO.getDeemdate(), ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			datcon1rec.intDate.set(hpadIO.getDeemdate());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void formatPolNextActDate223()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(hpadIO.getNextActDate(), varcom.vrcmMaxDate)
		|| isEQ(hpadIO.getNextActDate(), ZERO)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			datcon1rec.intDate.set(hpadIO.getNextActDate());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
