/*
 * File: Pcplmprmal.java
 * Date: 16 Sep 2013 1:02:23
 * Author: Sankaran
*/
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.UnltrevTableDAM;
import com.csc.life.reassurance.dataaccess.CovtrasTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkpfTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Premium Main Total Allocation   
*  002  Premium Alloc Fund          -> LPRMFND 	 	}
*  003  Premium Alloc Percent/Amt   -> LPRMPERC 	} occur 55 times (it can handle upto minimum of 5 coverages having 10 funds for total percentage allocation we are using the Premium Alloc Fund as
*  "Total" and Premium Alloc Percent/Amt as total allocation amount to funds in a coverage)
*  
*
*
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
***********************************************************************
* </pre>
*/
public class Pcplmprmal extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLMPRMAL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaTotalAllocationPrct = new PackedDecimalData(16, 2);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99-");
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(4200).init(SPACES);
	private String wsaaFirstTime = "Y";
	private String wsaaFirstTimeCoverage = "Y";
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(330);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(55, 6, filler, 0);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0).init(ZERO);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */	
	private String unllinkrec = "UNLLINKREC";
	private String descrec = "DESCREC";
	private String t5515 = "T5515";
	private String t5687 = "T5687";
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
		/*Logical over MLTHPF*/
	//private UnltrevTableDAM unltrevIO = new UnltrevTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private CovtrasTableDAM covtrasIO = new CovtrasTableDAM();
	private String covtrasrec = "COVTRASREC";
	private DescTableDAM descIO = new DescTableDAM();
	private DescTableDAM descIO1 = new DescTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();	
	//private UlnkpfTableDAM unllinkIO = new UlnkpfTableDAM();
	private UlnkTableDAM unllinkIO = new UlnkTableDAM();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(4, 0); 
	 
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090,
		exit109,
		exit399
	}

	public Pcplmprmal() {
		super();		
	}

public void mainline(Object... parmArray)
	{
	letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
	pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
	try {
		main000();
	}
	catch (COBOLExitProgramException e) {
	}

	setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000() {
		try {
			main010();
		} catch (GOTOException e) {
		} finally {
			exit090();
		}
}

protected void main010()
	{
	if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)
			&& isNE(letcIO.getRrn(),wsaaStoreRrn)) {
				initialise100();
				format201();
				while ( !(isEQ(unllinkIO.getStatuz(),varcom.endp))) 
				{
					
					formatUnltrev200();
					wsaaIndex.add(1);	
					
							if (isEQ(wsaaIndex ,3)){							 
								break;
							}
				}
			}
			format201();
			if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				goTo(GotoLabel.exit090);
			}
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
			pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
			pcpdatarec.dataLen.set(fldlen[offset.toInt()]);;
	}

protected void exit090()
{
	exitProgram();
}

protected void initialise100()
{
	try {
		para101();
	}
	catch (GOTOException e){
	}
}

protected void para101()
{
	wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
	wsaaStoreRrn.set(letcIO.getRrn());
	wsaaFirstTime = "Y";
	wsaaFirstTimeCoverage = "Y";
	wsaaTotalAllocationPrct.set(ZERO);
	wsaaSub.set(ZERO);
	offset.set(ZERO);	 
	wsaaCounter.set(ZERO);	
	unllinkIO.setStatuz(varcom.oK);	
	unllinkIO.setFormat(unllinkrec);
	if (isEQ(pcpdatarec.function,varcom.nextr)) {
		goTo(GotoLabel.exit109);
	}	
	wsaaIndex.set(1);
	//unllinkIO.chdrcoy.set(letcIO.getChdrcoy());
	//unllinkIO.chdrnum.set(letcIO.getChdrnum());	
	unllinkIO.setParams(SPACES);
	unllinkIO.setFunction(varcom.begn);	
}


protected void formatUnltrev200()
{
	/*FORMAT*/
	callUnltrevioSeq300();
	if (isEQ(unllinkIO.getStatuz(),varcom.oK)) {
		 getDetails200();
		formatAllFields();
	}
	/*EXIT*/
}

protected void callUnltrevioSeq300()
{
	try {
		begin310();
	}
	catch (GOTOException e){
	}
}

protected void begin310()
{
	unllinkIO.setStatuz(varcom.oK);
	unllinkIO.chdrcoy.set(letcIO.getChdrcoy());
	unllinkIO.chdrnum.set(letcIO.getChdrnum());	
	SmartFileCode.execute(appVars, unllinkIO);
	if (isNE(unllinkIO.getStatuz(),varcom.oK)
	&& isNE(unllinkIO.getStatuz(),varcom.endp)) {
		syserrrec.statuz.set(unllinkIO.getStatuz());
		fatalError900();
	}
	if (isEQ(unllinkIO.getStatuz(),varcom.endp)
	|| isNE(unllinkIO.chdrcoy,letcIO.getChdrcoy())
	|| isNE(unllinkIO.chdrnum,letcIO.getChdrnum())) {
		unllinkIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit399);
	}
	unllinkIO.setFunction(varcom.begn);
}

// This function is used to get the coverage description to which funds have been allocated, its going to be repeated in XML 
protected void getDetails200()
	{
	covtrasIO.setParams(SPACES);
	covtrasIO.setChdrcoy(unllinkIO.chdrcoy);
	covtrasIO.setChdrnum(unllinkIO.chdrnum);
	covtrasIO.setLife(unllinkIO.life);
	covtrasIO.setCoverage(unllinkIO.coverage);
	covtrasIO.setRider(unllinkIO.rider);
	covtrasIO.setFunction(varcom.readr);
	covtrasIO.setFormat(covtrasrec);
	SmartFileCode.execute(appVars, covtrasIO);
	
	if(isEQ(covtrasIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(covtrasIO.getParams());
		//fatalError900();
	}
	readT5687500();
	}


//This function is used to format all the fields 1. Coverage description 2. Fund Description 3. Fund allocation amount
protected void formatAllFields() {
	wsaaTotalAllocationPrct.set(ZERO);
	wsaaFirstTimeCoverage = "Y";
	 for (wsaaSub.set(1); !(isGT(wsaaSub,10) || isEQ(unllinkIO.getUalfnd(wsaaSub),SPACES)); wsaaSub.add(1))
	 {
	 			wsaaTotalAllocationPrct.add(unllinkIO.getUalprc(wsaaSub));	  	
	 			formatUnltrev300();
	 } 	 	
	 formatTotalAllocationAmount();
		
}

protected void formatUnltrev300() 
{
	if (isEQ(wsaaIndex,2))
	{ 
	format202();
	format203();	
	}
}
 
protected void format201()
{
	 
		offset.set(1);
		strpos[offset.toInt()].set(1);	fldlen[offset.toInt()].set(length(wsaaTotalAllocationPrct));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotalAllocationPrct);
}

// For formatting the Fund Description
protected void format202() {
	wsaaCounter.add(1);
	
	if (isEQ(offset,ZERO)) {
		offset.set(1);
		strpos[offset.toInt()].set(1);		 
	}
	else 
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	}
	descIO.setDataKey(SPACES);
	descIO.setDescpfx("IT");	
	descIO.setDesccoy(unllinkIO.chdrcoy);
	descIO.setDesctabl(t5515);
	descIO.setDescitem(unllinkIO.getUalfnd(wsaaSub));
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError900();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError900();
	}
	else {
		fldlen[offset.toInt()].set(length(descIO.getLongdesc()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}
}

//For formatting the fund allocation amount
protected void format203()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaAmount.set(unllinkIO.getUalprc(wsaaSub));
	fldlen[offset.toInt()].set(length(wsaaAmount));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
}

//This function is hardcoding the fund description as "Total" and Fund allocation amount is the total allocation to the Coverage
protected void formatTotalAllocationAmount() {
	if (isEQ(wsaaIndex,2))
	{
		wsaaCounter.add(1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length("Total"));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "Total");
		
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		/*for (int i=1; i<=10 && isNE(unltrevIO.getUalfnd(i),SPACES); i++) {
			wsaaTotalAllocationPrct.add(unltrevIO.getUalprc(i));
		}*/
		wsaaAmount.set(wsaaTotalAllocationPrct);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}
}

protected void readT5687500()
{
	getT5687Desc510();
}

protected void getT5687Desc510()
{
	descIO1.setDescpfx(smtpfxcpy.item);
	descIO1.setDesccoy(covtrasIO.getChdrcoy());
	descIO1.setDesctabl(t5687);
	descIO1.setDescitem(covtrasIO.getCrtable());
	descIO1.setItemseq(SPACES);
	descIO1.setLanguage(pcpdatarec.language);
	descIO1.setFunction(varcom.readr);
	descIO1.setFormat(descrec);
	SmartFileCode.execute(appVars, descIO1);
	if (isNE(descIO1.getStatuz(),varcom.oK)
	&& isNE(descIO1.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(descIO1.getStatuz());
		syserrrec.params.set(descIO1.getParams());
		fatalError900();
	}
	if (isEQ(descIO1.getStatuz(),varcom.mrnf)) {
		descIO1.setLongdesc(SPACES);
	}
}
protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
