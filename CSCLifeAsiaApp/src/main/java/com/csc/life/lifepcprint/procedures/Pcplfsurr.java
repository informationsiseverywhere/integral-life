/*
 * File: Pcplfsurr.java
 * Date: 30 August 2009 1:01:27
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLFSURR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Surrender Value
*
*
***********************************************************************
* </pre>
*/
public class Pcplfsurr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLFSURR";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaOrigamt = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaSacstype = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 3);
	private FixedLengthStringData wsaaSurrCurr = new FixedLengthStringData(3).isAPartOf(wsaaOtherKeys, 5);
	private String wsaaReadSuspamt = "";
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 6 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (2, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String acmvrec = "ACMVREC";
	private static final String rtrnrec = "RTRNREC";
	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplfsurr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			getLoanAndFormat200();
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.*/
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		wsaaReadSuspamt = "N";
		/*EXIT*/
	}

protected void getLoanAndFormat200()
	{
		start200();
		format201();
	}

protected void start200()
	{
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setRldgcoy(letcIO.getRdoccoy());
		acmvsacIO.setSacscode(wsaaSacscode);
		acmvsacIO.setSacstyp(wsaaSacstype);
		acmvsacIO.setRldgacct(letcIO.getRdocnum());
		acmvsacIO.setOrigcurr(wsaaSurrCurr);
		acmvsacIO.setFormat(acmvrec);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		readAcmvsac300();
		while ( !(isEQ(acmvsacIO.getStatuz(), varcom.endp)
		|| isNE(acmvsacIO.getRldgcoy(), letcIO.getRdoccoy())
		|| isNE(acmvsacIO.getSacscode(), wsaaSacscode)
		|| isNE(acmvsacIO.getRldgacct(), letcIO.getRdocnum())
		|| isNE(acmvsacIO.getSacstyp(), wsaaSacstype)
		|| isNE(acmvsacIO.getOrigcurr(), wsaaSurrCurr)
		|| isEQ(wsaaReadSuspamt, "Y"))) {
			if (isEQ(acmvsacIO.getTranno(), letcIO.getTranno())) {
				wsaaReadSuspamt = "Y";
			}
			else {
				acmvsacIO.setFunction(varcom.nextr);
				readAcmvsac300();
			}
		}
		
		if (isEQ(wsaaReadSuspamt, "Y")) {
			wsaaOrigamt.set(acmvsacIO.getOrigamt());
		}
		else {
			readRtrn310();
		}
	}

protected void format201()
	{
		/*    Full Surrender Amount*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(wsaaOrigamt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOrigamt);
		/*FORMAT*/
		/*    Currency*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaSurrCurr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSurrCurr);
		/*EXIT*/
	}

protected void readAcmvsac300()
	{
		/*ACMVSAC*/
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(), varcom.oK)
		&& isNE(acmvsacIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvsacIO.getParams());
			syserrrec.statuz.set(acmvsacIO.getStatuz());
			letcIO.setStatuz(acmvsacIO.getStatuz());
			fatalError900();
		}
		/*EXIT*/
	}

protected void readRtrn310()
	{
		rtrn310();
	}

protected void rtrn310()
	{
		/* Format Surrender amounts and date of surrender.*/
		rtrnsacIO.setParams(SPACES);
		rtrnsacIO.setRldgcoy(letcIO.getRdoccoy());
		rtrnsacIO.setSacscode(wsaaSacscode);
		rtrnsacIO.setRldgacct(letcIO.getRdocnum());
		rtrnsacIO.setSacstyp(wsaaSacstype);
		rtrnsacIO.setOrigccy(wsaaSurrCurr);
		rtrnsacIO.setFormat(rtrnrec);
		rtrnsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnsacIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		readRtrnsac320();
		while ( !(isEQ(rtrnsacIO.getStatuz(), varcom.endp)
		|| isNE(rtrnsacIO.getRldgcoy(), letcIO.getRdoccoy())
		|| isNE(rtrnsacIO.getSacscode(), wsaaSacscode)
		|| isNE(rtrnsacIO.getRldgacct(), letcIO.getRdocnum())
		|| isNE(rtrnsacIO.getSacstyp(), wsaaSacstype)
		|| isNE(rtrnsacIO.getOrigccy(), wsaaSurrCurr)
		|| isEQ(wsaaReadSuspamt, "Y"))) {
			if (isEQ(rtrnsacIO.getTranno(), letcIO.getTranno())) {
				wsaaReadSuspamt = "Y";
			}
			else {
				rtrnsacIO.setFunction(varcom.nextr);
				readRtrnsac320();
			}
		}
		
		if (isEQ(wsaaReadSuspamt, "Y")) {
			wsaaOrigamt.set(rtrnsacIO.getOrigamt());
		}
	}

protected void readRtrnsac320()
	{
		/*RTRNSAC*/
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(), varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			syserrrec.statuz.set(rtrnsacIO.getStatuz());
			letcIO.setStatuz(rtrnsacIO.getStatuz());
			fatalError900();
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
