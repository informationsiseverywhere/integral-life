/*
 * File: Pcpllniovd.java
 * Date: 30 August 2009 1:02:16
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLNIOVD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.contractservicing.dataaccess.TloantrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Loan Amount    -> LLNAMTOD }
*  002  Loan Date From -> LLNOVDFR } in T2636
*  003  Loan Date To   -> LLNOVDTO }
*  004  Loan Interest Amount
*  005  Loan Interest Overdue Amount
*  006  Loan Overdue Period
*  007  Loan Number
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcpllniovd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLLNIOVD";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaOrigamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOrigamt1 = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaIntovd = new ZonedDecimalData(11, 2).setPattern("ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaLoanamt = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private ZonedDecimalData wsaaLoanNo = new ZonedDecimalData(2, 0).isAPartOf(wsaaOtherKeys, 1).setUnsigned();
	private FixedLengthStringData wsaaLoanNoR = new FixedLengthStringData(2).isAPartOf(wsaaLoanNo, 0, REDEFINE);
	private FixedLengthStringData wsaaLoanType = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 3);
	private ZonedDecimalData wsaaLoanTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOtherKeys, 4).setUnsigned();
	private ZonedDecimalData wsaaFromDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaToDate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaOvdPrd = new ZonedDecimalData(3, 0).setPattern("ZZ9");
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 6 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (7, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* TABLES */
	private static final String t5645 = "T5645";
		/* FORMATS */
	private static final String acmvlonrec = "ACMVLONREC";
	private static final String tloantrrec = "TLOANTRREC";
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TloantrTableDAM tloantrIO = new TloantrTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		format220
	}

	public Pcpllniovd() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getLoanAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
		readT5645102();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		wsaaIntovd.set(ZERO);
		wsaaLoanamt.set(ZERO);
		wsaaOvdPrd.set(ZERO);
	}

protected void readT5645102()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRequestCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(letcIO.getHsublet());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError900();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getLoanAndFormat200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start210();
				case format220: 
					format220();
					format230();
					format250();
					format270();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start210()
	{
		tloantrIO.setRecKeyData(SPACES);
		tloantrIO.setChdrcoy(letcIO.getChdrcoy());
		tloantrIO.setChdrnum(letcIO.getChdrnum());
		tloantrIO.setLoanNumber(wsaaLoanNo);
		tloantrIO.setValidflag("2");
		tloantrIO.setFirstTranno(wsaaLoanTranno);
		tloantrIO.setFunction(varcom.readr);
		callTloantrio300();
		if (isEQ(tloantrIO.getStatuz(),varcom.oK)) {
			wsaaFromDate.set(tloantrIO.getLastCapnDate());
		}
		else {
			wsaaFromDate.set(99990101);
		}
		tloantrIO.setRecKeyData(SPACES);
		tloantrIO.setChdrcoy(letcIO.getChdrcoy());
		tloantrIO.setChdrnum(letcIO.getChdrnum());
		tloantrIO.setLoanNumber(wsaaLoanNo);
		tloantrIO.setValidflag("1");
		tloantrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tloantrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tloantrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LOANNUMBER", "VALIDFLAG");
		callTloantrioSeq300a();
		if (isEQ(tloantrIO.getStatuz(),varcom.oK)) {
			wsaaToDate.set(tloantrIO.getLastCapnDate());
		}
		else {
			wsaaToDate.set(99990101);
		}
		datcon3rec.intDate1.set(wsaaFromDate);
		datcon3rec.intDate2.set(wsaaToDate);
		datcon3rec.frequency.set("12");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(ZERO);
		}
		else {
			wsaaOvdPrd.set(datcon3rec.freqFactor);
		}
		wsaaLoanamt.set(tloantrIO.getLoanOriginalAmount());
		if (isEQ(wsaaLoanType,"P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstyp.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstyp.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		wsaaOrigamt.set(ZERO);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(letcIO.getChdrnum());
		stringVariable1.addExpression(wsaaLoanNoR);
		stringVariable1.setStringInto(wsaaRldgacct);
		acmvlonIO.setRecKeyData(SPACES);
		acmvlonIO.setRldgcoy(letcIO.getChdrcoy());
		acmvlonIO.setSacscode(wsaaSacscode);
		acmvlonIO.setSacstyp(wsaaSacstyp);
		acmvlonIO.setRldgacct(wsaaRldgacct);
		acmvlonIO.setEffdate(tloantrIO.getLastCapnDate());
		acmvlonIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvlonIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		callAcmvlonio400();
		acmvlonIO.setFunction(varcom.nextr);
		while ( !(isEQ(acmvlonIO.getStatuz(),varcom.endp))) {
			if (isNE(acmvlonIO.getGlsign(),"-")) {
				wsaaOrigamt.add(acmvlonIO.getOrigamt());
			}
			callAcmvlonio400();
		}
		
		if (isEQ(datcon3rec.freqFactor,ZERO)) {
			goTo(GotoLabel.format220);
		}
		compute(wsaaIntovd, 6).setRounded(div(wsaaOrigamt,datcon3rec.freqFactor));
	}

protected void format220()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		if (isEQ(wsaaOrigamt,0)) {
			acmvlonIO.setOrigamt("99999999999999999");
		}
		wsaaOrigamt1.set(wsaaOrigamt);
		fldlen[offset.toInt()].set(length(wsaaOrigamt1));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOrigamt1);
	}

protected void format230()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(wsaaFromDate);
		processDate500();
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(wsaaToDate);
		processDate500();
	}

protected void format250()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaLoanamt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLoanamt);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaIntovd));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaIntovd);
	}

protected void format270()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaOvdPrd));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOvdPrd);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaLoanNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLoanNo);
		/*EXIT*/
	}

protected void callTloantrio300()
	{
		/*CALL*/
		tloantrIO.setFormat(tloantrrec);
		SmartFileCode.execute(appVars, tloantrIO);
		if (isNE(tloantrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(tloantrIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void callTloantrioSeq300a()
	{
		/*A-CALL*/
		tloantrIO.setFormat(tloantrrec);
		SmartFileCode.execute(appVars, tloantrIO);
		if (isNE(tloantrIO.getStatuz(),varcom.oK)
		&& isNE(tloantrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(tloantrIO.getParams());
			fatalError900();
		}
		if (isEQ(tloantrIO.getStatuz(),varcom.endp)
		|| isNE(tloantrIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(tloantrIO.getChdrnum(),letcIO.getChdrnum())
		|| isNE(tloantrIO.getLoanNumber(),wsaaLoanNo)
		|| isNE(tloantrIO.getValidflag(),"1")) {
			tloantrIO.setStatuz(varcom.endp);
		}
		/*A-EXIT*/
	}

protected void callAcmvlonio400()
	{
		/*CALL*/
		acmvlonIO.setFormat(acmvlonrec);
		SmartFileCode.execute(appVars, acmvlonIO);
		if (isNE(acmvlonIO.getStatuz(),varcom.oK)
		&& isNE(acmvlonIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvlonIO.getParams());
			syserrrec.statuz.set(acmvlonIO.getStatuz());
			fatalError900();
		}
		if (isEQ(acmvlonIO.getStatuz(),varcom.endp)
		|| isNE(acmvlonIO.getRldgcoy(),letcIO.getChdrcoy())
		|| isNE(acmvlonIO.getSacscode(),wsaaSacscode)
		|| isNE(acmvlonIO.getSacstyp(),wsaaSacstyp)
		|| isNE(acmvlonIO.getRldgacct(),wsaaRldgacct)
		|| isNE(acmvlonIO.getEffdate(),tloantrIO.getLastCapnDate())) {
			acmvlonIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processDate500()
	{
		call510();
	}

protected void call510()
	{
		datcon1rec.function.set(varcom.conv);
		callDatcon1600();
		datcon1rec.function.set(varcom.edit);
		callDatcon1600();
		initialize(datcon6rec.datcon6Rec);
		datcon6rec.intDate1.set(datcon1rec.intDate);
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getChdrcoy());
		datcon6rec.function.set(SPACES);
		callDatcon6700();
		fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
	}

protected void callDatcon1600()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
			fatalError900();
		}
		/*EXIT*/
	}

protected void callDatcon6700()
	{
		/*CALL*/
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon1rec.intDate);
			fatalError900();
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
