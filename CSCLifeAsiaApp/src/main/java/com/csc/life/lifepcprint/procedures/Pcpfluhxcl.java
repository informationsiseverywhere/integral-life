/*
 * File: Pcpfluhxcl.java
 * Date: 17 Oct 2013  
 * Author: Sankaran J
 * TMLII-1494
 * Class transformed from Pcpfluhxcl.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 * Usage : To retrieve all exclusion lines of the contract number
 */
package com.csc.life.lifepcprint.procedures;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;

import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;



import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.csc.smart400framework.procedures.Syserr;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Clause Note (Occurance Field)
*
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
*
*
***********************************************************************
* </pre>
*/
public class Pcpfluhxcl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPFLUHXCL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaIx = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaHxclnote = new FixedLengthStringData(75);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(360);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(60, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private String hxclrec = "HXCLREC";
		/*Exclusion Clauses Logical File*/
	private HxclTableDAM hxclIO = new HxclTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit299
	}

	public Pcpfluhxcl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isNE(hxclIO.getStatuz(),varcom.oK))) {
				getHxclAndFormat200();
			}
			
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(wsaaDataBuffer,SPACES)) {
			pcpdatarec.data.set(SPACES);
			goTo(GotoLabel.exit090);
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
		hxclIO.setParams(SPACES);
		hxclIO.setStatuz(varcom.oK);		 
		hxclIO.setChdrcoy(letcIO.getChdrcoy());
		hxclIO.setChdrnum(letcIO.getChdrnum());
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFunction(varcom.begn);
		hxclIO.setStatuz(varcom.oK);
		hxclIO.setFormat(hxclrec);
	}

protected void getHxclAndFormat200()
	{
		try {
			getData200();
			format200();
		}
		catch (GOTOException e){
		}
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)
		&& isNE(hxclIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hxclIO.getStatuz());
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(),letcIO.getChdrnum())
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			hxclIO.setStatuz(varcom.endp);
		}
		if (isEQ(hxclIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit299);
		}
		hxclIO.setFunction(varcom.nextr);
	}

protected void format200()
	{
		for (wsaaIx.set(1); !(isGT(wsaaIx,6)); wsaaIx.add(1)){
			formatClnote300();
		}
	}

protected void formatClnote300()
	{
		clnote300();
	}

protected void clnote300()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(1);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		wsaaHxclnote.set(hxclIO.getHxclnote(wsaaIx));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaHxclnote, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaHxclnote);
		wsaaCounter.add(1);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
