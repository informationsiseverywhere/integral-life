/*
 * File: Pcplcompc.java
 * Date: 30 August 2009 1:00:42
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCOMPC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Djustfy;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Djustfyrec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.CovrafiTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Effective Date
*  002  Effective Year
*  003  Commence Date
*  004  Commence Year
*  005  Coverage Description
*  006  Current  Sum Insured
*  007  Current  Premium
*  008  Current  Risk Cessation Date
*  009  Current  Premium Cessation Date
*  010  Current  Mortality Class
*  011  Previous Sum Insured
*  012  Previous Premium
*  013  Previous Risk Cessation Date
*  014  Previous Premium Cessation Date
*  015  Previous Mortality Class
*  016  Current Risk Term
*  017  Current CurrFromDate
*
*  Processing.
*  -----------
*  This routine extracts the details of the component movement.
*  It may be used by Component Change, Component Add, Component
*  Lapse etc.
*
*  NOTE:
*  -----
*  WSAA-CONSTANT refers to the number of occurance fields to
*  be extracted.
***********************************************************************
* </pre>
*/
public class Pcplcompc extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCOMPC";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(2000).init(SPACES);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0).setUnsigned();
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaPremium = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCurrFromdte = new FixedLengthStringData(10);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
    
	/*bug #ILIFE-566 start*/
  //private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	//ILIFE-1709 STARTS
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (500, 6);
	//ILIFE-1709 ENDS
	/*bug #ILIFE-566 end*/
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String covrafirec = "COVRAFIREC";
	private static final String covrrec = "COVRREC";
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t5687 = "T5687";
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrafiTableDAM covrafiIO = new CovrafiTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Djustfyrec djustfyrec = new Djustfyrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		covrDesc300, 
		exit390, 
		exit590
	}

	public Pcplcompc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
				getCovrAndFormat200();
			}
			
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaFirstTime = "Y";
		covrIO.setParams(SPACES);
		covrIO.setStatuz(varcom.oK);
		covrIO.setChdrcoy(letcIO.getRdoccoy());
		covrIO.setChdrnum(letcIO.getRdocnum());
		covrIO.setLife("01");
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
	}

protected void getCovrAndFormat200()
	{
			getData200();
		}

protected void getData200()
	{
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError900();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(covrIO.getChdrnum(),letcIO.getRdocnum())) {
			covrIO.setStatuz(varcom.endp);
			return ;
		}
		covrIO.setFunction(varcom.nextr);
		if (isNE(covrIO.getTranno(),letcIO.getTranno())) {
			return ;
		}
		wsaaCounter.add(1);
		formatCurrFields300();
		readCovrafi400();
		if (isEQ(covrafiIO.getStatuz(),varcom.endp)) {
			covrafiIO.setSumins(ZERO);
			covrafiIO.setInstprem(ZERO);
			covrafiIO.setSingp(ZERO);
			covrafiIO.setRiskCessDate(varcom.vrcmMaxDate);
			covrafiIO.setPremCessDate(varcom.vrcmMaxDate);
			covrafiIO.setMortcls(SPACES);
		}
		formatPrevFields500();
		setRiskTerm();
		setCurrFromDte();
	}

protected void formatCurrFields300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					effdate300();
					effyear300();
					commdate300();
					commyear300();
				case covrDesc300: 
					covrDesc300();
					sumins310();
					premium315();
					riskCessDate330();
					premCessDate335();
					mortality345();
					riskTerm350();
					currFromDate355();
				case exit390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void effdate300()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			goTo(GotoLabel.covrDesc300);
		}
		datcon1rec.intDate.set(covrIO.getCrrcd());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void effyear300()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(4);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(datcon1rec.extDate, 7, 4));
	}

protected void commdate300()
	{
		offset.add(1);
		datcon1rec.intDate.set(covrIO.getCurrfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void commyear300()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(4);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(datcon1rec.extDate, 7, 4));
	}

protected void covrDesc300()
	{
		/*  Coverage Description.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		readT5687700();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void sumins310()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSumins.set(covrIO.getSumins());
		fldlen[offset.toInt()].set(length(wsaaSumins));
		djustfyrec.word.set(wsaaSumins);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(20);
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz,varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}

protected void premium315()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(covrIO.getInstprem(),ZERO)) {
			wsaaPremium.set(covrIO.getInstprem());
		}
		else {
			wsaaPremium.set(covrIO.getSingp());
		}
		fldlen[offset.toInt()].set(length(wsaaPremium));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremium);
	}

protected void riskCessDate330()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrIO.getRiskCessDate());
		callDatcon1800();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit390);
		}
		else {
			datcon1rec.function.set(varcom.edit);
			callDatcon1800();
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				pcpdatarec.statuz.set(varcom.bomb);
				goTo(GotoLabel.exit390);
			}
		}
		datcon6rec.function.set(SPACES);
		datcon6rec.intDate1.set(datcon1rec.intDate);
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.function.set(SPACES);
		callDatcon6850();
		if (isEQ(datcon6rec.statuz,varcom.oK)) {
			fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
		}
		else {
			pcpdatarec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit390);
		}
	}

protected void premCessDate335()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrIO.getPremCessDate());
		callDatcon1800();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit390);
		}
		else {
			datcon1rec.function.set(varcom.edit);
			callDatcon1800();
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				pcpdatarec.statuz.set(varcom.bomb);
				goTo(GotoLabel.exit390);
			}
		}
		datcon6rec.function.set(SPACES);
		datcon6rec.intDate1.set(datcon1rec.intDate);
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.function.set(SPACES);
		callDatcon6850();
		if (isEQ(datcon6rec.statuz,varcom.oK)) {
			fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
		}
		else {
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon6rec.intDate1);
			pcpdatarec.statuz.set(varcom.bomb);
		}
	}

protected void mortality345()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(covrIO.getMortcls()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covrIO.getMortcls());
	}

protected void riskTerm350()
{
	/*     Risk Cess Term                                              */
	if (isNE(covrIO.getRiskCessTerm(), 0)) {
		wsaaRiskCessTerm.set(covrIO.getRiskCessTerm());
	}
	else {
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(covrIO.getRiskCessTerm());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			wsaaRiskCessTerm.set(ZERO);
		}
		else {
			wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		}
	}
}

protected void currFromDate355()
{
	datcon1rec.function.set(varcom.conv);
	datcon1rec.intDate.set(covrIO.getCurrfrom());
	callDatcon1800();
	if (isNE(datcon1rec.statuz, varcom.oK)) {
		wsaaCurrFromdte.set(SPACES);
	}
	else {
		wsaaCurrFromdte.set(datcon1rec.extDate);
	}
}

protected void setRiskTerm() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
	fldlen[offset.toInt()].set(length(wsaaRiskCessTerm));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskCessTerm);
}

protected void setCurrFromDte() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
	fldlen[offset.toInt()].set(length(wsaaCurrFromdte));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrFromdte);
}

protected void readCovrafi400()
	{
		covr400();
	}

protected void covr400()
	{
		covrafiIO.setRecKeyData(SPACES);
		covrafiIO.setChdrcoy(covrIO.getChdrcoy());
		covrafiIO.setChdrnum(covrIO.getChdrnum());
		covrafiIO.setLife(covrIO.getLife());
		covrafiIO.setPlanSuffix(covrIO.getPlanSuffix());
		covrafiIO.setCoverage(covrIO.getCoverage());
		covrafiIO.setRider(covrIO.getRider());
		setPrecision(covrafiIO.getTranno(), 0);
		covrafiIO.setTranno(sub(covrIO.getTranno(),1));
		covrafiIO.setFunction(varcom.endr);
		covrafiIO.setFormat(covrafirec);
		SmartFileCode.execute(appVars, covrafiIO);
		if (isNE(covrafiIO.getStatuz(),varcom.oK)
		&& isNE(covrafiIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrafiIO.getStatuz());
			syserrrec.params.set(covrafiIO.getParams());
			fatalError900();
		}
		if (isEQ(covrafiIO.getTranno(),letcIO.getTranno())) {
			covrafiIO.setTranno(letcIO.getTranno());
		}
		if (isNE(covrafiIO.getChdrcoy(),covrIO.getChdrcoy())
		|| isNE(covrafiIO.getChdrnum(),covrIO.getChdrnum())
		|| isNE(covrafiIO.getLife(),covrIO.getLife())
		|| isNE(covrafiIO.getPlanSuffix(),covrIO.getPlanSuffix())
		|| isNE(covrafiIO.getCoverage(),covrIO.getCoverage())
		|| isNE(covrafiIO.getRider(),covrIO.getRider())) {
			covrafiIO.setStatuz(varcom.endp);
		}
	}

protected void formatPrevFields500()
	{
		try {
			sumins510();
			premium515();
			riskCessDate530();
			premCessDate535();
			mortality545();
		}
		catch (GOTOException e){
		}
	}

protected void sumins510()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSumins.set(covrafiIO.getSumins());
		fldlen[offset.toInt()].set(length(wsaaSumins));
		djustfyrec.word.set(wsaaSumins);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(20);
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz,varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}

protected void premium515()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(covrafiIO.getInstprem(),ZERO)) {
			wsaaPremium.set(covrafiIO.getInstprem());
		}
		else {
			wsaaPremium.set(covrafiIO.getSingp());
		}
		fldlen[offset.toInt()].set(length(wsaaPremium));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremium);
	}

protected void riskCessDate530()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrafiIO.getRiskCessDate());
		callDatcon1800();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
	}

protected void premCessDate535()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrafiIO.getPremCessDate());
		callDatcon1800();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
	}

protected void mortality545()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(covrafiIO.getMortcls()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covrafiIO.getMortcls());
	}

protected void readT5687700()
	{
		getT5687Desc710();
	}

protected void getT5687Desc710()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(covrIO.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrIO.getCrtable());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError900();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void callDatcon1800()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void callDatcon6850()
	{
		/*CALL*/
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
