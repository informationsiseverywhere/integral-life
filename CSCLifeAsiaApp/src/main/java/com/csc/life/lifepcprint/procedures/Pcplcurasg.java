/*
 * File: Pcplcurasg.java
 * Date: 30 August 2009 1:00:52
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCURASG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.AsgnmnaTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Last Assignee No.  -> LCASGNNO }
*   02    Last Assignee Name -> LCASGNNM } in T2636
*
*  Overview.
*  ---------
*  This subroutine reads the LETC record and get the last tranno
*  to read AGSNMNA to get the current assginee                   er.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplcurasg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCURASG";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private String wsaaFirstTime = "Y";
	private PackedDecimalData wsaaIx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(2, 0).init(2);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5).isAPartOf(wsaaOtherKeys, 1);
	private ZonedDecimalData wsaaTrannoR = new ZonedDecimalData(5, 0).isAPartOf(wsaaTranno, 0, REDEFINE).setUnsigned();
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String asgnmnarec = "ASGNMNAREC";
	private AsgnmnaTableDAM asgnmnaIO = new AsgnmnaTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplcurasg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main0000()
	{
		/*MAIN*/
		/*    If the IDCODE is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise1000();
			for (wsaaIx.set(1); !(isGT(wsaaIx,pcpdatarec.fldOccurMax)
			|| isEQ(asgnmnaIO.getStatuz(),varcom.endp)); wsaaIx.add(1)){
				formatAsgnmna2000();
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
			para1010();
		}

protected void para1010()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		if (isEQ(pcpdatarec.function,varcom.nextr)) {
			return ;
		}
		//start ILIFE-830
		asgnmnaIO.setParams(SPACES);
		//end ILIFE-830
		asgnmnaIO.setChdrcoy(letcIO.getChdrcoy());
		asgnmnaIO.setChdrnum(letcIO.getChdrnum());
		asgnmnaIO.setSeqno(0);
		
		//start ILIFE-830		
		asgnmnaIO.setFormat(asgnmnarec);
		//end ILIFE-830
		asgnmnaIO.setFunction(varcom.begn);
		
		//performance improvement -- Anjali		
		asgnmnaIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		asgnmnaIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		
	}

protected void formatAsgnmna2000()
	{
		/*START*/
		callAsgnmnaio2100();
		if (isEQ(asgnmnaIO.getStatuz(),varcom.oK)) {
			//start IIFE-830
			getCurrentAssignee2200();
			/*
			if (isEQ(asgnmnaIO.getTranno(),letcIO.getTranno())) {
				getCurrentAssignee2200();
			}*/
			//end IIFE-830
		}
		/*EXIT*/
	}

protected void callAsgnmnaio2100()
	{
		/*START*/
		SmartFileCode.execute(appVars, asgnmnaIO);
		if (isNE(asgnmnaIO.getStatuz(),varcom.oK)
		&& isNE(asgnmnaIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(asgnmnaIO.getStatuz());
			syserrrec.params.set(asgnmnaIO.getParams());
			fatalError9000();
		}
		//ILIFE-830
		if (isEQ(asgnmnaIO.getFunction(),varcom.begn)
		&& (isEQ(asgnmnaIO.getStatuz(),varcom.endp)
		|| isNE(asgnmnaIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(asgnmnaIO.getChdrnum(),letcIO.getChdrnum()))) {
			asgnmnaIO.setStatuz(varcom.endp);
			offset.set(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(asgnmnaIO.getAsgnnum()));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(3);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			wsaaCounter.set(1);
			return ;
		}		
		//ILIFE-830
		if (isEQ(asgnmnaIO.getStatuz(),varcom.endp)		
		|| isNE(asgnmnaIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(asgnmnaIO.getChdrnum(),letcIO.getChdrnum())) {
			asgnmnaIO.setStatuz(varcom.endp);
			return ;
		}
		asgnmnaIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void getCurrentAssignee2200()
	{
		get2210();
		format2220();
	}

protected void get2210()
	{
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of ASGNMNA. It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(asgnmnaIO.getAsgnpfx());
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(asgnmnaIO.getAsgnnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callNamadrs3000();
	}

protected void format2220()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(asgnmnaIO.getAsgnnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], asgnmnaIO.getAsgnnum());	
		/* Current Assignee Name*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void callNamadrs3000()
	{
		/*CALL*/
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
