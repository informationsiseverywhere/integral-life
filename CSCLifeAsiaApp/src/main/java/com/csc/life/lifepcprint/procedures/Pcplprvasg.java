/*
 * File: Pcplprvasg.java
 * Date: 30 August 2009 1:02:50
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLPRVASG.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.TashTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Previous Assignee No.  -> LLASGNNO }
*   02    Previous Assignee Name -> LLASGNNM } in T2636
*
*  Overview.
*  ---------
*  This subroutine reads the LETC record and get the last tranno
*  to read TASH to get the previous assginee.                    er.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplprvasg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPRVASG";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private String wsaaFirstTime = "Y";
	private PackedDecimalData wsaaIx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(2, 0).init(2);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaTranno = new FixedLengthStringData(5).isAPartOf(wsaaOtherKeys, 1);
	private ZonedDecimalData wsaaTrannoR = new ZonedDecimalData(5, 0).isAPartOf(wsaaTranno, 0, REDEFINE).setUnsigned();
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String tashrec = "TASHREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TashTableDAM tashIO = new TashTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplprvasg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main0000()
	{
		/*MAIN*/
		/*    If the IDCODE is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise1000();
			for (wsaaIx.set(1); !(isGT(wsaaIx,pcpdatarec.fldOccurMax)
			|| isEQ(tashIO.getStatuz(),varcom.endp)); wsaaIx.add(1)){
				formatTash2000();
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
			para1010();
		}

protected void para1010()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		if (isEQ(pcpdatarec.function,varcom.nextr)) {
			return ;
		}
		tashIO.setParams(SPACES);
		tashIO.setChdrcoy(letcIO.getChdrcoy());
		tashIO.setChdrnum(letcIO.getChdrnum());
		tashIO.setTranno(wsaaTrannoR);
		tashIO.setFormat(tashrec);
		tashIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tashIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void formatTash2000()
	{
		/*START*/
		callTashio2100();
		if (isEQ(tashIO.getStatuz(),varcom.oK)) {
			getPreviousAssignee2200();
		}
		/*EXIT*/
	}

protected void callTashio2100()
	{
			start2110();
		}

protected void start2110()
	{
		SmartFileCode.execute(appVars, tashIO);
		if (isNE(tashIO.getStatuz(),varcom.oK)
		&& isNE(tashIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(tashIO.getStatuz());
			syserrrec.params.set(tashIO.getParams());
			fatalError9000();
		}
		if (isEQ(tashIO.getFunction(),varcom.begn)
		&& (isEQ(tashIO.getStatuz(),varcom.endp)
		|| isNE(tashIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(tashIO.getChdrnum(),letcIO.getChdrnum())
		|| isNE(tashIO.getTranno(),wsaaTrannoR))) {
			tashIO.setStatuz(varcom.endp);
			offset.set(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(tashIO.getAsgnnum()));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(3);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			wsaaCounter.set(1);
			return ;
		}
		if (isEQ(tashIO.getStatuz(),varcom.endp)
		|| isNE(tashIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(tashIO.getChdrnum(),letcIO.getChdrnum())
		|| isNE(tashIO.getTranno(),wsaaTrannoR)) {
			tashIO.setStatuz(varcom.endp);
			return ;
		}
		tashIO.setFunction(varcom.nextr);
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of TASH.  It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
	}

protected void getPreviousAssignee2200()
	{
		get2210();
		format2220();
	}

protected void get2210()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(tashIO.getAsgnpfx());
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(tashIO.getAsgnnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callNamadrs3000();
	}

protected void format2220()
	{
		fldlen[offset.toInt()].set(length(tashIO.getAsgnnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tashIO.getAsgnnum());
		/*Previous Assignee Name*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
		/*EXIT*/
	}

protected void callNamadrs3000()
	{
		/*CALL*/
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
