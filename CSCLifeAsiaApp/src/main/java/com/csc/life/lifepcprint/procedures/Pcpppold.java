package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import com.quipoz.COBOLFramework.COBOLFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This routine will extract the necessary data for the document merge
*   
*
* Offset. Description............................................. .
*
* Occurance fields
*  001  	  Policy Wording Flag (N) 		
*    		  
*                                                                   *
 * </pre>
 */
public class Pcpppold extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pcpppold.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(COBOLFunctions.ZERO);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(500).init(COBOLFunctions.SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaNumberOfRecs = new PackedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray(50, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcpppold() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		/* MAIN */
		/* If the PCPD-IDCODE-COUNT is different from the one we had */
		/* before (or we are on the first call) we need to set up the */
		/* data buffer by reading data from the database. */
		if (COBOLFunctions.isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			LOGGER.info("==============PCPPPOLD=======================");
			initialise100();
			getDataAndFormat200();
		}

		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(COBOLFunctions.subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(Varcom.oK);
		/* EXIT */
		exitProgram();
	}

	protected void initialise100() {
		para101();
	}

	protected void para101() {
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		offset.set(COBOLFunctions.ZERO);
		wsaaNumberOfRecs.set(0);
	}

	protected void getDataAndFormat200() {
		formatPolDocMerge();
	}

	protected void formatPolDocMerge() {
		offset.add(1);
		if (COBOLFunctions.isEQ(offset, 1)) {
			strpos[offset.toInt()].set(1);
		} else {
			COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
					.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		}
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "N");

	}

	protected void fatalError600() {
		/* FATAL */
		syserrrec.subrname.set("PCPPPOLD");
		pcpdatarec.statuz.set(Varcom.bomb);
		if (COBOLFunctions.isNE(syserrrec.statuz, COBOLFunctions.SPACES)
				|| COBOLFunctions.isNE(syserrrec.syserrStatuz, COBOLFunctions.SPACES)) {
			syserrrec.syserrType.set("1");
		} else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/* EXIT */
		exitProgram();
	}

}
