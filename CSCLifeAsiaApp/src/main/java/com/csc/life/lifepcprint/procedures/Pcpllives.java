/*
 * File: Pcpllives.java
 * Date: 30 August 2009 1:02:09
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLIVES.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
//import com.csc.smart400framework.utility.ValueRange;
import java.util.ArrayList;
import java.util.List; //ILIFE-8709

import com.quipoz.framework.util.log.QPLogger;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ILIFE-8709
import com.csc.life.productdefinition.dataaccess.model.Lifepf; //ILIFE-8709
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;



import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.smart400framework.procedures.Syserr;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Life Client Number
*  002  Life name
*  003  Life Address line 01
*  004  Life Address line 02
*  005  Life Address line 03
*  006  Life Address line 04
*  007  Life Address line 05
*  008  Life Postcode.
*  009  Life DOB
*  010  Life ANB at CCD
*  011  Life Assured First Name
*  012  Life Assured Last Name
*  013  Life Gender			//TMLII-670 LT-01-009 Letter 5
*  014  Life Salutation
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Recurring Fields Retrieved.
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcpllives extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLLIVES ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	//TMLII-664 LT-01-002
	//private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	
	//Added by Manuj/Shubham : For TMLII-670 LT-01-009 Letter 8
	//private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(13).setUnsigned();
	//changes ended
	
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	//TMLII-664 LT-01-002
	//private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(2048).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	//TMLII-664 LT-01-002 START
	//private FixedLengthStringData filler = new FixedLengthStringData(240);
	//private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(40, 6, filler, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(360);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(60, 6, filler, 0);
	//TMLII-664 LT-01-002 END
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
		/*Life record*/
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	
	//TMLII-1901 : Gender description in the Letter
	private String t5670 = "T5670";
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30).init(SPACES);
	private String descrec = "DESCREC";
	//End
	 //ILIFE-8709 start
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> lifepfList = new ArrayList<Lifepf>();
	 //ILIFE-8709 end

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit299
	}

	public Pcpllives() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
 //ILIFE-8709 start			
			for (Lifepf lifepf : lifepfList) {
				if(isEQ(lifepf.getValidflag(),"2")) {
					continue;
				}
				getLifeAndFormat200(lifepf);
			}
 //ILIFE-8709 end			
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
 //ILIFE-8709 start
		lifepfList = lifepfDAO.getLifeData(letcIO.getRdoccoy().toString(), letcIO.getRdocnum().toString(), "01", "00");
 //ILIFE-8709 end
		wsaaAnbAtCcd.set(ZERO);
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
	}

protected void getLifeAndFormat200(Lifepf lifepf)
	{
		try {
			getData200(lifepf); //ILIFE-8709
			format201(lifepf); //ILIFE-8709
			format202(lifepf); //ILIFE-8709
			format203();
			format204();
			format205();
			format206();
			format207();
			format208();
			format209();
			format210(lifepf); //ILIFE-8709
			//TMLII-664 LT-01-002 START
			format211();
			format212();
			//TMLII-664 LT-01-002 END
			
			//TMLII-670 LT-01-009 Letter 8 START
			format213();
			//TMLII-670 LT-01-009 Letter 8 END
			format214();
		}
		catch (GOTOException e){
		}
	}

protected void getData200(Lifepf lifepf)
	{
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(letcIO.getClntcoy());
		clntIO.setClntnum(lifepf.getLifcnum()); //ILIFE-8709
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		wsaaCounter.add(1);
	}

protected void format201(Lifepf lifepf)
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(lifepf.getLifcnum())); //ILIFE-8709
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], lifepf.getLifcnum()); //ILIFE-8709
	}

protected void format202(Lifepf lifepf)
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(lifepf.getLifcnum()); //ILIFE-8709
		namadrsrec.function.set("PYNMN");
		namadrsrec.language.set(pcpdatarec.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void format203()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr1));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr1);
		
	}

protected void format204()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(namadrsrec.addr2));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr2);
}

protected void format205()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr3));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr3);
	
	}


protected void format206()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(namadrsrec.addr4));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr4);
}

protected void format207()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr5));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr5);
		
	}

protected void format208()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(namadrsrec.pcode));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.pcode);	
}

protected void format209()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(clntIO.getCltdob());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format210(Lifepf lifepf)
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAnbAtCcd.set(lifepf.getAnbAtCcd()); //ILIFE-8709
		fldlen[offset.toInt()].set(length(wsaaAnbAtCcd));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAnbAtCcd);
	}

	//TMLII-664 LT-01-002 START
	protected void format211(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getLgivname(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getLgivname(), 1, fldlen[offset.toInt()]));
	}
	
	protected void format212(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getLsurname(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getLsurname(), 1, fldlen[offset.toInt()]));
	
		
	}
	//TMLII-664 LT-01-002 END
	
	//TMLII-670 LT-01-009 Letter 8 START
	protected void format213(){
		offset.add(1);
		getClntGenderDesc();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaLongdesc, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaLongdesc, 1, fldlen[offset.toInt()]));
	}
	//TMLII-670 LT-01-009 Letter 8 END
	
	protected void format214(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getSalutl(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getSalutl(), 1, fldlen[offset.toInt()]));
	}
	
	//TMLII-1901 : Gender description in the Letter
	protected void getClntGenderDesc()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(letcIO.getChdrcoy());
		descIO.setDesctabl(t5670);
		descIO.setDescitem(clntIO.getCltsex());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			wsaaLongdesc.fill("?");
		}
		wsaaLongdesc.set(descIO.getLongdesc());
	}
	//End
	
protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
