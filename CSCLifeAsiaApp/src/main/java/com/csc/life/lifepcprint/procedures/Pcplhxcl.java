/*
 * File: Pcplhxcl.java
 * Date: 30 August 2009 1:01:48
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLHXCL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Clause Note (Occurance Field)
*
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
*
*
***********************************************************************
* </pre>
*/
public class Pcplhxcl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLHXCL ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaIx = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaHxclnote = new FixedLengthStringData(75);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (60, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String hxclrec = "HXCLREC";
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplhxcl() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isNE(hxclIO.getStatuz(),varcom.oK))) {
				getHxclAndFormat200();
			}
			
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		if (isEQ(wsaaDataBuffer,SPACES)) {
			pcpdatarec.data.set(SPACES);
			return ;
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
		hxclIO.setParams(SPACES);
		hxclIO.setStatuz(varcom.oK);
		if (letcokcpy.lifeFollowup.isTrue()) {
			hxclIO.setChdrcoy(letcokcpy.lfChdrcoy);
			hxclIO.setChdrnum(letcokcpy.lfChdrnum);
			hxclIO.setFupno(letcokcpy.lfFupno);
		}
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFunction(varcom.begn);
		hxclIO.setStatuz(varcom.oK);
		hxclIO.setFormat(hxclrec);
	}

protected void getHxclAndFormat200()
	{
		try {
			getData200();
			format200();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)
		&& isNE(hxclIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hxclIO.getStatuz());
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),letcokcpy.lfChdrcoy)
		|| isNE(hxclIO.getChdrnum(),letcokcpy.lfChdrnum)
		|| isNE(hxclIO.getFupno(),letcokcpy.lfFupno)
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			hxclIO.setStatuz(varcom.endp);
		}
		if (isEQ(hxclIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit299);
		}
		hxclIO.setFunction(varcom.nextr);
	}

protected void format200()
	{
		for (wsaaIx.set(1); !(isGT(wsaaIx,6)); wsaaIx.add(1)){
			formatClnote300();
		}
	}

protected void formatClnote300()
	{
		clnote300();
	}

protected void clnote300()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(1);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		wsaaHxclnote.set(hxclIO.getHxclnote(wsaaIx));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(wsaaHxclnote, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaHxclnote);
		wsaaCounter.add(1);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
