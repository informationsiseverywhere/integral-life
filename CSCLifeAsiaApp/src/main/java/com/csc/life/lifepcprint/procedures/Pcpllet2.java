/*
 * File: Pcpllet2.java
 * Date: 30 August 2009 1:02:02
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLET2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.sql.SQLException;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Print Date
*   02    Run Number
*   03    Agent Name
*   04    Tax Id
*   05    Agent Number
*   06    Sales Unit
*   07    Agent Bank Account No.
*   08    Address Line 1
*   09    Address Line 2
*   10    Address Line 3
*   11    Address Line 4
*   12    Address Line 5
*   13    Postal Code
*   14    Registerd Licence no
*   15    Branch
*   16    Run Period
*   17    Gross Commission
*   18    Tax amount
*   19    Deduction
*   20    Collateral Amount
*   21    Bank Charges
*   22    Net Commission
*   23    *** Not used ***
*   24    *** Not used ***
*   25    *** Not used ***
*   26    Policy Number
*   27    Total Premium
*   28    OPD Premium
*   29    Extra Premium
*   30    Commission
*   31    OPD Commission
*   32    Total Commission
*
*  Overview.
*  ---------
*  This will get the required information from LETT for printing
*  of Agent Statement. LETT records are created during T2AGENTSMT
*  or L4AGENTSMT. These two schedules calls BT516.
*
*  Processing.
*  -----------
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcpllet2 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqllettpf1rs = null;
	private java.sql.PreparedStatement sqllettpf1ps = null;
	private java.sql.Connection sqllettpf1conn = null;
	private String sqllettpf1 = "";
	private final String wsaaSubrname = "PCPLLET2  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private PackedDecimalData wsaaNumberOfRecs = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(5, 0).init(7);
	private String wsaaEof = "";
	private FixedLengthStringData wsaaAgentno = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/* ERRORS */
	private static final String esql = "ESQL";
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(8000).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(10, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (500, 12);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(10, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(10, 0, wsaaStartAndLength, 6);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private BabrTableDAM babrIO = new BabrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private SqlLettpfInner sqlLettpfInner = new SqlLettpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof290, 
		exit299, 
		eof390, 
		exit399
	}

	public Pcpllet2() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		/*    If the PCPD-IDCODE-COUNT is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getLettAndFormat200();
			if (isNE(wsaaEof,"Y")) {
				while ( !(isEQ(sqlLettpfInner.sqlRecordType, "T")
				|| isEQ(wsaaEof,"Y"))) {
					getLettAndFormat300();
				}
				
				formatTotalAmt400();
			}
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isGT(pcpdatarec.fldOccur,wsaaNumberOfRecs)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		if (isGT(pcpdatarec.fldOffset,0)
		&& isLT(pcpdatarec.fldOffset,26)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		wsaaAgentno.set(letcokcpy.asAgentno);
		wsaaNumberOfRecs.set(ZERO);
		offset.set(1);
		wsaaEof = "N";
		wsaaThreadNumber.set(1);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(LETTPF) TOFILE(");
		stringVariable1.addExpression(letcokcpy.asRunlib, SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(letcokcpy.asFilenm);
		stringVariable1.addExpression(letcokcpy.asRunid);
		stringVariable1.addExpression(letcokcpy.asJobno);
		stringVariable1.addExpression(") MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		sqllettpf1 = " SELECT  *" +
" FROM   " + getAppVars().getTableNameOverriden("LETTPF") + " " +
" WHERE SUBSTR(LETTREC, 26, 8) = ?";
		sqlerrorflag = false;
		try {
			sqllettpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.printing.dataaccess.LettpfTableDAM());
			sqllettpf1ps = getAppVars().prepareStatementEmbeded(sqllettpf1conn, sqllettpf1, "LETTPF");
			getAppVars().setDBString(sqllettpf1ps, 1, wsaaAgentno);
			sqllettpf1rs = getAppVars().executeQuery(sqllettpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void getLettAndFormat200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					getData200();
					formatRunNo202();
					formatTaxid204();
					formatSaleUnit206();
					formatAddress1208();
					formatAddress3210();
					formatAddress5212();
					formatLicenceNo214();
					formatRunPeriod216();
					formatIncomeTax218();
					formatCollateralAmt220();
					formatNetCommission222();
					reserveForFutureB224();
				case eof290: 
					eof290();
				case exit299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getData200()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqllettpf1rs)) {
				getAppVars().getDBObject(sqllettpf1rs, 1, sqlLettpfInner.sqlLettrec);
			}
			else {
				goTo(GotoLabel.eof290);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		/*FORMAT-PRINT-DATE*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlPrnDte));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlPrnDte);
	}

protected void formatRunNo202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlRunNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlRunNo);
		/*FORMAT-AGENT-NAME*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlAgntName));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlAgntName);
	}

protected void formatTaxid204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlTaxId));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlTaxId);
		/*FORMAT-AGNTNUM*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlAgntnum));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlAgntnum);
	}

protected void formatSaleUnit206()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlSalesUnt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlSalesUnt);
		/*FORMAT-AGENT-BANK-ACCT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlAgntBankacc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlAgntBankacc);
	}

protected void formatAddress1208()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		fldlen[offset.toInt()].add(inspectTally(sqlLettpfInner.sqlAgntAddr01, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(sqlLettpfInner.sqlAgntAddr01, 1, fldlen[offset.toInt()]));
		/*FORMAT-ADDRESS-2*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		fldlen[offset.toInt()].add(inspectTally(sqlLettpfInner.sqlAgntAddr02, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(sqlLettpfInner.sqlAgntAddr02, 1, fldlen[offset.toInt()]));
	}

protected void formatAddress3210()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		fldlen[offset.toInt()].add(inspectTally(sqlLettpfInner.sqlAgntAddr03, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(sqlLettpfInner.sqlAgntAddr03, 1, fldlen[offset.toInt()]));
		/*FORMAT-ADDRESS-4*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		fldlen[offset.toInt()].add(inspectTally(sqlLettpfInner.sqlAgntAddr04, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(sqlLettpfInner.sqlAgntAddr04, 1, fldlen[offset.toInt()]));
	}

protected void formatAddress5212()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		fldlen[offset.toInt()].add(inspectTally(sqlLettpfInner.sqlAgntAddr05, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(sqlLettpfInner.sqlAgntAddr05, 1, fldlen[offset.toInt()]));
		/*FORMAT-POSTAL-CODE*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlAgntPcode));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlAgntPcode);
	}

protected void formatLicenceNo214()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlLicenceNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlLicenceNo);
		/*FORMAT-BRANCH*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlBranch));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlBranch);
	}

protected void formatRunPeriod216()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlRunPeriod));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlRunPeriod);
		/*FORMAT-GROSS-COMMISSION*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlGrossComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatIncomeTax218()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlTaxAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-DEDUCTION*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlDeductnAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatCollateralAmt220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlCollateralAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-BANK-CHARGE*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlTdcchrg);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatNetCommission222()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlNetComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*RESERVE-FOR-FUTURE-A*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
	}

protected void reserveForFutureB224()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		/*RESERVE-FOR-FUTURE-C*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		goTo(GotoLabel.exit299);
	}

protected void eof290()
	{
		wsaaEof = "Y";
	}

protected void getLettAndFormat300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					formatPolicyNo301();
					formatOpdPremium303();
					formatCommission305();
					formatTotalCommission307();
				case eof390: 
					eof390();
				case exit399: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void formatPolicyNo301()
	{
		wsaaNumberOfRecs.add(1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(sqlLettpfInner.sqlChdrnum));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sqlLettpfInner.sqlChdrnum);
		/*FORMAT-TOTAL-PREMIUM*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlPrem);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatOpdPremium303()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlOpdPrem);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-LOAD-PREMIUM*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlLoadPrem);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatCommission305()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-OPD-COMMISSION*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlOpdComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatTotalCommission307()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlTotComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*NEXTR*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqllettpf1rs)) {
				getAppVars().getDBObject(sqllettpf1rs, 1, sqlLettpfInner.sqlLettrec);
			}
			else {
				goTo(GotoLabel.eof390);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		goTo(GotoLabel.exit399);
	}

protected void eof390()
	{
		wsaaEof = "Y";
	}

protected void formatTotalAmt400()
	{
		formatGrossCommission401();
		formatDeduction403();
		formatBankCharge405();
		formatNetCommission406();
	}

protected void formatGrossCommission401()
	{
		offset.set(17);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlGrossComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-INCOME-TAX*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlTaxAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatDeduction403()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlDeductnAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT-COLLATERAL-AMT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlCollateralAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatBankCharge405()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlTdcchrg);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatNetCommission406()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(sqlLettpfInner.sqlNetComm);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		getAppVars().freeDBConnectionIgnoreErr(sqllettpf1conn, sqllettpf1ps, sqllettpf1rs);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure SQL-LETTPF--INNER
 */
private static final class SqlLettpfInner { 

	private FixedLengthStringData sqlLettpf = new FixedLengthStringData(700);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData sqlLettrec = new FixedLengthStringData(700).isAPartOf(sqlLettpf, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(407+DD.cltaddr.length*5).isAPartOf(sqlLettpf, 0, FILLER_REDEFINE);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData sqlPrnDte = new FixedLengthStringData(10).isAPartOf(filler, 0);
	private FixedLengthStringData sqlRunNo = new FixedLengthStringData(15).isAPartOf(filler, 10);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(filler, 25);
	private FixedLengthStringData sqlAgntName = new FixedLengthStringData(60).isAPartOf(filler, 33);
	private FixedLengthStringData sqlTaxId = new FixedLengthStringData(10).isAPartOf(filler, 93);
	private FixedLengthStringData sqlSalesUnt = new FixedLengthStringData(30).isAPartOf(filler, 103);
	private FixedLengthStringData sqlAgntBankacc = new FixedLengthStringData(20).isAPartOf(filler, 133);
	private FixedLengthStringData sqlAgntAddr01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 153);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData sqlAgntAddr02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 153+DD.cltaddr.length);
	private FixedLengthStringData sqlAgntAddr03 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 153+DD.cltaddr.length*2);
	private FixedLengthStringData sqlAgntAddr04 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 153+DD.cltaddr.length*3);
	private FixedLengthStringData sqlAgntAddr05 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(filler, 153+DD.cltaddr.length*4);
	private FixedLengthStringData sqlAgntPcode = new FixedLengthStringData(10).isAPartOf(filler, 153+DD.cltaddr.length*5);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData sqlLicenceNo = new FixedLengthStringData(15).isAPartOf(filler, 163+DD.cltaddr.length*5);
	private FixedLengthStringData sqlBranch = new FixedLengthStringData(30).isAPartOf(filler, 178+DD.cltaddr.length*5);
	private FixedLengthStringData sqlRunPeriod = new FixedLengthStringData(10).isAPartOf(filler, 208+DD.cltaddr.length*5);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(filler, 218+DD.cltaddr.length*5);
	private ZonedDecimalData sqlPrem = new ZonedDecimalData(15, 2).isAPartOf(filler, 226+DD.cltaddr.length*5);
	private ZonedDecimalData sqlOpdPrem = new ZonedDecimalData(15, 2).isAPartOf(filler, 241+DD.cltaddr.length*5);
	private ZonedDecimalData sqlLoadPrem = new ZonedDecimalData(15, 2).isAPartOf(filler, 256+DD.cltaddr.length*5);
	private ZonedDecimalData sqlComm = new ZonedDecimalData(15, 2).isAPartOf(filler, 271+DD.cltaddr.length*5);
	private ZonedDecimalData sqlOpdComm = new ZonedDecimalData(15, 2).isAPartOf(filler, 286+DD.cltaddr.length*5);
	private ZonedDecimalData sqlTotComm = new ZonedDecimalData(15, 2).isAPartOf(filler, 301+DD.cltaddr.length*5);
	private FixedLengthStringData sqlRecordType = new FixedLengthStringData(1).isAPartOf(filler, 316+DD.cltaddr.length*5);
	private ZonedDecimalData sqlGrossComm = new ZonedDecimalData(15, 2).isAPartOf(filler, 317+DD.cltaddr.length*5);
	private ZonedDecimalData sqlTaxAmt = new ZonedDecimalData(15, 2).isAPartOf(filler, 332+DD.cltaddr.length*5);
	private ZonedDecimalData sqlDeductnAmt = new ZonedDecimalData(15, 2).isAPartOf(filler, 347+DD.cltaddr.length*5);
	private ZonedDecimalData sqlCollateralAmt = new ZonedDecimalData(15, 2).isAPartOf(filler, 362+DD.cltaddr.length*5);
	private ZonedDecimalData sqlTdcchrg = new ZonedDecimalData(15, 2).isAPartOf(filler, 377+DD.cltaddr.length*5);
	private ZonedDecimalData sqlNetComm = new ZonedDecimalData(15, 2).isAPartOf(filler, 392+DD.cltaddr.length*5);
}
}
