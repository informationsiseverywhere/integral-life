/*
 * File: Pcplprmrcp.java
 * Date: 30 August 2009 1:02:45
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLPRMRCP.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Risk Commencement Date
*  002  Bill To Date
*  003  Total Premium
*  004  Bill Currency
*  005  Pay To Date
*  006  Renewal Year
*  007  No. Of Instalment
*  008  Total Premium With Tax
*
*  Overview.
*  ---------
*  OFFSET 1,2,3,5,6&7 are used by letter type HPRMRCP1
*  OFFSET 1,2,3,4     are used by letter type HBILNOT1
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplprmrcp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPRMRCP";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaPremium = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaInstfrom = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaInstfrom, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaInstYr = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaNofInstlm = new FixedLengthStringData(2);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(200).init(SPACES);
		/* The following array is configured to store up to 7 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	//ILIFE-1971-Starts
	private PackedDecimalData wsaaTotTax = new PackedDecimalData(13, 2);
	//ILIFE-1971-ENDS
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (8, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String chdrlnbrec = "CHDRLNBREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplprmrcp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getChdrlnbAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setFunction(varcom.readr);
		/*EXIT*/
	}

protected void getChdrlnbAndFormat200()
	{
		getData200();
		format210();
		format220();
		format230();
		format235();
		format238();	//IBPLIFE-13475
	}

protected void getData200()
	{
		chdrlnbIO.setStatuz(varcom.oK);
		chdrlnbIO.setChdrcoy(letcIO.getChdrcoy());
		chdrlnbIO.setChdrnum(letcIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		callChdrlnbio300();
		offset.set(1);
		strpos[offset.toInt()].set(1);
		/*FORMAT*/
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(chdrlnbIO.getOccdate());
		processDate400();
	}

protected void format210()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(chdrlnbIO.getBtdate());
		processDate400();
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaPremium.set(chdrlnbIO.getSinstamt06());
		//ILIFE-1971 Starts
		wsaaTotTax.set(letcIO.getOtherKeys().substring(6));
		wsaaPremium.add(wsaaTotTax);
		//ILIFE-1971
		fldlen[offset.toInt()].set(length(wsaaPremium));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremium);
	}

protected void format220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrlnbIO.getCntcurr()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrlnbIO.getCntcurr());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(chdrlnbIO.getPtdate());
		processDate400();
	}

protected void format230()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		//wsaaInstfrom.set(chdrlnbIO.getInstfrom());
		wsaaInstfrom.set(chdrlnbIO.getBtdate());
		fldlen[offset.toInt()].set(length(wsaaInstYr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaInstYr, 1, fldlen[offset.toInt()]));
	}

protected void format235()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon3rec.intDate2.set(chdrlnbIO.getBtdate());
		datcon3rec.frequency.set(chdrlnbIO.getBillfreq());
		callDatcon3600();
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			datcon3rec.freqFactor.set(ZERO);
		}
		wsaaBillfreq.set(chdrlnbIO.getBillfreq());
		wsaaY.set(ZERO);
		wsaaX.set(ZERO);
		if (isEQ(chdrlnbIO.getBillfreq(),NUMERIC)
		&& isNE(wsaaBillfreq,ZERO)) {
			compute(wsaaY, 5).setDivide(datcon3rec.freqFactor, (wsaaBillfreq));
			wsaaX.setRemainder(wsaaY);
			if (isEQ(wsaaX,ZERO)) {
				wsaaNofInstlm.set(wsaaBillfreq);
			}
			else {
				wsaaNofInstlm.set(wsaaX);
			}
		}
		else {
			wsaaNofInstlm.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(wsaaNofInstlm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaNofInstlm, 1, fldlen[offset.toInt()]));
	}

protected void format238() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	linsrnlIO.setParams(SPACES);
	linsrnlIO.setChdrcoy(letcIO.getChdrcoy());
	linsrnlIO.setChdrnum(letcIO.getChdrnum());
	linsrnlIO.setInstfrom(chdrlnbIO.getInstfrom());
	linsrnlIO.setFormat("LINSRNLREC");
	linsrnlIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, linsrnlIO);
	if (isNE(linsrnlIO.getStatuz(),Varcom.oK)) {
		if (isEQ(linsrnlIO.getStatuz(),Varcom.mrnf)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
			return;
		}
		syserrrec.params.set(linsrnlIO.getParams());
		syserrrec.statuz.set(linsrnlIO.getStatuz());
		fatalError900();
	}
	ZonedDecimalData wsaaRnwlPrm = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	wsaaRnwlPrm.set(linsrnlIO.getInstamt06());
	fldlen[offset.toInt()].set(length(wsaaRnwlPrm));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRnwlPrm);
}

protected void callChdrlnbio300()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError900();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void processDate400()
	{
			call410();
		}

protected void call410()
	{
		datcon1rec.function.set(varcom.conv);
		callDatcon1500();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			return ;
		}
		else {
			datcon1rec.function.set(varcom.edit);
			callDatcon1500();
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				pcpdatarec.statuz.set(varcom.bomb);
				return ;
			}
		}
		datcon6rec.function.set(SPACES);
		datcon6rec.intDate1.set(datcon1rec.intDate);
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.function.set(SPACES);
		callDatcon6700();
		if (isEQ(datcon6rec.statuz,varcom.oK)) {
			fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
		}
		else {
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon6rec.intDate1);
			pcpdatarec.statuz.set(varcom.bomb);
		}
	}

protected void callDatcon1500()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void callDatcon3600()
	{
		/*CALL*/
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		/*EXIT*/
	}

protected void callDatcon6700()
	{
		/*CALL*/
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void getdesc800()
	{
		/*GETDESC*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(pcpdatarec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.fill("?");
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
