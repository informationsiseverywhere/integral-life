/*
 * File: Pcplcvrall.java
 * Date: 30 August 2009 1:00:54
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCVRALL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
//ILAE-63 START by dpuhawan
import com.csc.fsu.general.recordstructures.Hfmtdterec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
//IALE-63 END
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.ZrstpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Zrstpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Total Premium
*  002  Cover/Rider Table
*  003  Cover Description
*  004  Premium
*  005  Sum Insured
*  006  Prem Cessation Date
*  007  Prem Cessation Term
*  008  Risk Cessation Date
*  009  Risk Cessation Term
*  010  Premium Status
*  011  Premium Status Description
*  012  Risk Status
*  013  Risk Status Description
*  014  Cost of Insurance
*  015  Life Coverage Insured
*  302  Main Premium Cess Term
*  303  Riskcommdte 
*  304  Total Cost of Insurance
*  016  extra premium
*  017  Loading Duration
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include Total Premium which is not an occurance field.
*
*  Processing.
*  -----------
*  This routine extracts all the coverages for the contract.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Pcpllcvral extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCVRALL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(16).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(2, 0).setUnsigned();
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaTotPrem = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaPremAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(11, 2).setPattern("ZZZ,ZZZ,ZZ9.99");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

		/*    03  WSAA-START-AND-LENGTH   OCCURS 30.                       */
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (340, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String covrrec = "COVRREC";
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t5687 = "T5687";
	private static final String t5681 = "T5681";
	private static final String t5682 = "T5682";
	//private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	
	//ILAE-63 START by dpuhawan
	//Ticket #TMLII-1398 [Incorrect Values in XML for Insurance Change, Paid Up Letter ]
	private PackedDecimalData oldoffset = new PackedDecimalData(3, 0);
	/*LT-01-0005*/
	private ZonedDecimalData wsaaMainPremCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private FixedLengthStringData wsaaRiskcommdte = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaTotalCoi = new ZonedDecimalData(15, 2);
	private Hfmtdterec hfmtdterec = new Hfmtdterec();
	private ZonedDecimalData wsaaComponentCoi = new ZonedDecimalData(15, 2);
	//private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private String liferec = "LIFEREC";
	private String zrstrec = "ZRSTREC";	
	//ILAE-63 END
	
	private Zrstpf zrstpf = null;
	private Covrpf covrpf = null;
	
	private ZrstpfDAO zrstpfDAO = getApplicationContext().getBean("zrstpfDAO", ZrstpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(15, 2);
	private FixedLengthStringData wsaaTerm = new FixedLengthStringData(3);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcpllcvral() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			List<String> data = new ArrayList<>();
			data.add(letcIO.getRdocnum().toString());
			Map<String,List<Covrpf>> val = covrpfDAO.searchCovrMap(letcIO.getRdoccoy().toString(),data);
			Iterator<Entry<String, List<Covrpf>>> keyit = val.entrySet().iterator();
					while(keyit.hasNext())
					{
						List<Covrpf> covr = keyit.next().getValue();
						Iterator<Covrpf> covrit = covr.iterator();
					    while(covrit.hasNext())
					    {
					    	covrpf = covrit.next();
					    	getCovrAndFormat200();
					    }
				
				
			}
//			while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
//				getCovrAndFormat200();
//			}
//			
			formatTotprem600();
			//ILAE-63 START by dpuhawan
			format250();
			format255(); 
			format260();
			//ILAE-63 END
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		/* IF PCPD-FLD-OCCUR > WSAA-COUNTER                             */
		/*    MOVE ENDP               TO PCPD-STATUZ                    */
		/*    GO TO 090-EXIT                                            */
		/* END-IF.                                                      */
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter)
			|| isGT(pcpdatarec.groupOccurance, wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC)
		|| isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		if (isEQ(strpos[offset.toInt()], 0)
		|| isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		/*  INITIALIZE                     WSAA-START-AND-LENGTH.<S19FIX>*/
		for (offset.set(1); !(isGT(offset, 100)); offset.add(1)){
			wsaaStartAndLength[offset.toInt()].set(ZERO);
		}
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/* MOVE LETC-OTHER-KEYS        TO LSAV-SAVE-OTHER-KEYS.         */
		if (isNE(pcpdatarec.groupOccurance, NUMERIC)) {
			pcpdatarec.groupOccurance.set(1);
		}
		wsaaCounter.set(ZERO);
		wsaaTotPrem.set(ZERO);
		wsaaPremCessTerm.set(ZERO);
		//ILAE-63 START by dpuhawan
		wsaaMainPremCessTerm.set(ZERO);
		wsaaRiskcommdte.set(ZERO);
		wsaaComponentCoi.set(ZERO);
		//ILAE-63 END
		/* Reserve the first offset for total premium.*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaPremAmt.set(wsaaTotPrem);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
		//covrIO.setParams(SPACES);
		//covrIO.setFunction(varcom.begn);
	}

protected void getCovrAndFormat200()
	{
		try {
			getData200();
			format201();
			format210();
			format215();
			format220();
			format225();
			format230();
			format235();
			format245();
			//ILAE-63 START by dpuhawan			
			getData300();
			format246();
			format247();
			//ILAE-63 END
			format248();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
//		covrIO.setStatuz(varcom.oK);
//		covrIO.setChdrcoy(letcIO.getRdoccoy());
//		covrIO.setChdrnum(letcIO.getRdocnum());
//		covrIO.setLife("01");
//		covrIO.setCoverage(SPACES);
//		covrIO.setRider(SPACES);
//		covrIO.setPlanSuffix(ZERO);
//		covrIO.setFormat(covrrec);
//		SmartFileCode.execute(appVars, covrIO);
//		if (isNE(covrIO.getStatuz(), varcom.oK)
//		&& isNE(covrIO.getStatuz(), varcom.endp)) {
//			syserrrec.params.set(covrIO.getParams());
//			fatalError900();
//		}
//		covrIO.setFunction(varcom.nextr);
//		if (isEQ(covrIO.getStatuz(), varcom.endp)
//		|| isNE(covrIO.getChdrcoy(), letcIO.getRdoccoy())
//		|| isNE(covrIO.getChdrnum(), letcIO.getRdocnum())) {
//			covrIO.setStatuz(varcom.endp);
//			goTo(GotoLabel.exit299);
//		}
//		if (isNE(covrIO.getValidflag(), "1")) {
//			goTo(GotoLabel.exit299);
//		}
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of COVR.  It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
	}

protected void format201()
	{
		/*  Coverage / Rider Table*/
		offset.add(1); //offset = 2
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(covrpf.getCrtable()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covrpf.getCrtable());
		/*FORMAT*/
		/*  Coverage Description.*/
		offset.add(1); //offset = 3
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		readT5687500();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void format210()
	{
		/*   Premium*/
		offset.add(1); //offset = 4
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isNE(covrpf.getInstprem(), ZERO)) {
			wsaaPremAmt.set(covrpf.getInstprem());
			wsaaTotPrem.add(covrpf.getInstprem().doubleValue());
		}
		else {
			wsaaPremAmt.set(covrpf.getSingp());
			wsaaTotPrem.add(covrpf.getInstprem().doubleValue());
		}
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
	}

protected void format215()
	{
		/*   Sum Insured*/
		offset.add(1); //offset = 5
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaSumins.set(covrpf.getSumins());
		fldlen[offset.toInt()].set(length(wsaaSumins));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSumins);
	}

protected void format220()
	{
		/*   Premium Cessation Date*/
		offset.add(1); //offset = 6
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrpf.getPremCessDate());
		callDatcon1300();
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		/*220-FORMAT.                                                      */
		/*     Premium Cess Term*/
		if (isNE(covrpf.getPremCessTerm(), 0)) {
			wsaaPremCessTerm.set(covrpf.getPremCessTerm());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrpf.getCrrcd());
			datcon3rec.intDate2.set(covrpf.getPremCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				/*        MOVE SPACES           TO WSAA-PREM-CESS-TERM           */
				wsaaPremCessTerm.set(ZERO);
			}
			else {
				wsaaPremCessTerm.set(datcon3rec.freqFactor);
			}
		}
		offset.add(1); //offset = 7
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPremCessTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremCessTerm);
	}

protected void format225()
	{
		/*   Risk Cessation Date                                           */
		offset.add(1); //offset = 8
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covrpf.getRiskCessDate());
		callDatcon1300();
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		
		//ILAE-63 START by dpuhawan
		//TMLII-2160 CLONE - Policy Summary - payment time (TMLII-2158)
		if(isEQ(covrpf.getPstatcode(),"SP")) {
			wsaaMainPremCessTerm.set("1");
		}//LT-01-0005//
		else if(isEQ(covrpf.getRider(),"00") && isEQ(covrpf.getCoverage(),"01") && isEQ(covrpf.getLife(),"01")) 
		{
			wsaaMainPremCessTerm.set(wsaaPremCessTerm);
		}	
		//ILAE-63 END
	}

protected void format230()
	{
		/*     Risk Cess Term                                              */
		if (isNE(covrpf.getRiskCessTerm(), 0)) {
			wsaaRiskCessTerm.set(covrpf.getRiskCessTerm());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrpf.getCrrcd());
			datcon3rec.intDate2.set(covrpf.getRiskCessTerm());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				/*        MOVE SPACES           TO WSAA-RISK-CESS-TERM   <S19FIX>*/
				wsaaRiskCessTerm.set(ZERO);
			}
			else {
				wsaaRiskCessTerm.set(datcon3rec.freqFactor);
			}
		}
		offset.add(1); //offset = 9
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaRiskCessTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskCessTerm);
		
		//ILAE-63 START by dpuhawan
		//lt-01-0005//
		if(isEQ(covrpf.getRider(),"00") && isEQ(covrpf.getCoverage(),"01")) 
		{
			wsaaRiskcommdte.set(hfmtdterec.hfmtOutputDate);
		}	
		//ILAE-63 END
	}

protected void format235()
	{
		/*  Premium Status                                                 */
		offset.add(1); //offset = 10
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(covrpf.getPstatcode()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covrpf.getPstatcode());
		/*FORMAT*/
		/*  Premium Status Description.                                    */
		offset.add(1); //offset = 11
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		readT5681700();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void format245()
	{
		/*  Risk Status                                                    */
		offset.add(1); //offset = 12
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(covrpf.getStatcode()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covrpf.getStatcode());
		/*FORMAT*/
		/*  Risk Status Description.                                       */
		offset.add(1); //offset = 13
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		readT5682800();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void callDatcon1300()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void readT5687500()
	{
		getT5687Desc510();
	}

protected void getT5687Desc510()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(covrpf.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covrpf.getCrtable());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError900();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void formatTotprem600()
	{
		/*TOT-PREM*/
		/*   Total Premium*/
		oldoffset.set(offset);
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaPremAmt.set(wsaaTotPrem);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
		/*EXIT*/
	}

protected void readT5681700()
	{
		getT5681Desc710();
	}

protected void getT5681Desc710()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(covrpf.getChdrcoy());
		descIO.setDesctabl(t5681);
		descIO.setDescitem(covrpf.getPstatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError900();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void readT5682800()
	{
		getT5682Desc810();
	}

protected void getT5682Desc810()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(covrpf.getChdrcoy());
		descIO.setDesctabl(t5682);
		descIO.setDescitem(covrpf.getStatcode());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError900();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
//ILAE-63 START by dpuhawan

protected void format246()
{
	/*  Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-sep-2013
	 */
		offset.add(1); //offset = 14 Cost of Insurance
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaPremAmt.set(wsaaComponentCoi);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
}

protected void format247()
{
	/*  Comment : Added for TMLII 574
	 *  Author : Sankaran J
	 *  Created: 12-Aug-2013
	 */
	 
	offset.add(1); //offset = 15 Life Coverage Insured
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));		
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set("CN");
	namadrsrec.clntCompany.set(letcIO.getClntcoy());
	//TMLII-1725 - Setting the Client number of LifeInsured
	namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
	//TMLII-1724 - insured name should be in format first name then last name, hence correcting input fucntion for Namadrs subroutine.
//	namadrsrec.function.set("PYNMN");
	namadrsrec.function.set("LGNMF");
	namadrsrec.language.set(pcpdatarec.language);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) 
	{
		fatalError900();
		syserrrec.statuz.set(namadrsrec.statuz);
	}
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
	
	if (isEQ(fldlen[offset.toInt()],ZERO)) 
	{
		fldlen[offset.toInt()].set(1);
	}
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
}

protected void getData300()  
{
	/*  Comment : Added for TMLII 574
	 *  Author : Sankaran J
	 *  Created: 12-Aug-2013
	 */
	   
	lifelnbIO.setDataKey(SPACES);		 
	lifelnbIO.setChdrcoy(covrpf.getChdrcoy());
	lifelnbIO.setChdrnum(covrpf.getChdrnum());
	lifelnbIO.setLife(covrpf.getLife());
	
	if(isEQ(covrpf.getJlife(),SPACES))
		lifelnbIO.setJlife("00");
	else
		lifelnbIO.setJlife(covrpf.getJlife());
	
	lifelnbIO.setFunction(varcom.readr);
	lifelnbIO.setFormat(liferec);
	SmartFileCode.execute(appVars, lifelnbIO);
	if(isNE(lifelnbIO.getStatuz(),varcom.mrnf) && isNE(lifelnbIO.getStatuz(),varcom.oK)) 
	{
		syserrrec.statuz.set(lifelnbIO.getStatuz());
		syserrrec.params.set(lifelnbIO.getParams());
		fatalError900();
	}

		getRider();

}

protected void getRider(){
//	zrstIO.setDataKey (SPACES);
//	zrstIO.setFunction(varcom.begn);
//	zrstIO.setChdrcoy(covrIO.getChdrcoy());
//	zrstIO.setChdrnum(covrIO.getChdrnum());
//	zrstIO.setLife(covrIO.getLife());
//	zrstIO.setCoverage (covrIO.getCoverage ());
//	zrstIO.setRider (covrIO.getRider());
//	zrstIO.setFormat(zrstrec);
//	zrstIO.setStatuz(Varcom.oK);
	zrstpf = new Zrstpf();
	zrstpf.setChdrcoy(covrpf.getChdrcoy());
	zrstpf.setChdrnum(covrpf.getChdrnum());
	zrstpf.setLife(covrpf.getLife());
	zrstpf.setCoverage(covrpf.getCoverage());
	zrstpf.setRider(covrpf.getRider());
	zrstpf.setSacstyp("MC");
	List<Zrstpf> zrstpfList = zrstpfDAO.findByKey(zrstpfDAO.getDBSchema(),zrstpf,"CHDRCOY ASC,CHDRNUM ASC,LIFE ASC,COVERAGE ASC,RIDER ASC,UNIQUE_NUMBER DESC");
	
	if(zrstpfList == null || zrstpfList.isEmpty())
	{		
		wsaaComponentCoi.set(ZERO);
		return;
	}
	Iterator<Zrstpf> zrstpfIT = zrstpfList.iterator();
	while (zrstpfIT.hasNext())
	{
		zrstpf = zrstpfIT.next();
		wsaaComponentCoi.set(zrstpf.getZramount01());
		wsaaTotalCoi.add(zrstpf.getZramount01().doubleValue());
		break;
	}

}

//protected void readZrst(){
////	SmartFileCode.execute(appVars, zrstIO);
////	if(isNE(zrstIO.getStatuz(), varcom.oK)
////		&& isNE(zrstIO.getStatuz(), varcom.endp)){
////			syserrrec.statuz.set(zrstIO.getStatuz());
////			syserrrec.params.set(zrstIO.getParams());
////			fatalError900();	
////		}
////	if(isNE(zrstIO.getStatuz(),Varcom.oK)						
////		|| isNE(zrstIO.getChdrnum(),covrIO.getChdrnum())
////		|| isNE(zrstIO.getChdrcoy(),covrIO.getChdrcoy())
////	    || isNE(zrstIO.getLife(),covrIO.getLife())
////		|| isNE(zrstIO.getCoverage(),covrIO.getCoverage())
////		|| isNE(zrstIO.getRider(),covrIO.getRider()))
////		{
////			wsaaComponentCoi.set(ZERO);
////			zrstIO.setStatuz(Varcom.endp);
////			return;
////		}
//	//if(isEQ(zrstIO.getSacstyp(),"MC")) {
//		wsaaComponentCoi.set(zrstIO.getZramount01());
//		wsaaTotalCoi.add(zrstIO.getZramount01());
//		zrstIO.setStatuz(Varcom.endp);
//		return;
//	}
//		//zrstIO.setFunction(Varcom.nextr);		
//}

protected void format250()
{
	/*  Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-sep-2013
	 */
	//Ticket #TMLII-1398 [Incorrect Values in XML for Insurance Change, Paid Up Letter ]
			offset.set(302);			
			oldoffset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(oldoffset,1).toInt()],fldlen[sub(oldoffset,1).toInt()]));
			
			//strpos[offset.toInt()].set(302);
			fldlen[offset.toInt()].set(length(wsaaMainPremCessTerm));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()],wsaaMainPremCessTerm);
}


protected void format255()
{
	/*  Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-sep-2013
	 */
		//Ticket #TMLII-1398 [Incorrect Values in XML for Insurance Change, Paid Up Letter ]
		offset.add(1);
		//offset.set(303);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaRiskcommdte));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()],wsaaRiskcommdte);
}

protected void format260()
{
	/*  Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-sep-2013
	 */
	//Ticket #TMLII-1398 [Incorrect Values in XML for Insurance Change, Paid Up Letter ]
		//offset.set(304);
	//compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		//strpos[offset.toInt()].set(304);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaPremAmt.set(wsaaTotalCoi);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);	
}

//ILAE-63 END
// Add by YY
protected void format248()
{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaZlinstprem.set(covrpf.getZlinstprem());
		fldlen[offset.toInt()].set(length(wsaaZlinstprem));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaZlinstprem.getbigdata());	
		
		Lextpf lextpf = new Lextpf();
		lextpf.setChdrcoy(covrpf.getChdrcoy());
		lextpf.setChdrnum(covrpf.getChdrnum());
		lextpf.setLife(covrpf.getLife());
		lextpf.setCoverage(covrpf.getCoverage());
		lextpf.setRider(covrpf.getRider());
		List<Lextpf> list = lextpfDAO.getLextData(lextpf);
		wsaaTerm.set(0);
		if(list != null && !list.isEmpty()) {
			wsaaTerm.set(list.get(0).getExtCessTerm());
		} 
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTerm);
}
}
