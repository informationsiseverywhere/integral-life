/*
 * File: Pcplfndsw.java
 * Date: 30 August 2009 1:01:21
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLFNDSW.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Effective Date
*  002  Switch Fee
*  003  Source Fund Name
*  004  Source Fund Amount
*  005  Source Fund Units
*  006  Source Fund Currency
*  007  Target Fund Name
*  008  Target Fund Amount
*  009  Target Fund Currency
*  010  Target Fund Units
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include Total Premium which is not an occurance field.
*
*  Processing.
*  -----------
*  This routine extracts all the coverages for the contract.
*
*
*
***********************************************************************
* </pre>
*/
public class Pcplfndsw extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLFNDSW";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(10).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaEffDate = new PackedDecimalData(8, 0);
	private String wsaaFeeFnd = "";
	private PackedDecimalData wsaaFundFee = new PackedDecimalData(9, 2).setUnsigned();
	private PackedDecimalData wsaaSrcSub = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaTgtSub = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);

		/* WSAA-SRC-FNDS */
	private FixedLengthStringData[] wsaaSrcFunds = FLSInittedArray (20, 21);    // IBPLIFE-7496
	private FixedLengthStringData[] wsaaSrcFnd = FLSDArrayPartOfArrayStructure(4, wsaaSrcFunds, 0);
	private PackedDecimalData[] wsaaSrcAmt = PDArrayPartOfArrayStructure(9, 2, wsaaSrcFunds, 4, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaSrcCur = FLSDArrayPartOfArrayStructure(3, wsaaSrcFunds, 9);
	private PackedDecimalData[] wsaaSrcUnt = PDArrayPartOfArrayStructure(16, 5, wsaaSrcFunds, 12, UNSIGNED_TRUE);

		/* WSAA-TGT-FNDS */
	private FixedLengthStringData[] wsaaTgtFunds = FLSInittedArray (20, 21);   //IBPLIFE-7496
	private FixedLengthStringData[] wsaaTgtFnd = FLSDArrayPartOfArrayStructure(4, wsaaTgtFunds, 0);
	private PackedDecimalData[] wsaaTgtAmt = PDArrayPartOfArrayStructure(9, 2, wsaaTgtFunds, 4, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaTgtCur = FLSDArrayPartOfArrayStructure(3, wsaaTgtFunds, 9);
	private PackedDecimalData[] wsaaTgtUnt = PDArrayPartOfArrayStructure(16, 5, wsaaTgtFunds, 12, UNSIGNED_TRUE);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(7000).init(SPACES);   //IBPLIFE-7496
	private FixedLengthStringData wsaaFundCur = new FixedLengthStringData(3);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaFundAmt = new ZonedDecimalData(16, 2).setPattern("ZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaFundUnt = new ZonedDecimalData(18, 5).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99999");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (120, 6);    //IBPLIFE-7496
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String hitrrevrec = "HITRREVREC";
	private static final String utrnrevrec = "UTRNREVREC";
	private static final String acmvrec = "ACMVREC";
	private static final String itdmrec = "ITEMREC";
		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private AcmvsacTableDAM acmvsacIO = new AcmvsacTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Getdescrec getdescrec = new Getdescrec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplfndsw() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getDetails200();
			formatOthers400();
			for (wsaaSrcSub.set(1); !(isEQ(wsaaSrcFnd[wsaaSrcSub.toInt()],SPACES)
			&& isEQ(wsaaTgtFnd[wsaaSrcSub.toInt()],SPACES)); wsaaSrcSub.add(1)){
				formatSourceFund410();
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		if (isEQ(pcpdatarec.fldOffset,1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaCounter.set(ZERO);
		wsaaSrcSub.set(ZERO);
		wsaaTgtSub.set(ZERO);
		wsaaFundFee.set(ZERO);
		wsaaFundAmt.set(ZERO);
		for (wsaaSrcSub.set(1); !(isGT(wsaaSrcSub,10)); wsaaSrcSub.add(1)){
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(SPACES);
			wsaaTgtFnd[wsaaSrcSub.toInt()].set(SPACES);
			wsaaSrcCur[wsaaSrcSub.toInt()].set(SPACES);
			wsaaTgtCur[wsaaSrcSub.toInt()].set(SPACES);
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(ZERO);
			wsaaTgtAmt[wsaaSrcSub.toInt()].set(ZERO);
			wsaaSrcUnt[wsaaSrcSub.toInt()].set(ZERO);
			wsaaTgtUnt[wsaaSrcSub.toInt()].set(ZERO);
		}
		wsaaSrcSub.set(ZERO);
		/*EXIT*/
	}

protected void getDetails200()
	{
		getData200();
		readT5688210();
		readT5645220();
		fund230();
	}

protected void getData200()
	{
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.oK)
		&& (isNE(chdrIO.getChdrpfx(),"CH")
		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum()))) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
	}

protected void readT5688210()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(letcIO.getRdoccoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrIO.getCnttype());
		itdmIO.setItmfrm(chdrIO.getOccdate());
		itdmIO.setItempfx("IT");
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),chdrIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemitem(chdrIO.getCnttype());
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void readT5645220()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.begn);
		itemIO.setItemcoy(letcIO.getRdoccoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem("FUNDSWCH");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		switchFee340();
	}

protected void fund230()
	{
		hitrrevIO.setStatuz(varcom.oK);
		hitrrevIO.setChdrcoy(letcIO.getRdoccoy());
		hitrrevIO.setChdrnum(letcIO.getRdocnum());
		hitrrevIO.setTranno(letcIO.getTranno());
		hitrrevIO.setFeedbackInd("Y");
		hitrrevIO.setFormat(hitrrevrec);
		hitrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hitrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		readHitrrev300();
		while ( !(isEQ(hitrrevIO.getStatuz(),varcom.endp)
		|| isNE(hitrrevIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(hitrrevIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(hitrrevIO.getTranno(),letcIO.getTranno()))) {
			intFndDets310();
			hitrrevIO.setFunction(varcom.nextr);
			readHitrrev300();
		}
		
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(letcIO.getRdoccoy());
		utrnrevIO.setChdrnum(letcIO.getRdocnum());
		utrnrevIO.setTranno(letcIO.getTranno());
		utrnrevIO.setFeedbackInd("Y");
		utrnrevIO.setFormat(utrnrevrec);
		utrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		readUtrnrev320();
		while ( !(isEQ(utrnrevIO.getStatuz(),varcom.endp)
		|| isNE(utrnrevIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(utrnrevIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(utrnrevIO.getTranno(),letcIO.getTranno()))) {
			unlFndDets330();
			utrnrevIO.setFunction(varcom.nextr);
			readUtrnrev320();
		}
		
	}

protected void readHitrrev300()
	{
		/*HITRREV*/
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			syserrrec.statuz.set(hitrrevIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void intFndDets310()
	{
		intFnd310();
	}

protected void intFnd310()
	{
		if (isLT(hitrrevIO.getContractAmount(),0)) {
			wsaaSrcSub.add(1);
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(hitrrevIO.getZintbfnd());
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(hitrrevIO.getContractAmount());
			wsaaSrcCur[wsaaSrcSub.toInt()].set(hitrrevIO.getCntcurr());
			wsaaSrcUnt[wsaaSrcSub.toInt()].set(ZERO);
		}
		else {
			wsaaTgtSub.add(1);
			wsaaTgtFnd[wsaaTgtSub.toInt()].set(hitrrevIO.getZintbfnd());
			wsaaTgtAmt[wsaaTgtSub.toInt()].set(hitrrevIO.getContractAmount());
			wsaaTgtCur[wsaaTgtSub.toInt()].set(hitrrevIO.getCntcurr());
			wsaaTgtUnt[wsaaTgtSub.toInt()].set(ZERO);
		}
		wsaaEffDate.set(hitrrevIO.getEffdate());
	}

protected void readUtrnrev320()
	{
		/*UTRNREV*/
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)
		&& isNE(utrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void unlFndDets330()
	{
		unlFnd330();
	}

protected void unlFnd330()
	{
		if (isLT(utrnrevIO.getContractAmount(),0)) {
			wsaaSrcSub.add(1);
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(utrnrevIO.getUnitVirtualFund());
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(utrnrevIO.getContractAmount());
			wsaaSrcCur[wsaaSrcSub.toInt()].set(utrnrevIO.getCntcurr());
			wsaaSrcUnt[wsaaSrcSub.toInt()].set(utrnrevIO.getNofDunits());
		}
		else {
			wsaaTgtSub.add(1);
			wsaaTgtFnd[wsaaTgtSub.toInt()].set(utrnrevIO.getUnitVirtualFund());
			wsaaTgtAmt[wsaaTgtSub.toInt()].set(utrnrevIO.getContractAmount());
			wsaaTgtCur[wsaaTgtSub.toInt()].set(utrnrevIO.getCntcurr());
			wsaaTgtUnt[wsaaTgtSub.toInt()].set(utrnrevIO.getNofDunits());
		}
		wsaaEffDate.set(utrnrevIO.getMoniesDate());
	}

protected void switchFee340()
	{
		start340();
	}

protected void start340()
	{
		wsaaFeeFnd = "N";
		acmvsacIO.setParams(SPACES);
		acmvsacIO.setRldgcoy(chdrIO.getChdrcoy());
		acmvsacIO.setRldgacct(chdrIO.getChdrnum());
		acmvsacIO.setOrigcurr(chdrIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc,"Y")) {
			wsaaSacscode.set(t5645rec.sacscode06);
			wsaaSacstyp.set(t5645rec.sacstype06);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstyp.set(t5645rec.sacstype01);
		}
		acmvsacIO.setSacscode(wsaaSacscode);
		acmvsacIO.setSacstyp(wsaaSacstyp);
		acmvsacIO.setFormat(acmvrec);
		acmvsacIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		acmvsacIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		readAcmvsac350();
		while ( !(isEQ(acmvsacIO.getStatuz(),varcom.endp)
		|| isNE(acmvsacIO.getRldgcoy(),chdrIO.getChdrcoy())
		|| isNE(acmvsacIO.getRldgacct(),chdrIO.getChdrnum())
		|| isNE(acmvsacIO.getOrigcurr(),chdrIO.getCntcurr())
		|| isNE(acmvsacIO.getSacscode(),wsaaSacscode)
		|| isNE(acmvsacIO.getSacstyp(),wsaaSacstyp)
		|| isEQ(wsaaFeeFnd,"Y"))) {
			if (isEQ(acmvsacIO.getTranno(),letcIO.getTranno())) {
				wsaaFeeFnd = "Y";
				wsaaFundFee.set(acmvsacIO.getOrigamt());
			}
			else {
				acmvsacIO.setFunction(varcom.nextr);
				readAcmvsac350();
			}
		}
		
	}

protected void readAcmvsac350()
	{
		/*ACMVSAC*/
		SmartFileCode.execute(appVars, acmvsacIO);
		if (isNE(acmvsacIO.getStatuz(),varcom.oK)
		&& isNE(acmvsacIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(acmvsacIO.getParams());
			syserrrec.statuz.set(acmvsacIO.getStatuz());
			letcIO.setStatuz(acmvsacIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void formatOthers400()
	{
		format401();
		format402();
	}

protected void format401()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		datcon1rec.intDate.set(wsaaEffDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format402()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundAmt.set(wsaaFundFee);
		fldlen[offset.toInt()].set(length(wsaaFundAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundAmt);
		/*EXIT*/
	}

protected void formatSourceFund410()
	{
		start410();
		format412();
		format414();
		format415();
		format416();
		format418();
	}

protected void start410()
	{
		wsaaCounter.add(1);
		/*FORMAT*/
		if (isEQ(offset,0)) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(wsaaSrcFnd[wsaaSrcSub.toInt()]);
		getdesc500();
		fldlen[offset.toInt()].set(length(getdescrec.longdesc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(getdescrec.longdesc, 1, fldlen[offset.toInt()]));
	}

protected void format412()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundAmt.set(wsaaSrcAmt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundAmt);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundCur.set(wsaaSrcCur[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundCur));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundCur);
	}

protected void format414()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundUnt.set(wsaaSrcUnt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundUnt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundUnt);
	}

protected void format415()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(wsaaTgtFnd[wsaaSrcSub.toInt()]);
		getdesc500();
		fldlen[offset.toInt()].set(length(getdescrec.longdesc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(getdescrec.longdesc, 1, fldlen[offset.toInt()]));
	}

protected void format416()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundAmt.set(wsaaTgtAmt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundAmt);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundCur.set(wsaaTgtCur[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundCur));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundCur);
	}

protected void format418()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundUnt.set(wsaaTgtUnt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundUnt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundUnt);
		/*EXIT*/
	}

protected void getdesc500()
	{
		/*GET*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(pcpdatarec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.fill("?");
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
