/*
 * File: Pcplbnfytr.java
 * Date: 30 August 2009 1:00:15
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLBNFYTR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.enquiries.dataaccess.TbnfytrTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Last Beneficiary Number
*  002  Last Beneficiary Name
*
*  Overview.
*  ---------
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplbnfytr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLBNFYTR";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private final int wsaaStoreRrn = 0;
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(2, 0).init(2);
	private PackedDecimalData wsaaIx = new PackedDecimalData(5, 0);
	private String wsaaFirstTime = "Y";
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1200).init(SPACES);
	private FixedLengthStringData wsaaBnyclt = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaOtherKeys, 1);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOtherKeys, 5);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (10, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String tbnfytrrec = "TBNFYTRREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TbnfytrTableDAM tbnfytrIO = new TbnfytrTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplbnfytr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)
		&& isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			for (wsaaIx.set(1); !(isGT(wsaaIx,pcpdatarec.fldOccurMax)
			|| isEQ(tbnfytrIO.getStatuz(),varcom.endp)); wsaaIx.add(1)){
				formatBnfy200();
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		tbnfytrIO.setStatuz(varcom.oK);
		tbnfytrIO.setFormat(tbnfytrrec);
		//wsaaOtherKeys.set(letcIO.getOtherKeys());
		wsaaTranno.set(letcIO.getTranno());
		tbnfytrIO.setChdrcoy(letcIO.getChdrcoy());
		tbnfytrIO.setChdrnum(letcIO.getChdrnum());
		tbnfytrIO.setBnyclt(SPACES);
		/*    MOVE '1'                    TO TBNFYTR-VALIDFLAG.            */
		tbnfytrIO.setValidflag("2");
		tbnfytrIO.setTranno(wsaaTranno);
		tbnfytrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tbnfytrIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void formatBnfy200()
	{
		/*FORMAT*/
		callTbnfytrioSeq300();
		if (isEQ(tbnfytrIO.getStatuz(),varcom.oK)) {
			getBeneficiary400();
		}
		/*EXIT*/
	}

protected void callTbnfytrioSeq300()
	{
			begin310();
		}

protected void begin310()
	{
		SmartFileCode.execute(appVars, tbnfytrIO);
		if (isNE(tbnfytrIO.getStatuz(),varcom.oK)
		&& isNE(tbnfytrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(tbnfytrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(tbnfytrIO.getFunction(),varcom.begn)
		&& (isEQ(tbnfytrIO.getStatuz(),varcom.endp)
		|| isNE(tbnfytrIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(tbnfytrIO.getChdrnum(),letcIO.getChdrnum())
		|| isNE(tbnfytrIO.getValidflag(),"2")
		|| isNE(tbnfytrIO.getTranno(),wsaaTranno))) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(3);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(3);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "NIL");
			wsaaCounter.add(1);
			tbnfytrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(tbnfytrIO.getStatuz(),varcom.endp)
		|| isNE(tbnfytrIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(tbnfytrIO.getChdrnum(),letcIO.getChdrnum())
		|| isNE(tbnfytrIO.getValidflag(),"2")
		|| isNE(tbnfytrIO.getTranno(),wsaaTranno)) {
			tbnfytrIO.setStatuz(varcom.endp);
			return ;
		}
		/*    IF TBNFYTR-VALIDFLAG        = '1'                            */
		/*    OR TBNFYTR-TRANNO       NOT = WSAA-TRANNO                    */
		/*       MOVE NEXTR               TO TBNFYTR-FUNCTION              */
		/*       GO TO 399-EXIT                                            */
		/*    END-IF.                                                      */
		tbnfytrIO.setFunction(varcom.nextr);
		wsaaCounter.add(1);
	}

protected void getBeneficiary400()
	{
		begin410();
		prepareField315();
	}

protected void begin410()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(pcpdatarec.fsuco);
		namadrsrec.clntNumber.set(tbnfytrIO.getBnyclt());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}

protected void prepareField315()
	{
		/*  Build the Last beneficiary Number*/
		wsaaBnyclt.set(tbnfytrIO.getBnyclt());
		fldlen[offset.toInt()].set(length(wsaaBnyclt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBnyclt);
		/*  Build the Last beneficiary Name*/
		offset.add(1);
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
