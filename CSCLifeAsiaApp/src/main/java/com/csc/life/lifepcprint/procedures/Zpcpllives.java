/*
 * File: Zpcpllives.java as per requirement of TMLII-561
 * Date: 17 July 2013 1:02:23
 * Author: Anjali Arora 
 * 
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Lives Name					}
*  002  Lives DOB					}
*  003  Lives Age					}
*  004  Lives Smoker Indicator		}	all fields occur 10 times
*  005  Lives Sex					}
*  006  Lives ID Number	
*  007  Lives Number			}	
*  008	Lives Occupation
*  009	Lives Occupation Class
*  010	Lives Smoker Description
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Recurring Fields Retrieved, in this case it should be equal to 6
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Zpcpllives extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "ZPCPLLIVES ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaAnbAtCcd = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1060).init(SPACES);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(7).setUnsigned();
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(420);
	private ZonedDecimalData wsaaLifeAge = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(70, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private PackedDecimalData wsaaIx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLife = new PackedDecimalData(3, 0);
	private String liferec = "LIFEREC";
		/*Client logical file*/
	private ClntTableDAM clntIO = new ClntTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	//TMLII-1901 : Gender description in the Letter
	private String t5670 = "T5670";
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30).init(SPACES);
	private String descrec = "DESCREC";
	
	//End

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit299
	}

	public Zpcpllives() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}


protected void main010()
{
	if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
		initialise100();
		while ( !(isEQ(lifeIO.getStatuz(),varcom.endp))) {
			getLifeAndFormat200();
		}
		
	}
	if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
		pcpdatarec.statuz.set(varcom.endp);
		goTo(GotoLabel.exit090);
	}
	compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
	pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
	pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
	pcpdatarec.statuz.set(varcom.oK);
	}


protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaStoreRrn.set(letcIO.getRrn());
		lifeIO.setParams(SPACES);
		lifeIO.setStatuz(varcom.oK);
		lifeIO.setChdrcoy(letcIO.getRdoccoy());
		lifeIO.setChdrnum(letcIO.getRdocnum());
		lifeIO.setLife("01");
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		wsaaAnbAtCcd.set(ZERO);
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
	}

protected void getLifeAndFormat200()
	{
		try {
			getData200();
			format201();
			format202();
			format203();
			format204();
			format205();
			format206();
			format208();
			format209();
			format210();
		}
		catch (GOTOException e){
		}
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		lifeIO.setFunction(varcom.nextr);
		
		if(isEQ(lifeIO.getStatuz(),varcom.oK)
		&& isEQ(lifeIO.getChdrcoy(),letcIO.getRdoccoy())
		&& isEQ(lifeIO.getChdrnum(),letcIO.getRdocnum())) {
			if(isNE(lifeIO.getValidflag(),"2")) {
				clntIO.setClntpfx("CN");
				clntIO.setClntcoy(letcIO.getClntcoy());
				clntIO.setClntnum(lifeIO.getLifcnum());
				clntIO.setFunction(varcom.readr);
				SmartFileCode.execute(appVars, clntIO);
				if (isNE(clntIO.getStatuz(),varcom.oK)) {
					syserrrec.params.set(clntIO.getParams());
					fatalError600();
				}
				wsaaCounter.add(1);
			}
			else {
				getLifeAndFormat200(); 
			}
		}
		if(isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(lifeIO.getChdrnum(),letcIO.getRdocnum()))
		{
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		
	}
		
		
	
protected void format201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(lifeIO.getLifcnum());
		namadrsrec.function.set("PYNMN");
		namadrsrec.language.set(pcpdatarec.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void format202()
	{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	datcon1rec.datcon1Rec.set(SPACES);
	if (isNE(clntIO.getCltdob(),0)
	&& isNE(clntIO.getCltdob(),99999999)) {
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(clntIO.getCltdob());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			return;
		}
	}
	else {
		datcon1rec.extDate.set(SPACES);
	}
	fldlen[offset.toInt()].set(length(datcon1rec.extDate));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format203()
	{
	
	chdrIO.setChdrpfx("CH");
	chdrIO.setChdrcoy(letcIO.getRdoccoy());
	chdrIO.setChdrnum(letcIO.getRdocnum());
	chdrIO.setCurrfrom(lifeIO.getCurrfrom());
	chdrIO.setFunction(varcom.begn);
	//TMLII-1853 Query Performance optimization code commented to resolve this issue.	
	//chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	//chdrIO.setFitKeysSearch("CHDRPFX","CHDRCOY","CHDRNUM","CURRFROM");
	SmartFileCode.execute(appVars, chdrIO);
	if (isNE(chdrIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(chdrIO.getParams());
		fatalError600();
	}
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	if (isNE(clntIO.getCltdob(),0)
	&& isNE(clntIO.getCltdob(),99999999)) {
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(pcpdatarec.language);
		agecalcrec.cnttype.set(chdrIO.getCnttype());
		agecalcrec.intDate1.set(clntIO.getCltdob());
		agecalcrec.intDate2.set(chdrIO.getOccdate());
		agecalcrec.company.set(letcIO.getClntcoy());
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			syserrrec.params.set(agecalcrec.agecalcRec);
			fatalError600();
		}
		else {
			wsaaLifeAge.set(agecalcrec.agerating);
		}
	}
	else {
		wsaaLifeAge.set(ZERO);
	}
	fldlen[offset.toInt()].set(length(wsaaLifeAge));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLifeAge);
	}

protected void format204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(lifeIO.getSmoking()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], lifeIO.getSmoking());
		/*FORMAT*/
		offset.add(1);
		getClntGenderDesc();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaLongdesc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLongdesc);
	}
//TMLII-1901 : Gender description in the Letter
protected void getClntGenderDesc()
{
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(letcIO.getChdrcoy());
	descIO.setDesctabl(t5670);
	descIO.setDescitem(clntIO.getCltsex());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		wsaaLongdesc.fill("?");
	}
	wsaaLongdesc.set(descIO.getLongdesc());
}
//End

protected void format205()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(clntIO.getSecuityno()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clntIO.getSecuityno());
		/*FORMAT*/
	}

protected void format206()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(lifeIO.getLife()));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], lifeIO.getLife().toInt()-1);
	/*FORMAT*/
}

protected void format208() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	String occpDesc=getDescT3644();
	fldlen[offset.toInt()].set(inspectTally(occpDesc, " ", "CHARACTERS", "    ", null));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], occpDesc);
}

protected String getDescT3644() {
	if(clntIO.getOccpcode()==null || isEQ(clntIO.getOccpcode(),SPACES)) {
		return " ";
	}
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(clntIO.getClntcoy());
	descIO.setDesctabl("T3644");
	descIO.setDescitem(clntIO.getOccpcode());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFormat(descrec);
	descIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),Varcom.oK)
	&& isNE(descIO.getStatuz(),Varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),Varcom.mrnf)) {
		return " ";
	}
	return descIO.getLongdesc().toString().trim();
}

protected void format209() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	String statcodeDesc=getDescT3628();
	fldlen[offset.toInt()].set(inspectTally(statcodeDesc, " ", "CHARACTERS", "    ", null));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], statcodeDesc);
}

protected String getDescT3628() {
	if(clntIO.getStatcode()==null || isEQ(clntIO.getStatcode(),SPACES)) {
		return " ";
	}
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(clntIO.getClntcoy());
	descIO.setDesctabl("T3628");
	descIO.setDescitem(clntIO.getStatcode());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFormat(descrec);
	descIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),Varcom.oK)
	&& isNE(descIO.getStatuz(),Varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),Varcom.mrnf)) {
		return " ";
	}
	return descIO.getLongdesc().toString().trim();
}

protected void format210() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	String smokerDesc=getDescT5668();
	fldlen[offset.toInt()].set(inspectTally(smokerDesc, " ", "CHARACTERS", "    ", null));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], smokerDesc);
}

protected String getDescT5668() {
	if(lifeIO.getSmoking()==null || isEQ(lifeIO.getSmoking(),SPACES)) {
		return " ";
	}
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(lifeIO.getChdrcoy());
	descIO.setDesctabl("T5668");
	descIO.setDescitem(lifeIO.getSmoking());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFormat(descrec);
	descIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),Varcom.oK)
	&& isNE(descIO.getStatuz(),Varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),Varcom.mrnf)) {
		return " ";
	}
	return descIO.getLongdesc().toString().trim();
}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
