/*
 * File: Pcplccval.java
 * Date: 11 September 2013 1:02:23
 * Author: Quipoz Limited
 * 
 * Class transformed from Pcplccval.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.dataaccess.CovrsurTableDAM;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * Description: (2-3 lines)
 * 
 * Offset. Description...............................................
 * 
 * 01   Policy Effective Date
 * 02   Policy Anniversary
 * 03   Policy Cash Value Amount
 * 
 *  WSAA-CONSTANT must be = Number of Fields Retrieved. This
 *  does not include 01, 02, 03 which are not the occurance field.
 *********************************************************************** 
 * </pre>
 */
public class Pcplccval extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLCCVAL";
	private String covrsurrec = "COVRSURREC";
	private Atmodrec atmodrec = new Atmodrec();
	private CovrsurTableDAM covrsurIO = new CovrsurTableDAM();
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private int wsaaNofSingoccFld = 1;
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	/*private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(3600).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	/*TMLII-2432  New Product TM Legacy VIP 
	private FixedLengthStringData filler = new FixedLengthStringData(984);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(164, 6, filler, 0);*/
	private FixedLengthStringData filler = new FixedLengthStringData(1200);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(200, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0,wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0,wsaaStartAndLength, 3);
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private String t5687 = "T5687";
	private String t6598 = "T6598";
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5687rec t5687rec = new T5687rec();
	private String itemrec = "ITEMREC";
	private T6598rec t6598rec = new T6598rec();
	private Srcalcpy srcalcpy = new Srcalcpy();	
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ZonedDecimalData wsaaAccumSurrenderValue = new ZonedDecimalData(17,2).init(0).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private Datcon3rec datcon3rec = new Datcon3rec();
	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8);
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private FixedLengthStringData wsaaRiskdate = new FixedLengthStringData(8);	
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPolicyAnniversay = new PackedDecimalData(18,0).init(ZERO);
	private PackedDecimalData wsaaCounterData = new PackedDecimalData(18, 0).init(ZERO);
	private String covrrec = "COVRREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String itdmrec = "ITEMREC   ";
	private ZonedDecimalData wsaaPolicyTerm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSumCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, exit090, exit299
	}

	public Pcplccval() {
		super();
	}

	public void mainline(Object... parmArray) {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec,parmArray, 0);
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			main000();
		} catch (COBOLExitProgramException e) {
		}	

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		try {
			main010();
		} catch (GOTOException e) {
		} finally {
			exit090();
		}
	}

	protected void main010() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			getData200();
			getDetails200();
		}

	/*	if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set("GEND");
			goTo(GotoLabel.exit090);
		} else if (isGT(pcpdatarec.fldOccur, wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		} else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()],fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		
		*/

		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);;
	}
	
	protected void initialise100() {
		/* PARA */
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaCounter.set(ZERO);
		wsaaCounterData.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaEffdate.set(ZERO);
		wsaaAccumSurrenderValue.set(ZERO);
		wsaaPolicyAnniversay.set(ZERO);
		wsaaRiskdate.set(ZERO);
		offset.set(1);
		covrIO.setParams(SPACES);
		covrIO.setFunction(varcom.begn);
		wsaaSumCount.set(ZERO);
		/* EXIT */
	}
	protected void getSurrValues2000()
	{  
		try {
			for(wsaaSumCount.set(1); !isGT(wsaaSumCount, wsaaPolicyTerm); wsaaSumCount.add(1)){
				start2000();
				wsaaAccumSurrenderValue.set(ZERO);
			}
			
		}
		catch (GOTOException e){
		}
	}

protected void start2000()
	{
		srcalcpy.surrenderRec.set(SPACES);
		srcalcpy.tsvtot.set(ZERO);
		srcalcpy.tsv1tot.set(ZERO);
		srcalcpy.chdrChdrcoy.set(covrIO.getChdrcoy());
		srcalcpy.chdrChdrnum.set(covrIO.getChdrnum());
		srcalcpy.planSuffix.set(covrIO.getPlanSuffix());
		srcalcpy.polsum.set(chdrlnbIO.getPolsum());
		srcalcpy.lifeLife.set(covrIO.getLife());
		srcalcpy.lifeJlife.set(covrIO.getJlife());
		srcalcpy.covrCoverage.set(covrIO.getCoverage());
		srcalcpy.covrRider.set(covrIO.getRider());
		srcalcpy.crtable.set(covrIO.getCrtable());
		srcalcpy.crrcd.set(covrIO.getCrrcd());
		//srcalcpy.ptdate.set(chdrlnbIO.getPtdate());
		//srcalcpy.effdate.set(wsaaEffdate);
		srcalcpy.language.set(pcpdatarec.language);
		srcalcpy.polsum.set(chdrlnbIO.polsum);
		srcalcpy.convUnits.set(covrIO.getConvertInitialUnits());
		srcalcpy.chdrCurr.set(chdrlnbIO.getCntcurr());
		srcalcpy.billfreq.set(chdrlnbIO.getBillfreq());
		srcalcpy.currcode.set(covrIO.getPremCurrency());
		srcalcpy.pstatcode.set(chdrlnbIO.getPstatcode());
		srcalcpy.status.set(SPACES);
		srcalcpy.billfreq.set(chdrlnbIO.billfreq);
		srcalcpy.status.set(SPACES);
		if (isEQ(t6598rec.calcprog, SPACES)) {
			srcalcpy.status.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(covrIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaSumCount);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		srcalcpy.effdate.set(datcon2rec.intDate2);
		srcalcpy.ptdate.set(datcon2rec.intDate2);
		while (!(isEQ(srcalcpy.status, varcom.endp)))
		{
			srcalcpy.estimatedVal.set(ZERO);
			srcalcpy.actualVal.set(ZERO);
			callProgram(t6598rec.calcprog, srcalcpy.surrenderRec);
			if (isNE(srcalcpy.status, varcom.oK)
					&& isNE(srcalcpy.status, varcom.endp)) {
				syserrrec.statuz.set(srcalcpy.status);
				syserrrec.params.set(srcalcpy.surrenderRec);
				fatalError600();
			}
			if (isGT(srcalcpy.estimatedVal, ZERO)) {
				wsaaAccumSurrenderValue.add(srcalcpy.estimatedVal);
			} else 
			{
				wsaaAccumSurrenderValue.add(srcalcpy.actualVal);
			} 
		}
		format202();
		format203();
	}
	
protected void readChdrlnb500() {
	/* RAEDR */
	chdrlnbIO.setFormat(chdrlnbrec);
	chdrlnbIO.setChdrcoy(covrIO.chdrcoy);
	chdrlnbIO.setChdrnum(covrIO.chdrnum);
	chdrlnbIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		// dbError8100();
	}
	/* EXIT */
}

	protected void exit090() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		exitProgram();
	}
	protected void getData200() 
	{
		covrIO.setStatuz(varcom.oK);
		covrIO.setChdrcoy(letcIO.getRdoccoy());
		covrIO.setChdrnum(letcIO.getRdocnum());
		covrIO.setLife("01");
		covrIO.setCoverage(SPACES);
		covrIO.setRider(SPACES);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
				&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError900();
		}
		covrIO.setFunction(varcom.nextr);
		if (isEQ(covrIO.getStatuz(), varcom.endp) || isNE(covrIO.getChdrcoy(), letcIO.getRdoccoy()) || isNE(covrIO.getChdrnum(), letcIO.getRdocnum())) 
		{
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			goTo(GotoLabel.exit299);
		}
		wsaaPremCessTerm.set(covrIO.getPremCessTerm());
		//wsaaCounter.add(1);
	}

	protected void getDetails200() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		readT5687();
		readT6598();
		getDetails100();
		readChdrlnb500();
		getSurrValues2000();	 
	}

	protected void readT5687() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */

		itemIO.setDataArea(SPACES);
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(covrIO.getChdrcoy());
		// itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5687);
		itemIO.setItemitem(covrIO.getCrtable());
		itemIO.setItmfrm(covrIO.getCrrcd());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			// dbError580();
		}
		t5687rec.t5687Rec.set(itemIO.getGenarea());
	}

	protected void readT6598() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		itemIO.setDataArea(SPACES);
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(covrIO.getChdrcoy());
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t6598);
		itemIO.setItemitem(t5687rec.svMethod);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			// dbError580();
		} else {
			t6598rec.t6598Rec.set(itemIO.getGenarea());
		}
	}

	protected void getDetails100() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */

		datcon3rec.datcon3Rec.set(SPACES);
		wsaaEffdate.set(covrIO.getCrrcd());
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		
		//TMLII-2113 : CLONE - surrender value and schedule withdrawal(Tahapan) (TMLII-2109) for TM Education
		//datcon3rec.intDate2.set(covrIO.getPremCessDate());
		datcon3rec.intDate2.set(covrIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		//datcon4rec.freqFactor.set(2);	 
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);	 
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		} else {
			wsaaPolicyTerm.set(datcon3rec.freqFactor.toInt());
		}
		format201();
	}

	protected void fatalError600() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES) || isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		} else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		exitProgram();
	}

	protected void format201() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		offset.set(1);
		if (isEQ(offset, 1)) 
		{
			strpos[offset.toInt()].set(offset);
		}

		fldlen[offset.toInt()].set(length(wsaaEffdate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaEffdate);
	}

	protected void format202() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		wsaaCounter.add(1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		// wsaaPolicyAnniversay.set(sub(wsaaEffdate,wsaaRiskdate));
		wsaaCounterData.add(1);
		wsaaPolicyAnniversay.set(wsaaSumCount);
		fldlen[offset.toInt()].set(length(wsaaPolicyAnniversay));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPolicyAnniversay);
	}

	protected void format203() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaAccumSurrenderValue));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAccumSurrenderValue);
	}

	protected void fatalError900() {
		/*
		 * Comment : Added for LT-01-005 Author : Sankaran J Created:
		 * 12-sep-2013
		 */
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES) || isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		} else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/* EXIT */
		exitProgram();
	}
}
