/*
 * File: Pcplcvtall.java
 * Date: 30 August 2009 1:00:59
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCVTALL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Djustfy;
import com.csc.fsu.general.recordstructures.Djustfyrec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
//ILIFE-3139-STARTS
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.fsu.general.recordstructures.Datcon3rec; 
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.fsu.general.procedures.Datcon3;
//ILIFE-3139-ENDS
//ILIFE-5074
import com.quipoz.framework.util.AppVars;
/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Total Premium
*  002  Total Adjusted Premium
*  003  Reserved-1
*  004  Reserved-2
*  005  Cover/Rider Table
*  006  Cover Description
*  007  Premium
*  008  Sum Insured
*  009  Adjusted Premium
*  010  Installment Prem
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include 1st 4 fields which are not occurance fields. .
*
*  Processing.
*  -----------
*  This routine extracts all the coverages for the contract.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Pcplcvtall extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCVTALL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(6).setUnsigned();
	private final int wsaaNofSingoccFld = 4;
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingpFee = new PackedDecimalData(17, 2);
	public FixedLengthStringData name = new FixedLengthStringData(80);//ILIFE-3139
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  lar                                                            */
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaTotPrem = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaTotAdjprm = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaPremAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaSumin = new ZonedDecimalData(9, 0).setPattern("ZZZ,ZZZ,ZZ9");
	private ZonedDecimalData wsaaSuminWop = new ZonedDecimalData(11, 2).setPattern("ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaDecimal = new ZonedDecimalData(2, 0).setUnsigned();
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (100, 6);//ILIFE-3139
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String covtrec = "COVTREC";
	private static final String chdrrec = "CHDRREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5674 = "T5674";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private CovtTableDAM covtIO = new CovtTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private T5674rec t5674rec = new T5674rec();
	private T5688rec t5688rec = new T5688rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Djustfyrec djustfyrec = new Djustfyrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	//ILIFE-3139- start
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private String liferec = "LIFEREC";
	private ClntTableDAM clntIO = new ClntTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	
	private FixedLengthStringData wsaaCond = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaRiskcommdte = new FixedLengthStringData(10);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	//ILIFE-3139- end

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplcvtall() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isEQ(covtIO.getStatuz(), varcom.endp))) {
				getCovtAndFormat200();
			}
			
			formatTotprem600();
			//ILIFE-3139-STARTS
			formatPremCessTrm();
			formatRiskCommDte();
			//ILIFE-3139- ENDS 
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set("GEND");
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		/*    IF PCPD-FLD-OCCUR > WSAA-COUNTER                             */
		/*       MOVE ENDP               TO PCPD-STATUZ                    */
		/*       GO TO 090-EXIT                                            */
		/*    END-IF.                                                      */
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		if (isLTE(pcpdatarec.fldOffset, wsaaNofSingoccFld)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC)
		|| isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		if (isEQ(strpos[offset.toInt()], 0)
		|| isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		/*  INITIALIZE                     WSAA-START-AND-LENGTH.<S19FIX>*/
		for (offset.set(1); !(isGT(offset, 100)); offset.add(1)){
			wsaaStartAndLength[offset.toInt()].set(ZERO);
		}
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/* MOVE LETC-OTHER-KEYS        TO LSAV-SAVE-OTHER-KEYS.         */
		if (isNE(pcpdatarec.groupOccurance, NUMERIC)) {
			pcpdatarec.groupOccurance.set(1);
		}
		wsaaCounter.set(ZERO);
		wsaaTotPrem.set(ZERO);
		wsaaTotAdjprm.set(ZERO);
		//ILIFE-3139-STARTS
		wsaaPremCessTerm.set(ZERO);
		wsaaRiskcommdte.set(SPACES);
		//ILIFE-3139-ENDS
		reserveOffsets110();
		covtIO.setParams(SPACES);
		covtIO.setFunction(varcom.begn);
	}

protected void reserveOffsets110()
	{
		reserveTotPrem110();
		reserveTotAdjprm110();
		//ILIFE-3139-STARTS
		//reserve1110();
		reservePremCessTrm();
		reserveRiskCommDte();
		//ILIFE-3139-ENDS
	}

protected void reserveTotPrem110()
	{
		/* Reserve the first 4 offsets for single occurance fields.*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaPremAmt.set(wsaaTotPrem);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		djustfyrec.word.set(wsaaPremAmt);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(length(wsaaPremAmt));
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}

protected void reserveTotAdjprm110()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaPremAmt.set(wsaaTotAdjprm);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		djustfyrec.word.set(wsaaPremAmt);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(length(wsaaPremAmt));
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}

protected void reserve1110()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		/*110-RESERVE-1.                                                   */
		/*    ADD  1                      TO OFFSET                        */
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		/*EXIT*/
	}
//ILIFE-3139-STARTS
	protected void reservePremCessTrm() {
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(
				add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1)
						.toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPremCessTerm));
		djustfyrec.word.set(wsaaPremCessTerm);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(length(wsaaPremCessTerm));
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}

	protected void reserveRiskCommDte()
	{	offset.add(1);
		compute(strpos[offset.toInt()], 0).set(
				add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1)
						.toInt()]));
		fldlen[offset.toInt()].set(length(wsaaRiskcommdte));
		djustfyrec.word.set(wsaaRiskcommdte);
		djustfyrec.function.set("RIGHT");
		djustfyrec.length.set(length(wsaaRiskcommdte));
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
	}
//ILIFE-3139-ENDS
protected void getCovtAndFormat200()
	{
		try {
			getData200();
			format201();
			format210();
			format215();
			//ILIFE-3139-STARTS
			getData300();
			format230();
			format235();
			//ILIFE-3139-ENDS
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		covtIO.setStatuz(varcom.oK);
		covtIO.setChdrcoy(letcIO.getRdoccoy());
		covtIO.setChdrnum(letcIO.getRdocnum());
		covtIO.setLife("01");
		covtIO.setCoverage(SPACES);
		covtIO.setRider(SPACES);
		covtIO.setPlanSuffix(ZERO);
		covtIO.setFormat(covtrec);
		SmartFileCode.execute(appVars, covtIO);
		if (isNE(covtIO.getStatuz(), varcom.oK)
		&& isNE(covtIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtIO.getParams());
			fatalError900();
		}
		covtIO.setFunction(varcom.nextr);
		if (isEQ(covtIO.getStatuz(), varcom.endp)
		|| isNE(covtIO.getChdrcoy(), letcIO.getRdoccoy())
		|| isNE(covtIO.getChdrnum(), letcIO.getRdocnum())) {
			covtIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of COVT.  It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
	}

protected void format201()
	{
		/*  Coverage / Rider Table*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(covtIO.getCrtable()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], covtIO.getCrtable());
		/*FORMAT*/
		/*  Coverage Description.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		readT5687500();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void format210()
	{
		/*   Premium*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isNE(covtIO.getInstprem(), ZERO)) {
			wsaaPremAmt.set(covtIO.getInstprem());
			wsaaTotPrem.add(covtIO.getInstprem());
		}
		else {
			wsaaPremAmt.set(covtIO.getSingp());
			wsaaTotPrem.add(covtIO.getInstprem());
		}
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
	}

protected void format215()
	{
		/*   Sum Insured*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		compute(wsaaDecimal, 2).set(mult(covtIO.getSumins(), 100));
		if (isGT(wsaaDecimal, 0)) {
			wsaaSuminWop.set(covtIO.getSumins());
			fldlen[offset.toInt()].set(length(wsaaSuminWop));
			djustfyrec.length.set(length(wsaaSuminWop));
			djustfyrec.word.set(wsaaSuminWop);
		}
		else {
			wsaaSumin.set(covtIO.getSumins());
			fldlen[offset.toInt()].set(length(wsaaSumin));
			djustfyrec.length.set(length(wsaaSumin));
			djustfyrec.word.set(wsaaSumin);
		}
		djustfyrec.function.set("RIGHT");
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
		/*210-FORMAT.                                                      */
		/*   Adjusted Premium*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isNE(covtIO.getInstprem(), ZERO)) {
			wsaaPrem.set(covtIO.getInstprem());
		}
		else {
			wsaaPrem.set(covtIO.getSingp());
		}
		wsaaCntfee.set(ZERO);
		wsaaSingpFee.set(ZERO);
		if (isEQ(wsaaCounter, 1)) {
			checkOtherChg700();
		}
		compute(wsaaPrem, 2).set(add(add(wsaaPrem, wsaaCntfee), wsaaSingpFee));
		wsaaPremAmt.set(wsaaPrem);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		djustfyrec.length.set(length(wsaaPremAmt));
		djustfyrec.word.set(wsaaPremAmt);
		djustfyrec.function.set("RIGHT");
		callProgram(Djustfy.class, djustfyrec.rec);
		if (isEQ(djustfyrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(djustfyrec.rec);
			syserrrec.statuz.set(djustfyrec.statuz);
			fatalError900();
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], djustfyrec.word);
		wsaaTotAdjprm.add(wsaaPrem);
		/*210-FORMAT.                                                      */
		/*   Installment Premium*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaPremAmt.set(covtIO.getZlinstprem());
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
	}
//ILIFE-3139-STARTS
protected void getData300() {
	lifelnbIO.setDataKey(SPACES);
	lifelnbIO.setChdrcoy(covtIO.getChdrcoy());
	lifelnbIO.setChdrnum(covtIO.getChdrnum());
	lifelnbIO.setLife(covtIO.getLife());
	if(isEQ(covtIO.getJlife(),SPACES))
		lifelnbIO.setJlife("00");
	else
		lifelnbIO.setJlife(covtIO.getJlife());
	lifelnbIO.setFunction(varcom.readr);
	lifelnbIO.setFormat(liferec);
	SmartFileCode.execute(appVars, lifelnbIO);
	if(isNE(lifelnbIO.getStatuz(),varcom.mrnf) && isNE(lifelnbIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(lifelnbIO.getStatuz());
		syserrrec.params.set(lifelnbIO.getParams());
		fatalError900();
	}
	
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(letcIO.getClntcoy());
		clntIO.setClntnum(lifelnbIO.getLifcnum());
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError900();
		}
	}


protected void format230()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set("CN");
	namadrsrec.clntCompany.set(letcIO.getClntcoy());
	namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
	namadrsrec.function.set("GIVNM");
	namadrsrec.language.set(pcpdatarec.language);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError900();
	}
	name.set(namadrsrec.name);
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set("CN");
	namadrsrec.clntCompany.set(letcIO.getClntcoy());
	namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
	namadrsrec.function.set("PLNAM");
	namadrsrec.language.set(pcpdatarec.language);
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isNE(namadrsrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(namadrsrec.statuz);
		fatalError900();
	}
	name.set(name.toString().trim()+" "+namadrsrec.name.toString().trim());
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(name, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(name, 1, fldlen[offset.toInt()]));
}

protected void format235() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(ZERO);
	datcon1rec.datcon1Rec.set(SPACES);
	if (isNE(covtIO.getEffdate(),0)
	&& isNE(covtIO.getEffdate(),99999999)) {
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covtIO.getEffdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			return;
		}
	}
	else {
		datcon1rec.extDate.set(SPACES);
	}
	fldlen[offset.toInt()].set(length(datcon1rec.extDate));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	if(isEQ(covtIO.getRider(),"00") && isEQ(covtIO.getCoverage(),"01") && isEQ(covtIO.getLife(),"01")) {
		wsaaRiskcommdte.set(datcon1rec.extDate);
		
		if (isEQ(covtIO.getBillfreq(),"00")) {
			wsaaPremCessTerm.set("1");
		}
		else if (isNE(covtIO.getPremCessTerm(),0)) {
			wsaaPremCessTerm.set(covtIO.getPremCessTerm());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covtIO.getEffdate());
			datcon3rec.intDate2.set(covtIO.getPremCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				wsaaPremCessTerm.set(ZERO);
			}
			else {
				wsaaPremCessTerm.set(datcon3rec.freqFactor);
			}
		}
	}
			
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(ZERO);
	datcon1rec.datcon1Rec.set(SPACES);
	if (isNE(covtIO.getRiskCessDate(),0)
	&& isNE(covtIO.getRiskCessDate(),99999999)) {
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(covtIO.getRiskCessDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			return;
		}
	}
	else {
		datcon1rec.extDate.set(SPACES);
	}
	fldlen[offset.toInt()].set(length(datcon1rec.extDate));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
}


//ILIFE-3139-ENDS
protected void readT5687500()
	{
		getT5687Desc510();
	}

protected void getT5687Desc510()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(covtIO.getChdrcoy());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covtIO.getCrtable());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError900();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void formatTotprem600()
	{
		/*TOT-PREM*/
		/*   Total Premium*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaPremAmt.set(wsaaTotPrem);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
		/*   Total Adjusted Premium*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		wsaaPremAmt.set(wsaaTotAdjprm);
		fldlen[offset.toInt()].set(length(wsaaPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremAmt);
		/*EXIT*/
	}

//ILIFE-3139 start//
protected void formatPremCessTrm() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(wsaaPremCessTerm));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremCessTerm);
}

protected void formatRiskCommDte() { 
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(wsaaRiskcommdte));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskcommdte);	
}
//ILIFE-3139 end//
protected void checkOtherChg700()
	{
		/*OTHER-CHG*/
		readChdr700a();
		readT5688700b();
		readT5674700c();
		/* Get Contract Fee...*/
		wsaaBillfreq.set(chdrIO.getBillfreq());
		readMgfl700d();
		/* Get Single Prem Fee.*/
		wsaaBillfreq.set("00");
		readMgfl700d();
		/*EXIT*/
	}

protected void readChdr700a()
	{
		chdr700a();
	}

protected void chdr700a()
	{
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFormat(chdrrec);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRNUM");
		SmartFileCode.execute(appVars, chdrIO);
		if (isEQ(chdrIO.getStatuz(), varcom.endp)
		|| isNE(chdrIO.getChdrnum(), letcIO.getRdocnum())) {
			syserrrec.params.set(chdrIO.getParams());
			syserrrec.statuz.set(varcom.endp);
			fatalError900();
		}
	}

protected void readT5688700b()
	{
		/*B-T5688*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(letcIO.getRequestCompany());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrIO.getCnttype());
		itdmIO.setItmfrm(chdrIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/*B-EXIT*/
	}

protected void readT5674700c()
	{
		t5674700c();
	}

protected void t5674700c()
	{
		t5674rec.commsubr.set(SPACES);
		if (isEQ(t5688rec.feemeth, SPACES)
		|| isEQ(chdrIO.getBtdate(), chdrIO.getOccdate())) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setItemcoy(letcIO.getRequestCompany());
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError900();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
	}

protected void readMgfl700d()
	{
		mgfl700d();
	}

protected void mgfl700d()
	{
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrIO.getCnttype());
		mgfeelrec.billfreq.set(wsaaBillfreq);
		mgfeelrec.effdate.set(chdrIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrIO.getCntcurr());
		mgfeelrec.company.set(letcIO.getRequestCompany());
		//ILIFE-5074 starts
		if(!AppVars.getInstance().getAppConfig().isVpmsEnable() || isEQ(wsaaBillfreq, chdrIO.getBillfreq()))
		{
			callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
			if (isNE(mgfeelrec.statuz, varcom.oK)
			&& isNE(mgfeelrec.statuz, varcom.endp)) {
				syserrrec.params.set(mgfeelrec.mgfeelRec);
				fatalError900();
			}
		}		
		//ILIFE-5074 ends
		if (isEQ(wsaaBillfreq, "00")) {
			wsaaSingpFee.set(mgfeelrec.mgfee);
		}
		else {
			wsaaCntfee.set(mgfeelrec.mgfee);
		}
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
