/*
 * File: Pcplprmall.java
*/
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.UnltrevTableDAM;
import com.csc.life.reassurance.dataaccess.CovtrasTableDAM;
import com.csc.life.terminationclaims.dataaccess.dao.ZhlcpfDAO;
import com.csc.life.terminationclaims.dataaccess.dao.impl.ZhlcpfDAOImpl;
import com.csc.life.terminationclaims.dataaccess.model.Zhlcpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.newbusiness.dataaccess.dao.UnltpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.impl.UnltpfDAOImpl;
import com.csc.life.newbusiness.dataaccess.model.Unltpf;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Coverage Description    	-> COVDESC		}
*  002  Premium Alloc Fund          -> LPRMFND 	 	}
*  003  Premium Alloc Percent/Amt   -> LPRMPERC 	} occur 55 times (it can handle upto minimum of 5 coverages having 10 funds for total percentage allocation we are using the Premium Alloc Fund as
*  "Total" and Premium Alloc Percent/Amt as total allocation amount to funds in a coverage)
*  
*
*
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
***********************************************************************
* </pre>
*/
public class Pcplprmall extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLPRMALL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaTotalAllocationPrct = new PackedDecimalData(16, 2);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99-");
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private int y = 0;
	private BigDecimal unitallocationfundamonut ;
	
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(4200).init(SPACES);
	private String wsaaFirstTime = "Y";
	private String unitallocationfund = SPACE;
	private String wsaaFirstTimeCoverage = "Y";
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(330);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(55, 6, filler, 0);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0).init(ZERO);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private String unltrevrec = "UNLTREVREC";
	private String descrec = "DESCREC";
	private String t5515 = "T5515";
	private String t5687 = "T5687";
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Unltpf unltpf = new Unltpf();
		/*Logical over MLTHPF*/
	private UnltrevTableDAM unltrevIO = new UnltrevTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private CovtrasTableDAM covtrasIO = new CovtrasTableDAM();
	private String covtrasrec = "COVTRASREC";
	private DescTableDAM descIO = new DescTableDAM();
	private DescTableDAM descIO1 = new DescTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private UnltpfDAO unltpfDAO = new  UnltpfDAOImpl();
	
	private List<Unltpf> ls= null;
	private Iterator lsIterator = null;
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090,
		exit109,
		exit399
	}

	public Pcplprmall() {
		super();
	}

public void mainline(Object... parmArray)
	{
	letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
	pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
	try {
		main000();
	}
	catch (COBOLExitProgramException e) {
	}

	setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000() {
		try {
			main010();
		} catch (GOTOException e) {
		} finally {
			exit090();
		}
}

protected void main010()
	{
	if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)
			&& isNE(letcIO.getRrn(),wsaaStoreRrn)) {
				initialise100();
//				while ( !(isEQ(unltrevIO.getStatuz(),varcom.endp))) {
//					formatUnltrev200();
//				}
				
				if(ls == null || (ls != null  && ls.size()==0)){
					
					return;
				}else{
					formatUnltrev200();
				}
			}
	
			if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				goTo(GotoLabel.exit090);
			}
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
			pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
			pcpdatarec.dataLen.set(fldlen[offset.toInt()]);;
	}

protected void exit090()
{
	exitProgram();
}

protected void initialise100()
{
	try {
		para101();
	}
	catch (GOTOException e){
	}
}

protected void para101()
{
	wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
	wsaaStoreRrn.set(letcIO.getRrn());
	wsaaFirstTime = "Y";
	wsaaFirstTimeCoverage = "Y";
	wsaaTotalAllocationPrct.set(ZERO);
	wsaaSub.set(ZERO);
	
	offset.set(ZERO);
	wsaaCounter.set(ZERO);
	unltpf.setChdrcoy(letcIO.getChdrcoy().toString());
	unltpf.setChdrnum(letcIO.getChdrnum().toString());
	unltpf.setValidflag("1");

	ls=	unltpfDAO.readUnltpf(unltpf); 
	
}


protected void formatUnltrev200()
{
	
	for (Unltpf unltpf:ls) {
		getDetails200(unltpf);
		formatAllFields(unltpf);
		
	
}


}

protected void callUnltrevioSeq300()
{
	try {
		begin310();
	}
	catch (GOTOException e){
	}
}

protected void begin310()
{
	SmartFileCode.execute(appVars, unltrevIO);
	if (isNE(unltrevIO.getStatuz(),varcom.oK)
	&& isNE(unltrevIO.getStatuz(),varcom.endp)) {
		syserrrec.statuz.set(unltrevIO.getStatuz());
		fatalError900();
	}
	if (isEQ(unltrevIO.getStatuz(),varcom.endp)
	|| isNE(unltrevIO.getChdrcoy(),letcIO.getChdrcoy())
	|| isNE(unltrevIO.getChdrnum(),letcIO.getChdrnum())) {
		unltrevIO.setStatuz(varcom.endp);
		goTo(GotoLabel.exit399);
	}
	unltrevIO.setFunction(varcom.nextr);
}

// This function is used to get the coverage description to which funds have been allocated, its going to be repeated in XML 
protected void getDetails200(Unltpf unltpf )
	{
	covtrasIO.setParams(SPACES);
	covtrasIO.setChdrcoy(unltpf.getChdrcoy());
	covtrasIO.setChdrnum(unltpf.getChdrnum());
	covtrasIO.setLife(unltpf.getLife());
	covtrasIO.setCoverage(unltpf.getCoverage());
	covtrasIO.setRider(unltpf.getRider());
	covtrasIO.setFunction(varcom.readr);
	covtrasIO.setFormat(covtrasrec);
	SmartFileCode.execute(appVars, covtrasIO);
	
	if(isEQ(covtrasIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(covtrasIO.getParams());
		fatalError900();
	}
	readT5687500();
	}


//This function is used to format all the fields 1. Coverage description 2. Fund Description 3. Fund allocation amount
protected void formatAllFields(Unltpf unltpf) {
	wsaaTotalAllocationPrct.set(ZERO);
	wsaaFirstTimeCoverage = "Y";
	unitallocationfundamonut = new BigDecimal(0.00);
	for ( int x = 1; !((x > 10) || isEQ(unltpf.getUalfnd(x),SPACES)); x++){
		
		unitallocationfundamonut = unitallocationfundamonut.add(new BigDecimal(unltpf.getUalprc(x).doubleValue()));
		wsaaTotalAllocationPrct.set(unltpf.getUalprc(x));
		unitallocationfund = unltpf.getUalfnd(x);
		
		
		formatUnltrev300();
	}
	format201();
	formatTotalAllocationAmount();
}

protected void formatUnltrev300() {
	format201();
	format202();
	format203();
}

// For formatting the Coverage description
protected void format201() {
	wsaaCounter.add(1);
	if (isEQ(wsaaFirstTime,"Y")) {
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaFirstTime = "N";
	}
	else {
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	}
	if(isEQ(wsaaFirstTimeCoverage,"Y")) {
		fldlen[offset.toInt()].set(length(descIO1.getLongdesc()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO1.getLongdesc());
		wsaaFirstTimeCoverage = "N";
	}
	else {
		fldlen[offset.toInt()].set(length(""));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "");
	}
}

// For formatting the Fund Description
protected void format202() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	descIO.setDataKey(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(unltpf.getChdrcoy());
	descIO.setDesctabl(t5515);
	descIO.setDescitem(unitallocationfund);
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError900();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError900();
	}
	else {
		fldlen[offset.toInt()].set(length(descIO.getLongdesc()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}
}

//For formatting the fund allocation amount
protected void format203()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaAmount.set(wsaaTotalAllocationPrct);
	fldlen[offset.toInt()].set(length(wsaaAmount));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
}

//This function is hardcoding the fund description as "Total" and Fund allocation amount is the total allocation to the Coverage
protected void formatTotalAllocationAmount() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length("Total"));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "Total");
	
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	/*for (int i=1; i<=10 && isNE(unltrevIO.getUalfnd(i),SPACES); i++) {
		wsaaTotalAllocationPrct.add(unltrevIO.getUalprc(i));
	}*/
	wsaaAmount.set(unitallocationfundamonut);
	fldlen[offset.toInt()].set(length(wsaaAmount));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
}

protected void readT5687500()
{
	getT5687Desc510();
}

protected void getT5687Desc510()
{
	descIO1.setDescpfx(smtpfxcpy.item);
	descIO1.setDesccoy(covtrasIO.getChdrcoy());
	descIO1.setDesctabl(t5687);
	descIO1.setDescitem(covtrasIO.getCrtable());
	descIO1.setItemseq(SPACES);
	descIO1.setLanguage(pcpdatarec.language);
	descIO1.setFunction(varcom.readr);
	descIO1.setFormat(descrec);
	SmartFileCode.execute(appVars, descIO1);
	if (isNE(descIO1.getStatuz(),varcom.oK)
	&& isNE(descIO1.getStatuz(),varcom.mrnf)) {
		syserrrec.statuz.set(descIO1.getStatuz());
		syserrrec.params.set(descIO1.getParams());
		fatalError900();
	}
	if (isEQ(descIO1.getStatuz(),varcom.mrnf)) {
		descIO1.setLongdesc(SPACES);
	}
}
protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
