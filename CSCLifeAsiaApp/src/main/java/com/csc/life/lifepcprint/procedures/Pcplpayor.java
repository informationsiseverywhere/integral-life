/*
 * File: Pcplpayor.java
 * Date: 30 August 2009 1:02:35
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLPAYOR.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClbaTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO; //IBPLIFE-680
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO; //ILIFE-8709
import com.csc.fsu.general.dataaccess.model.Payrpf; //ILIFE-8709
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.model.Chdrpf; //IBPLIFE-680
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Payor Client Number
*  002  Payor name
*  003  Payor DOB
*  004  Payor Age
*  005	Payor ID
*  006 	Payor Bank Name
*  007	Payor Bank City
*  008	Payor Bank Branch
*  009	Payor Bank Account Number
*  010	Payor Bank Account Name
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Recurring Fields Retrieved.
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplpayor extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPAYOR";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private ZonedDecimalData wsaaPayorAge = new ZonedDecimalData(3, 0).setPattern("ZZ9");

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (10, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String clrfrec = "CLRFREC";
	private ClntTableDAM clntIO = new ClntTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Agecalcrec agecalcrec = new Agecalcrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	//ILAE-62 ISSUE 8 STARTS
	private ClbaTableDAM clbaIO = new ClbaTableDAM();
	private String clbarec = "CLBAREC";
	private BabrTableDAM babrIO = new BabrTableDAM();
	private String babrrec = "BABRREC";
	//ILAE-62 ISSUE 8 ENDS
 //ILIFE-8709 start	
	private Payrpf payrpf=new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
 //ILIFE-8709 end
 //IBPLIFE-680 start
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	protected Chdrpf chdrpf;
 //IBPLIFE-680 end
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit299
	}

	public Pcplpayor() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getPayrAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaPayorAge.set(ZERO);
		/*EXIT*/
	}

protected void getPayrAndFormat200()
	{
		try {
			getData200();
			format201();
			format202();
			format203();
			format204();
			//ILAE-62 ISSUE 8 STARTS
			//TMLII-561 start //
			format205();
			format206();
			//TMLII-561 end //
			//ILAE-62 ISSUE 8 STARTS
		}
		catch (GOTOException e){
		}
	}

protected void getData200()
	{
	
		chdrpf = chdrpfDAO.readChdrpfForLetter(letcIO.getRdoccoy().toString().trim(), letcIO.getRdocnum().toString().trim()); //IBPLIFE-680
 //ILIFE-8709 start		
		payrpf=new Payrpf();
		payrpf = payrpfDAO.getpayrRecord(letcIO.getRdoccoy().toString(), letcIO.getRdocnum().toString(),1);
		if(payrpf==null) {
			syserrrec.params.set(letcIO.getRdoccoy().concat(letcIO.getRdocnum()));
			fatalError600();
		}
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrpf.getChdrcoy());
 //ILIFE-8709 end
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(letcIO.getClntcoy());
		clntIO.setClntnum(clrfIO.getClntnum());
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
	}

protected void format201()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(clrfIO.getClntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clrfIO.getClntnum());
	}

protected void format202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(clrfIO.getClntnum());
		namadrsrec.function.set("PYNMN");
		namadrsrec.language.set(pcpdatarec.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void format203()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.datcon1Rec.set(SPACES);
		if (isNE(clntIO.getCltdob(),0)
		&& isNE(clntIO.getCltdob(),99999999)) {
			datcon1rec.function.set(varcom.conv);
			datcon1rec.intDate.set(clntIO.getCltdob());
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
		}
		else {
			datcon1rec.extDate.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(clntIO.getCltdob(),0)
		&& isNE(clntIO.getCltdob(),99999999)) {
			initialize(agecalcrec.agecalcRec);
			agecalcrec.function.set("CALCP");
			agecalcrec.language.set(pcpdatarec.language);
			agecalcrec.cnttype.set(chdrpf.getCnttype()); //IBPLIFE-680
			agecalcrec.intDate1.set(clntIO.getCltdob());
			agecalcrec.intDate2.set(chdrpf.getOccdate()); //IBPLIFE-680
			agecalcrec.company.set(letcIO.getClntcoy());
			callProgram(Agecalc.class, agecalcrec.agecalcRec);
			if (isNE(agecalcrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(agecalcrec.statuz);
				syserrrec.params.set(agecalcrec.agecalcRec);
				fatalError600();
			}
			else {
				wsaaPayorAge.set(agecalcrec.agerating);
			}
		}
		else {
			wsaaPayorAge.set(ZERO);
		}
		fldlen[offset.toInt()].set(length(wsaaPayorAge));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPayorAge);
	}
//ILAE-62 ISSUE 8 STARTS
//TMLII-561 start //
protected void format205(){
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clntIO.getSecuityno(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getSecuityno(), 1, fldlen[offset.toInt()]));
}


protected void format206() {
	clbaIO.setClntpfx(clntIO.getClntpfx());
	clbaIO.setClntcoy(clntIO.getClntcoy());
	clbaIO.setClntnum(clntIO.getClntnum());
	clbaIO.setFormat(clbarec);
	clbaIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, clbaIO);
	if(isEQ(clbaIO.getStatuz(),varcom.mrnf)){
		//set spaces for all the following 5 fields
		for(int i=0; i<5; i++) {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	} else if(isEQ(clbaIO.getStatuz(),varcom.oK)) {
		babrIO.setBankkey(clbaIO.getBankkey());
		babrIO.setFormat(babrrec);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(babrIO.getParams());
			fatalError600();
		}
		else {
			//to set bank name
			offset.add(1);
			fldlen[offset.toInt()].set(ZERO);
			fldlen[offset.toInt()].add(30);
			if (isEQ(fldlen[offset.toInt()],ZERO)) {
				fldlen[offset.toInt()].set(1);
			}
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(babrIO.getBankdesc(), 1, fldlen[offset.toInt()]));
			
			//to set bank city
			offset.add(1);
			fldlen[offset.toInt()].set(ZERO);
			fldlen[offset.toInt()].add(inspectTally(babrIO.getBankAddr04(), " ", "CHARACTERS", "    ", null));
			if (isEQ(fldlen[offset.toInt()],ZERO)) {
				fldlen[offset.toInt()].set(1);
			}
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(babrIO.getBankAddr04(), 1, fldlen[offset.toInt()]));
			
			//to set bank branch
			offset.add(1);
			fldlen[offset.toInt()].set(ZERO);
			fldlen[offset.toInt()].add(30);
			if (isEQ(fldlen[offset.toInt()],ZERO)) {
				fldlen[offset.toInt()].set(1);
			}
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(babrIO.getBankdesc(), 31, fldlen[offset.toInt()]));
			
			format207();
		}
	}
}

protected void format207() {
	//to set bank account number
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clbaIO.getBankacckey(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clbaIO.getBankacckey(), 1, fldlen[offset.toInt()]));
	
	//to set bank account name
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clbaIO.getBankaccdsc(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clbaIO.getBankaccdsc(), 1, fldlen[offset.toInt()]));
}
//ILAE-62 ISSUE 8 ENDS
protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
