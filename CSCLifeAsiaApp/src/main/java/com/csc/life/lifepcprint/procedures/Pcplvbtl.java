/*
 * File: Pcplvbtl.java
 * Date: 27 August 2013
 * Author: Arockia Silva Stanislas
 * 
 * TMLI LT-01-007
 */
package com.csc.life.lifepcprint.procedures;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;

import com.csc.fsu.clients.tablestructures.T5999rec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;

import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.terminationclaims.dataaccess.MtlhTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;



import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.csc.smart400framework.procedures.Syserr;

import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Policy Bank Account            -> LVABNKNM
*  002  Policy Virtual Account Number  -> LVBNKANO
*  003  Policy Bank Account Holder     -> LVBNKAH
*
*
*  
*  
***********************************************************************
* </pre>
*/

public class Pcplvbtl extends COBOLConvCodeModel{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLVBTL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	
	private FixedLengthStringData filler = new FixedLengthStringData(18);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(3, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	/* FORMATS */
	private String payrrec = "PAYRREC";
	
	
	
	/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	/*Payor Details Logical File*/
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	
	private T5999rec T5999rec = new T5999rec();
	private Itemkey wsaaItemkey = new Itemkey();
	private String t5999 = "T5999";
	
	private FixedLengthStringData wsaaBnkAcntNme = new FixedLengthStringData(18).init(SPACE);
	private String wsaaVrtulAcntNumr;

	private String wsaaBnkAcntHlder = "PT Tokio Marine Life Insurance Indonesia";
	
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaBillChnl= new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaBillCurr = new FixedLengthStringData(4).isAPartOf(wsaaItem, 1);
	

	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090
	}

	public Pcplvbtl() {
		super();
	}
	
	public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000()
		{
			main010();
			exit090();
		}
	
	protected void main010()
	{
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getDetails200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}
	protected void exit090()
	{
		exitProgram();
	}

	protected void initialise100()
		{
			/*PARA*/
			wsaaStoreRrn.set(letcIO.getRrn());
			offset.set(ZERO);
			/*EXIT*/
		}

	protected void getDetails200()
		{
			start201();
			format201();
		}

	protected void start201()
		{
			payrIO.setParams(SPACES);
			payrIO.setStatuz(varcom.oK);
			payrIO.setChdrcoy(letcIO.getRdoccoy());
			payrIO.setChdrnum(letcIO.getChdrnum());
			payrIO.setPayrseqno(1);
			payrIO.setFunction(varcom.readr);
			payrIO.setFormat(payrrec);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}
		}


	protected void format201()
	{
		/*CURR-CLNTNUM*/
		wsaaBillChnl.set(payrIO.getBillchnl());
		wsaaBillCurr.set(payrIO.getBillcurr());
		
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(payrIO.getChdrcoy());
		itemIO.setItemtabl(t5999);
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if(isEQ(itemIO.getStatuz(),varcom.oK))
		{
			T5999rec.t5999Rec.set(itemIO.getGenarea());
			
			descIO.setParams(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(payrIO.getChdrcoy());
			descIO.setDesctabl(t5999);
			descIO.setDescitem(wsaaItem);
			descIO.setLanguage(pcpdatarec.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isEQ(descIO.getStatuz(),varcom.oK)) {
				wsaaBnkAcntNme.set(descIO.getLongdesc());
			}
		
			offset.add(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(wsaaBnkAcntNme));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBnkAcntNme);
			
			// 	Policy Virtual Account Number
			if(isEQ(wsaaBillChnl,"B")){		
				wsaaVrtulAcntNumr = T5999rec.zbankcde.trim()+"000"+ payrIO.getChdrnum().toString();
			}
			else if(isEQ(wsaaBillChnl,"M")){
				//TMLII-1839 && TMLII-2145 Ticket ask us to change the x000 with 0100.
				wsaaVrtulAcntNumr = T5999rec.zbankcde.substring(1, 4).trim()+"0100"+payrIO.getChdrnum().toString();
			}
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(wsaaVrtulAcntNumr));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaVrtulAcntNumr);
			
			//Policy Bank Account Holder
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(wsaaBnkAcntHlder));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBnkAcntHlder);
		/*EXIT*/
		}
		else
		{
			offset.add(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(SPACE));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACE);
			
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(SPACE));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACE);
			
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(SPACE));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACE);
			
		}
	}
	protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
	
}
