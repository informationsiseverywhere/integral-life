package com.csc.life.lifepcprint.procedures;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
//WS-003 Changes Start.. Phani
import com.csc.life.terminationclaims.dataaccess.SurdclmTableDAM;
import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsTableDAM;
import com.csc.life.contractservicing.dataaccess.LoanenqTableDAM;

//WS-003 Changes End.. Phani

import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
* 001 offset Surrender Amount
* 002 offset Automatic Policy Loan
* 003 offset Policy Loan Balance
* 004 offset Outstanding Premium
* 005 offset Other Balance
* 006 offset Net Amount Transfer
* Overview
* Important Note
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*
*
*  Processing.
*  -----------
* 
***********************************************************************
* </pre>
*/
public class Pcplsurall extends COBOLConvCodeModel {
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLSURALL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(11).setUnsigned();
	private int wsaaNofSingoccFld = 6;
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(7800).init(SPACES);
	/*
	 * WS-003 Changes..Start..Phani
	 * Storage Variables For Storing ACTVALUE ,LSTCAPLAMT,INSTAMT06,OTHERADJST
	 * 
	 *  
	 */
	private ZonedDecimalData wsaaTotActValueAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaPolLoanAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaPolLoanBalAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaOutStndPremAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaOtherAdjAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");	
	private ZonedDecimalData wsaaTotNetAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaFinalAdjustmentAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	//WS-003 Changes..End..Phani
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	
	private FixedLengthStringData filler = new FixedLengthStringData(2040);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(340, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	//WS Changes ..Start..Phani
	private String surdclmrec  = "SURDCLMREC";
	private String surhclmrec  = "SURHCLMREC";
	private String loanrec  = "LOANENQ";
	private String linsrec  = "LINS";
	//WS Changes ..End..Phani
	
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
		/*Contract header file*/
	//WS -003 Changes. Start...Phani
	private SurdclmTableDAM surdclmIO = new SurdclmTableDAM();
	private LinsTableDAM linsIO = new LinsTableDAM();
	private LoanenqTableDAM loanIO = new LoanenqTableDAM();
	private SurhclmTableDAM surhclmIO = new SurhclmTableDAM();
	//WS -003 Changes. End...Phani
	
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit299, 
		exit790c, 
		exit790d
	}

	public Pcplsurall() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

	protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			processPolisySurrender200();
			
		}
		
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}
	
	
	protected void processPolisySurrender200() {
		readSurdclm220();
		readPolisyLoan230();
		readLins240();
		readSurhclm250();
		format260();
		format270();
		format280();
		format290();
		format300();
		calculateNetAmount310();
	}

	protected void exit090()
	{	
		exitProgram();
	}

	protected void initialise100() {
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaCounter.set(ZERO);
		wsaaOtherAdjAmt.set(ZERO);
		wsaaTotActValueAmt.set(ZERO);
		wsaaPolLoanAmt.set(ZERO);
		wsaaPolLoanBalAmt.set(ZERO);
		wsaaOutStndPremAmt.set(ZERO);
		wsaaFinalAdjustmentAmt.set(ZERO);
		wsaaTotNetAmt.set(ZERO);		
		/*EXIT*/
	}
	
	protected void readSurdclm220() {
		surdclmIO.setChdrcoy(letcIO.getRdoccoy());
		surdclmIO.setChdrnum(letcIO.getRdocnum());		
		surdclmIO.setFunction(varcom.begn);
		surdclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surdclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, surdclmIO);
		while ( !(isEQ(surdclmIO.getStatuz(),varcom.endp))) {
			if(isEQ(surdclmIO.getStatuz(),varcom.oK)){
				if (isEQ(surdclmIO.getFieldType(),"A") 
						|| isEQ(surdclmIO.getFieldType(),"S")
						|| isEQ(surdclmIO.getFieldType(),"D")) {			
					wsaaTotActValueAmt.add(surdclmIO.getActvalue());
					}
			}
			surdclmIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, surdclmIO);
		}
		wsaaTotNetAmt.add(wsaaTotActValueAmt);
	}
	

	protected void readPolisyLoan230() {
		loanIO.setChdrcoy(letcIO.getRdoccoy());
		loanIO.setChdrnum(letcIO.getRdocnum());
		loanIO.setFunction(varcom.begn);
		loanIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		loanIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, loanIO);
		if ( isEQ(loanIO.getStatuz(),varcom.oK)) {
				if (isEQ(loanIO.getLoanType(),"A") && isEQ(loanIO.getValidflag(),'1')) {
					wsaaPolLoanAmt.set(loanIO.getLastCapnLoanAmt());
					wsaaTotNetAmt.add(wsaaPolLoanAmt);
				}
				if (isEQ(loanIO.getLoanType(),"P") && isEQ(loanIO.getValidflag(),'1')) {
					wsaaPolLoanBalAmt.set(loanIO.getLastCapnLoanAmt());
					wsaaTotNetAmt.add(wsaaPolLoanBalAmt);
				}
		}
		
	}
	protected void readLins240() {
		linsIO.setChdrcoy(letcIO.getRdoccoy());
		linsIO.setChdrnum(letcIO.getRdocnum());
		linsIO.setFunction(varcom.begn);
		linsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		linsIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, linsIO);
		while ( !(isEQ(linsIO.getStatuz(),varcom.endp))) {
			if (isEQ(linsIO.getStatuz(),varcom.oK)) {
					if (isEQ(linsIO.getPayflag(),'0')) {
						wsaaOutStndPremAmt.add(linsIO.getInstamt06());
					}
			}
			linsIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, linsIO);
		}
		wsaaFinalAdjustmentAmt.add(wsaaOutStndPremAmt);
	}
	

	protected void readSurhclm250() {
		surhclmIO.setChdrcoy(letcIO.getRdoccoy());
		surhclmIO.setChdrnum(letcIO.getRdocnum());
		surhclmIO.setFunction(varcom.begn);
		surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		SmartFileCode.execute(appVars, surhclmIO);
		while ( !(isEQ(surhclmIO.getStatuz(),varcom.endp))) {
			if (isEQ(surhclmIO.getStatuz(),varcom.oK)) {	
				if (isEQ(surhclmIO.getTranno(),letcIO.getTranno())) {
					wsaaOtherAdjAmt.set(surhclmIO.getOtheradjst());
					surhclmIO.setStatuz(varcom.endp);
				}
			}
			if(!(isEQ(surhclmIO.getStatuz(),varcom.endp))){
				surhclmIO.setFunction(varcom.nextr);
				SmartFileCode.execute(appVars, surhclmIO);
			}
		}
		wsaaFinalAdjustmentAmt.add(wsaaOtherAdjAmt);
	}
	
	
	protected void format260(){
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(wsaaTotActValueAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotActValueAmt);
	}
	protected void format270(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPolLoanAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPolLoanAmt);
	}
	protected void format280(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPolLoanBalAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPolLoanBalAmt);
	}
	protected void format290(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaOutStndPremAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOutStndPremAmt);
		
	}
	protected void format300(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaOtherAdjAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOtherAdjAmt);
	}
	protected void calculateNetAmount310() {
		offset.add(1);
		wsaaTotNetAmt.set(sub(wsaaTotActValueAmt,wsaaFinalAdjustmentAmt));
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaTotNetAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTotNetAmt);
	}
	protected void fatalError900()	
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}




}

