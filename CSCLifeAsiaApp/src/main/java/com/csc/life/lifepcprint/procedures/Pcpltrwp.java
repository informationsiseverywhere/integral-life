/*
 * File: Pcpltrwp.java
 * Date: 30 August 2009 1:03:09
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLTRWP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.regularprocessing.dataaccess.TrwpTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* Offset. Description...............................................
*
*  001  Receipt No.
*  002  Receipt Date
*  003  Orig Currency
*  004  Receipt Amount
*
*  NOTE:
*  -----
*  WSAA-CONSTANT refers to the number of occurance fields to
*  be extracted.
*****************************************************************
* </pre>
*/
public class Pcpltrwp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLTRWP";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaRcptAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaEffdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaRwpdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaRwpdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaRwpYr = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
	private ZonedDecimalData wsaaCntr = new ZonedDecimalData(3, 0).setUnsigned();
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String trwprec = "TRWPREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TrwpTableDAM trwpIO = new TrwpTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcpltrwp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isEQ(trwpIO.getStatuz(),varcom.endp))) {
				getTrwpAndFormat200();
			}
			
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/* MOVE LETC-OTHER-KEYS        TO LSAV-SAVE-OTHER-KEYS.         */
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
		trwpIO.setParams(SPACES);
		trwpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		trwpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		trwpIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		trwpIO.setStatuz(varcom.oK);
		trwpIO.setChdrcoy(letcIO.getRdoccoy());
		trwpIO.setChdrnum(letcIO.getRdocnum());
		trwpIO.setFormat(trwprec);
		wsaaCntr.set(ZERO);
		wsaaEffdate.set(letcIO.getLetterRequestDate());
	}

protected void getTrwpAndFormat200()
	{
		try {
			getData200();
			format201();
			format202();
			format203();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, trwpIO);
		if (isNE(trwpIO.getStatuz(),varcom.oK)
		&& isNE(trwpIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(trwpIO.getParams());
			fatalError900();
		}
		if (isEQ(trwpIO.getStatuz(),varcom.endp)
		|| isNE(trwpIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(trwpIO.getChdrnum(),letcIO.getRdocnum())
		|| isGTE(wsaaCntr,12)) {
			trwpIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		if (isNE(letcIO.getTranno(),0)) {
			if (isNE(trwpIO.getTranno(),letcIO.getTranno())) {
				trwpIO.setFunction(varcom.nextr);
				goTo(GotoLabel.exit299);
			}
		}
		if (isNE(trwpIO.getValidflag(),"1")) {
			goTo(GotoLabel.exit299);
		}
		wsaaRwpdate.set(trwpIO.getEffdate());
		if (isNE(wsaaEffYr,wsaaRwpYr)) {
			trwpIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit299);
		}
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of TRWP.  It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
	}

protected void format201()
	{
		/*  Receipt Number*/
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(1);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(trwpIO.getRdocnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], trwpIO.getRdocnum());
	}

protected void format202()
	{
		/*   Receipt Effective Date*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(trwpIO.getEffdate());
		callDatcon1300();
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			goTo(GotoLabel.exit299);
		}
		else {
			fldlen[offset.toInt()].set(length(datcon1rec.extDate));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		}
	}

protected void format203()
	{
		/*  Receipt Currency*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(trwpIO.getOrigcurr()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], trwpIO.getOrigcurr());
		/*FORMAT*/
		/*   Receipt Amount*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaRcptAmt.set(trwpIO.getOrigamt());
		fldlen[offset.toInt()].set(length(wsaaRcptAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRcptAmt);
		trwpIO.setFunction(varcom.nextr);
	}

protected void callDatcon1300()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
