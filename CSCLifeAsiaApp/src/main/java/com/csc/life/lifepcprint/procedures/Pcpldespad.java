/*
 * File: Pcpldespad.java
 * Date: 30 August 2009 1:01:04
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLDESPAD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    New Despatch Client No.  -> LNEWDANO }
*   02    New Despatch Client Name -> LNEWDANM }
*   03    New Despatch Address 1   -> LNEWDAD1 }
*   04    New Despatch Address 2   -> LNEWDAD2 }
*   05    New Despatch Address 3   -> LNEWDAD3 }
*   06    New Despatch Address 4   -> LNEWDAD4 }
*   07    New Despatch Address 5   -> LNEWDAD5 }
*   08    New Despatch Postcode    -> LNEWDPST }
*   09    Prv Despatch Client No.  -> LPRVDANO }
*   10    Prv Despatch Client Name -> LPRVDANM }
*   11    Prv Despatch Address 1   -> LPRVDAD1 }
*   12    Prv Despatch Address 2   -> LPRVDAD2 }
*   13    Prv Despatch Address 3   -> LPRVDAD3 }
*   14    Prv Despatch Address 4   -> LPRVDAD4 }
*   15    Prv Despatch Address 5   -> LPRVDAD5 }
*   16    Prv Despatch Postcode    -> LPRVDPST }
*
*  Overview.
*  ---------
*  This subroutine reads the LETC record and get the new and old
*  despatch address.                                             er.
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcpldespad extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLDESPAD";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaOtherKeys, 1);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOtherKeys, 5).setUnsigned();
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (16, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String clrfrec = "CLRFREC";
	private static final String chdrtrxrec = "CHDRTRXREC";
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcpldespad() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getDataAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		/*EXIT*/
	}

protected void getDataAndFormat200()
	{
		get210();
		formatField220();
		getPreviousDespatch230();
	}

protected void get210()
	{
		clrfIO.setForepfx(letcIO.getRdocpfx());
		clrfIO.setForecoy(letcIO.getRdoccoy());
		clrfIO.setForenum(letcIO.getRdocnum());
		clrfIO.setClrrrole("DA");
		clrfIO.setFunction(varcom.readr);
		callClrfio300();
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(clrfIO.getClntpfx());
		namadrsrec.clntCompany.set(clrfIO.getClntcoy());
		namadrsrec.clntNumber.set(clrfIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callNamadrs500();
	}

protected void formatField220()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(clrfIO.getClntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clrfIO.getClntnum());
		saveDespatch600();
	}

protected void getPreviousDespatch230()
	{
		chdrtrxIO.setParams(SPACES);
		chdrtrxIO.setChdrcoy(letcIO.getChdrcoy());
		chdrtrxIO.setChdrnum(letcIO.getChdrnum());
		//TICKEt# ILIFE-1183, decrement transaction number to get the previous details.
		compute(wsaaTranno, 0).set(sub(letcIO.getTranno(),1));
		//TICKEt# ILIFE-1183 Ends 
		chdrtrxIO.setTranno(wsaaTranno);
		chdrtrxIO.setFunction(varcom.readr);
		callChdrtrxio400();
		
		if (isEQ(chdrtrxIO.getStatuz(),varcom.oK) && isNE(chdrtrxIO.getDespnum(),SPACES))
		{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(chdrtrxIO.getDespcoy());
		namadrsrec.clntNumber.set(chdrtrxIO.getDespnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callNamadrs500();
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.clntNumber));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.clntNumber);
		saveDespatch600();
		}
	}

protected void callClrfio300()
	{
		/*CALL*/
		clrfIO.setFormat(clrfrec);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clrfIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void callChdrtrxio400()
	{
		/*CALL*/
		chdrtrxIO.setFormat(chdrtrxrec);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)  && isNE(chdrtrxIO.getStatuz(),varcom.mrnf)  ) {
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void callNamadrs500()
	{
		/*CALL*/
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError900();
		}
		/*EXIT*/
	}

protected void saveDespatch600()
	{
		formatField620();
	}

protected void formatField620()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr1));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr2);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr3));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr3);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr4));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr4);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.addr5));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr5);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.pcode));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.pcode);
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
