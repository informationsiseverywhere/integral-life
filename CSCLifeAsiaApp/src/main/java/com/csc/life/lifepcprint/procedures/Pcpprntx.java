package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import com.quipoz.COBOLFramework.COBOLFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.fsu.printing.tablestructures.Tr4bzrec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.dataaccess.model.Descpf;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/***
 * 001 Phrase 002 Template ID 003 Include Attachment Yes 004 Include Attachment
 * No 005 Subject 006 Send Email Ind 007 Agent To Email 008 Document Type 009
 * Agent Template Id 010 Agent CC Email 011 Client To Email 012 Client CC Email
 */

public class Pcpprntx extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pcpprntx.class); // GEPAS-4889
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaDesc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(31000).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray(60, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);

	private LetcTableDAM letcIO = new LetcTableDAM();
	private Tr4bzrec tr4bzrec = new Tr4bzrec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private DescpfDAO descpfDao = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private Chdrpf chdr = new Chdrpf();
	private Agntpf agntpf = new Agntpf();
	private Clntpf ownerDetails = new Clntpf();
	private Clntpf agentDetails = new Clntpf();
	private Itempf item = new Itempf();
	private Descpf desc = new Descpf();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private String clientEmailId = "";
	private String clientCcEmailId = "";
	private String agentEmailId = "";
	private String agentCcEmailId = "";
	private String proposalTransactionCode = "";

	public Pcpprntx() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		/* MAIN */
		/* If the PCPD-IDCODE-COUNT is different from the one we had */
		/* before (or we are on the first call) we need to set up the */
		/* data buffer by reading data from the database. */
		if (COBOLFunctions.isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			getDataAndFormat200();
		}
		/* Now we simply set the offset and return the data from */
		/* the data buffer. */
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(COBOLFunctions.subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(Varcom.oK);
		/* EXIT */
		exitProgram();
	}

	protected void initialise100() {
		para101();
	}

	protected void para101() {
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		clientEmailId = "";
		agentEmailId = "";
		clientCcEmailId = "";
		agentCcEmailId = "";
	}

	private void getDataAndFormat200() {
		getData200();
		formatPhrase001();
		formatTemplateId002();
		formatIncludeAttachmentY003();
		formatIncludeAttachmentN004();
		formatSubject005();
		formatSendEmail006();
		formatAgentToEmail007();
		formatDoctype008();
		formatAgntTmpId009();
		formatAgentCcEmail010();
		formatClientToEmail011();
		formatClientCcEmail012();
	}

	private void getData200() {
		String company = letcIO.getRdoccoy().trim(); 
		proposalTransactionCode = letcIO.getTransCode().toString();
           if(proposalTransactionCode.equals( "T643") || proposalTransactionCode.equals("T644") || proposalTransactionCode.equals("TA63")    ){
        	   	chdr = chdrpfDAO.getChdr(letcIO.getRdocpfx().toString().trim(), company, letcIO.getChdrnum().toString().trim(),
					"3", letcIO.getTranno().toString().trim());
		}
		else{
			chdr = chdrpfDAO.getchdrRecord(company, letcIO.getChdrnum().toString().trim());
		}
           
		ownerDetails = clntpfDAO.getClntpf("CN", "9", chdr.getCownnum().trim());
		agntpf = agntpfDAO.getClientData("AG", company, chdr.getAgntnum().trim());
		agentDetails = clntpfDAO.getClntpf("CN", "9", agntpf.getClntnum().trim());

		item = itempfDAO.findItemByItem("IT", company, "TR4BZ", letcIO.getTransCode().trim());
		tr4bzrec.tr4bzRec.set(StringUtil.rawToString(item.getGenarea()));
		desc = descpfDao.getTableDescByItem("IT", company, "T1688", "E", letcIO.getTransCode().trim());
		getClientEmailData();
		getAgentEmailData();
	}

	protected void getClientEmailData() {
		Clexpf clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", ownerDetails.getClntnum());
		if (clexpf == null) {
			clientEmailId = "";
			clientCcEmailId = "";
		} else {
			String toEmailId = clexpf.getRinternet();
			if (toEmailId == null || "".equals(toEmailId.trim())) {
				toEmailId = "";
			}
			clientEmailId = toEmailId.trim();

			String ccEmailId = clexpf.getRinternet2();
			if (ccEmailId == null || "".equals(ccEmailId.trim())) {
				ccEmailId = "";
			}
			clientCcEmailId = ccEmailId.trim();
		}
	}

	private void getAgentEmailData() {
		Clexpf clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", agentDetails.getClntnum());
		if (clexpf == null) {
			agentEmailId = "";
			agentCcEmailId = "";
		} else {
			String toEmailId = clexpf.getRinternet();
			if (toEmailId == null || "".equals(toEmailId.trim())) {
				toEmailId = "";
			}
			agentEmailId = toEmailId.trim();

			String ccEmailId = clexpf.getRinternet2();
			if (ccEmailId == null || "".equals(ccEmailId.trim())) {
				ccEmailId = "";
			}
			agentCcEmailId = ccEmailId.trim();
		}
	}

	private void formatPhrase001() {
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(ZERO);
		String phrase = ownerDetails.getCltdob().toString().trim();
		fldlen[offset.toInt()].add(COBOLFunctions.length(phrase));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], phrase);
	}

	private void formatTemplateId002() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(COBOLFunctions.length(tr4bzrec.onlnIdentifier.trim()));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tr4bzrec.onlnIdentifier.trim());

	}

	private void formatIncludeAttachmentY003() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(COBOLFunctions.length("Y"));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "Y");
	}

	private void formatIncludeAttachmentN004() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "N");
	}

	private void formatSubject005() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		wsaaDesc.set(desc.getLongdesc());
		LOGGER.info("Transaction Code: {} and Transaction Description {}", desc.getDescitem(), desc.getLongdesc());
		fldlen[offset.toInt()].add(COBOLFunctions.inspectTally(wsaaDesc.trim(), " ", "CHARACTERS", "    ", null));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()],
				COBOLFunctions.subString(wsaaDesc, 1, fldlen[offset.toInt()]));
	}

	protected void formatSendEmail006() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		String sendEmail = "Y";
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(COBOLFunctions.length(sendEmail));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sendEmail);
	}

	protected void formatAgentToEmail007() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		if (!"".equals(agentEmailId)) {
			fldlen[offset.toInt()].set(COBOLFunctions.length(agentEmailId));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], agentEmailId);
		} else {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	}

	protected void formatDoctype008() {
		String mergeAndSend = "MERSND";
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(COBOLFunctions.length(mergeAndSend));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], mergeAndSend);
	}

	protected void formatAgntTmpId009() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(COBOLFunctions.length(tr4bzrec.onlnIdentifier.trim()));
		if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tr4bzrec.onlnIdentifier.trim());
	}

	protected void formatAgentCcEmail010() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		if (!"".equals(agentCcEmailId)) {
			fldlen[offset.toInt()].set(COBOLFunctions.length(agentCcEmailId));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], agentCcEmailId);
		} else {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	}

	protected void formatClientToEmail011() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		if (!"".equals(clientEmailId)) {
			fldlen[offset.toInt()].set(COBOLFunctions.length(clientEmailId));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clientEmailId);
		} else {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	}

	protected void formatClientCcEmail012() {
		offset.add(1);
		COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions
				.add(strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
		if (!"".equals(clientCcEmailId)) {
			fldlen[offset.toInt()].set(COBOLFunctions.length(clientCcEmailId));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clientCcEmailId);
		} else {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	}
}