/*
 * File: Pcplnote.java
 * Date: 30 August 2009 1:02:29
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLNOTE.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.dataaccess.NoteTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Note Pad Line 1
*  002  Note Pad Line 2
*  003  Note Pad Line 3
*  004  Note Pad Line 4
*  005  Note Pad Line 5
*
***********************************************************************
* </pre>
*/
public class Pcplnote extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLNOTE";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(200).init(SPACES);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLetoChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLetokeys, 0);
	private FixedLengthStringData wsaaLetoTrancde = new FixedLengthStringData(4).isAPartOf(wsaaLetokeys, 8);
	private FixedLengthStringData wsaaLetoLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 12);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (5, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String noterec = "NOTEREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private NoteTableDAM noteIO = new NoteTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		format206,
		exit299
	}

	public Pcplnote() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getNoteFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaLetokeys.set(letcIO.getOtherKeys());
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getNoteFormat200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start210();
					format201();
					format202();
					format203();
					format204();
					format205();
				case format206:
					format206();
					format208();
					format210();
				case exit299:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start210()
	{
		/* Read NOTE to obtain information for printing*/
		noteIO.setDataKey(SPACES);
		noteIO.setNotepfx("CH");
		noteIO.setNotecoy(letcIO.getRdoccoy());
		noteIO.setNotenum(letcIO.getRdocnum());
		noteIO.setSubkeya(wsaaLetoTrancde);
		noteIO.setNoteseq("999");
		noteIO.setFormat(noterec);
		noteIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		noteIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		noteIO.setFitKeysSearch("NOTEPFX", "NOTECOY", "SUBKEYA");
		SmartFileCode.execute(appVars, noteIO);
		if (isNE(noteIO.getStatuz(),varcom.oK)
		&& isNE(noteIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(noteIO.getParams());
			fatalError600();
		}
		if (isNE(noteIO.getNotepfx(),"CH")
		|| isNE(noteIO.getNotecoy(),letcIO.getRdoccoy())
		|| isNE(subString(noteIO.getNotenum(), 1, 8),letcIO.getRdocnum())
		|| isNE(noteIO.getSubkeya(),wsaaLetoTrancde)) {
			noteIO.setStatuz(varcom.endp);
		}
		if (isNE(noteIO.getStatuz(),varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.format206);
		}
	}

protected void format201()
	{
		/*   Note Pad Line 1*/
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		if (isEQ(noteIO.getNoteline01(),SPACES)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
		else {
			ix.set(0);
			ix.add(inspectTally(noteIO.getNoteline01(), " ", "CHARACTERS", "  ", null));
			fldlen[offset.toInt()].set(ix);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(noteIO.getNoteline01(), 1, ix));
		}
	}

protected void format202()
	{
		/*   Note Pad Line 2*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(noteIO.getNoteline02(),SPACES)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
		else {
			ix.set(0);
			ix.add(inspectTally(noteIO.getNoteline02(), " ", "CHARACTERS", "  ", null));
			fldlen[offset.toInt()].set(ix);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(noteIO.getNoteline02(), 1, ix));
		}
	}

protected void format203()
	{
		/*   Note Pad Line 3*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(noteIO.getNoteline03(),SPACES)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
		else {
			ix.set(0);
			ix.add(inspectTally(noteIO.getNoteline03(), " ", "CHARACTERS", "  ", null));
			fldlen[offset.toInt()].set(ix);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(noteIO.getNoteline03(), 1, ix));
		}
	}

protected void format204()
	{
		/*   Note Pad Line 4*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(noteIO.getNoteline04(),SPACES)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
		else {
			ix.set(0);
			ix.add(inspectTally(noteIO.getNoteline04(), " ", "CHARACTERS", "  ", null));
			fldlen[offset.toInt()].set(ix);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(noteIO.getNoteline04(), 1, ix));
		}
	}

protected void format205()
	{
		/*   Note Pad Line 5*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isEQ(noteIO.getNoteline05(),SPACES)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
		else {
			ix.set(0);
			ix.add(inspectTally(noteIO.getNoteline05(), " ", "CHARACTERS", "  ", null));
			fldlen[offset.toInt()].set(ix);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(noteIO.getNoteline05(), 1, ix));
		}
		goTo(GotoLabel.exit299);
	}

protected void format206()
	{
		/*   Note Pad Line 1*/
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		/*FORMAT*/
		/*   Note Pad Line 2*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
	}

protected void format208()
	{
		/*   Note Pad Line 3*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		/*FORMAT*/
		/*   Note Pad Line 4*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
	}

protected void format210()
	{
		/*   Note Pad Line 5*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
