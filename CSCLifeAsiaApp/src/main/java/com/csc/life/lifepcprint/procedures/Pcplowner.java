/*
 * File: Pcplowner.java
 * Date: 30 August 2009 1:02:32
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLOWNER.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClexTableDAM;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Owner Name.
*   02    Joint Owner Name.
*   03    Owner/s Name/s.
*   04    Owner Number
*   05	  Owner's Date of Birth
*   06	  Owner's Gender
*   07	  Owner's ID
*   08	  Policy Owner Salutation
*   09    Policy Owner First Name
*   10    Policy Owner Last Name
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplowner extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLOWNER ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10);
	private String wsaaFirstTime = "Y";
	private String wsaaFirst = "Y";
	private PackedDecimalData wsaaStrpos = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaFldlen = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaOwnersNames = new FixedLengthStringData(103);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(20, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
		private Datcon1rec datcon1rec = new Datcon1rec();
	//TMLII-664 LT-01-002
	private CltsTableDAM cltsIO = new CltsTableDAM();
	
	/* Tables*/
	//TMLII-664 LT-01-002
	private String t3583 = "T3583";
	
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	// TMLII-561 start //
	private ClexTableDAM clexIO = new ClexTableDAM();
	private String clexrec = "CLEXREC";
	// TMLII-561 end //
	
	//TMLII-1901 : Gender description in the Letter
	private String t5670 = "T5670";
	private FixedLengthStringData wsaaLongdesc = new FixedLengthStringData(30).init(SPACES);
	private String descrec = "DESCREC";
	//End
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		prepareField315
	}

	public Pcplowner() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getChdrAndFormat200();
			clntIO.setClntnum(chdrlifIO.getCownnum());
			getClntAndFormat300();
			clntIO.setClntnum(chdrlifIO.getJownnum());
			getClntAndFormat300();
			currentClntnum400();
			//TMLII-664 LT-01-002 START (Required for both TLMII-561 and TMLII-664)
			/* need to read CLNT again by setting CNLT-CNLTNUM as CHDRLIF-COWNNUM (Owner), as CLNT-CLNTNUM had been */
			/* set to CHDRLIF-JOWNNUM to get joint owner earlier */
			readClnt();
			//TMLII-664 LT-01-002 END
			//TMLII-561
			currentClntExtraDetails();
			//TMLII-664 LT-01-002
			ownerClts400a();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		wsaaFirstTime = "Y";
		wsaaFirst = "Y";
		wsaaStrpos.set(1);
		/*EXIT*/
	}

protected void getChdrAndFormat200()
	{
		/*GET-DATA*/
		chdrlifIO.setChdrpfx("CH");
		chdrlifIO.setChdrcoy(letcIO.getRdoccoy());
		chdrlifIO.setChdrnum(letcIO.getRdocnum());
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrlifIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void getClntAndFormat300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para301();
					para302();
					formatField310();
				case prepareField315:
					prepareField315();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para301()
	{
		if (isEQ(chdrlifIO.getJownnum(),SPACES)
		&& isEQ(clntIO.getClntnum(),SPACES)) {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
			goTo(GotoLabel.prepareField315);
		}
	}

protected void para302()
	{
		clntIO.setClntpfx(chdrlifIO.getCownpfx());
		clntIO.setClntcoy(chdrlifIO.getCowncoy());
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
	}

protected void formatField310()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(pcpdatarec.fsuco);
		namadrsrec.clntNumber.set(clntIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void prepareField315()
	{
		wsaaFldlen.set(ZERO);
		wsaaFldlen.add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaOwnersNames.setSub1String(wsaaStrpos, wsaaFldlen, subString(namadrsrec.name, 1, wsaaFldlen));
		wsaaStrpos.add(wsaaFldlen);
		if (isEQ(wsaaFirst,"Y")) {
			if (isNE(chdrlifIO.getJownnum(),SPACES)) {
				wsaaOwnersNames.setSub1String(wsaaStrpos, 3, " & ");
				wsaaStrpos.add(3);
			}
			wsaaFirst = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(ZERO);
			fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
			if (isEQ(fldlen[offset.toInt()],ZERO)) {
				fldlen[offset.toInt()].set(1);
				wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
			}
		}
	}

protected void currentClntnum400()
	{
		/*CURR-CLNTNUM*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrlifIO.getCownnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrlifIO.getCownnum());
		/*EXIT*/
	}

//TMLII-664 LT-01-002 START (Required for both TLMII-561 and TMLII-664)
protected void readClnt(){
	clntIO.setDataArea(SPACES);
	clntIO.setClntnum(chdrlifIO.getCownnum());
	clntIO.setClntpfx(chdrlifIO.getCownpfx());
	clntIO.setClntcoy(chdrlifIO.getCowncoy());
	clntIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, clntIO);
	if (isNE(clntIO.getStatuz(),varcom.oK)) {
		syserrrec.statuz.set(clntIO.getStatuz());
		syserrrec.params.set(clntIO.getParams());
		fatalError600();
	}
}
//TMLII-664 LT-01-002 START

//TMLII-561 start
protected void currentClntExtraDetails() {
	formatclntDOB();
	getClntSexnID();
	getClntTaxnPhone();//ILIFE-3139
}

protected void formatclntDOB()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	datcon1rec.datcon1Rec.set(SPACES);
	if (isNE(clntIO.getCltdob(),0)
	&& isNE(clntIO.getCltdob(),99999999)) {
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(clntIO.getCltdob());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			return;
		}
	}
	else {
		datcon1rec.extDate.set(SPACES);
	}
	fldlen[offset.toInt()].set(length(datcon1rec.extDate));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
}

protected void getClntSexnID()
{
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	getClntGenderDesc();
	fldlen[offset.toInt()].add(inspectTally(wsaaLongdesc, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaLongdesc, 1, fldlen[offset.toInt()]));
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clntIO.getSecuityno(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getSecuityno(), 1, fldlen[offset.toInt()]));
	
	
}

//TMLII-1901 : Gender description in the Letter

protected void getClntGenderDesc()
{
	descIO.setParams(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(letcIO.getChdrcoy());
	descIO.setDesctabl(t5670);
	descIO.setDescitem(clntIO.getCltsex());
	descIO.setLanguage(pcpdatarec.language);
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
		wsaaLongdesc.fill("?");
	}
	wsaaLongdesc.set(descIO.getLongdesc());
}
//End
//ILIFE-3139-STARTS
protected void getClntTaxnPhone(){
	/*  Read the CLEX file to see if client exist.*/
	clexIO.setParams(SPACES);
	clexIO.setClntpfx(clntIO.getClntpfx());
	clexIO.setClntcoy(clntIO.getClntcoy());
	clexIO.setClntnum(clntIO.getClntnum());
	clexIO.setFunction(varcom.readr);
	clexIO.setFormat(clexrec);
	SmartFileCode.execute(appVars, clexIO);
	if (isNE(clexIO.getStatuz(), varcom.oK)
	&& isNE(clexIO.getStatuz(), varcom.mrnf)) {
		syserrrec.statuz.set(clexIO.getStatuz());
		syserrrec.params.set(clexIO.getParams());
		callProgram(Syserr.class, syserrrec.syserrRec);
		return ;
	}
	
//	offset.add(1);
//	if (isEQ(clexIO.getStatuz(), varcom.oK)) {
//		fldlen[offset.toInt()].set(ZERO);
//		fldlen[offset.toInt()].add(inspectTally(clexIO.getXrefno(), " ", "CHARACTERS", "    ", null));
//		if (isEQ(fldlen[offset.toInt()],ZERO)) {
//			fldlen[offset.toInt()].set(1);
//		}
//		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
//		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clexIO.getXrefno(), 1, fldlen[offset.toInt()]));
//	}
//	else {
//		fldlen[offset.toInt()].set(length(""));
//		if (isEQ(fldlen[offset.toInt()],ZERO)) {
//			fldlen[offset.toInt()].set(1);
//		}
//		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
//		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], "");
//	}


		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].add(inspectTally(clexIO.getRtaxidnum(), " ", "CHARACTERS", "   ", null));
		if (isEQ(clexIO.getStatuz(), varcom.mrnf)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
		else {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clexIO.getRtaxidnum());
	   }
		

	if(isNE(clntIO.getCltphone01(),SPACES)){
		offset.add(1);
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltphone01(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltphone01(), 1, fldlen[offset.toInt()]));

	}else{
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clntIO.getCltphone01(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltphone01(), 1, fldlen[offset.toInt()]));
	}
	

	//TMLII-1610 Phone Number should be "Home Phone number" and if Home Number is missing than it should be "Office Number"
	//Check if avilable getCltphone02()(Home) if not then set getCltphone01()
	
	if(isNE(clntIO.getCltphone02(),SPACES)){
		offset.add(1);
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltphone02(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltphone02(), 1, fldlen[offset.toInt()]));

	}else{
	offset.add(1);
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(clntIO.getCltphone01(), " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltphone01(), 1, fldlen[offset.toInt()]));
	}
	
}
//ILIFE-3139-ENDS
//TMLII-561 end

//TMLII-664 LT-01-002 START
	protected void ownerClts400a(){
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntpfx(chdrlifIO.getCownpfx());
		cltsIO.setClntcoy(chdrlifIO.getCowncoy());
		cltsIO.setClntnum(chdrlifIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if(isNE(cltsIO.getStatuz(), varcom.oK)){
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		/* Policy Owner Salutation */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		//descIO.setDesccoy(chdrlifIO.getChdrcoy());
		descIO.setDesccoy(chdrlifIO.getCowncoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setDesctabl(t3583);
		descIO.setDescitem(cltsIO.getSalutl());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		fldlen[offset.toInt()].set(ZERO);
		//check the length by 2 space
		fldlen[offset.toInt()].add(inspectTally(descIO.getShortdesc(), " ", "CHARACTERS", "  ", null)); 
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getShortdesc(), 1, fldlen[offset.toInt()]));
		
		/* Policy Owner First Name */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(cltsIO.getLgivname(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(cltsIO.getLgivname(), 1, fldlen[offset.toInt()]));
		
		/* Policy Owner Last Name */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(cltsIO.getLsurname(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(cltsIO.getLsurname(), 1, fldlen[offset.toInt()]));
		
	}
//TMLII-664 LT-01-002 END

protected void callDatcon1500()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
