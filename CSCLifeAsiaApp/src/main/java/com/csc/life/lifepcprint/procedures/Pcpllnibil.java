/*
 * File: Pcpllnibil.java
 * Date: 30 August 2009 1:02:12
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLNIBIL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.AcmvlonTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Loan Amount    -> LLOANAMT }
*  002  Loan Date From -> LLNDTEFR } in T2636
*  003  Loan Date To   -> LLNDTETO }
*  003  Loan No.       -> LLOANNO  }
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcpllnibil extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLLNIBIL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaOrigamt = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaLoanNo = new FixedLengthStringData(2).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaLoanType = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 3);
	private ZonedDecimalData wsaaToDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaOtherKeys, 4).setUnsigned();
	private ZonedDecimalData wsaaFromDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaOtherKeys, 12).setUnsigned();
	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (4, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* TABLES */
	private static final String t5645 = "T5645";
		/* FORMATS */
	private static final String acmvlonrec = "ACMVLONREC";
	private AcmvlonTableDAM acmvlonIO = new AcmvlonTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private T5645rec t5645rec = new T5645rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcpllnibil() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getLoanAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
		readT5645102();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
	}

protected void readT5645102()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRequestCompany());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(letcIO.getHsublet());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError900();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getLoanAndFormat200()
	{
		start210();
		format220();
		format230();
		format250();
	}

protected void start210()
	{
		if (isEQ(wsaaLoanType,"P")) {
			wsaaSacscode.set(t5645rec.sacscode01);
			wsaaSacstyp.set(t5645rec.sacstype01);
		}
		else {
			wsaaSacscode.set(t5645rec.sacscode02);
			wsaaSacstyp.set(t5645rec.sacstype02);
		}
		wsaaRldgacct.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(letcIO.getChdrnum());
		stringVariable1.addExpression(wsaaLoanNo);
		stringVariable1.setStringInto(wsaaRldgacct);
		acmvlonIO.setRecKeyData(SPACES);
		acmvlonIO.setRldgcoy(letcIO.getChdrcoy());
		acmvlonIO.setSacscode(wsaaSacscode);
		acmvlonIO.setSacstyp(wsaaSacstyp);
		acmvlonIO.setRldgacct(wsaaRldgacct);
		acmvlonIO.setEffdate(wsaaToDate);
		acmvlonIO.setFunction(varcom.readr);
		callAcmvlonio300();
	}

protected void format220()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		if (isEQ(acmvlonIO.getStatuz(),varcom.mrnf)) {
			acmvlonIO.setOrigamt("99999999999999999");
		}
		wsaaOrigamt.set(acmvlonIO.getOrigamt());
		fldlen[offset.toInt()].set(length(wsaaOrigamt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaOrigamt);
	}

protected void format230()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(wsaaFromDate);
		processDate400();
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(wsaaToDate);
		processDate400();
	}

protected void format250()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaLoanNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLoanNo);
		/*EXIT*/
	}

protected void callAcmvlonio300()
	{
		/*CALL*/
		acmvlonIO.setFormat(acmvlonrec);
		SmartFileCode.execute(appVars, acmvlonIO);
		if (isNE(acmvlonIO.getStatuz(),varcom.oK)
		&& isNE(acmvlonIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(acmvlonIO.getParams());
			syserrrec.statuz.set(acmvlonIO.getStatuz());
			fatalError900();
		}
		/*EXIT*/
	}

protected void processDate400()
	{
		call410();
	}

protected void call410()
	{
		datcon1rec.function.set(varcom.conv);
		callDatcon1500();
		datcon1rec.function.set(varcom.edit);
		callDatcon1500();
		initialize(datcon6rec.datcon6Rec);
		datcon6rec.intDate1.set(datcon1rec.intDate);
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getChdrcoy());
		datcon6rec.function.set(SPACES);
		callDatcon6600();
		fldlen[offset.toInt()].set(length(datcon6rec.intDate2));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon6rec.intDate2);
	}

protected void callDatcon1500()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			getAppVars().addDiagnostic("Bomb at \"DATCON1\" for date : "+datcon1rec.intDate);
			fatalError900();
		}
		/*EXIT*/
	}

protected void callDatcon6600()
	{
		/*CALL*/
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			pcpdatarec.statuz.set(varcom.bomb);
			getAppVars().addDiagnostic("Bomb at \"DATCON6\" for date : "+datcon1rec.intDate);
			fatalError900();
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
