/*
 * File: Pcplprd.java
 * Date: 14 January 2015 
 * Author: vchawda
 * 
 * Class Created for ILIFE-1074 
 * 
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;

import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Fund Name
*  002  Fund Amount / Percentage
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
*
***********************************************************************
* </pre>
*/
public class Pcplprd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPRD  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private PackedDecimalData wsaaSrcSub = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaFirstTime = "Y";
	private PackedDecimalData wsaaCounter = new PackedDecimalData(1, 0);
	/*  Make this field as large as you think the the data which
	  will be stored in it is ever likely to be. Calcualate
	  carefully as too small will result in truncation and too
	  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	
	/* WSAA-FNDS */
	private FixedLengthStringData[] wsaaSrcFunds = FLSInittedArray (10, 21);
	private FixedLengthStringData[] wsaaSrcFnd = FLSDArrayPartOfArrayStructure(4, wsaaSrcFunds, 0);
	private PackedDecimalData[] wsaaSrcAmt = PDArrayPartOfArrayStructure(9, 2, wsaaSrcFunds, 4, UNSIGNED_TRUE);
	
	/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaCurrUnitBal = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	
	/* The following array is configured to store up to 20 fields,
	 ensure you change it to store exactly how many fields are
	 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (30, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String ulnkrec = "ULNKREC";
	/* TABLES */
	private LetcTableDAM letcIO = new LetcTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplprd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray) {
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		main010();
		exit090();
	}

	protected void main010() {
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getData200();
			for (wsaaSrcSub.set(1);!(isGT(wsaaSrcSub,10)); wsaaSrcSub.add(1)){
				unlFnd210();	
				getUtrsAndFormat200();
			}
		}
		
		if (isEQ(pcpdatarec.fldOffset,1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

	protected void exit090() {
		exitProgram();
	}

	protected void initialise100() {
		/*PARA*/
		offset.set(0);
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		ulnkIO.setParams(SPACES);
		ulnkIO.setFunction(varcom.begn);
		wsaaSrcSub.set(ZERO);
		for (wsaaSrcSub.set(1); !(isGT(wsaaSrcSub,10)); wsaaSrcSub.add(1)){
			
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(SPACES);
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(ZERO);
			
		}
		wsaaSrcSub.set(ZERO);
		/*EXIT*/
	}

	protected void getUtrsAndFormat200() {
		try {

			format205();

		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void getData200() {
		ulnkIO.setStatuz(varcom.oK);
		ulnkIO.setChdrcoy(letcIO.getRdoccoy());
		ulnkIO.setChdrnum(letcIO.getRdocnum());
		ulnkIO.setPlanSuffix(ZERO);
		ulnkIO.setTranno(letcIO.getTranno());
		
		ulnkIO.setFormat(ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(),varcom.oK)
		&& isNE(ulnkIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(ulnkIO.getParams());
			fatalError600();
		}
				
		if (isNE(ulnkIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		
		wsaaCounter.add(1);
	}

	protected void format205() {
		/*  Fund Name.*/
		if (isEQ(offset,0)) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
		} else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
			
		fldlen[offset.toInt()].set(length(wsaaSrcFnd[wsaaSrcSub.toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSrcFnd[wsaaSrcSub.toInt()]);
		
		//Fund Amount 
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaCurrUnitBal.set(wsaaSrcAmt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaCurrUnitBal));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrUnitBal);
		
		//Fund Split Plan
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(ulnkIO.getPercOrAmntInd()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], ulnkIO.getPercOrAmntInd());
	}


	protected void unlFnd210() {
		wsaaSrcFnd[wsaaSrcSub.toInt()].set(ulnkIO.getUalfnd(wsaaSrcSub));
		wsaaSrcAmt[wsaaSrcSub.toInt()].set(ulnkIO.getUalprc(wsaaSrcSub));
	}


	protected void fatalError600() {
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES) 
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		} else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
