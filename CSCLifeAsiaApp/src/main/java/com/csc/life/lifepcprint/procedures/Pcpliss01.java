/*
 * File: Pcpliss01.java
 * Date: 30 August 2009 1:01:53
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLISS01.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcprulerec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* This is a PCP400 Issuance Rules Subroutine.
* It can be used as a prototype for more complex Subroutines.
*
* Program Description.
*
* This is a simple subroutine that checks whether the policy
* that we are about to print is actually In Force.
*
***********************************************************************
* </pre>
*/
public class Pcpliss01 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLISS01 ";
		/* ERRORS */
	private String f761 = "F761";
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Pcprulerec pcprulerec = new Pcprulerec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	public Pcpliss01() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcprulerec.pcprulesRec = convertAndSetParam(pcprulerec.pcprulesRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		initialise1000();
		if (isEQ(pcprulerec.function,"CHECK")){
			getChdrAndCheck2000();
		}
		/*EXIT*/
		exitProgram();
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcprulerec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*PARA*/
		chdrIO.setParams(SPACES);
		/*EXIT*/
	}

protected void getChdrAndCheck2000()
	{
		para2010();
	}

protected void para2010()
	{
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(letcIO.getLetterRequestDate());
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.oK)
		&& (isNE(chdrIO.getChdrpfx(),letcIO.getRdocpfx())
		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum()))) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatcode(),"IF")) {
			pcprulerec.statuz.set(varcom.oK);
		}
		else {
			pcprulerec.statuz.set(f761);
		}
	}
}
