/*
 * File: Pcplaglf.java
 * Date: 30 August 2009 1:00:10
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLAGLF.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpscrlrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  Overview.
*  ---------
*
*  This is a P.C.Printing/400 "scroll" Subroutine. Its purpose is
*  to return LIFE/400 Agents.
*
*  Processing.
*  -----------
*
*  LETC Fields Required: Completed U.T.I.
*
*  On a change of LETC RRN (a new letter request) read through the
*  AGLF file loading each active agent into a working storage table.
*  (We do this because other data extract subroutines may upset our
*  logical file pointer if we use begn/nextr logic.)
*  Setup the LETC Other Keys area using the LETCOKCPY copybook and
*  return the first agent number.
*
*  On subsequent calls, add 1 to the pointer and return the next
*  agent until all agents have been returned.
*
***********************************************************************
* </pre>
*/
public class Pcplaglf extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaIdCode = new FixedLengthStringData(8);
	private BinaryData wsaaStoreRrn = new BinaryData(9, 0).init(ZERO);
	private PackedDecimalData ixMax = new PackedDecimalData(3, 0);
	private PackedDecimalData ixAgt = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaAgtArray = new FixedLengthStringData(4000);
	private FixedLengthStringData[] wsaaAgtItem = FLSArrayPartOfStructure(500, 8, wsaaAgtArray, 0);
	private FixedLengthStringData[] wsaaAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaAgtItem, 0);
	private String aglfrec = "AGLFREC";
		/* ERRORS */
	private String e005 = "E005";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpscrlrec pcpscrlrec = new Pcpscrlrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit209, 
		exit809
	}

	public Pcplaglf() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpscrlrec.pcpscrlRec = convertAndSetParam(pcpscrlrec.pcpscrlRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*PARA*/
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			while ( !(isEQ(aglfIO.getStatuz(),varcom.endp))) {
				getAgent200();
			}
			
		}
		returnCode800();
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaIdCode.set(pcpscrlrec.idcode);
		wsaaStoreRrn.set(letcIO.getRrn());
		ixAgt.set(ZERO);
		ixMax.set(ZERO);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		if (!letcokcpy.lifeAgentStatement.isTrue()) {
			syserrrec.statuz.set(e005);
			fatalError900();
		}
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(letcIO.getRequestCompany());
		aglfIO.setAgntnum(SPACES);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		aglfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		aglfIO.setFitKeysSearch("AGNTCOY");
	}

protected void getAgent200()
	{
		try {
			para201();
		}
		catch (GOTOException e){
		}
	}

protected void para201()
	{
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(aglfIO.getParams());
			fatalError900();
		}
		if (isEQ(aglfIO.getStatuz(),varcom.endp)
		|| isNE(aglfIO.getAgntcoy(),letcIO.getRequestCompany())) {
			aglfIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit209);
		}
		ixMax.add(1);
		wsaaAgntnum[ixMax.toInt()].set(aglfIO.getAgntnum());
		aglfIO.setFunction(varcom.nextr);
	}

protected void returnCode800()
	{
		try {
			start801();
		}
		catch (GOTOException e){
		}
	}

protected void start801()
	{
		ixAgt.add(1);
		if (isGT(ixAgt,ixMax)) {
			pcpscrlrec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit809);
		}
		pcpscrlrec.statuz.set(varcom.oK);
		letcokcpy.asAgentno.set(wsaaAgntnum[ixAgt.toInt()]);
		letcIO.setOtherKeys(letcokcpy.saveOtherKeys);
		wsaaIdCode.set("LAGST");
		pcpscrlrec.idcode.set(wsaaIdCode);
	}

protected void fatalError900()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
