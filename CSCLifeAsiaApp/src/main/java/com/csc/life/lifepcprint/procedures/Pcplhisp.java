/*
 * File: Pcplhisp.java
 * Date: 30 August 2009 1:01:39
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLHISP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.interestbearing.dataaccess.ThisptrTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Previous Interest Suppress From -> LPINTSFR }
*  002  Previous Interest Suppress To   -> LPINTSTO } in T2636
*  003  Current Interest Suppress From  -> LCINTSFR }
*  004  Current Interest Suppress To    -> LCINTSTO }
*
*
***********************************************************************
* </pre>
*/
public class Pcplhisp extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLHISP";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
		/* The following array is configured to store up to 2 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (4, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String thisptrrec = "THISPTRREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private ThisptrTableDAM thisptrIO = new ThisptrTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		format240, 
		exit299
	}

	public Pcplhisp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getPreviousInterest200();
			getCurrentInterest300();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getPreviousInterest200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start210();
					format220();
					format230();
				case format240: 
					format240();
				case exit299: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start210()
	{
		thisptrIO.setParams(SPACES);
		thisptrIO.setStatuz(varcom.oK);
		thisptrIO.setValidflag("2");
		thisptrIO.setChdrcoy(letcIO.getRdoccoy());
		thisptrIO.setChdrnum(letcIO.getChdrnum());
		thisptrIO.setTranno(letcIO.getTranno());
		thisptrIO.setFunction(varcom.endr);
		thisptrIO.setFormat(thisptrrec);
		SmartFileCode.execute(appVars, thisptrIO);
		if (isNE(thisptrIO.getStatuz(),varcom.oK)
		&& isNE(thisptrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(thisptrIO.getParams());
			fatalError600();
		}
		if (isEQ(thisptrIO.getStatuz(),varcom.endp)
		|| isNE(thisptrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(thisptrIO.getChdrnum(),letcIO.getRdocnum())) {
			goTo(GotoLabel.format240);
		}
	}

protected void format220()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(thisptrIO.getHintsupfrm());
		processDate400();
	}

protected void format230()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(thisptrIO.getHintsupto());
		processDate400();
		goTo(GotoLabel.exit299);
	}

protected void format240()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
	}

protected void getCurrentInterest300()
	{
		start300();
		format310();
	}

protected void start300()
	{
		thisptrIO.setParams(SPACES);
		thisptrIO.setStatuz(varcom.oK);
		thisptrIO.setValidflag("1");
		thisptrIO.setChdrcoy(letcIO.getRdoccoy());
		thisptrIO.setChdrnum(letcIO.getRdocnum());
		thisptrIO.setTranno(letcIO.getTranno());
		thisptrIO.setFunction(varcom.readr);
		thisptrIO.setFormat(thisptrrec);
		SmartFileCode.execute(appVars, thisptrIO);
		if (isNE(thisptrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(thisptrIO.getParams());
			fatalError600();
		}
	}

protected void format310()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(thisptrIO.getHintsupfrm());
		processDate400();
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(thisptrIO.getHintsupto());
		processDate400();
		/*EXIT*/
	}

protected void processDate400()
	{
		/*CALL*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
