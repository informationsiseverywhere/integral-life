/*
 * File: Pcplsbnfy1.java
 * Date: 30 August 2009 1:02:58
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLSBNFY1.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpscrlrec;
import com.csc.life.enquiries.dataaccess.BnfyenqTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  Overview.
*  ---------
*
*  This is a P.C.Printing/400 "scroll" Subroutine. Its purpose is
*  to return LIFE/400 information on lives per contract. It may
*  be used as a genuine "scroll" subroutine or cloned. The order
*  that the lives are returned is life number order.
*
*  Processing.
*  -----------
*
*  LETC Fields Required: Completed RDOCNUM
*
*  On a change of LETC RRN (a new letter request) read through the
*  the BNFY file loading each active Life into a working
*  storage table. (We do this because other data extract subroutines
*  may upset our logical file pointer if we use begn/nextr logic.)
*  Setup the LETC Other Keys area using the LETCOKCPY copybook and
*  return the first life.
*
*  On subsequent calls, add 1 to the pointer and return the next
*  life until all lives have been returned.
*
***********************************************************************
* </pre>
*/
public class Pcplsbnfy1 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaIdCode = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMainCode = new FixedLengthStringData(4).isAPartOf(wsaaIdCode, 0);
	private FixedLengthStringData wsaaSubCode = new FixedLengthStringData(4).isAPartOf(wsaaIdCode, 4);
	private BinaryData wsaaStoreRrn = new BinaryData(9, 0).init(ZERO);
	private PackedDecimalData ixMax = new PackedDecimalData(3, 0);
	private PackedDecimalData ixBnfy = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaBnfyArray = new FixedLengthStringData(400);
	private FixedLengthStringData[] wsaaBnyclt = FLSArrayPartOfStructure(50, 8, wsaaBnfyArray, 0);
	private String bnfyenqrec = "BNFYENQREC";
		/*Contract Beneficiaries - Contract Enquir*/
	private BnfyenqTableDAM bnfyenqIO = new BnfyenqTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Pcpscrlrec pcpscrlrec = new Pcpscrlrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit709
	}

	public Pcplsbnfy1() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpscrlrec.pcpscrlRec = convertAndSetParam(pcpscrlrec.pcpscrlRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*PARA*/
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			callBnfyenqio250();
			bnfyenqIO.setFunction(varcom.nextr);
			while ( !(isEQ(bnfyenqIO.getStatuz(),varcom.endp))) {
				storeBnfyDetails300();
				callBnfyenqio250();
			}

		}
		returnCode700();
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaIdCode.set(pcpscrlrec.idcode);
		wsaaStoreRrn.set(letcIO.getRrn());
		ixBnfy.set(ZERO);
		ixMax.set(ZERO);
		bnfyenqIO.setParams(SPACES);
		bnfyenqIO.setChdrcoy(letcIO.getChdrcoy());
		bnfyenqIO.setChdrnum(letcIO.getChdrnum());
		bnfyenqIO.setBnyclt(SPACES);
		bnfyenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bnfyenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfyenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		bnfyenqIO.setStatuz(varcom.oK);
		/*EXIT*/
	}

protected void callBnfyenqio250()
	{
		/*CALL*/
		bnfyenqIO.setFormat(bnfyenqrec);
		SmartFileCode.execute(appVars, bnfyenqIO);
		if (isNE(bnfyenqIO.getStatuz(),varcom.oK)
		&& isNE(bnfyenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(bnfyenqIO.getParams());
			fatalError900();
		}
		if (isEQ(bnfyenqIO.getStatuz(),varcom.endp)
		|| isNE(bnfyenqIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(bnfyenqIO.getChdrnum(),letcIO.getChdrnum())) {
			bnfyenqIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void storeBnfyDetails300()
	{
		/*GET-DATA*/
		ixMax.add(1);
		/*EXIT*/
	}

protected void returnCode700()
	{
		try {
			start701();
		}
		catch (GOTOException e){
		}
	}

protected void start701()
	{
		ixBnfy.add(1);
		if (isGT(ixBnfy,ixMax)) {
			pcpscrlrec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit709);
		}
		pcpscrlrec.statuz.set(varcom.oK);
		pcpscrlrec.idcode.set(wsaaIdCode);
		pcpscrlrec.newidIndicator.set("N");
	}

protected void fatalError900()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
