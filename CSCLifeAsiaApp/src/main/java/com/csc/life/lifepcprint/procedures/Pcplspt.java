/*
 * File: Pcplspt.java
 * Date: 23 Januray 2015 
 * Author: vchawda
 * 
 * 
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.enquiries.dataaccess.AcmvsacTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Source Fund Name
*  002  Source Fund Amount
*  003  Non - Invested Amount
*  004  Total Amount
*  005  Tax
*    
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include Total Premium which is not an occurance field.
*
*
*
*
***********************************************************************
* </pre>
*/
public class Pcplspt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLSPT";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(5).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaEffDate = new PackedDecimalData(8, 0);
	private String wsaaFeeFnd = "";
	private PackedDecimalData wsaaFundFee = new PackedDecimalData(9, 2).setUnsigned();
	private PackedDecimalData wsaaSrcSub = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaSacstyp = new FixedLengthStringData(2);

		/* WSAA-SRC-FNDS */
	private FixedLengthStringData[] wsaaSrcFunds = FLSInittedArray (10, 21);
	private FixedLengthStringData[] wsaaSrcFnd = FLSDArrayPartOfArrayStructure(4, wsaaSrcFunds, 0);
	private PackedDecimalData[] wsaaSrcAmt = PDArrayPartOfArrayStructure(9, 2, wsaaSrcFunds, 4, UNSIGNED_TRUE);

		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaFundAmt = new ZonedDecimalData(16, 2).setPattern("ZZ,ZZZ,ZZZ,ZZZ,ZZ9.99");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (82, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String utrnrevrec = "UTRNREVREC";
		/* TABLES */

	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Getdescrec getdescrec = new Getdescrec();

	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplspt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getDetails200();
			for (wsaaSrcSub.set(1); !(isEQ(wsaaSrcFnd[wsaaSrcSub.toInt()],SPACES)); wsaaSrcSub.add(1)){
				formatSourceFund410();
				format402();
				format416();	
			}
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaCounter.set(ZERO);
		wsaaSrcSub.set(ZERO);
		wsaaFundFee.set(ZERO);
		wsaaFundAmt.set(ZERO);
		for (wsaaSrcSub.set(1); !(isGT(wsaaSrcSub,10)); wsaaSrcSub.add(1)){
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(SPACES);
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(ZERO);
		}
		wsaaSrcSub.set(ZERO);
		/*EXIT*/
	}

protected void getDetails200()
	{
		getData200();
		fund230();
	}

protected void getData200()
	{
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.oK)
		&& (isNE(chdrIO.getChdrpfx(),"CH")
		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum()))) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
	}


protected void fund230()
	{
		utrnrevIO.setParams(SPACES);
		utrnrevIO.setChdrcoy(letcIO.getRdoccoy());
		utrnrevIO.setChdrnum(letcIO.getRdocnum());
		utrnrevIO.setTranno(letcIO.getTranno());
		utrnrevIO.setFeedbackInd("Y");
		utrnrevIO.setFormat(utrnrevrec);
		utrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		utrnrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "TRANNO");
		readUtrnrev320();
		while ( !(isEQ(utrnrevIO.getStatuz(),varcom.endp)
		|| isNE(utrnrevIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(utrnrevIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(utrnrevIO.getTranno(),letcIO.getTranno()))) {
			if(isEQ(utrnrevIO.getUnitSubAccount(),"ACUM")){
				unlFndDets330();
			}
			if(isEQ(utrnrevIO.getUnitSubAccount(),"NVST")){
				wsaaFundFee.add(utrnrevIO.getContractAmount());
			}
			
			utrnrevIO.setFunction(varcom.nextr);
			readUtrnrev320();
		}
		
	}

protected void readHitrrev300()
	{
		/*HITRREV*/
		SmartFileCode.execute(appVars, hitrrevIO);
		if (isNE(hitrrevIO.getStatuz(),varcom.oK)
		&& isNE(hitrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hitrrevIO.getParams());
			syserrrec.statuz.set(hitrrevIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}


protected void readUtrnrev320()
	{
		/*UTRNREV*/
		SmartFileCode.execute(appVars, utrnrevIO);
		if (isNE(utrnrevIO.getStatuz(),varcom.oK)
		&& isNE(utrnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrnrevIO.getParams());
			syserrrec.statuz.set(utrnrevIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void unlFndDets330()
	{
		unlFnd330();
	}

protected void unlFnd330()
	{

			wsaaSrcSub.add(1);
			wsaaSrcFnd[wsaaSrcSub.toInt()].set(utrnrevIO.getUnitVirtualFund());
			wsaaSrcAmt[wsaaSrcSub.toInt()].set(utrnrevIO.getContractAmount());
			wsaaEffDate.set(utrnrevIO.getMoniesDate());
	}

protected void format402()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundAmt.set(wsaaFundFee);
		fldlen[offset.toInt()].set(length(wsaaFundAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundAmt);
		/*EXIT*/
	}

protected void formatSourceFund410()
	{
		start410();
		format412();
	}

protected void start410()
	{
		wsaaCounter.add(1);
		/* Fund Name*/
		if (isEQ(offset,0)) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		
		fldlen[offset.toInt()].set(length(wsaaSrcFnd[wsaaSrcSub.toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(wsaaSrcFnd[wsaaSrcSub.toInt()], 1, fldlen[offset.toInt()]));
	}

protected void format412()
	{
		// Fund Amt
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaFundAmt.set(wsaaSrcAmt[wsaaSrcSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaFundAmt));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFundAmt);
	}


protected void format416()
	{
	
	taxdIO.setChdrcoy(utrnrevIO.getChdrcoy());
	taxdIO.setChdrnum(utrnrevIO.getChdrnum());
	taxdIO.setLife(utrnrevIO.getLife());
	taxdIO.setCoverage(utrnrevIO.getCoverage());
	taxdIO.setRider(utrnrevIO.getRider());
	taxdIO.setPlansfx(utrnrevIO.getPlanSuffix());
	taxdIO.setInstfrom(utrnrevIO.getCrComDate());
	taxdIO.setTrantype("PREM");
	taxdIO.setFunction(varcom.readh);

	SmartFileCode.execute(appVars, taxdIO);
	if (isNE(taxdIO.getStatuz(),varcom.oK)
	&& isNE(taxdIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(taxdIO.getParams());
		syserrrec.statuz.set(taxdIO.getStatuz());
		fatalError600();
	}
	
		// Total Amount
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(taxdIO.getBaseamt()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], taxdIO.getBaseamt());
		/*Tax Amount*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(add(taxdIO.getTaxamt01(),taxdIO.getTaxamt02(),taxdIO.getTaxamt03())));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], add(taxdIO.getTaxamt01(),taxdIO.getTaxamt02(),taxdIO.getTaxamt03()));
	}


protected void getdesc500()
	{
		/*GET*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(pcpdatarec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.fill("?");
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
