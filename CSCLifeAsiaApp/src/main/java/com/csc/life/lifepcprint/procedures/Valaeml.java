package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.recordstructures.Hvalletrec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.tablestructures.Thsuzrec;

/**
 * Validation subroutine for agent email sending. This subroutine checks if
 * email address exist for agent.
 * 
 * @author vhukumagrawa
 */
public class Valaeml extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Valaeml.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Hvalletrec hvalletrec = new Hvalletrec();
	private Thsuzrec thsuzrec = new Thsuzrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Clexpf clexpf = new Clexpf();
	private Chdrpf chdr = new Chdrpf();
	private Agntpf agntpf = new Agntpf();
	private static final String thsuz = "THSUZ"; 
	

	public Valaeml() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		hvalletrec.params = convertAndSetParam(hvalletrec.params, parmArray, 0);
		try {
			main000();
		} catch (COBOLExitProgramException e) {
			// Exit program
		}
	}

	protected void main000() {
		letcokcpy.saveOtherKeys.set(hvalletrec.hvalOtherKeys);

		chdr = chdrpfDAO.getchdrRecord(hvalletrec.hvalChdrcoy.trim(), hvalletrec.hvalChdrnum.trim());
		agntpf = agntpfDAO.getClientData("AG", hvalletrec.hvalChdrcoy.trim(), chdr.getAgntnum().trim());
		clexpf = clexpfDAO.getClexpfByClntkey("CN", "9", agntpf.getClntnum().trim());

		Itempf itempf = itemDAO.findItemByItem("2",thsuz,hvalletrec.hvalTrcde.toString());
		if(itempf!=null) {
			thsuzrec.thsuzRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}else {
			return;
		}
		
		hvalletrec.hvalStatuz.set(Varcom.mrnf);
		if (clexpf != null && clexpf.getRinternet() != null && !"".equals(clexpf.getRinternet().trim()) && (isEQ(thsuzrec.flag, "2") || isEQ(thsuzrec.flag, "4"))){
			hvalletrec.hvalStatuz.set(Varcom.oK);
		}

		LOGGER.info("==Valaeml subroutine exit status==== {}", hvalletrec.hvalStatuz);
		exitProgram();
	}

}
