/*
 * File: Pcplfund.java
 * Date: 30 August 2009 1:01:30
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLFUND.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Fund Description
*  002  Unit Type
*  003  Unit Current Balance
*  004  Deemed Unit Current Balance
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplfund extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLFUND  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(4).setUnsigned();
	private String wsaaFirstTime = "Y";
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaCurrUnitBal = new ZonedDecimalData(16, 5).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99999");
	private ZonedDecimalData wsaaCurrDunitBal = new ZonedDecimalData(16, 5).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99999");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String utrsrec = "UTRSREC";
		/* TABLES */
	private static final String t5540 = "T5540";
	private static final String t6649 = "T6649";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplfund() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			while ( !(isEQ(utrsIO.getStatuz(),varcom.endp))) {
				getUtrsAndFormat200();
			}
			
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		utrsIO.setParams(SPACES);
		utrsIO.setFunction(varcom.begn);
		/*EXIT*/
	}

protected void getUtrsAndFormat200()
	{
		try {
			getData200();
			format205();
			format210();
			format215();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		utrsIO.setStatuz(varcom.oK);
		utrsIO.setChdrcoy(letcIO.getRdoccoy());
		utrsIO.setChdrnum(letcIO.getRdocnum());
		utrsIO.setLife(letcokcpy.luLife);
		utrsIO.setCoverage(letcokcpy.luCoverage);
		utrsIO.setRider(letcokcpy.luRider);
		utrsIO.setPlanSuffix(ZERO);
		utrsIO.setUnitVirtualFund(SPACES);
		utrsIO.setUnitType(SPACES);
		utrsIO.setFormat(utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(),varcom.oK)
		&& isNE(utrsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(utrsIO.getParams());
			fatalError600();
		}
		utrsIO.setFunction(varcom.nextr);
		if (isEQ(utrsIO.getStatuz(),varcom.endp)
		|| isNE(utrsIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(utrsIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(utrsIO.getLife(),letcokcpy.luLife)
		|| isNE(utrsIO.getCoverage(),letcokcpy.luCoverage)) {
			utrsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		/*  The following counter is incremented by 1 per each*/
		/*  successful read of UTRS.  It is used to stop PCP400*/
		/*  retrieving recurring fields when there are no more*/
		/*  records.*/
		wsaaCounter.add(1);
	}

protected void format205()
	{
		/*  Fund Description.*/
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		itemIO.setItemtabl(t5540);
		itemIO.setItemitem(utrsIO.getUnitVirtualFund());
		getdesc800();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(getdescrec.longdesc, " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(getdescrec.longdesc, 1, fldlen[offset.toInt()]));
	}

protected void format210()
	{
		/*   Unit Type*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		itemIO.setItemtabl(t6649);
		itemIO.setItemitem(utrsIO.getUnitType());
		getdesc800();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(getdescrec.longdesc, " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(getdescrec.longdesc, 1, fldlen[offset.toInt()]));
	}

protected void format215()
	{
		/*   Unit Current Balance*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaCurrUnitBal.set(utrsIO.getCurrentUnitBal());
		fldlen[offset.toInt()].set(length(wsaaCurrUnitBal));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrUnitBal);
		/*FORMAT*/
		/*   Deemed Unit Current Balance*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaCurrDunitBal.set(utrsIO.getCurrentDunitBal());
		fldlen[offset.toInt()].set(length(wsaaCurrDunitBal));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrDunitBal);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void getdesc800()
	{
		/*GETDESC*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(pcpdatarec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.fill("?");
		}
		/*EXIT*/
	}
}
