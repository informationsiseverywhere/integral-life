/*
 * File: Pcplmath.java
 * Date: 30 August 2009 1:02:21
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLMATH.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.terminationclaims.dataaccess.MathclmTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Effective Date              -> LMHEFDAT    }
*  002  Currency Code               -> LMHCURCD    }
*  003  Policy Loan                 -> LMHPLOAN    }
*  004  Policy Debt Amount          -> LMHDBAMT    }
*  005  Other Adjustment on Claim   -> LMHOTADJ    }
*  006  Reason Code                 -> LMHRSNCD    }
*  007  Reason Description          -> LMHRSNDC    }
*  008  Estimate Total Claim Value  -> LMHETCLM    }
*  009  Claim Amount                -> LMHCLAMT    }
*  010  Cash Amount                 -> LMHCASH     }
*  011  Plan Suffix                 -> LMHPLSFX    }
*
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplmath extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLMATH  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAmountX = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99-");

	private FixedLengthStringData wsaaOkey = new FixedLengthStringData(29);
	private PackedDecimalData wsaaOkey1 = new PackedDecimalData(4, 0).isAPartOf(wsaaOkey, 0);
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(500).init(SPACES);
		/* The following array is configured to store up to 3 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (10, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String mathclmrec = "MATHCLMREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private MathclmTableDAM mathclmIO = new MathclmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplmath() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getDetails200();
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.*/
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/*EXIT*/
	}

protected void getDetails200()
	{
		try {
			start201();
			format202();
			format203();
			format205();
			format206();
			format208();
			format210();
			format211();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start201()
	{
		mathclmIO.setParams(SPACES);
		mathclmIO.setStatuz(varcom.oK);
		mathclmIO.setChdrcoy(letcIO.getChdrcoy());
		mathclmIO.setChdrnum(letcIO.getChdrnum());
		mathclmIO.setTranno(letcIO.getTranno());
		/*
		 * wsaaOkey.set(letcIO.getOtherKeys()); if (isEQ(wsaaOkey,SPACES)) {
		 * wsaaOkey1.set(ZERO); }
		 */
		mathclmIO.setPlanSuffix(wsaaOkey1);
		mathclmIO.setFunction(varcom.readr);
		callMathclmio300();
	}

protected void format202()
	{
		/*    Field 01 - Effective Date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(mathclmIO.getEffdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format203()
	{
		/*    Field 02 - Currency Code*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(mathclmIO.getCurrcd()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], mathclmIO.getCurrcd());
		/*FORMAT*/
		/*    Field 03 - Policy Loan*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getPolicyloan());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format205()
	{
		/*    Field 04 - Policy Debt Amount*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getTdbtamt());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format206()
	{
		/*    Field 05 - Other Adjustment on Claim*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getOtheradjst());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
		/*FORMAT*/
		/*    Field 06 - Reason Code*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(mathclmIO.getReasoncd()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], mathclmIO.getReasoncd());
	}

protected void format208()
	{
		/*    Field 07 - Reason Description*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(mathclmIO.getResndesc()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], mathclmIO.getResndesc());
		/*FORMAT*/
		/*    Field 08 - Estimate Total Claim Value*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getEstimateTotalValue());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format210()
	{
		/*    Field 09 - Claim Amount*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getClamamt());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format211()
	{
		/*    Field 10 - Plan Suffix*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(mathclmIO.getPlanSuffix());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
		if (isEQ(mathclmIO.getPlanSuffix(),ZERO)) {
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
		}
	}

protected void callMathclmio300()
	{
		/*CALL*/
		mathclmIO.setFormat(mathclmrec);
		SmartFileCode.execute(appVars, mathclmIO);
		if (isNE(mathclmIO.getStatuz(),varcom.oK)) {
			if(isEQ(mathclmIO.getStatuz(),varcom.mrnf)) {
				exitProgram();
			}
			syserrrec.params.set(mathclmIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
