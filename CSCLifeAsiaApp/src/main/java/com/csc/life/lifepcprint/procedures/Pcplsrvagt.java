/*
 * File: Pcplsrvagt.java
 * Date: 30 August 2009 1:02:59
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLSRVAGT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Agent Number
*   02    Agent Name
*   03    Agent Type
*   04    Agent Type Description
*   05    License Number
*   06	  Agent Branch Description
*   07    Agent Branch Area Description
*   08    Agent Sales Unit Description
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplsrvagt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLSRVAGT";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(144);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(24, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	//TMLII-664 LT-01-002 START
	private FixedLengthStringData wsaaT5628Item = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaAgntbr = new FixedLengthStringData(2).isAPartOf(wsaaT5628Item, 0);
	private FixedLengthStringData wsaaAracde = new FixedLengthStringData(3).isAPartOf(wsaaT5628Item, 2);
	//TMLII-664 LT-01-002 END
		/* FORMATS */
	private static final String aglfrec = "AGLFREC";
	private static final String agntrec = "AGNTREC";
		/* TABLES */
	private String t3692 = "T1692";
	private String t1692 = "T1692";
	private String t5628 = "T5628";
	private String tt518 = "TT518";
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit399
	}

	public Pcplsrvagt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getChdr200();
			getAgentAndFormat300();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		/*EXIT*/
	}

protected void getChdr200()
	{
		getData200();
	}

protected void getData200()
	{
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setValidflag("1");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)
		|| isNE(chdrIO.getChdrpfx(),"CH")
		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum())) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
	}

protected void getAgentAndFormat300()
	{
		try {
			getData300();
			formatAgntnum301();
			formatAgtName302();
			formatAgtype303();
			formatAgtypeDesc304();
			formatLicno305();
			//TMLII-664 LT-01-002 START
			formatBranchDesc306();
			formatBranchArea307();
			formatSalesUnit308();
			//TMLII-664 LT-01-002 START
		}
		catch (GOTOException e){
		}
	}

protected void getData300()
	{
		agntIO.setRecKeyData(SPACES);
		agntIO.setAgntpfx(chdrIO.getAgntpfx());
		agntIO.setAgntcoy(chdrIO.getAgntcoy());
		agntIO.setAgntnum(chdrIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		agntIO.setFormat(agntrec);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)
		&& isNE(agntIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			goTo(GotoLabel.exit399);
		}
	}

protected void formatAgntnum301()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(agntIO.getAgntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], agntIO.getAgntnum());
	}

protected void formatAgtName302()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set(agntIO.getClntpfx());
		namadrsrec.clntCompany.set(agntIO.getClntcoy());
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("LGNMF");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set("NAMADRS");
			fatalError600();
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatAgtype303()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(agntIO.getAgtype()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], agntIO.getAgtype());
	}

protected void formatAgtypeDesc304()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3692);
		descIO.setDescitem(agntIO.getAgtype());
		callDesc400();
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatLicno305()
	{
		aglfIO.setRecKeyData(SPACES);
		aglfIO.setAgntcoy(chdrIO.getAgntcoy());
		aglfIO.setAgntnum(chdrIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		aglfIO.setFormat(aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(aglfIO.getTlaglicno()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], aglfIO.getTlaglicno());
	}

	//TMLII-664 LT-01-002 START
	protected void formatBranchDesc306(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(agntIO.getAgntbr());
		callDesc400();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}
	protected void formatBranchArea307(){
		// set value for wsaaT5628Item
		wsaaAgntbr.set(agntIO.getAgntbr());
		wsaaAracde.set(aglfIO.getAracde());
		
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5628);
		descIO.setDescitem(wsaaT5628Item);
		callDesc400();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}
	protected void formatSalesUnit308(){
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		descIO.setParams(SPACES);
		descIO.setDesctabl(tt518);
		descIO.setDescitem(aglfIO.getTsalesunt());
		callDesc400();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}
	//TMLII-664 LT-01-002 END



protected void callDesc400()
	{
		/*START*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrIO.getChdrcoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
