package com.csc.life.lifepcprint.procedures;

	import com.quipoz.framework.datatype.*;
	import com.quipoz.framework.exception.ServerException;
	import com.quipoz.framework.exception.TransferProgramException;
	import com.quipoz.framework.screenmodel.ScreenModel;
	import com.quipoz.framework.sessionfacade.JTAWrapper;
	import com.quipoz.framework.util.*;
	import com.quipoz.COBOLFramework.util.*;
	import static com.quipoz.COBOLFramework.COBOLFunctions.*;
	import static com.quipoz.COBOLFramework.util.CLFunctions.*;
	import com.csc.smart400framework.dataaccess.SmartFileCode;
	import com.csc.smart400framework.utility.ExitSection;	
	import com.quipoz.framework.util.log.QPLogger;
	import com.csc.fsu.general.dataaccess.LetcTableDAM;
	import com.csc.smart.dataaccess.DescTableDAM;
	//WS -003 Changes Start ..Phani
	import com.csc.life.terminationclaims.dataaccess.PtshclmTableDAM;
	import com.csc.life.terminationclaims.dataaccess.SurhclmTableDAM;
	import com.csc.life.terminationclaims.dataaccess.ClmhclmTableDAM;
	//WS -003 Changes End..Phani
	import com.csc.smart.recordstructures.Datcon1rec;
	import com.csc.fsu.printing.recordstructures.Pcpdatarec;
	import com.csc.smart.recordstructures.Syserrrec;
	import com.csc.smart.recordstructures.Varcom;

	import com.csc.smart400framework.utility.Datcon1;
	
	import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
	import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
	import com.csc.smart400framework.procedures.Syserr;


	import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;

	/**
	* <pre>
	* Description: (2-3 lines)
	*
	* Offset. Description...............................................
	*
	*01 Offset Effective Date
	*02 Offset Underwriting Date
	*03 Offset Alteration Type

	**/


	public class Pcplbenclm extends COBOLConvCodeModel {
		public static final String ROUTINE = QPUtilities.getThisClass();
		public int numberOfParameters = 0;
		private String wsaaSubrname = "PCPLBENCLM";
		private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
		//TMLI WS-003 Additions. Start
		private FixedLengthStringData wsaaTransCde = new FixedLengthStringData(10);//Code to get the description to set at Offset 1
		private FixedLengthStringData wsaaLifeBenefitApplnDate = new FixedLengthStringData(10);
		private FixedLengthStringData wsaaLifeBenefitDocRcvdDate = new FixedLengthStringData(10);
		//TMLI WS-003 Additions.End		
		private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		private PackedDecimalData offset = new PackedDecimalData(3, 0);
		private FixedLengthStringData filler = new FixedLengthStringData(126);
		private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(21, 6, filler, 0);
		private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
		private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		//-----------------> Change the transaction Number here
		private String t1688 = "T1688";
		//TMLI WS-002 Additions. End
		
		/*Standard Letter Control File*/
		private LetcTableDAM letcIO = new LetcTableDAM();
		
		//WS-003 Changes Start...Phani
		/*Logical over CVUW*/
		//private CvuwTableDAM cvuwIO = new CvuwTableDAM();
		private PtshclmTableDAM ptshclmIO = new PtshclmTableDAM();
		private SurhclmTableDAM surhclmIO = new SurhclmTableDAM(); 
		private ClmhclmTableDAM clmhclmIO = new ClmhclmTableDAM();		
		private DescTableDAM descIO = new DescTableDAM();
		
		private String ptschclmrec = "PTSHCLMREC";
		private String surhclmrec = "SURHCLMREC";
		private String clmhclmrec = "CLMHCLMREC";
		
		
		
		private Datcon1rec datcon1rec = new Datcon1rec();
		private Pcpdatarec pcpdatarec = new Pcpdatarec();
		private Syserrrec syserrrec = new Syserrrec();
		private Varcom varcom = new Varcom();
		//WS-003 Changes Start...Phani
		
		
		private enum GotoLabel implements GOTOInterface {
			DEFAULT, 
			exit090
		}

		public Pcplbenclm() {
			super();
		}
		
		
		public void mainline(Object... parmArray)
		{
			letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
			pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
			
			try {
				main000();
			}
			catch (COBOLExitProgramException e) {
			}

			setReturningParam(letcIO.getParams(), parmArray, 1);
		}
		
		protected void main000()
		{
			try {
				main010();
			}
			catch (GOTOException e){			
			}
			finally{
				exit090();
			}
		}

		protected void main010()
		{
			if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
				initialise100();
				getDetails200();
			}		
			offset.set(pcpdatarec.fldOffset);
			pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));		
			pcpdatarec.dataLen.set(fldlen[offset.toInt()]);		
			pcpdatarec.statuz.set(varcom.oK);
			/* EXIT*/
			exit090();		
		}
		
		protected void exit090()
		{
			exitProgram();
		}
		
		protected void initialise100()
		{
			/*PARA*/
			wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
			//wsaaStoreRrn.set(letcIO.getRrn());
			wsaaTransCde.set(SPACES);
			wsaaLifeBenefitApplnDate.set(SPACES);
			wsaaLifeBenefitDocRcvdDate.set(SPACES);		
			
		}
		
		protected void getDetails200()
		{	
			getLifeBenefitType201();
			wsaaTransCde.set(letcIO.getTransCode());
			if (isEQ(wsaaTransCde,"T510")) {	
				readPtshClaim202();
			}else if (isEQ(wsaaTransCde,"T512")) {
				readsurhclaim203();	
			}else if(isEQ(wsaaTransCde,"T668")){
				readclaimh204();
			}
			format210();
			format220();
			format230();
		}
		
		protected void getLifeBenefitType201()
		{
			descIO.setDataKey(SPACES);
			descIO.setDescpfx("IT");
			descIO.setDesccoy(letcIO.getRdoccoy());
			descIO.setDesctabl(t1688);
			descIO.setDescitem(letcIO.getTransCode());
			descIO.setLanguage(pcpdatarec.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)
			&& isEQ(descIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(descIO.getParams());
				fatalError900();
			} else {
				return;
			}			
		}
		
		protected void readPtshClaim202() {
			ptshclmIO.setDataKey(SPACES);
			ptshclmIO.setChdrcoy(letcIO.getRdoccoy());
			ptshclmIO.setChdrnum(letcIO.getRdocnum());
			ptshclmIO.setTranno(letcIO.getTranno());
			ptshclmIO.setPlanSuffix(0);			
			ptshclmIO.setFunction(varcom.begn);
			ptshclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			ptshclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
			SmartFileCode.execute(appVars, ptshclmIO);
			if(isEQ(ptshclmIO.getStatuz(),varcom.oK)){
				datcon1rec.intDate.set(ptshclmIO.getEffdate());
				datcon1rec.function.set(varcom.conv);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaLifeBenefitApplnDate.set(datcon1rec.extDate);
			}
		}
		
		protected void readsurhclaim203() {
			
			surhclmIO.setDataKey(SPACES);
			surhclmIO.setChdrcoy(letcIO.getRdoccoy());
			surhclmIO.setChdrnum(letcIO.getRdocnum());
			surhclmIO.setTranno(letcIO.getTranno());
			surhclmIO.setPlanSuffix(0);			
			surhclmIO.setFunction(varcom.begn);
			surhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			surhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
			SmartFileCode.execute(appVars, surhclmIO);
			if(isEQ(surhclmIO.getStatuz(),varcom.oK)){
				datcon1rec.intDate.set(surhclmIO.getEffdate());
				datcon1rec.function.set(varcom.conv);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaLifeBenefitDocRcvdDate.set(datcon1rec.extDate);		
			}
		}
		protected void readclaimh204(){
			clmhclmIO.setDataKey(SPACES);
			clmhclmIO.setChdrcoy(letcIO.getRdoccoy());
			clmhclmIO.setChdrnum(letcIO.getRdocnum());		
			clmhclmIO.setTranno(letcIO.getTranno());
			clmhclmIO.setFunction(varcom.begn);
			clmhclmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			clmhclmIO.setFitKeysSearch("CHDRCOY","CHDRNUM","TRANNO");
			
			SmartFileCode.execute(appVars, clmhclmIO);
			if(isEQ(clmhclmIO.getStatuz(),varcom.oK)){
				datcon1rec.intDate.set(clmhclmIO.getEffdate());
				datcon1rec.function.set(varcom.conv);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaLifeBenefitDocRcvdDate.set(datcon1rec.extDate);	
			}
			
		}
		
		protected void format210(){
			offset.set(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(descIO.getLongdesc()));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());			
		}
		protected void format220(){
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(wsaaLifeBenefitApplnDate));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLifeBenefitApplnDate);
		}
		protected void format230(){
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(length(wsaaLifeBenefitDocRcvdDate));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaLifeBenefitDocRcvdDate);
		}
		
		protected void fatalError900()	
		{
			/*FATAL*/
			syserrrec.subrname.set(wsaaSubrname);
			pcpdatarec.statuz.set(varcom.bomb);
			if (isNE(syserrrec.statuz,SPACES)
			|| isNE(syserrrec.syserrStatuz,SPACES)) {
				syserrrec.syserrType.set("1");
			}
			else {
				syserrrec.syserrType.set("2");
			}
			callProgram(Syserr.class, syserrrec.syserrRec);
			/*EXIT*/
			exitProgram();
		}
}//End Class

