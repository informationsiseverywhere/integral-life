/*
 * File: Pcplnamchg.java
 * Date: 30 August 2009 1:02:26
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLNAMCHG.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.TclhTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Last Salutation  ---> LLSALUTE }
*  002  Last Given Name  ---> LLGIVNAM }
*  003  Last Surname     ---> LLSURNAM }
*  004  Current Salutation -> LCSALUTE } in T2636
*  005  Current Given Name -> LCGIVNAM }
*  006  Current Surname  ---> LCSURNAM }
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplnamchg extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLNAMCHG";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaClntcoy = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 1);
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).isAPartOf(wsaaOtherKeys, 2);
	
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(271).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (7, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String tclhrec = "TCLHREC";
	private static final String cltsrec = "CLTSREC";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private TclhTableDAM tclhIO = new TclhTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(15);
	FixedLengthStringData sRole=new FixedLengthStringData(15);
	
	private enum GotoLabel implements GOTOInterface {
		 
		clrfNextr1480, 
		 
	}
	public Pcplnamchg() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getTclhAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		/*EXIT*/
	}

protected void getTclhAndFormat200()
	{
		start200();
	}

protected void start200()
	{
		tclhIO.setParams(SPACES);
		tclhIO.setStatuz(varcom.oK);
		tclhIO.setClntpfx(fsupfxcpy.clnt);
		tclhIO.setClntcoy(wsaaClntcoy);
		tclhIO.setClntnum(wsaaClntnum);
		tclhIO.setFormat(tclhrec);
		tclhIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		tclhIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		tclhIO.setFitKeysSearch("CLNTCOY", "CLNTNUM");
		callTclhio300();
		while ( !(isEQ(tclhIO.getStatuz(),varcom.endp))) {
			if (isEQ(tclhIO.getClntpfx(),fsupfxcpy.clnt)
			&& isEQ(tclhIO.getClntcoy(),wsaaClntcoy)
			&& isEQ(tclhIO.getClntnum(),wsaaClntnum)) {
				if (isEQ(tclhIO.getValidflag(),"1")) {
					getPreviousName400();
					getCurrentName500();
					tclhIO.setStatuz(varcom.endp);
				}
				else {
					tclhIO.setFunction(varcom.nextr);
					callTclhio300();
				}
			}
		}
		
	}

protected void callTclhio300()
	{
		/*CALL*/
		SmartFileCode.execute(appVars, tclhIO);
		if (isNE(tclhIO.getStatuz(),varcom.oK)
		&& isNE(tclhIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(tclhIO.getParams());
			fatalError900();
		}
		if (isEQ(tclhIO.getStatuz(),varcom.endp)
		|| isNE(tclhIO.getClntpfx(),"CN")
		|| isNE(tclhIO.getClntcoy(),wsaaClntcoy)
		|| isNE(tclhIO.getClntnum(),wsaaClntnum)) {
			tclhIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getPreviousName400()
	{
		format410();
		format430();
	}

protected void format410()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(tclhIO.getSalutl()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tclhIO.getSalutl());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(tclhIO.getLgivname()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tclhIO.getLgivname());
	}

protected void format430()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(tclhIO.getLsurname()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], tclhIO.getLsurname());
		/*EXIT*/
	}

protected void getCurrentName500()
	{
		call510();
		format530();
		namechng();
	}

protected void call510()
	{
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsaaClntcoy);
		cltsIO.setClntnum(wsaaClntnum);
		cltsIO.setFunction(varcom.readr);
		callCltsio600();
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(cltsIO.getSalutl()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], cltsIO.getSalutl());
	}

protected void format530()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(cltsIO.getLgivname()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], cltsIO.getLgivname());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(cltsIO.getLsurname()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], cltsIO.getLsurname());
		/*EXIT*/
	}

protected void callCltsio600()
	{
		/*CALL*/
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void namechng()
{
	 
	clrfIO.setParams(SPACES);
    clrfIO.setForepfx("CH");
    clrfIO.setForecoy(letcIO.getChdrcoy());
    clrfIO.setForenum(letcIO.getChdrnum());
    clrfIO.setFunction(varcom.begn);
    //performance improvement -- Anjali
    clrfIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
    clrfIO.setFitKeysSearch("FOREPFX", "FORECOY");
    SmartFileCode.execute(appVars, clrfIO);
    if (isNE(clrfIO.getStatuz(),varcom.oK)
    && isNE(clrfIO.getStatuz(),varcom.endp)) {
                  syserrrec.params.set(clrfIO.getParams());
                  fatalError900();
    }
    wsaaForenum.set(clrfIO.getForenum());
    while ( !(isEQ(clrfIO.getStatuz(),varcom.endp)
    || isNE(clrfIO.getForepfx(),"CH")
    || isNE(clrfIO.getForecoy(),letcIO.getChdrcoy())
    || isNE(wsaaForenum,letcIO.getChdrnum()))) {
                  searchclientrole();
    }
    
    if (isEQ(sRole,"BN") )
    {
    	sRole.set("Beneficiary");
    }
    else
    {	
    	sRole.set("Life Assured");
    }
    offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(sRole));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], sRole);
	/*EXIT*/

}

private void searchclientrole() {
	
	// TODO Auto-generated method stub
	if (isEQ(clrfIO.getUsedToBe(),SPACES)
			&& (isEQ(clrfIO.getClrrrole(),"LF")
			|| isEQ(clrfIO.getClrrrole(),"BN") || isEQ(clrfIO.getClrrrole(),"OW") )) {
		if  (isEQ(wsaaClntnum,clrfIO.getClntnum()) )
		{
		sRole.set(clrfIO.getClrrrole());
	
				/*NEXT_SENTENCE*/
			}
	 
		
	}
	 
	clrfNextr1480();		
}

protected void clrfNextr1480()
{
	clrfIO.setFunction(varcom.nextr);
	SmartFileCode.execute(appVars, clrfIO);
	if (isNE(clrfIO.getStatuz(),varcom.oK)
	&& isNE(clrfIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(clrfIO.getParams());
		 
	}
	wsaaForenum.set(clrfIO.getForenum());
	/*EXIT*/
}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
