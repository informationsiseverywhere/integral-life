/*
 * File: Zpcplttrc.java
 
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.exception.ServerException;
import com.quipoz.framework.exception.TransferProgramException;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.sessionfacade.JTAWrapper;
import com.quipoz.framework.util.*;
import com.quipoz.COBOLFramework.util.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import static com.quipoz.COBOLFramework.util.CLFunctions.*;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.ExitSection;
import com.csc.smart400framework.utility.Validator;
import java.util.ArrayList;
import com.quipoz.framework.util.log.QPLogger;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
//ILAE-63 START by dpuhawan
//import com.csc.fsu.financials.dataaccess.TtrcTableDAM;
import com.csc.fsu.reinsurance.dataaccess.TtrcTableDAM;
//ILAE-63 END
import com.csc.smart.recordstructures.Varcom;



import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.csc.smart400framework.procedures.Syserr;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Contract Receipt No
*  002  Contract Receipt Date
*  003  Temporary Receipt No
*  004  Temporary Receipt Date
*
***********************************************************************
* </pre>
*/
public class Zpcplttrc extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "ZPCPLTTRC";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(4, 6, filler, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
	private String rtrnrec = "RTRNREC";
	private String ttrcrec = "TTRCREC";
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
		/*Receipt Details - Contract Enquiry.*/
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
		/*Temporary Receipt Detail*/
	private TtrcTableDAM ttrcIO = new TtrcTableDAM();
	private Varcom varcom = new Varcom();

	public Zpcplttrc() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getFormatFirstReceipt200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getFormatFirstReceipt200()
	{
		start210();
		formatTempReceiptNo201();
		formatTempReceiptDate201();
	}

protected void start210()
	{
		chdrlnbIO.setChdrcoy(letcIO.getChdrcoy());
		chdrlnbIO.setChdrnum(letcIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		ttrcIO.setDataKey(SPACES);
		ttrcIO.setChdrcoy(letcIO.getChdrcoy());
		ttrcIO.setChdrnum(letcIO.getChdrnum());
		ttrcIO.setEffdate(varcom.vrcmMaxDate);
		ttrcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ttrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ttrcIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		ttrcIO.setFormat(ttrcrec);
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(),varcom.oK)
		&& isNE(ttrcIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isNE(ttrcIO.getStatuz(),varcom.oK)
		|| isNE(ttrcIO.getChdrcoy(),letcIO.getChdrcoy())
		|| isNE(ttrcIO.getChdrnum(),letcIO.getChdrnum())) {
			ttrcIO.setTtmprcno(SPACES);
			ttrcIO.setTtmprcdte(varcom.vrcmMaxDate);
		}
	}


protected void formatTempReceiptNo201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(ttrcIO.getTtmprcno()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], ttrcIO.getTtmprcno());
	}

protected void formatTempReceiptDate201()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(ttrcIO.getTtmprcdte());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
