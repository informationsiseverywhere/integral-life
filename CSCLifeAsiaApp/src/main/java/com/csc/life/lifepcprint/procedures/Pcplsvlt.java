/*
 * File: Pcplsvlt.java
 * Date: 30 August 2009 1:03:06
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLSVLT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.terminationclaims.dataaccess.SvltclmTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Loan amount
*   02    Other Adjustment
*   03    Total Estimated Surrender Value
*   04    Total Actual Surrender Value
*   05    Short Description            }
*   06    Estimated Surrender Value    }
*   07    Actual Surrender Value       } Occurrance fields
*   08    Currency                     }
*
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include 05, 06, 07, 08 which are not the occurance field.
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplsvlt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLSVLT";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);

		/* WSAA-SUR */
	private FixedLengthStringData[] wsaaSurField = FLSInittedArray (10, 27);
	private FixedLengthStringData[] wsaaSurDes = FLSDArrayPartOfArrayStructure(10, wsaaSurField, 0);
	private PackedDecimalData[] wsaaSurEst = PDArrayPartOfArrayStructure(13, 2, wsaaSurField, 10, UNSIGNED_TRUE);
	private PackedDecimalData[] wsaaSurAct = PDArrayPartOfArrayStructure(13, 2, wsaaSurField, 17, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaSurCur = FLSDArrayPartOfArrayStructure(3, wsaaSurField, 24);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private PackedDecimalData wsaaEstmatValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaActualValue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPolicyloan = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOtheradjst = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99-");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(4).setUnsigned();
		/* The following array is configured to store up to 40 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (44, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private SvltclmTableDAM svltclmIO = new SvltclmTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplsvlt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getSvlt200();
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		if (isEQ(pcpdatarec.fldOffset,1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		wsaaCounter.set(ZERO);
		wsaaSub.set(ZERO);
		wsaaPolicyloan.set(ZERO);
		wsaaOtheradjst.set(ZERO);
		wsaaEstmatValue.set(ZERO);
		wsaaActualValue.set(ZERO);
		offset.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			wsaaSurDes[wsaaSub.toInt()].set(SPACES);
			wsaaSurCur[wsaaSub.toInt()].set(SPACES);
			wsaaSurEst[wsaaSub.toInt()].set(ZERO);
			wsaaSurAct[wsaaSub.toInt()].set(ZERO);
		}
		/*EXIT*/
	}

protected void getSvlt200()
	{
		getData200();
		format201();
		format202();
		format204();
	}

protected void getData200()
	{
		svltclmIO.setChdrcoy(letcIO.getRdoccoy());
		svltclmIO.setChdrnum(letcIO.getRdocnum());
		svltclmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, svltclmIO);
		if (isNE(svltclmIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(svltclmIO.getStatuz());
			syserrrec.params.set(svltclmIO.getParams());
			fatalError600();
		}
		wsaaPolicyloan.set(svltclmIO.getPolicyloan());
		wsaaOtheradjst.set(svltclmIO.getOtheradjst());
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)
		|| isEQ(svltclmIO.getShortds(wsaaSub),SPACES)); wsaaSub.add(1)){
			wsaaSurDes[wsaaSub.toInt()].set(svltclmIO.getShortds(wsaaSub));
			wsaaSurEst[wsaaSub.toInt()].set(svltclmIO.getEmv(wsaaSub));
			wsaaSurAct[wsaaSub.toInt()].set(svltclmIO.getActvalue(wsaaSub));
			wsaaSurCur[wsaaSub.toInt()].set(svltclmIO.getCnstcur(wsaaSub));
			wsaaEstmatValue.add(svltclmIO.getEmv(wsaaSub));
			wsaaActualValue.add(svltclmIO.getActvalue(wsaaSub));
		}
	}

protected void format201()
	{
		offset.add(1);
		if (isEQ(offset,1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		wsaaAmount.set(wsaaPolicyloan);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void format202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(wsaaOtheradjst);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(wsaaEstmatValue);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void format204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(wsaaActualValue);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)
		|| isEQ(svltclmIO.getShortds(wsaaSub),SPACES)); wsaaSub.add(1)){
			formatSvlt300();
		}
		/*EXIT*/
	}

protected void formatSvlt300()
	{
		format301();
		format303();
	}

protected void format301()
	{
		wsaaCounter.add(1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaSurDes[wsaaSub.toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSurDes[wsaaSub.toInt()]);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(wsaaSurEst[wsaaSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void format303()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(wsaaSurAct[wsaaSub.toInt()]);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaSurCur[wsaaSub.toInt()]));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSurCur[wsaaSub.toInt()]);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
