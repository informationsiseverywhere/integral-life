/*
 * File: Pcpslife2.java
 * Date: 30 August 2009 1:03:19
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPSLIFE2.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpscrlrec;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  Overview.
*  ---------
*
*  This is a P.C.Printing/400 "scroll" Subroutine. Its purpose is
*  to return LIFE/400 information on lives per contract. It may
*  be used as a genuine "scroll" subroutine or cloned. The order
*  that the lives are returned is life number order.
*
*  Processing.
*  -----------
*
*  LETC Fields Required: Completed RDOCNUM
*
*  On a change of LETC RRN (a new letter request) read through the
*  the LIFE file loading each active Life into a working
*  storage table. (We do this because other data extract subroutines
*  may upset our logical file pointer if we use begn/nextr logic.)
*  Setup the LETC Other Keys area using the LETCOKCPY copybook and
*  return the first life.
*
*  On subsequent calls, add 1 to the pointer and return the next
*  life until all lives have been returned.
*
***********************************************************************
* </pre>
*/
public class Pcpslife2 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaIdCode = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaMainCode = new FixedLengthStringData(4).isAPartOf(wsaaIdCode, 0);
	private FixedLengthStringData wsaaSubCode = new FixedLengthStringData(4).isAPartOf(wsaaIdCode, 4);
	private BinaryData wsaaStoreRrn = new BinaryData(9, 0).init(ZERO);
	private PackedDecimalData ixMax = new PackedDecimalData(3, 0);
	private PackedDecimalData ixRsk = new PackedDecimalData(3, 0);

	private FixedLengthStringData wsaaLifeArray = new FixedLengthStringData(800);
	private FixedLengthStringData[] wsaaItem = FLSArrayPartOfStructure(100, 8, wsaaLifeArray, 0);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaItem, 0);
	private FixedLengthStringData[] wsaaCovr = FLSDArrayPartOfArrayStructure(2, wsaaItem, 2);
	private FixedLengthStringData[] wsaaSubId = FLSDArrayPartOfArrayStructure(4, wsaaItem, 4);
	private String liferec = "LIFEREC";
	private String covrrec = "COVRREC";
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private Pcpscrlrec pcpscrlrec = new Pcpscrlrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit259, 
		exit309, 
		exit709
	}

	public Pcpslife2() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpscrlrec.pcpscrlRec = convertAndSetParam(pcpscrlrec.pcpscrlRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*PARA*/
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getChdrDetails200();
			while ( !(isEQ(lifeIO.getStatuz(),varcom.endp))) {
				getLifeDetails250();
				while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) {
					getCovrDetails300();
				}
				
			}
			
		}
		returnCode700();
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaIdCode.set(pcpscrlrec.idcode);
		wsaaStoreRrn.set(letcIO.getRrn());
		ixRsk.set(ZERO);
		ixMax.set(ZERO);
		lifeIO.setParams(SPACES);
		lifeIO.setLife("01");
		lifeIO.setFunction(varcom.begn);
		covrIO.setStatuz(varcom.oK);
		/*EXIT*/
	}

protected void getChdrDetails200()
	{
		getChdrDetails201();
	}

protected void getChdrDetails201()
	{
		chdrIO.setChdrpfx("CH");
		chdrIO.setChdrcoy(letcIO.getRdoccoy());
		chdrIO.setChdrnum(letcIO.getRdocnum());
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setValidflag("1");
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM", "VALIDFLAG");
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrIO.getStatuz(),varcom.endp)
		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(chdrIO.getChdrpfx(),"CH")
		|| isNE(chdrIO.getValidflag(),"1")) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
	}

protected void getLifeDetails250()
	{
		try {
			getLifeDetails251();
		}
		catch (GOTOException e){
		}
	}

protected void getLifeDetails251()
	{
		lifeIO.setStatuz(varcom.oK);
		lifeIO.setChdrcoy(letcIO.getRdoccoy());
		lifeIO.setChdrnum(letcIO.getRdocnum());
		lifeIO.setJlife("00");
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(),varcom.oK)
		&& isNE(lifeIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		lifeIO.setFunction(varcom.nextr);
		if (isEQ(lifeIO.getStatuz(),varcom.endp)
		|| isNE(lifeIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(lifeIO.getChdrnum(),letcIO.getRdocnum())) {
			lifeIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit259);
		}
		if (isNE(lifeIO.getJlife(),"00")) {
			goTo(GotoLabel.exit259);
		}
		ixMax.add(1);
		wsaaLife[ixMax.toInt()].set(lifeIO.getLife());
		wsaaCovr[ixMax.toInt()].set(SPACES);
		wsaaSubId[ixMax.toInt()].set(chdrIO.getCnttype());
		covrIO.setFunction(varcom.begn);
		covrIO.setStatuz(varcom.oK);
	}

protected void getCovrDetails300()
	{
		try {
			getData301();
		}
		catch (GOTOException e){
		}
	}

protected void getData301()
	{
		covrIO.setChdrcoy(letcIO.getRdoccoy());
		covrIO.setChdrnum(letcIO.getRdocnum());
		covrIO.setLife(lifeIO.getLife());
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		covrIO.setFunction(varcom.nextr);
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(covrIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(covrIO.getLife(),lifeIO.getLife())) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit309);
		}
		if (isNE(covrIO.getRider(),"00")) {
			goTo(GotoLabel.exit309);
		}
		ixMax.add(1);
		wsaaLife[ixMax.toInt()].set(lifeIO.getLife());
		wsaaCovr[ixMax.toInt()].set(covrIO.getCoverage());
		wsaaSubId[ixMax.toInt()].set(covrIO.getCrtable());
	}

protected void fatalError600()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void returnCode700()
	{
		try {
			start701();
		}
		catch (GOTOException e){
		}
	}

protected void start701()
	{
		ixRsk.add(1);
		if (isGT(ixRsk,ixMax)) {
			pcpscrlrec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit709);
		}
		pcpscrlrec.statuz.set(varcom.oK);
		letcokcpy.lu.set(SPACES);
		letcokcpy.recCode.set("LU");
		letcokcpy.luLife.set(wsaaLife[ixRsk.toInt()]);
		letcokcpy.luCoverage.set(wsaaCovr[ixRsk.toInt()]);
		letcokcpy.luPlanSuffix.set(ZERO);
		letcIO.setOtherKeys(letcokcpy.saveOtherKeys);
		wsaaSubCode.set(wsaaSubId[ixRsk.toInt()]);
		pcpscrlrec.idcode.set(wsaaIdCode);
	}
}
