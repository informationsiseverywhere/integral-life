/*
 * File: Pcplchcl.java
 * Date: 30 August 2009 1:00:24
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCHCL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Contract Number.
*   02    Contract Original Start Date
*   03    Current from Date
*   04    Current to Date
*   05    Servicing Branch
*   06    Client Number.
*   07    Client Name.
*   08    Client DOB.
*   09    Client Address line 01
*   10    Client Address line 02
*   11    Client Address line 03
*   12    Client Address line 04
*   13    Client Address line 05
*   14    Client Postcode.
*   15    Client Type.
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplchcl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCHCL  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (30, 6);//ILIFE-4116
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* TABLES */
	private static final String t1692 = "T1692";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit399
	}

	public Pcplchcl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getChdrAndFormat200();
			clrfIO.setParams(SPACES);
			clrfIO.setForepfx(letcIO.getRdocpfx());
			clrfIO.setForecoy(letcIO.getRdoccoy());
			clrfIO.setForenum(letcIO.getRdocnum());
			clrfIO.setClrrrole("OW");
			clrfIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clrfIO);
			if (isNE(clrfIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(clrfIO.getParams());
				fatalError600();
			}
			clntIO.setClntnum(clrfIO.getClntnum());
			getClntAndFormat300();
			clrfIO.setParams(SPACES);
			clrfIO.setForepfx(letcIO.getRdocpfx());
			clrfIO.setForecoy(letcIO.getRdoccoy());
			clrfIO.setForenum(letcIO.getRdocnum());
			clrfIO.setClrrrole("JO");
			clrfIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clrfIO);
			if (isNE(clrfIO.getStatuz(),varcom.oK)
			&& isNE(clrfIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(clrfIO.getParams());
				fatalError600();
			}
			if (isEQ(clrfIO.getStatuz(),varcom.oK)) {
				clntIO.setClntnum(clrfIO.getClntnum());
				getClntAndFormat300();
			}
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/*EXIT*/
	}

protected void getChdrAndFormat200()
	{
		getData200();
		formatChdrnum201();
		formatOrigCommDate202();
		formatEffectiveDate203();
		formatEffectiveTo204();
		formatBranch205();
	}

protected void getData200()
	{
		chdrlifIO.setChdrpfx(letcIO.getRdocpfx());
		chdrlifIO.setChdrcoy(letcIO.getRdoccoy());
		chdrlifIO.setChdrnum(letcIO.getRdocnum());
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(letcIO.getLetterRequestDate());
		chdrlifIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
	}

protected void formatChdrnum201()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(chdrlifIO.getChdrnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrlifIO.getChdrnum());
	}

protected void formatOrigCommDate202()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrlifIO.getOccdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveDate203()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrlifIO.getCurrfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveTo204()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrlifIO.getCurrto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBranch205()
	{
		offset.add(1);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(letcIO.getRequestCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrlifIO.getCntbranch());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "    ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
	}

protected void getClntAndFormat300()
	{
		try {
			para301();
			formatField1305();
			formatField2310();
			formatField315();
			formatField380();
			formatField381();
			formatField382();
			formatField383();
			formatField384();
			formatField385();
			formatField386();
		}
		catch (GOTOException e){
		}
	}

protected void para301()
	{
		clntIO.setDataKey(SPACES);
		clntIO.setClntnum(chdrlifIO.getCownnum());
		clntIO.setClntpfx(chdrlifIO.getCownpfx());
		clntIO.setClntcoy(chdrlifIO.getCowncoy());
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
	}

protected void formatField1305()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(clntIO.getClntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clntIO.getClntnum());
	}

protected void formatField2310()
	{
		offset.add(1);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(pcpdatarec.fsuco);
		namadrsrec.clntNumber.set(clntIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatField315()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(clntIO.getCltdob());
		callDatcon1500();
		fldlen[offset.toInt()].set(length(wsaaDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDate);
		if (isEQ(clntIO.getClntnum(),chdrlifIO.getJownnum())) {
			goTo(GotoLabel.exit399);
		}
	}

protected void formatField380()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltaddr01(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltaddr01(), 1, fldlen[offset.toInt()]));
	}

protected void formatField381()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltaddr02(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltaddr02(), 1, fldlen[offset.toInt()]));
	}

protected void formatField382()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltaddr03(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltaddr03(), 1, fldlen[offset.toInt()]));
	}

protected void formatField383()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltaddr04(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltaddr04(), 1, fldlen[offset.toInt()]));
	}

protected void formatField384()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltaddr05(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltaddr05(), 1, fldlen[offset.toInt()]));
	}

protected void formatField385()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(clntIO.getCltpcode(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(clntIO.getCltpcode(), 1, fldlen[offset.toInt()]));
	}

protected void formatField386()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(clntIO.getClttype()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clntIO.getClttype());
	}

protected void callDatcon1500()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaDate.set(datcon1rec.extDate);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
