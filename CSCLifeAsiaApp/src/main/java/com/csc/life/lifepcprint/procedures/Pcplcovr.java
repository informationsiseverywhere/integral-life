/*
 * File: Pcplcovr.java
 * Date: 30 August 2009 1:00:48
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCOVR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Coverage Description
*  002  Invested Amount - no longer use!!!
*  002  Inst Premium
*  003  Single Premium
*  004  Maturity Date
*  005  Sumins
*  006  Risk Commencement Date
*  007  Premium Cess Date
*  008  Benefit Cess Date
*  009  Risk Cess Term
*  010  Premium Cess Term
*  011  Benefit Cess Term
*  012  Risk Cess Age
*  013  Premium Cess Age
*  014  Benefit Cess Age
*
*  Overview.
*  ---------
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplcovr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCOVR  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/*   SUBROUTINE SPECIFIC FIELDS.*/
	private ZonedDecimalData wsaaSingp = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaCrinst01 = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(15, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private ZonedDecimalData wsaaBenCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private ZonedDecimalData wsaaRiskCessAge = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private ZonedDecimalData wsaaPremCessAge = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private ZonedDecimalData wsaaBenCessAge = new ZonedDecimalData(3, 0).setPattern("ZZ9");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (14, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String covrrec = "COVRREC";
		/* TABLES */
	private static final String t5687 = "T5687";
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Getdescrec getdescrec = new Getdescrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplcovr() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getCovrAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		covrIO.setParams(SPACES);
		covrIO.setFunction(varcom.begn);
		wsaaRiskCessTerm.set(ZERO);
		wsaaPremCessTerm.set(ZERO);
		wsaaBenCessTerm.set(ZERO);
		wsaaRiskCessAge.set(ZERO);
		wsaaPremCessAge.set(ZERO);
		wsaaBenCessAge.set(ZERO);
		/*EXIT*/
	}

protected void getCovrAndFormat200()
	{
		try {
			getData200();
			format201();
			format202();
			format204();
			format205();
			format206();
			format207();
			format208();
			format209();
			format210();
			format211();
			format212();
			format213();
			format214();
		}
		catch (GOTOException e){
		}
	}

protected void getData200()
	{
		covrIO.setStatuz(varcom.oK);
		covrIO.setChdrcoy(letcIO.getRdoccoy());
		covrIO.setChdrnum(letcIO.getRdocnum());
		covrIO.setLife("01");
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(covrIO.getChdrnum(),letcIO.getRdocnum())
		|| isNE(covrIO.getLife(),"01")
		|| isNE(covrIO.getLife(),"01")
		|| isNE(covrIO.getRider(),"00")) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
	}

protected void format201()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		itemIO.setItemtabl(t5687);
		itemIO.setItemitem(covrIO.getCrtable());
		getdesc800();
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(getdescrec.longdesc, " ", "CHARACTERS", "  ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(getdescrec.longdesc, 1, fldlen[offset.toInt()]));
	}

protected void format202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaCrinst01.set(covrIO.getInstprem());
		fldlen[offset.toInt()].set(length(wsaaCrinst01));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCrinst01);
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSingp.set(covrIO.getSingp());
		fldlen[offset.toInt()].set(length(wsaaSingp));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSingp);
	}

protected void format204()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(covrIO.getRiskCessDate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format205()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSumins.set(covrIO.getSumins());
		fldlen[offset.toInt()].set(length(wsaaSumins));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSumins);
	}

protected void format206()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(covrIO.getCrrcd());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format207()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(covrIO.getPremCessDate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format208()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		if (isNE(covrIO.getBenCessDate(),0)
		&& isNE(covrIO.getBenCessDate(),99999999)) {
			datcon1rec.intDate.set(covrIO.getBenCessDate());
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format209()
	{
		if (isNE(covrIO.getRiskCessTerm(),0)) {
			wsaaRiskCessTerm.set(covrIO.getRiskCessTerm());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrIO.getCrrcd());
			datcon3rec.intDate2.set(covrIO.getRiskCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
			wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaRiskCessTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskCessTerm);
	}

protected void format210()
	{
		if (isNE(covrIO.getPremCessTerm(),0)) {
			wsaaPremCessTerm.set(covrIO.getPremCessTerm());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrIO.getCrrcd());
			datcon3rec.intDate2.set(covrIO.getPremCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
			wsaaPremCessTerm.set(datcon3rec.freqFactor);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPremCessTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremCessTerm);
	}

protected void format211()
	{
		if (isNE(covrIO.getBenCessTerm(),0)) {
			wsaaBenCessTerm.set(covrIO.getBenCessTerm());
		}
		else {
			if (isNE(covrIO.getBenCessDate(),0)
			&& isNE(covrIO.getBenCessDate(),99999999)) {
				datcon3rec.datcon3Rec.set(SPACES);
				datcon3rec.intDate1.set(covrIO.getCrrcd());
				datcon3rec.intDate2.set(covrIO.getBenCessDate());
				datcon3rec.frequency.set("01");
				callProgram(Datcon3.class, datcon3rec.datcon3Rec);
				if (isNE(datcon3rec.statuz,varcom.oK)) {
					goTo(GotoLabel.exit299);
				}
				wsaaBenCessTerm.set(datcon3rec.freqFactor);
			}
			else {
				wsaaBenCessTerm.set(wsaaRiskCessTerm);
			}
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaBenCessTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBenCessTerm);
	}

protected void format212()
	{
		if (isNE(covrIO.getRiskCessAge(),0)) {
			wsaaRiskCessAge.set(covrIO.getRiskCessAge());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrIO.getCrrcd());
			datcon3rec.intDate2.set(covrIO.getRiskCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
			compute(wsaaRiskCessAge, 5).set(add(datcon3rec.freqFactor,covrIO.getAnbAtCcd()));
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaRiskCessAge));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskCessAge);
	}

protected void format213()
	{
		if (isNE(covrIO.getPremCessAge(),0)) {
			wsaaRiskCessAge.set(covrIO.getPremCessAge());
		}
		else {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(covrIO.getCrrcd());
			datcon3rec.intDate2.set(covrIO.getPremCessDate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				goTo(GotoLabel.exit299);
			}
			compute(wsaaPremCessAge, 5).set(add(datcon3rec.freqFactor,covrIO.getAnbAtCcd()));
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaPremCessAge));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPremCessAge);
	}

protected void format214()
	{
		if (isNE(covrIO.getBenCessAge(),0)) {
			wsaaRiskCessAge.set(covrIO.getBenCessAge());
		}
		else {
			if (isNE(covrIO.getBenCessDate(),0)
			&& isNE(covrIO.getBenCessDate(),99999999)) {
				datcon3rec.datcon3Rec.set(SPACES);
				datcon3rec.intDate1.set(covrIO.getCrrcd());
				datcon3rec.intDate2.set(covrIO.getBenCessDate());
				datcon3rec.frequency.set("01");
				callProgram(Datcon3.class, datcon3rec.datcon3Rec);
				if (isNE(datcon3rec.statuz,varcom.oK)) {
					return ;
				}
				compute(wsaaBenCessAge, 5).set(add(datcon3rec.freqFactor,covrIO.getAnbAtCcd()));
			}
			else {
				wsaaBenCessAge.set(wsaaRiskCessAge);
			}
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaBenCessAge));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaBenCessAge);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

protected void getdesc800()
	{
		/*GETDESC*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRdoccoy());
		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(pcpdatarec.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz,varcom.oK)) {
			getdescrec.longdesc.fill("?");
		}
		/*EXIT*/
	}
}
