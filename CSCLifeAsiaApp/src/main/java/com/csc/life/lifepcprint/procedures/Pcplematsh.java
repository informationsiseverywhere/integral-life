/*
 * File: pcplematsh.java
 * Date: 12 September 2013 1:00:59
 * Author: Quipoz Limited
 * 
 * Class transformed from pcplematsh.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.life.anticipatedendowment.dataaccess.ZraepfTableDAM;
import com.csc.life.anticipatedendowment.recordstructures.Zraekey;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Djustfy;
import com.csc.fsu.general.procedures.Hfmtdte;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Djustfyrec;
import com.csc.fsu.general.recordstructures.Hfmtdterec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.terminationclaims.recordstructures.Srcalcpy;
import com.csc.life.terminationclaims.tablestructures.Tt550rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*
01 offset   Policy Effective Date
02 offset   Policy Anniversary - A
03 offset  Policy Maturity Date - A
04 offset  Policy Maturity Amount - A
26 offset   Policy Anniversary Bonus 
27 offset   Policy Risk Cess Date
28 offset   Policy Bonus Amount
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved. This
*  does not include 1st 4 fields which are not occurance fields. .
*
*  Processing.
*  -----------
*  This routine extracts all the coverages for the contract.
*
*
*
***********************************************************************
* </pre>
*/
public class Pcplematsh extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLEMATSH";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);	
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private int wsaaNofSingoccFld = 4;
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private PackedDecimalData nCCounter = new PackedDecimalData(2,0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(5000).init(SPACES);
	private ZonedDecimalData wsaaTotBPrem = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaTotLPrem = new ZonedDecimalData(15, 2);	
	private ZonedDecimalData wsaaTotPrem = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wsaaTotAdjprm = new ZonedDecimalData(15, 2);
	private ZonedDecimalData wssaBonusAmount = new ZonedDecimalData(15, 2).setPattern("ZZZ,ZZZ,ZZ9.ZZ");
	private ZonedDecimalData wssaMaturityAmount = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wssaMaturityAmountpercentage = new ZonedDecimalData(5, 2).setPattern("ZZZ.ZZ");
	//private PackedDecimalData wssaBonusAmount = new ZonedDecimalData(15, 2);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaHundredValue = new PackedDecimalData(3, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(2040);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(340, 6, filler, 0);	 
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);	
	private String itemrec = "ITEMREC";		
	private String t5687 = "T5687";	
	private CovtTableDAM covtIO = new CovtTableDAM();		
	private ItemTableDAM itemIO = new ItemTableDAM();		
	private LetcTableDAM letcIO = new LetcTableDAM();	
	private Pcpdatarec pcpdatarec = new Pcpdatarec();	
	private Syserrrec syserrrec = new Syserrrec();	
	private Varcom varcom = new Varcom();	
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(3, 0).setPattern("ZZ9");
	private FixedLengthStringData wsaaRiskcommdte = new FixedLengthStringData(10);	
	private FixedLengthStringData wsaaContractNumber = new FixedLengthStringData(10);	
	private CovrTableDAM covrIO = new CovrTableDAM();		
	private  T5687rec t5687rec = new T5687rec();	
	private ZonedDecimalData wsaaAccumSurrenderValue = new ZonedDecimalData(17, 2).init(0).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99-");;
	private String tt550="TT550";
	private Tt550rec tt550rec =new Tt550rec();	
	private Datcon4rec datcon4rec = new Datcon4rec();	
	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffdate, 0).setUnsigned();
	private ZonedDecimalData wsaaEffmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 4).setUnsigned();
	private ZonedDecimalData wsaaEffdd = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 6).setUnsigned();
	private ZonedDecimalData wsaaEffdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaEffdate, 0, REDEFINE).setUnsigned();	
	
	private FixedLengthStringData wsaaRiskdate = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaRiskyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaRiskdate, 0).setUnsigned();
	private ZonedDecimalData wsaaRiskmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiskdate, 4).setUnsigned();
	private ZonedDecimalData wsaaRiskdd = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiskdate, 6).setUnsigned();
	private ZonedDecimalData wsaaRiskdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaRiskdate, 0, REDEFINE).setUnsigned();
	
	private ZonedDecimalData wsaaMaturitydate = new ZonedDecimalData(8);
	private ZonedDecimalData wsaaMaturityyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaMaturitydate, 0).setUnsigned();
	private ZonedDecimalData wsaaMaturitymm = new ZonedDecimalData(2, 0).isAPartOf(wsaaMaturitydate, 4).setUnsigned();
	private ZonedDecimalData wsaaMaturitydd = new ZonedDecimalData(2, 0).isAPartOf(wsaaMaturitydate, 6).setUnsigned();
	private ZonedDecimalData wsaaMaturitydate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaMaturitydate, 0, REDEFINE).setUnsigned();
	
	
	private ZonedDecimalData wsaaEffMaturitydate = new ZonedDecimalData(10).setPattern("ZZZZ,ZZ,ZZ");
	private ZonedDecimalData wsaaEffMaturityyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffMaturitydate, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMaturitymm = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffMaturitydate, 4).setUnsigned();
	private ZonedDecimalData wsaaEffMaturitydd = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffMaturitydate, 6).setUnsigned();
	private ZonedDecimalData wsaaEffMaturitydate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaEffMaturitydate, 0, REDEFINE).setUnsigned();
	
	private FixedLengthStringData wsaapolicyBonus = new FixedLengthStringData(5);
	private String sqlzraepf1 = "";
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlzraepf1rs = null;
	private java.sql.PreparedStatement sqlzraepf1ps = null;
	private java.sql.Connection sqlzraepf1conn = null;	
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);	
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");
	private FixedLengthStringData filler55 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler55, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler55, 8);
	private String esql = "ESQL";	
	/* SQL-ZRAEPF */
	private FixedLengthStringData sqlZraerec = new FixedLengthStringData(127);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlZraerec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlZraerec, 1);
	private FixedLengthStringData sqlLife = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 9);
	private FixedLengthStringData sqlCoverage = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 11);
	private FixedLengthStringData sqlRider = new FixedLengthStringData(2).isAPartOf(sqlZraerec, 13);
	private PackedDecimalData sqlPlanSfx = new PackedDecimalData(4, 0).isAPartOf(sqlZraerec, 15);
	private PackedDecimalData sqlZrduedte01 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 18);
	private PackedDecimalData sqlZrduedte02 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 23);
	private PackedDecimalData sqlZrduedte03 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 28);
	private PackedDecimalData sqlZrduedte04 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 33);
	private PackedDecimalData sqlZrduedte05 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 38);
	private PackedDecimalData sqlZrduedte06 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 43);
	private PackedDecimalData sqlZrduedte07 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 48);
	private PackedDecimalData sqlZrduedte08 = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 53);
	private PackedDecimalData sqlPrcnt01 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 58);
	private PackedDecimalData sqlPrcnt02 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 61);
	private PackedDecimalData sqlPrcnt03 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 64);
	private PackedDecimalData sqlPrcnt04 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 67);
	private PackedDecimalData sqlPrcnt05 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 70);
	private PackedDecimalData sqlPrcnt06 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 73);
	private PackedDecimalData sqlPrcnt07 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 76);
	private PackedDecimalData sqlPrcnt08 = new PackedDecimalData(5, 2).isAPartOf(sqlZraerec, 79);
	private PackedDecimalData sqlCurrFrom = new PackedDecimalData(8, 0).isAPartOf(sqlZraerec, 82);
	
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	/* SQL-ZRAEPF */	
	private PackedDecimalData wsaaPolicyAnniversay = new PackedDecimalData(2);
	private String covrrec = "COVRREC";
	private String descrec = "DESCREC";
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Hfmtdterec hfmtdterec = new Hfmtdterec();
	private ZonedDecimalData wsaaPremAmt = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZ9.99");;
	
	private enum GotoLabel implements GOTOInterface 
	{
		DEFAULT, 
		exit090, 
		exit299, 
		exit790c, 
		exit790d
	}
	public Pcplematsh() {
		super();
	}

public void mainline(Object... parmArray)
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void main010()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			    initialise100();
			    format50();			
			    nCCounter.set(1);	
				//getData200();
		  	 	ReadT5687();				  	 	
			 	ReadTT550();
			 	getCovtAndFormat200();
			 	ReadFile();
			 	covrIO.setFunction(varcom.begn);
			 	while ( !(isEQ(covrIO.getStatuz(),varcom.endp))) 
			 	{
			 		getData200();
			 		formatAllFields();
			 	}
			  
				format500();
				format600();
				format700();
				format400();				 
		}
		/*if (isEQ(wsaaCounter,ZERO)) {
			pcpdatarec.statuz.set("GEND");
			goTo(GotoLabel.exit090);
		}
		else {
			if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				goTo(GotoLabel.exit090);
			}
		}
		if (isLTE(pcpdatarec.fldOffset,wsaaNofSingoccFld)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);*/
		
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit090);
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);;
	}
	


protected void formatAllFields() {
	 
	for (wsaaSub.set(1); !(isGT(wsaaSub,8)); wsaaSub.add(1))
	{	 
		getCovrAndFormat300(wsaaSub); 
	}
	 
}

	 

protected void format50()
{  
	
			covrIO.setStatuz(varcom.oK);
			covrIO.setChdrcoy(letcIO.getRdoccoy());
			covrIO.setChdrnum(letcIO.getRdocnum());
			covrIO.setLife("01");
			covrIO.setCoverage(SPACES);
			covrIO.setRider(SPACES);
			covrIO.setPlanSuffix(ZERO);
			covrIO.setFormat(covrrec);
			
			SmartFileCode.execute(appVars, covrIO);
			if (isNE(covrIO.getStatuz(),varcom.oK)
			&& isNE(covrIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrIO.getParams());
				//fatalError900();
			}
			wsaaContractNumber.set(covrIO.getChdrnum());
			wsaaSumins.set(covrIO.getSumins());
			wsaaEffMaturitydate.set(covrIO.getRiskCessDate());
			covrIO.setFunction(varcom.nextr);
			if (isEQ(covrIO.getStatuz(),varcom.endp)
			|| isNE(covrIO.getChdrcoy(),letcIO.getRdoccoy())
			|| isNE(covrIO.getChdrnum(),letcIO.getRdocnum())) {
				covrIO.setStatuz(varcom.endp);
				//goTo(GotoLabel.exit299);
			}
			if (isNE(covrIO.getValidflag(),"1")) {
				//goTo(GotoLabel.exit299);
			}
			wsaaContractNumber.set(covrIO.getChdrnum());
			wsaaSumins.set(covrIO.getSumins());
			 
			offset.set(1);
			strpos[offset.toInt()].set(1);
			fldlen[offset.toInt()].set(length(wsaaContractNumber));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaContractNumber);
}


protected void getCovrAndFormat300(ZonedDecimalData nCCounter) {
	try {
		
		
		format500(nCCounter, sqlzraepf1rs);						
		
		format100(nCCounter);
		format200();
		format300();
		
		
	  } 
	 catch (GOTOException e) 
	 {
	 }
}
protected void exit090()
	{	
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		exitProgram();
	}

protected void initialise100()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaCounter.set(ZERO);
		wsaaTotPrem.set(ZERO);
		wsaaTotAdjprm.set(ZERO);
		wsaaPremCessTerm.set(ZERO);
		wsaaRiskcommdte.set(SPACES);		
		wsaaTotBPrem.set(ZERO);
		wsaaTotLPrem.set(ZERO);		
		covrIO.setParams(SPACES);
		covrIO.setFunction(varcom.begn);
		wsaaHundredValue.set(100);
		offset.set(ZERO);		 
		wssaMaturityAmountpercentage.set("0.01");
		wsaaPolicyAnniversay.set(SPACES);
	}
	

protected void getData200()
{
		covrIO.setStatuz(varcom.oK);
		covrIO.setChdrcoy(letcIO.getRdoccoy());
		covrIO.setChdrnum(letcIO.getRdocnum());
		covrIO.setLife("01");
		covrIO.setCoverage(SPACES);
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		
		
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			//fatalError900();
		}
		wsaaContractNumber.set(covrIO.getChdrnum());
		wsaaSumins.set(covrIO.getSumins());
		covrIO.setFunction(varcom.nextr);
		if (isEQ(covrIO.getStatuz(),varcom.endp)
		|| isNE(covrIO.getChdrcoy(),letcIO.getRdoccoy())
		|| isNE(covrIO.getChdrnum(),letcIO.getRdocnum())) {
			covrIO.setStatuz(varcom.endp);
			//goTo(GotoLabel.exit299);
		}
		if (isNE(covrIO.getValidflag(),"1")) {
			//goTo(GotoLabel.exit299);
		}
		//wsaaCounter.add(1);
}

 
protected void format400()
{  			
			datcon1rec.intDate.set(wsaaEffMaturitydate);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			offset.add(1);
			compute(strpos[offset.toInt()],0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			fldlen[offset.toInt()].set(10);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
			
}

protected void format600()
{
	/*  Comment : Added for TMLII 574
	 *  Author : Sankaran J
	 *  Created: 12-Aug-2013
	 *  27 offset   Policy Risk Cess Date
	 */
	 
		offset.add(1);
		compute(strpos[offset.toInt()],0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(0);
		datcon1rec.datcon1Rec.set(SPACES);		
		if ( isNE(covrIO.getRiskCessDate() ,0)  &&  isNE(covrIO.getRiskCessDate(),99999999) )
		{
			datcon1rec.function.set(varcom.conv);	
			datcon1rec.intDate.set(covrIO.crrcd);
			datcon1rec.extDate.set(covrIO.getRiskCessDate()); 
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);		
			if (isNE(datcon1rec.statuz,varcom.oK)) 
			{
				exit090();
			}
		}
		else		
		{
			datcon1rec.extDate.set(SPACES);
		}
		wsaaRiskdate.set(datcon1rec.extDate);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));		
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRiskdate);	
}


protected void format700()
{
		/*  Comment : Added for TMLII 574
		 *  Author : Sankaran J
		 *  Created: 12-Aug-2013
		 * 28 offset   Policy Bonus Amount
		 */		 	
		
		//offset.add(1);
		//compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));		
		//fldlen[offset.toInt()].set(length(wsaaSumins));
		//wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSumins);
	   if (isEQ(tt550rec.tmatpct,ZERO))
		   tt550rec.tmatpct.set(1);
	   
		wssaBonusAmount.set(mult(mult(wsaaSumins ,tt550rec.tmatpct),wssaMaturityAmountpercentage));		
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));			 
		fldlen[offset.toInt()].set(length(wssaBonusAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wssaBonusAmount);	
}

protected void format500()
{
		/*  Comment : Added for TMLII 574
		 *  Author : Sankaran J
		 *  Created: 12-Aug-2013
		 * 26 offset   Policy Anniversary Bonus
		 * 26 offset   Policy Anniversary Bonus 
		 */

	/*TMLII-2483  CLONE - Letter generate unexpected letters like B-O-N-U-S */
		offset.set(26);
		compute(strpos[offset.toInt()],0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		//strpos[offset.toInt()].set(250);
		
		//compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));		
		wsaapolicyBonus.set("BONUS");
		fldlen[offset.toInt()].set(length(wsaapolicyBonus));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaapolicyBonus);	
}
	protected void ReadT5687()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrIO.getChdrcoy());
		itemIO.setItemtabl(t5687);
		itemIO.setItemitem(covrIO.getCrtable());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			//fatalError600();
		}
		t5687rec.t5687Rec.set(itemIO.getGenarea());
	}

	protected void ReadTT550()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covrIO.getChdrcoy());
		itemIO.setItemtabl(tt550);
		itemIO.setItemitem(t5687rec.maturityCalcMeth);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());		
		}
		tt550rec.tt550Rec.set(itemIO.getGenarea());
	}
	
   protected void format100(ZonedDecimalData nCCounter)
   {
  	    wsaaCounter.add(1);
  	    //wsaaPolicyAnniversay.set(nCCounter);	
  	    
	 	if (isEQ(offset,ZERO)) 
		{
	 		offset.set(1);
	 		strpos[offset.toInt()].set(1);
		}
		else 
		{   offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));		 
		}
	 	if(isEQ(wsaaMaturitydate,0)){
	 		fldlen[offset.toInt()].set(length(SPACE));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACE);
	  	}else{
	  		callDatacon3();
		fldlen[offset.toInt()].set(length(wsaaPolicyAnniversay));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaPolicyAnniversay);
	  	}
   }
   protected void callDatacon3()
	{
		datcon3rec.intDate1.set(sqlCurrFrom);
		datcon3rec.intDate2.set(wsaaMaturitydate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaPolicyAnniversay.set(datcon3rec.freqFactor);
		
	}
   
   protected void format200()
   {
	   
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		hfmtdterec.rec.set(SPACES);
		hfmtdterec.hfmtFunction.set("CHGDT");
		hfmtdterec.hfmtLanguage.set(pcpdatarec.language);
		hfmtdterec.hfmtCompany.set(letcIO.getRequestCompany());
		hfmtdterec.hfmtInputDate.set(wsaaMaturitydate);
		hfmtdterec.hfmtOutputDate.set(SPACE);
		callProgram(Hfmtdte.class, hfmtdterec.rec);
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(hfmtdterec.hfmtOutputDate, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(hfmtdterec.hfmtOutputDate, 1, fldlen[offset.toInt()]));
   }
   protected void format300()
   {
	 	offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));		
		  if(isEQ(wsaaMaturitydate,0)){
			  	fldlen[offset.toInt()].set(length(SPACE));
				wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACE);
		  	}else{
		  		fldlen[offset.toInt()].set(length(wssaMaturityAmount));
		  		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wssaMaturityAmount);
		  	}
   }
   
	protected void GetDetails100()
	{		
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		//
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(covrIO.getCrrcd());
		datcon4rec.intDate2.set(covrIO.getPremCessDate());
		datcon4rec.frequency.set("01");
		datcon4rec.freqFactor.set(2);		
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		
		if (isNE(datcon4rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon4rec.datcon4Rec);
			syserrrec.statuz.set(datcon4rec.statuz);
			//fatalError600();
		} else {
			wsaaEffdate.set(datcon4rec.intDate1);
			wsaaRiskdate.set(datcon4rec.intDate2);
		}
				
		sqlzraepf1 = " SELECT  A.CHDRCOY, A.CHDRNUM, A.LIFE, A.COVERAGE, A.RIDER, A.PLNSFX, A.ZRDUEDTE01, A.ZRDUEDTE02, A.ZRDUEDTE03, A.ZRDUEDTE04, A.ZRDUEDTE05, A.ZRDUEDTE06, A.ZRDUEDTE07, A.ZRDUEDTE08, A.PRCNT01, A.PRCNT02, A.PRCNT03, A.PRCNT04, A.PRCNT05, A.PRCNT06, A.PRCNT07, A.PRCNT08,  A.CURRFROM, A.NPAYDATE, C.CNTTYPE, C.AGNTNUM, C.COWNPFX, C.AGNTCOY, C.COWNNUM" +
		" FROM   " + appVars.getTableNameOverriden("ZRAEPF") + "  A,  " + appVars.getTableNameOverriden("CHDRPF") + "  C" +
		" WHERE A.CHDRNUM = C.CHDRNUM" +
		" AND C.VALIDFLAG = '1'" +
		" AND A.VALIDFLAG = '1'" +
		" AND A.CHDRNUM = ?" + 
		//" AND A.NPAYDATE <= ?" +
		" ORDER BY C.CNTTYPE, A.NPAYDATE";
				sqlerrorflag = false;
				try {
					sqlzraepf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.life.anticipatedendowment.dataaccess.ZraepfTableDAM(), new com.csc.fsu.general.dataaccess.ChdrpfTableDAM()});
					sqlzraepf1ps = appVars.prepareStatementEmbeded(sqlzraepf1conn, sqlzraepf1);
					//ILAE-62 STARTS
					appVars.setDBString(sqlzraepf1ps, 1, wsaaContractNumber.toString().trim());
					//ILAE-62 ENDS
					//appVars.setDBDouble(sqlzraepf1ps, 2, wsaaEndListing.toDouble());
					sqlzraepf1rs = appVars.executeQuery(sqlzraepf1ps);
				}
				catch (SQLException ex){
					sqlca = ex;
					sqlerrorflag = true;
				}
				if (sqlerrorflag) {
					//sqlError9000();
				}
	}
	
protected void getCovtAndFormat200()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		try {			
			GetDetails100();			
			 
		}
		catch (GOTOException e){
		}
	}



protected void ReadFile()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		try
		{
	    		ReadFile2010();
	    	 
			}
			catch (GOTOException e){
				//nextMethod = (GotoLabel) e.getNextMethod();
			}
		 
	}

protected void ReadFile2010( )
	{
	/*  Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-Sep-2013
	 */
		sqlerrorflag = false;
		try {
			if (sqlzraepf1rs.next()) {
				appVars.getDBObject(sqlzraepf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlzraepf1rs, 2, sqlChdrnum);
				appVars.getDBObject(sqlzraepf1rs, 3, sqlLife);
				appVars.getDBObject(sqlzraepf1rs, 4, sqlCoverage);
				appVars.getDBObject(sqlzraepf1rs, 5, sqlRider);
				appVars.getDBObject(sqlzraepf1rs, 6, sqlPlanSfx);
				appVars.getDBObject(sqlzraepf1rs, 7, sqlZrduedte01);
				appVars.getDBObject(sqlzraepf1rs, 8, sqlZrduedte02);
				appVars.getDBObject(sqlzraepf1rs, 9, sqlZrduedte03);
				appVars.getDBObject(sqlzraepf1rs, 10, sqlZrduedte04);
				appVars.getDBObject(sqlzraepf1rs, 11, sqlZrduedte05);
				appVars.getDBObject(sqlzraepf1rs, 12, sqlZrduedte06);
				appVars.getDBObject(sqlzraepf1rs, 13, sqlZrduedte07);
				appVars.getDBObject(sqlzraepf1rs, 14, sqlZrduedte08);
				appVars.getDBObject(sqlzraepf1rs, 15, sqlPrcnt01);
				appVars.getDBObject(sqlzraepf1rs, 16, sqlPrcnt02);
				appVars.getDBObject(sqlzraepf1rs, 17, sqlPrcnt03);
				appVars.getDBObject(sqlzraepf1rs, 18, sqlPrcnt04);
				appVars.getDBObject(sqlzraepf1rs, 19, sqlPrcnt05);
				appVars.getDBObject(sqlzraepf1rs, 20, sqlPrcnt06);
				appVars.getDBObject(sqlzraepf1rs, 21, sqlPrcnt07);
				appVars.getDBObject(sqlzraepf1rs, 22, sqlPrcnt08);	
				appVars.getDBObject(sqlzraepf1rs, 23, sqlCurrFrom);
				
				
			}
			else {
				goTo(GotoLabel.exit090);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		 
	}

 protected void format500(ZonedDecimalData nCCounter,java.sql.ResultSet sqlzraepf1rs)
 {  
	 /* Comment : Added for LT-01-005
	 *  Author : Sankaran J
	 *  Created: 12-Sep-2013
	 *  02 offset   Policy Anniversary
	 	03 offset  Policy Maturity Date
	 	04 offset  Policy Maturity Amount
	 */
	  
	 
	 try
	 {
		 if ( isEQ(nCCounter,1))	
		 {
			 if ( isNE(sqlZrduedte01 ,0)  &&  isNE(sqlZrduedte01,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte01);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt01),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,2))
		 {
			 if ( isNE(sqlZrduedte02 ,0)  &&  isNE(sqlZrduedte02,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte02);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt02),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,3))
		 {
			 if ( isNE(sqlZrduedte03 ,0)  &&  isNE(sqlZrduedte03,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte03);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	  wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt03),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,4))
		 {
			 if ( isNE(sqlZrduedte04 ,0)  &&  isNE(sqlZrduedte04,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte04);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt04),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,5))
		 {
			 if ( isNE(sqlZrduedte05 ,0)  &&  isNE(sqlZrduedte05,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte05);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt05),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,6))
		 {
			 if ( isNE(sqlZrduedte06 ,0)  &&  isNE(sqlZrduedte06,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte06);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt06),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,7))
		 {
			 if ( isNE(sqlZrduedte07 ,0)  &&  isNE(sqlZrduedte07,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte07);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt07),wssaMaturityAmountpercentage));
		 }
		 else if ( isEQ(nCCounter,8))
		 {
			 if ( isNE(sqlZrduedte08 ,0)  &&  isNE(sqlZrduedte08,99999999) )
			 {
			 wsaaMaturitydate.set(sqlZrduedte08);
			 }
			 else
			 {
				 wsaaMaturitydate.set(0); 
			 }
		 	 wssaMaturityAmount.set( mult(mult(wsaaSumins,sqlPrcnt08),wssaMaturityAmountpercentage ));
		 }
		 	 
	 
			
	 }
	 catch(Exception ex)
	  {
	 
	  }
	 
 }
protected void fatalError900()
	{	
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES) || isNE(syserrrec.syserrStatuz,SPACES)) 
		{		
			syserrrec.syserrType.set("1");
		}
		else 
		{
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);		
		exitProgram();		
	}
	protected void fatalError600()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
	
	protected void sqlError9000()
	{
		/*  Comment : Added for LT-01-005
		 *  Author : Sankaran J
		 *  Created: 12-sep-2013
		 */
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		//fatalError600();
	}
}
