/*
 * File: Pcplhbxt.java
 * Date: 30 August 2009 1:01:33
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLHBXT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.regularprocessing.dataaccess.HbxtpntTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
* * - Premium Notice - *
*
*  001  Contract Currency
*  002  Contract Premium
*  003  Contract Billing Currency
*  004  Contract Billing Premium
*  005  Premium Due Date
*
***********************************************************************
* </pre>
*/
public class Pcplhbxt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLHBXT";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaAmt = new FixedLengthStringData(18);
	private FixedLengthStringData wsaaCurrency = new FixedLengthStringData(4);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (10, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String hbxtpntrec = "HBXTPNTREC";
	private static final String linsrnlrec = "LINSRNLREC";
	private HbxtpntTableDAM hbxtpntIO = new HbxtpntTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplhbxt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getHbxtpntFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		offset.set(ZERO);
		/*EXIT*/
	}

protected void getHbxtpntFormat200()
	{
		start210();
		formatContrCcy200();
		formatContrPrem200();
		formatBillCcy200();
		formatBillPrem200();
		formatDueDate200();
	}

protected void start210()
	{
		hbxtpntIO.setDataKey(SPACES);
		hbxtpntIO.setChdrcoy(letcIO.getRdoccoy());
		hbxtpntIO.setChdrnum(letcIO.getRdocnum());
		hbxtpntIO.setInstfrom(letcokcpy.ldDate);
		hbxtpntIO.setFormat(hbxtpntrec);
		hbxtpntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hbxtpntIO);
		if (isNE(hbxtpntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hbxtpntIO.getStatuz());
			syserrrec.params.set(hbxtpntIO.getParams());
			fatalError600();
		}
		linsrnlIO.setParams(SPACES);
		linsrnlIO.setChdrcoy(hbxtpntIO.getChdrcoy());
		linsrnlIO.setChdrnum(hbxtpntIO.getChdrnum());
		linsrnlIO.setInstfrom(hbxtpntIO.getInstfrom());
		linsrnlIO.setFormat(linsrnlrec);
		linsrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(linsrnlIO.getParams());
			syserrrec.statuz.set(linsrnlIO.getStatuz());
			fatalError600();
		}
	}

protected void formatContrCcy200()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		if (isNE(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
			wsaaCurrency.set(linsrnlIO.getCntcurr());
		}
		else {
			wsaaCurrency.set(hbxtpntIO.getCntcurr());
		}
		fldlen[offset.toInt()].set(length(wsaaCurrency));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrency);
	}

protected void formatContrPrem200()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
			wsaaAmount.set(linsrnlIO.getInstamt06());
		}
		else {
			wsaaAmount.set(hbxtpntIO.getInstamt06());
		}
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatBillCcy200()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("(");
			stringVariable1.addExpression(hbxtpntIO.getCntcurr());
			stringVariable1.setStringInto(wsaaCurrency);
		}
		else {
			wsaaCurrency.set(SPACES);
		}
		fldlen[offset.toInt()].set(length(wsaaCurrency));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCurrency);
	}

protected void formatBillPrem200()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		if (isNE(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
			wsaaAmount.set(hbxtpntIO.getInstamt06());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaAmount);
			stringVariable1.addExpression(")");
			stringVariable1.setStringInto(wsaaAmt);
			fldlen[offset.toInt()].set(length(wsaaAmt));
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmt);
		}
		else {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
		}
	}

protected void formatDueDate200()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		datcon1rec.intDate.set(hbxtpntIO.getPtdate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
