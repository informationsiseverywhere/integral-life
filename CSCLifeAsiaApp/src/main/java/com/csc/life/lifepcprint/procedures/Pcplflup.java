/*
 * File: Pcplflup.java
 * Date: 30 August 2009 1:01:13
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLFLUP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.Iterator;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.dataaccess.dao.FluppfDAO;
import com.csc.fsu.clients.dataaccess.model.Fluppf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.tablestructures.T3609rec;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.contractservicing.dataaccess.dao.FupepfDAO;
import com.csc.life.contractservicing.dataaccess.model.Fupepf;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.tablestructures.Th557rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  FLUPLNB Client Number
*  002  FLUPLNB Insured Life Name
*  003  FLUPLNB Address line 01
*  004  FLUPLNB Address line 02
*  005  FLUPLNB Address line 03
*  006  FLUPLNB Address line 04
*  007  FLUPLNB Address line 05
*  008  FLUPLNB Postcode.
*  009  FLUPLNB Client ID
*  010  FLUPLNB DOB
*  011  FLUPLNB SNO
*  012  FLUPLNB FLUP Code Description 1
*  013  FLUPLNB FLUP Code Description 2
*  014  FLUPLNB FLUP Code Description 3
*  015  FUPE Extended Text01
*  016  FUPE Extended Text02
*  017  FUPE Extended Text03
*  018  FUPE Extended Text04
*  019  FUPE Extended Text05
*  020  FUPE Extended Text06
*  021  FUPE Extended Text07
*  022  FUPE Extended Text08
*  023  FUPE Extended Text09
*  024  FUPE Extended Text10
*  025  FUPE Extended Text11
*  026  FUPE Extended Text12
*  027  FUPE Extended Text13
*  028  FUPE Extended Text14
*  029  FUPE Extended Text15
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Recurring Fields Retrieved.
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplflup extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLFLUP";
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(29).setUnsigned();
	private PackedDecimalData wsaaDocseq = new PackedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private ZonedDecimalData wsaaFoccCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaFocc = new FixedLengthStringData(2);
	private String wsaaValidFlup = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaTh557Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTh557Lang = new FixedLengthStringData(1).isAPartOf(wsaaTh557Key, 0);
	private FixedLengthStringData wsaaTh557Zletkey = new FixedLengthStringData(3).isAPartOf(wsaaTh557Key, 1);
	private FixedLengthStringData wsaaTh557Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaTh557Key, 4);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDatetext1 = new FixedLengthStringData(1).isAPartOf(wsaaDatetext, 0);
	private FixedLengthStringData wsaaDatetext2 = new FixedLengthStringData(21).isAPartOf(wsaaDatetext, 1);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(10000).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

		/* 03  WSAA-START-AND-LENGTH   OCCURS 200.                      */
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (400, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);

	private FixedLengthStringData wsaaFupetxt = new FixedLengthStringData(1170);
	private FixedLengthStringData[] fupetxt = FLSArrayPartOfStructure(15, 78, wsaaFupetxt, 0);
	private String wsaaFupeNotFound = "";

	private static final String liferec = "LIFEREC";
	private static final String itemrec = "ITEMREC";
	
		/* TABLES */
	private static final String t3609 = "T3609";
	private static final String t5661 = "T5661";
	private static final String th557 = "TH557";
	private ClntTableDAM clntIO = new ClntTableDAM();	
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private T3609rec t3609rec = new T3609rec();
	private T5661rec t5661rec = new T5661rec();
	private Th557rec th557rec = new Th557rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Fluppf fluppf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplflup() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		if (isNE(letcIO.getRrn(), wsaaStoreRrn)) {
			/* IF  PCPD-IDCODE-COUNT   NOT = WSAA-LAST-IDCODE-COUNT         */
			initialise100();
			/*        PERFORM 200-GET-FLUPLNB-AND-FORMAT                       */
			/*          UNTIL FLUPLNB-STATUZ  NOT = O-K                        */
			
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		/*    IF PCPD-FLD-OCCUR > WSAA-COUNTER                             */
		/*       MOVE ENDP               TO PCPD-STATUZ                    */
		/*       GO TO 090-EXIT                                            */
		/*    END-IF.                                                      */
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set("GEND");
			return ;
		}
		else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter)
			|| isGT(pcpdatarec.groupOccurance, wsaaCounter)) {
				pcpdatarec.data.set(SPACES);
				pcpdatarec.dataLen.set(1);
				pcpdatarec.statuz.set(varcom.endp);
				return ;
			}
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a costant number of positions between fields.*/
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			return ;
		}
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC)
		|| isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		if (isEQ(strpos[offset.toInt()], 0)
		|| isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return ;
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaStoreRrn.set(letcIO.getRrn());
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		wsaaDataBuffer.set(SPACES);
		offset.set(ZERO);
		wsaaCounter.set(ZERO);
		wsaaFoccCount.set(ZERO);		
		FluppfDAO fluppfDAO = getApplicationContext().getBean("flupDAO",FluppfDAO.class);
		List<Fluppf> data = fluppfDAO.searchFlupRecordByChdrNum(letcIO.getRdoccoy().toString(),letcIO.getRdocnum().toString(),letcokcpy.lfLife.toString(),letcokcpy.lfJlife.toString());
		Iterator<Fluppf> it = data.iterator();
		while(it.hasNext())
		{
			fluppf = it.next();
			getFluplnbAndFormat200();
		}
	}

protected void getFluplnbAndFormat200()
	{
		try {
			getData200();
			format201();
			format202();
			format203();
			format204();
			format205();
			format206();
			format207();
			format208();
			format210();
			format211();
			format212();
			format213();
			format215();
			format216();
			format217();
			format218();
			format219();
			format220();
			format221();
			format222();
			format223();
			format224();
			format225();
			format226();
			format227();
			format228();
			format229();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		/* Read each Follow Up for this contract and output any extra*/
		/* details which may be present.*/		
		/*    IF FLUPLNB-STATUZ        NOT = O-K        AND                */
		/*       FLUPLNB-STATUZ        NOT = ENDP                          */
		/*    IF FLUPLNB-STATUZ        NOT = O-K                   <V72L11>*/
		/*       MOVE FLUPLNB-STATUZ      TO SYSR-STATUZ                   */
		/*       MOVE FLUPLNB-PARAMS      TO SYSR-PARAMS                   */
		/*       PERFORM 600-FATAL-ERROR                                   */
		/*    END-IF.                                                      */		
		/* Read LIFE*/
		lifeIO.setChdrcoy(fluppf.getChdrcoy());
		lifeIO.setChdrnum(fluppf.getChdrnum());
		lifeIO.setLife(fluppf.getLife());
		lifeIO.setJlife(fluppf.getjLife());
		lifeIO.setCurrfrom(varcom.vrcmMaxDate);
		lifeIO.setFunction(varcom.begn);
		lifeIO.setFormat(liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getChdrcoy(), fluppf.getChdrcoy())
		|| isNE(lifeIO.getChdrnum(), fluppf.getChdrnum())
		|| isNE(lifeIO.getLife(), fluppf.getLife())
		|| isNE(lifeIO.getJlife(), fluppf.getjLife())
		|| isEQ(lifeIO.getStatuz(), varcom.endp)) {
			lifeIO.setStatuz(varcom.endp);
		}
		if (isNE(lifeIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(lifeIO.getStatuz());
			syserrrec.params.set(lifeIO.getParams());
			fatalError600();
		}
		/* Read CLNT*/
		clntIO.setParams(SPACES);
		clntIO.setClntpfx(fsupfxcpy.clnt);
		clntIO.setClntcoy(letcIO.getClntcoy());
		clntIO.setClntnum(lifeIO.getLifcnum());
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clntIO.getStatuz());
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
		getFollowUpExtendedtxt250();
		getFollowUpDesc300();
		if (isEQ(wsaaValidFlup, "N")) {
			goTo(GotoLabel.exit299);
		}
		wsaaCounter.add(1);
	}

protected void format201()
	{
		/*    FLUPLNB Client Number*/
		offset.add(1);
		if (isEQ(offset, 1)) {
			strpos[offset.toInt()].set(offset);
		}
		else {
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		}
		fldlen[offset.toInt()].set(length(lifeIO.getLifcnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], lifeIO.getLifcnum());
	}

protected void format202()
	{
		/*    Life name*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.clntNumber.set(lifeIO.getLifcnum());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void format203()
	{
		/*    Address1*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr1, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr1);
	}

protected void format204()
	{
		/*    Address2*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr2, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr2);
	}

protected void format205()
	{
		/*    Address3*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr3, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr3);
	}

protected void format206()
	{
		/*    Address4*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr4, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr4);
	}

protected void format207()
	{
		/*    Address5*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.addr5, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.addr5);
	}

protected void format208()
	{
		/*    Post Code*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(namadrsrec.pcode));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], namadrsrec.pcode);
		/*FORMAT*/
		/* Client ID*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(clntIO.getSecuityno()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], clntIO.getSecuityno());
	}

protected void format210()
	{
		/*    Client's DOC*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		datcon6rec.language.set(pcpdatarec.language);
		datcon6rec.company.set(letcIO.getRequestCompany());
		datcon6rec.intDate1.set(clntIO.getCltdob());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz, varcom.oK)) {
			datcon6rec.intDate2.set(SPACES);
		}
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetext1, SPACES)) {
			wsaaDatetext.set(wsaaDatetext2);
		}
		fldlen[offset.toInt()].set(length(wsaaDatetext));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaDatetext);
	}

protected void format211()
	{
		/* Follow Up SNO*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		if (isEQ(th557rec.th557Rec, SPACES)) {
			wsaaFocc.set(SPACES);
		}
		else {
			wsaaFoccCount.add(1);
			wsaaFocc.set(wsaaFoccCount);
		}
		fldlen[offset.toInt()].set(length(wsaaFocc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaFocc);
		/*211-FORMAT.                                                      */
		/* Follow Up Code Desc 1*/
		/* Read T5661 for the Follow Up code.*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(th557rec.zflpxtra01, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], th557rec.zflpxtra01);
	}

protected void format212()
	{
		/* Follow Up Code Desc 2*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(th557rec.zflpxtra02, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], th557rec.zflpxtra02);
	}

protected void format213()
	{
		/* Follow Up Code Desc 3*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(th557rec.zflpxtra03, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], th557rec.zflpxtra03);
	}

protected void format215()
	{
		/* Follow-up extended text 01                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[1], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[1]);
	}

protected void format216()
	{
		/* Follow-up extended text 02                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[2], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[2]);
	}

protected void format217()
	{
		/* Follow-up extended text 03                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[3], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[3]);
	}

protected void format218()
	{
		/* Follow-up extended text 04                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[4], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[4]);
	}

protected void format219()
	{
		/* Follow-up extended text 05                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[5], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[5]);
	}

protected void format220()
	{
		/* Follow-up extended text 06                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[6], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[6]);
	}

protected void format221()
	{
		/* Follow-up extended text 07                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[7], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[7]);
	}

protected void format222()
	{
		/* Follow-up extended text 08                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[8], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[8]);
	}

protected void format223()
	{
		/* Follow-up extended text 09                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[9], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[9]);
	}

protected void format224()
	{
		/* Follow-up extended text 10                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[10], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[10]);
	}

protected void format225()
	{
		/* Follow-up extended text 11                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[11], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[11]);
	}

protected void format226()
	{
		/* Follow-up extended text 12                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[12], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[12]);
	}

protected void format227()
	{
		/* Follow-up extended text 13                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[13], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[13]);
	}

protected void format228()
	{
		/* Follow-up extended text 14                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[14], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[14]);
	}

protected void format229()
	{
		/* Follow-up extended text 15                                      */
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(fupetxt[15], " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], fupetxt[15]);
	}

protected void getFollowUpExtendedtxt250()
	{
		getFollowUpExtendedtext251();
	}

protected void getFollowUpExtendedtext251()
	{
		wsaaFupetxt.set(SPACES);
		Fupepf fupepf = new Fupepf();
		fupepf.setChdrcoy(fluppf.getChdrcoy().toString());
		fupepf.setChdrnum(fluppf.getChdrnum());
		fupepf.setFupno(fluppf.getFupNo());
		fupepf.setTranno(fluppf.getTranNo());
		fupepf.setClamnum(fluppf.getClamNum());
		FupepfDAO fupepfDAO = getApplicationContext().getBean("fupepfDAO",FupepfDAO.class);
		List<Fupepf> data = fupepfDAO.readRecord(fupepf);
		Iterator<Fupepf> it = data.iterator();		
		wsaaDocseq.set(0);
		wsaaFupeNotFound = data.isEmpty() ? "Y" : "N"; 
		while (it.hasNext()) {
			fupepf = it.next();
			wsaaDocseq.add(1);
			fupetxt[wsaaDocseq.toInt()].set(fupepf.getMessage());
		}
		
		while ( !(isGT(wsaaDocseq, 14))) {
			wsaaDocseq.add(1);
			fupetxt[wsaaDocseq.toInt()].set(SPACES);
		}
		
	}

protected void getFollowUpDesc300()
	{
		followUpDesc301();
	}

protected void followUpDesc301()
	{
		th557rec.th557Rec.set(SPACES);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRequestCompany());
		itemIO.setItemtabl(t5661);
		/*    MOVE FLUPLNB-FUPCODE        TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(pcpdatarec.language);
		wsaaT5661Fupcode.set(fluppf.getFupCde());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			t5661rec.t5661Rec.set(SPACES);
		}
		else {
			t5661rec.t5661Rec.set(itemIO.getGenarea());
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)
		&& isEQ(wsaaFupeNotFound, "Y")) {
			wsaaDocseq.set(1);
			while ( !(isGT(wsaaDocseq, 15))) {
				if (isLT(wsaaDocseq, 6)) {
					fupetxt[wsaaDocseq.toInt()].set(t5661rec.message[wsaaDocseq.toInt()]);
				}
				else {
					fupetxt[wsaaDocseq.toInt()].set(SPACES);
				}
				wsaaDocseq.add(1);
			}
			
		}
		/* Ensure Follow Up code is valid for this Letter Type and*/
		/* is still outstanding before retrieving Extra details.*/
		wsaaValidFlup = "Y";
		if ((isNE(t5661rec.zlettype, letcIO.getLetterType())
		&& isNE(t5661rec.zlettyper, letcIO.getLetterType()))
		|| (isEQ(t5661rec.zlettype,t5661rec.zlettyper)
		&& (isNE(fluppf.getFupSts(),t5661rec.zletstat)	
		&& isNE(fluppf.getFupSts(),t5661rec.zletstatr)))
		|| (isNE(t5661rec.zlettype,t5661rec.zlettyper) &&((isEQ(t5661rec.zlettype, letcIO.getLetterType())
		&& isNE(fluppf.getFupSts(), t5661rec.zletstat))
		|| (isEQ(t5661rec.zlettyper, letcIO.getLetterType())
		&& isNE(fluppf.getFupSts(), t5661rec.zletstatr))))) {
			wsaaValidFlup = "N";
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)
		|| isEQ(wsaaValidFlup, "N")); wsaaSub.add(1)){
			if (isEQ(fluppf.getFupSts(), t5661rec.fuposs[wsaaSub.toInt()])) {
				wsaaValidFlup = "N";
			}
		}
		if (isEQ(wsaaValidFlup, "N")) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRequestCompany());
		itemIO.setItemtabl(t3609);
		itemIO.setItemitem(letcIO.getLetterType());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			t3609rec.t3609Rec.set(SPACES);
		}
		else {
			t3609rec.t3609Rec.set(itemIO.getGenarea());
		}
		if (isEQ(t3609rec.zletkey, SPACES)) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcIO.getRequestCompany());
		itemIO.setItemtabl(th557);
		wsaaTh557Lang.set(pcpdatarec.language);
		wsaaTh557Zletkey.set(t3609rec.zletkey);
		wsaaTh557Fupcode.set(fluppf.getFupCde());
		itemIO.setItemitem(wsaaTh557Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			th557rec.th557Rec.set(SPACES);
		}
		else {
			th557rec.th557Rec.set(itemIO.getGenarea());
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}

}
