/*
 * File: Pcplhcsd.java
 * Date: 30 August 2009 1:01:36
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLHCSD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Previous Dividend Option
*   02    Previous Dividend Description
*   03    New Dividend Option
*   04    New Dividend Description
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Pcplhcsd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLHCSD  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaNumberOfRecs = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(5, 0).init(4);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* TABLES */
	private static final String th500 = "TH500";
		/* FORMATS */
	private static final String hcsdrec = "HCSDREC";
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplhcsd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		/*    If the LETC-RRN is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(letcIO.getRrn(), wsaaStoreRrn)) {
			initialise100();
			while ( !(isEQ(hcsdIO.getStatuz(), varcom.endp))) {
				getHcsdAndFormat200();
			}
			
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isGT(pcpdatarec.fldOccur, wsaaNumberOfRecs)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaStoreRrn.set(letcIO.getRrn());
		wsaaNumberOfRecs.set(ZERO);
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaLetokeys.set(letcIO.getOtherKeys());
		hcsdIO.setParams(SPACES);
		hcsdIO.setChdrcoy(letcIO.getRdoccoy());
		hcsdIO.setChdrnum(letcIO.getRdocnum());
		hcsdIO.setPlanSuffix(0);
		hcsdIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hcsdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hcsdIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		hcsdIO.setFormat(hcsdrec);
	}

protected void getHcsdAndFormat200()
	{
		try {
			getData200();
			format205();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, hcsdIO);
		/*    IF HCSD-STATUZ               NOT = O-K                       */
		if (isNE(hcsdIO.getStatuz(), varcom.oK)
		&& isNE(hcsdIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hcsdIO.getParams());
			fatalError600();
		}
		if (isEQ(hcsdIO.getStatuz(), varcom.endp)
		|| isNE(hcsdIO.getChdrcoy(), letcIO.getRdoccoy())
		|| isNE(hcsdIO.getChdrnum(), letcIO.getRdocnum())) {
			hcsdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		if (isNE(hcsdIO.getRider(), "00")) {
			hcsdIO.setFunction(varcom.nextr);
			goTo(GotoLabel.exit299);
		}
		/* Get new dividend option*/
		if (isEQ(hcsdIO.getTranno(), letcIO.getTranno())
		|| isEQ(hcsdIO.getTranno(), wsaaOkeyTranno)) {
			getDivoptDesc300();
		}
		else {
			hcsdIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit299);
		}
		wsaaNumberOfRecs.add(1);
	}

protected void format205()
	{
		/*  Dividend Option*/
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(hcsdIO.getZdivopt(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(hcsdIO.getZdivopt(), 1, fldlen[offset.toInt()]));
		/*  Dividend Description*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(descIO.getLongdesc(), " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()], ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(descIO.getLongdesc(), 1, fldlen[offset.toInt()]));
		hcsdIO.setFunction(varcom.nextr);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
	}

protected void getDivoptDesc300()
	{
		start3210();
	}

protected void start3210()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(hcsdIO.getChdrcoy());
		descIO.setDesctabl(th500);
		descIO.setDescitem(hcsdIO.getZdivopt());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
