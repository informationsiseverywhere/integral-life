/*
 * File: Pcpllext.java
 * Date: 30 August 2009 1:02:07
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLEXT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Adjustment Period
*
*
***********************************************************************
* </pre>
*/
public class Pcpllext extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLLEXT ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaTerm = new FixedLengthStringData(3);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (10, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String lextrec = "LEXTREC";
	private LetcTableDAM letcIO = new LetcTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcpllext() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getLextAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		para101();
	}

protected void para101()
	{
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		lextIO.setParams(SPACES);
		lextIO.setStatuz(varcom.oK);
		if (letcokcpy.lifeFollowup.isTrue()) {
			lextIO.setChdrcoy(letcokcpy.lfChdrcoy);
			lextIO.setChdrnum(letcokcpy.lfChdrnum);
			lextIO.setLife(letcokcpy.lfLife);
		}
		lextIO.setSeqnbr(ZERO);
		lextIO.setFunction(varcom.begn);
		lextIO.setFormat(lextrec);
	}

protected void getLextAndFormat200()
	{
		getData200();
		format200();
	}

protected void getData200()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getChdrcoy(),letcokcpy.lfChdrcoy)
		|| isNE(lextIO.getChdrnum(),letcokcpy.lfChdrnum)
		|| isNE(lextIO.getLife(),letcokcpy.lfLife)
		|| isEQ(lextIO.getStatuz(),varcom.endp)) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isNE(lextIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(lextIO.getStatuz());
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
	}

protected void format200()
	{
		/* LEXT Adjustment Term*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaTerm.set(lextIO.getExtCessTerm());
		if (isEQ(subString(wsaaTerm, 1, 1),ZERO)) {
			wsaaTerm.setSub1String(1, 1, SPACES);
		}
		if (isEQ(subString(wsaaTerm, 2, 1),ZERO)) {
			wsaaTerm.setSub1String(2, 1, SPACES);
		}
		if (isEQ(subString(wsaaTerm, 3, 1),ZERO)) {
			wsaaTerm.setSub1String(3, 1, SPACES);
		}
		fldlen[offset.toInt()].set(length(wsaaTerm));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaTerm);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
