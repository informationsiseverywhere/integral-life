/*
 * File: Pcplbonl.java
 * Date: 30 August 2009 1:00:18
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLBONL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.regularprocessing.dataaccess.BonlrvbTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Bonus Date From          -> LBONDFR     }
*  002  Bonus Date To            -> LBONDTO     }
*  003  Bonus Long Decription 1  -> LBONLD01    }
*  004  Bonus Long Decription 2  -> LBONLD02    }
*  005  Bonus Long Decription 3  -> LBONLD03    }
*  006  Bonus Long Decription 4  -> LBONLD04    }
*  007  Bonus Long Decription 5  -> LBONLD05    }
*  008  Bonus Long Decription 6  -> LBONLD06    }
*  009  Bonus Long Decription 7  -> LBONLD07    }
*  010  Bonus Long Decription 8  -> LBONLD08    }
*  011  Bonus Long Decription 9  -> LBONLD09    }
*  012  Bonus Long Decription 10 -> LBONLD10    }
*  013  Bonus Sum Insured 1      -> LBONSI01    }
*  014  Bonus Sum Insured 2      -> LBONSI02    }
*  015  Bonus Sum Insured 3      -> LBONSI03    }
*  016  Bonus Sum Insured 4      -> LBONSI04    }
*  017  Bonus Sum Insured 5      -> LBONSI05    }
*  018  Bonus Sum Insured 6      -> LBONSI06    }
*  019  Bonus Sum Insured 7      -> LBONSI07    }
*  020  Bonus Sum Insured 8      -> LBONSI08    }
*  021  Bonus Sum Insured 9      -> LBONSI09    }
*  022  Bonus Sum Insured 10     -> LBONSI10    }
*  023  Bonus Calc Method 1      -> LBONCM01    }
*  024  Bonus Calc Method 2      -> LBONCM02    }
*  025  Bonus Calc Method 3      -> LBONCM03    }
*  026  Bonus Calc Method 4      -> LBONCM04    }
*  027  Bonus Calc Method 5      -> LBONCM05    }
*  028  Bonus Calc Method 6      -> LBONCM06    }
*  029  Bonus Calc Method 7      -> LBONCM07    }
*  030  Bonus Calc Method 8      -> LBONCM08    }
*  031  Bonus Calc Method 9      -> LBONCM09    }
*  032  Bonus Calc Method 10     -> LBONCM10    }
*  033  Bonus Pay This Year 1    -> LBONPT01    }
*  034  Bonus Pay This Year 2    -> LBONPT02    }
*  035  Bonus Pay This Year 3    -> LBONPT03    }
*  036  Bonus Pay This Year 4    -> LBONPT04    }
*  037  Bonus Pay This Year 5    -> LBONPT05    }
*  038  Bonus Pay This Year 6    -> LBONPT06    }
*  039  Bonus Pay This Year 7    -> LBONPT07    }
*  040  Bonus Pay This Year 8    -> LBONPT08    }
*  041  Bonus Pay This Year 9    -> LBONPT09    }
*  042  Bonus Pay This Year 10   -> LBONPT10    }
*  043  Bonus Pay Last Year 1    -> LBONPL01    }
*  044  Bonus Pay Last Year 2    -> LBONPL02    }
*  045  Bonus Pay Last Year 3    -> LBONPL03    }
*  046  Bonus Pay Last Year 4    -> LBONPL04    }
*  047  Bonus Pay Last Year 5    -> LBONPL05    }
*  048  Bonus Pay Last Year 6    -> LBONPL06    }
*  049  Bonus Pay Last Year 7    -> LBONPL07    }
*  050  Bonus Pay Last Year 8    -> LBONPL08    }
*  051  Bonus Pay Last Year 9    -> LBONPL09    }
*  052  Bonus Pay Last Year 10   -> LBONPL10    }
*  053  Total Bonus 1            -> LTOTBN01    }
*  054  Total Bonus 2            -> LTOTBN02    }
*  055  Total Bonus 3            -> LTOTBN03    }
*  056  Total Bonus 4            -> LTOTBN04    }
*  057  Total Bonus 5            -> LTOTBN05    }
*  058  Total Bonus 6            -> LTOTBN06    }
*  059  Total Bonus 7            -> LTOTBN07    }
*  060  Total Bonus 8            -> LTOTBN08    }
*  061  Total Bonus 9            -> LTOTBN09    }
*  062  Total Bonus 10           -> LTOTBN10    }
*  063  Bonus Date Text          -> LBONDATX    }
*  064  Total SI                 -> LBONTSI     }
*  065  Total Bonus Paid Last Yr -> LBONTPLY    }
*  066  Total Bonus Paid This Yr -> LBONTPTY    }
*  067  Final Total Bonus        -> LTTOTBNS    }
*
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcplbonl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLBONL  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAmountX = new ZonedDecimalData(17, 2).setPattern("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99-");
	private ZonedDecimalData wsaaAmount1 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaAmountZ = new ZonedDecimalData(18, 2).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99-");
	private ZonedDecimalData wsaaTotSi = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotPty = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotPly = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotBonus = new ZonedDecimalData(18, 2);
		/*  Make this field as large as you think the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1500).init(SPACES);
		/* The following array is configured to store up to 3 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (70, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private static final String bonlrvbrec = "BONLRVBREC";
	private BonlrvbTableDAM bonlrvbIO = new BonlrvbTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcplbonl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getBonusDetails200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		wsaaTotSi.set(ZERO);
		wsaaTotPty.set(ZERO);
		wsaaTotPly.set(ZERO);
		wsaaTotBonus.set(ZERO);
		/*EXIT*/
	}

protected void getBonusDetails200()
	{
		try {
			start201();
			format202();
			format203();
			format204();
			format206();
			format208();
			format210();
			format212();
			format214();
			format215();
			format216();
			format217();
			format218();
			format219();
			format220();
			format221();
			format222();
			format223();
			format224();
			format226();
			format228();
			format230();
			format232();
			format234();
			format235();
			format236();
			format237();
			format238();
			format239();
			format240();
			format241();
			format242();
			format243();
			format244();
			format245();
			format246();
			format247();
			format248();
			format249();
			format250();
			format251();
			format252();
			format253();
			format254();
			format255();
			format256();
			format257();
			format258();
			format259();
			format260();
			format261();
			format262();
			format263();
			format264();
			formatTotalPly264();
			formatTotalPty264();
			formatTotalBonus264();
		}
		catch (GOTOException e){
		}
	}

protected void start201()
	{
		bonlrvbIO.setDataArea(SPACES);
		bonlrvbIO.setStatuz(varcom.oK);
		bonlrvbIO.setChdrcoy(letcIO.getChdrcoy());
		bonlrvbIO.setChdrnum(letcIO.getChdrnum());
		bonlrvbIO.setFunction(varcom.readr);
		callBonlrvbio300();
	}

protected void format202()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(bonlrvbIO.getCurrfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format203()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(bonlrvbIO.getCurrto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			goTo(GotoLabel.exit299);
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void format204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc01()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc01());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc02()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc02());
	}

protected void format206()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc03()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc03());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc04()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc04());
	}

protected void format208()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc05()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc05());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc06()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc06());
	}

protected void format210()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc07()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc07());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc08()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc08());
	}

protected void format212()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc09()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc09());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getLongdesc10()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getLongdesc09());
	}

protected void format214()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins01());
		wsaaAmount.set(bonlrvbIO.getSumins01());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format215()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins02());
		wsaaAmount.set(bonlrvbIO.getSumins02());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format216()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins03());
		wsaaAmount.set(bonlrvbIO.getSumins03());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format217()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins04());
		wsaaAmount.set(bonlrvbIO.getSumins04());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format218()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins05());
		wsaaAmount.set(bonlrvbIO.getSumins05());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format219()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins06());
		wsaaAmount.set(bonlrvbIO.getSumins06());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins07());
		wsaaAmount.set(bonlrvbIO.getSumins07());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format221()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins08());
		wsaaAmount.set(bonlrvbIO.getSumins08());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format222()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins09());
		wsaaAmount.set(bonlrvbIO.getSumins09());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format223()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotSi.add(bonlrvbIO.getSumins10());
		wsaaAmount.set(bonlrvbIO.getSumins10());
		wsaaAmountX.set(wsaaAmount);
		fldlen[offset.toInt()].set(length(wsaaAmountX));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountX);
	}

protected void format224()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth01()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth01());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth02()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth02());
	}

protected void format226()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth03()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth03());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth04()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth04());
	}

protected void format228()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth05()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth05());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth06()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth06());
	}

protected void format230()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth07()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth07());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth08()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth08());
	}

protected void format232()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth09()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth09());
		/*FORMAT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getBonusCalcMeth10()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getBonusCalcMeth10());
	}

protected void format234()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr01());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr01());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format235()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr02());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr02());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format236()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr03());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr03());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format237()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr04());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr04());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format238()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr05());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr05());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format239()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr06());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr06());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format240()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr07());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr07());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format241()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr08());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr08());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format242()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr09());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr09());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format243()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPty.add(bonlrvbIO.getBonPayThisYr10());
		wsaaAmount1.set(bonlrvbIO.getBonPayThisYr10());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format244()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr01());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr01());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format245()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr02());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr02());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format246()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr03());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr03());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format247()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr04());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr04());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format248()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr05());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr05());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format249()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr06());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr06());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format250()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr07());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr07());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format251()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr08());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr08());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format252()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr09());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr09());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format253()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotPly.add(bonlrvbIO.getBonPayLastYr10());
		wsaaAmount1.set(bonlrvbIO.getBonPayLastYr10());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format254()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus01());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus01());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format255()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus02());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus02());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format256()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus03());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus03());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format257()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus04());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus04());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format258()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus05());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus05());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format259()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus06());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus06());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format260()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus07());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus07());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format261()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus08());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus08());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format262()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus09());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus09());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format263()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotBonus.add(bonlrvbIO.getTotalBonus10());
		wsaaAmount1.set(bonlrvbIO.getTotalBonus10());
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void format264()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(bonlrvbIO.getDatetexc()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], bonlrvbIO.getDatetexc());
		/*FORMAT-TOTAL-SI*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount1.set(wsaaTotSi);
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void formatTotalPly264()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount1.set(wsaaTotPly);
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void formatTotalPty264()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount1.set(wsaaTotPty);
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void formatTotalBonus264()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount1.set(wsaaTotBonus);
		wsaaAmountZ.set(wsaaAmount1);
		fldlen[offset.toInt()].set(length(wsaaAmountZ));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmountZ);
	}

protected void callBonlrvbio300()
	{
		/*CALL*/
		bonlrvbIO.setFormat(bonlrvbrec);
		SmartFileCode.execute(appVars, bonlrvbIO);
		if (isNE(bonlrvbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bonlrvbIO.getParams());
			fatalError900();
		}
		/*EXIT*/
	}

protected void fatalError900()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
