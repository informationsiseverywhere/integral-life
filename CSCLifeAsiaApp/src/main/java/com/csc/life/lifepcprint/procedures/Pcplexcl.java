/*
 * File: Pcplexcl.java
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.terminationclaims.tablestructures.Tr5agrec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2002.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
Exclusion Details 
*
* Offset. Description............................................. ....
* 002 Coverage Name
* 003 Coverage Description
* 004 Exclusion Code	
* 005 Exclusion Description/Text
*   
*
****************************************************************** ****
* </pre>
*/
public class Pcplexcl extends SMARTCodeModel {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(Pcplexcl.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLEXCL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(100000).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (340, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private LetcTableDAM letcIO = new LetcTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private PackedDecimalData wsaaCounter = new PackedDecimalData(3, 0);
	private static final String covrrec = "COVRREC";
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(3).setUnsigned();
	private Exclpf exclpf=null;
	private String crtable="";
	private String longdesc="";
	private String exclcode="";
	private String excltxt="";
	private String predeftxt="";
	private Tr5agrec tr5agrec = new Tr5agrec();
	List<Exclpf> exclpflist = new LinkedList<Exclpf>();
	private FixedLengthStringData wsaaCover = new FixedLengthStringData(30).init(SPACES);

	private List <Descpf> t5687List=new ArrayList<Descpf>();
	public Pcplexcl() {
		super();
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		
		if (isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			while (!(isEQ(covrIO.getStatuz(), varcom.endp))) {
				getDataAndFormat200();
			}

		}
		if (isEQ(wsaaCounter, ZERO)) {
			pcpdatarec.statuz.set(varcom.oK);
			return;
		} else {
			if (isGT(pcpdatarec.fldOccur, wsaaCounter) || isGT(pcpdatarec.groupOccurance, wsaaCounter)) {
				pcpdatarec.statuz.set(varcom.endp);
				return;
			}
		}
		
		if (isEQ(pcpdatarec.fldOffset, 1)) {
			offset.set(pcpdatarec.fldOffset);
		} else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset, (mult((sub(pcpdatarec.fldOccur, 1)), wsaaConstant))));
		}
		if (isNE(strpos[offset.toInt()], NUMERIC) || isNE(fldlen[offset.toInt()], NUMERIC)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return;
		}
		if (isEQ(strpos[offset.toInt()], 0) || isEQ(fldlen[offset.toInt()], 0)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.dataLen.set(1);
			pcpdatarec.statuz.set(varcom.oK);
			return;
		}
		
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);

	}
protected void initialise100()
	{
		for (offset.set(1); !(isGT(offset, 100)); offset.add(1)){
			wsaaStartAndLength[offset.toInt()].set(ZERO);
		}
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		if (isNE(pcpdatarec.groupOccurance, NUMERIC)) {
			pcpdatarec.groupOccurance.set(1);
		}
		wsaaCounter.set(ZERO);
		offset.set(1);
		strpos[offset.toInt()].set(1);	
		wsaaCover.set(SPACES);
		fldlen[offset.toInt()].set(length(wsaaCover));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaCover);
		covrIO.setParams(SPACES);
		covrIO.setFunction(varcom.begn);
	}

protected void getDataAndFormat200()
	{
		try {
			getData200();
		
		}
		catch(Exception e)
		{
			LOGGER.error("Exception occured in getDataAndFomat200()",e);
		}
	
	}

protected void getData200()
	{
	covrIO.setStatuz(varcom.oK);
	covrIO.setChdrcoy(letcIO.getRdoccoy());
	covrIO.setChdrnum(letcIO.getRdocnum());
	covrIO.setLife("01");
	covrIO.setCoverage(SPACES);
	covrIO.setRider(SPACES);
	covrIO.setPlanSuffix(ZERO);
	covrIO.setFormat(covrrec);
	SmartFileCode.execute(appVars, covrIO);
	if (isNE(covrIO.getStatuz(), varcom.oK)
	&& isNE(covrIO.getStatuz(), varcom.endp)) {
		syserrrec.params.set(covrIO.getParams());
		fatalError600();
	}
	covrIO.setFunction(varcom.nextr);
	if (isEQ(covrIO.getStatuz(), varcom.endp)
	|| isNE(covrIO.getChdrcoy(), letcIO.getRdoccoy())
	|| isNE(covrIO.getChdrnum(), letcIO.getRdocnum())) {
		covrIO.setStatuz(varcom.endp);
		return;
	}
	if (isNE(covrIO.getValidflag(), "1")) {
		return;
	}

	exclpflist=exclpfDAO.getExclpfRecord(letcIO.getChdrnum().toString(),covrIO.getCrtable().toString(),covrIO.getLife().toString(),covrIO.getCoverage().toString(),covrIO.getRider().toString());
	getT5687Desc();
	crtable=covrIO.getCrtable().toString();
	 for (int i = 0; i < exclpflist.size(); i++) 
	{
		exclpf = exclpflist.get(i);	
		exclcode=exclpf.getExcda().trim();
		excltxt=exclpf.getExadtxt().trim();	
		readItemTabl();
		predeftxt= tr5agrec.tr5agRec.toString();
		formatCovrDetails();
		formatExclCode();
		formatExclDesc();		
		longdesc = "";
		crtable="";
		wsaaCounter.add(1);	
	}


	}
protected void getT5687Desc()
{
	
	t5687List = descDAO.getItemByDescItem("IT", covrIO.getChdrcoy().toString(), "t5687",covrIO.getCrtable().toString(),pcpdatarec.language.toString());		
	for (Descpf descItem : t5687List) {
		if (descItem.getDescitem().trim().equals(covrIO.getCrtable().toString().trim())){
			longdesc= descItem.getLongdesc();
			break;
		}
	}
}
protected void readItemTabl()	
{
	itemIO.setDataKey(SPACES);
	itemIO.setItempfx("IT");
	itemIO.setItemitem(exclcode);
	itemIO.setItemcoy(covrIO.getChdrcoy().toString());
	itemIO.setItemtabl("TR5AG");
	itemIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(), varcom.oK)
	&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();			
	}
	tr5agrec.tr5agRec.set(itemIO.getGenarea());
}
protected void formatCovrDetails()
	{
	
	//Coverage Type and Coverage Description
		
		StringBuilder result = new StringBuilder(crtable).append(" ").append(longdesc);
		offset.add(1);  //offset=2
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(result.toString()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], result.toString().trim());
		
	
		
	}

protected void formatExclCode()
	{
	//Exclusion Code
		offset.add(1);  //offset=3
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(exclcode));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], exclcode);
	}

protected void formatExclDesc()
	{
	//Exclusion Predefined and additional text
		offset.add(1);  //offset=4
		StringBuilder result = new StringBuilder(tr5agrec.tr5agRec.toString().trim()).append(System.getProperty("line.separator")).append(excltxt);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset, 1).toInt()], fldlen[sub(offset, 1).toInt()]));
		fldlen[offset.toInt()].set(length(result.toString()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], result.toString().trim());
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);	
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz, SPACES)
		|| isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
