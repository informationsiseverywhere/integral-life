/*
 * File: Pcpljlife.java
 * Date: 30 August 2009 1:01:55
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLJLIFE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List; //ILIFE-8709

import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ILIFE-8709
import com.csc.life.productdefinition.dataaccess.model.Lifepf; //ILIFE-8709
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*  001  Joint Life name
*  002  Joint Life DOB
*
*  Overview.
*  ---------
*  IMPORTANT NOTE:
*  WSAA-CONSTANT must be = Number of Fields Retrieved.
*
*
*  Processing.
*  -----------
*
***********************************************************************
* </pre>
*/
public class Pcpljlife extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLJLIFE ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private ZonedDecimalData wsaaConstant = new ZonedDecimalData(2, 0).init(2).setUnsigned();
	private String wsaaFirstTime = "Y";
	private PackedDecimalData wsaaCounter = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (20, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private ClntTableDAM clntIO = new ClntTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
 //ILIFE-8709 start	
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
 //ILIFE-8709 end

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcpljlife() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
 //ILIFE-8709 start
			List<Lifepf> lifepfList;
			lifepfList = lifepfDAO.getLifeList(letcIO.getRdoccoy().toString(), letcIO.getRdocnum().toString());
			
			for (Lifepf lifepf : lifepfList) {
				if(isEQ(lifepf.getLife(),"01")) {
					getLifeAndFormat200(lifepf);
				}
			}
 //ILIFE-8709 end			
		}
		if (isGT(pcpdatarec.fldOccur,wsaaCounter)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		wsaaFirstTime = "Y";
		wsaaCounter.set(ZERO);
		/*EXIT*/
	}

protected void getLifeAndFormat200(Lifepf lifepf)
	{
		try {
			getData200(lifepf);
			format205(lifepf);
			format210();
		}
		catch (GOTOException e){
		}
	}

protected void getData200(Lifepf lifepf)
	{
		if (isEQ(lifepf.getJlife(),SPACES)
		|| isEQ(lifepf.getJlife(),"00")) {
			if (isEQ(wsaaFirstTime,"Y")) {
				offset.set(1);
				strpos[offset.toInt()].set(1);
				wsaaFirstTime = "N";
			}
			else {
				offset.add(1);
				compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
			}
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
			goTo(GotoLabel.exit299);
		}
		wsaaCounter.add(1);
		clntIO.setClntpfx("CN");
		clntIO.setClntcoy(letcIO.getClntcoy());
		clntIO.setClntnum(lifepf.getLifcnum()); //ILIFE-8709
		clntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clntIO);
		if (isNE(clntIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(clntIO.getParams());
			fatalError600();
		}
	}

protected void format205(Lifepf lifepf)
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			offset.set(1);
			strpos[offset.toInt()].set(1);
			wsaaFirstTime = "N";
		}
		else {
			offset.add(1);
			compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(letcIO.getClntcoy());
		namadrsrec.clntNumber.set(lifepf.getLifcnum()); //ILIFE-8709
		namadrsrec.function.set("ADRSE");
		namadrsrec.language.set(pcpdatarec.language);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void format210()
	{
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(clntIO.getCltdob());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			return ;
		}
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(datcon1rec.extDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
