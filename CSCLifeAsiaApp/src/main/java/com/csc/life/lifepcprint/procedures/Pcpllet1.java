/*
 * File: Pcpllet1.java
 * Date: 30 August 2009 1:01:59
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLLET1.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.dataaccess.LettpfTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    Total Amount
*   02    Print Date
*   03    Bank Description
*   04    Direct Debit Date
*   05    ** Not used **
*   06    ** Not used **
*   07    Sequence Number
*   08    Bank Key
*   09    Mandate Reference
*   10    Amount
*   11    Name
*   12    Surname
*
*  Overview.
*  ---------
*  This will get the required information from LETT for printing
*  of Direct Debits and Credits Letter. LETT records are created
*  during the Direct Debits or Credits run.
*
*  Processing.
*  -----------
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcpllet1 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private LettpfTableDAM lettpf = new LettpfTableDAM();
	private LettpfTableDAM lettpfRec = new LettpfTableDAM();
	private final String wsaaSubrname = "PCPLLET1  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaTotalAmt = new ZonedDecimalData(15, 2);
	private PackedDecimalData wsaaNumberOfRecs = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaConstant = new PackedDecimalData(5, 0).init(6);
	private String wsaaEof = "";
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (186, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private BabrTableDAM babrIO = new BabrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private LetterRecordInner letterRecordInner = new LetterRecordInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit299
	}

	public Pcpllet1() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
			main010();
			exit090();
		}

protected void main010()
	{
		/*    If the PCPD-IDCODE-COUNT is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(pcpdatarec.idcodeCount,wsaaLastIdcodeCount)) {
			initialise100();
			getLettAndFormat200();
			while ( !(isEQ(wsaaEof,"Y"))) {
				getLettAndFormat300();
			}
			
			formatTotalAmt400();
			lettpf.close();
		}
		/*    If the following condition is satisfied all existing*/
		/*    occurrences of the field have been retrieved.*/
		if (isGT(pcpdatarec.fldOccur,wsaaNumberOfRecs)) {
			pcpdatarec.statuz.set(varcom.endp);
			return ;
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.  The offset calculation below caters*/
		/*    for recurring sets of fields.  In such cases PCP400*/
		/*    only knows the offset of the 1st occurrence of any field*/
		/*    so the position of further occurrences is calculated on*/
		/*    the basis of a constant number of positions between fields.*/
		if (isGT(pcpdatarec.fldOffset,0)
		&& isLT(pcpdatarec.fldOffset,7)) {
			offset.set(pcpdatarec.fldOffset);
		}
		else {
			compute(offset, 0).set(add(pcpdatarec.fldOffset,(mult((sub(pcpdatarec.fldOccur,1)),wsaaConstant))));
		}
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		/* MOVE LETC-OTHER-KEYS        TO LSAV-SAVE-OTHER-KEYS.         */
		wsaaNumberOfRecs.set(ZERO);
		wsaaTotalAmt.set(ZERO);
		offset.set(1);
		wsaaEof = "N";
		lettpf.openInput();
		/*EXIT*/
	}

protected void getLettAndFormat200()
	{
		try {
			getData200();
			formatPrintDate202();
			formatDdDate204();
			reserveForFutureB206();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void getData200()
	{
		lettpf.read(lettpfRec);
		if (lettpf.isAtEnd()) {
			wsaaEof = "Y";
			goTo(GotoLabel.exit299);
		}
		letterRecordInner.letterRecord.set(lettpfRec);
		/*FORMAT-TOTAL-AMT-DUE*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaAmount.set(wsaaTotalAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatPrintDate202()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letPrnDte));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letPrnDte);
		/*FORMAT-BANK-DESC*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letBankdesc));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letBankdesc);
	}

protected void formatDdDate204()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letDdDate));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letDdDate);
		/*RESERVE-FOR-FUTURE-A*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
	}

protected void reserveForFutureB206()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
	}

protected void getLettAndFormat300()
	{
		formatSequenceNo301();
		formatMandateRef303();
		formatName305();
		nextr307();
	}

protected void formatSequenceNo301()
	{
		wsaaNumberOfRecs.add(1);
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letSeqNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letSeqNo);
		/*FORMAT-BANKKEY*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letBankacckey));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letBankacckey);
	}

protected void formatMandateRef303()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(letterRecordInner.letLetNo));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], letterRecordInner.letLetNo);
		/*FORMAT-AMOUNT*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaTotalAmt.add(letterRecordInner.letAmount);
		wsaaAmount.set(letterRecordInner.letAmount);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void formatName305()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(letterRecordInner.letName, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(letterRecordInner.letName, 1, fldlen[offset.toInt()]));
		/*FORMAT-SURNAME*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(letterRecordInner.letSurname, " ", "CHARACTERS", "         ", null));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(letterRecordInner.letSurname, 1, fldlen[offset.toInt()]));
	}

protected void nextr307()
	{
		lettpf.read(lettpfRec);
		if (lettpf.isAtEnd()) {
			wsaaEof = "Y";
		}
		letterRecordInner.letterRecord.set(lettpfRec);
		/*EXIT*/
	}

protected void formatTotalAmt400()
	{
		/*TOTAL-AMT*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaAmount.set(wsaaTotalAmt);
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
/*
 * Class transformed  from Data Structure LETTER-RECORD--INNER
 */
private static final class LetterRecordInner { 

	private FixedLengthStringData letterRecord = new FixedLengthStringData(202);
	private FixedLengthStringData letLetDetails = new FixedLengthStringData(100).isAPartOf(letterRecord, 0);
	private FixedLengthStringData letPrnDte = new FixedLengthStringData(20).isAPartOf(letLetDetails, 0);
	private FixedLengthStringData letBankdesc = new FixedLengthStringData(60).isAPartOf(letLetDetails, 20);
	private FixedLengthStringData letDdDate = new FixedLengthStringData(20).isAPartOf(letLetDetails, 80);
	private FixedLengthStringData letBnkDetails = new FixedLengthStringData(102).isAPartOf(letterRecord, 100);
	private ZonedDecimalData letSeqNo = new ZonedDecimalData(4, 0).isAPartOf(letBnkDetails, 0).setUnsigned();
	private FixedLengthStringData letLetNo = new FixedLengthStringData(5).isAPartOf(letBnkDetails, 4);
	private FixedLengthStringData letBankacckey = new FixedLengthStringData(20).isAPartOf(letBnkDetails, 9);
	private FixedLengthStringData letChdrnum = new FixedLengthStringData(8).isAPartOf(letBnkDetails, 29);
	private ZonedDecimalData letAmount = new ZonedDecimalData(15, 2).isAPartOf(letBnkDetails, 37);
	private FixedLengthStringData letName = new FixedLengthStringData(20).isAPartOf(letBnkDetails, 52);
	private FixedLengthStringData letSurname = new FixedLengthStringData(30).isAPartOf(letBnkDetails, 72);
}
}
