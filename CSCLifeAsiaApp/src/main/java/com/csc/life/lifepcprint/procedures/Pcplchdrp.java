/*
 * File: Pcplchdrp.java
 * Date: 30 August 2009 1:00:34
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCHDRP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.tablestructures.Th558rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* This program is using to get the previous CHDR info.
*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* IF THERE IS ANY CHANGE ON PCPLCHDR, PLEASE CHANGE THIS PROGRAM
* ACCORDINGLY.
*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
*
* Offset. Description...............................................
*
*   01    Contract Number.
*   02    Contract Original Start Date
*   03    Current from Date
*   04    Current to Date
*   05    APL Suppress From Date
*   06    APL Suppress To Date
*   07    Bill Suppress From Date
*   08    Bill Suppress To Date
*   09    Suppress Notice From Date
*   10    Suppress Notice To Date
*   11    BTDATE
*   12    Bill Chnl
*   13    Payment Description
*   14    PTDATE
*   15    Renewal Year
*   16    Billing Freq
*   17    Billing Freq Description
*   18    Contract type
*   19    Contract type Description
*   20    Status Code
*   21    Status Description
*   22    SINSTAMT06
*   23    Rnwl supr from
*   24    Rnwl supr to
*   25    Branch Number
*   26    Branch Name
*   27    Contract Currency
*   28    Contract Currency Description
*   29    Agent Number
*   30    Agent Name
*   31    Owner Number
*   32    Owner Name
*
*
*****************************************************************
* </pre>
*/
public class Pcplchdrp extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubrname = "PCPLCHDRP";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private ZonedDecimalData wsaaInstto = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaInstto, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaInstoYr = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaOtherKeys, 1);
	private ZonedDecimalData wsaaLastTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaOtherKeys, 5).setUnsigned();
	private ZonedDecimalData wsaaSinstamt06 = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaStartAndLength = FLSArrayPartOfStructure(32, 6, filler2, 0);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
		/* FORMATS */
	private String chdrtrxrec = "CHDRTRXREC";
	private String agntrec = "AGNTREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t3620 = "T3620";
	private String t3590 = "T3590";
	private String t3623 = "T3623";
	private String t3629 = "T3629";
	private String t5688 = "T5688";
	private String th558 = "TH558";
		/*Agent header*/
	private AgntTableDAM agntIO = new AgntTableDAM();
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Standard Letter Control File*/
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Th558rec th558rec = new Th558rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit090, 
		exit299
	}

	public Pcplchdrp() {
		super();
	}

public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		try {
			main010();
		}
		catch (GOTOException e){
		}
		finally{
			exit090();
		}
	}

protected void main010()
	{
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getChdrAndFormat200();
		}
		if (isEQ(chdrtrxIO.getStatuz(),varcom.mrnf)) {
			pcpdatarec.data.set(SPACES);
			pcpdatarec.statuz.set(varcom.oK);
			goTo(GotoLabel.exit090);
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		/*EXIT*/
	}

protected void getChdrAndFormat200()
	{
		try {
			getData200();
			formatChdrnum201();
			formatOrigCommDate202();
			formatEffectiveDate203();
			formatEffectiveTo204();
			formatAplspfromDate205();
			formatAplsptoDate206();
			formatBillspfromDate207();
			formatBillsptoDate208();
			formatNotsspfromDate209();
			formatNotssptoDate210();
			formatBtdate211();
			formatBillchnl212();
			formatPaymentDesc213();
			formatPtdate214();
			formatRenewalYear215();
			formatBillfreqDesc217();
			formatCnttype218();
			formatCnttypeDesc219();
			formatStatcode220();
			formatStatcodeDesc221();
			formatSinstamt06222();
			formatRnwlspfrom223();
			formatRnwlspto224();
			formatCntbranch225();
			formatCntbranchDesc226();
			formatCntcurr227();
			formatCntcurrDesc228();
			formatAgntnum229();
			formatAgntname230();
			formatCownnum229();
			formatOwnerName229();
		}
		catch (GOTOException e){
		}
	}

protected void getData200()
	{
		wsaaOtherKeys.set(letcIO.getOtherKeys());
		chdrtrxIO.setChdrpfx("CH");
		chdrtrxIO.setChdrcoy(letcIO.getRdoccoy());
		chdrtrxIO.setChdrnum(letcIO.getRdocnum());
		compute(wsaaLastTranno, 0).set(sub(letcIO.getTranno(),1));
		chdrtrxIO.setTranno(wsaaLastTranno);
		chdrtrxIO.setFunction(varcom.readr);
		chdrtrxIO.setFormat(chdrtrxrec);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
		&& isNE(chdrtrxIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrtrxIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit299);
		}
	}

protected void formatChdrnum201()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(chdrtrxIO.getChdrnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getChdrnum());
	}

protected void formatOrigCommDate202()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getOccdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveDate203()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getCurrfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveTo204()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getCurrto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatAplspfromDate205()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getAplspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatAplsptoDate206()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getAplspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillspfromDate207()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getBillspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillsptoDate208()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getBillspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatNotsspfromDate209()
	{
		offset.add(1);
		if (isEQ(chdrtrxIO.getNotsspfrom(),ZERO)) {
			chdrtrxIO.setNotsspfrom(varcom.vrcmMaxDate);
		}
		datcon1rec.intDate.set(chdrtrxIO.getNotsspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatNotssptoDate210()
	{
		offset.add(1);
		if (isEQ(chdrtrxIO.getNotsspto(),ZERO)) {
			chdrtrxIO.setNotsspto(varcom.vrcmMaxDate);
		}
		datcon1rec.intDate.set(chdrtrxIO.getNotsspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBtdate211()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getBtdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillchnl212()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getBillchnl()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getBillchnl());
	}

protected void formatPaymentDesc213()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3620);
		descIO.setDescitem(chdrtrxIO.getBillchnl());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatPtdate214()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrtrxIO.getBtdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatRenewalYear215()
	{
		offset.add(1);
		wsaaInstto.set(chdrtrxIO.getPtdate());
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaInstoYr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaInstoYr);
		/*FORMAT-BILLFREQ*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getBillfreq()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getBillfreq());
	}

protected void formatBillfreqDesc217()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(chdrtrxIO.getBillfreq());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatCnttype218()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getCnttype()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getCnttype());
	}

protected void formatCnttypeDesc219()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrtrxIO.getChdrcoy());
		itemIO.setItemtabl(th558);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(pcpdatarec.language.toString());
		stringVariable1.append(chdrtrxIO.getCnttype().toString());
		itemIO.getItemitem().setLeft(stringVariable1.toString());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			th558rec.th558Rec.set(itemIO.getGenarea());
			if (isNE(th558rec.adsc,SPACES)) {
				fldlen[offset.toInt()].set(length(th558rec.adsc));
				wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], th558rec.adsc);
			}
		}
		else {
			descIO.setParams(SPACES);
			descIO.setDesctabl(t5688);
			descIO.setDescitem(chdrtrxIO.getCnttype());
			callDesc510();
			fldlen[offset.toInt()].set(30);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
		}
	}

protected void formatStatcode220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getStatcode()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getStatcode());
	}

protected void formatStatcodeDesc221()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrtrxIO.getStatcode());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatSinstamt06222()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSinstamt06.set(chdrtrxIO.getSinstamt06());
		fldlen[offset.toInt()].set(length(wsaaSinstamt06));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSinstamt06);
	}

protected void formatRnwlspfrom223()
	{
		offset.add(1);
		if (isEQ(chdrtrxIO.getRnwlspfrom(),ZERO)) {
			chdrtrxIO.setRnwlspfrom(varcom.vrcmMaxDate);
		}
		datcon1rec.intDate.set(chdrtrxIO.getRnwlspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatRnwlspto224()
	{
		offset.add(1);
		if (isEQ(chdrtrxIO.getRnwlspto(),ZERO)) {
			chdrtrxIO.setRnwlspto(varcom.vrcmMaxDate);
		}
		datcon1rec.intDate.set(chdrtrxIO.getRnwlspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatCntbranch225()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getCntbranch()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getCntbranch());
	}

protected void formatCntbranchDesc226()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrtrxIO.getCntbranch());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatCntcurr227()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getCntcurr()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getCntcurr());
	}

protected void formatCntcurrDesc228()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(chdrtrxIO.getCntcurr());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatAgntnum229()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getAgntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getAgntnum());
	}

protected void formatAgntname230()
	{
		getAgentName520();
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatCownnum229()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrtrxIO.getCownnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrtrxIO.getCownnum());
	}

protected void formatOwnerName229()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(chdrtrxIO.getCowncoy());
		namadrsrec.clntNumber.set(chdrtrxIO.getCownnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void callDatcon1500()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callDesc510()
	{
		/*START*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrtrxIO.getChdrcoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*EXIT*/
	}

protected void getAgentName520()
	{
		agentName520();
	}

protected void agentName520()
	{
		agntIO.setAgntpfx(chdrtrxIO.getAgntpfx());
		agntIO.setAgntcoy(chdrtrxIO.getAgntcoy());
		agntIO.setAgntnum(chdrtrxIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		agntIO.setFormat(agntrec);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		namadrsrec.clntPrefix.set(agntIO.getClntpfx());
		namadrsrec.clntCompany.set(agntIO.getClntcoy());
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
