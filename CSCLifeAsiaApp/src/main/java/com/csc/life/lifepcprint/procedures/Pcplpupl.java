/*
 * File: Pcplpupl.java
 * Date: 30 August 2009 1:02:53
 * Author: Quipoz Limited
 *
 * Class transformed from PCPLPUPL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.terminationclaims.dataaccess.PuplTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* Offset. Description...............................................
*
*   01    New Sumins
*   02    Fund Amount
*   03    Paid-Up Fee
*
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Pcplpupl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLPUPL";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
		/*  Make this field as large as you think the the data which
		  will be stored in it is ever likely to be. Calcualate
		  carefully as too small will result in truncation and too
		  large will result in poor performance.*/
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
		/* The following array is configured to store up to 20 fields,
		 ensure you change it to store exactly how many fields are
		 stored/retrieved by this subroutine.*/
	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (3, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private LetcTableDAM letcIO = new LetcTableDAM();
	private PuplTableDAM puplIO = new PuplTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();

	public Pcplpupl() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		/*    If the LETC-RRN is different from the one we had*/
		/*    before (or we are on the first call) we need to set up the*/
		/*    data buffer by reading data from the database.*/
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getPuplAndFormat200();
		}
		/*    Now we simply set the offset and return the data from*/
		/*    the data buffer.*/
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		/*EXIT*/
	}

protected void getPuplAndFormat200()
	{
		getData200();
		format201();
		format203();
	}

protected void getData200()
	{
		puplIO.setChdrcoy(letcIO.getRdoccoy());
		puplIO.setChdrnum(letcIO.getRdocnum());
		puplIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, puplIO);
		if (isNE(puplIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(puplIO.getParams());
			fatalError600();
		}
	}

protected void format201()
	{
		/*  New Sumins*/
		offset.set(1);
		strpos[offset.toInt()].set(1);
		wsaaAmount.set(puplIO.getNewsuma());
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*FORMAT*/
		/*  Fund Amount*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(puplIO.getFundAmount());
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
	}

protected void format203()
	{
		/*  Paid-Up Fee*/
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaAmount.set(puplIO.getPupfee());
		fldlen[offset.toInt()].set(length(wsaaAmount));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
		/*EXIT*/
	}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
