package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import com.quipoz.COBOLFramework.COBOLFunctions;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.dao.LetcpfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.dataaccess.model.Letcpf;
import com.csc.fsu.printing.dataaccess.dao.DnumpfDAO;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.fsu.printing.tablestructures.T2634rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   This routine will extract the necessary data for the document merge
*   
*
* Offset. Description............................................. .
*
* Occurance fields
*  001-008    Document Code
*
*                                                                   *
 * </pre>
 */
public class Pcppxdoc extends SMARTCodeModel {

	private static final Logger LOGGER = LoggerFactory.getLogger(Pcppxdoc.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(500).init(SPACES);

	private PackedDecimalData offset = new PackedDecimalData(3, 0);

	private PackedDecimalData wsaaNumberOfRecs = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaFieldsPerRec = new PackedDecimalData(5, 0).init(1);
	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray(50, 6);
	private PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	private PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private ZonedDecimalData docidnum = new ZonedDecimalData(8, 0).setUnsigned();
	private LetcTableDAM letcIO = new LetcTableDAM();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	private LetcpfDAO letcpfDAO = getApplicationContext().getBean("letcpfDAO", LetcpfDAO.class);
	private DnumpfDAO dnumpfDAO = getApplicationContext().getBean("dnumpfDAO", DnumpfDAO.class);
	private List<Letcpf> listLectpf = new ArrayList<Letcpf>();
	private List<String> docListName = new ArrayList<String>();
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private T2634rec t2634rec = new T2634rec();

	public Pcppxdoc() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

	protected void main000() {
		/* MAIN */
		/* If the PCPD-IDCODE-COUNT is different from the one we had */
		/* before (or we are on the first call) we need to set up the */
		/* data buffer by reading data from the database. */
		if (COBOLFunctions.isNE(pcpdatarec.idcodeCount, wsaaLastIdcodeCount)) {
			initialise100();
			getDataAndFormat200();
		}

		// GEPAS-4565-START
		/* If the following condition is satisfied all existing */
		/* occurrences of the field have been retrieved. */
		if (COBOLFunctions.isEQ(wsaaNumberOfRecs, ZERO)) {
			pcpdatarec.statuz.set(Varcom.oK);
			return;
		}
		if (COBOLFunctions.isGT(pcpdatarec.fldOccur, wsaaNumberOfRecs)
				|| COBOLFunctions.isGT(pcpdatarec.groupOccurance, wsaaNumberOfRecs)) {
			pcpdatarec.statuz.set(Varcom.endp);
			return;
		}
		/* Now we simply set the offset and return the data from */
		/* the data buffer. The offset calculation below caters */
		/* for recurring sets of fields. In such cases PCP400 */
		/* only knows the offset of the 1st occurrence of any field */
		/* so the position of further occurrences is calculated on */
		/* the basis of a constant number of positions between fields. */
		COBOLFunctions.compute(offset, 0).set(COBOLFunctions.add(pcpdatarec.fldOffset,
				(COBOLFunctions.mult((COBOLFunctions.sub(pcpdatarec.fldOccur, 1)), wsaaFieldsPerRec))));
		if (COBOLFunctions.isGT(pcpdatarec.groupOccurance, 1)) {
			COBOLFunctions.compute(offset, 0).set(COBOLFunctions.add(pcpdatarec.fldOffset,
					(COBOLFunctions.mult((COBOLFunctions.sub(pcpdatarec.groupOccurance, 1)), wsaaFieldsPerRec))));
		}
		// GEPAS-4565-END
		/* Now we simply set the offset and return the data from */
		/* the data buffer. */
		// offset.set(pcpdatarec.fldOffset); //GEPAS-4565-END
		pcpdatarec.data.set(COBOLFunctions.subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(Varcom.oK);
		/* EXIT */
		exitProgram();
	}

	protected void initialise100() {
		para101();
	}

	protected void para101() {
		wsaaLastIdcodeCount.set(pcpdatarec.idcodeCount);
		letcokcpy.saveOtherKeys.set(letcIO.getOtherKeys());
		offset.set(ZERO);
		String docName;
		wsaaNumberOfRecs.set(0);
		docListName.clear();
		String company = letcIO.getRdoccoy().toString().trim();
		listLectpf = letcpfDAO.readLetcpfRecord(letcIO.getLetterType().toString(), company,
				letcIO.getRdocnum().toString(), "2", letcIO.getTransCode().toString().trim(),
				letcIO.getTranno().toInt());
		LOGGER.info("===listLectpf=== {}", listLectpf);

		int counter = 0;
		for (Letcpf letc : listLectpf) {
			int docId = 0;
			docId = dnumpfDAO.getDocumentId(letc.getLettype(), letc.getLetseqno().toString(), letc.getClntnum());
			docidnum.set(docId);

			boolean isCustomerLetter = false;
			isCustomerLetter = checkCustomerLetter(company, letc.getHsublet().trim());
			if (isCustomerLetter && counter < 10) {
				docName = letc.getClntnum().trim() + docidnum.toString().trim() + letc.getHsublet().trim() + ".pdf";
				LOGGER.info("======docName========:{}", docName);// GEPAS-4565
				docListName.add(docName);
				wsaaNumberOfRecs.set(8); // GEPAS-4565
			}
			counter++;
		}

	}

	protected boolean checkCustomerLetter(String company, String hsublet) {
		boolean result = false;
		Itempf t2634Itempf = itempfDAO.findItemByItem("IT", company, "T2634", hsublet);
		if (t2634Itempf != null) {
			t2634rec.t2634Rec.set(StringUtil.rawToString(t2634Itempf.getGenarea()));
			String documentValidFor = t2634rec.rdocpfx05.trim();
			if ("B".equalsIgnoreCase(documentValidFor) || "C".equalsIgnoreCase(documentValidFor)) {
				result = true;
			}
		} else {
			t2634rec.t2634Rec.set(SPACES);
			result = false;
		}

		return result;
	}

	protected void getDataAndFormat200() {
		formatDocName001();
	}

	protected void formatDocName001() {
		LOGGER.info("===doclistDocname001== {}", docListName); // GEPAS-4889
		for (String docName : docListName) {
			offset.add(1);
			if (COBOLFunctions.isEQ(offset, 1)) {
				strpos[offset.toInt()].set(1);
			} else {
				COBOLFunctions.compute(strpos[offset.toInt()], 0).set(COBOLFunctions.add(
						strpos[COBOLFunctions.sub(offset, 1).toInt()], fldlen[COBOLFunctions.sub(offset, 1).toInt()]));
			}
			fldlen[offset.toInt()].set(ZERO);
			fldlen[offset.toInt()].add(COBOLFunctions.length(docName));
			if (COBOLFunctions.isEQ(fldlen[offset.toInt()], ZERO)) {
				fldlen[offset.toInt()].set(1);
			}
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], docName);

		}
	}

	protected void fatalError600() {
		/* FATAL */
		syserrrec.subrname.set("PCPPXDOC");
		pcpdatarec.statuz.set(Varcom.bomb);
		if (COBOLFunctions.isNE(syserrrec.statuz, SPACES) || COBOLFunctions.isNE(syserrrec.syserrStatuz, SPACES)) {
			syserrrec.syserrType.set("1");
		} else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/* EXIT */
		exitProgram();
	}

	protected void a1000CallDbcstrnc2() {
		/* A1010-DBCSTRNC */
		dbcstrcpy2.dbcsStatuz.set(SPACES);
		dbcsTrnc2(dbcstrcpy2.rec);
		if (COBOLFunctions.isNE(dbcstrcpy2.dbcsStatuz, Varcom.oK)) {
			dbcstrcpy2.dbcsOutputString.set(SPACES);
		}
		/* A1090-EXIT */
	}

}
