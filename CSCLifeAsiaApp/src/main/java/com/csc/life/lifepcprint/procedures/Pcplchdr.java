/*
 * File: Pcplchdr.java
 * Date: 30 August 2009 1:00:28
 * Author: Quipoz Limited
 * 
 * Class transformed from PCPLCHDR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.lifepcprint.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.length;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.LetcTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Upper;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Upperrec;
import com.csc.fsu.printing.recordstructures.Pcpdatarec;
import com.csc.life.newbusiness.tablestructures.Th558rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.HbxtpntTableDAM;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Description: (2-3 lines)
*
* This program is using to get the current CHDR info.
*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
* IF THERE IS ANY CHANGE ON PCPLCHDRP, PLEASE CHANGE THIS PROGRAM
* ACCORDINGLY.
*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
*
* Offset. Description...............................................
*
*   01    Contract Number.
*   02    Contract Original Start Date
*   03    Current from Date
*   04    Current to Date
*   05    APL Suppress From Date
*   06    APL Suppress To Date
*   07    Bill Suppress From Date
*   08    Bill Suppress To Date
*   09    Suppress Notice From Date
*   10    Suppress Notice To Date
*   11    BTDATE
*   12    Bill Chnl
*   13    Payment Description                                     <PCPPRT
*   14    PTDATE
*   15    Renewal Year
*   16    Billing Freq
*   17    Billing Freq Description
*   18    Contract type
*   19    Contract type Description
*   20    Status Code
*   21    Status Description
*   22    SINSTAMT06
*   23    Rnwl supr from
*   24    Rnwl supr to
*   25    Branch Number
*   26    Branch Name
*   27    Contract Currency
*   28    Contract Currency Description
*   29    Agent Number
*   30    Agent Name
*   31    Owner Number
*   32    Owner Name
*   33    Owner Salutation
*	34	  No Of OverDue Days
*   35    Premium Due
*   36	  Old BillFreq
*   37    Renewal Premium
*   38 	  Effective Date
*
*  Overview.
*  ---------
*
*  Processing.
*  -----------
*
***********************************************************************
*                                                                     *
* </pre>
* 
* ILPI-258 - Performance Improvement Replaced chdrIO with chdrpf
*/
public class Pcplchdr extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubrname = "PCPLCHDR  ";
	private PackedDecimalData wsaaLastIdcodeCount = new PackedDecimalData(18, 0).init(ZERO);
	private FixedLengthStringData wsaaDate = new FixedLengthStringData(10);
	private PackedDecimalData wsaaStoreRrn = new PackedDecimalData(9, 0);
	protected FixedLengthStringData wsaaDataBuffer = new FixedLengthStringData(1024).init(SPACES);
	private ZonedDecimalData wsaaInstto = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaDays = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaEffectiveDate = new FixedLengthStringData(10);

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaInstto, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaInstoYr = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaSinstamt06 = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	protected PackedDecimalData offset = new PackedDecimalData(3, 0);

	private FixedLengthStringData[] wsaaStartAndLength = FLSInittedArray (38, 6);
	protected PackedDecimalData[] strpos = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 0);
	protected PackedDecimalData[] fldlen = PDArrayPartOfArrayStructure(5, 0, wsaaStartAndLength, 3);
	private static final String agntrec = "AGNTREC";
	protected static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3620 = "T3620";
	private static final String t3590 = "T3590";
	private static final String t3623 = "T3623";
	private static final String t3629 = "T3629";
	protected static final String t5688 = "T5688";
	protected static final String th558 = "TH558";
	private static final String t6654 = "T6654";
	private AgntTableDAM agntIO = new AgntTableDAM();
	//ILPI-258
//	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LetcTableDAM letcIO = new LetcTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	protected Syserrrec syserrrec = new Syserrrec();
	protected Varcom varcom = new Varcom();
	protected Th558rec th558rec = new Th558rec();
	private T6654rec t6654rec = new T6654rec();
	private Pcpdatarec pcpdatarec = new Pcpdatarec();
	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Chdrpf chdrpf;
	private Map<String, List<Itempf>> t6654ListMap;
	
	public Pcplchdr() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		letcIO.setParams(convertAndSetParam(letcIO.getParams(), parmArray, 1));
		pcpdatarec.pcpdataRec = convertAndSetParam(pcpdatarec.pcpdataRec, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		}

		setReturningParam(letcIO.getParams(), parmArray, 1);
	}

protected void main000()
	{
		/*MAIN*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		wsaaEffectiveDate.set(datcon1rec.extDate);
		if (isNE(letcIO.getRrn(),wsaaStoreRrn)) {
			initialise100();
			getChdrAndFormat200();
		}
		offset.set(pcpdatarec.fldOffset);
		pcpdatarec.data.set(subString(wsaaDataBuffer, strpos[offset.toInt()], fldlen[offset.toInt()]));
		pcpdatarec.dataLen.set(fldlen[offset.toInt()]);
		pcpdatarec.statuz.set(varcom.oK);
		/*EXIT*/
		exitProgram();
	}

protected void initialise100()
	{
		/*PARA*/
		wsaaStoreRrn.set(letcIO.getRrn());
		/*EXIT*/
	}

protected void getChdrAndFormat200()
	{
		getData200();
		formatChdrnum201();
		formatOrigCommDate202();
		formatEffectiveDate203();
		formatEffectiveTo204();
		formatAplspfromDate205();
		formatAplsptoDate206();
		formatBillspfromDate207();
		formatBillsptoDate208();
		formatNotsspfromDate209();
		formatNotssptoDate210();
		formatBtdate211();
		formatBillchnl212();
		formatPaymentDesc213();
		formatPtdate214();
		formatRenewalYear215();
		formatBillFreq216();
		formatBillfreqDesc217();
		formatCnttype218();
		formatCnttypeDesc219();
		formatStatcode220();
		formatStatcodeDesc221();
		formatSinstamt06222();
		formatRnwlspfrom223();
		formatRnwlspto224();
		formatCntbranch225();
		formatCntbranchDesc226();
		formatCntcurr227();
		formatCntcurrDesc228();
		formatAgntnum229();
		formatAgntname230();
		formatCownnum231();
		formatOwnerName232();
		formatOwnerSalutl233();
		formatOverDue234();
		formatPremDue235();
		formatOldBillFreq236();
		formatRnwlPrem237();	//IBPLIFE-13475
		formatEffectiveDate238();
	}

protected void getData200()
	{
	//ILPI-258 STARTS
//		chdrIO.setChdrpfx("CH");
//		chdrIO.setChdrcoy(letcIO.getRdoccoy());
//		chdrIO.setChdrnum(letcIO.getRdocnum());
//		chdrIO.setValidflag("1");
//		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
//		chdrIO.setFunction(varcom.begn);
//		//performance improvement -- Anjali
//		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
//		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM");
//		SmartFileCode.execute(appVars, chdrIO);
//		if (isNE(chdrIO.getStatuz(),varcom.oK)) {
//			syserrrec.params.set(chdrIO.getParams());
//			fatalError600();
//		}
//		if (isEQ(chdrIO.getStatuz(),varcom.oK)
//		&& (isNE(chdrIO.getChdrpfx(),"CH")
//		|| isNE(chdrIO.getChdrcoy(),letcIO.getRdoccoy())
//		|| isNE(chdrIO.getChdrnum(),letcIO.getRdocnum()))) {
//			syserrrec.params.set(chdrIO.getParams());
//			fatalError600();
//		}
	
		chdrpf = chdrpfDAO.readChdrpfForLetter(letcIO.getRdoccoy().toString().trim(), letcIO.getRdocnum().toString().trim());
		
		//ILPI-258 ENDS
	}

protected void formatChdrnum201()
	{
		offset.set(1);
		strpos[offset.toInt()].set(1);
		fldlen[offset.toInt()].set(length(chdrpf.getChdrnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getChdrnum());
	}

protected void formatOrigCommDate202()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getOccdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveDate203()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getCurrfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatEffectiveTo204()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getCurrto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatAplspfromDate205()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getAplspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatAplsptoDate206()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getAplspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillspfromDate207()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getBillspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillsptoDate208()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getBillspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatNotsspfromDate209()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getNotsspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatNotssptoDate210()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getNotsspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBtdate211()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getBtdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatBillchnl212()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getBillchnl()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getBillchnl());
	}

protected void formatPaymentDesc213()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3620);
		descIO.setDescitem(chdrpf.getBillchnl());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatPtdate214()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getPtdate());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatRenewalYear215()
	{
		offset.add(1);
		wsaaInstto.set(chdrpf.getPtdate());
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(wsaaInstoYr));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaInstoYr);
	}

protected void formatBillFreq216()
{
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	fldlen[offset.toInt()].set(length(chdrpf.getBillfreq()));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getBillfreq());
}

protected void formatBillfreqDesc217()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setLanguage(pcpdatarec.language);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(chdrpf.getBillfreq());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatCnttype218()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getCnttype()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getCnttype());
	}

protected void formatCnttypeDesc219()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrpf.getChdrcoy());
		itemIO.setItemtabl(th558);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(pcpdatarec.language);
		stringVariable1.addExpression(chdrpf.getCnttype());
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			th558rec.th558Rec.set(itemIO.getGenarea());
			if (isNE(th558rec.adsc,SPACES)) {
				fldlen[offset.toInt()].set(length(th558rec.adsc));
				wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], th558rec.adsc);
			}
		}
		else {
			descIO.setParams(SPACES);
			descIO.setDesctabl(t5688);
			descIO.setDescitem(chdrpf.getCnttype());
			callDesc510();
			fldlen[offset.toInt()].set(30);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
		}
	}

protected void formatStatcode220()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getStatcode()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getStatcode());
	}

protected void formatStatcodeDesc221()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrpf.getStatcode());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatSinstamt06222()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		wsaaSinstamt06.set(chdrpf.getSinstamt06());
		fldlen[offset.toInt()].set(length(wsaaSinstamt06));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaSinstamt06);
	}

protected void formatRnwlspfrom223()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getRnwlspfrom());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatRnwlspto224()
	{
		offset.add(1);
		datcon1rec.intDate.set(chdrpf.getRnwlspto());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(10);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], datcon1rec.extDate);
	}

protected void formatCntbranch225()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getCntbranch()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getCntbranch());
	}

protected void formatCntbranchDesc226()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrpf.getCntbranch());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatCntcurr227()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getCntcurr()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getCntcurr());
	}

protected void formatCntcurrDesc228()
	{
		offset.add(1);
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(chdrpf.getCntcurr());
		callDesc510();
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(30);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], descIO.getLongdesc());
	}

protected void formatAgntnum229()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getAgntnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getAgntnum());
	}

protected void formatAgntname230()
	{
		getAgentName520();
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatCownnum231()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		fldlen[offset.toInt()].set(length(chdrpf.getCownnum()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpf.getCownnum());
	}

protected void formatOwnerName232()
	{
		offset.add(1);
		compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(chdrpf.getCowncoy());
		namadrsrec.clntNumber.set(chdrpf.getCownnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(namadrsrec.name, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], subString(namadrsrec.name, 1, fldlen[offset.toInt()]));
	}

protected void formatOwnerSalutl233() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	String salutlName=getDescT3583();
	fldlen[offset.toInt()].set(ZERO);
	fldlen[offset.toInt()].add(inspectTally(salutlName, " ", "CHARACTERS", "    ", null));
	if (isEQ(fldlen[offset.toInt()],ZERO)) {
		fldlen[offset.toInt()].set(1);
	}
	fldlen[offset.toInt()].set(length(salutlName));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], salutlName);
}


protected String getDescT3583() {
	Clntpf clntpf = new Clntpf();
	clntpf.setClntpfx("CN");
	clntpf.setClntcoy(chdrpf.getCowncoy().toString());
	clntpf.setClntnum(chdrpf.getCownnum());
	clntpf = clntpfDAO.selectActiveClient(clntpf);
	if(clntpf.getSalutl()==null || clntpf.getSalutl().trim().isEmpty()) {
		return " ";
	}
	String t3583Item;
	Upperrec upperrec = new Upperrec();
	upperrec.upperRec.set(SPACES);
	upperrec.name.set(clntpf.getSalutl().trim());
	callProgram(Upper.class, upperrec.upperRec);
	if (isNE(upperrec.statuz, Varcom.oK)) {
		t3583Item = clntpf.getSalutl().trim();
	} else {
		t3583Item = upperrec.name.toString().trim();
	}
	Descpf descpf=descDAO.getdescData("IT", "T3583", t3583Item, namadrsrec.clntCompany.toString(), namadrsrec.language.toString());
	if(descpf==null) {
		return " ";
	} else {
		return descpf.getShortdesc().trim();
	}
}



protected void callDatcon1500()
	{
		/*START*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callDesc510()
	{
		/*START*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(chdrpf.getChdrcoy());
		descIO.setLanguage(pcpdatarec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/*EXIT*/
	}

protected void getAgentName520()
	{
		agentName520();
	}

protected void agentName520()
	{
		agntIO.setAgntpfx(chdrpf.getAgntpfx());
		agntIO.setAgntcoy(chdrpf.getAgntcoy());
		agntIO.setAgntnum(chdrpf.getAgntnum());
		agntIO.setFunction(varcom.readr);
		agntIO.setFormat(agntrec);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		namadrsrec.clntPrefix.set(agntIO.getClntpfx());
		namadrsrec.clntCompany.set(agntIO.getClntcoy());
		namadrsrec.clntNumber.set(agntIO.getClntnum());
		namadrsrec.language.set(pcpdatarec.language);
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}


protected void formatOverDue234(){
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	String key;
	t6654ListMap = new HashMap<String, List<Itempf>>();
	t6654ListMap = itemDAO.loadSmartTable("IT", letcIO.getRdoccoy().toString(), t6654);
	key = chdrpf.getBillchnl().trim().concat(chdrpf.getCnttype().trim());
	if (!readT6654(key)) {
		key = chdrpf.getBillchnl().trim().concat("***");
		if (!readT6654(key)) {
			fatalError600();
		}
	}	
	calculateDays();
	if (isLTE(wsaaDays, t6654rec.daexpy01)) {
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(t6654rec.daexpy01, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		fldlen[offset.toInt()].set(length(t6654rec.daexpy01));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], t6654rec.daexpy01);
	}
	else if(isGT(wsaaDays, t6654rec.daexpy01) && isLTE(wsaaDays, t6654rec.daexpy02)) {
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(t6654rec.daexpy02, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		fldlen[offset.toInt()].set(length(t6654rec.daexpy02));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], t6654rec.daexpy02);
	}
	else if(isGT(wsaaDays, t6654rec.daexpy02) && isLTE(wsaaDays, t6654rec.daexpy03)) {
		fldlen[offset.toInt()].set(ZERO);
		fldlen[offset.toInt()].add(inspectTally(t6654rec.daexpy03, " ", "CHARACTERS", "    ", null));
		if (isEQ(fldlen[offset.toInt()],ZERO)) {
			fldlen[offset.toInt()].set(1);
		}
		fldlen[offset.toInt()].set(length(t6654rec.daexpy03));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], t6654rec.daexpy03);
	}
}

protected void formatPremDue235() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	
	HbxtpntTableDAM hbxtpntIO = new HbxtpntTableDAM();
	hbxtpntIO.setDataKey(SPACES);
	hbxtpntIO.setChdrcoy(letcIO.getRdoccoy());
	hbxtpntIO.setChdrnum(letcIO.getRdocnum());
	hbxtpntIO.setInstfrom(chdrpf.getPtdate());
	hbxtpntIO.setFormat("HBXTPNTREC");
	hbxtpntIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, hbxtpntIO);
	if (isNE(hbxtpntIO.getStatuz(),Varcom.oK)) {
		if (isEQ(hbxtpntIO.getStatuz(),Varcom.mrnf)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
			return;
		}
		syserrrec.statuz.set(hbxtpntIO.getStatuz());
		syserrrec.params.set(hbxtpntIO.getParams());
		fatalError600();
	}
	LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	linsrnlIO.setParams(SPACES);
	linsrnlIO.setChdrcoy(hbxtpntIO.getChdrcoy());
	linsrnlIO.setChdrnum(hbxtpntIO.getChdrnum());
	linsrnlIO.setInstfrom(hbxtpntIO.getInstfrom());
	linsrnlIO.setFormat("LINSRNLREC");
	linsrnlIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, linsrnlIO);
	if (isNE(linsrnlIO.getStatuz(),Varcom.oK)) {
		syserrrec.params.set(linsrnlIO.getParams());
		syserrrec.statuz.set(linsrnlIO.getStatuz());
		fatalError600();
	}
	
	ZonedDecimalData wsaaAmount = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	if (isNE(linsrnlIO.getCntcurr(),linsrnlIO.getBillcurr())) {
		wsaaAmount.set(linsrnlIO.getInstamt06());
	}
	else {
		wsaaAmount.set(hbxtpntIO.getInstamt06());
	}
	fldlen[offset.toInt()].set(length(wsaaAmount));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaAmount);
}

protected boolean readT6654(String searchItem) {
	String keyItemitem = searchItem.trim();
	List<Itempf> itempfList = new ArrayList<Itempf>();
	boolean itemFound = false;	
	
	if (t6654ListMap.containsKey(keyItemitem)){	
		itempfList = t6654ListMap.get(keyItemitem);
		Iterator<Itempf> iterator = itempfList.iterator();
		while (iterator.hasNext()) {
			itemFound = true;
			Itempf itempf = new Itempf();
			itempf = iterator.next();
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}
	return itemFound;	
}

protected void calculateDays() {
	
	if (isNE(chdrpf.getPtdate(),ZERO))
	{
	datcon3rec.datcon3Rec.set(SPACES);
	datcon3rec.intDate1.set(chdrpf.getPtdate());
	datcon3rec.intDate2.set(wsaaToday);
	datcon3rec.frequency.set("DY");
	datcon3rec.freqFactor.set(0);
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.params.set(datcon3rec.datcon3Rec);
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	wsaaDays.set(datcon3rec.freqFactor);
}
}

protected void formatOldBillFreq236() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	int maxTranno = chdrpfDAO.getMaxTranno(chdrpf.getChdrnum());
	Chdrpf chdrpfTemp = chdrpfDAO.getRecordByChdrnum("CH", chdrpf.getChdrcoy().toString(), chdrpf.getChdrnum(), maxTranno-1);
	if(null != chdrpfTemp) {
		fldlen[offset.toInt()].set(length(chdrpfTemp.getBillfreq()));
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], chdrpfTemp.getBillfreq());
	}
	else {
		fldlen[offset.toInt()].set(1);
		wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], SPACES);
	}
}

protected void formatRnwlPrem237() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	
	LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	linsrnlIO.setParams(SPACES);
	linsrnlIO.setChdrcoy(letcIO.getChdrcoy());
	linsrnlIO.setChdrnum(letcIO.getChdrnum());
	linsrnlIO.setInstfrom(chdrpf.getInstfrom());
	linsrnlIO.setFormat("LINSRNLREC");
	linsrnlIO.setFunction(Varcom.readr);
	SmartFileCode.execute(appVars, linsrnlIO);
	if (isNE(linsrnlIO.getStatuz(),Varcom.oK)) {
		if (isEQ(linsrnlIO.getStatuz(),Varcom.mrnf)) {
			fldlen[offset.toInt()].set(1);
			wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], " ");
			return;
		}
		syserrrec.params.set(linsrnlIO.getParams());
		syserrrec.statuz.set(linsrnlIO.getStatuz());
		fatalError600();
	}
	
	ZonedDecimalData wsaaRnwlPrm = new ZonedDecimalData(13, 2).setPattern("ZZ,ZZZ,ZZZ,ZZ9.99");
	wsaaRnwlPrm.set(linsrnlIO.getInstamt06());
	fldlen[offset.toInt()].set(length(wsaaRnwlPrm));
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaRnwlPrm);
}

protected void formatEffectiveDate238() {
	offset.add(1);
	compute(strpos[offset.toInt()], 0).set(add(strpos[sub(offset,1).toInt()],fldlen[sub(offset,1).toInt()]));
	
	fldlen[offset.toInt()].set(10);
	wsaaDataBuffer.setSub1String(strpos[offset.toInt()], fldlen[offset.toInt()], wsaaEffectiveDate);
}

protected void fatalError600()
	{
		/*FATAL*/
		syserrrec.subrname.set(wsaaSubrname);
		pcpdatarec.statuz.set(varcom.bomb);
		if (isNE(syserrrec.statuz,SPACES)
		|| isNE(syserrrec.syserrStatuz,SPACES)) {
			syserrrec.syserrType.set("1");
		}
		else {
			syserrrec.syserrType.set("2");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		exitProgram();
	}
}
