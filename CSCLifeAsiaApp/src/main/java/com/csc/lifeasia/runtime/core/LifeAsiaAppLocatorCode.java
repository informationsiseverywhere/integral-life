package com.csc.lifeasia.runtime.core;

import java.util.List;

import org.apache.commons.lang3.ClassUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsuframework.core.FsuAsiaAppLocatorCode;
import com.quipoz.framework.util.AppPathLoader;
import com.quipoz.framework.util.QPUtilities;

/**
 * @author Quipoz - Chris Hulley
 * @version 2.0 Nov 2007
 *
 * Purpose: To locate the classpath where classes are stored without the need
 * to externalise strings containing the path.
 * This program replicates the Polisy Asia Model, in that there are no duplicates.
 * Thus, investigation of the library list is not needed.
 */
public class LifeAsiaAppLocatorCode extends FsuAsiaAppLocatorCode {
	/**
	 * Logger for this class
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeAsiaAppLocatorCode.class);

	static {
		AppPathLoader.load(FsuAsiaAppLocatorCode.getLocations(), LifeAsiaAppLocatorCode.class
		    .getResourceAsStream("lifeasia.path"));
	};

	/**
	 * Find an executable program in one of the available application
	 * paths based on the Screen Name.
	 * @param code Unqualified program name
	 * @return Fully qualified path to program
	 */
	public static String findPath(String code) {

		if (code != null) {
			code = code.trim();
		}
		else {
			code = "DummyVarCodeWasNull";
		}

		/* Standard is: Screen name is program name + Form. Remove trailing Form. */
		code = QPUtilities.removeTrailing(code, "Form.jsp");
		if (code.charAt(0) == 'S') {
			code = "P" + code.substring(1);
		}


		String path = (String) knownClasses.get(code);
		String base = null;

		if (path == null) {
			List<String> locations = FsuAsiaAppLocatorCode.getLocations();
			for (int l = 0; l < locations.size(); l++) {
				path = locations.get(l) + '.' + code;
				try {
					ClassUtils.getClass(path); // IJTI-713
					knownClasses.put(code, path);
					break;
				}
				catch (ClassNotFoundException e) {
					path = null;
				}
			}
		}

		if (path == null) {
			throw new RuntimeException("Did not find '" + code + "' in any known path.");
		}

		base = QPUtilities.splitLastOccurence(path, ".")[0];

		return base;
	}

	/**
	 * Find an executable program in one of the available application
	 * paths. The available paths are determined by the current Library List.
	 * @param code Unqualified program name
	 * @return Class of program
	 */
	public static Class<?> find(String code) {

		if (code != null) {
			code = code.trim();
		}
		else {
			code = "DummyVarCodeWasNull";
		}

		String[] sa = new String[2];
		sa[1] = code;
		/* Is it a qualified program, eg INPRD/II110 */
		if (code.indexOf('/') >= 0 || code.indexOf('.') >= 0) {
			code = code.replace('.', '/');
			code = code.split("/", 2)[1];
		}

		/* Capitalise as necessary. Eg CHDRPA becomes Chdrpa */
		code = QPUtilities.capitalise(code.toLowerCase());

		int l = 0;
		String path = null;
		Class<?> vm = null;

		/* Search the current library list to see if the requested program was
		 * previously found, for performance. */
		path = (String) knownClasses.get(code);

		if (path != null) {
			try {
				return ClassUtils.getClass(path); // IJTI-713
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		final List<String> locations = FsuAsiaAppLocatorCode.getLocations();
		for (l = 0; l < locations.size(); l++) {
			path = locations.get(l) + '.' + code;
			try {
				vm = ClassUtils.getClass(path); // IJTI-713
				LOGGER.debug("find(String) - Program found in {} ", path);
				knownClasses.put(code, path);
				return vm;
			}
			catch (ClassNotFoundException e) {
			}
		}
		StringBuffer error = new StringBuffer("Did not find program '");
		error.append(code);
		error.append("' in any known location. Locations are");
		for (l = 0; l < libraries.size(); l++) {
			error.append("\n - " + locations.get(l));
		}
		throw new RuntimeException(error.toString());
	}

	/**
	 * Getter of locations
	 *
	 * @return List of locations
	 */
	public static List<String> getLocations() {
		return FsuAsiaAppLocatorCode.getLocations();
	}
}
