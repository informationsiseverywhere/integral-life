package com.csc.lifeasiaframework.dataaccess;

import com.csc.life.anticipatedendowment.dataaccess.AnticipatedEndowmentDAOFactory;

public class LifeAsiaDAOFactory {
	
	//ILIFE-1827 START by avemula
	/**
	 * @return AnticipatedEndowmentDAOFactory
	 */
	public static AnticipatedEndowmentDAOFactory anticipatedEndowmentInstance()
	{
		return AnticipatedEndowmentDAOFactory.getInstance();
	}
	//ILIFE-1827 END

}


