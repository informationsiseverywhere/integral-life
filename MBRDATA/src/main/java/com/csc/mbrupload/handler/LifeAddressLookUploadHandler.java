package com.csc.mbrupload.handler;

import java.io.FileNotFoundException;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.csc.mbrupload.DAO.LifeUploadDAO;

/**
 * Servlet implementation class RECUploadHandler
 */
public class LifeAddressLookUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final DateFormat dateFormat = new SimpleDateFormat(
			"yyyy/MM/dd HH:mm:ss");

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LifeAddressLookUploadHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uploadStatus = "Upload failed, Please check your file data!!!";
		try {

			ServletInputStream fs = request.getInputStream();
			byte[] junk = new byte[1024];
			int bytesRead = 0;
			List<String> queryList = new ArrayList<String>();
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			Workbook workbook = WorkbookFactory.create(fs);			
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
				Row row = sheet.getRow(i);
				StringBuilder stringQuery=new StringBuilder("");
				String query = "";
				if(row.getCell(0)!=null && !row.getCell(0).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(0).toString().trim()).append("' ,");
				}else{
					continue;
				}
				if(row.getCell(1)!=null && !row.getCell(1).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(1).toString().trim()).append("' ,");
				}
				else{
					continue;
				}
				if(row.getCell(2)!=null && !row.getCell(2).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(2).toString().trim()).append("' ,");
				}
				else{
					continue;
				}
				if(row.getCell(3)!=null && !row.getCell(3).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(3).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				
				if(row.getCell(4)!=null && !row.getCell(4).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(4).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(5)!=null && !row.getCell(5).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(5).toString().trim().charAt(0)).append("' ,");
				}
				else{
					stringQuery.append("'' ,");
				}
				if(row.getCell(6)!=null && !row.getCell(6).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(6).toString().trim().charAt(0)).append("' ,");
				}
				else{
					stringQuery.append("'' ,");
				}
				if(row.getCell(7)!=null && !row.getCell(7).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(7).toString().trim().charAt(0)).append("' ,");
				}
				else{
					stringQuery.append("'' ,");
				}
				if(row.getCell(8)!=null && !row.getCell(8).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(8).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(9)!=null && !row.getCell(9).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(9).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(10)!=null && !row.getCell(10).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(10).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(11)!=null && !row.getCell(11).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(11).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(12)!=null && !row.getCell(12).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(12).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(13)!=null && !row.getCell(13).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(13).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(14)!=null && !row.getCell(14).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(14).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(15)!=null && !row.getCell(15).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(15).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(16)!=null && !row.getCell(16).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(16).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				
				if(row.getCell(17)!=null && !row.getCell(17).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(17).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(18)!=null && !row.getCell(18).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(18).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(19)!=null && !row.getCell(19).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(19).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(20)!=null && !row.getCell(20).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(20).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(21)!=null && !row.getCell(21).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(21).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(22)!=null && !row.getCell(22).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(22).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(23)!=null && !row.getCell(23).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(23).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(24)!=null && !row.getCell(24).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(24).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(25)!=null && !row.getCell(25).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(25).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(26)!=null && !row.getCell(26).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(26).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(27)!=null && !row.getCell(27).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(27).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(28)!=null && !row.getCell(28).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(28).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(29)!=null && !row.getCell(29).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(29).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(30)!=null && !row.getCell(30).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(30).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(31)!=null && !row.getCell(31).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(31).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(32)!=null && !row.getCell(32).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(32).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(33)!=null && !row.getCell(33).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(33).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(34)!=null && !row.getCell(34).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(34).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(35)!=null && !row.getCell(35).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(35).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(36)!=null && !row.getCell(36).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(36).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(37)!=null && !row.getCell(37).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(37).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(38)!=null && !row.getCell(38).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(38).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(39)!=null && !row.getCell(39).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(39).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(40)!=null && !row.getCell(40).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(40).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(41)!=null && !row.getCell(41).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(41).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(42)!=null && !row.getCell(42).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(42).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(43)!=null && !row.getCell(43).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(43).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(44)!=null && !row.getCell(44).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(44).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(45)!=null && !row.getCell(45).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(45).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(46)!=null && !row.getCell(46).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(46).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(47)!=null && !row.getCell(47).toString().trim().isEmpty()){
					stringQuery.append(Integer.valueOf(row.getCell(47).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(48)!=null && !row.getCell(48).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(48).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(49)!=null && !row.getCell(49).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(49).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(50)!=null && !row.getCell(50).toString().trim().isEmpty()){
					stringQuery.append(Byte.valueOf(row.getCell(50).toString().trim())).append(" ,");
				}
				else{
					stringQuery.append("0 ,");
				}
				if(row.getCell(51)!=null && !row.getCell(51).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(51).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				if(row.getCell(52)!=null && !row.getCell(52).toString().trim().isEmpty()){
					stringQuery.append("'").append(row.getCell(52).toString().trim()).append("' ,");
				}
				else{
					stringQuery.append("' ' ,");
				}
				query = query +stringQuery.toString()+"to_date('"+ getCurrentTimeStamp() + "', 'yyyy/mm/dd hh24:mi:ss'))";
					query = query.substring(0, query.length() - 1);

				queryList.add(query);
			}
			LifeUploadDAO uploadDAO = new LifeUploadDAO();
			
			fs.close();
			boolean flag = false;
			try {
				flag = uploadDAO.loadADRSPFDataIntoDB(queryList);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (flag) {
				uploadStatus = "File Uploaded Successfully";
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("uploadAddressStatusExcel", uploadStatus);
		request.getRequestDispatcher("/LIFEUpload.jsp").forward(request,
				response);
	}
	private static String getCurrentTimeStamp() {

		java.util.Date today = new java.util.Date();
		return dateFormat.format(today.getTime());

	}

}
