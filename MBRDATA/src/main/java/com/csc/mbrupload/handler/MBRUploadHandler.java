package com.csc.mbrupload.handler;

import java.io.DataInputStream;
import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.csc.mbrupload.DAO.MBRUploadDAO;

/**
 * Servlet implementation class LabelValueImportServlet
 */
public class MBRUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MBRUploadHandler() {
		super();
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void service(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);

		MBRUploadDAO mbrDao = new MBRUploadDAO();

		String contentType = request.getContentType();
		
		String uploadStatus = "Upload failed, Please check your file data!!!";

		if ((contentType != null)
				&& (contentType.indexOf("multipart/form-data") >= 0)) {
			DataInputStream in = new DataInputStream(request.getInputStream());
			int formDataLength = request.getContentLength();
			byte dataBytes[] = new byte[formDataLength];
			
			int byteRead = 0;
			int totalBytesRead = 0;
			while (totalBytesRead < formDataLength) {
				byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
				totalBytesRead += byteRead;
			}

			String file = new String(dataBytes);
			String saveFile = file.substring(file.indexOf("filename=\"") + 10);
			saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
			saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,
					saveFile.indexOf("\""));
			int lastIndex = contentType.lastIndexOf("=");
			String boundary = contentType.substring(lastIndex + 1, contentType
					.length());

			int pos;

			pos = file.indexOf("filename=\"");
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			int boundaryLocation = file.indexOf(boundary, pos) - 4;
			int startPos = ((file.substring(0, pos)).getBytes()).length;
			int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
			String str = file.substring(startPos, boundaryLocation);
			String fileStr[] = str.split("\\n");
			System.out.println("Value of my File is:: " + str);

			for (int i = 0; i < fileStr.length; i++) {
				System.out.println("Value of my array at " + (i + 1)
						+ " th location is:: \n" + fileStr[i]);
			}
			boolean flag = false;
			try {
				flag = mbrDao.loadDataIntoDB(fileStr);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			if (flag) {
				uploadStatus = "File Uploaded Successfully";
			}
			request.setAttribute("uploadStatus", uploadStatus);
			request.getRequestDispatcher("/MBRUpload.jsp").forward(request,
					response);
		}
	}

}
