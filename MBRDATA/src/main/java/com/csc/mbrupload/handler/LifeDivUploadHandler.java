package com.csc.mbrupload.handler;

import java.io.FileNotFoundException;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.csc.mbrupload.DAO.LifeUploadDAO;

/**
 * Servlet implementation class RECUploadHandler
 */
public class LifeDivUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LifeDivUploadHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String uploadStatus = "Upload failed, Please check your file data!!!";
		try {

			ServletInputStream fs = request.getInputStream();
			byte[] junk = new byte[1024];
			int bytesRead = 0;
			List<String> queryList = new ArrayList<String>();
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			Workbook workbook = WorkbookFactory.create(fs);			
			Sheet sheet = workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();
			rowIterator.next();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				String query = "";

				for (int k = 0; k < 116; k++) {
					Cell cell = row.getCell(k);
					if (cell != null) {
						switch (cell.getCellType()) {
						case NUMERIC:/* IBPTE-1944 */
							int val = (int) cell.getNumericCellValue();
							if (val != 0) {
									query = query + "" + val + " ,";
							} else {
								query = query + "0,";
							}
							break;
						case STRING:/* IBPTE-1944 */
							String value = cell.getStringCellValue().trim();
							if (value != null && !value.trim().equals("")) {
								 query = query + "'" + value + "' ,";
							} else {
								query = query + "null,";
							}
							break;
						case BLANK:/* IBPTE-1944 */
							if(k!=0)
								query = query + "0,";	
							else
								query = query + "null,";
						
						break;
						}
					} else {
						query = query + "null,";
					}
				}
				query = query.substring(0, query.length() - 1);

				queryList.add(query);
			}
			LifeUploadDAO uploadDAO = new LifeUploadDAO();
			
			fs.close();
			boolean flag = false;
			try {
				flag = uploadDAO.loadDIVRPFDataIntoDB(queryList);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (flag) {
				uploadStatus = "File Uploaded Successfully";
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		request.setAttribute("uploadStatusExcel", uploadStatus);
		request.getRequestDispatcher("/LIFEUpload.jsp").forward(request,
				response);
	}

}
