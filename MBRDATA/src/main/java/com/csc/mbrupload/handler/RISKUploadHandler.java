package com.csc.mbrupload.handler;

import java.io.IOException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.csc.mbrupload.DAO.RiskUploadDAO;
import com.csc.riskupload.util.FireRisk;
import com.csc.riskupload.util.FireRiskFio;
import com.csc.riskupload.util.FireRiskFir;
import com.csc.riskupload.util.FireRiskLpp;
import com.csc.riskupload.util.FireRiskReader;
import com.csc.riskupload.util.FireRiskType;
import com.csc.riskupload.util.FireRisksFio;
import com.csc.riskupload.util.FireRisksFir;
import com.csc.riskupload.util.FireRisksLpp;


/**
 * @author sgadkari
 * 
 */


/**
 * Servlet implementation class RISKUploadHandler
 */
public class RISKUploadHandler extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RISKUploadHandler() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String uploadStatus = "Upload failed, Please check your file data!!!";
		ServletInputStream fs = request.getInputStream();
		FireRiskReader fireRiskReader = new FireRiskReader();
		try {

			byte[] junk = new byte[1024];
			int bytesRead = 0;
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);
			bytesRead = fs.readLine(junk, 0, junk.length);

			Workbook workbook = WorkbookFactory.create(fs);
			List<Sheet> sheetList = new ArrayList<Sheet>();
			int numberOfSheets = workbook.getNumberOfSheets();
			if (numberOfSheets != 3) {
				uploadStatus = "Invalid number of sheets "+numberOfSheets;
				fs.close();
			}

			for(int i=0; i < numberOfSheets; i++) {
			    Sheet sheet = workbook.getSheetAt(i);
			    sheetList.add(sheet);            
			}
			List<FireRiskFir> fireRiskFirList = new ArrayList<FireRiskFir>();
			List<FireRiskFio> fireRiskFioList = new ArrayList<FireRiskFio>();
			List<FireRiskLpp> fireRiskLppList = new ArrayList<FireRiskLpp>();
			
			String xmlName = "";
			String sheetName = "";
			fireRiskReader = new FireRiskReader();

	            for(int i=0; i < sheetList.size(); i++) {
	                Sheet sheet = sheetList.get(i);
	            	sheetName = sheet.getSheetName();
	            	FireRiskType cureentSheet  = FireRiskType.valueOf(sheetName);
	            	switch (cureentSheet) {
		            case FIR:
		            	fireRiskFirList = fireRiskReader.getFireRiskFirList(sheet);
		                break;
		            case FIO:
		            	fireRiskFioList = fireRiskReader.getFireRiskFioList(sheet);
		                break;
		            case LPP:
		            	fireRiskLppList = fireRiskReader.getFireRiskLppList(sheet);
		                break;
		            default:
		            	uploadStatus = "Invalid sheet "+cureentSheet;
	            	}            	
	            }  		
			FireRisksFir fireRisksFir = new FireRisksFir();

			FireRisk fireRisk = new FireRisk();		
			fireRisksFir.setFireRiskFirList(fireRiskFirList);		
			fireRisk.setFireRisksFir(fireRisksFir);

			FireRisksFio fireRisksFio = new FireRisksFio();
			fireRisksFio.setFireRiskFioList(fireRiskFioList);
			fireRisk.setFireRisksFio(fireRisksFio);

			FireRisksLpp fireRisksLpp = new FireRisksLpp();
			fireRisksLpp.setFireRiskLppList(fireRiskLppList);
			fireRisk.setFireRisksLpp(fireRisksLpp);			
			
			RiskUploadDAO uploadDAO = new RiskUploadDAO();
			String bulkId = getBulkId();
			uploadDAO.setBulkId(bulkId);
			boolean status=	uploadDAO.saveBRSTPFDataIntoDB(fireRisk);
				if (status){
					fs.close();
			uploadStatus = "File Uploaded Successfully:"+bulkId;
				}		

			
		} catch (Exception e) {
			e.printStackTrace();
		}
		fs.close();
		request.setAttribute("uploadStatusExcel", uploadStatus);
		request.getRequestDispatcher("/RISKUpload.jsp").forward(request,
				response);
	} 
	public String getBulkId(){
		//get current date time with Date()
		DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");		   
		   Date date = new Date();		   
		   String currentDate=dateFormat.format(date);  		   
		   String bulkid="BLK" + currentDate;		   
		   return bulkid;
	}
		}
	


