package com.csc.mbrupload.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * @author spandey20
 * 
 */
public class MBRUploadDAO {

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getPrimaryKey()
	 */
	public String getPrimaryKey() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getTableName()
	 */
	public String getTableName() {
		return "COMMON";
	}

	/**
	 * @param firstSheet
	 * @return
	 * @return
	 * @throws SQLException
	 */
	public boolean loadDataIntoDB(String str[]) throws SQLException {

		String sqlSelectQuery = "select * from micdpf";
		String sqlDeleteQuery = "delete from micdpf";

		Connection conn = DBConnection.getConnection();

		try {
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;

			stmt.executeUpdate(sqlDeleteQuery);
			//rs = null;

			int temp = 0;
			PreparedStatement statmt =  conn.prepareStatement("Insert into MICDPF (MIDATA) values (?)");			
			for (int i = 0; i < str.length; i++) {				
				statmt.setString(1, str[i]);
				temp = statmt.executeUpdate();
			}

			rs = stmt.executeQuery(sqlSelectQuery);

			System.out.println("Inserted Table Data is ::");
			while (rs.next()) {
				System.out.println(rs.getString(2));

			}
			if (temp != 1) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			System.out.println("Unable to insert data into micdpf Table. ");
			e.printStackTrace();
		} finally {
			conn.commit();
			conn.close();
		}
		return false;

	}
	
	public boolean saveRJSFPFDataIntoDB(List<String> queryList) throws SQLException {

		String sqlDeleteQuery = "delete from RJSFPF";
		String query = "INSERT INTO RJSFPF (BATCH, CHDRNUM, APLNNO, MBRNO, DPNTNO, ORIGAMT, BANKCODE, PAYTYPE, CHQNUM, TCHQDATE, BANKKEY, ZCHQTYP ) VALUES ";
		Connection conn = DBConnection.getConnection();

		try {
			conn.setAutoCommit(false);
			Statement stmt = conn.createStatement();
			ResultSet rs = null;

			rs = stmt.executeQuery(sqlDeleteQuery);
			rs = null;
			int temp = 0; int recordInserted = 0;
			for (int i = 0; i < queryList.size(); i++) {
				System.out.println(query + "("+ queryList.get(i) + ")");
				temp = stmt.executeUpdate(query + "("+ queryList.get(i) + ")");
				recordInserted ++;
			}

			System.out.println("No of records inserted in table :: " +recordInserted);
			if (temp != 1) {
				return false;
			}
			return true;
		} catch (SQLException e) {
			System.out.println("Unable to insert data into micdpf Table. ");
			e.printStackTrace();
		} finally {
			conn.commit();
			conn.close();
		}
		return false;

	}
}
