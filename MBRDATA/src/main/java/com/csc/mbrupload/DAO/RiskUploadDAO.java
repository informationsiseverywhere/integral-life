package com.csc.mbrupload.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.csc.riskupload.util.FireRisk;
import com.csc.riskupload.util.FireRiskFio;
import com.csc.riskupload.util.FireRiskFir;
import com.csc.riskupload.util.FireRiskLpp;

/**
 * This program insert data into brstpf table
 * @author sgadkari
 * 
 */
public class RiskUploadDAO {
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getPrimaryKey()
	 */
	public String getPrimaryKey() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.gild.dao.BaseDAO#getTableName()
	 */
	public String getTableName() {
		return "COMMON";
	}

	private String bulkId;

	/**
	 * @param firstSheet
	 * @return
	 * @return
	 * @throws SQLException
	 */
	
	
	
	public boolean saveBRSTPFDataIntoDB(FireRisk fireRisk) throws SQLException {
		
		
		   
//		Connection conn = RUDBConnection.getConnection();
		Connection conn = DBConnection.getConnection();

		
		List<FireRiskFir> fireRiskFirList = fireRisk.getFireRisksFir().getFireRiskFirList();
		List<FireRiskFio> fireRiskFioList = fireRisk.getFireRisksFio().getFireRiskFioList();
		List<FireRiskLpp> fireRiskLppList = fireRisk.getFireRisksLpp().getFireRiskLppList();		
		
		
		String query;
		
		 int j=1;
			for(FireRiskFir fireRiskFir : fireRiskFirList){
				
				if(fireRiskFir.getPolicyNumber()!=null && !fireRiskFir.getPolicyNumber().isEmpty() &&
						fireRiskFir.getRiskType() != null && !fireRiskFir.getRiskType().isEmpty()){
				
				query = "INSERT INTO BRSTPF (POLICNO, RISKTYPE, RIMETH,STATECODE,REGSRCH, RATECDE, CONSYEAR,  INTCDE,ZITEMSI, PREMRATE,  SPRC,ZSITUA,BULKID,STATUS,BLKSNO,CEDEBS,CREATEDATIME) VALUES "+
						"( '"+fireRiskFir.getPolicyNumber()+"','"+fireRiskFir.getRiskType()+"','"+fireRiskFir.getReinsuranceMethod()+
						"','"+fireRiskFir.getState()+"','"+fireRiskFir.getRegister()+"','"+fireRiskFir.getRiskRatingCode()
						+"','"+fireRiskFir.getConstructionYear()+"','"+fireRiskFir.getInterestInsured()+"','"+fireRiskFir.getSumInsured()
						+"','"+fireRiskFir.getPremiumRate()+"','"+fireRiskFir.getPremiumClass()+"','"+fireRiskFir.getSituation()+"','"+getBulkId()+"','0','"+j+"','"+fireRiskFir.getRiCedingBasis()+"',current_timestamp)";
				
				System.out.println(query);
				j++;
				
			try {
				conn.setAutoCommit(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = null;			
				rs = null;
				int temp = 0; 
				temp=stmt.executeUpdate(query);
				if (temp != 1) {
					return false;
				}
				
			}				
		catch (SQLException e) {
			System.out.println("Unable to insert data into brst Table. ");
			e.printStackTrace();
		} finally {
			
		}	
			}}
			
			conn.commit();
      for(FireRiskFio fireRiskFio : fireRiskFioList){
    	  if(fireRiskFio.getPolicyNumber()!=null && !fireRiskFio.getPolicyNumber().isEmpty() &&
    			  fireRiskFio.getRiskType() != null && !fireRiskFio.getRiskType().isEmpty()){
				query = "INSERT INTO BRSTPF (POLICNO, RISKTYPE, RIMETH,STATECODE,REGSRCH, RATECDE, CONSYEAR,  INTCDE,ZITEMSI, PREMRATE,  SPRC,ZSITUA,BULKID,STATUS,BLKSNO,CEDEBS,CREATEDATIME) VALUES "+
						"( '"+fireRiskFio.getPolicyNumber()+"','"+fireRiskFio.getRiskType()+"','"+fireRiskFio.getReinsuranceMethod()+
						"','"+fireRiskFio.getState()+"','"+fireRiskFio.getRegister()+"','"+fireRiskFio.getRiskRatingCode()
						+"','"+fireRiskFio.getConstructionYear()+"','"+fireRiskFio.getInterestInsured()+"','"+fireRiskFio.getSumInsured()
						+"','"+fireRiskFio.getPremiumRate()+"','"+fireRiskFio.getPremiumClass()+"','"+fireRiskFio.getSituation()+"','"+getBulkId()+"','0','"+j+"','"+fireRiskFio.getRiCedingBasis()+"',current_timestamp)";
				
				System.out.println(query);
				j++;
				
			try {
				conn.setAutoCommit(false);
				Statement stmt = conn.createStatement();
				ResultSet rs = null;			
				rs = null;
				int temp = 0; 
				temp=stmt.executeUpdate(query);
				if (temp != 1) {
					return false;
				}
				
			}				
		catch (SQLException e) {
			System.out.println("Unable to insert data into brst Table. ");
			e.printStackTrace();
		} finally {
			
		}	}
			}
conn.commit();
for(FireRiskLpp fireRiskLpp : fireRiskLppList){
	 if(fireRiskLpp.getPolicyNumber()!=null && !fireRiskLpp.getPolicyNumber().isEmpty() &&
			 fireRiskLpp.getRiskType() != null && !fireRiskLpp.getRiskType().isEmpty()){
	
	query = "INSERT INTO BRSTPF (POLICNO, RISKTYPE, RIMETH, RATECDE,LIMAOP,ZITEMSI, PREMRATE,  SPRC,BULKID,STATUS,BLKSNO,CREATEDATIME) VALUES "+
			"( '"+fireRiskLpp.getPolicyNumber()+"','"+fireRiskLpp.getRiskType()+"','"+fireRiskLpp.getReinsuranceMethod()+
			"','"+fireRiskLpp.getBase()+"','"+fireRiskLpp.getAop()+"','"+fireRiskLpp.getBaseAmount()
			+"','"+fireRiskLpp.getRate()+"','"+fireRiskLpp.getPremiumClass()+"','"+getBulkId()+"','0','"+j+"',current_timestamp)";
	
	System.out.println(query);
	j++;
	
try {
	conn.setAutoCommit(false);
	Statement stmt = conn.createStatement();
	ResultSet rs = null;			
	rs = null;
	int temp = 0;
	temp=stmt.executeUpdate(query);
	if (temp != 1) {
		return false;
	}
	
}				
catch (SQLException e) {
System.out.println("Unable to insert data into brst Table. ");
e.printStackTrace();
} finally {

}	
}}
conn.commit();  
			conn.close();
		return true;

	}
	

	public String getBulkId() {
		return bulkId;
	}
	
	public void setBulkId(String bulkId) {
		this.bulkId = bulkId;
	}


}
	




	
	
	

