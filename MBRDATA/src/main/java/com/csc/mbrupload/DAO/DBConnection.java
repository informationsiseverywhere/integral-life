package com.csc.mbrupload.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;

import com.csc.riskupload.util.Config;



public class DBConnection {

	public DBConnection() {
	}
	
	public static Connection getConnection() {
		Properties props = Config.loadConfiguration();
		String db_connection_driver = props.getProperty("connection.driver");
		String db_connect_string = props.getProperty("connection.url");
		String db_userid = props.getProperty("connection.username");
		String db_password = props.getProperty("connection.password");
		
		Connection conn = null;
		try {
			 //Class class1 = ClassLoader.getSystemClassLoader().loadClass("oracle.jdbc.driver.OracleDriver");
			 Class class1 =  Class.forName(db_connection_driver);
			 
			conn = DriverManager.getConnection(db_connect_string,db_userid, db_password);
			return conn;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	 
	
//	public static void closeConnection() {
//	
//		if (conn == null) {
//			return;
//		}
//
//		try {
//			if (!conn.isClosed()) {
//				conn.close();
//			}
//		} catch (SQLException ex) {
//			logger.error(
//					"Error occurs when attempting to close the database connection.",
//					ex);
//		}
//
//	}
}



 
