package com.csc.mbrupload.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;

/**
 * @author gkashyap2
 *
 */
public class LifeUploadDAO {

      /*
       * (non-Javadoc)
       *
       * @see com.csc.gild.dao.BaseDAO#getPrimaryKey()
       */
      public String getPrimaryKey() {
       return "";
      }

      /*
       * (non-Javadoc)
       *
       * @see com.csc.gild.dao.BaseDAO#getTableName()
       */
      public String getTableName() {
       return "COMMON";
      }

      /**
       * @param firstSheet
       * @return
       * @return
       * @throws SQLException
       */
      public boolean loadDataIntoHRATPF(List<String> queryList) throws SQLException {

       String sqlDeleteQuery = "delete from HRATPF";
       String query = "INSERT INTO HRATPF (ITEMITEM,MFACTHM,MFACTHY,MFACTM,MFACTQ,MFACTW,MFACTWB,MFACTWD,MFACTY,PMUNIT,UNIT," +
       		"DISCNTMETH,INSPRM01,INSPRM02,INSPRM03,INSPRM04,INSPRM05,INSPRM06,INSPRM07,INSPRM08,INSPRM09,INSPRM10,INSPRM11,INSPRM12," +
       		"INSPRM13,INSPRM14,INSPRM15,INSPRM16,INSPRM17,INSPRM18,INSPRM19,INSPRM20,INSPRM21,INSPRM22,INSPRM23,INSPRM24,INSPRM25," +
       		"INSPRM26,INSPRM27,INSPRM28,INSPRM29,INSPRM30,INSPRM31,INSPRM32,INSPRM33,INSPRM34,INSPRM35,INSPRM36,INSPRM37,INSPRM38," +
       		"INSPRM39,INSPRM40,INSPRM41,INSPRM42,INSPRM43,INSPRM44,INSPRM45,INSPRM46,INSPRM47,INSPRM48,INSPRM49,INSPRM50,INSPRM51," +
       		"INSPRM52,INSPRM53,INSPRM54,INSPRM55,INSPRM56,INSPRM57,INSPRM58,INSPRM59,INSPRM60,INSPRM61,INSPRM62,INSPRM63,INSPRM64," +
       		"INSPRM65,INSPRM66,INSPRM67,INSPRM68,INSPRM69,INSPRM70,INSPRM71,INSPRM72,INSPRM73,INSPRM74,INSPRM75,INSPRM76,INSPRM77," +
       		"INSPRM78,INSPRM79,INSPRM80,INSPRM81,INSPRM82,INSPRM83,INSPRM84,INSPRM85,INSPRM86,INSPRM87,INSPRM88,INSPRM89,INSPRM90," +
       		"INSPRM91,INSPRM92,INSPRM93,INSPRM94,INSPRM95,INSPRM96,INSPRM97,INSPRM98,INSPRM99,USRPRF,JOBNM,DATIME,INSPREM,INSTPR01," +
       		"INSTPR02,INSTPR03,INSTPR04,INSTPR05,INSTPR06,INSTPR07,INSTPR08,INSTPR09,INSTPR10,INSTPR11 ) VALUES ";
       Connection conn = DBConnection.getConnection();

       try {
           conn.setAutoCommit(false);
           Statement stmt = conn.createStatement();
           ResultSet rs = null;

           rs = stmt.executeQuery(sqlDeleteQuery);
           rs = null;
           int temp = 0; int recordInserted = 0;
           for (int i = 0; i < queryList.size(); i++) {    
        	String sub=queryList.subList(0,queryList.size()).get(i).substring(0, 4);
        	   if(sub.equals("null")){        		  
        		   break;
        	   }
               System.out.println(query + "("+ queryList.get(i) + ")");
               temp = stmt.executeUpdate(query + "("+ queryList.get(i) + ")");
               recordInserted ++;
           }

           System.out.println("No of records inserted in table :: " +recordInserted);
           if (temp != 1) {
               return false;
           }
           return true;
       } catch (SQLException e) {
           System.out.println("Unable to insert data into HRATPF Table. ");
           e.printStackTrace();
       } finally {
           conn.commit();
           conn.close();
       }
       return false;
      }

      public boolean loadDIVRPFDataIntoDB(List<String> queryList) throws SQLException {

       String sqlDeleteQuery = "delete from DIVRPF";
       String query = "INSERT INTO DIVRPF (ITEMITEM,PMUNIT,UNIT,INSPRM01,INSPRM02,INSPRM03,INSPRM04,INSPRM05,INSPRM06,INSPRM07," +
       		"INSPRM08,INSPRM09,INSPRM10,INSPRM11,INSPRM12,INSPRM13,INSPRM14,INSPRM15,INSPRM16,INSPRM17,INSPRM18,INSPRM19,INSPRM20,INSPRM21," +
       		"INSPRM22,INSPRM23,INSPRM24,INSPRM25,INSPRM26,INSPRM27,INSPRM28,INSPRM29,INSPRM30,INSPRM31,INSPRM32,INSPRM33,INSPRM34,INSPRM35," +
       		"INSPRM36,INSPRM37,INSPRM38,INSPRM39,INSPRM40,INSPRM41,INSPRM42,INSPRM43,INSPRM44,INSPRM45,INSPRM46,INSPRM47,INSPRM48,INSPRM49," +
       		"INSPRM50,INSPRM51,INSPRM52,INSPRM53,INSPRM54,INSPRM55,INSPRM56,INSPRM57,INSPRM58,INSPRM59,INSPRM60,INSPRM61,INSPRM62,INSPRM63," +
       		"INSPRM64,INSPRM65,INSPRM66,INSPRM67,INSPRM68,INSPRM69,INSPRM70,INSPRM71,INSPRM72,INSPRM73,INSPRM74,INSPRM75,INSPRM76,INSPRM77," +
       		"INSPRM78,INSPRM79,INSPRM80,INSPRM81,INSPRM82,INSPRM83,INSPRM84,INSPRM85,INSPRM86,INSPRM87,INSPRM88,INSPRM89,INSPRM90,INSPRM91," +
       		"INSPRM92,INSPRM93,INSPRM94,INSPRM95,INSPRM96,INSPRM97,INSPRM98,INSPRM99,USRPRF,JOBNM,DATIME,INSTPR01,INSTPR02,INSTPR03,INSTPR04," +
       		"INSTPR05,INSTPR06,INSTPR07,INSTPR08,INSTPR09,INSTPR10,INSTPR11) VALUES ";
       Connection conn = DBConnection.getConnection();

       try {
           conn.setAutoCommit(false);
           Statement stmt = conn.createStatement();
           ResultSet rs = null;

           rs = stmt.executeQuery(sqlDeleteQuery);
           rs = null;
           int temp = 0; int recordInserted = 0;
           for (int i = 0; i < queryList.size(); i++) {
        	   String sub=queryList.subList(0,queryList.size()).get(i).substring(0, 4);
        	   if(sub.equals("null")){        		  
        		   break;
        	   }
               System.out.println(query + "("+ queryList.get(i) + ")");
               temp = stmt.executeUpdate(query + "("+ queryList.get(i) + ")");
               recordInserted ++;
           }

           System.out.println("No of records inserted in table :: " +recordInserted);
           if (temp != 1) {
               return false;
           }
           return true;
       } catch (SQLException e) {
           System.out.println("Unable to insert data into DIVRPF Table. ");
           e.printStackTrace();
       } finally {
           conn.commit();
           conn.close();
       }
       return false;

      }
      
      public boolean loadADRSPFDataIntoDB(List<String> queryList) throws SQLException {

    	  System.setProperty("file.encoding" , "UTF-8");
    	  Date currDate = new Date();
    	  String sqlDeleteQuery = "delete from ADRSPF";
          StringBuilder sql = new StringBuilder();
          sql.append("INSERT INTO ADRSPF");
  		sql.append("(ADDRCD,ADDRCDNW,KPOSTCD,BARCD,BARCDLEN,POSTCDINFA,POSTCDINFB,");
  		sql.append("PACFLAG,PACCD,PRFCTIND,PRFCT,CITY,OAZA,CHOME,PRFCTLEN,CITYLEN,");
  		sql.append("OAZALEN,CHOMELEN,TTLADLEN,KJPRFCT,KJCITY,KJOAZA,KJCHOME,KJPRFCTLEN,KJCITYLEN,KJOAZALEN,");
  		sql.append("KJCHOMELEN,KJTTLADLEN,JISLVL01,JISLVL02,JISLVL03,JISLVL04,JISLVL05,JISLVL06,JISLVL07,OAZAFLAG,");
  		sql.append("AZAFLAG,ADALFLAG,ALIASFLAG,EFCTYYYYMM,ABRGYYYYMM,ADCDYYYYMM,ADDRYYYYMM,POSTYYYYMM,BARYYYYMM,PACYYYYMM,");
  		sql.append("ALIYYYYMM,CHOYYYMM,POSTCDOLD,ADDRFIL,MAINTCD,USRPRF,JOBNM,DATIME)");
  		sql.append("VALUES ");
  		String query=sql.toString();
          Connection conn = DBConnection.getConnection();

          try {
              conn.setAutoCommit(false);
              Statement stmt = conn.createStatement();
              ResultSet rs = null;

              rs = stmt.executeQuery(sqlDeleteQuery);
              rs = null;
              int temp = 0; int recordInserted = 0;
              for (int i = 0; i < queryList.size(); i++) {
           	   String sub=queryList.subList(0,queryList.size()).get(i).substring(0, 4);
           	   if(sub.equals("null")){        		  
           		   break;
           	   }
                  System.out.println(query + "("+ queryList.get(i) + ")");
                  temp = stmt.executeUpdate(query + "("+ queryList.get(i) + ")");
                  recordInserted ++;
              }

              System.out.println("No of records inserted in table :: " +recordInserted);
              if (temp != 1) {
                  return false;
              }
              return true;
          } catch (SQLException e) {
              System.out.println("Unable to insert data into DIVRPF Table. ");
              e.printStackTrace();
          } finally {
              conn.commit();
              conn.close();
          }
          return false;

         }
}
