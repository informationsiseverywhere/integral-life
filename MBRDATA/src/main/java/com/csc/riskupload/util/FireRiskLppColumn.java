package com.csc.riskupload.util;

public enum FireRiskLppColumn {
	
	Policy_Number,
    Risk_Type,
    Reinsurance_Method,
   Premium_Class,
    Territorial_Limit,
    AOP,
    Base,
    Rate,
    Base_Amount
}
