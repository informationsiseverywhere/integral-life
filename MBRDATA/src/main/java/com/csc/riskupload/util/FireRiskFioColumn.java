package com.csc.riskupload.util;

public enum FireRiskFioColumn {
	
	Policy_Number,
    Risk_Type,
    Reinsurance_Method,
    State,
    Register,
    Risk_Rating_Code,
    Construction_Year,
    Interest_Insured,
    Sum_Insured,
    Premium_Rate,
    Premium_Class,    
    Situation,
    RI_Ceding_Basis,
}
