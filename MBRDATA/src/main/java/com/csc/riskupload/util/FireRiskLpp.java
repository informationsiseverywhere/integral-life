package com.csc.riskupload.util;

public class FireRiskLpp extends FireRisk{
    
    private String policyNumber;
    private String riskType;
    private String reinsuranceMethod;
    private String baseAmount;
    private String territorialLimit;
    private String aop;
    private String base;
    private String rate;
    private String premiumClass;
    
	/**
	 * @return the policyNumber
	 */

	public String getPolicyNumber() {
		return policyNumber;
	}
	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	/**
	 * @return the riskType
	 */
	public String getRiskType() {
		return riskType;
	}
	/**
	 * @param riskType the riskType to set
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	/**
	 * @return the reinsuranceMethod
	 */
	public String getReinsuranceMethod() {
		return reinsuranceMethod;
	}
	/**
	 * @param reinsuranceMethod the reinsuranceMethod to set
	 */
	public void setReinsuranceMethod(String reinsuranceMethod) {
		this.reinsuranceMethod = reinsuranceMethod;
	}
	/**
	 * @return the baseAmount
	 */
	public String getBaseAmount() {
		return baseAmount;
	}
	/**
	 * @param baseAmount the baseAmount to set
	 */
	public void setBaseAmount(String baseAmount) {
		this.baseAmount = baseAmount;
	}
	/**
	 * @return the territorialLimit
	 */
	public String getTerritorialLimit() {
		return territorialLimit;
	}
	/**
	 * @param territorialLimit the territorialLimit to set
	 */
	public void setTerritorialLimit(String territorialLimit) {
		this.territorialLimit = territorialLimit;
	}
	/**
	 * @return the aop
	 */
	public String getAop() {
		return aop;
	}
	/**
	 * @param aop the aop to set
	 */
	public void setAop(String aop) {
		this.aop = aop;
	}
	/**
	 * @return the base
	 */
	public String getBase() {
		return base;
	}
	/**
	 * @param base the base to set
	 */
	public void setBase(String base) {
		this.base = base;
	}
	/**
	 * @return the rate
	 */
	public String getRate() {
		return rate;
	}
	/**
	 * @param rate the rate to set
	 */
	public void setRate(String rate) {
		this.rate = rate;
	}
	/**
	 * @return the premiumClass
	 */
	public String getPremiumClass() {
		return premiumClass;
	}
	/**
	 * @param premiumClass the premiumClass to set
	 */
	public void setPremiumClass(String premiumClass) {
		this.premiumClass = premiumClass;
	}
  
     
}
