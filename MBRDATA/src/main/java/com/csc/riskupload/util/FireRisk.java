package com.csc.riskupload.util;

public class FireRisk {

	private FireRisksFir fireRisksFir;
	private FireRisksFio fireRisksFio;
	private FireRisksLpp fireRisksLpp;	
	
	
	/**
	 * @return the fireRisksFir
	 */
	
	public FireRisksFir getFireRisksFir() {
		return fireRisksFir;
	}
	/**
	 * @param fireRisksFir the fireRisksFir to set
	 */
	public void setFireRisksFir(FireRisksFir fireRisksFir) {
		this.fireRisksFir = fireRisksFir;
	}
	/**
	 * @return the fireRiskFioList
	 */
	public FireRisksFio getFireRisksFio() {
		return fireRisksFio;
	}
	/**
	 * @param fireRisksFio the fireRisksFio to set
	 */
	public void setFireRisksFio(FireRisksFio fireRisksFio) {
		this.fireRisksFio = fireRisksFio;
	}
	/**
	 * @return the fireRisksLpp
	 */
	public FireRisksLpp getFireRisksLpp() {
		return fireRisksLpp;
	}
	/**
	 * @param fireRisksLpp the fireRisksLpp to set
	 */
	public void setFireRisksLpp(FireRisksLpp fireRisksLpp) {
		this.fireRisksLpp = fireRisksLpp;
	}
	
}
