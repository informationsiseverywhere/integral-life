package com.csc.riskupload.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.encryption.pbe.config.EnvironmentStringPBEConfig;
import org.jasypt.properties.EncryptableProperties;

import com.csc.mbrupload.DAO.DBConnection;

public class Config {
	private static Logger logger = LogManager.getLogger(Config.class);
	
	public static Properties loadConfiguration(){
		Properties props = new Properties();
		try {
			props.load(DBConnection.class.getClassLoader().getResourceAsStream("config/MBRDATA.properties"));
		} catch (IOException e) {
			logger.error("Error occurs when attempting to load the resource file.", e);
		}
		
		if(props.getProperty("mbrdata.encryption").trim().equals("Y")){
			return loadProtectedConfiguration(props.getProperty("mbrdata.encryptionalgorithm").trim(), props.getProperty("mbrdata.var").trim());
		} else{
			return props;
		}
	}
	
	private static Properties loadProtectedConfiguration(String algorithm, String evnVar){
		StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
		EnvironmentStringPBEConfig envConfig = new EnvironmentStringPBEConfig();
		envConfig.setAlgorithm(algorithm);
		envConfig.setPasswordSysPropertyName(evnVar);
		encryptor.setConfig(envConfig);
		Properties props = new EncryptableProperties(encryptor);
		try {
			props.load(Config.class.getClassLoader().getResourceAsStream("config/MBRDATA.properties"));
		}catch (IOException ex) {
			logger.error("Error occurs when attempting to load the resource file.", ex);
		}
		
		return props;
	}

}
