package com.csc.riskupload.util;

import java.util.List;


public class FireRisksFio {

	private List <FireRiskFio> fireRiskFioList;
	
	

	/**
	 * @return the fireRiskFioList
	 */

	public List<FireRiskFio> getFireRiskFioList() {
		return fireRiskFioList;
	}

	/**
	 * @param fireRiskFioList the fireRiskFioList to set
	 */
	public void setFireRiskFioList(List<FireRiskFio> fireRiskFioList) {
		this.fireRiskFioList = fireRiskFioList;
	}
	
	
     
}
