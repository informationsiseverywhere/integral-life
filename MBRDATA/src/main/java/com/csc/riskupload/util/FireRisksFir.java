package com.csc.riskupload.util;

import java.util.List;


public class FireRisksFir {

	private List <FireRiskFir> fireRiskFirList;
	
	

	/**
	 * @return the fireRiskFirList
	 */
	public List<FireRiskFir> getFireRiskFirList() {
		return fireRiskFirList;
	}

	/**
	 * @param fireRiskFirList the fireRiskFirList to set
	 */
	public void setFireRiskFirList(List<FireRiskFir> fireRiskFirList) {
		this.fireRiskFirList = fireRiskFirList;
	}	
     
}
