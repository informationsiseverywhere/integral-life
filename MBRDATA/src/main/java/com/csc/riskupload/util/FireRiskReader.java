package com.csc.riskupload.util;
/*
 * Read data from excel.
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
public class FireRiskReader {
	
	public static List<FireRiskFir> getFireRiskFirList(Sheet sheet) {
        List<FireRiskFir> fireRiskFirList = new ArrayList<FireRiskFir>();
        
        int policyNumberIndex = 0;
        int riskTypeIndex = 0;
        int reinsuranceMethodIndex = 0;
        int stateIndex = 0;
        int registerIndex = 0;
        int riskRatingCodeIndex = 0;
        int constructionYearIndex = 0;
        int interestInsuredIndex = 0;
        int sumInsuredIndex = 0;
        int premiumRateIndex = 0;
        int premiumClassIndex = 0;   
        int situationIndex = 0;
        int riCedingBasisIndex = 0;

        			int totalRows = sheet.getLastRowNum() + 1;
        			int totalColumns = sheet.getRow(0).getLastCellNum();
        			for(int header = 0; header < 1; header++){
        			    Row row = sheet.getRow(header);
        			        for (int j = 0; j < totalColumns; j++){
        			            Cell cell = row.getCell(j);
        			            String columnName = "";
        			            if (null != cell){
        			            	columnName = cell.getStringCellValue(); 
        			            	FireRiskFirColumn currentColumn  = FireRiskFirColumn.valueOf(columnName);   
        			            	switch (currentColumn) {
            			            case Policy_Number:
            			            	policyNumberIndex = cell.getColumnIndex();
            			                break;
            			            case Risk_Type:
            			            	riskTypeIndex = cell.getColumnIndex();
            			                break;
            			            case Reinsurance_Method:
            			            	reinsuranceMethodIndex = cell.getColumnIndex();
            			                break;
            			            case State:
            			            	stateIndex = cell.getColumnIndex();
            			                break;
            			            case Register:
            			            	registerIndex = cell.getColumnIndex();
            			                break;
            			            case Risk_Rating_Code:
            			            	riskRatingCodeIndex = cell.getColumnIndex();
            			                break;
            			            case Construction_Year:
            			            	constructionYearIndex = cell.getColumnIndex();
            			                break;
            			            case Interest_Insured:
            			            	interestInsuredIndex = cell.getColumnIndex();
            			                break;
            			            case Sum_Insured:
            			            	sumInsuredIndex = cell.getColumnIndex();
            			                break;
            			            case Premium_Rate:
            			            	premiumRateIndex = cell.getColumnIndex();
            			                break;
            			            case Premium_Class:
            			            	premiumClassIndex = cell.getColumnIndex();
            			                break;
            			            case RI_Ceding_Basis:
            			            	riCedingBasisIndex = cell.getColumnIndex();
            			                break;            			            
            			            case Situation:
            			            	situationIndex=cell.getColumnIndex();
            			            	break;
            			            default:
            			                throw new IllegalArgumentException("Invalid column: "+currentColumn);        			           
        			            }
        			          }        			        	
        			        }
        			}
        			for(int record = 1; record < totalRows; record++){
        				String policyNumber = "";
        			    String riskType = "";
        			    String reinsuranceMethod = "";
        			    String state = "";
        			    String register = "";
        			    String riskRatingCode = "";
        			    String constructionYear = "";
        			    String interestInsured = "";
        			    String sumInsured = "";
        			    String premiumRate = "";
        			    String premiumClass = "";
        			    String riCedingBasis = "";
        			    String situation="";
        			    
        			    String value = "";
        			    Row row = sheet.getRow(record);
        			    if (null != row) {
        			    	 for (int j = 0; j < totalColumns; j++){
         			            if (j == policyNumberIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	policyNumber = value;
         			            }
         			            if (j == riskTypeIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);	            	
         			            	riskType = value;
         			            }
         			            if (j == reinsuranceMethodIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	reinsuranceMethod = value;
         			            }
         			           if (j == stateIndex) {
        			            	Cell cell = row.getCell(j);        			            	
        			            	value = getCellValueAsString(cell);
        			            	state = value;
        			            }
        			            if (j == registerIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);		            	
        			            	register = value;
        			            }
        			            if (j == riskRatingCodeIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	riskRatingCode = value;
        			            }
        			            if (j == constructionYearIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	constructionYear = value;
         			            }
         			            if (j == interestInsuredIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);		            	
         			            	interestInsured = value;
         			            }
         			            if (j == sumInsuredIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	sumInsured = value;
         			            }
         			           if (j == premiumRateIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	premiumRate = value;
        			            }
        			            if (j == premiumClassIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);		            	
        			            	premiumClass = value;
        			            }
        			            if (j == riCedingBasisIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	riCedingBasis = value;
        			            }
        			            
        			            if (j == situationIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	situation = value;
        			            }
         			        }
        			    }
        			       
        			        FireRiskFir fireRiskFir = new FireRiskFir();
        			        fireRiskFir.setPolicyNumber(policyNumber);
        			        fireRiskFir.setRiskType(riskType);
        			        fireRiskFir.setReinsuranceMethod(reinsuranceMethod);
        			        fireRiskFir.setState(state);
        			        fireRiskFir.setRegister(register);
        			        fireRiskFir.setRiskRatingCode(riskRatingCode);
        			        fireRiskFir.setConstructionYear(constructionYear);
        			        fireRiskFir.setInterestInsured(interestInsured);
        			        fireRiskFir.setSumInsured(sumInsured);
        			        fireRiskFir.setPremiumRate(premiumRate);
        			        fireRiskFir.setPremiumClass(premiumClass);
        			        fireRiskFir.setRiCedingBasis(riCedingBasis);        			        
        			        fireRiskFir.setSituation(situation);
        			        
        			        
        			        fireRiskFirList.add(fireRiskFir);
        			}               
        return fireRiskFirList;
    }		
	
	public static List<FireRiskFio> getFireRiskFioList(Sheet sheet) {
        List<FireRiskFio> fireRiskFioList = new ArrayList<FireRiskFio>();
        
        int policyNumberIndex = 0;
        int riskTypeIndex = 0;
        int reinsuranceMethodIndex = 0;
        int stateIndex = 0;
        int registerIndex = 0;
        int riskRatingCodeIndex = 0;
        int constructionYearIndex = 0;
        int interestInsuredIndex = 0;
        int sumInsuredIndex = 0;
        int premiumRateIndex = 0;
        int premiumClassIndex = 0;
        int riCedingBasisIndex = 0;
        int situationIndex = 0;

       

        			int totalRows = sheet.getLastRowNum() + 1;
        			int totalColumns = sheet.getRow(0).getLastCellNum();
        			for(int header = 0; header < 1; header++){
        			    Row row = sheet.getRow(header);
        			        for (int j = 0; j < totalColumns; j++){
        			            Cell cell = row.getCell(j);
        			            String columnName = "";
        			            if (null != cell){
        			            	columnName = cell.getStringCellValue(); 
        			            	FireRiskFioColumn currentColumn  = FireRiskFioColumn.valueOf(columnName);   
        			            	switch (currentColumn) {
            			            case Policy_Number:
            			            	policyNumberIndex = cell.getColumnIndex();
            			                break;
            			            case Risk_Type:
            			            	riskTypeIndex = cell.getColumnIndex();
            			                break;
            			            case Reinsurance_Method:
            			            	reinsuranceMethodIndex = cell.getColumnIndex();
            			                break;
            			            case State:
            			            	stateIndex = cell.getColumnIndex();
            			                break;
            			            case Register:
            			            	registerIndex = cell.getColumnIndex();
            			                break;
            			            case Risk_Rating_Code:
            			            	riskRatingCodeIndex = cell.getColumnIndex();
            			                break;
            			            case Construction_Year:
            			            	constructionYearIndex = cell.getColumnIndex();
            			                break;
            			            case Interest_Insured:
            			            	interestInsuredIndex = cell.getColumnIndex();
            			                break;
            			            case Sum_Insured:
            			            	sumInsuredIndex = cell.getColumnIndex();
            			                break;
            			            case Premium_Rate:
            			            	premiumRateIndex = cell.getColumnIndex();
            			                break;
            			            case Premium_Class:
            			            	premiumClassIndex = cell.getColumnIndex();
            			                break;
            			            case RI_Ceding_Basis:
            			            	riCedingBasisIndex = cell.getColumnIndex();
            			                break;
            			            
            			            case Situation:
            			            	situationIndex=cell.getColumnIndex();
            			            	break;
            			            default:
            			                throw new IllegalArgumentException("Invalid column: "+currentColumn);        			           
        			            }
        			          }        			        	
        			        }
        			}
        			for(int record = 1; record < totalRows; record++){
        				String policyNumber = "";
        			    String riskType = "";
        			    String reinsuranceMethod = "";
        			    String state = "";
        			    String register = "";
        			    String riskRatingCode = "";
        			    String constructionYear = "";
        			    String interestInsured = "";
        			    String sumInsured = "";
        			    String premiumRate = "";
        			    String premiumClass = "";
        			    String riCedingBasis = "";
        			    String situation="";
        			    
        			    String value = "";
        			    Row row = sheet.getRow(record);
        			    if (null != row) {
        			    	 for (int j = 0; j < totalColumns; j++){
         			            if (j == policyNumberIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	policyNumber = value;
         			            }
         			            if (j == riskTypeIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);	            	
         			            	riskType = value;
         			            }
         			            if (j == reinsuranceMethodIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	reinsuranceMethod = value;
         			            }
         			           if (j == stateIndex) {
        			            	Cell cell = row.getCell(j);        			            	
        			            	value = getCellValueAsString(cell);
        			            	state = value;
        			            }
        			            if (j == registerIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);		            	
        			            	register = value;
        			            }
        			            if (j == riskRatingCodeIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	riskRatingCode = value;
        			            }
        			            if (j == constructionYearIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	constructionYear = value;
         			            }
         			            if (j == interestInsuredIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);		            	
         			            	interestInsured = value;
         			            }
         			            if (j == sumInsuredIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	sumInsured = value;
         			            }
         			           if (j == premiumRateIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	premiumRate = value;
        			            }
        			            if (j == premiumClassIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);		            	
        			            	premiumClass = value;
        			            }
        			            if (j == riCedingBasisIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	riCedingBasis = value;
        			            }
        			            if (j == situationIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	situation = value;
        			            }
         			        }
        			    }
        			       
        			        FireRiskFio fireRiskFio = new FireRiskFio();
        			        fireRiskFio.setPolicyNumber(policyNumber);
        			        fireRiskFio.setRiskType(riskType);
        			        fireRiskFio.setReinsuranceMethod(reinsuranceMethod);
        			        fireRiskFio.setState(state);
        			        fireRiskFio.setRegister(register);
        			        fireRiskFio.setRiskRatingCode(riskRatingCode);
        			        fireRiskFio.setConstructionYear(constructionYear);
        			        fireRiskFio.setInterestInsured(interestInsured);
        			        fireRiskFio.setSumInsured(sumInsured);
        			        fireRiskFio.setPremiumRate(premiumRate);
        			        fireRiskFio.setPremiumClass(premiumClass);
        			        fireRiskFio.setRiCedingBasis(riCedingBasis);        			       
        			        fireRiskFio.setSituation(situation);
        			        
        			        fireRiskFioList.add(fireRiskFio);
        			}
           
        return fireRiskFioList;
    }
	
	public static List<FireRiskLpp> getFireRiskLppList(Sheet sheet) {
        List<FireRiskLpp> fireRiskLppList = new ArrayList<FireRiskLpp>();
        
        int policyNumberIndex = 0;
        int riskTypeIndex = 0;
        int reinsuranceMethodIndex = 0;
        int baseAmountIndex = 0;
        int territorialLimitIndex = 0;
        int aopIndex = 0;
        int baseIndex = 0;
        int rateIndex = 0;
        int premiumClassIndex = 0;

        			int totalRows = sheet.getLastRowNum() + 1;
        			int totalColumns = sheet.getRow(0).getLastCellNum();
        			for(int header = 0; header < 1; header++){
        			    Row row = sheet.getRow(header);
        			        for (int j = 0; j < totalColumns; j++){
        			            Cell cell = row.getCell(j);
        			            String columnName = "";
        			            if (null != cell){
        			            	columnName = cell.getStringCellValue(); 
        			            	FireRiskLppColumn currentColumn  = FireRiskLppColumn.valueOf(columnName);   
        			            	switch (currentColumn) {
            			            case Policy_Number:
            			            	policyNumberIndex = cell.getColumnIndex();
            			                break;
            			            case Risk_Type:
            			            	riskTypeIndex = cell.getColumnIndex();
            			                break;
            			            case Reinsurance_Method:
            			            	reinsuranceMethodIndex = cell.getColumnIndex();
            			                break;
            			            case Base_Amount:
            			            	baseAmountIndex = cell.getColumnIndex();
            			                break;
            			            case Territorial_Limit:
            			            	territorialLimitIndex = cell.getColumnIndex();
            			                break;
            			            case AOP:
            			            	aopIndex = cell.getColumnIndex();
            			                break;
            			            case Base:
            			            	baseIndex = cell.getColumnIndex();
            			                break;
            			            case Rate:
            			            	rateIndex = cell.getColumnIndex();
            			                break;
            			            case Premium_Class:
            			            	premiumClassIndex = cell.getColumnIndex();
            			                break;
            			            default:
            			                throw new IllegalArgumentException("Invalid column: "+currentColumn);        			           
        			            }
        			          }        			        	
        			        }
        			}
        			for(int record = 1; record < totalRows; record++){
        				String policyNumber = "";
        			    String riskType = "";
        			    String reinsuranceMethod = "";
        			    String baseAmount = "";
        			    String territorialLimit = "";
        			    String aop = "";
        			    String base = "";
        			    String rate = "";
        			    String riCedingBasis = "";
        			    String premiumClass = "";
        			    
        			    
        			    String value = "";
        			    Row row = sheet.getRow(record);
        			    if (null != row) {
        			    	 for (int j = 0; j < totalColumns; j++){
         			            if (j == policyNumberIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	policyNumber = value;
         			            }
         			            if (j == riskTypeIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);	            	
         			            	riskType = value;
         			            }
         			            if (j == reinsuranceMethodIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	reinsuranceMethod = value;
         			            }
         			           if (j == baseAmountIndex) {
        			            	Cell cell = row.getCell(j);        			            	
        			            	value = getCellValueAsString(cell);
        			            	baseAmount = value;
        			            }
        			            if (j == territorialLimitIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);		            	
        			            	territorialLimit = value;
        			            }
        			            if (j == aopIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	aop = value;
        			            }
        			            if (j == baseIndex) {
         			            	Cell cell = row.getCell(j);
         			            	value = getCellValueAsString(cell);
         			            	base = value;
         			            }
         			           if (j == rateIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	rate = value;
        			            }
        			            if (j == premiumClassIndex) {
        			            	Cell cell = row.getCell(j);
        			            	value = getCellValueAsString(cell);
        			            	premiumClass = value;
        			            }
         			        }
        			    }
        			       
        			        FireRiskLpp fireRiskLpp = new FireRiskLpp();
        			        fireRiskLpp.setPolicyNumber(policyNumber);
        			        fireRiskLpp.setRiskType(riskType);
        			        fireRiskLpp.setReinsuranceMethod(reinsuranceMethod);
        			        fireRiskLpp.setBaseAmount(baseAmount);
        			        fireRiskLpp.setTerritorialLimit(territorialLimit);
        			        fireRiskLpp.setAop(aop);
        			        fireRiskLpp.setBase(base);
        			        fireRiskLpp.setRate(rate);
        			        fireRiskLpp.setPremiumClass(premiumClass);
        			        
        			        fireRiskLppList.add(fireRiskLpp);
        			}
              
        return fireRiskLppList;
    }
	
	
	private static String getCellValueAsString(Cell cell) {
        String strCellValue = "";
        if (cell != null) {
            switch (cell.getCellType()) {
            case STRING:/* IBPTE-1944 */
                strCellValue = cell.toString();
                break;
            case NUMERIC:/* IBPTE-1944 */
                if (DateUtil.isCellDateFormatted(cell)) {
                    SimpleDateFormat dateFormat = new SimpleDateFormat(
                            "dd/MM/yyyy");
                    strCellValue = dateFormat.format(cell.getDateCellValue());
                } else {
                    Double value = cell.getNumericCellValue();
                    Long longValue = value.longValue();
                    strCellValue = new String(longValue.toString());
                }
                break;
            case BOOLEAN:/* IBPTE-1944 */
                strCellValue = new String(new Boolean(
                        cell.getBooleanCellValue()).toString());
                break;
            case BLANK:/* IBPTE-1944 */
                strCellValue = "";
                break;
            }
        }
        return strCellValue;
    }
	
	
	
	
	
	
}

