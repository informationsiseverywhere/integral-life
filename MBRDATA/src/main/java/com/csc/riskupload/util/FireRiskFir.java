package com.csc.riskupload.util;

public class FireRiskFir extends FireRisk{

	private String policyNumber;
    private String riskType;
    private String reinsuranceMethod;    
    private String state;
    private String register;
    private String riskRatingCode;
    private String constructionYear;
    private String interestInsured;
    private String sumInsured;
    private String premiumRate;
    private String premiumClass;
    private String riCedingBasis;   
    /**
	 * @return the riCedingBasis
	 */
	public String getRiCedingBasis() {
		return riCedingBasis;
	}
	/**
	 * @param riCedingBasis the riCedingBasis to set
	 */
	public void setRiCedingBasis(String riCedingBasis) {
		this.riCedingBasis = riCedingBasis;
	}
	private String situation;
  
    
	
	/**
	 * @return the policyNumber
	 */
	public String getPolicyNumber() {
		return policyNumber;
	}
	/**
	 * @param policyNumber the policyNumber to set
	 */
	public void setPolicyNumber(String policyNumber) {
		this.policyNumber = policyNumber;
	}
	/**
	 * @return the riskType
	 */
	public String getRiskType() {
		return riskType;
	}
	/**
	 * @param riskType the riskType to set
	 */
	public void setRiskType(String riskType) {
		this.riskType = riskType;
	}
	/**
	 * @return the reinsuranceMethod
	 */
	public String getReinsuranceMethod() {
		return reinsuranceMethod;
	}
	/**
	 * @param reinsuranceMethod the reinsuranceMethod to set
	 */
	public void setReinsuranceMethod(String reinsuranceMethod) {
		this.reinsuranceMethod = reinsuranceMethod;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the register
	 */
	public String getRegister() {
		return register;
	}
	/**
	 * @param register the register to set
	 */
	public void setRegister(String register) {
		this.register = register;
	}
	/**
	 * @return the riskRatingCode
	 */
	public String getRiskRatingCode() {
		return riskRatingCode;
	}
	/**
	 * @param riskRatingCode the riskRatingCode to set
	 */
	public void setRiskRatingCode(String riskRatingCode) {
		this.riskRatingCode = riskRatingCode;
	}
	/**
	 * @return the constructionYear
	 */
	public String getConstructionYear() {
		return constructionYear;
	}
	/**
	 * @param constructionYear the constructionYear to set
	 */
	public void setConstructionYear(String constructionYear) {
		this.constructionYear = constructionYear;
	}
	/**
	 * @return the interestInsured
	 */
	public String getInterestInsured() {
		return interestInsured;
	}
	/**
	 * @param interestInsured the interestInsured to set
	 */
	public void setInterestInsured(String interestInsured) {
		this.interestInsured = interestInsured;
	}
	/**
	 * @return the sumInsured
	 */
	public String getSumInsured() {
		return sumInsured;
	}
	/**
	 * @param sumInsured the sumInsured to set
	 */
	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}
	/**
	 * @return the premiumRate
	 */
	public String getPremiumRate() {
		return premiumRate;
	}
	/**
	 * @param premiumRate the premiumRate to set
	 */
	public void setPremiumRate(String premiumRate) {
		this.premiumRate = premiumRate;
	}
	/**
	 * @return the premiumClass
	 */
	public String getPremiumClass() {
		return premiumClass;
	}
	/**
	 * @param premiumClass the premiumClass to set
	 */
	public void setPremiumClass(String premiumClass) {
		this.premiumClass = premiumClass;
	}

	
	
	
	/**
	 * @return the situation
	 */
	public String getSituation() {
		return situation;
	}
	/**
	 * @param situation the situation to set
	 */
	public void setSituation(String situation) {
		this.situation = situation;
	}
	
    
}
