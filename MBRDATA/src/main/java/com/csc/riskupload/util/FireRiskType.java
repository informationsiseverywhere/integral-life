package com.csc.riskupload.util;

public enum FireRiskType {
	
	FIR,
    FIO,
    LPP
}
