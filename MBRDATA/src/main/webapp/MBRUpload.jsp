<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">              
<%@ page language="java" pageEncoding="UTF-8" %>
<html>
<head>
<title>Group - Member Upload</title>
<%String ctx = request.getContextPath() + "/";%>
<link type="text/css" href="<%=ctx%>theme/tabs.css" rel="stylesheet"/>

	<script type="text/javascript">

	function importLabel() {
		var importFile = this.document.getElementById("importFile");
	
		var messages = "";
		if(importFile.value.length == 0)
			messages = "File Name ";
		
		if(messages.length != 0 ){
			messages += "can not be empty. ";
			alert(messages);
			$("#message").val('');
			return;
		}
		document.MbrUpload.submit();
	}
	function importLabelExcel() {

		var importFile = this.document.getElementById("importExcelFile");
		
		var messages = "";
		if(importFile.value.length == 0)
			messages = "File Name ";
		
		if(messages.length != 0 ){
			messages += "can not be empty. ";
			alert(messages);
			$("#message").val('');
			return;
		}
		document.recUpload.submit();
	}
	</script>
	<link type="text/css" href="./css/site.css" rel="stylesheet"/>
	</head>

<body>
<div
	style="position: absolute; left:250px; top: 5px; height: 250px; width: 788; 
	width: 786px; border: #4d81b1 1px solid; background-color: #eeeeee; height: 700px;"
	id="mainareaDiv">

<ol id="toc">
    <li class="current"><a href="MBRUpload.jsp"><span>Group</span></a></li>
    <li><a href="RISKUpload.jsp"><span>Polisy</span></a></li>
    <li><a href="LIFEUpload.jsp"><span>Life</span></a></li>
</ol>


<div style="position: absolute; left:100px; top: 40px; height: 150px; width: 500; ">

<form name="MbrUpload" action="MBRUploadHandler" method="post" enctype="multipart/form-data">
<br>
<table align="center">
	<tr>
		<td>
		<h2>Member Upload - File Uploading System</h2>
		</td>
	</tr>
</table>

<div style="border: 2px solid #5185B5; height: 100px;">

<table width="600px" align="center" style=" background-color: #EEEEEE; ">
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr>
		<td style="width: 10%;">&nbsp;</td>
		<td style="width: 30%;"><b>File to be uploaded:</b></td>
		<td style="width: 60%;"><input type="file" name="importFile" id="importFile" width="200px" style="border: 1px solid #000000; background-color: #FFFFFF; "/></td>
	</tr>
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td><input type="button" value="Import" onclick="importLabel()"
						style="background-color: silver; font: oblique; "/></td>
	</tr>
</table>
<br/>
<br/>
<br/>
<table style="position: relative;">
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr>
		<td style="width: 50%;">&nbsp;</td><td style="text-align: center;">${uploadStatus}</td>
	</tr>

</table>
</div>
</form>
<form name="recUpload" action="RECUploadHandler" method="post" enctype="multipart/form-data">
<br>
<table align="center">
	<tr>
		<td>
		<h2>Receipts Upload - File Uploading System</h2>
		</td>
	</tr>
</table>

<div style="border: 2px solid #5185B5; height: 200px;">

<table width="600px" align="center" style=" background-color: #EEEEEE; ">
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr>
		<td style="width: 10%;">&nbsp;</td>
		<td style="width: 30%;"><b>File to be uploaded:</b></td>
		<td style="width: 60%;"><input type="file" name="importExcelFile" id="importExcelFile" width="200px" style="border: 1px solid #000000; background-color: #FFFFFF; "/></td>
	</tr>
	<tr><td colspan=3>&nbsp;</td></tr>
	<tr>
		<td>&nbsp;</td><td>&nbsp;</td><td><input type="button" value="Import" onclick="importLabelExcel()"
						style="background-color: silver; font: oblique; "/></td>
	</tr>
</table>
<br/>
<br/>
<br/>
<table style="position: relative;">
	<tr><td colspan=2>&nbsp;</td></tr>
	<tr>
		<td style="width: 50%;">&nbsp;</td><td style="text-align: center;">${uploadStatusExcel}</td>
	</tr>

</table>
</div>
</form>
</div>
</div>


</body>
</html>